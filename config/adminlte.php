<?php

$menu = [  
    //user menu
    [
        'header' => 'MI CUENTA',
        'can'    => 'user',
    ],
    [
        'text' => 'Perfil',
        'url'  => 'profile',
        'icon' => 'user-circle-o',
        'can'  => 'user',
    ],
    [
        'text' => 'Mi reputacion',
        'url'  => 'reputation',
        'icon' => 'line-chart',
        'can'  => 'user',
    ],
    [
        'text' => 'Mis wallets',
        'url'  => 'wallets',
        'icon' => 'bank',
        'can'  => 'user',
    ],
    [
        'text' => 'Conversiones',
        'url'  => 'exchanges',
        'icon' => 'exchange',
        'can'  => 'user',
    ],
    [
        'header' => 'CREDITOS',
        'can'    => 'user',
    ],
    [
        'text' => 'Mis créditos',
        'url'  => 'credits',
        'icon' => 'usd',
        'can'  => 'user',
    ],
    [
        'text' => 'Financiamientos',
        'url'  => 'financings',
        'icon' => 'handshake-o',
        'can'  => 'user',
    ],
    [
        'header' => 'COMPROBANTES',
        'can'    => 'user',
    ],
    [
        'text' => 'Mis comprobantes',
        'url'  => 'vouchers',
        'icon' => 'calculator',
        'can'  => 'user',
    ],
    //admin
    [
        'header' => 'MENU ADMINISTRADOR',
        'can' => 'admin'
    ],
    //[
    //    'text' => 'Roles',
    //    'url'  => '/roles',
    //    'icon' => 'bar-chart',
    //    'can'  => 'admin',
    //],
    [
        'text' => 'Usuarios',
        'url'  => '/users',
        'can'  => 'admin',
    ],
    //[
    //    'text' => 'Permisos',
    //    'url'  => '/404',
    //    'icon' => 'bar-chart',
    //    'can'  => 'admin',
    //],
    //'param-manager'
    [
        'header' => 'GESTOR DE PARAMETROS',
        'can' => 'param-manager'
    ],
    [
        'text' => 'Perfil',
        'url'  => 'profile',
        'icon' => 'user-circle-o',
        'can'  => 'param-manager',
    ],
    [
        'header' => 'PARAMETROS',
        'can' => 'param-manager'
    ],
    [
        'text'   => 'Paises',
        'url'    => '/countries',
        'icon'   => 'bar-chart',
        'can'    => 'param-manager',
    ],
    [
        'text'   => 'Provincias',
        'url'    => '/districts',
        'icon'   => 'bar-chart',
        'can'    => 'param-manager',
    ],
    [
        'text'   => 'Ciudades',
        'url'    => '/districts',
        'icon'   => 'bar-chart',
        'can'    => 'param-manager',
    ],
    //'insurance-manager'
    [
        'header' => 'DIVISION DE SEGUROS',
        'can'    => 'insurance-manager',
    ],
    [
        'text' => 'Perfil',
        'url'  => 'profile',
        'icon' => 'user-circle-o',
        'can'  => 'insurance-manager',
    ],
    [
        'text'   => 'Wallets',
        'url'    => 'wallets',
        'icon'   => 'bank',
        'can'    => 'insurance-manager',
    ],
    //'commission-manager'
    [
        'header' => 'DIVISION COMISIONES',
        'can'    => 'commission-manager',
    ],
    [
        'text' => 'Perfil',
        'url'  => 'profile',
        'icon' => 'user-circle-o',
        'can'  => 'commission-manager',
    ],
    [
        'text'   => 'Wallets',
        'url'    => 'wallets',
        'icon'   => 'bank',
        'can'    => 'commission-manager',
    ],

    //'lawsuit-manager'
    [
        'header' => 'DIVISION LITIGIOS',
        'can' => 'lawsuit-manager'
    ],
    [
        'text' => 'Perfil',
        'url'  => 'profile',
        'icon' => 'user-circle-o',
        'can'  => 'lawsuit-manager',
    ],
    [
        'header' => 'CREDITOS',
        'can'    => 'lawsuit-manager',
    ],
    [
        'text' => 'Créditos con citación',
        'url'  => '/credits/judicial-citation',
        'can'  => 'lawsuit-manager',
    ],
    [
        'text' => 'Créditos en litigio',
        'url'  => '/credits/lawsuit',
        'can'  => 'lawsuit-manager',
    ],
    [
        'text' => 'Créditos incobrables',
        'url'  => '/credits/bad',
        'can'  => 'lawsuit-manager',
    ],

    //'defaulter-manager'
    [
        'header' => 'DIVISION MOROSOS',
        'can' => 'defaulter-manager'
    ],
    [
        'text' => 'Perfil',
        'url'  => 'profile',
        'icon' => 'user-circle-o',
        'can'  => 'defaulter-manager',
    ],
    [
        'header' => 'CREDITOS',
        'can'    => 'defaulter-manager'
    ],
    [
        'text'   => 'Pago retrasado',
        'url'    => '/credits/late-payment',
        'icon'   => 'bar-chart',
        'can'    => 'defaulter-manager'
    ],
    [
        'text'   => 'Morosos',
        'url'    => '/credits/defaulter',
        'icon'   => 'bar-chart',
        'can'    => 'defaulter-manager'
    ],

    //'credit-manager'
    [
        'header' => 'DIVISION CREDITOS',
        'can' => 'credit-manager'
    ],
    [
        'text' => 'Perfil',
        'url'  => 'profile',
        'icon' => 'user-circle-o',
        'can'  => 'credit-manager',
    ],
    [
        'header' => 'CREDITOS',
        'can'    => 'credit-manager'
    ],
    [
        'text' => 'Créditos sin aprobar',
        'url'  => 'credits/to-approve',
        'icon' => 'bar-chart',
        'can'  => 'credit-manager',
    ],
    [
        'header' => 'PARAMETROS',
        'can'    => 'credit-manager'
    ],
    [
        'text' => 'Divisas',
        'url'  => '/currencies',
        'icon' => 'bar-chart',
        'can'  => 'credit-manager',
    ],
    [
        'text' => 'Profesiones',
        'url'  => '/professions',
        'icon' => 'bar-chart',
        'can'  => 'credit-manager',
    ],
    [
        'text' => 'Planes de pago',
        'url'  => '/payment-plans',
        'icon' => 'bar-chart',
        'can'  => 'credit-manager',
    ], 
    [
        'text' => 'Montos maximos',
        'url'  => '/max-amounts',
        'icon' => 'bar-chart',
        'can'  => 'credit-manager',
    ], 
    //audit
    [
        'header' => 'AUDITORIAS',
        'can'    => 'audit',
    ],
    [
        'text' => 'Ultimos mil registros',
        'url'  => '/audits/count=1000',
        'icon' => 'bar-chart',
        'can'  => 'audit',
    ],
    [
        'text' => 'Ultimos 10 mil registros',
        'url'  => '/audits/count=10000',
        'icon' => 'bar-chart',
        'can'  => 'audit',
    ],
    [
        'text' => 'Ultimos 100 mil registros',
        'url'  => '/audits/count=100000',
        'icon' => 'bar-chart',
        'can'  => 'audit',
    ],
    [
        'text' => 'Registro completo',
        'url'  => '/audits/count=all',
        'icon' => 'bar-chart',
        'can'  => 'audit',
    ], 
];
return [

    /*
    |--------------------------------------------------------------------------
    | Title
    |--------------------------------------------------------------------------
    |
    | The default title of your admin panel, this goes into the title tag
    | of your page. You can override it per page with the title section.
    | You can optionally also specify a title prefix and/or postfix.
    |
    */

    'title' => 'SisCred',

    'title_prefix' => '',

    'title_postfix' => '',

    /*
    |--------------------------------------------------------------------------
    | Logo
    |--------------------------------------------------------------------------
    |
    | This logo is displayed at the upper left corner of your admin panel.
    | You can use basic HTML here if you want. The logo has also a mini
    | variant, used for the mini side bar. Make it 3 letters or so
    |
    */

    'logo' => '<b>SisCred</b>',

    'logo_mini' => '<b>SC</b>',

    /*
    |--------------------------------------------------------------------------
    | Skin Color
    |--------------------------------------------------------------------------
    |
    | Choose a skin color for your admin panel. The available skin colors:
    | blue, black, purple, yellow, red, and green. Each skin also has a
    | ligth variant: blue-light, purple-light, purple-light, etc.
    |
    */

    'skin' => 'yellow',

    /*
    |--------------------------------------------------------------------------
    | Layout
    |--------------------------------------------------------------------------
    |
    | Choose a layout for your admin panel. The available layout options:
    | null, 'boxed', 'fixed', 'top-nav'. null is the default, top-nav
    | removes the sidebar and places your menu in the top navbar
    |
    */

    'layout' => 'fixed',

    /*
    |--------------------------------------------------------------------------
    | Collapse Sidebar
    |--------------------------------------------------------------------------
    |
    | Here we choose and option to be able to start with a collapsed side
    | bar. To adjust your sidebar layout simply set this  either true
    | this is compatible with layouts except top-nav layout option
    |
    */

    'collapse_sidebar' => false,

    /*
    |--------------------------------------------------------------------------
    | URLs
    |--------------------------------------------------------------------------
    |
    | Register here your dashboard, logout, login and register URLs. The
    | logout URL automatically sends a POST request in Laravel 5.3 or higher.
    | You can set the request to a GET or POST with logout_method.
    | Set register_url to null if you don't want a register link.
    |
    */

    'dashboard_url' => 'home',

    'logout_url' => 'logout',

    'logout_method' => null,

    'login_url' => 'login',
 
    'register_url' => 'register',

    /*
    |--------------------------------------------------------------------------
    | Menu Items
    |--------------------------------------------------------------------------
    |
    | Specify your menu items to display in the left sidebar. Each menu item
    | should have a text and and a URL. You can also specify an icon from
    | Font Awesome. A string instead of an array represents a header in sidebar
    | layout. The 'can' is a filter on Laravel's built in Gate functionality.
    |
    */
    
    'menu' => $menu,

    /*
    |--------------------------------------------------------------------------
    | Menu Filters
    |--------------------------------------------------------------------------
    |
    | Choose what filters you want to include for rendering the menu.
    | You can add your own filters to this array after you've created them.
    | You can comment out the GateFilter if you don't want to use Laravel's
    | built in Gate functionality
    |
    */

    'filters' => [
        JeroenNoten\LaravelAdminLte\Menu\Filters\HrefFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\ActiveFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\SubmenuFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\ClassesFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\GateFilter::class,
    ],

    /*
    |--------------------------------------------------------------------------
    | Plugins Initialization
    |--------------------------------------------------------------------------
    |
    | Choose which JavaScript plugins should be included. At this moment,
    | only DataTables is supported as a plugin. Set the value to true
    | to include the JavaScript file from a CDN via a script tag.
    |
    */

    'plugins' => [
        'datatables' => true,
        'select2'    => true,
        'chartjs'    => true,
    ],
];

