function drawExchangeRate(){
    var array = [];
    var currency_from = document.getElementById('from_currency_id').value;
	var currency_to   = document.getElementById('to_currency_id').value;
    var url = 'http://'+document.domain+':'+location.port+'/ajax/data-for-draw-exchange-rate-areachart/'+currency_from+'/'+currency_to;
    $.get(url, function(items){  

        console.log(items);
        $.each(items, function(index,item){
            array.push(item);
        });
        google.charts.load('current', {'packages':['corechart']});
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() {
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Fecha'); // Implicit domain label col.
            data.addColumn('number', 'Tasa de conversión'); // Implicit series 1 data col.
            data.addColumn({type:'string', role:'tooltip'});
            data.addRows(array);
            console.log(array);
            url = 'http://'+document.domain+':'+location.port+'/ajax/currencies/'+currency_from;
            $.get(url,function(from){
                url = 'http://'+document.domain+':'+location.port+'/ajax/currencies/'+currency_to;
                $.get(url,function(to){
                    var options = {
                        title: 'Tasa de conversión '+from.iso_code+'/'+to.iso_code,
                        hAxis: {title: 'Tiempo',  titleTextStyle: {color: '#333'}},
                        vAxis: {minValue: 0},
                        backgroundColor: 'transparent',
                    };
                    $('#exchange-rate-areachart').empty();
                    var chart = new google.visualization.AreaChart(document.getElementById('exchange-rate-areachart'));
                    chart.draw(data, options);
                });
            });
        }
    });
}
function exchangeDetailsAreachart(){
    var array = [];
    var exhange_id = document.getElementById('exchange_id').value;
    var currency_from = document.getElementById('from_currency_id').value;
	var currency_to   = document.getElementById('to_currency_id').value;
    var url = 'http://'+document.domain+':'+location.port+'/ajax/data-for-draw-exchange-details-areachart/'+exhange_id;
    $.get(url, function(items){
        console.log(items);
        $.each(items, function(index,item){
            array.push(item);
        });
        google.charts.load('current', {'packages':['corechart']});
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() {
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Fecha'); // Implicit domain label col.
            data.addColumn('number', 'Tasa de conversión'); // Implicit series 1 data col.
            data.addColumn({type:'string', role:'annotation'}); // annotation role col.
            data.addColumn({type:'string', role:'annotationText'}); // annotationText col.
            data.addColumn({type:'string', role:'tooltip'});
            data.addRows(array);
            console.log(array);
            url = 'http://'+document.domain+':'+location.port+'/ajax/currencies/'+currency_from;
            $.get(url,function(from){
                url = 'http://'+document.domain+':'+location.port+'/ajax/currencies/'+currency_to;
                $.get(url,function(to){
                    var options = {
                        title: 'Tasa de conversión '+from.iso_code+'/'+to.iso_code,
                        hAxis: {title: 'Tiempo',  titleTextStyle: {color: '#333'}},
                        vAxis: {minValue: 0},
                        backgroundColor: 'transparent',
                    };
                    var chart = new google.visualization.AreaChart(document.getElementById('exchange-details-areachart'));
                    chart.draw(data, options);
                });
            });
        }
    });
};
function walletsFoundsDonutchart(){
    var array = [];
    $.get('http://'+document.domain+':'+location.port+'/ajax/data-for-draw-wallets-founds-donutchart', function(items){  
        console.log(items);
        $.each(items, function(index,item){
            array.push(item);
        });
        google.charts.load("current", {packages:["corechart"]});
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() { 
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Wallet'); // Implicit domain label col.
            data.addColumn('number', 'Fondos'); // Implicit series 1 data col.
            data.addColumn({type:'string', role:'tooltip'});
            data.addRows(array);
            url = 'http://'+document.domain+':'+location.port+'/ajax/usd-amount-for-wallets';
            $.get(url, function(amount){
                var options = {
                    title: 'Montos totales en dólares: '+amount,
                    pieHole: 0.35,
                    backgroundColor: 'transparent',
                    chartArea:{width:'60%',height:'70%'},
                    animation: {
                        startup: true,
                        duration: 1000,
                    },
                };
                var chart = new google.visualization.PieChart(document.getElementById('wallets-founds-donutchart'));
                chart.draw(data, options);
            });
        }
    });
};
function walletTransactionsAreachart(){
    var array = [];
    var wallet_id = document.getElementById('wallet_id').value;
    var url = 'http://'+document.domain+':'+location.port+'/ajax/data-for-draw-wallet-transactions-areachart/'+wallet_id;
    $.get(url, function(items){  
        console.log(items);
        $.each(items, function(index,item){
            array.push(item);
        });
        google.charts.load('current', {'packages':['corechart']});
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() {
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Fecha'); // Implicit domain label col.
            data.addColumn('number', 'Balance'); // Implicit series 1 data col.
            data.addColumn({type:'string', role:'tooltip'});
            data.addRows(array);

            var options = {
              title: 'Historial del balande de la cuenta',
              hAxis: {title: 'Tiempo',  titleTextStyle: {color: '#333'}},
              vAxis: {minValue: 0},
              backgroundColor: 'transparent',
            };

            var chart = new google.visualization.AreaChart(document.getElementById('wallet-transactions-areachart'));
            chart.draw(data, options);
        }
    });
};
function creditsWalletDonutchart(){
    var array = [];
    var wallet_id = document.getElementById('wallet_id').value;
    var url = 'http://'+document.domain+':'+location.port+'/ajax/data-for-draw-credits-wallet-donutchart/'+wallet_id;
    $.get(url, function(items){  
        console.log(items);
        $.each(items, function(index,item){
            array.push(item);
        });
        google.charts.load("current", {packages:["corechart"]});
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() { 
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Estado'); // Implicit domain label col.
            data.addColumn('number', 'Cantidad'); // Implicit series 1 data col.
            data.addColumn({type:'string', role:'tooltip'});
            data.addRows(array);
            var options = {
                title: 'Ultimos estados de los créditos',
                pieHole: 0.35,
                backgroundColor: 'transparent',
                chartArea:{width:'60%',height:'70%'},
            };
            var chart = new google.visualization.PieChart(document.getElementById('credits-wallet-donutchart'));
            chart.draw(data, options);
        }
    });
};
function financingsWalletDonutchart(){
    var array = [];
    var wallet_id = document.getElementById('wallet_id').value;
    var url = 'http://'+document.domain+':'+location.port+'/ajax/data-for-draw-financings-wallet-donutchart/'+wallet_id;
    $.get(url, function(items){  
        console.log(items);
        $.each(items, function(index,item){
            array.push(item);
        });
        google.charts.load("current", {packages:["corechart"]});
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() { 
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Estado'); // Implicit domain label col.
            data.addColumn('number', 'Cantidad'); // Implicit series 1 data col.
            data.addColumn({type:'string', role:'tooltip'});
            data.addRows(array);
            var options = {
                title: 'Ultimos estados de los financiamientos',
                pieHole: 0.35,
                backgroundColor: 'transparent',
                chartArea:{width:'60%',height:'70%'},
            };
            var chart = new google.visualization.PieChart(document.getElementById('financings-wallet-donutchart'));
            chart.draw(data, options);
        }
    });
};
function creditsAllDonutchart(){
    var array = [];
    var url = 'http://'+document.domain+':'+location.port+'/ajax/data-for-draw-credits-all-donutchart';
    $.get(url, function(items){  
        console.log(items);
        $.each(items, function(index,item){
            array.push(item);
        });
        google.charts.load("current", {packages:["corechart"]});
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() { 
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Divisa'); // Implicit domain label col.
            data.addColumn('number', 'Cantidad'); // Implicit series 1 data col.
            data.addColumn({type:'string', role:'tooltip'});
            data.addRows(array);
            url = 'http://'+document.domain+':'+location.port+'/ajax/usd-amount-for-credits';
            $.get(url, function(amount){
                var options = {
                    title: 'Créditos totales en dólares: '+amount,
                    pieHole: 0.35,
                    backgroundColor: 'transparent',
                    animation: {
                        startup: true,
                        duration: 1000,
                    },
                    chartArea:{width:'60%',height:'70%'},
                };
                var chart = new google.visualization.PieChart(document.getElementById('credits-all-donutchart'));
                chart.draw(data, options);
            });
        }
    });
};
function financingsAllDonutchart(){
    var array = [];
    var url = 'http://'+document.domain+':'+location.port+'/ajax/data-for-draw-financings-all-donutchart';
    $.get(url, function(items){  
        console.log(items);
        $.each(items, function(index,item){
            array.push(item);
        });
        google.charts.load("current", {packages:["corechart"]});
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() { 
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Divisa'); // Implicit domain label col.
            data.addColumn('number', 'Cantidad'); // Implicit series 1 data col.
            data.addColumn({type:'string', role:'tooltip'});
            data.addRows(array);
            url = 'http://'+document.domain+':'+location.port+'/ajax/usd-amount-for-financings';
            $.get(url, function(amount){  
                var options = {
                    title: 'Financiaciones totales en dólares: '+amount,
                    pieHole: 0.35,
                    backgroundColor: 'transparent',
                    chartArea:{width:'60%',height:'70%'},
                };
                var chart = new google.visualization.PieChart(document.getElementById('financings-all-donutchart'));
                chart.draw(data, options);
            });
        }
    });
};
function creditFinancingAreachart(){
    var array = [];
    var credit_id = document.getElementById('credit_id').value;
    var url = 'http://'+document.domain+':'+location.port+'/ajax/data-for-draw-credit-financing-areachart/'+credit_id;
    $.get(url, function(items){  
        console.log(items);
        $.each(items, function(index,item){
            array.push(item);
        });
        google.charts.load('current', {'packages':['corechart']});
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() {
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Fecha'); // Implicit domain label col.
            data.addColumn('number', 'Financiamientos'); // Implicit series 1 data col.
            data.addColumn({type:'string', role:'tooltip'});
            data.addColumn({type:'boolean', role:'certainty'}); // certainty col.
            data.addRows(array);
            console.log(array);
            var options = {
                title: 'Financiamientos para el credito',
                hAxis: {title: 'Tiempo',  titleTextStyle: {color: '#333'}},
                vAxis: {minValue: 0},
                backgroundColor: 'transparent',
              };
          
              var chart = new google.visualization.AreaChart(document.getElementById('credit-financing-areachart'));
              chart.draw(data, options);
        }
    });
};
function creditPayLeftDonutchart(){
    var array = [];
    var credit_id = document.getElementById('credit_id').value;
    var url = 'http://'+document.domain+':'+location.port+'/ajax/data-for-draw-credit-pay-left-donutchart/'+credit_id;
    $.get(url, function(items){  
        console.log(items);
        $.each(items, function(index,item){
            array.push(item);
        });
        google.charts.load("current", {packages:["corechart"]});
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() { 
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Restante'); // Implicit domain label col.
            data.addColumn('number', 'Monto'); // Implicit series 1 data col.
            data.addColumn({type:'string', role:'tooltip'});
            data.addRows(array);
            var options = {
                title: 'Monto restante por pagar',
                pieHole: 0.35,
                backgroundColor: 'transparent',
                chartArea:{width:'60%',height:'70%'},
            };
            var chart = new google.visualization.PieChart(document.getElementById('credit-pay-left-donutchart'));
            chart.draw(data, options);
        }
    });
};
function walletsFoundsColumnchart(){
    var array = [];
    $.get('http://'+document.domain+':'+location.port+'/ajax/data-for-draw-wallets-founds-donutchart', function(items){  
        console.log(items);
        $.each(items, function(index,item){
            array.push(item);
        });
        google.charts.load('current', {'packages':['bar']});
        google.charts.setOnLoadCallback(drawStuff);
        function drawStuff() {
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Wallet'); // Implicit domain label col.
            data.addColumn('number', ''); // Implicit series 1 data col.
            data.addColumn({type:'string', role:'tooltip'});
            data.addRows(array);
            var options = {
                title: 'Monto en los wallets',
                backgroundColor: 'transparent',
                animation: {
                    startup: true,
                    duration: 1000,
                },
                bar: {
                    groupWidth: "50%"
                },
                chartArea:{width:'50%',height:'60%'}
            };
            var chart = new google.charts.Bar(document.getElementById('wallets-founds-columnchart'));
            // Convert the Classic options to Material options.
            chart.draw(data, google.charts.Bar.convertOptions(options));
        };
    });
};
function paymentsIndexCalendarChart(){
    var array = [];
    var credit_id = document.getElementById('credit_id').value;
    var url = 'http://'+document.domain+':'+location.port+'/ajax/data-for-draw-payments-index-calendarchart/'+credit_id;
    $.get(url, function(items){  
        console.log(items);
        $.each(items, function(index,item){
            var date = new Date(item[0][0],(item[0][1]) - 1,item[0][2]);
            array.push([
                date,
                item[1],
                item[2],
            ]);
        });
        google.charts.load('current', {'packages':['calendar']});
        google.charts.setOnLoadCallback(drawChart);
        
        function drawChart() {
            var dataTable = new google.visualization.DataTable();
            dataTable.addColumn({ type: 'date', id: 'Fecha' });
            dataTable.addColumn({ type: 'number', id: 'Estado' });
            dataTable.addColumn({type:'string', role:'tooltip'});
            dataTable.addRows(array);
        
            var chart = new google.visualization.Calendar(document.getElementById('payments-index-calendarchart'));
        
            var options = {
                subtitle: "Historial de estados de la cuota",
                //height: 350,
                backgroundColor: 'transparent',
                chartArea:{width:'50%',height:'60%'}
            };
        
            chart.draw(dataTable, options);
        };
    });
};
function paymentPhasesCalendarchart(){
    var array = [];
    var payment_id = document.getElementById('payment_id').value;
    var url = 'http://'+document.domain+':'+location.port+'/ajax/data-for-draw-payment-phases-calendarchart/'+payment_id;
    $.get(url, function(items){  
        console.log(items);
        $.each(items, function(index,item){
            var date = new Date(item[0][0],(item[0][1]) - 1,item[0][2]);

            array.push([
                date,
                item[1],
                item[2],
            ]);
        });
        google.charts.load('current', {'packages':['calendar']});
        google.charts.setOnLoadCallback(drawChart);
        
        function drawChart() {
            var dataTable = new google.visualization.DataTable();
            dataTable.addColumn({ type: 'date', id: 'Fecha' });
            dataTable.addColumn({ type: 'number', id: 'Estado' });
            dataTable.addColumn({type:'string', role:'tooltip'});
            dataTable.addRows(array);
        
            var chart = new google.visualization.Calendar(document.getElementById('payment-phases-calendarchart'));
        
            var options = {
                subtitle: "Historial de estados de la cuota",
                //height: 350,
                backgroundColor: 'transparent',
                chartArea:{width:'50%',height:'60%'}
            };
        
            chart.draw(dataTable, options);
        };
    });
};
function creditPhasesCalendarchart(){
    var array = [];
    var credit_id = document.getElementById('credit_id').value;
    var url = 'http://'+document.domain+':'+location.port+'/ajax/data-for-draw-credit-phases-calendarchart/'+credit_id;
    $.get(url, function(items){  
        console.log(items);
        $.each(items, function(index,item){
            var date = new Date(item[0][0],(item[0][1]) - 1,item[0][2]);

            array.push([
                date,
                item[1],
                item[2],
            ]);
        });
        google.charts.load('current', {'packages':['calendar']});
        google.charts.setOnLoadCallback(drawChart);
        
        function drawChart() {
            var dataTable = new google.visualization.DataTable();
            dataTable.addColumn({ type: 'date', id: 'Fecha' });
            dataTable.addColumn({ type: 'number', id: 'Estado' });
            dataTable.addColumn({type:'string', role:'tooltip'});
            dataTable.addRows(array);
        
            var chart = new google.visualization.Calendar(document.getElementById('credit-phases-calendarchart'));
        
            var options = {
                subtitle: "Historial de estados del credito",
                //height: 350,
                backgroundColor: 'transparent',
                chartArea:{width:'50%',height:'60%'}
            };
        
            chart.draw(dataTable, options);
        };
    });
};
function financingPhasesCalendarchart(){
    var array = [];
    var financing_id = document.getElementById('financing_id').value;
    var url = 'http://'+document.domain+':'+location.port+'/ajax/data-for-draw-financing-phases-calendarchart/'+financing_id;
    $.get(url, function(items){  
        console.log(items);
        $.each(items, function(index,item){
            var date = new Date(item[0][0],(item[0][1]) - 1,item[0][2]);

            array.push([
                date,
                item[1],
                item[2],
            ]);
        });
        google.charts.load('current', {'packages':['calendar']});
        google.charts.setOnLoadCallback(drawChart);
        
        function drawChart() {
            var dataTable = new google.visualization.DataTable();
            dataTable.addColumn({ type: 'date', id: 'Fecha' });
            dataTable.addColumn({ type: 'number', id: 'Estado' });
            dataTable.addColumn({type:'string', role:'tooltip'});
            dataTable.addRows(array);
        
            var chart = new google.visualization.Calendar(document.getElementById('financing-phases-calendarchart'));
        
            var options = {
                subtitle: "Historial de estados de la financiacion",
                //height: 350,
                backgroundColor: 'transparent',
                chartArea:{width:'50%',height:'60%'}
            };
        
            chart.draw(dataTable, options);
        };
    });
};
function marginAreachart(){
    var array = [];
    var reputation_id = document.getElementById('reputation_id').value;
    var url = 'http://'+document.domain+':'+location.port+'/ajax/data-for-draw-margin-areachart/'+reputation_id;
    console.log('Anttes de hacer la peticion get, url '+url);
    $.get(url, function(items){  
        console.log('Ya hice la peticion get');
        console.log(items);
        $.each(items, function(index,item){
            array.push(item);
        });
        google.charts.load('current', {'packages':['corechart']});
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() {
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Fecha'); // Implicit domain label col.
            data.addColumn('number', 'Margen'); // Implicit series 1 data col.
            data.addColumn({type:'string', role:'tooltip'});
            data.addRows(array);
            console.log(array);
            var options = {
                title: 'Historial del margen del usuario',
                hAxis: {title: 'Tiempo',  titleTextStyle: {color: '#333'}},
                vAxis: {minValue: 0},
                backgroundColor: 'transparent',
              };
          
              var chart = new google.visualization.AreaChart(document.getElementById('margin-areachart'));
              chart.draw(data, options);
        }
    });
};
function interestAreachart(){
    var array = [];
    var reputation_id = document.getElementById('reputation_id').value;
    var url = 'http://'+document.domain+':'+location.port+'/ajax/data-for-draw-interest-areachart/'+reputation_id;
    console.log('Anttes de hacer la peticion get, url '+url);
    $.get(url, function(items){  
        console.log('Ya hice la peticion get');
        console.log(items);
        $.each(items, function(index,item){
            array.push(item);
        });
        google.charts.load('current', {'packages':['corechart']});
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() {
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Fecha'); // Implicit domain label col.
            data.addColumn('number', 'Porcentaje'); // Implicit series 1 data col.
            data.addColumn({type:'string', role:'tooltip'});
            data.addRows(array);
            console.log(array);
            var options = {
                title: 'Historial de porcentajes de interes',
                hAxis: {title: 'Tiempo',  titleTextStyle: {color: '#333'}},
                vAxis: {minValue: 0},
                backgroundColor: 'transparent',
              };
          
              var chart = new google.visualization.AreaChart(document.getElementById('interest-areachart'));
              chart.draw(data, options);
        }
    });
};
function insuranceAreachart(){
    var array = [];
    var reputation_id = document.getElementById('reputation_id').value;
    var url = 'http://'+document.domain+':'+location.port+'/ajax/data-for-draw-insurance-areachart/'+reputation_id;
    console.log('Anttes de hacer la peticion get, url '+url);
    $.get(url, function(items){  
        console.log('Ya hice la peticion get');
        console.log(items);
        $.each(items, function(index,item){
            array.push(item);
        });
        google.charts.load('current', {'packages':['corechart']});
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() {
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Fecha'); // Implicit domain label col.
            data.addColumn('number', 'Porcentaje'); // Implicit series 1 data col.
            data.addColumn({type:'string', role:'tooltip'});
            data.addRows(array);
            console.log(array);
            var options = {
                title: 'Historial de porcentajes de seguro',
                hAxis: {title: 'Tiempo',  titleTextStyle: {color: '#333'}},
                vAxis: {minValue: 0},
                backgroundColor: 'transparent',
              };
          
              var chart = new google.visualization.AreaChart(document.getElementById('insurance-areachart'));
              chart.draw(data, options);
        }
    });
};