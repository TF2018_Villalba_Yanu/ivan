function countryChange(){
    var country_id = document.getElementById('country_id').value;
    var url = 'http://'+document.domain+':'+location.port+'/ajax/districts/'+country_id;
    $.get(url, function(data){
        console.log(data);
        $('#district_id').empty();
        $.each(data,function(index,districtObj){
            $('#district_id').append('<option value="' + districtObj.id + '">' + districtObj.name + '</option>');
        });
    });
}
function districtChange(){
    var district_id = document.getElementById('district_id').value;
    var url = 'http://'+document.domain+':'+location.port+'/ajax/cities/'+district_id;
    $.get(url, function(data){
        console.log(data);
        $('#city_id').empty();
        $.each(data,function(index,cityObj){
            $('#city_id').append('<option value="' + cityObj.id + '">' + cityObj.name + '</option>');
        });
    });

};
$('#initial_limit').on('focusout',function(e){
    var min = e.target.value;
    $('#max_limit_min').empty();
    $('#max_limit_min').append('<input required type="number" class="form-control" placeholder="Monto maximo" name="max_limit" id="max_limit"  min="'+min+'">');

});
$(document).ready(function() {
    $('.table-paginated').DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"      
        }
    });
} );
//new Vue({
//    el: '#app',
//    data:{
//        credits: [],
//        currencies: [],
//        paymentPlans: [],
//    },
//    methods:{
//        getCredits: function() {
//            var urlCredits = 'credit';
//            axios.get(urlCredits).then(response => {
//                this.credits = response.data
//            });
//        },
//        getCurrencies: function() {
//            var urlCurrencies = 'currency';
//            axios.get(urlCurrencies).then(response => {
//                this.currencies = response.data
//            });
//        },
//        getPaymentPlansByCurrency: function(currency_id) {
//            var urlPaymentPlan = 'payment-plan-by-currency/' + currency_id;
//            axios.get(urlPaymentPlan).then(response => {
//                this.paymentPlans = response.data
//            });
//        },
//    },
//}),
//
//
//new Vue({
//    el: '#credits',
//    created: function(){
//        this.getCredits();
//    },
//    data: {
//        credits: []
//    },
//    methods:{
//        getCredits: function() {
//            var urlCredits = 'credit';
//            axios.get(urlCredits).then(response => {
//                this.credits = response.data
//            });
//        },
//        deleteCredit: function(){
//            toastr.success('Eliminado correctamente');
//        },
//    },
//}),
//new Vue({
//    el: '#payment-plans',
//    created: function(){
//        this.getPaymentPlans();
//    },
//    data: {
//        paymentPlans: []
//    },
//    methods:{
//        getPaymentPlans: function() {
//            var urlPaymentPlan = 'payment-plan';
//            axios.get(urlPaymentPlan).then(response => {
//                this.paymentPlans = response.data
//            });
//        },
//        
//    }
//}),
//new Vue({
//    el: '#currencies',
//    created: function(){
//        this.getCurrencies();
//    },
//    data: {
//        currencies: []
//    },
//    methods:{
//        getCurrencies: function() {
//            var urlCurrencies = 'currency';
//            axios.get(urlCurrencies).then(response => {
//                this.currencies = response.data
//            });
//        },
//    }
//}),
//new Vue({
//    el: '#wallets',
//    created: function(){
//        this.getWallets();
//    },
//    data: {
//        wallets: []
//    },
//    methods:{
//        getWallets: function() {
//            var urlWallets = 'wallet';
//            axios.get(urlWallets).then(response => {
//                this.wallets = response.data
//            });
//        },
//        getCurrency: function(wallet_id) {
//            var urlWallets = 'wallet/'+wallet_id+'/get-currency';
//            axios.get(urlWallets).then(response => {
//                return response.data
//            });
//        },
//    }
//})
//
//