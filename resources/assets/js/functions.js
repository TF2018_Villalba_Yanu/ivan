function inputMoneyWhitCurrency(currency_id){
	console.log('Entre a inputMoneyWhitCurrency(currency_id)');
	$('#modal-header').empty();
	$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
	$('#modal-header').append('<h4 class="modal-title">Ingresar dinero</h4>');
	loading();
	var url = 'http://'+document.domain+':'+location.port+'/ajax/currencies/'+currency_id;
	$.get(url, function(selected_currency){
		$('#modal-header').empty();
		$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
		$('#modal-header').append('<h4 class="modal-title">Ingresar dinero en '+ selected_currency.name+'</h4>');

		$('#modal-body').empty();
		$('#modal-body').append('<label>Seleccione la divisa</label>');
		url = 'http://'+document.domain+':'+location.port+'/ajax/currencies';
		$.get(url, function(currencies){
			console.log('Entre al select');
			console.log(currencies);
			$('#modal-body').append('<div class="form-group">');
			$('#modal-body').append('<select class="form-control" name="currency_id" id="currency_id" onchange="inputMoneyWhitCurrency(value)"></select>');
			$('#currency_id').append('<option value="' + selected_currency.id + ' selected">' + selected_currency.name + '</option>');			
			$.each(currencies,function(index,currency){
    	       if (currency.id != selected_currency.id) {
					$('#currency_id').append('<option value="' + currency.id + '">' + currency.name + '</option>');
				}
    	    });
			$('#modal-body').append('</div>');
			console.log('Sali del select')
			url = 'http://'+document.domain+':'+location.port+'/ajax/local-convert/usd/'+selected_currency.iso_code+'/5000';
			$.get(url, function(max_amount){
				$('#modal-body').append('<input type="hidden"  id="min_amount" value="'+selected_currency.min_value+'">');
				$('#modal-body').append('<input type="hidden"  id="max_amount" value="'+max_amount+'">');
				$('#modal-body').append('<input type="hidden"  id="currency-id" value="'+ selected_currency.id+'">');
				$('#modal-body').append('<div class="form-group">');

				url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+selected_currency.id+'/'+max_amount+'/1';
				$.get(url, function(max){
					$('#modal-body').append('<label>Ingrese el monto (máximo '+max+')<br></label>');
					url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+selected_currency.id+'/123.4567890/1';
					$.get(url, function(placeholder){
						$('#modal-body').append('<input type="text" class="form-control" id="amount" placeholder="Ejemplo: '+placeholder.replace(selected_currency.simbol+' ','')+'" autofocus onkeyup="amountVerifier()">');
						$('#modal-body').append('<label id="amount-label"></label>');
						$('#modal-body').append('</div>');

						$('#modal-footer').empty();
						$('#modal-footer').append('<button id="submit-button" type="button" class="btn btn-success" onclick="inputMoney()">Ingresar Dinero</button>');
						$('#submit-button').hide();
						$('#modal-footer').append('<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>');	
					});
				});
			});
		});
	});
}
function outputMoneyWhitCurrency(wallet_id){
	console.log('Entre a outputMoneyWhitCurrency(wallet_id)');
	$('#modal-header').empty();
	$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
	$('#modal-header').append('<h4 class="modal-title" >Retirar dinero</h4>');
	loading();
	var url = 'http://'+document.domain+':'+location.port+'/ajax/wallets/'+wallet_id;
	$.get(url, function(selected_wallet){
		url = 'http://'+document.domain+':'+location.port+'/ajax/currencies/'+selected_wallet.currency_id;
		$.get(url, function(selected_currency){
			$('#modal-header').empty();
			$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
			$('#modal-header').append('<h4 class="modal-title" >Retirar dinero en '+ selected_currency.name+'</h4>');
		
			$('#modal-body').empty();
			$('#modal-body').append('<label>Seleccione la divisa</label>');
			url = 'http://'+document.domain+':'+location.port+'/ajax/wallets-with-balance';
			$.get(url, function(wallets){
				console.log('Entre al select');
				console.log(wallets);
				$('#modal-body').append('<div class="form-group">');
				$('#modal-body').append('<select class="form-control" name="currency_id" id="currency_id" onchange="outputMoneyWhitCurrency(value)">');
				$('#currency_id').append('<option value="' + selected_wallet.id + ' selected">' + selected_currency.name + '</option>');			
				$.each(wallets,function(index,wallet){
					if (wallet.currency_id != selected_currency.id) {
						url = 'http://'+document.domain+':'+location.port+'/ajax/currencies/'+wallet.currency_id;
						$.get(url, function(currency){
							$('#currency_id').append('<option value="' + wallet.id + '">' + currency.name + '</option>');
						});
					}
	    	    });
				$('#modal-body').append('</select><br>');
				$('#modal-body').append('</div>');
				console.log('Sali del select')
				$('#modal-body').append('<input type="hidden"  id="min_amount" value="'+selected_currency.min_value+'">');
				$('#modal-body').append('<input type="hidden"  id="max_amount" value="'+selected_wallet.account_balance+'">');
				$('#modal-body').append('<input type="hidden"  id="currency-id" value="'+ selected_currency.id+'">');
				$('#modal-body').append('<div class="form-group">');
				url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+selected_currency.id+'/'+selected_wallet.account_balance+'/1';
				$.get(url, function(max){
					$('#modal-body').append('<label>Ingrese el monto (máximo '+max+')<br></label>');
					url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+selected_currency.id+'/123.4567890/1';
					$.get(url, function(placeholder){
						$('#modal-body').append('<input type="text" class="form-control" id="amount" placeholder="Ejemplo: '+placeholder.replace(selected_currency.simbol+' ','')+'" autofocus onkeyup="amountVerifier()">');
						$('#modal-body').append('<label id="amount-label"></label>');
						$('#modal-body').append('</div>');

						$('#modal-footer').empty();
						$('#modal-footer').append('<button id="submit-button" type="button" class="btn btn-danger" onclick="outputMoney()">Retirar Dinero</button>');
						$('#submit-button').hide();
						$('#modal-footer').append('<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>');	
					});				
				});
			});
		});
	});
}
function selectCurrencyToFinanciate(wallet_id){
	console.log('Entre a selectCurrencyToFinanciate(wallet_id)');
	$('#modal-header').empty();
	$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
	$('#modal-header').append('<h4 class="modal-title" >Financiar</h4>');
	loading();
	var url = 'http://'+document.domain+':'+location.port+'/ajax/wallets/'+wallet_id;
	$.get(url, function(selected_wallet){
		url = 'http://'+document.domain+':'+location.port+'/ajax/currencies/'+selected_wallet.currency_id;
		$.get(url, function(selected_currency){
			url = 'http://'+document.domain+':'+location.port+'/ajax/wallets-to-financiate';
			$.get(url, function(wallets){
				console.log('Entre al select');
				console.log(wallets);
				$('#modal-header').empty();
				$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
				$('#modal-header').append('<h4 class="modal-title" >Financiar en '+ selected_currency.name+'</h4>');
			
				$('#modal-body').empty();
				$('#modal-body').append('<label>Seleccione la divisa</label>');	
				$('#modal-body').append('<div class="form-group">');
				$('#modal-body').append('<select class="form-control" name="currency_id" id="currency_id" onchange="selectCurrencyToFinanciate(value)">');
				$('#currency_id').append('<option value="' + selected_wallet.id + ' selected">' + selected_currency.name + '</option>');			
				$.each(wallets,function(index,wallet){
					if (wallet.currency_id != selected_currency.id) {
						url = 'http://'+document.domain+':'+location.port+'/ajax/currencies/'+wallet.currency_id;
						$.get(url, function(currency){
							$('#currency_id').append('<option value="' + wallet.id + '">' + currency.name + '</option>');
						});
					}
	    	    });
				$('#modal-body').append('</select>');
				$('#modal-body').append('</div>');

				$('#modal-footer').empty();
				$('#modal-footer').append('<a href="http://'+document.domain+':'+location.port+'/financings/wallet='+selected_wallet.id+'" class="btn btn-success">Comenzar</a>');
				$('#modal-footer').append('<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>');
			});
		});
	});
}
function financiateWithCredit(credit_id){
	console.log('Entre a financiateWithCredit(credit_id)');
	$('#modal-header').empty();
	$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
	$('#modal-header').append('<h4 class="modal-title" >Financiar crédito</h4>');
	loading();
	var url = 'http://'+document.domain+':'+location.port+'/ajax/credits/'+credit_id;
	$.get(url,function(credit){
		$('#modal-header').empty();
		$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
		$('#modal-header').append('<h4 class="modal-title" >Financiar crédito '+credit.id+' con un perfil '+credit.rank+'</h4>');
		url = 'http://'+document.domain+':'+location.port+'/ajax/currencies/'+credit.wallet.currency_id;
		$.get(url, function(currency){
			url = 'http://'+document.domain+':'+location.port+'/ajax/financings/credit='+credit_id;
			$.get(url,function(financing){
				$('#modal-body').empty();
				if(financing != ''){
					console.log('El crédito tiene financiamiento previo')
					url = 'http://'+document.domain+':'+location.port+'/ajax/financings/'+financing.id+'/amount';
					$.get(url,function(financing_amount){
						url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+currency.id+'/'+financing_amount+'/1';
						$.get(url, function(amount_string){
							$('#modal-body').append('<label>Ya financiaste '+amount_string+' en este credito.</label>');
						});
					});
					$('#modal-body').append('<input type="hidden"  name="insurance" value="'+financing.have_insurance+'">');
					$('#modal-body').append('<input type="hidden"  id="financing-id" value="'+financing.id+'">');
				}else{
					$('#modal-body').append('<input type="hidden"  id="financing-id" value="0">');
				}
				var max = credit.financing_left;
				url = 'http://'+document.domain+':'+location.port+'/ajax/wallets/currency='+currency.id;
				$.get(url,function(wallet){
					if(Number(wallet.account_balance) < Number(max)){
						max = wallet.account_balance;
					}
					url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+currency.id+'/'+max+'/1';
					$.get(url,function(max_string){
						$('#modal-body').append('<input type="hidden"  id="is_abort_financing" value="0">');
						$('#modal-body').append('<input type="hidden"  id="min_amount" value="'+currency.min_value+'">');
						$('#modal-body').append('<input type="hidden"  id="max_amount" value="'+max+'">');
						$('#modal-body').append('<input type="hidden"  id="currency-id" value="'+currency.id+'">');
						$('#modal-body').append('<input type="hidden"  id="credit-id" value="'+credit.id+'">');
						$('#modal-body').append('<div class="form-group">');
						$('#modal-body').append('<label>Ingrese el monto (máximo '+max_string+')<br></label>');
						url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+currency.id+'/123.4567890/1';
						$.get(url, function(placeholder){
							$('#modal-body').append('<input type="text" class="form-control" id="amount" placeholder="Ejemplo: '+placeholder.replace(currency.simbol+' ','')+'" autofocus onkeyup="amountVerifierForFinancing()">');
							$('#modal-body').append('<label id="amount-label"></label>');
							$('#modal-body').append('</div>');
							if (financing == '') {
								$('#modal-body').append('<div class="form-group">');
								$('#modal-body').append('<label>Seguro</label><br>');
								$('#modal-body').append('<input required type="radio" name="insurance" value="1" checked onchange="simulateFinancing()"> Con seguro (si no te devuelven, nosotros lo hacemos)<br>');
								$('#modal-body').append('<input required type="radio" name="insurance" value="0" onchange="simulateFinancing()"> Sin seguro (pueden no devolverte el dinero)<br>');
								$('#modal-body').append('</div>');
							}else{
								if(financing.have_insurance == 1){
									$('#modal-body').append('<p><strong>Financiaste con seguro.</strong> Esta opcion ya no la puedes cambiar</p>');
									$('#modal-body').append('<input type="hidden"  id="insurace-id" value="1">');
								}else{
									$('#modal-body').append('<p><strong>Financiaste sin seguro.</strong> Esta opcion ya no la puedes cambiar</p>');
									$('#modal-body').append('<input type="hidden"  id="insurace-id" value="0">');
								}
							}
							$('#modal-body').append('<br><div id="simulate-div">');
							$('#modal-footer').empty();
							$('#modal-footer').append('<button id="submit-button" type="button" class="btn btn-success" onclick="financiate()">Financiar</button>');
							$('#submit-button').hide();
							$('#modal-footer').append('<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>');							
						});
					});
				});
			});
		});
	});
}
function cancelFinancingWithCredit(credit_id){
	console.log('Entre a cancelFinancingWithCredit(credit_id)');
	$('#modal-header').empty();
	$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
	$('#modal-header').append('<h4 class="modal-title" >Quitar dinero del financiamiento</h4>');
	loading();
	var url = 'http://'+document.domain+':'+location.port+'/ajax/credits/'+credit_id;
	$.get(url,function(credit){
		url = 'http://'+document.domain+':'+location.port+'/ajax/currencies/'+credit.wallet.currency_id;
		$.get(url, function(currency){
			url = 'http://'+document.domain+':'+location.port+'/ajax/financings/credit='+credit_id;
			$.get(url,function(financing){
				$('#modal-header').empty();
				$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
				$('#modal-header').append('<h4 class="modal-title" >Quitar dinero del financiamiento '+financing.id+'</h4>');
				$('#modal-body').empty();
				if(financing != ''){
					url = 'http://'+document.domain+':'+location.port+'/ajax/financings/'+financing.id+'/amount';
					$.get(url,function(financing_amount){
						url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+currency.id+'/'+financing_amount+'/1';
						$.get(url, function(amount_string){
							$('#modal-body').append('<input type="hidden"  id="is_abort_financing" value="1">');
							$('#modal-body').append('<input type="hidden"  id="max_amount" value="'+financing_amount+'">');
							$('#modal-body').append('<input type="hidden"  id="min_amount" value="'+currency.min_value+'">');
							$('#modal-body').append('<input type="hidden"  id="currency-id" value="'+currency.id+'">');
							$('#modal-body').append('<input type="hidden"  id="financing-id" value="'+financing.id+'">');
							$('#modal-body').append('<input type="hidden"  id="credit-id" value="'+credit.id+'">');
							$('#modal-body').append('<input type="hidden"  id="insurace-id" value="'+financing.have_insurance+'">');
							$('#modal-body').append('<div class="form-group">');
							$('#modal-body').append('<label>Ingresar el monto (maximo '+amount_string+')');
							url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+currency.id+'/123.4567890/1';
							$.get(url, function(placeholder){
								$('#modal-body').append('<input type="text" class="form-control" id="amount" placeholder="Ejemplo: '+placeholder.replace(currency.simbol+' ','')+'" autofocus onkeyup="amountVerifierForFinancing()">');
								$('#modal-body').append('<label id="amount-label"></label>');
								$('#modal-body').append('</div><br>');
								$('#modal-body').append('<div id="simulate-div">');
								$('#modal-footer').empty();
								$('#modal-footer').append('<button id="submit-button" type="button" class="btn btn-danger" onclick="cancelFinancing()">Quitar</button>');
								$('#submit-button').hide();
								$('#modal-footer').append('<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>');			
							});
						});
					});
				}
			});
		});
	});
}
function payWithCredit(credit_id,wallet_id){
	console.log('Entre a payWhitCredit(credit_id= '+credit_id+',wallet_id= '+wallet_id+')');
	$('#modal-header').empty();
	$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
	$('#modal-header').append('<h4 class="modal-title" >Realizar pago</h4>');
	loading();
	var url = 'http://'+document.domain+':'+location.port+'/ajax/credits/'+credit_id;
	$.get(url,function(credit){
		$('#modal-header').empty();
		$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
		$('#modal-header').append('<h4 class="modal-title" >Realizar pago del credito '+credit.id+'</h4>');
		url = 'http://'+document.domain+':'+location.port+'/ajax/wallets/'+wallet_id;
		$.get(url,function(selected_wallet){
			$('#modal-header').empty();
			$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
			$('#modal-header').append('<h4 class="modal-title" >Realizar pago del credito '+credit.id+' con '+selected_wallet.currency.name+'</h4>');
			url = 'http://'+document.domain+':'+location.port+'/ajax/payments/'+credit.first_unpayed_payment.id+'/unpayed-amount';
			$.get(url,function(payment_amount){
				url = 'http://'+document.domain+':'+location.port+'/ajax/wallets-with-balance';
				$.get(url, function(wallets){
					url = 'http://'+document.domain+':'+location.port+'/ajax/local-convert/'+selected_wallet.currency.iso_code+'/'+credit.currency.iso_code+'/'+selected_wallet.account_balance;
					$.get(url,function(selected_account_balance_in_currency){
						$('#modal-body').empty();
						$('#modal-body').append('<div class="form-group">');
						$('#modal-body').append('<label>Seleccione la divisa</label>');
						$('#modal-body').append('<select class="form-control" name="wallet_id" id="wallet_id" onchange="payWithCredit('+credit.id+',value)">');	
						url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+credit.currency.id+'/'+selected_account_balance_in_currency+'/1';
						$.get(url,function(selected_account_balance_in_currency_string){
							$('#wallet_id').append('<option value="' + selected_wallet.id + ' selected">' + selected_wallet.currency.name +' ('+credit.currency.iso_code+' '+selected_account_balance_in_currency_string+' disponibles)</option>');			
							$.each(wallets,function(index,wallet){
								if (wallet.currency_id != selected_wallet.currency_id) {
									url = 'http://'+document.domain+':'+location.port+'/ajax/wallets/'+wallet.id;
									$.get(url, function(wallet_getted){
										url = 'http://'+document.domain+':'+location.port+'/ajax/local-convert/'+wallet_getted.currency.iso_code+'/'+credit.currency.iso_code+'/'+wallet_getted.account_balance;
										$.get(url,function(account_balance_in_currency){
											if(account_balance_in_currency >= credit.currency.min_value){
												url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+credit.currency.id+'/'+account_balance_in_currency+'/1';
												$.get(url,function(account_balance_in_currency_string){
													$('#wallet_id').append('<option value="' + wallet_getted.id + '">' + wallet_getted.currency.name +' ('+credit.currency.iso_code+' '+account_balance_in_currency_string+' disponibles)</option>');
												});
											}
										});
									});
								}
							});
						});
						$('#modal-body').append('</select>');
						if(credit.currency.id != selected_wallet.currency_id){
							$('#modal-body').append('<input type="hidden"  id="from_currency_id" value="'+selected_wallet.currency_id+'">');
							$('#modal-body').append('<input type="hidden"  id="to_currency_id" value="'+credit.currency.id+'">');
							$('#modal-body').append('<div id=exchange-rate-areachart style="height: 150px;"></div>');
							drawExchangeRate();
						}
						var max = credit.unpayed_amount;
						if(Number(selected_account_balance_in_currency) < Number(max)){
							max = selected_account_balance_in_currency;
						}
						var min =credit.currency.min_value;
						url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+credit.currency.id+'/'+credit.unpayed_amount+'/1';
						$.get(url,function(unpayed_amount_string){
							url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+credit.currency.id+'/'+payment_amount+'/1';
							$.get(url,function(payment_amount_string){
								url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+credit.currency.id+'/'+max+'/1';
								$.get(url,function(max_string){
									$('#modal-body').append('<h4>');
									if(Number(max) == Number(credit.unpayed_amount)){
										$('#modal-body').append('<button class="btn btn-default" onclick="fillAmountId(\''+unpayed_amount_string+'\')">Crédito ('+unpayed_amount_string+')</button>');
									}
									url = 'http://'+document.domain+':'+location.port+'/ajax/payments/'+credit.first_unpayed_payment.id+'/payment-number';
									$.get(url,function(payment_number){
										if(Number(max) >= Number(payment_amount)){
											$('#modal-body').append('<button class="btn btn-default pull-right" onclick="fillAmountId(\''+payment_amount_string+'\')">Cuota '+payment_number+' ('+payment_amount_string+')</button>');
										}
										$('#modal-body').append('</h4>');
										$('#modal-body').append('<br>');
										$('#modal-body').append('<input type="hidden"  id="max_amount" value="'+max+'">');
										$('#modal-body').append('<input type="hidden"  id="min_amount" value="'+min+'">');
										$('#modal-body').append('<input type="hidden"  id="currency-id" value="'+credit.currency.id+'">');
										$('#modal-body').append('<input type="hidden"  id="from_wallet_id" value="'+selected_wallet.id+'">');
										$('#modal-body').append('<input type="hidden"  id="credit-id" value="'+credit.id+'">');
										$('#modal-body').append('<label>Ingrese el monto (máximo '+max_string+')<br></label>');
										url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+credit.currency.id+'/123.4567890/1';
										$.get(url, function(placeholder){
											$('#modal-body').append('<input type="text" class="form-control" id="amount" placeholder="Ejemplo: '+placeholder.replace(credit.currency.simbol+' ','')+'" autofocus onkeyup="amountVerifierForPayCredit()">');
											$('#modal-body').append('<label id="amount-label"></label>');
											$('#modal-body').append('</div><br>');
											$('#modal-body').append('<div id="simulate-div">');
											$('#modal-body').append('</div>');
											$('#modal-body').append('<br>');
											$('#modal-footer').empty();
											$('#modal-footer').append('<button id="submit-button" type="button" class="btn btn-success" onclick="payCredit()">Realizar pago</button>');
											$('#submit-button').hide();
											$('#modal-footer').append('<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>');										
										});
									});
								});
							});
						});
					});
				});
			});
		});
	});
}
function requestCreditWitCurrency(currency_id){
	console.log('Entre a requestCreditWitCurrency(currency_id)');
	$('#modal-header').empty();
	$('#modal-header').empty();
	$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
	$('#modal-header').append('<h4 class="modal-title" >Solicitar crédito</h4>');
	loading();
	var url = 'http://'+document.domain+':'+location.port+'/ajax/currencies/'+currency_id;
	$.get(url, function(selected_currency){
		$('#modal-header').empty();
		$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
		$('#modal-header').append('<h4 class="modal-title" >Solicitar crédito en '+ selected_currency.name+'</h4>');

		$('#modal-body').empty();
		$('#modal-body').append('<label>Seleccione la divisa</label>');
		url = 'http://'+document.domain+':'+location.port+'/ajax/currencies';
		$.get(url, function(currencies){
			console.log('Entre al select');
			console.log(currencies);
			$('#modal-body').append('<div class="form-group">');
			$('#modal-body').append('<select class="form-control" name="currency_id" id="currency_id" onchange="requestCreditWitCurrency(value)">');
			$('#currency_id').append('<option value="' + selected_currency.id + ' selected">' + selected_currency.name + '</option>');			
			$.each(currencies,function(index,currency){
    	       if (currency.id != selected_currency.id) {
					$('#currency_id').append('<option value="' + currency.id + '">' + currency.name + '</option>');
				}
    	    });
			$('#modal-body').append('</select><br>');
			$('#modal-body').append('</div>');
			console.log('Sali del select')
			url = 'http://'+document.domain+':'+location.port+'/ajax/currencies/'+currency_id+'/credit-margin';
			$.get(url,function(margin){
				url = 'http://'+document.domain+':'+location.port+'/ajax/currencies/'+currency_id+'/credit-min-amount';
				$.get(url,function(min){
					$('#modal-body').append('<input type="hidden"  id="min_amount" value="'+min+'">');
					$('#modal-body').append('<input type="hidden"  id="max_amount" value="'+margin+'">');
					$('#modal-body').append('<input type="hidden"  id="currency-id" value="'+ selected_currency.id+'">');
					$('#modal-body').append('<div class="form-group">');
					url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+selected_currency.id+'/'+margin+'/1';
					$.get(url,function(margin_string){
						url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+selected_currency.id+'/'+min+'/1';
						$.get(url,function(min_string){
							$('#modal-body').append('<label>Ingrese el monto (mínimo: '+min_string+' máximo: '+margin_string+')<br></label>');
							url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+selected_currency.id+'/123.4567890/1';
							$.get(url, function(placeholder){
								$('#modal-body').append('<input type="text" class="form-control" id="amount" placeholder="Ejemplo: '+placeholder.replace(selected_currency.simbol+' ','')+'" autofocus onkeyup="amountVerifierForRequestCredit()">');
								$('#modal-body').append('<label id="amount-label"></label>');
								$('#modal-body').append('</div>');
								url = 'http://'+document.domain+':'+location.port+'/ajax/payment-plans/currency='+selected_currency.id;
								$.get(url,function(payment_plans){
									$('#modal-body').append('<div class="form-group">');
									$('#modal-body').append('<label>Seleccione el plan de pago</label>');
									$('#modal-body').append('<select class="form-control" name="payment_plan_id" id="payment_plan_id" onchange="simulateCredit()">');
									$.each(payment_plans,function(index,payment_plan){
										$('#payment_plan_id').append('<option value="' + payment_plan.id + '">' + payment_plan.description + '</option>');
									});
									$('#modal-body').append('</select><br>');
									$('#modal-body').append('</div>');
									$('#modal-body').append('<div id="simulate-div">');
									$('#modal-body').append('</div>');

									$('#modal-footer').empty();
									$('#modal-footer').append('<button id="submit-button" type="button" class="btn btn-success" onclick="requestCredit()">Solicitar Crédito</button>');
									$('#submit-button').hide();
									$('#modal-footer').append('<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>');						
								});
							});
						});
					});
				});
			});
		});	
	});	
}
function exchangeCurrency(wallet_id){
	console.log('exchangeCurrency(wallet_id)');
	$('#modal-header').empty();
	$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
	$('#modal-header').append('<h4 class="modal-title" >Convertir divisa</h4>');
	loading();
	var url = 'http://'+document.domain+':'+location.port+'/ajax/wallets/'+wallet_id;
	$.get(url, function(selected_wallet){
		url = 'http://'+document.domain+':'+location.port+'/ajax/currencies/'+selected_wallet.currency_id;
		$.get(url, function(selected_currency){
			$('#modal-header').empty();
			$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
			$('#modal-header').append('<h4 class="modal-title" >Convertir divisa desde '+ selected_currency.name+'</h4>');			
			url = 'http://'+document.domain+':'+location.port+'/ajax/wallets-with-balance';
			$.get(url, function(wallets){
				console.log('Entre al select');
				console.log(wallets);
				$('#modal-body').empty();
				$('#modal-body').append('<div id=exchange-rate-areachart style="height: 150px;"></div>');
				$('#modal-body').append('<div class="form-group">');
				$('#modal-body').append('<label>Divisa origen</label>');
				$('#modal-body').append('<select class="form-control" name="from_wallet_id" id="from_wallet_id" onchange="exchangeCurrency(value)">');
				$('#from_wallet_id').append('<option value="' + selected_wallet.id + ' selected">' + selected_currency.name + '</option>');			
				$.each(wallets,function(index,wallet){
					if (wallet.currency_id != selected_currency.id) {
						url = 'http://'+document.domain+':'+location.port+'/ajax/currencies/'+wallet.currency_id;
						$.get(url, function(this_currency){
							$('#from_wallet_id').append('<option value="' + wallet.id + '">' + this_currency.name + '</option>');
						});
					}
	    	    });
				$('#modal-body').append('</select><br>');;

				url = 'http://'+document.domain+':'+location.port+'/ajax/currencies';
				$.get(url, function(currencies){
					$('#modal-body').append('<label>Divisa destino</label>');
					$('#modal-body').append('<select class="form-control" name="to_currency_id" id="to_currency_id" onchange="exchangeRateLoaderInExchange()">');
					$.each(currencies,function(index,currency){
						if(currency.id != selected_currency.id){
							$('#to_currency_id').append('<option value="' + currency.id + '">' + currency.name + '</option>');
						}
					});
					$('#modal-body').append('</select><br>');
					$('#modal-body').append('</div>');
					url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+selected_currency.id+'/'+selected_wallet.account_balance+'/1';
					$.get(url, function(max){
						$('#modal-body').append('<input type="hidden"  id="min_amount" value="'+selected_currency.min_value+'">');
						$('#modal-body').append('<input type="hidden"  id="max_amount" value="'+selected_wallet.account_balance+'">');
						$('#modal-body').append('<input type="hidden"  id="from_currency_id" value="'+ selected_currency.id+'">');
						$('#modal-body').append('<label>Ingrese el monto en '+selected_currency.name+' (máximo '+max+')<br></label>');
						url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+selected_currency.id+'/123.4567890/1';
						$.get(url, function(placeholder){
							$('#modal-body').append('<input type="text" class="form-control" id="amount" placeholder="Ejemplo: '+placeholder.replace(selected_currency.simbol+' ','')+'" autofocus onkeyup="amountVerifierForExchange()">');
							$('#modal-body').append('<label id="amount-label"></label>');
							$('#modal-body').append('</div>');
							exchangeRateLoaderInExchange();
							$('#modal-body').append('<div id="simulate-div">');
							$('#modal-body').append('</div>');
							$('#modal-body').append('</div>');

							$('#modal-footer').empty();
							$('#modal-footer').append('<button id="submit-button" type="button" class="btn btn-success" onclick="exchange()">Realizar conversión</button>');
							$('#submit-button').hide();
							$('#modal-footer').append('<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>');
						});
					});
				});
			});
		});
	});
}
function askGrantCredit(){
	console.log('Entre a askAbortCredit()');
	$('#modal-header').empty();
	$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
	$('#modal-header').append('<h4 class="modal-title">Transferir Crédito</h4>');

	$('#modal-body').empty();
	$('#modal-body').append('<i class="fa fa-question fa-5x justify-content-center" aria-hidden="true"></i>');
	$('#modal-body').append('<h5>¿Quieres el crédito a tu wallet?</h5>');
	$('#modal-body').append('<p>Se generarán las cuotas y tendrás el dinero listo para usarlo</p>');

	$('#modal-footer').empty();
	$('#modal-footer').append('<button id="submit-button" type="button" class="btn btn-success" onclick="grantCredit()">Transferir crédito</button>');
	$('#modal-footer').append('<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>');
	
}
function askAbortCredit(){
	console.log('Entre a askAbortCredit()');
	$('#modal-header').empty();
	$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
	$('#modal-header').append('<h4 class="modal-title">Cancelar Crédito</h4>');

	$('#modal-body').empty();
	$('#modal-body').append('<i class="fa fa-question fa-5x justify-content-center" aria-hidden="true"></i>');
	$('#modal-body').append('<h5>¿Quieres cancelar el crédito? Esta opción no se puede deshacer</h5>');
	$('#modal-body').append('<p>Pero no te preocupes, si querés, podés solicitar otro crédito del mismo monto en el futuro</p>');

	$('#modal-footer').empty();
	$('#modal-footer').append('<button id="submit-button" type="button" class="btn btn-danger" onclick="abortCredit()">Cancelar crédito</button>');
	$('#modal-footer').append('<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>');
	
}
function askRejectCredit(){
	console.log('Entre a askRejectCredit()');
	$('#modal-header').empty();
	$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
	$('#modal-header').append('<h4 class="modal-title">Rechazar Crédito</h4>');

	$('#modal-body').empty();
	$('#modal-body').append('<i class="fa fa-question fa-5x justify-content-center" aria-hidden="true"></i>');
	$('#modal-body').append('<h5>¿Quieres rechazar el crédito? Esta opción no se puede deshacer</h5>');

	$('#modal-footer').empty();
	$('#modal-footer').append('<button id="submit-button" type="button" class="btn btn-danger" onclick="rejectCredit()">Rechazar crédito</button>');
	$('#modal-footer').append('<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>');
	
}
function askApprovateCredit(){
	console.log('Entre a askApprovateCredit()');
	$('#modal-header').empty();
	$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
	$('#modal-header').append('<h4 class="modal-title">Aprobar Crédito</h4>');

	$('#modal-body').empty();
	$('#modal-body').append('<i class="fa fa-question fa-5x justify-content-center" aria-hidden="true"></i>');
	$('#modal-body').append('<h5>¿Quieres aprobar el crédito? Esta opción no se puede deshacer</h5>');

	$('#modal-footer').empty();
	$('#modal-footer').append('<button id="submit-button" type="button" class="btn btn-success" onclick="approvateCredit()">Aprobar crédito</button>');
	$('#modal-footer').append('<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>');
	
}
function askJudicialCitationForCredit(){
	console.log('Entre a askJudicialCitationForCredit()');
	$('#modal-header').empty();
	$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
	$('#modal-header').append('<h4 class="modal-title">Enviar citación judicial</h4>');

	$('#modal-body').empty();
	$('#modal-body').append('<i class="fa fa-question fa-5x justify-content-center" aria-hidden="true"></i>');
	$('#modal-body').append('<h5>¿Quieres enviar la citacón judicial para el crédito? Esta opción no se puede deshacer</h5>');

	$('#modal-footer').empty();
	$('#modal-footer').append('<button id="submit-button" type="button" class="btn btn-danger" onclick="juditialCitationForCredit()">Enviar citación</button>');
	$('#modal-footer').append('<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>');
	
}
function askLawsuitCredit(){
	console.log('Entre a askLawsuitCredit()');
	$('#modal-header').empty();
	$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
	$('#modal-header').append('<h4 class="modal-title">Comenzar litigio</h4>');

	$('#modal-body').empty();
	$('#modal-body').append('<i class="fa fa-question fa-5x justify-content-center" aria-hidden="true"></i>');
	$('#modal-body').append('<h5>¿Quieres comenzar el litigio para el crédito? Esta opción no se puede deshacer</h5>');

	$('#modal-footer').empty();
	$('#modal-footer').append('<button id="submit-button" type="button" class="btn btn-danger" onclick="lawsuitCredit()">Comenzar litigio</button>');
	$('#modal-footer').append('<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>');
	
}
function askBadCredit(){
	console.log('Entre a askBadCredit()');
	$('#modal-header').empty();
	$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
	$('#modal-header').append('<h4 class="modal-title">Declarar incobrable</h4>');

	$('#modal-body').empty();
	$('#modal-body').append('<i class="fa fa-question fa-5x justify-content-center" aria-hidden="true"></i>');
	$('#modal-body').append('<h5>¿Quieres marcar el crédito como incobrable? Esta opción no se puede deshacer</h5>');

	$('#modal-footer').empty();
	$('#modal-footer').append('<button id="submit-button" type="button" class="btn btn-danger" onclick="badCredit()">Declarar incobrable</button>');
	$('#modal-footer').append('<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>');
	
}


function badCredit(){
	console.log('Entre a badCredit()');
	var credit_id = document.getElementById('credit_id').value;
	loading();
	$.ajax({
		type: 'post',
		data: {
			_token: document.getElementsByName('_token')[0].value,
			credit_id: credit_id,
		},
		url: 'http://'+document.domain+':'+location.port+'/ajax/bad-credit',
		success: function(json){
			console.log('Entre a ajaxSuccess()');
			console.log(json);
			$('#modal-header').empty();
			$('#modal-header').append('<a onclick="redirect(\'/credits/lawsuit/currency='+json.currency_id+'\')" type="button" class="close"><span aria-hidden="true">&times;</span></a>');
			$('#modal-header').append('<h4 class="modal-title" >Crédito aprobado</h4>');
		
			$('#modal-body').empty();
			$('#modal-body').append('<i class="fa fa-check fa-5x" aria-hidden="true"></i>');
			$('#modal-body').append('<h2>Declaraste el crédito como incobrable, muchas gracias</h2>');
			$('#modal-body').append('<h3>Detalles</h3>');
			$('#modal-body').append('<table class="table table-hover table-striped">\n<thead>\n<th>Concepto</th>\n<th>Valor</th>\n</thead>\n<tbody>\n<tr>\n<td>ID de operacion</td>\n<td>'+json.credit_phase_id+'</td>\n</tr>\n<tr>\n<td>ID de credito</td>\n<td>'+json.credit_id+'</td>\n</tr>\n<tr>\n<td>Monto solicitado</td>\n<td>'+json.credit_amount_string+'</td>\n</tr>\n</tbody>\n</table>');
			
			$('#modal-footer').empty();
			$('#modal-footer').append('<button class="btn btn-primary" onclick="printDiv(\'modal\')">Generar PDF</button>');
			$('#modal-footer').append('<a onclick="redirect(\'/credits/lawsuit/currency='+json.currency_id+'\')" class="btn btn-primary">Cerrar</a>');
		},
		error: function(json){
			console.log('Entre a ajaxError()');
			console.log(json);
			$('#modal-header').empty();
			$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
			$('#modal-header').append('<h4 class="modal-title" >Error de conexión</h4>');
		
			$('#modal-body').empty();
			$('#modal-body').append('<i class="fa fa-times fa-5x" aria-hidden="true"></i>');
			$('#modal-body').append('<h2>No pudimos establecer conexión con nuestros sistemas</h2>');
			$('#modal-body').append('<h4>Por favor, recargá la página</h4>');
		
			$('#modal-footer').empty();
			$('#modal-footer').append('<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>');
		},
	});
}
function lawsuitCredit(){
	console.log('Entre a lawsuitCredit()');
	var credit_id = document.getElementById('credit_id').value;
	loading();
	$.ajax({
		type: 'post',
		data: {
			_token: document.getElementsByName('_token')[0].value,
			credit_id: credit_id,
		},
		url: 'http://'+document.domain+':'+location.port+'/ajax/lawsuit-credit',
		success: function(json){
			console.log('Entre a ajaxSuccess()');
			console.log(json);
			$('#modal-header').empty();
			$('#modal-header').append('<a onclick="redirect(\'/credits/judicial-citation/currency='+json.currency_id+'\')" type="button" class="close"><span aria-hidden="true">&times;</span></a>');
			$('#modal-header').append('<h4 class="modal-title" >Crédito aprobado</h4>');
		
			$('#modal-body').empty();
			$('#modal-body').append('<i class="fa fa-check fa-5x" aria-hidden="true"></i>');
			$('#modal-body').append('<h2>Comenzaste el litigio para el crédito, muchas gracias</h2>');
			$('#modal-body').append('<h3>Detalles</h3>');
			$('#modal-body').append('<table class="table table-hover table-striped">\n<thead>\n<th>Concepto</th>\n<th>Valor</th>\n</thead>\n<tbody>\n<tr>\n<td>ID de operacion</td>\n<td>'+json.credit_phase_id+'</td>\n</tr>\n<tr>\n<td>ID de credito</td>\n<td>'+json.credit_id+'</td>\n</tr>\n<tr>\n<td>Monto solicitado</td>\n<td>'+json.credit_amount_string+'</td>\n</tr>\n</tbody>\n</table>');
			
			$('#modal-footer').empty();
			$('#modal-footer').append('<button class="btn btn-primary" onclick="printDiv(\'modal\')">Generar PDF</button>');
			$('#modal-footer').append('<a onclick="redirect(\'/credits/judicial-citation/currency='+json.currency_id+'\')" class="btn btn-primary">Cerrar</a>');
		},
		error: function(json){
			console.log('Entre a ajaxError()');
			console.log(json);
			$('#modal-header').empty();
			$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
			$('#modal-header').append('<h4 class="modal-title" >Error de conexión</h4>');
		
			$('#modal-body').empty();
			$('#modal-body').append('<i class="fa fa-times fa-5x" aria-hidden="true"></i>');
			$('#modal-body').append('<h2>No pudimos establecer conexión con nuestros sistemas</h2>');
			$('#modal-body').append('<h4>Por favor, recargá la página</h4>');
		
			$('#modal-footer').empty();
			$('#modal-footer').append('<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>');
		},
	});
}
function juditialCitationForCredit(){
	console.log('Entre a juditialCitationForCredit()');
	var credit_id = document.getElementById('credit_id').value;
	loading();
	$.ajax({
		type: 'post',
		data: {
			_token: document.getElementsByName('_token')[0].value,
			credit_id: credit_id,
		},
		url: 'http://'+document.domain+':'+location.port+'/ajax/citation-credit',
		success: function(json){
			console.log('Entre a ajaxSuccess()');
			console.log(json);
			$('#modal-header').empty();
			$('#modal-header').append('<a onclick="redirect(\'/credits/defaulter/currency='+json.currency_id+'\')" type="button" class="close"><span aria-hidden="true">&times;</span></a>');
			$('#modal-header').append('<h4 class="modal-title" >Crédito aprobado</h4>');
		
			$('#modal-body').empty();
			$('#modal-body').append('<i class="fa fa-check fa-5x" aria-hidden="true"></i>');
			$('#modal-body').append('<h2>Se envió la citación para el crédito, muchas gracias</h2>');
			$('#modal-body').append('<h3>Detalles</h3>');
			$('#modal-body').append('<table class="table table-hover table-striped">\n<thead>\n<th>Concepto</th>\n<th>Valor</th>\n</thead>\n<tbody>\n<tr>\n<td>ID de operacion</td>\n<td>'+json.credit_phase_id+'</td>\n</tr>\n<tr>\n<td>ID de credito</td>\n<td>'+json.credit_id+'</td>\n</tr>\n<tr>\n<td>Monto solicitado</td>\n<td>'+json.credit_amount_string+'</td>\n</tr>\n</tbody>\n</table>');
			
			$('#modal-footer').empty();
			$('#modal-footer').append('<button class="btn btn-primary" onclick="printDiv(\'modal\')">Generar PDF</button>');
			$('#modal-footer').append('<a onclick="redirect(\'/credits/defaulter/currency='+json.currency_id+'\')" class="btn btn-primary">Cerrar</a>');
		},
		error: function(json){
			console.log('Entre a ajaxError()');
			console.log(json);
			$('#modal-header').empty();
			$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
			$('#modal-header').append('<h4 class="modal-title" >Error de conexión</h4>');
		
			$('#modal-body').empty();
			$('#modal-body').append('<i class="fa fa-times fa-5x" aria-hidden="true"></i>');
			$('#modal-body').append('<h2>No pudimos establecer conexión con nuestros sistemas</h2>');
			$('#modal-body').append('<h4>Por favor, recargá la página</h4>');
		
			$('#modal-footer').empty();
			$('#modal-footer').append('<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>');
		},
	});
}

function approvateCredit(){
	console.log('Entre a approvateCredit()');
	var credit_id = document.getElementById('credit_id').value;
	loading();
	$.ajax({
		type: 'post',
		data: {
			_token: document.getElementsByName('_token')[0].value,
			credit_id: credit_id,
		},
		url: 'http://'+document.domain+':'+location.port+'/ajax/approvate-credit',
		success: function(json){
			console.log('Entre a ajaxSuccess()');
			console.log(json);
			$('#modal-header').empty();
			$('#modal-header').append('<a onclick="redirect(\'/credits/to-approve/currency='+json.currency_id+'\')" type="button" class="close"><span aria-hidden="true">&times;</span></a>');
			$('#modal-header').append('<h4 class="modal-title" >Crédito aprobado</h4>');
		
			$('#modal-body').empty();
			$('#modal-body').append('<i class="fa fa-check fa-5x" aria-hidden="true"></i>');
			$('#modal-body').append('<h2>Aprobaste el credito, muchas gracias</h2>');
			$('#modal-body').append('<h3>Detalles</h3>');
			$('#modal-body').append('<table class="table table-hover table-striped">\n<thead>\n<th>Concepto</th>\n<th>Valor</th>\n</thead>\n<tbody>\n<tr>\n<td>ID de operacion</td>\n<td>'+json.credit_phase_id+'</td>\n</tr>\n<tr>\n<td>ID de credito</td>\n<td>'+json.credit_id+'</td>\n</tr>\n<tr>\n<td>Monto solicitado</td>\n<td>'+json.credit_amount_string+'</td>\n</tr>\n</tbody>\n</table>');
			
			$('#modal-footer').empty();
			$('#modal-footer').append('<button class="btn btn-primary" onclick="printDiv(\'modal\')">Generar PDF</button>');
			$('#modal-footer').append('<a onclick="redirect(\'/credits/to-approve/currency='+json.currency_id+'\')" class="btn btn-primary">Cerrar</a>');
		},
		error: function(json){
			console.log('Entre a ajaxError()');
			console.log(json);
			$('#modal-header').empty();
			$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
			$('#modal-header').append('<h4 class="modal-title" >Error de conexión</h4>');
		
			$('#modal-body').empty();
			$('#modal-body').append('<i class="fa fa-times fa-5x" aria-hidden="true"></i>');
			$('#modal-body').append('<h2>No pudimos establecer conexión con nuestros sistemas</h2>');
			$('#modal-body').append('<h4>Por favor, recargá la página</h4>');
		
			$('#modal-footer').empty();
			$('#modal-footer').append('<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>');
		},
	});
}
function rejectCredit(){
	console.log('Entre a rejectCredit()');
	var credit_id = document.getElementById('credit_id').value;
	loading();
	$.ajax({
		type: 'post',
		data: {
			_token: document.getElementsByName('_token')[0].value,
			credit_id: credit_id,
		},
		url: 'http://'+document.domain+':'+location.port+'/ajax/reject-credit',
		success: function(json){
			console.log('Entre a ajaxSuccess()');
			console.log(json);
			$('#modal-header').empty();
			$('#modal-header').append('<a onclick="redirect(\'/credits/to-approve/currency='+json.currency_id+'\')" type="button" class="close"><span aria-hidden="true">&times;</span></a>');
			$('#modal-header').append('<h4 class="modal-title" >Crédito rechazado</h4>');
		
			$('#modal-body').empty();
			$('#modal-body').append('<i class="fa fa-check fa-5x" aria-hidden="true"></i>');
			$('#modal-body').append('<h2>Rechazaste el credito, muchas gracias</h2>');
			$('#modal-body').append('<h3>Detalles</h3>');
			$('#modal-body').append('<table class="table table-hover table-striped">\n<thead>\n<th>Concepto</th>\n<th>Valor</th>\n</thead>\n<tbody>\n<tr>\n<td>ID de operacion</td>\n<td>'+json.credit_phase_id+'</td>\n</tr>\n<tr>\n<td>ID de credito</td>\n<td>'+json.credit_id+'</td>\n</tr>\n<tr>\n<td>Monto solicitado</td>\n<td>'+json.credit_amount_string+'</td>\n</tr>\n</tbody>\n</table>');
			
			$('#modal-footer').empty();
			$('#modal-footer').append('<button class="btn btn-primary" onclick="printDiv(\'modal\')">Generar PDF</button>');
			$('#modal-footer').append('<a  onclick="redirect(\'/credits/to-approve/currency='+json.currency_id+'\')" class="btn btn-primary">Cerrar</a>');
		},
		error: function(json){
			console.log('Entre a ajaxError()');
			console.log(json);
			$('#modal-header').empty();
			$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
			$('#modal-header').append('<h4 class="modal-title" >Error de conexión</h4>');
		
			$('#modal-body').empty();
			$('#modal-body').append('<i class="fa fa-times fa-5x" aria-hidden="true"></i>');
			$('#modal-body').append('<h2>No pudimos establecer conexión con nuestros sistemas</h2>');
			$('#modal-body').append('<h4>Por favor, recargá la página</h4>');
		
			$('#modal-footer').empty();
			$('#modal-footer').append('<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>');
		},
	});
}
//Ojo porque no funciona la impresion
function grantCredit(){
	console.log('Entre a grantCredit()');
	var wallet_id = document.getElementById('wallet_id').value;
	var credit_id = document.getElementById('credit_id').value;
	loading();
	$.ajax({
		type: 'post',
		data: {
			_token: document.getElementsByName('_token')[0].value,
			credit_id: credit_id,
		},
		url: 'http://'+document.domain+':'+location.port+'/ajax/grant-credit',
		success: function(json){
			console.log('Entre a ajaxSuccess()');
			console.log(json);
			$('#modal-header').empty();
			$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="location.reload()"><span aria-hidden="true">&times;</span></button>');
			$('#modal-header').append('<h4 class="modal-title" >Crédito transferido</h4>');
		
			$('#modal-body').empty();
			$('#modal-body').append('<i class="fa fa-check fa-5x" aria-hidden="true"></i>');
			$('#modal-body').append('<h2>Ya dispones del dinero en tu wallet, muchas gracias</h2>');

			$('#modal-body').append('<h3>Detalles</h3>');
			$('#modal-body').append('<table class="table table-hover table-striped">\n<thead>\n<th>Concepto</th>\n<th>Valor</th>\n</thead>\n<tbody>\n<tr>\n<td>ID de transaccion</td>\n<td>'+json.credit_phase_id+'</td>\n</tr>\n<tr>\n<td>ID de credito</td>\n<td>'+json.credit_id+'</td>\n</tr>\n<tr>\n<td>Monto de credito</td>\n<td>'+json.credit_amount_string+'</td>\n</tr>\n<tr>\n<td>Cantidad de cuotas</td>\n<td>'+json.payments_numer+'</td>\n</tr>\n<tr>\n<td>Monto de cada cuota</td>\n<td>'+json.payments_amount_string+'</td>\n</tr>\n<tr>\n<td>Vencimiento primer cuota</td>\n<td>'+json.due_date_for_first_payment+'</td>\n</tr>\n</tbody>\n</table>');
			
			$('#modal-footer').empty();
			$('#modal-footer').append('<button class="btn btn-primary" onclick="printDiv(\'modal\')">Generar PDF</button>');
			$('#modal-footer').append('<a onclick="redirect(\'/wallets/'+wallet_id+'\')" class="btn btn-success">Ir al Wallet</a>');
			$('#modal-footer').append('<button type="button" class="btn btn-primary" data-dismiss="modal" onclick="location.reload()">Cerrar</button>');
		},
		error: function(json){
			console.log('Entre a ajaxError()');
			console.log(json);
			$('#modal-header').empty();
			$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
			$('#modal-header').append('<h4 class="modal-title" >Error de conexión</h4>');
		
			$('#modal-body').empty();
			$('#modal-body').append('<i class="fa fa-times fa-5x" aria-hidden="true"></i>');
			$('#modal-body').append('<h2>No pudimos establecer conexión con nuestros sistemas</h2>');
			$('#modal-body').append('<h4>Por favor, recargá la página</h4>');
		
			$('#modal-footer').empty();
			$('#modal-footer').append('<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>');
		},
	});
}
//Ojo porque no funciona la impresion
function abortCredit(){
	console.log('Entre a abortCredit()');
	var credit_id = document.getElementById('credit_id').value;
	loading();
	$.ajax({
		type: 'post',
		data: {
			_token: document.getElementsByName('_token')[0].value,
			credit_id: credit_id,
		},
		url: 'http://'+document.domain+':'+location.port+'/ajax/abort-credit',
		success: function(json){
			console.log('Entre a ajaxSuccess()');
			console.log(json);
			$('#modal-header').empty();
			$('#modal-header').append('<button type="button" class="close" aria-label="Close" onclick="location.reload()"><span aria-hidden="true">&times;</span></button>');
			$('#modal-header').append('<h4 class="modal-title" >Crédito cancelado</h4>');
		
			$('#modal-body').empty();
			$('#modal-body').append('<i class="fa fa-check fa-5x" aria-hidden="true"></i>');
			$('#modal-body').append('<h2>Cancelaste el credito, muchas gracias</h2>');
			
			$('#modal-body').append('<h3>Detalles</h3>');
			$('#modal-body').append('<table class="table table-hover table-striped">\n<thead>\n<th>Concepto</th>\n<th>Valor</th>\n</thead>\n<tbody>\n<tr>\n<td>ID de transaccion</td>\n<td>'+json.credit_phase_id+'</td>\n</tr>\n<tr>\n<td>ID de credito</td>\n<td>'+json.credit_id+'</td>\n</tr>\n<tr>\n<td>Monto de credito</td>\n<td>'+json.credit_amount_string+'</td>\n</tr>\n</tbody>\n</table>');
		
			$('#modal-footer').empty();
			$('#modal-footer').append('<button class="btn btn-primary" onclick="printDiv(\'modal\')">Generar PDF</button>');
			$('#modal-footer').append('<button type="button" class="btn btn-primary" onclick="location.reload()">Cerrar</button>');
		},
		error: function(json){
			console.log('Entre a ajaxError()');
			console.log(json);
			$('#modal-header').empty();
			$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
			$('#modal-header').append('<h4 class="modal-title" >Error de conexión</h4>');
		
			$('#modal-body').empty();
			$('#modal-body').append('<i class="fa fa-times fa-5x" aria-hidden="true"></i>');
			$('#modal-body').append('<h2>No pudimos establecer conexión con nuestros sistemas</h2>');
			$('#modal-body').append('<h4>Por favor, recargá la página</h4>');
		
			$('#modal-footer').empty();
			$('#modal-footer').append('<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>');
		},
	});
}
function requestCredit(){
	console.log('Entre a requestCredit()');
	var currency_id = document.getElementById('currency-id').value;
	var amount_string = document.getElementById('amount').value;
	var payment_plan_id = document.getElementById('payment_plan_id').value;
	loading();
	amount_string = amount_string.replace(' ','');
	var url = 'http://'+document.domain+':'+location.port+'/ajax/string-to-number/'+currency_id+'/'+ amount_string;
	$.get(url,function(amount){
		$.ajax({
			type: 'post',
			data: {
				_token: document.getElementsByName('_token')[0].value,
				amount: amount,
				payment_plan_id: payment_plan_id,
			},
			url: 'http://'+document.domain+':'+location.port+'/ajax/request-credit',
			success: function(json){
				console.log('Entre a ajaxSuccess()');
				console.log(json);
				$('#modal-header').empty();
				$('#modal-header').append('<a onclick="redirect(\'/credits/currency='+currency_id+'\')" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></a>');
				$('#modal-header').append('<h4 class="modal-title" >Crédito solicitado</h4>');
			
				$('#modal-body').empty();
				$('#modal-body').append('<i class="fa fa-check fa-5x" aria-hidden="true"></i>');
				$('#modal-body').append('<h2>Solicitaste el credito, muchas gracias</h2>');

				$('#modal-body').append('<h3>Detalles</h3>');
				$('#modal-body').append('<table class="table table-hover table-striped">\n<thead>\n<th>Concepto</th>\n<th>Valor</th>\n</thead>\n<tbody>\n<tr>\n<td>ID de transaccion</td>\n<td>'+json.credit_phase_id+'</td>\n</tr>\n<tr>\n<td>ID de credito</td>\n<td>'+json.credit_id+'</td>\n</tr>\n<tr>\n<td>Monto solicitado</td>\n<td>'+json.credit_amount_string+'</td>\n</tr>\n</tbody>\n</table>');
	
				$('#modal-footer').empty();
				$('#modal-footer').append('<button class="btn btn-primary" onclick="printDiv(\'modal\')">Generar PDF</button>');
				$('#modal-footer').append('<a onclick="redirect(\'/credits/currency='+currency_id+'\')" class="btn btn-primary">Cerrar</a>');
			},
			error: function(json){
				console.log('Entre a ajaxError()');
				console.log(json);
				$('#modal-header').empty();
				$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
				$('#modal-header').append('<h4 class="modal-title" >Error de conexión</h4>');
			
				$('#modal-body').empty();
				$('#modal-body').append('<i class="fa fa-times fa-5x" aria-hidden="true"></i>');
				$('#modal-body').append('<h2>No pudimos establecer conexión con nuestros sistemas</h2>');
				$('#modal-body').append('<h4>Por favor, recargá la página</h4>');
			
				$('#modal-footer').empty();
				$('#modal-footer').append('<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>');
			},
		});
	});
}
//Ojo porque no funciona la impresion
function payCredit(){
	console.log('Entre a payCredit()');
	var currency_id       = document.getElementById('currency-id').value;
	var pay_amount_string = document.getElementById('amount').value;
	var from_amount       = document.getElementById('from_amount').value;
	var exchange_rate_id  = document.getElementById('exchange_rate_id').value;
	var wallet_id         = document.getElementById('wallet_id').value;
	var credit_id         = document.getElementById('credit-id').value;
	loading();
	amount_string = pay_amount_string.replace(' ','');
	var url = 'http://'+document.domain+':'+location.port+'/ajax/string-to-number/'+currency_id+'/'+ pay_amount_string;
	$.get(url,function(pay_amount){
		$.ajax({
			type: 'post',
			data: {
				_token: document.getElementsByName('_token')[0].value,
				credit_id: credit_id,	
				pay_amount: pay_amount,
				from_amount:from_amount,
				exchange_rate_id:exchange_rate_id,
				wallet_id:wallet_id,
			},
			url: 'http://'+document.domain+':'+location.port+'/ajax/pay-credit',
			success: function(json){
				console.log('Entre a ajaxSuccess()');
				console.log(json);
				$('#modal-header').empty();
				$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="location.reload()"><span aria-hidden="true">&times;</span></button>');
				$('#modal-header').append('<h4 class="modal-title" >Pago realizado</h4>');
			
				$('#modal-body').empty();
				$('#modal-body').append('<i class="fa fa-check fa-5x" aria-hidden="true"></i>');
				$('#modal-body').append('<h2>Realizaste el pago con éxito, muchas gracias</h2>');
				$('#modal-body').append('<h3>Detalles</h3>');
				$('#modal-body').append('<table class="table table-hover table-striped">\n<thead>\n<th>Concepto</th>\n<th>Valor</th>\n</thead>\n<tbody>\n<tr>\n<td>ID de credito</td>\n<td>'+json.credit_id+'</td>\n</tr>\n<tr>\n<td>Monto solicitado</td>\n<td>'+json.credit_amount_string+'</td>\n</tr>\n<tr>\n<td>Monto pagado</td>\n<td>'+json.payed_amount_string+'</td>\n</tr>\n</tbody>\n</table>');
			
				$('#modal-footer').empty();
				$('#modal-footer').append('<button class="btn btn-primary" onclick="printDiv(\'modal\')">Generar PDF</button>');
				$('#modal-footer').append('<button type="button" class="btn btn-primary" data-dismiss="modal" onclick="location.reload()">Cerrar</button>');
			},
			error: function(json){
				console.log('Entre a ajaxError()');
				console.log(json);
				$('#modal-header').empty();
				$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
				$('#modal-header').append('<h4 class="modal-title" >Error de conexión</h4>');
			
				$('#modal-body').empty();
				$('#modal-body').append('<i class="fa fa-times fa-5x" aria-hidden="true"></i>');
				$('#modal-body').append('<h2>No pudimos establecer conexión con nuestros sistemas</h2>');
				$('#modal-body').append('<h4>Por favor, recargá la página</h4>');
			
				$('#modal-footer').empty();
				$('#modal-footer').append('<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>');
			},
		});
	});
}
//Ojo porque no funciona la impresion
function cancelFinancing(){
	console.log('Entre a cancelFinancing()');
	var currency_id = document.getElementById('currency-id').value;
	var amount_string = document.getElementById('amount').value;
	var financing_id = document.getElementById('financing-id').value;
	loading();
	amount_string = amount_string.replace(' ','');
	var url = 'http://'+document.domain+':'+location.port+'/ajax/string-to-number/'+currency_id+'/'+ amount_string;
	$.get(url,function(amount){
		$.ajax({
			type: 'post',
			data: {
				_token: document.getElementsByName('_token')[0].value,
				financing_id: financing_id,	
				amount: amount,
			},
			url: 'http://'+document.domain+':'+location.port+'/ajax/cancel-financing',
			success: function(json){
				console.log('Entre a ajaxSuccess()');
				console.log(json);
				if(json != ''){
					$('#modal-header').empty();
					$('#modal-header').append('<button type="button" class="close" aria-label="Close" data-dismiss="modal" onclick="location.reload()" ><span aria-hidden="true">&times;</span></button>');
					$('#modal-header').append('<h4 class="modal-title" >Financiamiento cancelado</h4>');
				
					$('#modal-body').empty();
					$('#modal-body').append('<i class="fa fa-check fa-5x" aria-hidden="true"></i>');
					$('#modal-body').append('<h2>Quitaste dinero del financiamiento con éxito</h2>');
					$('#modal-body').append('<h3>Detalles</h3>');
					$('#modal-body').append('<table class="table table-hover table-striped">\n<thead>\n<th>Concepto</th>\n<th>Valor</th>\n</thead>\n<tbody>\n<tr>\n<td>ID de financiamiento</td>\n<td>'+json.financing_id+'</td>\n</tr>\n<tr>\n<td>ID de credito</td>\n<td>'+json.credit_id+'</td>\n</tr>\n<tr>\n<td>Monto cancelado</td>\n<td>'+json.financing_cancel_amount_string+'</td>\n</tr>\n<tr>\n<td>Monto financiado actualmente</td>\n<td>'+json.financing_amount_string+'</td>\n</tr>\n<tr>\n<td>'+json.financing_profits_title+'</td>\n<td>'+json.financing_profits_string+'</td>\n</tr>\n</tbody>\n</table>');

					$('#modal-footer').empty();
					$('#modal-footer').append('<button class="btn btn-primary" onclick="printDiv(\'modal\')">Generar PDF</button>');
					$('#modal-footer').append('<button type="button" class="btn btn-primary" data-dismiss="modal" onclick="location.reload()">Cerrar</button>');
				}else{
					$('#modal-header').empty();
					$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
					$('#modal-header').append('<h4 class="modal-title" >Detectamos un error</h4>');
				
					$('#modal-body').empty();
					$('#modal-body').append('<i class="fa fa-times fa-5x" aria-hidden="true"></i>');
					$('#modal-body').append('<h2>Hubo un error al procesar tu solicitud</h2>');
					$('#modal-body').append('<h4>Por favor, intentá nuevamente mas tarde</h4>');
					$('#modal-body').append('<h5>Si seguís teniendo este error, comunicate con nosotros</h5>');
				
					$('#modal-footer').empty();
					$('#modal-footer').append('<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>');
				
				}
			},
			error: function(json){
				console.log('Entre a ajaxError()');
				console.log(json);
				$('#modal-header').empty();
				$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
				$('#modal-header').append('<h4 class="modal-title" >Error de conexión</h4>');
			
				$('#modal-body').empty();
				$('#modal-body').append('<i class="fa fa-times fa-5x" aria-hidden="true"></i>');
				$('#modal-body').append('<h2>No pudimos establecer conexión con nuestros sistemas</h2>');
				$('#modal-body').append('<h4>Por favor, recargá la página</h4>');
			
				$('#modal-footer').empty();
				$('#modal-footer').append('<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>');
			},
		});
	});
}
function inputMoney(){
	console.log('Entre a inputMoney()');
	var currency_id = document.getElementById('currency-id').value;
	var amount_string = document.getElementById('amount').value;
	loading();
	amount_string = amount_string.replace(' ','');
	var url = 'http://'+document.domain+':'+location.port+'/ajax/string-to-number/'+currency_id+'/'+ amount_string;
	$.get(url,function(amount){
		$.ajax({
			type: 'post',
			data: {
				_token: document.getElementsByName('_token')[0].value,
				amount: amount,
				currency_id: currency_id,	
			},
			url: 'http://'+document.domain+':'+location.port+'/ajax/input-money',
			success: function(json){
				console.log('Entre a ajaxSuccess()');
				console.log(json);
				if(json != ''){
					$('#modal-header').empty();
					$('#modal-header').append('<a onclick="redirect(\'/wallets/'+json.wallet_id+'\')" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></a>');
					$('#modal-header').append('<h4 class="modal-title" >Transacción realizada</h4>');
				
					$('#modal-body').empty();
					$('#modal-body').append('<i class="fa fa-check fa-5x" aria-hidden="true"></i>');
					$('#modal-body').append('<h2>Ingresaste dinero con éxito</h2>');
					$('#modal-body').append('<h3>Detalles</h3>');
					$('#modal-body').append('<table class="table table-hover table-striped">\n<thead>\n<th>Concepto</th>\n<th>Valor</th>\n</thead>\n<tbody>\n<tr>\n<td>ID de transaccion</td>\n<td>'+json.wallet_transaction_id+'</td>\n</tr>\n<tr>\n<td>Divisa</td>\n<td>'+json.currency_name+'</td>\n</tr>\n<tr>\n<td>Monto que ingresaste</td>\n<td>'+json.input_amount_string+'</td>\n</tr>\n</tbody>\n</table>');
				
					$('#modal-footer').empty();
					$('#modal-footer').append('<button class="btn btn-primary" onclick="printDiv(\'modal\')">Generar PDF</button>');
					$('#modal-footer').append('<a onclick="redirect(\'/wallets/'+json.wallet_id+'\')" class="btn btn-primary">Cerrar</a>');
				}else{
					console.log('Entre a ajaxSuccess()');
					$('#modal-header').empty();
					$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
					$('#modal-header').append('<h4 class="modal-title" >Detectamos un error</h4>');
				
					$('#modal-body').empty();
					$('#modal-body').append('<i class="fa fa-times fa-5x" aria-hidden="true"></i>');
					$('#modal-body').append('<h2>Hubo un error al procesar tu solicitud</h2>');
					$('#modal-body').append('<h4>Por favor, intentá nuevamente mas tarde</h4>');
					$('#modal-body').append('<h5>Si seguís teniendo este error, comunicate con nosotros</h5>');
				
					$('#modal-footer').empty();
					$('#modal-footer').append('<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>');
				
				}
			},
			error: function(){
				$('#modal-header').empty();
				$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
				$('#modal-header').append('<h4 class="modal-title" >Error de conexión</h4>');
			
				$('#modal-body').empty();
				$('#modal-body').append('<i class="fa fa-times fa-5x" aria-hidden="true"></i>');
				$('#modal-body').append('<h2>No pudimos establecer conexión con nuestros sistemas</h2>');
				$('#modal-body').append('<h4>Por favor, recargá la página</h4>');
			
				$('#modal-footer').empty();
				$('#modal-footer').append('<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>');
			},
		});
	});
	
}
function outputMoney(){
	console.log('Entre a outputMoney()');
	var currency_id = document.getElementById('currency-id').value;
	var amount_string = document.getElementById('amount').value;
	loading();
	amount_string = amount_string.replace(' ','');
	var url = 'http://'+document.domain+':'+location.port+'/ajax/string-to-number/'+currency_id+'/'+ amount_string;
	$.get(url,function(amount){
		$.ajax({
			type: 'post',
			data: {
				_token: document.getElementsByName('_token')[0].value,
				amount: amount,
				currency_id: currency_id,	
			},
			url: 'http://'+document.domain+':'+location.port+'/ajax/output-money',
			success: function(json){
				console.log('Entre a ajaxSuccess()');
				console.log(json);
				if(json != ''){
					$('#modal-header').empty();
					$('#modal-header').append('<a onclick="redirect(\'/wallets/'+json.wallet_id+'\')" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></a>');
					$('#modal-header').append('<h4 class="modal-title" >Transacción realizada</h4>');
				
					$('#modal-body').empty();
					$('#modal-body').append('<i class="fa fa-check fa-5x" aria-hidden="true"></i>');
					$('#modal-body').append('<h2>Retiraste dinero con éxito</h2>');
					$('#modal-body').append('<h3>Detalles</h3>');
					$('#modal-body').append('<table class="table table-hover table-striped">\n<thead>\n<th>Concepto</th>\n<th>Valor</th>\n</thead>\n<tbody>\n<tr>\n<td>ID de transaccion</td>\n<td>'+json.wallet_transaction_id+'</td>\n</tr>\n<tr>\n<td>Divisa</td>\n<td>'+json.currency_name+'</td>\n</tr>\n<tr>\n<td>Monto que retiraste</td>\n<td>'+json.output_amount_string+'</td>\n</tr>\n</tbody>\n</table>');
				
					$('#modal-footer').empty();
					$('#modal-footer').append('<button class="btn btn-primary" onclick="printDiv(\'modal\')">Generar PDF</button>');
					$('#modal-footer').append('<a onclick="redirect(\'/wallets/'+json.wallet_id+'\')" class="btn btn-primary">Cerrar</a>');
				}else{
					$('#modal-header').empty();
					$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
					$('#modal-header').append('<h4 class="modal-title" >Detectamos un error</h4>');
				
					$('#modal-body').empty();
					$('#modal-body').append('<i class="fa fa-times fa-5x" aria-hidden="true"></i>');
					$('#modal-body').append('<h2>Hubo un error al procesar tu solicitud</h2>');
					$('#modal-body').append('<h4>Por favor, intentá nuevamente mas tarde</h4>');
					$('#modal-body').append('<h5>Si seguís teniendo este error, comunicate con nosotros</h5>');
				
					$('#modal-footer').empty();
					$('#modal-footer').append('<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>');
				
				}
			},
			error: function(json){
				console.log('Entre a ajaxError()');
				console.log(json);
				$('#modal-header').empty();
				$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
				$('#modal-header').append('<h4 class="modal-title" >Error de conexión</h4>');
			
				$('#modal-body').empty();
				$('#modal-body').append('<i class="fa fa-times fa-5x" aria-hidden="true"></i>');
				$('#modal-body').append('<h2>No pudimos establecer conexión con nuestros sistemas</h2>');
				$('#modal-body').append('<h4>Por favor, recargá la página</h4>');
			
				$('#modal-footer').empty();
				$('#modal-footer').append('<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>');
			},
		});
	});
	
}
function financiate(){
	console.log('Entre a financiate()');
	var currency_id = document.getElementById('currency-id').value;
	var amount_string = document.getElementById('amount').value;
	var credit_id =document.getElementById('credit-id').value;
	var financing_id = document.getElementById('financing-id').value;
	var insurance = 3;
	if (Number(financing_id) == 0){
		insurance = $("input[name='insurance']:checked").val();
	}else{
		insurance =  document.getElementById('insurace-id').value;
	}
	console.log('Valor del seguro es (deberia ser 0 o 1: '+insurance);
	loading();
	amount_string = amount_string.replace(' ','');
	var url = 'http://'+document.domain+':'+location.port+'/ajax/string-to-number/'+currency_id+'/'+ amount_string;
	$.get(url,function(amount){
		$.ajax({
			type: 'post',
			data: {
				_token: document.getElementsByName('_token')[0].value,
				amount: amount,
				credit_id: credit_id,	
				insurance: insurance,
			},
			url: 'http://'+document.domain+':'+location.port+'/ajax/financiate',
			success: function(json){
				console.log('Entre a ajaxSuccess()');
				console.log(json);
				if(json != ''){
					$('#modal-header').empty();
					$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="location.reload()"><span aria-hidden="true">&times;</span></button>');
					$('#modal-header').append('<h4 class="modal-title" >Financiamiento realizado</h4>');
				
					$('#modal-body').empty();
					$('#modal-body').append('<i class="fa fa-check fa-5x" aria-hidden="true"></i>');
					$('#modal-body').append('<h2>Financiaste dinero con éxito</h2>');
					$('#modal-body').append('<h3>Detalles</h3>');
					$('#modal-body').append('<table class="table table-hover table-striped">\n<thead>\n<th>Concepto</th>\n<th>Valor</th>\n</thead>\n<tbody>\n<tr>\n<td>ID de transaccion</td>\n<td>'+json.financing_phase_id+'</td>\n</tr>\n<tr>\n<td>ID de financiamiento</td>\n<td>'+json.financing_id+'</td>\n</tr>\n<tr>\n<td>ID de credito</td>\n<td>'+json.credit_id+'</td>\n</tr>\n<tr>\n<td>Monto financiado total</td>\n<td>'+json.financing_amount_string+'</td>\n</tr>\n<tr>\n<td>'+json.financing_profits_title+'</td>\n<td>'+json.financing_profits_string+'</td>\n</tr>\n</tbody>\n</table>');
				
					$('#modal-footer').empty();
					$('#modal-footer').append('<button class="btn btn-primary" onclick="printDiv(\'modal\')">Generar PDF</button>');
					$('#modal-footer').append('<button type="button" class="btn btn-primary" data-dismiss="modal" onclick="location.reload()">Cerrar</button>');
				}else{
					$('#modal-header').empty();
					$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
					$('#modal-header').append('<h4 class="modal-title" >Detectamos un error</h4>');
				
					$('#modal-body').empty();
					$('#modal-body').append('<i class="fa fa-times fa-5x" aria-hidden="true"></i>');
					$('#modal-body').append('<h2>Hubo un error al procesar tu solicitud</h2>');
					$('#modal-body').append('<h4>Por favor, intentá nuevamente mas tarde</h4>');
					$('#modal-body').append('<h5>Si seguís teniendo este error, comunicate con nosotros</h5>');
				
					$('#modal-footer').empty();
					$('#modal-footer').append('<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>');
				
				}
			},
			error: function(){
				$('#modal-header').empty();
				$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
				$('#modal-header').append('<h4 class="modal-title" >Error de conexión</h4>');
			
				$('#modal-body').empty();
				$('#modal-body').append('<i class="fa fa-times fa-5x" aria-hidden="true"></i>');
				$('#modal-body').append('<h2>No pudimos establecer conexión con nuestros sistemas</h2>');
				$('#modal-body').append('<h4>Por favor, recargá la página</h4>');
			
				$('#modal-footer').empty();
				$('#modal-footer').append('<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>');
			},
		});
	});
}
function exchange(){
	console.log('Entre a exchange()');
	//Exchange::exchange($wallet, $currency, $amount, $exchange_rate, $date[0])
	var currency_from = document.getElementById('from_currency_id').value;
	var currency_to   = document.getElementById('to_currency_id').value;
	var amount_string = document.getElementById('amount').value;
	var exchange_rate = document.getElementById('exchange-rate-id').value;
	loading();
	amount_string = amount_string.replace(' ','');
	var url = 'http://'+document.domain+':'+location.port+'/ajax/string-to-number/'+currency_from+'/'+ amount_string;
	$.get(url,function(amount_from){
		$.ajax({
			type: 'post',
			data: {
				_token: document.getElementsByName('_token')[0].value,
				amount_from: amount_from,
				currency_from: currency_from,
				currency_to: currency_to,	
				exchange_rate : exchange_rate,
			},
			url: 'http://'+document.domain+':'+location.port+'/ajax/exchange',
			success: function(json){
				console.log('Entre a ajaxSuccess()');
				console.log(json);
				if(json != ''){
					$('#modal-header').empty();
					$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="location.reload()"><span aria-hidden="true">&times;</span></button>');
					$('#modal-header').append('<h4 class="modal-title" >Conversión de divisas</h4>');
				
					$('#modal-body').empty();
					$('#modal-body').append('<i class="fa fa-check fa-5x" aria-hidden="true"></i>');
					$('#modal-body').append('<h2>Realizaste la conversión con éxito</h2>');
					$('#modal-body').append('<h3>Detalles</h3>');
					$('#modal-body').append('<table class="table table-hover table-striped">\n<thead>\n<th>Concepto</th>\n<th>Valor</th>\n</thead>\n<tbody>\n<tr>\n<td>ID de transaccion</td>\n<td>'+json.exchange_id+'</td>\n</tr>\n<tr>\n<td>Divida de origen</td>\n<td>'+json.from_currency_name+'</td>\n</tr>\n<tr>\n<td>Divisa de destino</td>\n<td>'+json.to_currency_name+'</td>\n</tr>\n<tr>\n<td>Monto de egreso</td>\n<td>'+json.from_amount_string+'</td>\n</tr>\n<tr>\n<td>Monto de ingreso</td>\n<td>'+json.to_amount_string+'</td>\n</tr>\n<tr>\n<td>Monto taza de conversion</td>\n<td>'+json.exchange_rate+'</td>\n</tr>\n</tbody>\n</table>');
				
					$('#modal-footer').empty();
					$('#modal-footer').append('<button class="btn btn-primary" onclick="printDiv(\'modal\')">Generar PDF</button>');
					$('#modal-footer').append('<button type="button" class="btn btn-primary" data-dismiss="modal" onclick="location.reload()">Cerrar</button>');
				}else{
					console.log('Entre a ajaxSuccess()');
					$('#modal-header').empty();
					$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
					$('#modal-header').append('<h4 class="modal-title" >Detectamos un error</h4>');
				
					$('#modal-body').empty();
					$('#modal-body').append('<i class="fa fa-times fa-5x" aria-hidden="true"></i>');
					$('#modal-body').append('<h2>Hubo un error al procesar tu solicitud</h2>');
					$('#modal-body').append('<h4>Por favor, intentá nuevamente mas tarde</h4>');
					$('#modal-body').append('<h5>Si seguís teniendo este error, comunicate con nosotros</h5>');
				
					$('#modal-footer').empty();
					$('#modal-footer').append('<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>');
				
				}
			},
			error: function(json){
				console.log('Entre a ajaxError()');
				console.log(json);
				$('#modal-header').empty();
				$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
				$('#modal-header').append('<h4 class="modal-title" >Error de conexión</h4>');
			
				$('#modal-body').empty();
				$('#modal-body').append('<i class="fa fa-times fa-5x" aria-hidden="true"></i>');
				$('#modal-body').append('<h2>No pudimos establecer conexión con nuestros sistemas</h2>');
				$('#modal-body').append('<h4>Por favor, recargá la página</h4>');
			
				$('#modal-footer').empty();
				$('#modal-footer').append('<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>');
			},
		});
	});
}



function amountVerifier(){
	console.log('Entre a amountVerifier()');
	$('#amount-label').empty();
	var show_button = true;
	var replace_amount = true;
	var amount = document.getElementById('amount').value;
	var max_amount = document.getElementById('max_amount').value;
	var min_amount = document.getElementById('min_amount').value;
	var currency_id = document.getElementById('currency-id').value;
	var url = 'http://'+document.domain+':'+location.port+'/ajax/string-to-number/'+currency_id+'/'+ amount.replace(' ','');
	$.get(url, function(answer){
		if(answer == 'error'){
			$('#submit-button').hide();
			show_button = false;
			replace_amount = false;
			$('#amount-label').append('Debe ingresar un numero valido<br>');
		}
		if(Number(answer) < Number(min_amount)){
			$('#submit-button').hide();
			show_button = false;
			var url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+currency_id+'/'+min_amount+'/1';
			$.get(url, function(min){
				var url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+currency_id+'/'+answer+'/1';
				$.get(url, function(string){
					$('#amount-label').append('Debe ingresar un numero mayor a '+min+' (ingresó '+string+')<br>');
				});
			});
		}
		if(Number(max_amount) != -1 && Number(answer) > Number(max_amount)){
			$('#submit-button').hide();
			show_button = false;
			var url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+currency_id+'/'+max_amount+'/1';
			$.get(url, function(max){
				var url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+currency_id+'/'+answer+'/1';
				$.get(url, function(string){
					$('#amount-label').append('Debe ingresar un numero menor a '+max+' (ingresó '+string+')<br>');
				});
			});
		}
		if(replace_amount){
			var url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+currency_id+'/'+answer+'/1';
			$.get(url, function(string){
				$("#amount").val(string);
				if(show_button && !isNaN(string.substr(-1))){
					$('#submit-button').show();
				}else{
					$('#submit-button').hide();
				}
			});
		}
	});
}
function amountVerifierForPayCredit(){
	console.log('Entre a amountVerifierForPayCredit()');
	$('#amount-label').empty();
	$('#simulate-div').empty();
	var show_button = true;
	var replace_amount = true;
	var amount = document.getElementById('amount').value;
	var max_amount = document.getElementById('max_amount').value;
	var min_amount = document.getElementById('min_amount').value;
	var currency_id = document.getElementById('currency-id').value;
	var url = 'http://'+document.domain+':'+location.port+'/ajax/string-to-number/'+currency_id+'/'+ amount.replace(' ','');
	$.get(url, function(answer){
		if(answer == 'error'){
			$('#submit-button').hide();
			show_button = false;
			replace_amount = false;
			$('#amount-label').append('Debe ingresar un numero valido<br>');
		}
		if(Number(answer) < Number(min_amount)){
			$('#submit-button').hide();
			show_button = false;
			var url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+currency_id+'/'+min_amount+'/1';
			$.get(url, function(min){
				var url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+currency_id+'/'+answer+'/1';
				$.get(url, function(string){
					$('#amount-label').append('Debe ingresar un numero mayor a '+min+' (ingresó '+string+')<br>');
				});
			});
		}
		if(Number(max_amount) != -1 && Number(answer) > Number(max_amount)){
			$('#submit-button').hide();
			show_button = false;
			var url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+currency_id+'/'+max_amount+'/1';
			$.get(url, function(max){
				var url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+currency_id+'/'+answer+'/1';
				$.get(url, function(string){
					$('#amount-label').append('Debe ingresar un numero menor a '+max+' (ingresó '+string+')<br>');
				});
			});
		}
		if(replace_amount){
			var url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+currency_id+'/'+answer+'/1';
			$.get(url, function(string){
				$("#amount").val(string);
				if(show_button && !isNaN(string.substr(-1))){
					$('#simulate-div').empty();
					$('#simulate-div').append('<input type="hidden"  id="from_amount" value="'+answer+'">');
					$('#simulate-div').append('<input type="hidden"  id="exchange_rate_id" value="0">');
					var from_wallet_id = document.getElementById('from_wallet_id').value;
					url = 'http://'+document.domain+':'+location.port+'/ajax/wallets/'+from_wallet_id;
					$.get(url, function(from_wallet){
						var credit_id = document.getElementById('credit-id').value;
						url = 'http://'+document.domain+':'+location.port+'/ajax/credits/'+credit_id;
						$.get(url, function(credit){
							if(credit.currency.id != from_wallet.currency.id){
								url = 'http://'+document.domain+':'+location.port+'/ajax/local-convert/'+credit.currency.iso_code+'/'+from_wallet.currency.iso_code+'/'+answer;
								$.get(url, function(exchange){
									url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+from_wallet.currency.id+'/'+exchange+'/1';
									$.get(url, function(exchange_string){
										url = 'http://'+document.domain+':'+location.port+'/ajax/last-exchange-rate/'+credit.currency.iso_code+'/'+from_wallet.currency.iso_code;
										$.get(url, function(exchange_rate){
											$('#simulate-div').empty();
											$('#simulate-div').append('<input type="hidden"  id="from_amount" value="'+exchange+'">');
											$('#simulate-div').append('<input type="hidden"  id="exchange_rate_id" value="'+exchange_rate.id+'">');

											$('#simulate-div').append('<button class="btn btn-default" onclick="amountVerifierForPayCredit()">Actualizar tasa de conversión</button>');
											$('#simulate-div').append('<br><strong>Resultado de la conversión</strong></br>');
											$('#simulate-div').append('<p>Se utilizarán <strong>'+exchange_string+'</strong> en su wallet de '+from_wallet.currency.name+' para pagar el credito en '+credit.currency.name+'</p>');
											$('#simulate-div').append('<p>Taza de conversión: '+exchange_rate.rate_in_currency+'</p>');
											$('#simulate-div').append('<p>Taza actualizada el '+exchange_rate.diff_for_humans+'</p>');
											$('#simulate-div').append('<p>Nota: mantendremos esta tasa de cambio durante 1 minuto de haber ingresdo el último dígito del monto. Si espera más tiempo, la conversión se realizará con la última tasa de cambio en vigencia. Para actualizar la tasa de cambio, simplemente actualice la tasa de cambio');
										});
									});
								});
							}
						});
					});										
					$('#submit-button').show();
				}else{
					$('#submit-button').hide();
				}
			});
		}
	});
}
function amountVerifierForRequestCredit(){
	console.log('Entre a amountVerifierForRequestCredit()');
	$('#amount-label').empty();
	$('#simulate-div').empty();
	var show_button = true;
	var replace_amount = true;
	var amount = document.getElementById('amount').value;
	var max_amount = document.getElementById('max_amount').value;
	var min_amount = document.getElementById('min_amount').value;
	var currency_id = document.getElementById('currency-id').value;
	var url = 'http://'+document.domain+':'+location.port+'/ajax/string-to-number/'+currency_id+'/'+ amount.replace(' ','');
	$.get(url, function(answer){
		if(answer == 'error'){
			$('#submit-button').hide();
			show_button = false;
			replace_amount = false;
			$('#amount-label').append('Debe ingresar un numero valido<br>');
		}
		if(Number(answer) < Number(min_amount)){
			$('#submit-button').hide();
			show_button = false;
			var url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+currency_id+'/'+min_amount+'/1';
			$.get(url, function(min){
				var url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+currency_id+'/'+answer+'/1';
				$.get(url, function(string){
					$('#amount-label').append('Debe ingresar un numero mayor a '+min+' (ingresó '+string+')<br>');
				});
			});
		}
		if(Number(max_amount) != -1 && Number(answer) > Number(max_amount)){
			$('#submit-button').hide();
			show_button = false;
			var url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+currency_id+'/'+max_amount+'/1';
			$.get(url, function(max){
				var url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+currency_id+'/'+answer+'/1';
				$.get(url, function(string){
					$('#amount-label').append('Debe ingresar un numero menor a '+max+' (ingresó '+string+')<br>');
				});
			});
		}
		if(replace_amount){
			var url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+currency_id+'/'+answer+'/1';
			$.get(url, function(string){
				//if(decimals_flag1){
				//	string = string + currency.decimal_point;
				//}
				//if(decimals_flag2){
				//	string = string + '0';
				//}
				$("#amount").val(string);
				if(show_button && !isNaN(string.substr(-1))){
					$('#submit-button').show();
					simulateCredit();
				}else{
					$('#submit-button').hide();
				}
			});
		}
	});
}
function amountVerifierForFinancing(){
	console.log('Entre a amountVerifierForFinancing()');
	$('#amount-label').empty();
	$('#simulate-div').empty();
	var show_button = true;
	var replace_amount = true;
	var amount = document.getElementById('amount').value;
	var max_amount = document.getElementById('max_amount').value;
	var min_amount = document.getElementById('min_amount').value;
	var currency_id = document.getElementById('currency-id').value;
	var url = 'http://'+document.domain+':'+location.port+'/ajax/string-to-number/'+currency_id+'/'+ amount.replace(' ','');
	$.get(url, function(answer){
		if(answer == 'error'){
			$('#submit-button').hide();
			show_button = false;
			replace_amount = false;
			$('#amount-label').append('Debe ingresar un numero valido<br>');
		}
		if(Number(answer) < Number(min_amount)){
			$('#submit-button').hide();
			show_button = false;
			var url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+currency_id+'/'+min_amount+'/1';
			$.get(url, function(min){
				var url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+currency_id+'/'+answer+'/1';
				$.get(url, function(string){
					$('#amount-label').append('Debe ingresar un numero mayor a '+min+' (ingresó '+string+')<br>');
				});
			});
		}
		if(Number(max_amount) != -1 && Number(answer) > Number(max_amount)){
			$('#submit-button').hide();
			show_button = false;
			var url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+currency_id+'/'+max_amount+'/1';
			$.get(url, function(max){
				var url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+currency_id+'/'+answer+'/1';
				$.get(url, function(string){
					$('#amount-label').append('Debe ingresar un numero menor a '+max+' (ingresó '+string+')<br>');
				});
			});
		}
		if(replace_amount){
			var url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+currency_id+'/'+answer+'/1';
			$.get(url, function(string){
				$("#amount").val(string);
				if(show_button && !isNaN(string.substr(-1))){
					$('#submit-button').show();
					simulateFinancing();
				}else{
					$('#submit-button').hide();
				}
			});
		}
	});
}
function amountVerifierForExchange(){
	console.log('Entre a amountVerifierForExchange()');
	$('#amount-label').empty();
	$('#simulate-div').empty();
	var show_button = true;
	var replace_amount = true;
	var amount = document.getElementById('amount').value;
	if (amount != '') {
		var max_amount = document.getElementById('max_amount').value;
		var min_amount = document.getElementById('min_amount').value;
		var currency_id = document.getElementById('from_currency_id').value;
		var url = 'http://'+document.domain+':'+location.port+'/ajax/string-to-number/'+currency_id+'/'+ amount.replace(' ','');
		$.get(url, function(answer){
			if(answer == 'error'){
				$('#submit-button').hide();
				show_button = false;
				replace_amount = false;
				$('#amount-label').append('Debe ingresar un numero valido<br>');
			}
			if(Number(answer) < Number(min_amount)){
				$('#submit-button').hide();
				show_button = false;
				var url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+currency_id+'/'+min_amount+'/1';
				$.get(url, function(min){
					var url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+currency_id+'/'+answer+'/1';
					$.get(url, function(string){
						$('#amount-label').append('Debe ingresar un numero mayor a '+min+' (ingresó '+string+')<br>');
					});
				});
			}
			if(Number(max_amount) != -1 && Number(answer) > Number(max_amount)){
				$('#submit-button').hide();
				show_button = false;
				var url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+currency_id+'/'+max_amount+'/1';
				$.get(url, function(max){
					var url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+currency_id+'/'+answer+'/1';
					$.get(url, function(string){
						$('#amount-label').append('Debe ingresar un numero menor a '+max+' (ingresó '+string+')<br>');
					});
				});
			}
			if(replace_amount){
				var url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+currency_id+'/'+answer+'/1';
				$.get(url, function(string){
					$("#amount").val(string);
					if(show_button && !isNaN(string.substr(-1))){
						$('#submit-button').show();
						url = 'http://'+document.domain+':'+location.port+'/ajax/currencies/'+currency_id;
						$.get(url, function(from_currency){
							url = 'http://'+document.domain+':'+location.port+'/ajax/currencies/'+document.getElementById('to_currency_id').value;
							$.get(url, function(to_currency){
								url = 'http://'+document.domain+':'+location.port+'/ajax/local-convert/'+from_currency.iso_code+'/'+to_currency.iso_code+'/'+answer;
								$.get(url, function(simulate_exchange){
									url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+to_currency.id+'/'+simulate_exchange+'/1';
									$.get(url, function(simulate_exchange_string){
										url = 'http://'+document.domain+':'+location.port+'/ajax/last-exchange-rate/'+from_currency.iso_code+'/'+to_currency.iso_code;
										$.get(url, function(exchange_rate){
											$('#simulate-div').empty();
											$('#simulate-div').append('<button class="btn btn-default" onclick="amountVerifierForExchange()">Actualizar tasa de conversión</button>');
											$('#simulate-div').append('<br><strong>Resultado de la conversión</strong></br>');
											$('#simulate-div').append('<p>Si realiza la conversión, obtendrá <strong>'+simulate_exchange_string+'</strong> en su wallet de '+to_currency.name+'</p>');
											$('#simulate-div').append('<p>Taza de conversión: '+exchange_rate.rate_in_currency+'</p>');
											$('#simulate-div').append('<p>Taza actualizada el '+exchange_rate.diff_for_humans+'</p>');
											$('#simulate-div').append('<p>Nota: mantendremos esta tasa de cambio durante 1 minuto de haber ingresdo el último dígito del monto. Si espera más tiempo, la conversión se realizará con la última tasa de cambio en vigencia. Para actualizar la tasa de cambio, simplemente actualice la tasa de cambio');
											$('#simulate-div').append('<input type="hidden"  id="exchange-rate-id" value="'+exchange_rate.id+'">');	
										});
									});
								});
							});
						});
						
					}else{
						$('#submit-button').hide();
					}
				});
			}
		});	
	}
}
function simulateCredit(){
	console.log('Entre a simulateCredit()');
	$('#simulate-div').empty();
	var payment_plan_id = document.getElementById('payment_plan_id').value;
	var amount_string = document.getElementById('amount').value;
	var currency_id = document.getElementById('currency-id').value;
	amount_string = amount_string.replace(' ','');
	var url = 'http://'+document.domain+':'+location.port+'/ajax/string-to-number/'+currency_id+'/'+amount_string;
	$.get(url,function(amount){
		url = 'http://'+document.domain+':'+location.port+'/ajax/simulate-credit/'+payment_plan_id;
		$.get(url,function(simulation){
			console.log(simulation);
			var percentage = simulation.interest_percentage + simulation.insurance_percentage.debtor;
			//(1 + $this->interest_percentage + $this->debtor_insurance_percentage);
			var amount_to_pay = amount * percentage;
			url = 'http://'+document.domain+':'+location.port+'/ajax/percentage/'+currency_id+'/'+percentage;
			$.get(url,function(percentage_string){
				url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+currency_id+'/'+(amount_to_pay/simulation.payments_number)+'/1';
				$.get(url,function(payment_amount_string){
					url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+currency_id+'/'+amount_to_pay+'/1';
					$.get(url,function(amount_to_pay_string){
						$('#simulate-div').empty();
						$('#simulate-div').append('<label>Detalles del credito:</label><br>');
						$('#simulate-div').append('<p>Interes del plan elegido: '+percentage_string+'</p>');
						$('#simulate-div').append('<p>Monto de cada cuota: '+payment_amount_string+'</p>');
						$('#simulate-div').append('<p>Total a pagar: '+amount_to_pay_string+'</p>');
					});
				});
			});
		});
	});
}
function simulateFinancing(){
	$('#simulate-div').empty();
	console.log('Entre a simulateFinancing()');
	var currency_id = document.getElementById('currency-id').value;
	var amount_string = document.getElementById('amount').value;
	var financing_id =  document.getElementById('financing-id').value; //0 o ID
	var abort_financing = document.getElementById('is_abort_financing').value; //0 o 1
	var credit_id = document.getElementById('credit-id').value;
	var insurance = 3;
	if (Number(financing_id) == 0){
		insurance = $("input[name='insurance']:checked").val();
	}else{
		insurance =  document.getElementById('insurace-id').value;
	}
	var profits = 0;
	var amount_of_financing = 0;
	var url = 'http://'+document.domain+':'+location.port+'/ajax/string-to-number/'+currency_id+'/'+ amount_string;
	$.get(url,function(amount){
		console.log('monto en ingresado: '+amount)
		url = 'http://'+document.domain+':'+location.port+'/ajax/financings/'+financing_id+'/amount';
		$.get(url,function(financing_amount){
			var url = 'http://'+document.domain+':'+location.port+'/ajax/credits/'+credit_id;
			$.get(url,function(credit){
				if(Number(abort_financing) == 0){//no es para cancelar financiamiento, sino para financiar
					
					if (financing_id == 0) {
						amount_of_financing = amount;
					}else{
						amount_of_financing = Number(amount) + Number(financing_amount);
						console.log('monto ya financiado: '+financing_amount);
					}
				}else{//abort financing
					amount_of_financing = Number(financing_amount) - Number(amount);
				}
				var profit_percentage = Number(credit.interest_percentage) - (Number(credit.interest_percentage) * Number(credit.commission_percentage));
				if (insurance == 1) {
					profit_percentage -= Number(credit.creditor_insurance_percentage);
				}
				profits =  Number(amount_of_financing) * Number(profit_percentage);
				console.log('ganacias'+profits);
				url = 'http://'+document.domain+':'+location.port+'/ajax/currencies/'+currency_id;
				$.get(url, function(currency){
					url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+currency_id+'/'+profits+'/1';
					$.get(url,function(profits_string){
						console.log('financiamiento total: '+amount_of_financing);
						url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+currency_id+'/'+(Number(amount_of_financing) + Number(profits))+'/1';
						$.get(url,function(payout_back_string){
							if(Number(abort_financing) == 0){//financiar
								$('#simulate-div').empty();
								$('#simulate-div').append('<label>Ganancia esperada:</label><br>');
								if(financing_id == 0){
									$('#simulate-div').append('<p>Con tu financiamiento obtendrás '+payout_back_string+' <strong>(ganancia: '+profits_string+')</strong></p>');
									if(insurance == 0){
										$('#simulate-div').append('<p><strong>¡Ten en cuenta!</strong> Como estás financiando sin asegurar, no podemos garantizar un reintegro total de tu financiamiento, ya que depende de los pagos que realice el beneficiario del crédito</p>');
									}else{
										$('#simulate-div').append('<p><strong>Nota:</strong> Buena elección, asegurando tu financiamiento estarás protegido. Si el beneficiario del crédito no abona sus cuotas, nosotros nos encargaremos de abonar tu parte</p>');
									}
								}else{
									$('#simulate-div').append('<p>Con tu financiamiento, mas lo que ya financiaste, obtendrás '+payout_back_string+' <strong>(ganancia: '+profits_string+')</strong></p>');
									if(insurance == 0){
										$('#simulate-div').append('<p><strong>¡Recuerda!</strong> Financiaste sin asegurar, este valor es solo estimativo, ya que depende exclusivamente de como realice los pagos el beneficiario del crédito</p>');
									}else{
										$('#simulate-div').append('<p><strong>Nota:</strong> Tu financiamiento está asegurado, por lo que recibirás el dinero, pase lo que pase</p>');
									}
								}
								
							}else{
								if (Number(amount_of_financing) >= currency.min_value) {
									$('#simulate-div').empty();
									$('#simulate-div').append('<label>Nuevo monto del financiamiento:</label><br>');
									$('#simulate-div').append('<p>Con tu disminución del financiamiento, obtendrás '+payout_back_string+' <strong>(ganancia: '+profits_string+')</strong></p>');
									if(insurance == 0){
										$('#simulate-div').append('<p><strong>¡Recuerda!</strong> Financiaste sin asegurar, este valor es solo estimativo, ya que depende exclusivamente de como realice los pagos el beneficiario del crédito</p>');
									}else{
										$('#simulate-div').append('<p><strong>Nota:</strong> Tu financiamiento está asegurado, por lo que recibirás el dinero, pase lo que pase</p>');
									}
								}else{
									$('#simulate-div').empty();
									$('#simulate-div').append('<label>Financiamiento inactivo:</label><br>');
									$('#simulate-div').append('<p>Tu financiamiento se agrupará junto a los financiamienstos inactivos, por lo que no recibirás ingresos por este financiamiento</p>');
									$('#simulate-div').append('<p><strong>Nota:</strong> Puedes volver a agregar dinero al financiamiento mientras el crédito siga en su etapa de financiamiento, activándolo nuevamente. No puedes cambiar las preferencias del seguro</p>');
								}
							}
						});
					});
				});
			});
		});
	});
}
function exchangeRateLoaderInExchange(){
	$('#exchange-rate-areachart').empty();
	drawExchangeRate();
	amountVerifierForExchange();
}


function loading(){
	console.log('Entre a loading()');
	$('#modal-body').empty();
	$('#modal-body').append('<i class="fa fa-clock-o fa-5x" aria-hidden="true"></i>');
	$('#modal-body').append('<h2>Estamos procesando su solicitud</h2>');
	$('#modal-body').append('<h4>Por favor espere</h4>');

	$('#modal-footer').empty();
}
function fillAmountId(text){
	console.log('Entre a fillAmountId(text)');
	$("#amount").val(text);
	amountVerifierForPayCredit();
}
function printDiv(divName) {
	var printContents = document.getElementById(divName).innerHTML;
	var originalContents = document.body.innerHTML;
	document.body.innerHTML = printContents;
	window.print();
	document.body.innerHTML = originalContents;
}
function redirect(url){
	location.href = url;
}
/*
	Funciones ajax en las que no imprime la pantalla:
		grantCredit()
		abortCredit()
		payCredit()
		cancelFinancing()
*/