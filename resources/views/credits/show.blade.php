@php
    use App\Models\CreditStage;
    use Illuminate\Database\Eloquent\Collection;
    use Carbon\Carbon;
    $currency = $credit->currency();
    $credit_phases = $credit->creditPhases->reverse();
    $wallets = auth()->user()->wallets;
    $wallets_with_balance = new Collection();
    foreach($wallets as $wallet){
        if($wallet->accountBalance() >= $wallet->currency->min_value){
            $wallets_with_balance->add($wallet);
        }
    }
@endphp
@extends('adminlte::page')

@section('title_prefix', 'Detalles de crédito - ')

@section('content')
    <div class="box" id="box">
        <div class="box-header">
            <h1 id="title">
                Detalles del crédito {!!$credit->id!!}
                @if ($credit->unpayedAmount() > 0)
                    @if ($wallets_with_balance->count() > 0)
                        @php
                            if ($wallets_with_balance->pluck('currency_id')->contains($credit->currency()->id)) {
                                $wallet_id = $wallets_with_balance->where('currency_id',$credit->currency()->id)->first()->id;
                            } else {
                                $wallet_id = $wallets_with_balance->random()->id;
                            }
                        @endphp
                        <a class="btn btn-success pull-right" data-toggle="modal" data-target="#custom-modal" onclick="payWithCredit({!!$credit->id.','.$wallet_id!!})">Realizar pago</a>
                    @endif
                @endif
                @if ($credit->nextCreditStages()->pluck('id')->contains(CreditStage::getGrant()->id))
                    <a class="btn btn-success pull-right" data-toggle="modal" data-target="#custom-modal" onclick="askGrantCredit()">Transferir a mi Wallet</a>
                @endif
                @if ($credit->nextCreditStages()->pluck('id')->contains(CreditStage::getAbort()->id))
                    <a class="btn btn-danger pull-right" data-toggle="modal" data-target="#custom-modal" onclick="askAbortCredit()">Cancelar credito</a>
                @endif
                @if ($credit->lastCreditPhase()->creditStage->id == CreditStage::getFinancing()->id)
                    <div id="chart">
                        <div id="credit-financing-areachart" style="height: 300px;"></div>
                    </div>
                    <form>
                        {!!csrf_field()!!}
                        <input type="hidden"  id="credit_id" value="{!!$credit->id!!}">
                        <input type="hidden"  id="wallet_id" value="{!!$credit->wallet->id!!}">
                    </form>
                    <script type="text/javascript">
                        creditFinancingAreachart();
                    </script>
                @else
                    @if ($credit->unpayedAmount() > 0)
                        <div id="chart">
                            <div id="credit-pay-left-donutchart" style="height: 300px;"></div>
                        </div> 
                        <form>
                            {!!csrf_field()!!}
                            <input type="hidden"  id="credit_id" value="{!!$credit->id!!}">
                            <input type="hidden"  id="wallet_id" value="{!!$credit->wallet->id!!}">
                        </form>
                        <script type="text/javascript">
                            creditPayLeftDonutchart();
                        </script>
                    @else
                    @php
                        $rows = abs($credit->creditPhases->first()->date->format('Y') - $credit->creditPhases->last()->date->format('Y')) + 1;
                    @endphp
                        <div id="chart">
                            <div id="credit-phases-calendarchart" style="height: {!!$rows * 175!!}px;"></div>
                        </div>  
                        <form>
                            {!!csrf_field()!!}
                            <input type="hidden"  id="credit_id" value="{!!$credit->id!!}">
                            <input type="hidden"  id="wallet_id" value="{!!$credit->wallet->id!!}">
                        </form>
                        <script type="text/javascript">
                            creditPhasesCalendarchart();
                        </script>
                    @endif
                @endif
            </h1>
        </div>
        <div class="box-body">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <th width="20%">Concepto</th>
                        <th width="20%">Valor</th>
                    </thead>
                    <tbody>
                        <tr>
                            <td>ID de crédito</td>
                            <td>{!!$credit->id!!}</td>
                        </tr>
                        <tr>
                            <td>Monto</td>
                            <td>{!!$currency->numberToString($credit->amount)!!}</td>
                        </tr>
                        <tr>
                            <td>Divisa</td>
                            <td>{!!ucfirst($currency->name)!!}</td>
                        </tr>
                        @if ($credit->lastCreditPhase()->creditStage->id == CreditStage::getFinancing()->id)
                            <td>Financiado</td>
                            <td>{!!$currency->numberToString($credit->financedAmount()).' ('.$currency->percentage($credit->financedAmount()/$credit->amount,2).')'!!}</td>
                        @endif
                        <tr>
                            <td>Intereses a pagar</td>
                            @php
                                $total_of_interests = $credit->debtorInterests() - 1    ;
                            @endphp
                            <td>{!!$currency->percentage($total_of_interests)!!}</td>
                        </tr>
                        @php
                            $total_for_repayment = $credit->amount * $total_of_interests;
                        @endphp
                        <tr>
                            <td>Total a pagar</td>
                            <td>{!!$currency->numberToString($total_for_repayment)!!}</td>
                        </tr>
                        @if ($credit->unpayedAmount() >= $currency->min_value)
                            <tr>
                                @php
                                    $left_payments = '';
                                    if($credit->firstUnpayedPayment() != null){
                                        $left_payments = $credit->payments->count() - $credit->firstUnpayedPayment()->number() + 1;
                                    }
                                    if((is_numeric($left_payments)) && ($left_payments == 1)){
                                        $left_payments = '1 cuota';
                                    }
                                    if((is_numeric($left_payments)) && ($left_payments > 1)){
                                        $left_payments = $left_payments.' cuotas';
                                    }
                                @endphp
                                <td>Saldo restante</td>
                                <td>{!!$currency->numberToString($credit->unpayedAmount()).' ('.$left_payments.')'!!}</td>
                            </tr>
                        @endif
                        <tr>
                            <td>Solicitado</td>
                            <td>{!!'El '.$credit->creditPhases->first()->date->format('d/m/Y').' a las '.$credit->creditPhases->first()->date->format('h:m').'<strong> ('.$credit->creditPhases->first()->date->diffForHumans().')</strong>'!!}</td>
                        </tr>
                        @if ($credit->unpayedAmount() < $currency->min_value)
                            <tr>
                                <td>Saldado</td>
                                <td>{!!$credit->lastCreditPhase()->date->format('d/m/Y').' ('.$credit->lastCreditPhase()->date->diffForHumans().')'!!}</td>
                            </tr>
                        @endif
                        @if ($credit->payments->count() > 0)
                            @if ($credit->retainsMargin())
                                <tr class="danger">
                            @else
                                @if (($credit->firstUnpayedPayment() != null) &&(Carbon::now()->diffInDays($credit->firstUnpayedPayment()->due_date) <= 3))
                                    <tr class="warning">
                                @else
                                    @if ($credit->unpayedAmount() < $credit->currency()->min_value)
                                        <tr>
                                    @else
                                        <tr class="success">                                     
                                    @endif
                                @endif
                            @endif
                                <td>Ver detalles de las cuotas</td>
                                <td>
                                    <a onclick="redirect('/credits/{!!$credit->id!!}/payments')" class="btn btn-primary">Ver cuotas</a>
                                </td>
                            </tr>
                        @endif                        
                    </tbody>                    
                </table>
            </div>
            <br>
            <div class="table-responsive container-border">
                <h3>Historial de estados del crédito</h3>
                <table class="table table-paginated table-striped table-hover">
                    <thead>
                        <th width="30px">Num</th>
                        <th>ID de estado</th>
                        <th>Estado</th>
                        <th>Fecha</th>
                    </thead>
                    @php
                        $entry_num = 1;
                    @endphp
                    <tbody>
                        @foreach ($credit_phases as $credit_phase)
                            <tr>
                                <td>{!!$entry_num!!}</td>
                                @php
                                    $entry_num++;
                                @endphp
                                <td>{!!$credit_phase->id!!}</td>
                                <td>{!!$credit_phase->creditStage->name!!}</td>
                                <td>{!!'El '.$credit_phase->date->format('d/m/Y').' a las '.$credit_phase->date->format('h:m').'<strong> ('.$credit_phase->date->diffForHumans().')</strong>'!!}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div>
            <button class="btn btn-primary" onclick="printDiv('box')">Generar PDF</button>
        </div>
    </div>
    @extends('modal')      
@endsection