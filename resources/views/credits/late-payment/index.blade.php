@extends('adminlte::page')

@section('title_prefix', 'Créditos con pago retrasado - ')

@section('content')
    <div class="box" id="box">
        <div class="box-header">
            <h1 id="title">Créditos con pago retrasado</h1>
        </div>
        <div class="box-body">
            <div class="table-resposive container-border">
            <table class="table table-paginated table-hover table-striped">
                <thead>
                    <th width="30px">Num</th>
                    <th>Divisa</th>
                    <th>Créditos Solicitados</th>
                    <th width="10%">Ver detalles</th>
                </thead>
                @php
                    $entry_num = 1;
                @endphp
                <tbody>
                    @foreach ($items as $item)
                        <tr>
                            <td>{!!$entry_num!!}</td>
                            @php
                                $entry_num++;
                            @endphp
                            <td>{!!ucfirst($item->first()->currency()->name)!!}</td>
                            @if ($item->count() == 1)
                                <td>1 crédito</td>
                            @else
                                <td>{!!$item->count()!!} créditos</td>
                            @endif
                            <td>
                                <a onclick="redirect('/credits/late-payment/currency={!!$item->first()->currency()->id!!}')" class="btn btn-primary">Detalles</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div>
            <button class="btn btn-primary" onclick="printDiv('box')">Generar PDF</button>
        </div>    
    </div>
@stop