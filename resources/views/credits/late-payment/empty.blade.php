@extends('adminlte::page')

@section('title_prefix', 'Créditos con pago retrasado - ')

@section('content')
    <div class="box" id="box">
        <div class="box-header">
            <h1 id="title">Créditos con pago retrasado</h1>
        </div>
        <div class="box-body">
            <h1><i class="fa fa-info-circle fa-5x" aria-hidden="true"></i></h1>
            <h1>No existen créditos con pago retrasado en este momento.</h1>
        </div>
@stop