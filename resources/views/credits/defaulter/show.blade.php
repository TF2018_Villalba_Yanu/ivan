@php
    use Illuminate\Database\Eloquent\Collection;
    use App\Models\CreditStage;
    use Carbon\Carbon;
    $currency = $credit->currency();
    $user = $credit->wallet->user;
@endphp
@extends('adminlte::page')

@section('title_prefix', 'Detalles de crédito - ')

@section('content')
    <div class="box"id="box">
        <div class="box-header">
            <h1 id="title">
                Detalles del crédito con mora
            </h1>
        </div>
        <div class="box-body">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <th width="20%">Concepto</th>
                        <th width="20%">Valor</th>
                    </thead>
                    <tbody>
                        <tr>
                           <td>ID del credito</td> 
                           <td>{!!$credit->id!!}</td>
                        </tr>
                        <tr>
                            <td>Fecha última fase</td>
                            <td>{!!'El '.$credit->lastCreditPhase()->date->format('d/m/Y').' a las '.$credit->lastCreditPhase()->date->format('h:m').'<strong> ('.$credit->lastCreditPhase()->date->diffForHumans().')</strong>'!!}</td>
                        </tr>
                        <tr>
                            <td>Divisa</td>
                            <td>{!!$currency->name.' ('.$currency->iso_code.')'!!}</td>
                        </tr>
                        <tr>
                            <td>Monto</td>
                            <td>{!!$currency->numberToString($credit->amount)!!}</td>
                        </tr>
                        <tr>
                            <td>Intereses a pagar</td>
                            @php
                                $total_of_interests = $credit->debtorInterests() - 1;
                            @endphp
                            <td>{!!$currency->percentage($total_of_interests)!!}</td>
                        </tr>                            
                        <tr>
                            <td>Numero de cuotas</td>
                            <td>{!!$credit->payments_number!!}</td>
                        </tr>
                        @php
                            $total_for_repayment = $credit->amount * $total_of_interests;
                            $payments_value = $total_for_repayment/$credit->payments_number;
                        @endphp      
                        <tr>
                            <td>Total a pagar</td>
                            <td><strong>{!!$currency->numberToString($payments_value*$credit->payments_number)!!}</strong></td>
                        </tr>            
                    </tbody>                    
                </table>
            </div>
            <br>
            <div class="table-responsive">
                <h3>Datos del usuario</h3>
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <th width="20%">Concepto</th>
                        <th width="20%">Valor</th>
                    </thead>
                    <tbody>
                        @php
                            $rank = $user->reputation->rank()
                        @endphp
                        @if (($rank == "A") || ($rank == "AA") || ($rank == "AAA"))
                            <tr class="success">
                        @else
                            @if ($rank == "B")
                                <tr class="warning">
                            @else
                                <tr class="danger">
                            @endif
                        @endif
                            <td>Perfil crediticio (*)</td>
                            <td>{!!$rank!!}</td>
                        </tr>
                        <tr>
                            <td>Ver detalles de la reputación</td>
                            <td>
                                <a onclick="redirect('/reputations/{!!$user->reputation->id!!}/from-credit={!!$credit->id!!}')" class="btn btn-primary">Reputación</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Nombre</td>
                            <td>{!!$user->name!!}</td>
                        </tr>
                        <tr>
                            <td>Telefono</td>
                            <td>{!!$user->phone!!}</td>
                        </tr>
                        <tr>
                            <td>Correo electronico</td>
                            <td>{!!$user->email!!}</td>
                        </tr>
                        <tr>
                            <td>Lugar de residencia</td>
                            <td>{!!$user->address->city->name.', '.$user->address->city->district->country->name!!}</td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="2">
                                <p>(*): Perfil obtenido en base al historial de pagos del usuario. El valor mas bajo es "D" y el mas alto "AAA". Para mas detalles, puede ver detalles de la reputacion</p>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
            @php
                $items = $user->credits();
                $active_credits = new Collection();
                $inactive_credits = new Collection();
                foreach ($items as $item) {
                    if ($item->isActive()) {
                        $active_credits->add($item);
                    }else{
                        $inactive_credits->add($item);
                    }
                }
            @endphp
            @if ($active_credits->count() > 1)
                <br>
                <div class="table-responsive container-border">
                    <h3>Créditos activos</h3>
                    <table class="table table-paginated table-striped table-hover">
                        <thead>
                            <th width="30px">Num</th>
                            <th>Monto</th>
                            <th>Denomiacion</th>
                            <th>Activo desde</th>
                            <th>Ultimo estado</th>
                            <th>Ver detalles</th>
                        </thead>
                        @php
                            $entry_num = 1;
                        @endphp
                        <tbody>
                            @foreach ($active_credits as $active_credit)
                                @if ($active_credit->id != $credit->id)
                                    @if ($active_credit->retainsMargin())
                                        <tr class="danger">
                                    @else
                                        @if (($active_credit->firstUnpayedPayment() != null) &&(Carbon::now()->diffInDays($active_credit->firstUnpayedPayment()->due_date) <= 3))
                                            <tr class="warning">
                                        @else
                                            <tr class="success"> 
                                        @endif
                                    @endif
                                        <td>{!!$entry_num!!}</td>
                                        @php
                                            $entry_num++;
                                        @endphp
                                        <td>{!!$active_credit->currency()->numberToString($active_credit->amount,1)!!}</td>
                                        <td>{!!$active_credit->currency()->iso_code!!}</td>
                                        <td>{!!'El '.$active_credit->creditPhases->first()->date->format('d/m/Y ').'<strong> ('.$active_credit->creditPhases->first()->date->diffForHumans().')</strong>'!!}</td>
                                        <td>{!!$active_credit->lastCreditPhase()->creditStage->name!!}</td>
                                        <td>
                                            <a onclick="redirect('/credits/{!!$active_credit->id!!}')" class="btn btn-primary">Detalles</a>
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                        </tbody>
                    </table>
                </div>
            @endif
            @if ($inactive_credits->count() > 0)
                <br>
                <div class="table-responsive container-border">
                    <h3>Créditos inactivos</h3>
                    <table class="table table-paginated table-striped table-hover table-condensed">
                        <thead>
                            <th width="30px">Num</th>
                            <th>Monto</th>
                            <th>Denomiacion</th>
                            <th>Inactivo desde</th>
                            <th>Ultimo estado</th>
                            <th>Ver detalles</th>
                        </thead>
                        @php
                            $entry_num = 1;
                        @endphp
                        <tbody>
                            @foreach ($inactive_credits as $inactive_credit)
                                <tr>
                                    <td>{!!$entry_num!!}</td>
                                    @php
                                        $entry_num++;
                                    @endphp
                                    <td>{!!$inactive_credit->currency()->numberToString($inactive_credit->amount,1)!!}</td>
                                    <td>{!!$inactive_credit->currency()->iso_code!!}</td>
                                    <td>{!!'El '.$inactive_credit->lastCreditPhase()->date->format('d/m/Y ').'<strong> ('.$inactive_credit->lastCreditPhase()->date->diffForHumans().')</strong>'!!}</td>
                                    <td>{!!$inactive_credit->lastCreditPhase()->creditStage->name!!}</td>
                                    <td>
                                        <a onclick="redirect('/credits/{!!$inactive_credit->id!!}')" class="btn btn-primary">Detalles</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            @endif
            <div> 
                <a class="btn btn-danger" data-toggle="modal" data-target="#custom-modal" onclick="askJudicialCitationForCredit()">Enviar citación judicial</a> 
                <button class="btn btn-primary pull-rigth" onclick="printDiv('box')">Generar PDF</button>            
            </div>
        </div>
    </div>
    <form>
        {!!csrf_field()!!}
        <input type="hidden"  id="credit_id" value="{!!$credit->id!!}">
    </form>
    @include('modal')        
@endsection