@php
    $credits = $credits->reverse();
    $currency = $credits->first()->currency();
@endphp 

@extends('adminlte::page')

@section('title_prefix','Créditos en '.$currency->name.' - ')

@section('content')
<div class="box" id="box">
    <div class="box-header">
        <h1 id="title">Créditos en litigio en {!!$currency->name!!}</h1>
    </div>
    <div class="box-body">
        <div class="table-responsive container-border">
            <table class="table table-paginated table-hover table-striped">
                <thead>
                    <th width="30px">Num</th>
                    <th>Fecha última fase</th>
                    <th>Numero de credito</th>
                    <th>Monto del credito</th>
                    <th>Usuario</th>
                    <th>Cantidad de créditos en vigencia</th>
                    <th>Detalles</th>
                </thead>
                @php
                    $entry_num = 1;
                @endphp
                <tbody>
                    @foreach ($credits as $credit)
                        @php
                            $currency = $credit->currency();
                        @endphp
                        <tr>
                            <td>{!!$entry_num!!}</td>
                            @php
                                $entry_num++;
                            @endphp
                            <td>{!!'El '.$credit->lastCreditPhase()->date->format('d/m/Y').' a las '.$credit->lastCreditPhase()->date->format('h:m').'<strong> ('.$credit->lastCreditPhase()->date->diffForHumans().')</strong>'!!}</td>
                            <td>{!!$credit->id!!}</td>
                            <td>{!!$currency->numberToString($credit->amount,1)!!}</td>
                            <td>{!!$credit->wallet->user->name!!}</td>
                            <td>{!!$credit->wallet->user->activeCredits()->count()!!}</td>
                            <td>
                                <a onclick="redirect('/credits/lawsuit/{!!$credit->id!!}')" class="btn btn-primary">Detalles</a>
                            </td>
                        </tr>                        
                    @endforeach
                </tbody>  
            </table>
        </div>
    </div>
    <div>
        <button class="btn btn-primary" onclick="printDiv('box')">Generar PDF</button>
    </div>
</div>
@stop