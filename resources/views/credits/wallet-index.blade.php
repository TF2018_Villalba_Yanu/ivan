@php
    use App\Models\CreditStage;
    use Carbon\Carbon;
    $user = auth()->user();
    $credits = $wallet->credits;
    $currency = $wallet->currency;
    $credits = $credits->sortByDesc(function($credit,$key){
        return $credit->id;
    });
@endphp 

@extends('adminlte::page')

@section('title_prefix', 'Créditos en '.strtolower($wallet->currency->name).' - ')

@section('content')
    <div class="box" id="box">
        <div class="box-header">
            <h1 id="title">
                Créditos en {!!strtolower($wallet->currency->name)!!}  
                @if (($user->reputation->activeCreditsLimit() > $user->activeCredits()->count()) && ($currency->creditMargin() >= $currency->creditMinAmount()) && ($user->creditsThatRetentsTheMargin()->count() == 0))
                   <a class="btn btn-success pull-right" data-toggle="modal" data-target="#custom-modal" onclick="requestCreditWitCurrency({!!$currency->id!!})">Solicitar crédito</a>
                @endif  
                <div id="chart">
                    <div id="credits-wallet-donutchart" style="height: 300px;"></div>
                </div>
                <form>
                    {!!csrf_field()!!}
                    <input type="hidden"  id="wallet_id" value="{!!$wallet->id!!}">
                </form>    
                <script type="text/javascript">
                    creditsWalletDonutchart();
                </script>
            </h1>
        </div>
        <div class="box-body">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <th width="20%">Concepto</th>
                        <th width="20%">Valor</th>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Créditos solicitados</td>
                            @if ($credits->count() == 1)
                                <td>1 credito</td>
                            @else
                                <td>{!!$credits->count()!!} créditos</td>
                            @endif
                        </tr>
                        <tr>
                            <td>Monto minimo para solicitar créditos</td>
                            <td>{!!$currency->numberToString($currency->creditMinAmount())!!}</td>
                        </tr>
                        <tr>
                            <td>Margen disponible</td>
                            <td>{!!$currency->numberToString($currency->creditMargin())!!}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            @if ($wallet->activeCredits()->count() > 0)
                <div class="table-responsive container-border">
                    <h3>Créditos activos</h3>
                    <table class="table table-paginated table-hover table-striped table-condensed">
                        <thead>
                            <th width="30px">Num</th>
                            <th>ID de crédito</th>
                            <th>Monto solicitado</th>
                            <th>Porcentaje de interes</th>
                            <th>Saldo restante</th>
                            <th>Ultimo estado del crédito</th>
                            <th width="10%">Ver detalles</th>
                        </thead>
                        @php
                            $entry_num = 1;
                        @endphp
                        <tbody>
                            @foreach ($credits as $credit)
                                @if ($credit->isActive())
                                    @if ($credit->retainsMargin())
                                        <tr class="danger">
                                    @else
                                        @if ((($credit->firstUnpayedPayment() != null) && (Carbon::now()->diffInDays($credit->firstUnpayedPayment()->due_date) <= 3))||($credit->payments->count() == 0))
                                            <tr class="warning">
                                        @else
                                            <tr class="success"> 
                                        @endif
                                    @endif
                                        <td>{!!$entry_num!!}</td>
                                        @php
                                            $entry_num++;
                                        @endphp
                                        <td>{!!$credit->id!!}</td>
                                        <td>{!!$currency->numberToString($credit->amount)!!}</td>
                                        <td>{!!$currency->percentage($credit->debtorInterests() - 1)!!}</td>
                                        @php
                                            $left_payments = '';
                                            if($credit->firstUnpayedPayment() != null){
                                                $left_payments = $credit->payments->count() - $credit->firstUnpayedPayment()->number() +1;
                                            }
                                            if((is_numeric($left_payments)) && ($left_payments == 1)){
                                                $left_payments = '1 cuota';
                                            }
                                            if((is_numeric($left_payments)) && ($left_payments > 1)){
                                                $left_payments = $left_payments.' cuotas';
                                            }
                                        @endphp
                                        @if ($credit->unpayedAmount() > 0)
                                            <td>{!!$currency->numberToString($credit->unpayedAmount()).' ('.$left_payments.')'!!}</td>
                                        @else
                                            <td>----</td>
                                        @endif
                                        @if ($credit->lastCreditPhase()->creditStage->id == CreditStage::getFinancing()->id)
                                            <td>{!!$credit->lastCreditPhase()->creditStage->name.' ('. $currency->percentage($credit->financedAmount()/ $credit->amount,2).')'!!}</td>
                                        @else
                                            <td>{!!$credit->lastCreditPhase()->creditStage->name!!}</td>
                                        @endif
                                        <td>
                                            <a  onclick="redirect('/credits/{!!$credit->id!!}')" class="btn btn-primary">Detalles</a>
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                        </tbody>
                    </table>
                </div>
            @endif
            
            @if (($wallet->credits->count() - $wallet->activeCredits()->count()) > 0)
                <br>
                <div class="table-responsive container-border">
                    <h3>Créditos inactivos</h3>
                    <table class="table table-paginated table-striped table-hover table-condensed">
                        <thead>
                            <th width="30px">Num</th>
                            <th>ID de crédito</th>
                            <th>Monto solicitado</th>
                            <th>Porcentaje de interes</th>
                            <th>Inactivo hace</th>
                            <th>Ultimo estado del crédito</th>
                            <th width="10%">Ver detalles</th>
                        </thead>
                        @php
                            $entry_num = 1;
                        @endphp
                        <tbody>
                            @foreach ($credits as $credit)
                                @if (!$credit->isActive())
                                    <tr>
                                        <td>{!!$entry_num!!}</td>
                                        @php
                                            $entry_num++;
                                        @endphp
                                        <td>{!!$credit->id!!}</td>
                                        <td>{!!$currency->numberToString($credit->amount)!!}</td>
                                        <td>{!!$currency->percentage($credit->debtorInterests()- 1)!!}</td>
                                        <td>{!!ucfirst($credit->lastCreditPhase()->date->diffForHumans())!!}</td>
                                        <td>{!!$credit->lastCreditPhase()->creditStage->name!!}</td>
                                        <td>
                                            <a  onclick="redirect('/credits/{!!$credit->id!!}')" class="btn btn-primary">Detalles</a>
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                        </tbody>
                    </table>
                </div>
            @endif
        </div>
        <div>
            <button class="btn btn-primary" onclick="printDiv('box')">Generar PDF</button>
        </div>
    </div>
    @include('modal')
@stop