@php
    $user = auth()->user();
    $wallets = $wallets->sortByDesc(function($wallet,$key){
        $sum = $wallet->financings->count() + ($wallet->activeCredits()->count() * 1000);
        return $sum;
    });
@endphp 

@extends('adminlte::page')

@section('title_prefix', 'Créditos por revisar- ')

@section('content')
    <div class="box" id="box">
        <div class="box-header">
            <h1 id="title">
               Créditos de {!!$user->name!!}
                @if (($user->reputation->activeCreditsLimit() > $user->activeCredits()->count()) && ($user->userMargin() > 5 ) && ($user->creditsThatRetentsTheMargin()->count() == 0))
                    <a class="btn btn-success pull-right" data-toggle="modal" data-target="#custom-modal" onclick="requestCreditWitCurrency({!!$user->currency()->id!!})">Solicitar crédito</a>
                @else
                    <a onclick="redirect('/wallets')" class="btn btn-success pull-right">Wallets</a>
                @endif
                <div id="chart">
                    <div id="credits-all-donutchart" style="height: 300px;"></div>
                </div>
                <script type="text/javascript">
                    creditsAllDonutchart();
                </script>
            </h1>
        </div>
        <div class="box-body">
            <div class="table-resposive container-border">
            <table class="table table-paginated table-hover table-striped">
                <thead>
                    <th width="30px">Num</th>
                    <th>Divisa</th>
                    <th>Monto</th>
                    <th>Créditos Solicitados</th>
                    <th>Créditos activos</th>
                    <th width="10%">Ver detalles</th>
                </thead>
                @php
                    $entry_num = 1;
                @endphp
                <tbody>
                    @foreach ($wallets as $wallet)
                        @if ($wallet->credits->count() > 0)
                        @php
                            $credits = $wallet->credits;
                            $active_credits = $wallet->activeCredits();
                        @endphp
                        @if ($wallet->retainsMargin())
                            <tr class="danger">
                        @else
                            @if ($active_credits->count() > 0)
                                <tr class="success">
                            @else
                                <tr class="active">  
                            @endif
                        @endif
                            <td>{!!$entry_num!!}</td>
                            @php
                                $entry_num++;
                            @endphp
                            <td>{!!'Créditos en '.strtolower($wallet->currency->name)!!}</td>
                            <td>{!!$wallet->currency->numberToString($wallet->creditsAmount())!!}</td>
                            @if ($credits->count() == 1)
                                <td>1 credito</td>
                            @else
                                <td>{!!$credits->count()!!} créditos</td>
                            @endif
                            @if ($active_credits->count() == 1)
                                <td>1 credito</td>
                            @else
                                <td>{!!$active_credits->count()!!} créditos</td>
                            @endif
                            <td>
                                <a onclick="redirect('/credits/currency={!!$wallet->currency->id!!}')" class="btn btn-primary">Detalles</a>
                            </td>
                        </tr> 
                        @endif
                    @endforeach
                </tbody>
            </table>
        </div>
        <div>
            <button class="btn btn-primary" onclick="printDiv('box')">Generar PDF</button>
        </div>
    </div>
    <form>
        {!!csrf_field()!!}
    </form>
    @include('modal')
@stop