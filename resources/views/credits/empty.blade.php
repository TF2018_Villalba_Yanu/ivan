@php
    $user = auth()->user();
@endphp 

@extends('adminlte::page')

@section('title_prefix', 'Créditos - ')

@section('content')
    <div class="box" id="box">
        <div class="box-header">
            <h1 id="title">
                Créditos de {!!$user->name!!}
                @if (($user->reputation->activeCreditsLimit() > $user->activeCredits()->count()) && ($user->userMargin() > 5 ) && ($user->creditsThatRetentsTheMargin()->count() == 0))
                    <a class="btn btn-success pull-right" data-toggle="modal" data-target="#custom-modal" onclick="requestCreditWitCurrency({!!$user->currency()->id!!})">Solicitar crédito</a>
                @endif
            </h1>
        </div>
        <div class="box-body">
            <h1><i class="fa fa-info-circle fa-5x" aria-hidden="true"></i></h1>
            <h1>Aún no tienes ningún crédito.</h1>
            @if (($user->reputation->activeCreditsLimit() > $user->activeCredits()->count()) && ($user->userMargin() > 5 ) && ($user->creditsThatRetentsTheMargin()->count() == 0))
                <h2>Puedes solicitar uno haciendo clic en <a data-toggle="modal" data-target="#custom-modal" onclick="requestCreditWitCurrency({!!$user->currency()->id!!})">Solicitar crédito</a></h2>
            @endif
            <form>
                {!!csrf_field()!!}
            </form>
        </div>
    </div>
    @include('modal')
@stop