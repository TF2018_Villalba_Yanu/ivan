@php
    use Illuminate\Database\Eloquent\Collection;
    use App\Models\Role;
    use App\Models\ExchangeRate;
    $user = auth()->user();
    $wallets_with_balance = new Collection();
    foreach($wallets as $wallet){
        if($wallet->accountBalance() >= $wallet->currency->min_value){
            $wallets_with_balance->add($wallet);
        }
    }
    $wallets = $wallets->sortByDesc(function($wallet,$key){
        return ExchangeRate::localConvert($wallet->currency->iso_code,'USD',$wallet->accountBalance());
    });
@endphp 

@extends('adminlte::page')

@section('title_prefix', 'Wallets - ')

@section('content')
    <div class="box" id="box">
        <div class="box-header">
            <h1 id="title">
                Wallets de {!!$user->name!!}
                <a class="btn btn-success pull-right" data-toggle="modal" data-target="#custom-modal" onclick="inputMoneyWhitCurrency({!!$user->currency()->id!!})"> Ingresar dinero</a>
                @if ($wallets_with_balance->count() > 0)
                    @php
                        if ($wallets_with_balance->pluck('currency_id')->contains($user->currency()->id)) {
                            $wallet_id = $wallets_with_balance->where('currency_id',$user->currency()->id)->first()->id;
                        } else {
                            $wallet_id = $wallets_with_balance->random()->id;
                        }
                    @endphp
                    <a class="btn btn-danger pull-right" data-toggle="modal" data-target="#custom-modal" onclick="outputMoneyWhitCurrency({!!$wallet_id!!})"> Retirar dinero</a>
                @endif
                @if ($user->role->id == Role::getUserRole()->id)
                    <div id="chart">
                        <div id="wallets-founds-donutchart" style="height: 300px;"></div>
                    </div>
                    <script>
                        walletsFoundsDonutchart();
                    </script>
                @else
                    <div id="chart">
                        <div id="wallets-founds-columnchart" style="height: 300px;"></div>
                    </div>
                    <script>
                        walletsFoundsColumnchart();
                    </script>
                @endif  
            </h1>
        </div>
        <div class="box-body">
            <div class="table-responsive container-border">
                <table class="table table-paginated table-hover table-striped">
                    <thead>
                        <th width="30px">Num</th>
                        <th>ID de wallet</th>
                        <th>Divisa</th>
                        <th>Dinero en cuenta</th>
                        <th>Fecha ultimo movimiento</th>
                        <th width="10%">Ver detalles</th>
                    </thead>
                    @php
                        $entry_num = 1;
                    @endphp
                    <tbody>
                        @foreach ($wallets as $wallet)
                            @if ($wallet->walletTransactions->count() > 0)
                                @if ($wallet->accountBalance() >= $wallet->currency->min_value)
                                    <tr class="success">
                                @else
                                    <tr> 
                                @endif
                                    <td>{!!$entry_num!!}</td>
                                    @php
                                        $entry_num++;
                                    @endphp
                                    <td>{!!$wallet->id!!}</td>
                                    <td>{!!ucfirst($wallet->currency->name)!!}</td>
                                    <td>{!!$wallet->currency->numberToString($wallet->accountBalance())!!}</td>
                                    <td>{!!$wallet->walletTransactions->last()->date->format('d/m/Y').'<strong> ('.$wallet->walletTransactions->last()->date->diffForHumans().')</strong>'!!}</td>
                                    <td>
                                        <a class="btn btn-primary" onclick="redirect('/wallets/{!!$wallet->id!!}')">Detalles</a>
                                    </td>
                                </tr> 
                            @endif
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div>
            <button class="btn btn-primary" onclick="printDiv('box')">Generar PDF</button>
        </div>
    </div>
    <form>
        {!!csrf_field()!!}
    </form>
    @include('modal')
@stop