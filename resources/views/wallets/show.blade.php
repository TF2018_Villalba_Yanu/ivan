@php
    use Carbon\Carbon;
    $currency = $wallet->currency;
@endphp
@extends('adminlte::page')

@section('title_prefix', 'Detalles del wallet - ')

@section('content')
    <div class="box" id="box">
        <div class="box-header">
            <h1 id="title">
                Detalles del wallet {!!$wallet->id!!} 
                <a class="btn btn-success pull-right" data-toggle="modal" data-target="#custom-modal" onclick="inputMoneyWhitCurrency({!!$currency->id!!})"> Ingresar dinero</a>
                @if ($wallet->accountBalance() >= $wallet->currency->min_value)
                    <a class="btn btn-danger pull-right" data-toggle="modal" data-target="#custom-modal" onclick="outputMoneyWhitCurrency({!!$wallet->id!!})"> Retirar dinero</a>
                @endif
                <div id="chart">
                    <div id="wallet-transactions-areachart" style="height: 300px;"></div>
                </div>
                <form>
                    {!!csrf_field()!!}
                    <input type="hidden" id="wallet_id" value="{!!$wallet->id!!}">
                </form>
                <script>
                    walletTransactionsAreachart();
                </script>
            </h1>
        </div>
        <div class="box-body">
            <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <th width="20%">Concepto</th>
                            <th width="20%">Valor</th>
                        </thead>
                        <tbody>
                            <tr>
                                <td>ID de wallet</td>
                                <td>{!!$wallet->id!!}</td>
                            </tr>
                            <tr>
                                <td>Divisa</td>
                                <td>{!!$currency->name!!}</td>
                            </tr>
                            <tr>
                                @php
                                    $amount = bcdiv($wallet->accountBalance(),1,$currency->significant_decimals);
                                @endphp
                                <td>Dinero en cuenta</td>
                                <td>{!!$currency->numberToString($amount)!!}</td>
                            </tr>
                        </tbody>    
                    </table>
            </div>
            <br>
            <div class="table-responsive container-border">
                <h3>Historial de transacciones del wallet</h3>
                <table class="table table-paginated table-hover table-condensed">
                    <thead>
                        <th width="30px">Num</th>
                        <th>ID de transacción</th>
                        <th>Tipo</th>
                        <th>Monto</th>
                        <th>Fecha</th>
                        <th width="10%">Ver detalles</th>
                    </thead>
                    @php
                        $wallet_transactions = $wallet->walletTransactions->reverse();
                        $entry_num = 1;
                    @endphp
                    <tbody>
                        @foreach ($wallet_transactions as $wallet_transaction)
                            @if ($wallet_transaction->amount >= 0)
                            <tr class="success">
                            @else
                            <tr class="danger"> 
                            @endif
                                <td>{!!$entry_num!!}</td>
                                @php
                                    $entry_num++;
                                @endphp
                                <td>{!!$wallet_transaction->id!!}</td>
                                <td>{!!ucfirst($wallet_transaction->walletTypeOfTransaction->name)!!}</td>
                                <td>{!!$currency->numberToString($wallet_transaction->amount)!!}</td>
                                <td>{!!'El '.$wallet_transaction->date->format('d/m/Y').' a las '.$wallet_transaction->date->format('h:m').'<strong> ('.$wallet_transaction->date->diffForHumans().')</strong>'!!}</td>
                                <td>
                                    <a  onclick="redirect('/transactions/{!!$wallet_transaction->id!!}')" class="btn btn-primary">Detalles</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div>
        <button class="btn btn-primary pull-right" onclick="printDiv('box')">Generar PDF</button>
    </div>
    @include('modal')
@endsection