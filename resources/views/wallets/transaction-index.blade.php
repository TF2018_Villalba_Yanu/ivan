@php
    use App\Models\WalletTypeOfTransaction;
    $currency = $wallet_transaction->wallet->currency;
    $type_of_transaction = $wallet_transaction->walletTypeOfTransaction;
@endphp
@extends('adminlte::page')

@section('title_prefix', 'Detalles de la transacción - ')

@section('content')
    <div class="box" id="box">
        <div class="box-header">
            <h1 id="title">Detalles de la transacción {!!$wallet_transaction->id!!}</h1>
        </div>
        <div class="box-body">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <th width="20%">Concepto</th>
                        <th width="20%">Valor</th>
                    </thead>
                    <tbody>
                        <tr>
                            <td>ID de transacción</td>
                            <td>{!!$wallet_transaction->id!!}</td>
                        </tr>
                        <tr>
                            <td>Concepto</td>
                            <td>{!!ucfirst($type_of_transaction->name)!!}</td>
                        </tr>
                        <tr>
                            <td>Descripcion</td>
                            <td>{!!ucfirst($type_of_transaction->description)!!}</td>
                        </tr>
                        @if (($type_of_transaction->id == WalletTypeOfTransaction::getCreditReturn()->id) || ($type_of_transaction->id == WalletTypeOfTransaction::getInsurancePayment()->id) || ($type_of_transaction->id == WalletTypeOfTransaction::getInsurancePayment()->id))
                            <tr>
                                <td>Cuota numero</td>
                                <td>{!!$wallet_transaction->repayment->paymentPhase->payment->number().' de '.$wallet_transaction->repayment->paymentPhase->payment->credit->payments->count();!!}</td>
                            </tr>
                        @endif
                        @if (($type_of_transaction->id == WalletTypeOfTransaction::getIncome()->id) || ($type_of_transaction->id == WalletTypeOfTransaction::getWithdraw()->id))
                            <tr>
                                <td>Medio</td>
                                @php
                                    $array = array("CuotaFacil", "Rapipago", "CobroExpress", "PIM", "Transferencia bancaria");
                                @endphp
                                <td>{!!$array[array_rand($array,1)]!!}</td>
                            </tr>
                            <tr>
                                <td>Fecha de solicitud de operacion</td>
                                <td>{!!'El '.$wallet_transaction->date->format('d/m/Y').' a las '.$wallet_transaction->date->format('h:m').'<strong> ('.$wallet_transaction->date->diffForHumans().')</strong>'!!}</td>
                            </tr>
                            <tr>
                                <td>Fecha de efectivezacion</td>
                                <td>{!!'El '.$wallet_transaction->date->format('d/m/Y').' a las '.$wallet_transaction->date->format('h:m').'<strong> ('.$wallet_transaction->date->diffForHumans().')</strong>'!!}</td>
                            </tr>
                        @else
                            <tr>
                                <td>Fecha</td>
                                <td>{!!'El '.$wallet_transaction->date->format('d/m/Y').' a las '.$wallet_transaction->date->format('h:m').'<strong> ('.$wallet_transaction->date->diffForHumans().')</strong>'!!}</td>
                            </tr>
                        @endif
                        <tr>
                            <td>Monto</td>
                            <td>{!!$currency->numberToString($wallet_transaction->amount)!!}</td>
                        </tr>
                        @if ($wallet_transaction->credit() != null)
                            <tr>
                                <td>Detalles del credito</td>
                                <td><a  onclick="redirect('/credits/{!!$wallet_transaction->credit()->id!!}')" class="btn btn-primary">Crédito</a></td>
                            </tr>
                        @endif
                        @if ($wallet_transaction->financing() != null)
                            <tr>    
                                <td>Detalles del financiamiento</td>
                                <td><a  onclick="redirect('/financings/{!!$wallet_transaction->financing()->id!!}')" class="btn btn-primary">Financiamiento</a></td>
                            </tr>
                        @endif
                        @if ($wallet_transaction->exchange != null)
                            <tr>
                                <td>Detalles de le conversión</td>
                                <td><a  onclick="redirect('/exchanges/{!!$wallet_transaction->exchange->id!!}')" class="btn btn-primary">Conversión</a></td>
                            </tr>
                        @endif
                    </tbody>    
                </table>
            </div>
        </div>
    </div>
@endsection