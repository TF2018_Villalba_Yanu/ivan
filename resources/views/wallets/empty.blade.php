@php
    $user = auth()->user();
@endphp 

@extends('adminlte::page')

@section('title_prefix', 'Wallets - ')

@section('content')
    <div class="box" id="box">
        <div class="box-header">
            <h1 id="title">
                Wallets de {!!$user->name!!}
                <a class="btn btn-success pull-right" data-toggle="modal" data-target="#custom-modal" onclick="inputMoneyWhitCurrency({!!$user->currency()->id!!})"> Ingresar dinero</a>
            </h1>
        </div>
        <div class="box-body">
            <h1><i class="fa fa-info-circle fa-5x" aria-hidden="true"></i></h1>
            <h1>Aún no tienes ningún wallet con transacciones.</h1>
                <h2>Puedes ingresar dinero haciendo clic en <a data-toggle="modal" data-target="#custom-modal" onclick="inputMoneyWhitCurrency({!!$user->currency()->id!!})"> Ingresar dinero</a></h2>
        </div>
    </div>
    <form>
        {!!csrf_field()!!}
    </form>
    @include('modal')
@stop