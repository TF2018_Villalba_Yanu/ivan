@php
    use Carbon\Carbon;
@endphp
@extends('adminlte::page')

@section('title_prefix', 'Registro de auditoria - ')

@section('content')
    <div class="box" id="box">
        <div class="box-header">
            <h1 id="title">
                Registro de auditoria  
            </h1>
        </div>
        <div class="box-body">
            <div class="table-resposive container-border">
                <table class="table table-paginated table-hover table-striped table-condensed">
                    <thead>
                        <th width="30px">Num</th>
                        <th>ID de registro</th>
                        <th>Fecha</th>
                        <th>Usuario</th>
                        <th>Tabla</th>
                        <th>Operación</th>
                        <th width="10%">Ver detalles</th>
                    </thead>
                    @php
                        $entry_num = 1;
                    @endphp
                    <tbody>
                        @foreach ($audits as $audit)
                            <tr>
                                <td>{!!$entry_num!!}</td>
                                @php
                                    $entry_num++;
                                @endphp
                                <td>{!!$audit->id!!}</td>
                                <td>{!!$audit->_date!!}</td>
                                @if ($audit->user != null)
                                    <td>{!!$audit->user->email!!}</td>                                
                                @else
                                    <td></td>
                                @endif
                                <td>{!!$audit->_table!!}</td>
                                <td>{!!$audit->_action!!}</td>
                                <td>
                                    <a onclick="redirect('/audits/{!!$audit->id!!}')" class="btn btn-primary">Detalles</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div>
            <button class="btn btn-primary" onclick="printDiv('box')">Generar PDF</button>
        </div>
    </div>
@stop