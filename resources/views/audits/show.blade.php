@php
    use App\Models\User;
@endphp
@extends('adminlte::page')

@section('title_prefix', 'Detalles de registro de auditoría - ')

@section('content')
    <div class="box" id="box">
        <div class="box-header">
            <h1 id="title">
                Detalles del registro de auditoría {!!$audit->id!!}
            </h1>
        </div>
        <div class="box-body">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <th width="20%">Concepto</th>
                        <th width="20%">Valor</th>
                    </thead>
                    <tbody>
                        <tr>
                            <td>ID de registro</td>
                            <td>{!!$audit->id!!}</td>
                        </tr> 
                        <tr>
                            <td>Tipo de operacion</td>
                            <td>{!!$audit->_action!!}</td>
                        </tr>
                        <tr>
                            <td>Tabla</td>
                            <td>{!!$audit->_table!!}</td>
                        </tr>
                        <tr>
                            <td>Fecha</td>
                            <td>{!!$audit->_date!!}</td>
                        </tr>
                        <tr>
                            <td>Usuario de la base de datos</td>
                            <td>{!!$audit->_db_user!!}</td>
                        </tr>
                        @if ($audit->user != null)
                            <tr>
                                <td>Usuario del sistema</td>
                                <td>{!!'ID: '.$audit->user->id.', nombre: '.$audit->user->name!!}</td>
                            </tr>
                        @endif
                    </tbody>                    
                </table>
            </div>
            <div class="table-responsive">
                <h3>Cambios en las columnas de la tabla</h3>
                <table class="table table-striped table-bordered table-hover">
                    @php
                        $theads   = explode(', ',$audit->_data);
                        $old_datas = explode(', ',$audit->_old_data);
                        $new_datas = explode(', ',$audit->_new_data);
                    @endphp
                    <thead>
                        @foreach ($theads as $thead)
                            <th>{!!$thead!!}</th>
                        @endforeach
                    </thead>
                    <tbody>
                        @if (count($old_datas) > 1)
                            <tr class="danger">
                                @php
                                    $count = 1;
                                @endphp
                                @foreach ($old_datas as $old_data)
                                    <td>{!!$old_data!!}</td>
                                    @php
                                        $count++;
                                    @endphp
                                @endforeach
                                @while ($count <= count($theads))
                                    <td>----</td>
                                    @php
                                        $count++;
                                    @endphp
                                @endwhile
                            </tr>
                        @endif
                        @if (count($new_datas) > 1)
                            <tr class="success">
                                @php
                                    $count = 1;
                                @endphp
                                @foreach ($new_datas as $new_data)
                                    <td>{!!$new_data!!}</td>
                                    @php
                                        $count++;
                                    @endphp
                                @endforeach
                                @while ($count <= count($theads))
                                    <td>----</td>
                                    @php
                                        $count++;
                                    @endphp
                                @endwhile
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
        <div>
            <button class="btn btn-primary" onclick="printDiv('box')">Generar PDF</button>
        </div>
    </div>    
@endsection