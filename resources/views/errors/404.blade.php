
    @extends('adminlte::page')

    @section('title_prefix', 'Pagina no encontrada - ')

    @section('content')
        <div class="box" id="box">
            <div class="box-header">
                <h1>Error 404</h1>
            </div>
            <div class="box-body">
                <div class="error-page">
                    <h2 class="headline text-yellow"> 404</h2>
                    <div class="error-content">
                      <h3><i class="fa fa-warning text-yellow"></i> Oops! No se encontró la página que buscas.</h3>
                    </div>
                  </div>
            </div>
        </div>
    @endsection 


