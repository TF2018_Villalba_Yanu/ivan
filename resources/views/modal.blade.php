<div class="modal fade" id="custom-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content" id="modal">
          <div class="modal-header" id="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <div class="modal-body" id="modal-body">
          </div>
          <div class="modal-footer" id="modal-footer">
          </div>
        </div>
      </div>
</div>