@php
    $interest  = $payment_plan->interest;
    $insurance = $payment_plan->insurance;
    $currency  = $payment_plan->currency;
@endphp
@extends('adminlte::page')

@section('title_prefix', 'Detalles del plan de pago - ')

@section('content')
    <div class="box" id="box">
        <div class="box-header">
            <h1>
                Detalles del plan de pago {!!$payment_plan->id!!}
                <a onclick="redirect('/payment-plans/{!!$payment_plan->id!!}/edit')" class="btn btn-primary pull-right"><i class="fa fa-pencil" aria-hidden="true"></i> Editar</a>
            </h1>
        </div>
        <div class="box-body">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <th width="20%">Concepto</th>
                        <th width="20%">Valor</th>
                    </thead>
                    <tbody>
                        <tr>
                            <td>ID del plan de pago</td>
                            <td>{!!$payment_plan->id!!}</td>
                        </tr>
                        <tr>
                            <td>Numero de cuotas</td>
                            <td>{!!$payment_plan->payments_number!!}</td>                        
                        </tr>
                        <tr>
                            <td>Vencimiento entre cuotas</td>
                            <td>Cada {!!$payment_plan->dueDatesInterval->toCarbonInterval()->diffForHumans()!!}</td>                        
                        </tr>
                        <tr>
                            <td>Divisa</td>
                            <td>{!!ucfirst($currency->name)!!}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="table-responsive">
                <h3>Detalles del interés</h3>
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <th width="20%">Concepto</th>
                        <th width="20%">Valor</th>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Porcentaje mínimo</td>
                            <td>{!!$currency->percentage($interest->min_percentage)!!}</td>
                        </tr>        
                        <tr>
                            <td>Porcentaje inicial</td>
                            <td>{!!$currency->percentage($interest->initial_percentage)!!}</td>
                        </tr>
                        <tr>
                            <td>Porcentaje máximo</td>
                            <td>{!!$currency->percentage($interest->max_percentage)!!}</td>
                        </tr>
                        <tr>
                            <td>Porcentaje de comisión*</td>
                            <td>{!!$currency->percentage($interest->commission_percentage)!!}</td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="2">
                                <p>(*): Porcentaje del interés total. Por ejemplo, si el porcentaje de interés del crédito es del <strong>{!!$currency->percentage(0.7)!!}</strong> y el porcentaje de comisión es de <strong>{!!$currency->percentage(0.2)!!}</strong>; la comisión será del <strong>{!!$currency->percentage(0.14)!!}</strong> (0.7 * 0.2 = 0.14)</p>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <div class="table-responsive">
                <h3>Detalles del seguro</h3>
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <th width="20%">Concepto</th>
                        <th width="20%">Valor</th>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Porcentaje mínimo</td>
                            <td>{!!$currency->percentage($insurance->min_percentage)!!}</td>
                        </tr>        
                        <tr>
                            <td>Porcentaje inicial</td>
                            <td>{!!$currency->percentage($insurance->initial_percentage)!!}</td>
                        </tr>
                        <tr>
                            <td>Porcentaje máximo</td>
                            <td>{!!$currency->percentage($insurance->max_percentage)!!}</td>
                        </tr>
                        <tr>
                            <td>Porcentaje deudor**</td>
                            <td>{!!$currency->percentage($insurance->debtor_percentage)!!}</td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="2">
                                <p>(**): Porcentaje del seguro que abonará el deudor. Por ejemplo, si el porcentaje de seguro del crédito es del <strong>{!!$currency->percentage(0.4)!!}</strong> y el porcentaje de deudor es de <strong>{!!$currency->percentage(0.65)!!}</strong>; el deudor abonará el <strong>{!!$currency->percentage(0.14)!!}</strong> del seguro y cada acreedor que asegure el financiamiento abonará <strong>{!!$currency->percentage(0.14)!!}</strong> en seguro (0.7 * 0.2 = 0.14)</p>
                                <p>Si escribe <strong>100 %</strong>, el deudor abonará todo el seguro; y si escribe <strong>0 %</strong>, los acreedores abonarán la totalidad del seguro</p>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
        <div>
            <button class="btn btn-primary" onclick="printDiv('box')">Generar PDF</button>
        </div>
    </div>
    @include('modal')
@endsection