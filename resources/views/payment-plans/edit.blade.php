@php
    use App\Models\Currency;
    $interest  = $payment_plan->interest;
    $insurance = $payment_plan->insurance;
    $currencies  = Currency::all();
@endphp
@extends('adminlte::page')

@section('title_prefix', 'Editar plan de pago - ')

@section('content')
    <div class="box" id="box">
        <div class="box-header">
            <h1>
                Editar plan de pago 
            </h1>
        </div>
        <div class="box-body">
            <form action="/payment-plans/{!!$payment_plan->id!!}" method="post">
                {!!csrf_field()!!}
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <th width="20%">Concepto</th>
                            <th width="20%">Valor</th>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Numero de cuotas (*)</td>
                                <td>
                                    <input required type="number" class="form-control" value="{!!$payment_plan->payments_number!!}" name="payments_number" id="payments_number" min="1" max="6000" step="1">
                                </td>                        
                            </tr>
                            <tr>
                                <td>Vencimiento entre cuotas en dias (*) (**) </td>
                                <td>
                                    <input required type="number" class="form-control" value="{!!$payment_plan->dueDatesInterval->toDays()!!}" name="due_days" id="due_days" min="1" max="3650">
                                </td>                        
                            </tr>
                            <tr>
                                <td>Divisa</td>
                                <td>
                                    <select required class="form-control" name="currency_id" id="currency_id">
                                        <option value="{!!$payment_plan->currency->id!!}" selected>{!!ucfirst($payment_plan->currency->name)!!}</option>
                                        @foreach ($currencies as $currency)
                                            @if ($currency->id != $payment_plan->currency->id)
                                                <option value="{!!$currency->id!!}">{!!ucfirst($currency->name)!!}</option>                                                
                                            @endif
                                        @endforeach
                                    </select>
                                </td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="2">
                                    <p>(*): Campo requerido.</p>
                                    <p>(**): Vencimiento en dias, si escribe 30, se traduce a 1 mes, si escribe 93, se traduce a 3 meses y 3 dias.</p>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <div class="table-responsive">
                    <h3>Detalles del interés</h3>
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <th width="20%">Concepto</th>
                            <th width="20%">Valor</th>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Porcentaje mínimo (*)</td>
                                <td>
                                    <input required type="number" class="form-control" value="{!!$interest->min_percentage!!}" name="interest_min_percentage" id="interest_min_percentage" min="0.001" max="5" step="0.0001">
                                </td>
                            </tr>        
                            <tr>
                                <td>Porcentaje inicial (*)</td>
                                <td>
                                    <input required type="number" class="form-control" value="{!!$interest->initial_percentage!!}" name="interest_initial_percentage" id="interest_initial_percentage" min="0.001" max="5" step="0.0001">
                                </td>
                            </tr>
                            <tr>
                                <td>Porcentaje máximo (*)</td>
                                <td>
                                    <input required type="number" class="form-control" value="{!!$interest->max_percentage!!}" name="interest_max_percentage" id="interest_max_percentage" min="0.001" max="5" step="0.0001">
                                </td>
                            </tr>
                            <tr>
                                <td>Porcentaje de comisión (*)(***)</td>
                                <td>
                                    <input required type="number" class="form-control" value="{!!$interest->commission_percentage!!}" name="interest_commission_percentage" id="interest_commission_percentage" min="0.001" max="1" step="0.0001">
                                </td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="2">
                                    <p>(*): Campo requerido.</p>
                                    <p>(***): Porcentaje del interés total. Por ejemplo, si el porcentaje de interés del crédito es del <strong>{!!0.7!!}</strong> y el porcentaje de comisión es de <strong>{!!0.2!!}</strong>; la comisión será del <strong>{!!0.14!!}</strong> (0.7 * 0.2 = 0.14).</p>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <div class="table-responsive">
                    <h3>Detalles del seguro</h3>
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <th width="20%">Concepto</th>
                            <th width="20%">Valor</th>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Porcentaje mínimo (*)</td>
                                <td>
                                    <input required type="number" class="form-control" value="{!!$insurance->min_percentage!!}" name="insurance_min_percentage" id="insurance_min_percentage" min="0.001" max="5" step="0.0001">
                                </td>
                            </tr>        
                            <tr>
                                <td>Porcentaje inicial (*)</td>
                                <td>
                                    <input required type="number" class="form-control" value="{!!$insurance->initial_percentage!!}" name="insurance_initial_percentage" id="insurance_initial_percentage" min="0.001" max="5" step="0.0001">
                                </td>
                            </tr>
                            <tr>
                                <td>Porcentaje máximo (*)</td>
                                <td>
                                    <input required type="number" class="form-control" value="{!!$insurance->max_percentage!!}" name="insurance_max_percentage" id="insurance_max_percentage" min="0.001" max="5" step="0.0001">
                                </td>
                            </tr>
                            <tr>
                                <td>Porcentaje deudor (*)(****)</td>
                                <td>
                                    <input required type="number" class="form-control" value="{!!$insurance->debtor_percentage!!}" name="insurance_debtor_percentage" id="insurance_debtor_percentage" min="0.001" max="1" step="0.0001">
                                </td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="2">
                                    <p>(*): Campo requerido.</p>
                                    <p>(****): Porcentaje del seguro que abonará el deudor. Por ejemplo, si el porcentaje de seguro del crédito es del <strong>{!!0.4!!}</strong> y el porcentaje de deudor es de <strong>{!!0.65!!}</strong>; el deudor abonará el <strong>{!!0.14!!}</strong> del seguro y cada acreedor que asegure el financiamiento abonará <strong>{!!0.14!!}</strong> en seguro (0.7 * 0.2 = 0.14). Si escribe <strong>1</strong>, el deudor abonará todo el seguro; y si escribe <strong>0</strong>, los acreedores abonarán la totalidad del seguro.</p>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <button type="submit" class="btn btn-success">Enviar</button>
            </form>
        </div>
    </div>
    @include('styles.supr_arrows_in_text_fields')
    @include('modal')
@endsection