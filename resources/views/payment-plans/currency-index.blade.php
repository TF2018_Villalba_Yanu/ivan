@extends('adminlte::page')

@section('title_prefix', 'Planes de pagos en '.strtolower($currency->name).' - ')

@section('content')
    <div class="box" id="box">
        <div class="box-header">
            <h1 id="title">
                Planes de pagos en {!!strtolower($currency->name)!!}  
                <a class="btn btn-success pull-right" onclick="redirect('/payment-plans/create')">Nuevo</a>
            </h1>                
        </div>
        <div class="box-body">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <th width="20%">Concepto</th>
                        <th width="20%">Valor</th>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Planes de pagos</td>
                            @if ($payment_plans->count() == 1)
                                <td>1 plan de pago</td>
                            @else
                                <td>{!!$payment_plans->count()!!} planes de pagos</td>
                            @endif
                        </tr>
                    </tbody>
                </table>
            </div>
            <br>
            <div class="table-responsive container-border">
                <table class="table table-paginated table-striped table-hover">
                    <thead>
                        <th width="30px">Num</th>
                        <th>Numero de pagos</th>
                        <th>Periodo entre pagos</th>
                        <th>Interes</th>
                        <th>Seguro</th>
                        <th>Ver detalles</th>
                    </thead>
                    @php
                        $entry_num = 1;
                    @endphp
                    <tbody>
                        @foreach ($payment_plans as $payment_plan)
                            <tr>
                                <td>{!!$entry_num!!}</td>
                                @php
                                    $entry_num++;
                                @endphp
                                @if ($payment_plan->payments_number == 1)
                                    <td>1 pago</td>
                                @else
                                    <td>{!!$payment_plan->payments_number!!} pagos</td>
                                @endif
                                <td>{!!ucfirst($payment_plan->dueDatesInterval->toCarbonInterval()->diffForHumans())!!}</td>
                                <td>{!!$currency->percentage($payment_plan->interest->initial_percentage)!!}</td>
                                <td>{!!$currency->percentage($payment_plan->insurance->initial_percentage)!!}</td>
                                <td>
                                        <a  onclick="redirect('/payment-plans/{!!$payment_plan->id!!}')" class="btn btn-primary">Detalles</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div>
            <button class="btn btn-primary" onclick="printDiv('box')">Generar PDF</button>
        </div>
    </div>
    <form>
        {!!csrf_field()!!}
    </form>
    @include('modal')
@stop