@extends('adminlte::page')

@section('title_prefix', 'Planes de pagos en '.strtolower($currency->name).' - ')

@section('content')
    <div class="box" id="box">
        <div class="box-header">
            <h1 id="title">
                Planes de pagos en {!!strtolower($currency->name)!!}  
                <a class="btn btn-success pull-right" onclick="redirect('/payment-plans/create')>Nuevo</a>
            </h1>                
        </div>
        <div class="box-body">
            <h1><i class="fa fa-info-circle fa-5x" aria-hidden="true"></i></h1>
            <h1>Aún no tienes ningún plan de pago para esta divisa.</h1>
            <h2>Puedes crear uno haciendo clic en  <a onclick="redirect('/payment-plans/create')>Nuevo</a></h2>
        </div>
    </div>
    <form>
        {!!csrf_field()!!}
    </form>
    @include('modal')
@stop