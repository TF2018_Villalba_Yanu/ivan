@php
    $user = auth()->user();
@endphp 

@extends('adminlte::page')

@section('title_prefix', 'Planes de pagos - ')

@section('content')
<div class="box" id="box">
    <div class="box-header">
        <h1 id="title">
           Planes de pagos
           <a class="btn btn-success pull-right" onclick="redirect('/payment-plans/create')">Nuevo</a>
        </h1>                 
    </div>
    <div class="box-body">
        <div class="table-resposive container-border">
            <table class="table table-paginated table-hover table-striped">
                <thead>
                    <th width="30px">Num</th>
                    <th>Divisa</th>
                    <th>Planes de pagos</th>
                    <th width="10%">Ver detalles</th>
                </thead>
                @php
                    $entry_num = 1;
                @endphp
                <tbody>
                    @foreach ($currencies as $currency)
                        <tr>
                            <td>{!!$entry_num!!}</td>
                            @php
                                $entry_num++;
                            @endphp
                            <td>Planes en <strong>{!!strtolower($currency->name)!!}</strong></td>
                            @if ($currency->paymentPlans->count() == 0)
                                <td>Sin planes de pagos</td>
                            @else
                                @if ($currency->paymentPlans->count() == 1)
                                    <td>1 plan de pago</td>
                                @else
                                    <td>{!!$currency->paymentPlans->count()!!} planes de pagos</td>
                                @endif
                            @endif
                            <td>
                                <a  onclick="redirect('/payment-plans/currency={!!$currency->id!!}')" class="btn btn-primary">Detalles</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <div>
        <button class="btn btn-primary" onclick="printDiv('box')">Generar PDF</button>
    </div>
</div>
@stop