@php
    use Illuminate\Database\Eloquent\Collection;
    $wallets_with_balance = new Collection();
    $user = auth()->user();
    foreach($wallets as $wallet){
        if($wallet->accountBalance() >= $wallet->currency->min_value){
            $wallets_with_balance->add($wallet);
        }
    }
    $exchanges = $exchanges->reverse();
@endphp 

@extends('adminlte::page')

@section('title_prefix', 'Conversiones de divisas - ')

@section('content')
    <div class="box" id="box">
        <div class="box-header">
            <h1 id="title">
               Conversiones de divisas de {!!$user->name!!}
               @if ($wallets_with_balance->count() > 0)
                    @php
                        if ($wallets_with_balance->pluck('currency_id')->contains($user->currency()->id)) {
                            $wallet_id = $wallets_with_balance->where('currency_id',$user->currency()->id)->first()->id;
                        } else {
                            $wallet_id = $wallets_with_balance->random()->id;
                        }
                    @endphp
                    <a class="btn btn-success pull-right" data-toggle="modal" data-target="#custom-modal" onclick="exchangeCurrency({!!$wallet_id!!})">Convertir</a>
                @endif
            </h1>
        </div>
        <div class="box-body">
            <div class="table-resposive container-border">
            <table class="table table-paginated table-hover table-striped">
                <thead>
                    <th width="30px">Num</th>
                    <th>ID transansacción</th>
                    <th>Divisas</th>
                    <th>Monto de salida</th>
                    <th>Monto de ingreso</th>
                    <th>Taza de conversión</th>
                    <th>Fecha</th>
                    <th width="10%">Ver detalles</th>
                </thead>
                @php
                    $entry_num = 1;
                @endphp
                <tbody>
                    @foreach ($exchanges as $exchange)
                        <tr>
                            <td>{!!$entry_num!!}</td>
                            @php
                                $entry_num++;
                            @endphp
                            <td>{!!$exchange->id!!}</td>
                            <td>{!!$exchange->fromWallet->currency->iso_code.'/'.$exchange->toWallet->currency->iso_code!!}</td>
                            <td>{!!$exchange->fromWallet->currency->numberToString($exchange->convert(0))!!}</td>
                            <td>{!!$exchange->toWallet->currency->numberToString($exchange->convert(1))!!}</td>
                            <td>1 {!!$exchange->fromWallet->currency->iso_code.' = '.round($exchange->exchangeRate->rate,5).' '.$exchange->toWallet->currency->iso_code!!}</td>
                            <td>{!!'El '.$exchange->date->format('d/m/Y').' a las '.$exchange->date->format('h:m').'<strong> ('.$exchange->date->diffForHumans().')</strong>'!!}</td>
                            <td>
                                <a  onclick="redirect('/exchanges/{!!$exchange->id!!}')" class="btn btn-primary">Detalles</a>
                            </td>
                        </tr> 
                    @endforeach
                </tbody>
            </table>
        </div>
        <div>
            <button class="btn btn-primary" onclick="printDiv('box')">Generar PDF</button>
        </div>    
    </div>
    <form>
        {!!csrf_field()!!}
    </form>
    @include('modal')
@stop