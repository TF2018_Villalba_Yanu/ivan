@php
    use Illuminate\Database\Eloquent\Collection;
    $wallets_with_balance = new Collection();
    $user = auth()->user();
    foreach($wallets as $wallet){
        if($wallet->accountBalance() >= $wallet->currency->min_value){
            $wallets_with_balance->add($wallet);
        }
    }
@endphp 

@extends('adminlte::page')

@section('title_prefix', 'Conversiones de divisas - ')

@section('content')
    <div class="box" id="box">
        <div class="box-header">
            <h1 id="title">
               Conversiones de divisas de {!!$user->name!!}
               @if ($wallets_with_balance->count() > 0)
                    @php
                        if ($wallets_with_balance->pluck('currency_id')->contains($user->currency()->id)) {
                            $wallet_id = $wallets_with_balance->where('currency_id',$user->currency()->id)->first()->id;
                        } else {
                            $wallet_id = $wallets_with_balance->random()->id;
                        }
                    @endphp
                    <a class="btn btn-success pull-right" data-toggle="modal" data-target="#custom-modal" onclick="exchangeCurrency({!!$wallet_id!!})">Convertir</a>
                @endif
            </h1>
        </div>
        <div class="box-body">
            <h1><i class="fa fa-info-circle fa-5x" aria-hidden="true"></i></h1>
            <h1>Aún no tienes ninguna conversión de divisas.</h1>
            @if ($wallets_with_balance->count() > 0)
                @php
                    if ($wallets_with_balance->pluck('currency_id')->contains($user->currency()->id)) {
                        $wallet_id = $wallets_with_balance->where('currency_id',$user->currency()->id)->first()->id;
                    } else {
                        $wallet_id = $wallets_with_balance->random()->id;
                    }
                @endphp
                <h2>Puedes convertir entre diferentes divisas haciendo clic en <a data-toggle="modal" data-target="#custom-modal" onclick="exchangeCurrency({!!$wallet_id!!})">Convertir</a></h2>
            @else
                <h2>Para realizar operaciones de conversión, primero tienes que ingresar dinero en <a onclick="redirect('/wallets')">Wallets</a></h2>
            @endif
        </div>
    </div>
    <form>
        {!!csrf_field()!!}
    </form>
    @include('modal')
@stop