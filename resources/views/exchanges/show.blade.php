@php

@endphp
@extends('adminlte::page')

@section('title_prefix', 'Detalles de la conversión - ')

@section('content')
    <div class="box" id="box">
        <div class="box-header">
            <h1 id="title">
                Detalles de la conversión {!!$exchange->id!!}
            </h1>
            <div id="chart">
                <div id="exchange-details-areachart" style="height: 300px;"></div>
            </div>
            <form>
                <input type="hidden"  id="exchange_id" value="{!!$exchange->id!!}">
                <input type="hidden"  id="from_currency_id" value="{!!$exchange->exchangeRate->fromCurrency->id!!}">
                <input type="hidden"  id="to_currency_id" value="{!!$exchange->exchangeRate->toCurrency->id!!}">
            </form> 
            <script>
                exchangeDetailsAreachart();
            </script>
        </div>
        <div class="box-body">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <th width="20%">Concepto</th>
                        <th width="20%">Valor</th>
                    </thead>
                    <tbody>
                        <tr>
                            <td>ID de conversión</td>
                            <td>{!!$exchange->id!!}</td>
                        </tr>
                        <tr>
                            <td>Monto de salida del wallet de {!!$exchange->fromWallet->currency->name!!}</td>
                            <td>{!!$exchange->toWallet->currency->numberToString($exchange->convert(0))!!}</td>
                        </tr> 
                        <tr>
                            <td>Monto de entrada del wallet de {!!$exchange->toWallet->currency->name!!}</td>
                            <td>{!!$exchange->toWallet->currency->numberToString($exchange->convert(1))!!}</td>
                        </tr> 
                        <tr>
                            <td>tasa de cambio de {!!$exchange->fromWallet->currency->iso_code.' a '.$exchange->toWallet->currency->iso_code!!}</td>
                            <td>{!!$exchange->toWallet->currency->numberToString($exchange->exchangeRate->rate)!!}</td>
                        </tr> 
                        <tr>
                            <td>Fecha de la conversión</td>
                            <td>{!!'El '.$exchange->date->format('d/m/Y').' a las '.$exchange->date->format('h:m').'<strong> ('.$exchange->date->diffForHumans().')</strong>'!!}</td>
                        </tr>
                    </tbody>                    
                </table>
            </div>
        </div>
        <div>
            <button class="btn btn-primary" onclick="printDiv('box')">Generar PDF</button>
        </div>    
    </div>
@endsection