@extends('adminlte::page')

@section('title_prefix', 'Home - ')

@section('content_header')
    <h1>Bienvenido!</h1>
@stop

@section('content')
    <h3>Estas logueado con el usuario <strong>{!!auth()->user()->email!!}</strong></h3>
@stop