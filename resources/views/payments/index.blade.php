@php
    use Illuminate\Database\Eloquent\Collection;
    $currency = $credit->currency();
    use Carbon\Carbon;
    //$credit_phases = $credit->creditPhases->reverse();
    $last_payment_date = null;
    $payments = $payments->sortByDesc(function($payment,$key){
        return $payment->id;
    });
    $wallets = auth()->user()->wallets;
    $wallets_with_balance = new Collection();
    foreach($wallets as $wallet){
        if($wallet->accountBalance() >= $wallet->currency->min_value){
            $wallets_with_balance->add($wallet);
        }
    }
@endphp
@extends('adminlte::page')

@section('title_prefix', 'Detalles de las cuotas - ')

@section('content')
    <div class="box" id="box">
        <div class="box-header">
            <h1 id="title">
                Detalles de las cuotas
                @if ($credit->unpayedAmount() >= $currency->min_value)
                    @if ($wallets_with_balance->count() > 0)
                    @php
                    if ($wallets_with_balance->pluck('currency_id')->contains($credit->currency()->id)) {
                        $wallet_id = $wallets_with_balance->where('currency_id',$credit->currency()->id)->first()->id;
                    } else {
                        $wallet_id = $wallets_with_balance->random()->id;
                    }
                @endphp
                <a class="btn btn-success pull-right" data-toggle="modal" data-target="#custom-modal" onclick="payWithCredit({!!$credit->id.','.$wallet_id!!})">Realizar pago</a>
                    @endif
                @endif
                @php
                    $last_payment_phase_date = new Carbon($payments->first()->paymentPhases->last()->date->toDateString());
                    foreach($payments as $payment){
                        $to_compare = new Carbon($payment->lastPhaseGeneratedOrPayOrPayedOrDueDate()->toDateString());
                        if ($last_payment_phase_date < $to_compare) {
                            $last_payment_phase_date = $to_compare;
                        }
                    }
                    //dd($payments->last(),$payments->last()->paymentPhases->last());
                    $rows = abs($payments->last()->paymentPhases->first()->date->format('Y') - $last_payment_phase_date->format('Y')) + 1;
                @endphp
                <div id="chart">
                    <div id="payments-index-calendarchart" style="height: {!!$rows * 175!!}px;"></div>
                </div>
                <form>
                    <input type="hidden"  id="credit_id" value="{!!$credit->id!!}">
                </form>
                <script>
                    paymentsIndexCalendarChart();
                </script>
            </h1>                
        </div>
        <div class="box-body">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <th width="20%">Concepto</th>
                        <th width="20%">Valor</th>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Detalles de cuotas</td>
                            @if ($credit->payments_number == 1)
                                <td>1 cuota de {!!$currency->numberToString($payments->first()->paymentPhases->first()->amount)!!}</td>
                            @else
                                <td>{!!$credit->payments_number.' cuotas de '.$currency->numberToString($payments->first()->paymentPhases->first()->amount).' cada uno'!!}</td>
                            @endif
                        </tr>
                        <tr>
                            <td>ID del credito</td>
                            <td>{!!$credit->id!!}</td>
                        </tr>
                        @if ($credit->unpayedAmount() >= $currency->min_value)
                            <tr>
                                @php
                                    $left_payments = '';
                                    if($credit->firstUnpayedPayment() != null){
                                        $left_payments = $credit->payments->count() - $credit->firstUnpayedPayment()->number() + 1;
                                    }
                                    if((is_numeric($left_payments)) && ($left_payments == 1)){
                                        $left_payments = '1 cuota';
                                    }
                                    if((is_numeric($left_payments)) && ($left_payments > 1)){
                                        $left_payments = $left_payments.' cuotas';
                                    }
                                @endphp
                                <td>Saldo restante</td>
                                <td>{!!$currency->numberToString($credit->unpayedAmount()).' ('.$left_payments.')'!!}</td>
                            </tr>
                        @else
                            <tr>
                                <td>Fecha ultimo pago</td>
                                <td>{!!ucfirst($credit->lastCreditPhase()->date->diffForHumans())!!}</td>
                            </tr>
                        @endif
                    </tbody>                 
                </table>
            </div>
                @if ($credit->firstUnpayedPayment() != null)
                    <br>
                    <div class="table-responsive container-border">
                        <h3>Cuotas pendientes</h3>
                        <table class="table table-paginated table-striped table-hover">
                            <thead>
                                <th width="30px">Num</th>
                                <th>ID de cuota</th>
                                <th>Cuota numero</th>
                                <th>Monto restante</th>
                                <th>Vencimiento</th>
                                <th width="10%">Ver detalles</th>
                            </thead>
                            @php
                                $entry_num = 1;
                            @endphp
                            <tbody>
                                @foreach ($payments as $payment)
                                    @if ($payment->unpayedAmount() >= $currency->min_value)
                                        @if ($payment->retainsMargin())
                                           <tr class="danger"> 
                                        @else
                                            @if (Carbon::now()->diffInDays($payment->due_date) <= 3) {
                                                <tr class="warning">
                                            @else 
                                                <tr class="success">
                                            @endif
                                        @endif
                                            <td>{!!$entry_num!!}</td>
                                            @php
                                                $entry_num++;
                                            @endphp
                                            <td>{!!$payment->id!!}</td>
                                            <td>{!!$payment->number().' de '.$credit->payments->count()!!}</td>
                                            <td>{!!$currency->numberToString($payment->unpayedAmount())!!}</td>
                                            @php
                                                $due_date = $payment->due_date->diffInDays(Carbon::now());
                                                $on_string = '';
                                                if ($due_date == 1){
                                                    if (Carbon::now() < $payment->due_date)
                                                        $on_string = 'mañana';
                                                    if (Carbon::now() > $payment->due_date)
                                                        $on_string = 'ayer';
                                                }
                                                if ($due_date == 0)
                                                    $on_string = 'hoy';
                                                if($due_date > 1)
                                                    $on_string = $payment->due_date->diffForHumans();
                                            @endphp
                                            <td>{!!'El '.$payment->due_date->format('d/m/Y').'<strong> ('.$on_string.')</strong>'!!}</td>
                                            <td><a  onclick="redirect('/payments/{!!$payment->id!!}')" class="btn btn-primary">Detalles</a></td>
                                        </tr>
                                    @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                @endif
                @if (($credit->firstUnpayedPayment() == null) || ($credit->firstUnpayedPayment()->id != $credit->payments->first()->id))
                <br>
                <div class="table-responsive container-border">
                    <h3>Cuotas pagadas</h3>
                    <table class="table table-paginated table-striped table-hover table-condensed">
                        <thead>
                            <th width="30px">Num</th>
                            <th>ID de cuota</th>
                            <th width="30%">Cuota numero</th>
                            <th width="30%">Monto</th>
                            <th width="30%">Pagado</th>
                            <th width="10%">Ver detalles</th>
                        </thead>
                        @php
                            $entry_num = 1;
                        @endphp
                        <tbody>
                            @foreach ($payments as $payment)
                                @if ($payment->unpayedAmount() < $currency->min_value)
                                    <tr>
                                        <td>{!!$entry_num!!}</td>
                                        @php
                                            $entry_num++;
                                        @endphp
                                        <td>{!!$payment->id!!}</td>
                                        <td>{!!$payment->number().' de '.$credit->payments->count()!!}</td>
                                        <td>{!!$currency->numberToString($payment->amount())!!}</td>
                                        <td>{!!'El '.$payment->lastPaymentPhase()->date->format('d/m/Y').' a las '.$payment->lastPaymentPhase()->date->format('h:m').'<strong> ('.$payment->lastPaymentPhase()->date->diffForHumans().')</strong>'!!}</td>
                                        <td><a  onclick="redirect('/payments/{!!$payment->id!!}')" class="btn btn-primary">Detalles</a></td>
                                    </tr>
                                @endif
                            @endforeach
                        </tbody>
                    </table>
                </div>
                @endif
            </div>
        </div>
        <div>
            <button class="btn btn-primary" onclick="printDiv('box')">Generar PDF</button>
        </div>
    </div>
    @include('modal')   
@endsection