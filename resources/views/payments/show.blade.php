@php
    use Carbon\Carbon;
    use App\Models\PaymentStage;
    use Illuminate\Database\Eloquent\Collection;
    $currency = $payment->credit->currency();
    $payment_phases = $payment->paymentPhases->reverse();
    $wallets = auth()->user()->wallets;
    $wallets_with_balance = new Collection();
    foreach($wallets as $wallet){
        if($wallet->accountBalance() >= $wallet->currency->min_value){
            $wallets_with_balance->add($wallet);
        }
    }
@endphp
@extends('adminlte::page')

@section('title_prefix', 'Detalles de la cuota - ')

@section('content')
    <div class="box" id="box">
        <div class="box-header">
            <h1 id="title">
                Detalles de la cuota {!!$payment->id!!}
                @if (($payment->unpayedAmount() >= $currency->min_value)&&($payment->id == $payment->credit->firstUnpayedPayment()->id))
                    @if ($wallets_with_balance->count() > 0)
                        @php
                            if ($wallets_with_balance->pluck('currency_id')->contains($payment->credit->currency()->id)) {
                                $wallet_id = $wallets_with_balance->where('currency_id',$payment->credit->currency()->id)->first()->id;
                            } else {
                                $wallet_id = $wallets_with_balance->random()->id;
                            }
                        @endphp
                        <a class="btn btn-success pull-right" data-toggle="modal" data-target="#custom-modal" onclick="payWithCredit({!!$payment->credit->id.','.$wallet_id!!})">Realizar pago</a>
                    @endif    
                @endif
                @php
                    $rows = abs($payment->paymentPhases->first()->date->format('Y') - $payment->paymentPhases->last()->date->format('Y')) + 1;
                @endphp
                <div id="chart">
                    <div id="payment-phases-calendarchart" style="height: {!!$rows * 175!!}px;"></div>
                </div>
                <form>
                    <input type="hidden"  id="payment_id" value="{!!$payment->id!!}">
                </form>
                <script>
                    paymentPhasesCalendarchart();
                </script>
            </h1>
        </div>
        <div class="box-body">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <th width="20%">Concepto</th>
                        <th width="20%">Valor</th>
                    </thead>
                    <tbody>
                        <tr>
                            <td>ID de la cuota</td>
                            <td>{!!$payment->id!!}</td>
                        </tr>
                        <tr>
                            <td>Denominacion</td>
                            <td>{!!$currency->iso_code!!}</td>
                        </tr>
                        <tr>
                            <td>Monto de la cuota</td>
                            <td>{!!$currency->numberToString($payment->amount())!!}</td>
                        </tr>
                        @if ($payment->unpayedAmount() >= $currency->min_value)
                            <tr>
                                <td>Saldo restante</td>
                                <td>{!!$currency->numberToString($payment->unpayedAmount())!!}</td>
                            </tr>
                            <tr>
                                @php
                                    $due_date = $payment->due_date->diffInDays(Carbon::now());
                                    $on_string = '';
                                    if ($due_date == 1){
                                        if (Carbon::now() < $payment->due_date)
                                            $on_string = 'mañana';
                                        if (Carbon::now() > $payment->due_date)
                                            $on_string = 'ayer';
                                    }
                                    if ($due_date == 0)
                                        $on_string = 'hoy';
                                    if($due_date > 1)
                                        $on_string = $payment->due_date->diffForHumans();
                                @endphp
                                <td>Vencimiento</td>
                                <td>{!!'El '.$payment->due_date->format('d/m/Y').'<strong> ('.$on_string.')</strong>'!!}</td>
                            </tr>
                        @else
                            <tr>
                                @php
                                    $payed_date = $payment->lastPaymentPhase()->date->diffInDays(Carbon::now());
                                    $on_string = '';
                                    if ($payed_date == 1){
                                        if (Carbon::now() < $payment->lastPaymentPhase()->date)
                                            $on_string = 'mañana';
                                        if (Carbon::now() > $payment->lastPaymentPhase()->date)
                                            $on_string = 'ayer';
                                    }
                                    if ($payed_date == 0)
                                        $on_string = 'hoy';
                                    if($payed_date > 1)
                                        $on_string = $payment->lastPaymentPhase()->date->diffForHumans();
                                @endphp
                                <td>Pagado</td> 
                                <td>{!!'El '.$payment->lastPaymentPhase()->date->format('d/m/Y').' a las '.$payment->lastPaymentPhase()->date->format('h:m').'<strong> ('.$on_string.')</strong>'!!}</td>
                            </tr>
                        @endif
                        
                    </tbody>
                </table>
            </div>
            <br>
            <div class="table-responsive container-border">
                <h3>Historial de estados de la cuota</h3>
                <table class="table table-paginated table-striped table-hover">
                    <thead>
                        <th width="30px">Num</th>
                        <th>ID de estado</th>
                        <th>Estado</th>
                        <th>Monto</th>
                        <th>Fecha</th>
                    </thead>
                    @php
                        $interest = 0;
                        $last_interest_id = 0;
                        $last_date = '';
                        $entry_num = 1;
                    @endphp
                    @foreach ($payment_phases as $payment_phase)
                        @if (($payment_phase->paymentStage->id != PaymentStage::getInsurancePay()->id) && ($payment_phase->paymentStage->id != PaymentStage::getAddInterest()->id))
                            @if ($interest > 0)
                                <tr class="danger">
                                    <td>{!!$entry_num!!}</td>
                                    @php
                                        $entry_num++;
                                    @endphp
                                    <td>{!!$last_interest_id!!}</td>
                                    <td>{!!PaymentStage::getAddInterest()->name!!}</td>
                                    <td>{!!$currency->numberToString($interest)!!}</td>
                                    <td>{!!'El '. $last_date->format('d/m/Y').' a las '. $last_date->format('h:m').'<strong> ('. $last_date->diffForHumans().')</strong>'!!}</td>
                                </tr>
                                @php
                                    $interest = 0;
                                    $last_interest_id = 0;
                                    $last_date = ''; 
                                @endphp
                            @endif
                            @if ($payment_phase->paymentStage->id == PaymentStage::getGenerated()->id)
                                <tr class="warning">
                            @else
                                @if (($payment_phase->paymentStage->id == PaymentStage::getPay()->id) || ($payment_phase->paymentStage->id == PaymentStage::getPayed()->id))
                                    <tr class="success">
                                @else
                                    @if (($payment_phase->paymentStage->id == PaymentStage::getDue()->id) || ($payment_phase->paymentStage->id == PaymentStage::getBad()->id))
                                        <tr class="danger">
                                    @else
                                        <tr class="active">
                                    @endif
                                @endif
                            @endif
                                <td>{!!$entry_num!!}</td>
                                    @php
                                        $entry_num++;
                                    @endphp
                                <td>{!!$payment_phase->id!!}</td>                                
                                <td>{!!$payment_phase->paymentStage->name!!}</td>
                                <td>{!!$currency->numberToString($payment_phase->amount)!!}</td>
                                <td>{!!'El '.$payment_phase->date->format('d/m/Y').' a las '.$payment_phase->date->format('h:m').'<strong> ('.$payment_phase->date->diffForHumans().')</strong>'!!}</td>
                            </tr>
                        @else
                            @if ($payment_phase->paymentStage->id == PaymentStage::getAddInterest()->id)
                                @php
                                    $interest += $payment_phase->amount;
                                    $last_interest_id = $payment_phase->id;
                                    $last_date = $payment_phase->date;
                                @endphp
                            @endif
                        @endif
                    @endforeach
                        
                </table>
            </div>
        </div>
        <div>
            <button class="btn btn-primary" onclick="printDiv('box')">Generar PDF</button>
        </div>
    </div>
    @extends('modal')
@endsection