@php
    use Illuminate\Database\Eloquent\Collection;
    use App\Models\Wallet;
    $user = auth()->user();
    $wallets_to_financiate = Wallet::walletsToFinancing();
@endphp 

@extends('adminlte::page')

@section('title_prefix', 'Financiamientos - ')

@section('content')
    <div class="box" id="box">
        <div class="box-header">
            <h1 id="title">
                Financiamientos de {!!$user->name!!}
                @if ($wallets_to_financiate->count() > 0)
                    @php
                        if ($wallets_to_financiate->pluck('currency_id')->contains($user->currency()->id)) {
                            $wallet_id = $wallets_to_financiate->where('currency_id',$user->currency()->id)->first()->id;
                        } else {
                            $wallet_id = $wallets_to_financiate->random()->id;
                        }
                    @endphp
                    <a class="btn btn-success pull-right" data-toggle="modal" data-target="#custom-modal" onclick="selectCurrencyToFinanciate({!!$wallet_id!!})">Financiar</a>
                @endif
            </h1>
        </div>
        <div class="box-body">
            <h1><i class="fa fa-info-circle fa-5x" aria-hidden="true"></i></h1>
            <h1>Aún no tienes ningún financiamiento realizado.</h1>
            @if ($wallets_to_financiate->count() > 0)
                @php
                    if ($wallets_to_financiate->pluck('currency_id')->contains($user->currency()->id)) {
                        $wallet_id = $wallets_to_financiate->where('currency_id',$user->currency()->id)->first()->id;
                    } else {
                        $wallet_id = $wallets_to_financiate->random()->id;
                    }
                @endphp
                <h2>Puedes realizar tu primer financiamiento haciendo clic en <a data-toggle="modal" data-target="#custom-modal" onclick="selectCurrencyToFinanciate({!!$wallet_id!!})">Financiar</a></h2>   
            @else
                <h2>Para poder financiar, primero tienes que ingresar dinero en <a onclick="redirect('/wallets')">Wallets</a></h2>           
            @endif
            <form>
                {!!csrf_field()!!}
            </form>
        </div>
    </div>
    @include('modal')
@stop