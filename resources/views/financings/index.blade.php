@php
    use Illuminate\Database\Eloquent\Collection;
    use App\Models\Wallet;
    $user = auth()->user();
    $wallets_to_financiate = Wallet::walletsToFinancing();

    $wallets = $wallets->sortByDesc(function($wallet,$key){
        $sum = $wallet->financings->count() + ($wallet->activeFinancings()->count() * 1000);
        return $sum;
    });
@endphp 

@extends('adminlte::page')

@section('title_prefix', 'Financiamientos - ')

@section('content')
    <div class="box" id="box">
        <div class="box-header">
            <h1 id="title">
                Financiamientos de {!!$user->name!!}
                @if ($wallets_to_financiate->count() > 0)
                    @php
                        if ($wallets_to_financiate->pluck('currency_id')->contains($user->currency()->id)) {
                            $wallet_id = $wallets_to_financiate->where('currency_id',$user->currency()->id)->first()->id;
                        } else {
                            $wallet_id = $wallets_to_financiate->random()->id;
                        }
                    @endphp
                    <a class="btn btn-success pull-right" data-toggle="modal" data-target="#custom-modal" onclick="selectCurrencyToFinanciate({!!$wallet_id!!})">Financiar</a>
                @else
                    <a onclick="redirect('/wallets')" class="btn btn-success pull-right">Wallets</a>
                @endif
                <div id="chart">
                    <div id="financings-all-donutchart" style="height: 300px;"></div>
                </div>
                <script type="text/javascript">
                    financingsAllDonutchart();
                </script>
            </h1>
        </div>
        <div class="box-body">
            <div class="table-resposive container-border">
            <table class="table table-paginated table-hover table-striped">
                <thead>
                    <th width="30px">Num</th>
                    <th>Divisa</th>
                    <th>Monto</th>
                    <th>Financiamientos</th>
                    <th>Financiamentos activos</th>
                    <th width="10%">Ver detalles</th>
                </thead>
                @php
                    $entry_num = 1;
                @endphp
                <tbody>
                    @foreach ($wallets as $wallet)
                        @if ($wallet->financings->count() > 0)
                            @php
                                $financings = $wallet->financings;
                                $active_financings = $wallet->activeFinancings();
                            @endphp
                            @if ($active_financings->count() > 0)
                                @if ($wallet->hasFinancingDelayedReturn())
                                    <tr class="danger">
                                @else
                                    <tr class="success">
                                @endif
                            @else
                                <tr class="active">  
                            @endif
                                <td>{!!$entry_num!!}</td>
                                @php
                                    $entry_num++;
                                @endphp
                                <td>{!!'Financiamientos en '.strtolower($wallet->currency->name)!!}</td>
                                <td>{!!$wallet->currency->numberToString($wallet->financingsAmount())!!}</td>
                                @if ($financings->count() == 1)
                                    <td>1 financiamiento</td>
                                @else
                                    <td>{!!$financings->count()!!} financiamientos</td>
                                @endif
                                @if ($active_financings->count() == 1)
                                    <td>1 financiamiento</td>
                                @else
                                    <td>{!!$active_financings->count()!!} financiamientos</td>
                                @endif
                                <td>
                                    <a  onclick="redirect('/financings/currency={!!$wallet->currency->id!!}')" class="btn btn-primary">Detalles</a>
                                </td>
                            </tr> 
                        @endif
                    @endforeach
                </tbody>
            </table>
        </div>
        <div>
            <button class="btn btn-primary" onclick="printDiv('box')">Generar PDF</button>
        </div>    
    </div>
    <form>
        {!!csrf_field()!!}
    </form>
    @include('modal')
@stop