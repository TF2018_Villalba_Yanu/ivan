@php
    use Carbon\CarbonInterval;
    $currency = $wallet->currency;
@endphp 

@extends('adminlte::page')

@section('title_prefix', 'Financiar - ')

@section('content')
    <div class="box" id="box">
        <div class="box-header">
            <h1 id="title">
                Financiar en {!!$currency->name!!} 
                <a class="btn btn-danger pull-right" onclick="redirect('/financings/currency={!!$currency->id!!}')">Finalizar</a>
            </h1>
            <h3>Saldo en el wallet: {!!$currency->numberToString($wallet->accountBalance())!!}</h3>                
        </div>
        <div class="box-body">
            <div class="table-responsive container-border">
                <table class="table table-paginated table-striped table-hover">
                    <thead>
                        <th width="30px">Num</th>
                        <th>Financiar</th>
                        <th>ID de credito</th>
                        <th>Perfil crediticio (*)</th>
                        <th>Porcentajes en interes</th>
                        <th>Monto restante a financiar</th>
                        <th>Cuotas</th>
                        <th>Nota</th>
                    </thead>
                    <tbody>
                        @php
                            $entry_num = 1;
                        @endphp
                        @foreach ($credits as $credit)
                            @php
                                $financing = $wallet->financings->where('credit_id',$credit->id)->first();
                            @endphp
                            @if ($financing != null)
                                <tr class="warning">
                            @else
                                <tr>                        
                            @endif
                                <td>{!!$entry_num!!}</td>
                                @php
                                    $entry_num++;
                                @endphp
                                <td>
                                    <button class="btn btn-success" data-toggle="modal" data-target="#custom-modal" onclick="financiateWithCredit({!!$credit->id!!})">Financiar</button>
                                </td>
                                <td>{!!$credit->id!!}</td>
                                <td>{!!$credit->wallet->user->reputation->rank()!!}</td>
                                @php
                                    $interest = $credit->interest_percentage - ($credit->interest_percentage * $credit->commission_percentage);
                                    $interest_with_insurance = $interest - $credit->creditor_insurance_percentage;
                                @endphp
                                <td>{!!$currency->percentage($interest_with_insurance).' / '.$currency->percentage($interest)!!}</td>
                                <td><strong>{!!$currency->numberToString($credit->amount - $credit->financedAmount())!!}</strong></td>
                                <td>{!!$credit->payments_number.' pagos cada '.CarbonInterval::instance($credit->dueDatesInterval->toDateInterval())->diffForHumans()!!}</td>
                                @if ($financing != null)
                                    <td>{!!'Financiaste '.$currency->numberToString($financing->amount())!!}</td>
                                @else
                                    <td>----</td>                        
                                @endif
                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="7">
                                <p>(*): Perfil obtenido en base al historial de pagos del usuario. El valor mas bajo es "D" y el mas alto "AAA".</p>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
    <form>
        {!!csrf_field()!!}
    </form>
    @include('modal')
@stop