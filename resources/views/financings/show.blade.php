@php
    use Carbon\CarbonInterval;
    //use App\Models\CreditStage;
    use App\Models\FinancingStage;
    //use App\Models\Role;
    //use Carbon\Carbon;
    $currency = $financing->wallet->currency;
    $financing_phases = $financing->financingPhases->reverse();
@endphp
@extends('adminlte::page')

@section('title_prefix', 'Detalles del financiamiento - ')

@section('content')
    <div class="box" id="box">
        <div class="box-header">
            <h1 id="title">
                Detalles del financiamiento {!!$financing->id!!}
                @if (($financing->isActive()) && ($financing->nextFinancingStages()->pluck('id')->contains(FinancingStage::getFinancingAbort()->id)))
                    <a class="btn btn-danger pull-right" data-toggle="modal" data-target="#custom-modal" onclick="cancelFinancingWithCredit({!!$financing->credit->id!!})">Quitar dinero</a>            
                @endif
                @if ($financing->canAddMoney())
                    <a class="btn btn-success pull-right" data-toggle="modal" data-target="#custom-modal" onclick="financiateWithCredit({!!$financing->credit->id!!})">Agregar dinero</a>
                @endif
                @php
                    $rows = abs($financing->financingPhases->first()->date->format('Y') - $financing->financingPhases->last()->date->format('Y')) + 1;
                @endphp
                <div id="chart">
                    <div id="financing-phases-calendarchart" style="height: {!!$rows * 175!!}px;"></div>
                </div>
                <form>
                    {!!csrf_field()!!}
                    <input type="hidden"  id="financing_id" value="{!!$financing->id!!}">
                </form>
                <script type="text/javascript">
                    financingPhasesCalendarchart();
                </script>
            </h1>                
        </div>
        <div class="box-body">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <th width="20%">Concepto</th>
                        <th width="20%">Valor</th>
                    </thead>
                    <tbody>
                        <tr>
                            <td>ID del financiamiento</td>
                            <td>{!!$financing->id!!}</td>
                        </tr>
                        <tr>
                            <td>ID del credito</td>
                            <td>{!!$financing->credit->id!!}</td>
                        </tr>
                        <tr>
                            <td>Monto financiado</td>
                            <td>{!!'<strong>'.$currency->numberToString($financing->amount()).'</strong> de '.$currency->numberToString($financing->credit->amount)!!}</td>
                        </tr>
                        <tr>
                            <td>Asegurado</td>
                            @if ($financing->have_insurance)
                                <td>Financiamiento asegurado</td>
                            @else
                                <td>Financiamiento sin asegurar</td>
                            @endif
                        </tr>
                        <tr>
                            <td>Reembolsos</td>
                            @if ($financing->credit->payments_number == 1)
                                <td>pago único</td>                            
                            @else
                                <td>{!!$financing->credit->payments_number.' pagos cada '.CarbonInterval::instance($financing->credit->dueDatesInterval->toDateInterval())->diffForHumans()!!}</td>
                            @endif
                        </tr>
                        @if (!$financing->isActive())
                            @if ($financing->amount() >= $currency->min_value)
                                <tr>
                                    <td>Finalizado</td>
                                    <td>{!!'El '.$financing->lastFinancingPhase()->date->format('d/m/Y').' a las '.$financing->lastFinancingPhase()->date->format('h:m').'<strong> ('.$financing->lastFinancingPhase()->date->diffForHumans().')</strong>'!!}</td>
                                </tr>
                            @else
                                <tr>
                                    <td>Cancelado</td>
                                    <td>{!!'El '.$financing->lastFinancingPhase()->date->format('d/m/Y').' a las '.$financing->lastFinancingPhase()->date->format('h:m').'<strong> ('.$financing->lastFinancingPhase()->date->diffForHumans().')</strong>'!!}</td>    
                                </tr>
                            @endif
                        @else
                            @if ($financing->credit->payments->count() == 0)
                                <tr>
                                    <td>Primer financiamiento</td>
                                    <td>{!!$financing->financingPhases->first()->date->format('d/m/Y').' ('.$financing->financingPhases->first()->date->diffForHumans().')'!!}</td>    
                                </tr>                            
                            @else
                                @if ($financing->amount() > $currency->min_value)
                                <tr>
                                    @if ($financing->have_insurance)
                                        <td>Monto devuelto*</td>   
                                    @else
                                        <td>Monto devuelto</td>
                                    @endif
                                    <td>{!!$currency->numberToString($financing->amountReturned())!!}</td>                                    
                                </tr>    
                                @endif
                            @endif
                        @endif                 
                    </tbody>
                    @if (($financing->have_insurance) && ($financing->amount() > $currency->min_value))
                    <tfoot>
                        <tr>
                            <td colspan="2">
                                <p>(*): Monto devuento menos el abono del seguro.</p>
                            </td>
                        </tr>
                    </tfoot>
                    @endif                    
                </table>
            </div>
            <br>
            <div class="table-responsive">
                <h3>Historial de estados del financiamiento</h3>
                <table class="table table-paginated table-striped table-bordered table-hover">
                    <thead>
                        <th width="30px">Num</th>
                        <th>ID de estado</th>
                        <th>Estado</th>
                        <th>Monto</th>
                        <th>Fecha</th>
                    </thead>
                    @php
                        $entry_num = 1;
                    @endphp
                    <tbody>
                        @foreach ($financing_phases as $financing_phase)
                            <tr>
                                <td>{!!$entry_num!!}</td>
                                @php
                                    $entry_num++;
                                @endphp
                                <td>{!!$financing_phase->id!!}</td>
                                <td>{!!$financing_phase->financingStage->name!!}</td>
                                @if (($financing_phase->amount < $currency->min_value) && ($financing_phase->amount > ($currency->min_value *-1)))
                                    <td>----</td>
                                @else
                                    <td>{!!$currency->numberToString($financing_phase->amount)!!}</td>
                                @endif
                                <td>{!!'El '.$financing_phase->date->format('d/m/Y').' a las '.$financing_phase->date->format('h:m').'<strong> ('.$financing_phase->date->diffForHumans().')</strong>'!!}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div>
            <button class="btn btn-primary" onclick="printDiv('box')">Generar PDF</button>
        </div>    
    </div>
    @extends('modal')
@endsection