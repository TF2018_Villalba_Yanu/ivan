@php
    use Illuminate\Database\Eloquent\Collection;
    use App\Models\Wallet;
    $user = auth()->user();
    $wallets_to_financiate = Wallet::walletsToFinancing();
    $financings = $wallet->financings;
    $financings = $financings->sortByDesc(function($financing,$key){
        return $financing->id;
    });
    $currency = $wallet->currency;
    $show_two_asterisk = false;
    $show_three_asterisk = false;
@endphp 

@extends('adminlte::page')

@section('title_prefix', 'Financiamientos en '.strtolower($wallet->currency->name).' - ')

@section('content')
    <div class="box" id="box">
        <div class="box-header">
            <h1 id="title">
                Financiamientos en {!!strtolower($wallet->currency->name)!!}
                @if ($wallets_to_financiate->count() > 0)
                    @php
                        if ($wallets_to_financiate->pluck('currency_id')->contains($wallet->currency->id)) {
                            $wallet_id = $wallets_to_financiate->where('currency_id',$wallet->currency->id)->first()->id;
                        } else {
                            $wallet_id = $wallets_to_financiate->random()->id;
                        }
                    @endphp
                    <a class="btn btn-success pull-right" data-toggle="modal" data-target="#custom-modal" onclick="selectCurrencyToFinanciate({!!$wallet_id!!})">Financiar</a>
                @else
                    <a onclick="redirect('/wallets/{!!$wallet->id!!}')" class="btn btn-success pull-right">Wallet</a>
                @endif
                <div id="chart">
                    <div id="financings-wallet-donutchart" style="height: 300px;"></div>
                </div>
                <form>
                    <input type="hidden"  id="wallet_id" value="{!!$wallet->id!!}">
                </form>    
                <script type="text/javascript">
                    financingsWalletDonutchart();
                </script>
            </h1>                 
        </div>
        <div class="box-body">
            <table class="table table-striped table-bordered table-hover">
                <thead>
                    <th width="20%">Concepto</th>
                    <th width="20%">Valor</th>
                </thead>
                <tbody>
                    <tr>
                        <td>Financiamientos realizados</td>
                        @if ($financings->count() == 1)
                            <td>1 financiamiento</td>
                        @else
                            <td>{!!$financings->count()!!} financiamientos</td>
                        @endif
                    </tr>
                    <tr>
                        <td>Monto financiado</td>
                        <td>{!!$currency->numberToString($wallet->financingsAmount())!!}</td>
                    </tr>
                    <tr>
                        <td>Beneficios obtenidos</td>
                        <td>{!!$currency->numberToString($wallet->financingsAmountReturned())!!}*</td>
                    </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="2">
                            <p>(*): Incluye el reintegro de de todo el dinero financiado por el wallet mas la ganancia en concepto de interes, menos el abono del seguro de los financiamientos asegurados.</p>
                        </td>
                    </tr>
                </tfoot>
            </table>
            @if ($wallet->activeFinancings()->count() > 0)
                <div class="table-responsive container-border">
                    <h3>Financiamientos activos</h3>
                    <table class="table table-paginated table-hover table-striped table-condensed">
                        <thead>
                            <th width="30px">Num</th>
                            <th>ID del financiamiento</th>
                            <th>ID del credito</th>
                            <th>Monto financiado</th>
                            <th>Ganancia esperada</th>
                            <th>Dinero devuelto</th>
                            <th width="10%">Ver detalles</th>
                        </thead>
                        @php
                            $entry_num = 1;
                        @endphp
                        <tbody>
                            @foreach ($financings as $financing)
                                @if ($financing->isActive())
                                    @if ($financing->hasDelayedReturn())
                                        <tr class="danger">
                                    @else
                                        @if ($financing->credit->payments->count() == 0)
                                            <tr class="warning">
                                        @else
                                            <tr class="success"> 
                                        @endif
                                    @endif
                                        <td>{!!$entry_num!!}</td>
                                        @php
                                            $entry_num++;
                                        @endphp
                                        <td>{!!$financing->id!!}</td>
                                        <td>{!!$financing->credit->id!!}</td>
                                        <td>{!!$currency->numberToString($financing->amount()).' ('.$currency->percentage($financing->amount()/$financing->credit->amount).')'!!}</td>
                                        @if (!$financing->have_insurance)
                                            <td>{!!$currency->numberToString($financing->financingProfitability())!!}**</td>
                                            @php
                                                $show_two_asterisk = true;
                                            @endphp
                                        @else
                                            <td>{!!$currency->numberToString($financing->financingProfitability())!!}</td>
                                        @endif
                                        @if ($financing->amountReturned() < $currency->min_value)
                                            <td>----</td>
                                        @else
                                            <td>{!!$currency->numberToString($financing->amountReturned()).' ('.$currency->percentage($financing->amountReturned()/$financing->financingProfitability()).')'!!}***</td>
                                            @php
                                                $show_three_asterisk = true;
                                            @endphp
                                        @endif
                                        <td>
                                            <a  onclick="redirect('/financings/{!!$financing->id!!}')" class="btn btn-primary">Detalles</a>
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                        </tbody>
                        @if (($show_two_asterisk) || ($show_three_asterisk))
                            <tfoot>
                                <tr>
                                    <td colspan="5">
                                        @if ($show_two_asterisk)
                                            <p> (**): En el caso de que el financiamiento no tenga seguro y se demore el pago, se cobrara un interes adicional por mora a beneficio de quien otorga la financiacion. Dicho interes no esta contemplado en la columna "Ganancia esperada".</p>
                                        @endif
                                        @if ($show_three_asterisk)
                                            <p>(***): Monto devuento, el abono del seguro está contemplado (en el caso de estar contratado).</p>                                    
                                        @endif
                                    </td>
                                </tr>
                            </tfoot>
                        @endif
                    </table>
                </div>
            @endif
            @if (($wallet->financings->count() - $wallet->activeFinancings()->count()) > 0)
                <br>
                <div class="table-responsive container-border">
                    <h3>Financiamientos inactivos</h3>
                    <table class="table table-paginated table-striped table-hover table-condensed">
                        <thead>
                            <th width="30px">Num</th>
                            <th>ID del financiamiento</th>
                            <th>ID del credito</th>
                            <th>Monto financiado</th>
                            <th>Dinero devuelto</th>
                            <th>Inactivo hace</th>
                            <th width="10%">Ver detalles</th>
                        </thead>
                        @php
                            $entry_num = 1;
                        @endphp
                        <tbody>
                            @foreach ($financings as $financing)
                                @if (!$financing->isActive())
                                    <tr>
                                        <td>{!!$entry_num!!}</td>
                                        @php
                                            $entry_num++;
                                        @endphp
                                        <td>{!!$financing->id!!}</td>
                                        <td>{!!$financing->credit->id!!}</td>
                                        <td>{!!$currency->numberToString($financing->amount())!!}</td>
                                        @if ($financing->amount() < $currency->min_value)
                                            <td>----</td>
                                        @else
                                            <td>{!!$currency->numberToString($financing->financingProfitability())!!}</td>                                    
                                        @endif
                                        <td>{!!ucfirst($financing->lastFinancingPhase()->date->diffForHumans())!!}</td>
                                        <td>
                                            <a  onclick="redirect('/financings/{!!$financing->id!!}')" class="btn btn-primary">Detalles</a>
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                        </tbody>
                    </table>
                </div>
            @endif
        </div>
        <div>
            <button class="btn btn-primary" onclick="printDiv('box')">Generar PDF</button>
        </div>    
    </div>
    @include('modal')
@stop