@php
    use Illuminate\Database\Eloquent\Collection;
    use App\Models\Wallet;
    $wallets_to_financiate = Wallet::walletsToFinancing();
    $user = auth()->user();
@endphp 

@extends('adminlte::page')

@section('title_prefix', 'Financiar - ')

@section('content')
    <div class="box" id="box">
        <div class="box-header">
            <h1 id="title">
                Financiar en {!!$currency->name!!} 
                <a class="btn btn-danger pull-right" onclick="redirect('/financings')">Finalizar</a>
            </h1>
        </div>
        <div class="box-body">
            <h1><i class="fa fa-info-circle fa-5x" aria-hidden="true"></i></h1>
            <h1>Lo sentimos, pero en este momento no hay mas creditos finánciandose en esta divisa.</h1>
            @if ($wallets_to_financiate->count() > 0)
                @php
                    if ($wallets_to_financiate->pluck('currency_id')->contains($user->currency()->id)) {
                        $wallet_id = $wallets_to_financiate->where('currency_id',$user->currency()->id)->first()->id;
                    } else {
                        $wallet_id = $wallets_to_financiate->random()->id;
                    }
                @endphp

                <h2>Puedes volver a <a onclick="redirect('/financings')">financiamientos</a> o <a data-toggle="modal" data-target="#custom-modal" onclick="selectCurrencyToFinanciate({!!$wallet_id!!})">seleccionar otra divisa</a></h2>        
            @else
                <h2>Puedes volver a <a onclick="redirect('/financings')">financiamientos</a> o <a onclick="redirect('/wallets')">agregar dinero en otra divisa</a></h2>        
            @endif
        </div>
    </div>
    <form>
        {!!csrf_field()!!}
    </form>
    @include('modal')
@stop