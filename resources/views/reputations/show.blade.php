@php
    use App\Models\Role;
    use App\Models\Currency;
    use App\Models\InterestModifier;
    use App\Models\InsuranceModifier;
    $currency = Currency::getCurrency('USD');
    $margin_modifiers    = $reputation->marginModifiers;
    $insurance_modifiers = $reputation->insuranceModifiers;
    $interest_modifiers  = $reputation->interestModifiers;
    if($margin_modifiers->count() >0 ){
        $margin_modifiers = $margin_modifiers->reverse();
    }
    if($insurance_modifiers->count() >0 ){
        $insurance_modifiers = $insurance_modifiers->reverse();
    }
    if($interest_modifiers->count() >0 ){
        $interest_modifiers = $interest_modifiers->reverse();
    }
@endphp
@extends('adminlte::page')

@section('title_prefix', 'Detalles de la reputacion - ')

@section('content')
    <div class="box" id="box">
        <div class="box-header">
            <h1 id="title">
                Reputacion de {!!$reputation->user()->name!!}        
                <form>
                    <input type="hidden" id="reputation_id" value="{!!$reputation->id!!}">
                </form>
            </h1>
        </div>
        <div class="box-body">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <th width="20%">Concepto</th>
                        <th width="20%">Valor</th>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Margen de cuenta</td>
                            <td>{!!$currency->numberToString($reputation->marginUSD())!!}</td>
                        </tr>
                        @php
                            $insurance = InsuranceModifier::percentage($reputation);
                            $interest  = InterestModifier::percentage($reputation);
                        @endphp
                        @if ($insurance == 0)
                            <tr class="warning">
                        @else
                           @if ($insurance < 0)
                               <tr class="success">
                           @else
                               <tr class="danger">
                           @endif 
                        @endif
                            <td>Seguro personalizado (*)</td>
                            <td>{!!$currency->percentage($insurance)!!}</td>
                        </tr>
                        @if ($interest == 0)
                            <tr class="warning">
                        @else
                           @if ($interest < 0)
                               <tr class="success">
                           @else
                               <tr class="danger">
                           @endif 
                        @endif
                            <td>Interes personalizado (*) (**)</td>
                            <td>{!!$currency->percentage($interest)!!}</td>
                        </tr>
                        @php
                            $rank = $reputation->rank()
                        @endphp
                        @if (($rank == "A") || ($rank == "AA") || ($rank == "AAA"))
                            <tr class="success">
                        @else
                            @if ($rank == "B")
                                <tr class="warning">
                            @else
                                <tr class="danger">
                            @endif
                        @endif
                            <td>Calificacion del usuario (***)</td>
                            <td>{!!$rank!!}</td>
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="2">
                                <p>(*): Depende de como abonás tus créditos. No es el porcentaje neto a aplica al solicitar uno, ya que depende del plan de pagos seleccionado. Al porcentaje del plan, se le suma o resta el porcentaje personalizado. Mientras mejor lleves el pago de tus cuotas, mas bajos seran estos porcentajes, y menos pagás en intereses!</p>
                                <p>(**): Manejamos un seguro por pagos retrasados o mora en las cuotas, que se encarga pagar a quienes te prestan el dinero si te retrasas en pagar. Este seguro se adiciona al interés neto y es transparente cuando solicitas un crédito. Si pagas bien tus créditos, se te descontará cada vez más el seguro</p>
                                <p>(***): Calificacion obtenida en base al historial de pagos del usuario. Este valor sera mostrado a quienes financien tus creditos para que puedan saber tu perfil crediticio. El peor valor a obtener es "D" y el mejor "AAA"</p>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
            @if ($margin_modifiers->count() >0)
                <br>
                <div class="table-responsive container-border">
                    <h3>Modificadores de margen
                        <div id="chart">
                            <div id="margin-areachart" style="height: 300px;"></div>
                        </div>
                        <script>
                            marginAreachart();
                        </script>
                    </h3>
                    <table  class="table table-paginated table-striped table-hover table-condensed">
                        <thead>
                            <th width="30px">Num</th>
                            <th>ID de modificador de margen</th>
                            <th>Tipo</th>
                            <th>Monto</th>
                            <th>Fecha</th>
                        </thead>
                        @php
                            $entry_num = 1;
                        @endphp    
                        <tbody>
                            @foreach ($margin_modifiers as $margin_modifier)
                                @if ($margin_modifier->amount >= 0)
                                    <tr class="success">
                                @else
                                    <tr class="danger">
                                @endif
                                    <td>{!!$entry_num!!}</td>
                                    @php
                                        $entry_num++;
                                    @endphp
                                    <td>{!!$margin_modifier->id!!}</td>
                                    <td>{!!$margin_modifier->marginModifierStage->name!!}</td>
                                    <td>{!!$currency->numberToString($margin_modifier->amount)!!}</td>
                                    <td>{!!'El '.$margin_modifier->date->format('d/m/Y ').'<strong> ('.$margin_modifier->date->diffForHumans().')</strong>'!!}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>  
            @endif
            @if ($interest_modifiers->count() >0)
                <br>
                <div class="table-responsive container-border">
                    <h3>Modificadores del interes
                        <div id="chart">
                            <div id="interest-areachart" style="height: 300px;"></div>
                        </div>
                        <script>
                            interestAreachart();
                        </script>
                    </h3>
                    <table  class="table table-paginated table-striped table-hover table-condensed">
                        <thead>
                            <th width="30px">Num</th>
                            <th>ID de modificador de interes</th>
                            <th>Tipo</th>
                            <th>Valor</th>
                            <th>Fecha</th>
                        </thead>
                        @php
                            $entry_num = 1;
                        @endphp    
                        <tbody>
                            @foreach ($interest_modifiers as $interest_modifier)
                                @if ($interest_modifier->percentage <= 0)
                                    <tr class="success">
                                @else
                                    <tr class="danger">
                                @endif
                                    <td>{!!$entry_num!!}</td>
                                    @php
                                        $entry_num++;
                                    @endphp
                                    <td>{!!$interest_modifier->id!!}</td>
                                    <td>{!!$interest_modifier->interestModifierStage->name!!}</td>
                                    <td>{!!$currency->percentage($interest_modifier->percentage)!!}</td>
                                    <td>{!!'El '.$interest_modifier->date->format('d/m/Y ').'<strong> ('.$interest_modifier->date->diffForHumans().')</strong>'!!}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>  
            @endif
            @if ($insurance_modifiers->count() >0)
                <br>
                <div class="table-responsive container-border">
                    <h3>Modificadores de seguro
                        <div id="chart">
                            <div id="insurance-areachart" style="height: 300px;"></div>
                        </div>  
                        <script>
                            insuranceAreachart();
                        </script>  
                    </h3>    
                    <table  class="table table-paginated table-striped table-hover table-condensed">
                        <thead>
                            <th width="30px">Num</th>
                            <th>ID de modificador de seguro</th>
                            <th>Tipo</th>
                            <th>Valor</th>
                            <th>Fecha</th>
                        </thead>
                        @php
                            $entry_num = 1;
                        @endphp    
                        <tbody>
                            @foreach ($insurance_modifiers as $insurance_modifier)
                                @if ($insurance_modifier->percentage <= 0)
                                    <tr class="success">
                                @else
                                    <tr class="danger">
                                @endif
                                    <td>{!!$entry_num!!}</td>
                                    @php
                                        $entry_num++;
                                    @endphp
                                    <td>{!!$insurance_modifier->id!!}</td>
                                    <td>{!!$insurance_modifier->insuranceModifierStage->name!!}</td>
                                    <td>{!!$currency->percentage($insurance_modifier->percentage)!!}</td>
                                    <td>{!!'El '.$insurance_modifier->date->format('d/m/Y ').'<strong> ('.$insurance_modifier->date->diffForHumans().')</strong>'!!}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>  
            @endif
        </div>
        <div>
            <button class="btn btn-primary" onclick="printDiv('box')">Generar PDF</button>
        </div>
    </div>
@endsection