@php
    use App\Models\CreditStage;
    use Illuminate\Database\Eloquent\Collection;
    use Carbon\Carbon;

@endphp
@extends('adminlte::page')

@section('title_prefix', 'Detalles del comprobante - ')

@section('content')
    <div class="box" id="box">
        <div class="box-header">
            <h1 id="title">
                Detalles del comprobante {!!$voucher->id!!}
            </h1>
        </div>
        <div class="box-body">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <th width="20%">Concepto</th>
                        <th width="20%">Valor</th>
                    </thead>
                    <tbody>
                        <tr>
                            <td>ID de comprobante</td>
                            <td>{!!$voucher->id!!}</td>
                        </tr>
                        <tr>
                            <td>Concepto</td>
                            <td>{!!$voucher->voucherType->name!!}</td>
                        </tr>
                        <tr>
                            <td>Descripción</td>
                            <td>{!!$voucher->voucherType->description!!}</td>
                        </tr>
                        @if ($voucher->walletTransaction != null)
                            <tr>
                                <td>ID de wallet</td>
                                <td>{!!$voucher->walletTransaction->wallet->id!!}</td>
                            </tr>
                        @else
                            @if ($voucher->creditPhase != null)
                                <tr>
                                    <td>ID de crédito</td>
                                    <td>{!!$voucher->creditPhase->credit->id!!}</td>
                                </tr>
                            @else
                                @if ($voucher->financingPhase != null)
                                    <tr>
                                        <td>ID de financiamiento</td>
                                        <td>{!!$voucher->financingPhase->financing->id!!}</td>
                                    </tr>
                                @endif
                            @endif
                        @endif
                        <tr>
                            <td>Divisa</td>
                            <td>{!!ucfirst($voucher->currency->name)!!}</td>
                        </tr>
                        <tr>
                            <td>Monto</td>
                            <td><strong>{!!$voucher->currency->numberToString($voucher->amount)!!}</strong></td>
                        </tr>
                        <tr>
                            <td>Fecha</td>
                            <td>{!!'El '.$voucher->date->format('d/m/Y').' a las '.$voucher->date->format('h:m').'<strong> ('.$voucher->date->diffForHumans().')</strong>'!!}</td>
                        </tr>                      
                    </tbody>                    
                </table>
            </div>
        </div>
        <div>
            <button class="btn btn-primary" onclick="printDiv('box')">Generar PDF</button>
        </div>
    </div>
        
@endsection