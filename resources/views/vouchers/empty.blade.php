@php
    $user = auth()->user();
@endphp 

@extends('adminlte::page')

@section('title_prefix', 'Comprobantes - ')

@section('content')
    <div class="box" id="box">
        <div class="box-header">
            <h1 id="title">
                Comprobantes de {!!$user->!!}
            </h1>
        </div>
        <div class="box-body">
            <h1><i class="fa fa-info-circle fa-5x" aria-hidden="true"></i></h1>
            <h1>Aún no tienes ningún comprobante generado.</h1>
            <h2>A medida que operes con el sistema, tus comprobantes aparecerán aquí</h2>
            <form>
                {!!csrf_field()!!}
            </form>
        </div>
    </div>
    @include('modal')
@stop