@php
    $user = auth()->user();
    $vouchers = $vouchers->sortByDesc(function($voucher,$key){
        return $voucher->id;
    });
@endphp 

@extends('adminlte::page')

@section('title_prefix', 'Comprobantes - ')

@section('content')
    <div class="box" id="box">
        <div class="box-header">
            <h1 id="title">
               Comprobantes de {!!$user->name!!}
            </h1>
        </div>
        <div class="box-body">
            <div class="table-resposive container-border">
            <table class="table table-paginated table-hover table-striped">
                <thead>
                    <th width="30px">Num</th>
                    <td>ID de comprobante</td>
                    <th>Concepto</th>
                    <th>Divisa</th>
                    <th>Monto</th>
                    <th width="10%">Ver detalles</th>
                </thead>
                @php
                    $entry_num = 1;
                @endphp
                <tbody>
                    @foreach ($vouchers as $voucher)
                        <tr>
                            <td>{!!$entry_num!!}</td>
                            @php
                                $entry_num++;
                            @endphp
                            <td>{!!$voucher->id!!}</td>
                            <td>{!!$voucher->voucherType->name!!}</td>
                            <td>{!!ucfirst($voucher->currency->name)!!}</td>
                            <td>{!!$voucher->currency->numberToString($voucher->amount)!!}</td>
                            <td>
                                <a  onclick="redirect('/vouchers/{!!$voucher->id!!}')" class="btn btn-primary">Detalles</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div>
            <button class="btn btn-primary" onclick="printDiv('box')">Generar PDF</button>
        </div>
    </div>
@stop