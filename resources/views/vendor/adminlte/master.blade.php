<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/vendor/bootstrap/dist/css/bootstrap.css') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/vendor/font-awesome/css/font-awesome.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/vendor/Ionicons/css/ionicons.css') }}">

    @if(config('adminlte.plugins.select2'))
        <!-- Select2 -->
        <link rel="stylesheet" href="//cdnjs.cloudflare/ajax/libs/select2/4.0.3/css/select2.css">
    @endif
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/dist/css/AdminLTE.css') }}">
    @if(config('adminlte.plugins.datatables'))
        <!-- DataTables with bootstrap 3 style -->
        <link rel="stylesheet" href="//cdn.datatables.net/v/bs/dt-1.10.18/datatables.css">
    @endif
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <title>@yield('title_prefix', config('adminlte.title_prefix', ''))
@yield('title', config('adminlte.title', 'AdminLTE 2'))
@yield('title_postfix', config('adminlte.title_postfix', ''))</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    

    @yield('adminlte_css')

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <script src="{{ asset('js/app.js') }}"></script>
</head>
<body class="hold-transition @yield('body_class')">
    <div>
            @yield('body')
            <script src="{{ asset('vendor/adminlte/vendor/jquery/dist/jquery.js') }}"></script>
            <script src="{{ asset('vendor/adminlte/vendor/jquery/dist/jquery.slimscroll.min.js') }}"></script>
            <script src="{{ asset('vendor/adminlte/vendor/bootstrap/dist/js/bootstrap.js') }}"></script>
            
            @if(config('adminlte.plugins.select2'))
                <!-- Select2 -->
                <script src="{{ asset('vendor/cloudflare/ajax/libs/select2/4.0.3/js/select2.js') }}"></script>
            @endif
            
            @if(config('adminlte.plugins.datatables'))
                <!-- DataTables with bootstrap 3 renderer -->
                <script src="{{ asset('vendor/datatables/v/bs/dt-1.10.18/datatables.js')}}"></script>
            @endif
            
            @if(config('adminlte.plugins.chartjs'))
                <!-- ChartJS -->
                <script src="{{ asset('vendor/cloudflare/ajax/libs/Chart.js/2.7.0/Chart.bundle.min.js')}}"></script>
            @endif
            @yield('adminlte_js')
    </div>
</body>
</html>
