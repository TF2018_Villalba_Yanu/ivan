
@php
    use Carbon\Carbon;
    use App\Models\Country;
    use App\Models\Profession;
    use App\Models\Role;
    $roles = role::all();
    $countries = Country::all();
    $professions = Profession::all();
    $birthdate_min = Carbon::now();
    $birthdate_min->addYears(-100);
    $birthdate_min = $birthdate_min->format('Y-m-d');
    $birthdate_max = Carbon::now();
    $birthdate_max->addYears(-15);
    $birthdate_max = $birthdate_max->format('Y-m-d');
@endphp
@extends('adminlte::page')

@section('title_prefix', 'Crear usuario - ')

@section('content')
    <div class="box" id="box">
        <div class="box-header">
            <h1>Crear usuario</h1>
        </div>
        <div class="box-body">
            <form action="/users" method="post">
                {!!csrf_field()!!}
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <th width="20%">Concepto</th>
                            <th width="20%">Valor</th>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Nombre</td>
                                <td><input required type="text" class="form-control" name="name" id="name" maxlength="30"></td>
                            </tr>

                            <tr>
                                <td>Telefono
                                    <select required name="country_phone" id="country_phone" class="pull-right">
                                        @foreach ($countries as $country)
                                            <option value="{!!$country->phone_code!!}">{!!$country->name!!}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td width="20%">
                                    <input required type="number" class="form-control" name="phone" id="phone" min="1000000000" max="9999999999">
                                </td>
                                </td>
                            </tr>
                            <tr>
                                <td>Email</td>
                                <td><input required type="email" class="form-control" name="email" id="email" maxlength="100"></td>
                            </tr>
                            <tr>
                                <td>Fecha de nacimiento</td>
                                <td><input required type="date" min={!!$birthdate_min!!} class="form-control" max="{!!$birthdate_max!!}" name="birthdate" id="birthdate"></td>
                            </tr>
                            <tr>
                                <td>Profesion</td>
                                <td>
                                    <select required name="profession_id" class="form-control" id="proffesion_id">
                                        @foreach ($professions as $profession)
                                            <option value="{!!$profession->id!!}">{!!$profession->name!!}</option>
                                        @endforeach
                                    </select>
                                </td>
                            </tr>
                            @if (auth()->user()->role->id == Role::getAdminRole()->id)
                                <tr>
                                    <td>Rol</td>
                                    <td>
                                        <select required name="role_id" class="form-control" id="role_id">
                                            @foreach ($roles as $role)
                                                <option value="{!!$role->id!!}">{!!$role->name!!}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                </tr>
                            @endif
                            <tr>
                                <td>Contraseña</td>
                                <td>
                                    <input required type="password" name="password" id="password" class="form-control" minlength="6">
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <br>
                <div class="table-responsive">
                        <h3>Direccion</h3>
                        <table class="table table-bordered table-striped table-hover">
                            <thead>
                                <th width="20%">Concepto</th>
                                <th width="20%">Valor</th>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Departamento</td>
                                    <td><input required type="text" class="form-control" name="address_department" id="address_department" maxlength="2"></td>
                                </tr>
                                <tr>
                                    <td>Piso</td>
                                    <td><input required type="number" class="form-control" min="1" max="130" name="address_floor" id="address_floor"></td>
                                </tr>
                                <tr>
                                    <td>Calle</td>
                                    <td><input required type="text" class="form-control" name="address_street" id="address_street" maxlength="60"></td>
                                </tr>
                                <tr>
                                    <td>Numero</td>
                                    <td><input required type="number" class="form-control" name="address_number" id="address_number" min="0" max="10000"></td>
                                </tr>
                                <tr>
                                    <td>Pais</td>
                                    <td>
                                        <select required class="form-control" name="country_id" id="country_id">
                                            @foreach ($countries as $country)
                                                <option value="{!!$country->id!!}">{!!$country->name!!}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Provincia</td>
                                    <td>
                                        <select required class="form-control" name="district_id" id="district_id">
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Ciudad</td>
                                    <td>
                                        <select required class="form-control" name="city_id" id="city_id">
                                        </select>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                <button type="submit" class="btn btn-success pull-right">Enviar</button>
            </form>
        </div>
    </div>
    @include('styles.supr_arrows_in_text_fields')
@endsection