
@php

@endphp
@extends('adminlte::page')

@section('title_prefix', 'Detalles del usuario - ')

@section('content')
    <div class="box" id="box">
        <div class="box-header">
            <h1>Mi perfil
                <a onclick="redirect('/users/{!!$user->id!!}/edit')" class="btn btn-primary pull-right"><i class="fa fa-pencil" aria-hidden="true"></i> Editar</a>
            </h1>
        </div>
        <div class="box-body">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <th width="20%">Concepto</th>
                        <th width="20%">Valor</th>
                    </thead>
                    <tbody>
                        <tr>
                            <td>ID de usuario</td>
                            <td>{!!$user->id!!}</td>
                        </tr>
                        <tr>
                            <td>Nombres</td>
                            <td>{!!$user->name!!}</td>
                        </tr>
                        <tr>
                            @php
                                $address = $user->address;
                            @endphp
                            <td>Direccion</td>
                            <td>{!!'Departamento '.$address->department.', piso '.$address->floor.', de la calle '.$address->street.', numero '.$address->number!!}</td>
                        </tr>
                        <tr>
                            <td>Localidad</td>
                            <td>{!!$address->city->name.', '.$address->city->district->name.', '.$address->city->district->country->name!!}</td>
                        </tr>
                        <tr>
                            <td>Telefono</td>
                            <td>{!!$user->phone!!}</td>
                        </tr>
                        <tr>
                            <td>Email</td>
                            <td>{!!$user->email!!}</td>
                        </tr>
                        <tr>
                            <td>Fecha de nacimiento</td>
                            <td>{!!$user->birtdate!!}</td>
                        </tr>
                        <tr>
                            <td>Rol</td>
                            <td>{!!$user->role->name!!}</td>
                        </tr>
                    </tbody>
                </table> 
            </div>
        </div>
        <div>
            <button class="btn btn-primary" onclick="printDiv('box')">Generar PDF</button>
        </div>
    </div>
    @include('styles.supr_arrows_in_text_fields')
@endsection