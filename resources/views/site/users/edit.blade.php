
@php
    use App\Models\Country;
    use App\Models\Role;
@endphp
@extends('adminlte::page')

@section('title_prefix', 'Editar usuario - ')

@section('content')
    <div class="box" id="box">
        <div class="box-header">
            <h1>Editar usuario</h1>
        </div>
        <div class="box-body">
            <form action="/users/{!!$user->id!!}" method="post">
                {!!csrf_field()!!}
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <th width="20%">Concepto</th>
                            <th width="20%">Valor</th>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Nombre*</td>
                                <td><input required type="text" class="form-control" name="name" value="{!!$user->name!!}" id="name" maxlength="30"></td>
                            </tr>

                            <tr>
                                <td>Telefono
                                        @if (strlen($user->phone) == 14)
                                            @php
                                                $default_country_phone = Country::where('phone_code',substr($user->phone,4))->first();
                                            @endphp
                                            <select required name="country_phone" id="country_phone" class="pull-right">
                                                <option value="{!!$default_country_phone->phone_code!!}">{!!$default_country_phone->name!!}</option>
                                                @foreach ($countries as $country)
                                                    <option value="{!!$country->phone_code!!}">{!!$country->name!!}</option>
                                                @endforeach
                                            </select>
                                        @else
                                            <select required name="country_phone" id="country_phone" class="pull-right">
                                                @foreach ($countries as $country)
                                                    <option value="{!!$country->phone_code!!}">{!!$country->name!!}</option>
                                                @endforeach
                                            </select>
                                        @endif
                                </td>
                                <td width="20%">
                                    <input type="number" class="form-control" value="{!!substr($user->phone,3)!!}" name="phone" id="phone" min="1000000000" max="9999999999">
                                </td>
                                </td>
                            </tr>
                            <tr>
                                <td>Email</td>
                                <td>{!!$user->email!!}</td>
                            </tr>
                            <tr>
                                <td>Fecha de nacimiento</td>
                                @php
                                    if($user->birthdate != null){
                                        $birthdate_value = $user->birthdate->format('Y-m-d');
                                    }else {
                                        $birthdate_value = '';
                                    }
                                @endphp
                                <td><input type="date" min={!!$birthdate_min!!} class="form-control" max="{!!$birthdate_max!!}" value="{!!$birthdate_value!!}" name="birthdate" id="birthdate"></td>
                            </tr>
                            <tr>
                                <td>Profesion*</td>
                                <td>
                                    <select required name="profession_id" class="form-control" id="proffesion_id">
                                        @foreach ($professions as $profession)
                                            <option value="{!!$profession->id!!}">{!!$profession->name!!}</option>
                                        @endforeach
                                    </select>
                                </td>
                            </tr>
                            @if ((auth()->user()->role->id == Role::getAdminRole()->id) && (auth()->user()->id != $user->id))
                                <tr>
                                    <td>Rol*</td>
                                    <td>
                                        <select required name="role_id" class="form-control" id="role_id">
                                            @foreach ($roles as $role)
                                                <option value="{!!$role->id!!}">{!!$role->name!!}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                </tr>                                    
                            @else
                            <tr>
                                <td>Contraseña*</td>
                                <td>
                                    <input required type="password" name="password" class="form-control" id="password" minlength="6">
                                </td>
                            </tr>
                            @endif
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="2">
                                    <p>(*): Campos requeridos</p>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <br>
                <div class="table-responsive">
                        <h3>Direccion</h3>
                        <table class="table table-bordered table-striped table-hover">
                            <thead>
                                <th width="20%">Concepto</th>
                                <th width="20%">Valor</th>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Departamento*</td>
                                    <td><input required type="text" class="form-control" name="address_department" id="address_department" value="{!!$user->address->department!!}" maxlength="2"></td>
                                </tr>
                                <tr>
                                    <td>Piso*</td>
                                    <td><input required type="number" class="form-control" min="1" max="130" name="address_floor" id="address_floor" value="{!!$user->address->floor!!}"></td>
                                </tr>
                                <tr>
                                    <td>Calle*</td>
                                    <td><input required type="text" class="form-control" name="address_street" id="address_street" value="{!!$user->address->street!!}" maxlength="60"></td>
                                </tr>
                                <tr>
                                    <td>Numero*</td>
                                    <td><input required type="number" class="form-control" name="address_number" id="address_number" value="{!!$user->address->number!!}" min="0" max="10000"></td>
                                </tr>
                                <tr>
                                    <td>País*</td>
                                    <td>
                                        <select required class="form-control" name="country_id" id="country_id" onchange="countryChange()">
                                            @foreach ($countries as $country)
                                                <option value="{!!$country->id!!}">{!!$country->name!!}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Provincia*</td>
                                    <td>
                                        <select required class="form-control" name="district_id" id="district_id" onchange="districtChange()" onmouseover="countryChange()">
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Ciudad*</td>
                                    <td>
                                        <select required class="form-control" name="city_id" id="city_id" onmouseover="districtChange()">
                                        </select>
                                    </td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="2">
                                        <p>(*): Campos requeridos</p>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                <button type="submit" class="btn btn-success pull-right">Enviar</button>
            </form>
        </div>
    </div>
    
    @include('styles.supr_arrows_in_text_fields')
@endsection