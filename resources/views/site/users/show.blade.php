@php
    use App\Models\Role;
@endphp
@extends('adminlte::page')

@section('title_prefix', 'Detalles del usuario - ')

@section('content')
    <div class="box" id="box">
        <div class="box-header">
            <h1>Detalles del usuario {!!$user->id!!}
                <a onclick="redirect('/users/{!!$user->id!!}/edit')" class="btn btn-primary pull-right"><i class="fa fa-pencil" aria-hidden="true"></i> Editar</a>
            </h1>
        </div>
        <div class="box-body">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <th width="20%">Concepto</th>
                        <th width="20%">Valor</th>
                    </thead>
                    <tbody>
                        <tr>
                            <td>ID de usuario</td>
                            <td>{!!$user->id!!}</td>
                        </tr>
                        <tr>
                            <td>Nombre</td>
                            <td>{!!$user->name!!}</td>
                        </tr>
                        <tr>
                            @php
                                $address = $user->address;
                            @endphp
                            <td>Direccion</td>
                            <td>{!!'Departamento '.$address->department.', piso '.$address->floor.', de la calle '.$address->street.', numero '.$address->number!!}</td>
                        </tr>
                        <tr>
                            <td>Localidad</td>
                            <td>{!!$address->city->name.', '.$address->city->district->name.', '.$address->city->district->country->name!!}</td>
                        </tr>
                        <tr>
                            <td>Telefono</td>
                            <td>{!!$user->phone!!}</td>
                        </tr>
                        <tr>
                            <td>Email</td>
                            <td>{!!$user->email!!}</td>
                        </tr>
                        <tr>
                            <td>Fecha de nacimiento</td>
                            @if ($user->birthdate != null)
                                <td>{!!$user->birthdate->format('d/m/Y')!!}</td>
                            @else
                                <td></td>
                            @endif
                            
                        </tr>
                        <tr>
                            <td>Profesion</td>
                            @if ($user->profession != null)
                                <td>{!!$user->profession->name!!}</td>
                            @else
                                <td></td>
                            @endif
                            
                        </tr>
                        @if(auth()->user()->role->id == Role::getAdminRole()->id)
                            <tr>
                                <td>Rol</td>
                                <td>{!!$user->role->name!!}</td>
                            </tr> 
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
        <div>
            <button class="btn btn-primary" onclick="printDiv('box')">Generar PDF</button>
        </div>
    </div>
@endsection