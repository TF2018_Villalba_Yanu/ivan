@php

@endphp 

@extends('adminlte::page')

@section('title_prefix','Usuarios - ')

@section('content')
    <div class="box" id="box">
        <div class="box-header">
            <h1>Usuarios del sistema
                <a onclick="redirect('/users/create')" class="btn btn-success pull-right">Nuevo</a>
            </h1>
        </div>
        <div class="box-body">
            <div class="table-responsive container-border">
                <table class="table table-paginated table-hover table-striped">
                    <thead>
                        <th width="30px">Num</th>
                        <th>Nombre</th>
                        <th>Correo electronico</th>
                        <th>Rol</th>
                        <th width="10%">Ver detalles</th>
                    </thead>
                    @php
                        $entry_num = 1;
                    @endphp 
                    <tbody>
                        @foreach ($users as $user)
                            <tr>
                                <td>{!!$entry_num!!}</td>
                                @php
                                    $entry_num++;
                                @endphp
                                <td>{!!$user->name!!}</td>
                                <td>{!!$user->email!!}</td>
                                <td>{!!$user->role->name!!}</td>
                                <td>
                                    <a  onclick="redirect('/users/{!!$user->id!!}')" class="btn btn-primary">Detalles</a>
                                </td>
                            </tr>                        
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div>
            <button class="btn btn-primary" onclick="printDiv('box')">Generar PDF</button>
        </div>
    </div>
@stop