
@extends('adminlte::page')

@section('title_prefix', 'Crear divisa - ')

@section('content')
    <div class="box" id="box">
        <div class="box-header">
            <h1>Crear divisa</h1>
        </div>
        <div class="box-body">
            <form action="/currencies" method="post">
                {!!csrf_field()!!}
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <th width="20%">Concepto</th>
                            <th width="20%">Valor</th>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Nombre</td>
                                <td><input required type="text" name="name" id="name" maxlength="20" class="form-control"></td>
                            </tr>
                            <tr>
                                <td>Codigo ISO</td>
                                <td><input required type="text" name="iso_code" id="iso_code" maxlength="3" minlength="3" class="form-control"></td>
                            </tr>
                            <tr>
                                <td>Simbolo</td>
                                <td><input required type="text" name="simbol" id="simbol" maxlength="5" class="form-control"></td>
                            </tr>
                            <tr>
                                <td>Decimales significativos</td>
                                <td><input required type="number" name="significant_decimals" id="significant_decimals" min="0" max="12" class="form-control"></td>
                            </tr>
                            <tr>
                                <td>Separador de decimales</td>
                                <td><input required type="text" name="decimal_point" id="decimal_point" maxlength="1" class="form-control"></td>
                            </tr>
                            <tr>
                                <td>Separador de miles</td>
                                <td><input required type="text" name="thousands_separator" id="thousands_separator" maxlength="1" class="form-control"></td>
                            </tr>
                            <tr>
                                <td>Porcentaje de deflacion tolerable (con respecto a USD)</td>
                                <td><input required type="number" name="max_deflation_percentage" id="max_deflation_percentage" min="0.0001" max="1" step="0.0001" class="form-control"></td>
                            </tr>
                            <tr>
                                <td>Porcentaje de inflacion tolerable (con respecto a USD)</td>
                                <td><input required type="number" name="max_inflation_percentage" id="max_inflation_percentage" min="0.0001" max="1" step="0.0001" class="form-control"></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <button type="submit" class="btn btn-success pull-right">Enviar</button>
            </form>
        </div>
    </div>
    @include('styles.supr_arrows_in_text_fields')
@endsection