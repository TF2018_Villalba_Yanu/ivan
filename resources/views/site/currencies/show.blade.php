@php
    use App\Models\ExchangeRate;
    use App\Models\Currency;
@endphp
@extends('adminlte::page')

@section('title_prefix', 'Detalles de la divisa - ')

@section('content')
    <div class="box" id="box">
        <div class="box-header">
            <h1>Detalles de la divisa {!!$currency->id!!}
                    <a onclick="redirect('/currencies/{!!$currency->id!!}/edit')" class="btn btn-primary pull-right"><i class="fa fa-pencil" aria-hidden="true"></i> Editar</a>
            </h1>
        </div>
        <div class="box-body">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <th width="20%">Concepto</th>
                        <th width="20%">Valor</th>
                    </thead>
                    <tbody>
                        <tr>
                            <td>ID de divisa</td>
                            <td>{!!$currency->id!!}</td>
                        </tr>
                        <tr>
                            <td>Nombre</td>
                            <td>{!!$currency->name!!}</td>
                        </tr>
                        <tr>
                            <td>Codigo ISO</td>
                            <td>{!!$currency->iso_code!!}</td>
                        </tr>
                        <tr>
                            <td>Simbolo</td>
                            <td>{!!$currency->simbol!!}</td>
                        </tr>
                        <tr>
                            <td>Decimales significativos</td>
                            <td>{!!$currency->significant_decimals!!}</td>
                        </tr>
                        <tr>
                            <td>Separador de decimales</td>
                            <td>{!!$currency->decimal_point!!}</td>
                        </tr>
                        <tr>
                            <td>Separador de miles</td>
                            <td>{!!$currency->thousands_separator!!}</td>
                        </tr>
                        @if ($currency->id != Currency::getCurrency('usd')->id)
                            <tr>
                                <td>Equivalente de 1 USD a {!!$currency->iso_code!!}</td>
                                <td>{!!$currency->numberToString(ExchangeRate::localConvert('USD',$currency->iso_code, 1)).' ( tasa actualizada hace '.ExchangeRate::lastExchangeRate($currency,currency::getCurrency('USD'))->date->diffForHumans().')'!!}</td>
                            </tr> 
                        @endif
                        <tr>
                            <td>Deflacion tolerable (con respecto a USD)</td>
                            <td>{!!$currency->max_deflation_percentage!!}</td>
                        </tr>
                        <tr>
                            <td>Inflacion tolerable (con respecto a USD)</td>
                            <td>{!!$currency->max_inflation_percentage!!}</td>
                        </tr>
                    </tbody>
                </table>
                <br>
                <div class="table-responsive container-border">
                    <h3>Montos maximos de credito</h3>
                    <table class="table table-paginated table-striped table-hover">
                        <thead>
                            <th width="30px">Num</th>
                            <th>Profesion</th>
                            <th>Limite maximo</th>
                            <th>Maximo de créditos activos</th>
                            <th width="10%">Ver detalles</th>
                        </thead>
                        @php
                            $entry_num = 1;
                        @endphp   
                        <tbody>
                            @foreach ($max_amount_credits as $max_amount_credit)
                                <tr>
                                    <td>{!!$entry_num!!}</td>
                                    @php
                                        $entry_num++;
                                    @endphp
                                    <td>{!!$max_amount_credit->profession->name!!}</td>
                                    <td>{!!$max_amount_credit->currency->numberToString($max_amount_credit->max_limit,0)!!}</td>
                                    <td>{!!$max_amount_credit->active_credits_limit!!}</td>
                                    <td>
                                        <a onclick="redirect('/max-amounts/{!!$max_amount_credit->id!!}')" class="btn btn-primary">Detalles</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody> 
                    </table>
                </div>
            </div>
        </div>
        <div>
            <button class="btn btn-primary" onclick="printDiv('box')">Generar PDF</button>
        </div>
    </div>
@endsection