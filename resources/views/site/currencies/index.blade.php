@php

@endphp 

@extends('adminlte::page')

@section('title_prefix','Divisas  - ')

@section('content')
    <div class="box" id="box">
        <div class="box-header">
            <h1>Divisas
                <a onclick="redirect('/currencies/create')" class="btn btn-success pull-right"> Nueva divisa</a>
            </h1>
        </div>
        <div class="box-body">
            <div class="table-responsive container-border">
                <table class="table table-paginated table-hover table-striped">
                    <thead>
                        <th width="30px">Num</th>
                        <th>Nombre</th>
                        <th>Codigo ISO</th>
                        <th>Deflacion tolerable (con respecto a USD)</th>
                        <th>Inflacion tolerable (con respecto a USD)</th>
                        <th width="10%">Ver detalles</th>
                    </thead>
                    @php
                        $entry_num = 1;
                    @endphp
                    <tbody>
                        @foreach ($currencies as $currency)
                            <tr>
                                <td>{!!$entry_num!!}</td>
                                @php
                                    $entry_num++;
                                @endphp
                                <td>{!!ucfirst($currency->name)!!}</td>
                                <td>{!!$currency->iso_code!!}</td>
                                <td>{!!$currency->max_deflation_percentage!!}</td>
                                <td>{!!$currency->max_inflation_percentage!!}</td>
                                <td>
                                    <a onclick="redirect('/currencies/{!!$currency->id!!}')" class="btn btn-primary">Detalles</a>
                                </td>
                            </tr>                        
                        @endforeach
                    </tbody>        
                </table>
            </div>
        </div>
        <div>
            <button class="btn btn-primary" onclick="printDiv('box')">Generar PDF</button>
        </div>
    </div>
@stop