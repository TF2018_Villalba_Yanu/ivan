@php
    use App\Models\Role;
@endphp
@extends('adminlte::page')

@section('title_prefix', 'Detalles del rol - ')

@section('content')
    <div class="box" id="box">
        <div class="box-header">
            <h1>Detalles del rol {!!$role->id!!}
                <a onclick="redirect('/roles/{!!$role->id!!}/edit')" class="btn btn-primary pull-right"><i class="fa fa-pencil" aria-hidden="true"></i> Editar</a>
            </h1>
        </div>
        <div class="box-body">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <th width="20%">Concepto</th>
                        <th width="20%">Valor</th>
                    </thead>
                    <tbody>
                        <tr>
                            <td>ID del rol</td>
                            <td>{!!$role->id!!}</td>
                        </tr>
                        <tr>
                            <td>Nombre</td>
                            <td>{!!$role->name!!}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div>
            <button class="btn btn-primary" onclick="printDiv('box')">Generar PDF</button>
        </div>
    </div>
@endsection