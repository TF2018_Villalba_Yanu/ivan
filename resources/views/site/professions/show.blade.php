@php

@endphp
@extends('adminlte::page')

@section('title_prefix', 'Detalles de la profesion - ')

@section('content')
    <div class="box" id="box">
        <div class="box-header">
            <h1>Detalles de la profesion {!!$profession->id!!}
                <a onclick="redirect('/professions/{!!$profession->id!!}/edit')" class="btn btn-primary pull-right"><i class="fa fa-pencil" aria-hidden="true"></i> Editar</a>
            </h1>
        </div>
        <div class="box-body">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <th width="20%">Concepto</th>
                        <th width="20%">Valor</th>
                    </thead>
                    <tbody>
                        <tr>
                            <td>ID de profesione</td>
                            <td>{!!$profession->id!!}</td>
                        </tr>
                        <tr>
                            <td>Nombre</td>
                            <td>{!!$profession->name!!}</td>
                        </tr>
                    </tbody>
                </table>
                <br>
                <div class="table-responsive container-border">
                    <h3>Montos maximos de credito</h3>
                    <table class="table table-paginated table-striped table-hover">
                        <thead>
                            <th width="30px">Num</th>
                            <th>Divisa</th>
                            <th>Limite maximo</th>
                            <th>Maximo de créditos activos</th>
                            <th width="10%">Ver detalles</th>
                        </thead>
                        @php
                            $entry_num = 1;
                        @endphp 
                        <tbody>
                            @foreach ($max_amount_credits as $max_amount_credit)
                                <tr>
                                    <td>{!!$entry_num!!}</td>
                                    @php
                                        $entry_num++;
                                    @endphp
                                    <td>{!!$max_amount_credit->currency->name!!}</td>
                                    <td>{!!$max_amount_credit->currency->numberToString($max_amount_credit->max_limit)!!}</td>
                                    <td>{!!$max_amount_credit->active_credits_limit!!}</td>
                                    <td>
                                        <a onclick="redirect('/max-amounts/{!!$max_amount_credit->id!!}')" class="btn btn-primary">Detalles</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div>
            <button class="btn btn-primary" onclick="printDiv('box')">Generar PDF</button>
        </div>
    </div>
    @include('styles.supr_arrows_in_text_fields')
@endsection