@php
    use App\Models\PaymentStage;
@endphp 

@extends('adminlte::page')

@section('title_prefix','Profesiones  - ')

@section('content')
    <div class="box" id="box">
        <div class="box-header">
            <h1>Profesiones
                <a onclick="redirect('/professions/create')" class="btn btn-success pull-right">Nueva</a>
            </h1>
        </div>
        <div class="box-body">
            <div class="table-responsive container-border">
                <table class="table table-paginated table-hover table-striped">
                    <thead>
                    <th width="30px">Num</th>
                        <th>Nombre</th>
                        <th width="10%">Ver detalles</th>
                    </thead>
                    @php
                        $entry_num = 1;
                    @endphp 
                    <tbody>
                        @foreach ($professions as $profession)
                            <tr>
                                <td>{!!$entry_num!!}</td>
                                @php
                                    $entry_num++;
                                @endphp
                                <td>{!!$profession->name!!}</td>
                                <td>
                                    <a onclick="redirect('/professions/{!!$profession->id!!}')" class="btn btn-primary">Detalles</a>
                                </td>
                            </tr>                        
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div>
            <button class="btn btn-primary" onclick="printDiv('box')">Generar PDF</button>
        </div>
    </div>
@stop