
@extends('adminlte::page')

@section('title_prefix', 'Crear Profesion - ')

@section('content')
    <div class="box" id="box">
        <div class="box-header">
            <h1>Crear Profesion</h1>
        </div>
        <div class="box-body">
            <form action="/professions" method="post">
                {!!csrf_field()!!}
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <th width="20%">Concepto</th>
                            <th width="20%">Valor</th>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Nombre</td>
                                <td><input required type="text" name="name" id="name" maxlength="20" class="form-control"></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <button type="submit" class="btn btn-success pull-right">Enviar</button>
            </form>
        </div>
    </div>
    @include('styles.supr_arrows_in_text_fields')
@endsection