@php
    use App\Models\PaymentStage;
@endphp 

@extends('adminlte::page')

@section('title_prefix','Montos maximos  - ')

@section('content')
    <div class="box" id="box">
        <div class="box-header">
            <h1>Montos maximos</h1>
        </div>
        <div class="box-body">
            <div class="table-responsive container-border">
                <table class="table table-paginated table-hover table-striped">
                    <thead>
                        <th width="30px">Num</th>
                        <th>Profesion</th>
                        <th>Divisa</th>
                        <th>Lim inicial</th>
                        <th>Lim maximo</th>                
                        <th>Lim de créditos</th>
                        <th width="10%">Ver detalles</th>
                    </thead>
                    @php
                        $entry_num = 1;
                    @endphp 
                    <tbody>
                        @foreach ($max_amounts as $max_amount)
                            <tr>
                                <td>{!!$entry_num!!}</td>
                                @php
                                    $entry_num++;
                                @endphp
                                <td>{!!$max_amount->profession->name!!}</td>                        
                                <td>{!!$max_amount->currency->name!!}</td>                        
                                <td>{!!$max_amount->currency->numberToString($max_amount->initial_limit)!!}</td>
                                <td>{!!$max_amount->currency->numberToString($max_amount->max_limit)!!}</td>
                                <td>{!!$max_amount->active_credits_limit!!}</td>       
                                <td>
                                    <a  onclick="redirect('/max-amounts/{!!$max_amount->id!!}')" class="btn btn-primary">Detalles</a>
                                </td>              
                            </tr>                        
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div>
            <button class="btn btn-primary" onclick="printDiv('box')">Generar PDF</button>
        </div>
    </div>
@stop