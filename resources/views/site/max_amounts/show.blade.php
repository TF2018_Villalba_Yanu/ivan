@extends('adminlte::page')

@section('title_prefix', 'Detalles del monto maximo - ')

@section('content')
    <div class="box" id="box">
        <div class="box-header">
            <h1>Detalles del monto maximo {!!$max_amount->id!!}
                <a onclick="redirect('/max-amounts/{!!$max_amount->id!!}/edit')" class="btn btn-primary pull-right"><i class="fa fa-pencil" aria-hidden="true"></i> Editar</a>
            </h1>
        </div>
        <div class="box-body">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <th width="20%">Concepto</th>
                        <th width="20%">Valor</th>
                    </thead>
                    <tbody>
                        <tr>
                            <td>ID del monto maximo</td>
                            <td>{!!$max_amount->id!!}</td>
                        </tr>
                        <tr>
                            <td>Profesion</td>
                            <td>{!!$max_amount->profession->name!!}</td>                        
                        </tr>
                        <tr>
                            <td>Divisa</td>
                            <td>{!!$max_amount->currency->name!!}</td>                        
                        </tr>
                        <tr>
                            <td>Limite inicial</td>
                            <td>{!!$max_amount->currency->numberToString($max_amount->initial_limit)!!}</td>
                        </tr>
                        <tr>
                            <td>Limite maximo</td>                
                            <td>{!!$max_amount->currency->numberToString($max_amount->max_limit)!!}</td>
                        </tr>
                        <tr>
                            <td>Limite de créditos activos</td>
                            <td>{!!$max_amount->active_credits_limit!!}</td>                     
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div>
            <button class="btn btn-primary" onclick="printDiv('box')">Generar PDF</button>
        </div>     
    </div>
@endsection