@extends('adminlte::page')

@section('title_prefix', 'Editar del monto maximo - ')

@section('content')
    <div class="box" id="box">
        <div class="box-header">
            <h1>Editar del monto maximo</h1>
        </div>
        <form action="/max-amounts/{!!$max_amount->id!!}" method="post">
            {!!csrf_field()!!}
        <div class="box-body">
            <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <th>Concepto</th>
                        <th>Valor</th>
                    </thead>
                    <tbody>
                            <tr>
                                <th>Profesion</th>
                                <td>{!!$max_amount->profession->name!!}</td>                        
                            </tr>
                            <tr>
                                <th>Divisa</th>
                                <td>{!!$max_amount->currency->name!!}</td>                        
                            </tr>
                            <tr>
                                <th>Limite inicial ({!!$max_amount->currency->simbol!!})</th>
                                <td>
                                    <input required type="number" class="form-control" value="{!!$max_amount->initial_limit!!}" name="initial_limit" id="initial_limit" min="{!!$max_amount->currency->min_value!!}" step="{!!$max_amount->currency->min_value!!}">
                                </td>
                            </tr>
                            <tr>
                                <th>Limite maximo ({!!$max_amount->currency->simbol!!})</th>                
                                <td id="max_limit_min">
                                    <input required type="number" class="form-control" value="{!!$max_amount->max_limit!!}" name="max_limit" id="max_limit"  min="{!!$max_amount->initial_limit!!}" step="{!!$max_amount->currency->min_value!!}">
                                </td>
                            </tr>
                            <tr>
                                <th>Limite de créditos activos</th>
                                <th>
                                    <input required type="number" class="form-control" value="{!!$max_amount->active_credits_limit!!}" name="active_credits_limit" id="active_credits_limit"  min="1" max="25">
                                </th>
                            </tr>
                    </tbody>
                </table>
            </div>
            <button type="submit" class="btn btn-success pull-right">Enviar</button>
        </form>
        </div>

                
            
    </div>
    @include('styles.supr_arrows_in_text_fields')
@endsection