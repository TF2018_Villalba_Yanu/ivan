(function(){var a="' of type ",k="SCRIPT",n="array",p="function",q="google.charts.load",t="hasOwnProperty",u="number",v="object",w="pre-45",x="propertyIsEnumerable",y="string",z="text/javascript",A="toLocaleString";function B(){return function(b){return b}}function C(){return function(){}}function D(b){return function(){return this[b]}}function E(b){return function(){return b}}var F,H=H||{};H.scope={};
H.Kq=function(b,c,d){b instanceof String&&(b=String(b));for(var e=b.length,f=0;f<e;f++){var g=b[f];if(c.call(d,g,f,b))return{Vj:f,Gl:g}}return{Vj:-1,Gl:void 0}};H.vh=!1;H.Zl=!1;H.$l=!1;H.Ao=!1;H.defineProperty=H.vh||typeof Object.defineProperties==p?Object.defineProperty:function(b,c,d){b!=Array.prototype&&b!=Object.prototype&&(b[c]=d.value)};H.Dj=function(b){return"undefined"!=typeof window&&window===b?b:"undefined"!=typeof global&&null!=global?global:b};H.global=H.Dj(this);
H.Mk=function(b){if(b){for(var c=H.global,d=["Promise"],e=0;e<d.length-1;e++){var f=d[e];f in c||(c[f]={});c=c[f]}d=d[d.length-1];e=c[d];b=b(e);b!=e&&null!=b&&H.defineProperty(c,d,{configurable:!0,writable:!0,value:b})}};H.Yp=function(b,c,d){if(null==b)throw new TypeError("The 'this' value for String.prototype."+d+" must not be null or undefined");if(c instanceof RegExp)throw new TypeError("First argument to String.prototype."+d+" must not be a regular expression");return b+""};
H.Di=function(b){var c=0;return function(){return c<b.length?{done:!1,value:b[c++]}:{done:!0}}};H.Ci=function(b){return{next:H.Di(b)}};H.Gg=function(b){var c="undefined"!=typeof Symbol&&Symbol.iterator&&b[Symbol.iterator];return c?c.call(b):H.Ci(b)};H.Qh=!1;
H.Mk(function(b){function c(b){this.ba=g.ya;this.la=void 0;this.ub=[];var c=this.fd();try{b(c.resolve,c.reject)}catch(r){c.reject(r)}}function d(){this.La=null}function e(b){return b instanceof c?b:new c(function(c){c(b)})}if(b&&!H.Qh)return b;d.prototype.Xe=function(b){null==this.La&&(this.La=[],this.Hi());this.La.push(b)};d.prototype.Hi=function(){var b=this;this.Ye(function(){b.pj()})};var f=H.global.setTimeout;d.prototype.Ye=function(b){f(b,0)};d.prototype.pj=function(){for(;this.La&&this.La.length;){var b=
this.La;this.La=[];for(var c=0;c<b.length;++c){var d=b[c];b[c]=null;try{d()}catch(G){this.Ii(G)}}}this.La=null};d.prototype.Ii=function(b){this.Ye(function(){throw b;})};var g={ya:0,Ka:1,na:2};c.prototype.fd=function(){function b(b){return function(e){d||(d=!0,b.call(c,e))}}var c=this,d=!1;return{resolve:b(this.Rk),reject:b(this.Sd)}};c.prototype.Rk=function(b){if(b===this)this.Sd(new TypeError("A Promise cannot resolve to itself"));else if(b instanceof c)this.jl(b);else{a:switch(typeof b){case v:var d=
null!=b;break a;case p:d=!0;break a;default:d=!1}d?this.Qk(b):this.zf(b)}};c.prototype.Qk=function(b){var c=void 0;try{c=b.then}catch(r){this.Sd(r);return}typeof c==p?this.kl(c,b):this.zf(b)};c.prototype.Sd=function(b){this.$g(g.na,b)};c.prototype.zf=function(b){this.$g(g.Ka,b)};c.prototype.$g=function(b,c){if(this.ba!=g.ya)throw Error("Cannot settle("+b+", "+c+"): Promise already settled in state"+this.ba);this.ba=b;this.la=c;this.rj()};c.prototype.rj=function(){if(null!=this.ub){for(var b=0;b<this.ub.length;++b)h.Xe(this.ub[b]);
this.ub=null}};var h=new d;c.prototype.jl=function(b){var c=this.fd();b.dc(c.resolve,c.reject)};c.prototype.kl=function(b,c){var d=this.fd();try{b.call(c,d.resolve,d.reject)}catch(G){d.reject(G)}};c.prototype.then=function(b,d){function e(b,c){return typeof b==p?function(c){try{f(b(c))}catch(aa){g(aa)}}:c}var f,g,h=new c(function(b,c){f=b;g=c});this.dc(e(b,f),e(d,g));return h};c.prototype["catch"]=function(b){return this.then(void 0,b)};c.prototype.dc=function(b,c){function d(){switch(e.ba){case g.Ka:b(e.la);
break;case g.na:c(e.la);break;default:throw Error("Unexpected state: "+e.ba);}}var e=this;null==this.ub?h.Xe(d):this.ub.push(d)};c.resolve=e;c.reject=function(b){return new c(function(c,d){d(b)})};c.race=function(b){return new c(function(c,d){for(var f=H.Gg(b),g=f.next();!g.done;g=f.next())e(g.value).dc(c,d)})};c.all=function(b){var d=H.Gg(b),f=d.next();return f.done?e([]):new c(function(b,c){function g(c){return function(d){h[c]=d;l--;0==l&&b(h)}}var h=[],l=0;do h.push(void 0),l++,e(f.value).dc(g(h.length-
1),c),f=d.next();while(!f.done)})};return c});var I=I||{};I.global=this;I.W=function(b){return void 0!==b};I.N=function(b){return typeof b==y};I.Yj=function(b){return"boolean"==typeof b};I.Rb=function(b){return typeof b==u};I.ld=function(b,c,d){b=b.split(".");d=d||I.global;b[0]in d||"undefined"==typeof d.execScript||d.execScript("var "+b[0]);for(var e;b.length&&(e=b.shift());)!b.length&&I.W(c)?d[e]=c:d=d[e]&&d[e]!==Object.prototype[e]?d[e]:d[e]={}};I.define=function(b,c){I.ld(b,c)};I.Z=!0;I.I="en";
I.Xc=!0;I.mi=!1;I.Mh=!I.Z;I.Pm=!1;I.Ps=function(b){if(I.tg())throw Error("goog.provide cannot be used within a module.");I.gf(b)};I.gf=function(b,c){I.ld(b,c)};I.$f=function(){null===I.gd&&(I.gd=I.Hj());return I.gd};I.Yh=/^[\w+/_-]+[=]{0,2}$/;I.gd=null;I.Hj=function(){var b=I.global.document;return(b=b.querySelector&&b.querySelector("script[nonce]"))&&(b=b.nonce||b.getAttribute("nonce"))&&I.Yh.test(b)?b:""};I.ui=/^[a-zA-Z_$][a-zA-Z0-9._$]*$/;
I.uc=function(b){if(!I.N(b)||!b||-1==b.search(I.ui))throw Error("Invalid module identifier");if(!I.sg())throw Error("Module "+b+" has been loaded incorrectly. Note, modules cannot be loaded as normal scripts. They require some kind of pre-processing step. You're likely trying to load a module via a script tag or as a part of a concatenated bundle without rewriting the module. For more info see: https://github.com/google/closure-library/wiki/goog.module:-an-ES6-module-like-alternative-to-goog.provide.");
if(I.ia.Sb)throw Error("goog.module may only be called once per module.");I.ia.Sb=b};I.uc.get=E(null);I.uc.lr=E(null);I.Ab={we:"es6",Vc:"goog"};I.ia=null;I.tg=function(){return I.sg()||I.ck()};I.sg=function(){return!!I.ia&&I.ia.type==I.Ab.Vc};I.ck=function(){if(I.ia&&I.ia.type==I.Ab.we)return!0;var b=I.global.$jscomp;return b?typeof b.qd!=p?!1:!!b.qd():!1};I.uc.hd=function(){I.ia.hd=!0};
I.fj=function(b){if(I.ia)I.ia.Sb=b;else{var c=I.global.$jscomp;if(!c||typeof c.qd!=p)throw Error('Module with namespace "'+b+'" has been loaded incorrectly.');c=c.Ok(c.qd());I.Fg[b]={sj:c,type:I.Ab.we,Jk:b}}};I.uc.xq=I.fj;I.Jt=function(b){if(I.Mh)throw b=b||"",Error("Importing test-only code into non-debug environment"+(b?": "+b:"."));};I.Qq=C();I.ob=function(b){b=b.split(".");for(var c=I.global,d=0;d<b.length;d++)if(c=c[b[d]],!I.bb(c))return null;return c};
I.xr=function(b,c){c=c||I.global;for(var d in b)c[d]=b[d]};I.kp=C();I.nu=!1;I.Qm=!0;I.zk=function(b){I.global.console&&I.global.console.error(b)};I.Ok=C();I.bt=function(){return{}};I.Li="";I.cb=C();I.jp=function(){throw Error("unimplemented abstract method");};I.lp=function(b){b.Ed=void 0;b.kr=function(){if(b.Ed)return b.Ed;I.Z&&(I.lg[I.lg.length]=b);return b.Ed=new b}};I.lg=[];I.Ln=!0;I.ii=I.Z;I.Fg={};I.Bm=!1;I.Xo="detect";I.Yo="";I.oi="transpile.js";I.Cd=null;
I.El=function(){if(null==I.Cd){try{var b=!eval('"use strict";let x = 1; function f() { return typeof x; };f() == "number";')}catch(c){b=!1}I.Cd=b}return I.Cd};I.Ll=function(b){return"(function(){"+b+"\n;})();\n"};
I.ts=function(b){var c=I.ia;try{I.ia={Sb:"",hd:!1,type:I.Ab.Vc};if(I.Ba(b))var d=b.call(void 0,{});else if(I.N(b))I.El()&&(b=I.Ll(b)),d=I.wk.call(void 0,b);else throw Error("Invalid module definition");var e=I.ia.Sb;if(I.N(e)&&e)I.ia.hd?I.gf(e,d):I.ii&&Object.seal&&typeof d==v&&null!=d&&Object.seal(d),I.Fg[e]={sj:d,type:I.Ab.Vc,Jk:I.ia.Sb};else throw Error('Invalid module name "'+e+'"');}finally{I.ia=c}};I.wk=function(b){eval(b);return{}};
I.Es=function(b){b=b.split("/");for(var c=0;c<b.length;)"."==b[c]?b.splice(c,1):c&&".."==b[c]&&b[c-1]&&".."!=b[c-1]?b.splice(--c,2):c++;return b.join("/")};I.uk=function(b){if(I.global.Gh)return I.global.Gh(b);try{var c=new I.global.XMLHttpRequest;c.open("get",b,!1);c.send();return 0==c.status||200==c.status?c.responseText:null}catch(d){return null}};
I.eu=function(b,c,d){var e=I.global.$jscomp;e||(I.global.$jscomp=e={});var f=e.de;if(!f){var g=I.Li+I.oi,h=I.uk(g);if(h){(function(){eval(h+"\n//# sourceURL="+g)}).call(I.global);if(I.global.$gwtExport&&I.global.$gwtExport.$jscomp&&!I.global.$gwtExport.$jscomp.transpile)throw Error('The transpiler did not properly export the "transpile" method. $gwtExport: '+JSON.stringify(I.global.$gwtExport));I.global.$jscomp.de=I.global.$gwtExport.$jscomp.transpile;e=I.global.$jscomp;f=e.de}}if(!f){var l=" requires transpilation but no transpiler was found.";
l+=' Please add "//javascript/closure:transpiler" as a data dependency to ensure it is included.';f=e.de=function(b,c){I.zk(c+l);return b}}return f(b,c,d)};
I.ca=function(b){var c=typeof b;if(c==v)if(b){if(b instanceof Array)return n;if(b instanceof Object)return c;var d=Object.prototype.toString.call(b);if("[object Window]"==d)return v;if("[object Array]"==d||typeof b.length==u&&"undefined"!=typeof b.splice&&"undefined"!=typeof b.propertyIsEnumerable&&!b.propertyIsEnumerable("splice"))return n;if("[object Function]"==d||"undefined"!=typeof b.call&&"undefined"!=typeof b.propertyIsEnumerable&&!b.propertyIsEnumerable("call"))return p}else return"null";
else if(c==p&&"undefined"==typeof b.call)return v;return c};I.ds=function(b){return null===b};I.bb=function(b){return null!=b};I.isArray=function(b){return I.ca(b)==n};I.Nb=function(b){var c=I.ca(b);return c==n||c==v&&typeof b.length==u};I.Pr=function(b){return I.ka(b)&&typeof b.getFullYear==p};I.Ba=function(b){return I.ca(b)==p};I.ka=function(b){var c=typeof b;return c==v&&null!=b||c==p};I.bg=function(b){return b[I.Va]||(b[I.Va]=++I.wl)};I.Ar=function(b){return!!b[I.Va]};
I.Nk=function(b){null!==b&&"removeAttribute"in b&&b.removeAttribute(I.Va);try{delete b[I.Va]}catch(c){}};I.Va="closure_uid_"+(1E9*Math.random()>>>0);I.wl=0;I.jr=I.bg;I.Xs=I.Nk;I.Wi=function(b){var c=I.ca(b);if(c==v||c==n){if(typeof b.clone===p)return b.clone();c=c==n?[]:{};for(var d in b)c[d]=I.Wi(b[d]);return c}return b};I.Ni=function(b,c,d){return b.call.apply(b.bind,arguments)};
I.Mi=function(b,c,d){if(!b)throw Error();if(2<arguments.length){var e=Array.prototype.slice.call(arguments,2);return function(){var d=Array.prototype.slice.call(arguments);Array.prototype.unshift.apply(d,e);return b.apply(c,d)}}return function(){return b.apply(c,arguments)}};I.bind=function(b,c,d){I.bind=Function.prototype.bind&&-1!=Function.prototype.bind.toString().indexOf("native code")?I.Ni:I.Mi;return I.bind.apply(null,arguments)};
I.eb=function(b,c){var d=Array.prototype.slice.call(arguments,1);return function(){var c=d.slice();c.push.apply(c,arguments);return b.apply(this,c)}};I.zs=function(b,c){for(var d in c)b[d]=c[d]};I.now=I.Xc&&Date.now||function(){return+new Date};
I.wr=function(b){if(I.global.execScript)I.global.execScript(b,"JavaScript");else if(I.global.eval){if(null==I.ic){try{I.global.eval("var _evalTest_ = 1;")}catch(e){}if("undefined"!=typeof I.global._evalTest_){try{delete I.global._evalTest_}catch(e){}I.ic=!0}else I.ic=!1}if(I.ic)I.global.eval(b);else{var c=I.global.document,d=c.createElement(k);d.type=z;d.defer=!1;d.appendChild(c.createTextNode(b));c.head.appendChild(d);c.head.removeChild(d)}}else throw Error("goog.globalEval not available");};
I.ic=null;I.gr=function(b,c){function d(b){b=b.split("-");for(var c=[],d=0;d<b.length;d++)c.push(e(b[d]));return c.join("-")}function e(b){return I.lf[b]||b}if("."==String(b).charAt(0))throw Error('className passed in goog.getCssName must not start with ".". You passed: '+b);var f=I.lf?"BY_WHOLE"==I.ej?e:d:B();b=c?b+"-"+f(c):f(b);return I.global.Fh?I.global.Fh(b):b};I.rt=function(b,c){I.lf=b;I.ej=c};
I.mr=function(b,c){c&&(b=b.replace(/\{\$([^}]+)}/g,function(b,e){return null!=c&&e in c?c[e]:b}));return b};I.nr=B();I.rf=function(b,c){I.ld(b,c,void 0)};I.Jq=function(b,c,d){b[c]=d};I.$a=function(b,c){function d(){}d.prototype=c.prototype;b.Hc=c.prototype;b.prototype=new d;b.prototype.constructor=b;b.Ki=function(b,d,g){for(var e=Array(arguments.length-2),f=2;f<arguments.length;f++)e[f-2]=arguments[f];return c.prototype[d].apply(b,e)}};
I.Ki=function(b,c,d){var e=arguments.callee.caller;if(I.mi||I.Z&&!e)throw Error("arguments.caller not defined.  goog.base() cannot be used with strict mode code. See http://www.ecma-international.org/ecma-262/5.1/#sec-C");if("undefined"!==typeof e.Hc){for(var f=Array(arguments.length-1),g=1;g<arguments.length;g++)f[g-1]=arguments[g];return e.Hc.constructor.apply(b,f)}if(typeof c!=y&&"symbol"!=typeof c)throw Error("method names provided to goog.base must be a string or a symbol");f=Array(arguments.length-
2);for(g=2;g<arguments.length;g++)f[g-2]=arguments[g];g=!1;for(var h=b.constructor;h;h=h.Hc&&h.Hc.constructor)if(h.prototype[c]===e)g=!0;else if(g)return h.prototype[c].apply(b,f);if(b[c]===e)return b.constructor.prototype[c].apply(b,f);throw Error("goog.base called from a method of one name to a method of a different name");};I.scope=function(b){if(I.tg())throw Error("goog.scope is not supported within a module.");b.call(I.global)};
I.pa=function(b,c){var d=c.constructor,e=c.ol;d&&d!=Object.prototype.constructor||(d=function(){throw Error("cannot instantiate an interface (no constructor defined).");});d=I.pa.aj(d,b);b&&I.$a(d,b);delete c.constructor;delete c.ol;I.pa.We(d.prototype,c);null!=e&&(e instanceof Function?e(d):I.pa.We(d,e));return d};I.pa.hi=I.Z;
I.pa.aj=function(b,c){function d(){var c=b.apply(this,arguments)||this;c[I.Va]=c[I.Va];this.constructor===d&&e&&Object.seal instanceof Function&&Object.seal(c);return c}if(!I.pa.hi)return b;var e=!I.pa.nk(c);return d};I.pa.nk=function(b){return b&&b.prototype&&b.prototype[I.ri]};I.pa.He=["constructor",t,"isPrototypeOf",x,A,"toString","valueOf"];
I.pa.We=function(b,c){for(var d in c)Object.prototype.hasOwnProperty.call(c,d)&&(b[d]=c[d]);for(var e=0;e<I.pa.He.length;e++)d=I.pa.He[e],Object.prototype.hasOwnProperty.call(c,d)&&(b[d]=c[d])};I.Yt=C();I.ri="goog_defineClass_legacy_unsealable";I.debug={};I.debug.Error=function(b){if(Error.captureStackTrace)Error.captureStackTrace(this,I.debug.Error);else{var c=Error().stack;c&&(this.stack=c)}b&&(this.message=String(b))};I.$a(I.debug.Error,Error);I.debug.Error.prototype.name="CustomError";I.a={};I.a.fa={Ja:1,am:2,ac:3,pm:4,Sm:5,Rm:6,mo:7,wm:8,Sc:9,Jm:10,Nh:11,Zn:12};I.o={};I.o.ma=I.Z;I.o.Vb=function(b,c){I.debug.Error.call(this,I.o.ql(b,c))};I.$a(I.o.Vb,I.debug.Error);I.o.Vb.prototype.name="AssertionError";I.o.Kh=function(b){throw b;};I.o.jd=I.o.Kh;I.o.ql=function(b,c){b=b.split("%s");for(var d="",e=b.length-1,f=0;f<e;f++)d+=b[f]+(f<c.length?c[f]:"%s");return d+b[e]};I.o.Aa=function(b,c,d,e){var f="Assertion failed";if(d){f+=": "+d;var g=e}else b&&(f+=": "+b,g=c);b=new I.o.Vb(""+f,g||[]);I.o.jd(b)};I.o.vt=function(b){I.o.ma&&(I.o.jd=b)};
I.o.assert=function(b,c,d){I.o.ma&&!b&&I.o.Aa("",null,c,Array.prototype.slice.call(arguments,2));return b};I.o.ha=function(b,c){I.o.ma&&I.o.jd(new I.o.Vb("Failure"+(b?": "+b:""),Array.prototype.slice.call(arguments,1)))};I.o.Ip=function(b,c,d){I.o.ma&&!I.Rb(b)&&I.o.Aa("Expected number but got %s: %s.",[I.ca(b),b],c,Array.prototype.slice.call(arguments,2));return b};
I.o.Lp=function(b,c,d){I.o.ma&&!I.N(b)&&I.o.Aa("Expected string but got %s: %s.",[I.ca(b),b],c,Array.prototype.slice.call(arguments,2));return b};I.o.up=function(b,c,d){I.o.ma&&!I.Ba(b)&&I.o.Aa("Expected function but got %s: %s.",[I.ca(b),b],c,Array.prototype.slice.call(arguments,2));return b};I.o.Jp=function(b,c,d){I.o.ma&&!I.ka(b)&&I.o.Aa("Expected object but got %s: %s.",[I.ca(b),b],c,Array.prototype.slice.call(arguments,2));return b};
I.o.qp=function(b,c,d){I.o.ma&&!I.isArray(b)&&I.o.Aa("Expected array but got %s: %s.",[I.ca(b),b],c,Array.prototype.slice.call(arguments,2));return b};I.o.rp=function(b,c,d){I.o.ma&&!I.Yj(b)&&I.o.Aa("Expected boolean but got %s: %s.",[I.ca(b),b],c,Array.prototype.slice.call(arguments,2));return b};I.o.sp=function(b,c,d){!I.o.ma||I.ka(b)&&b.nodeType==I.a.fa.Ja||I.o.Aa("Expected Element but got %s: %s.",[I.ca(b),b],c,Array.prototype.slice.call(arguments,2));return b};
I.o.vp=function(b,c,d,e){!I.o.ma||b instanceof c||I.o.Aa("Expected instanceof %s but got %s.",[I.o.ag(c),I.o.ag(b)],d,Array.prototype.slice.call(arguments,3));return b};I.o.tp=function(b,c,d){!I.o.ma||typeof b==u&&isFinite(b)||I.o.Aa("Expected %s to be a finite number but it is not.",[b],c,Array.prototype.slice.call(arguments,2));return b};I.o.Kp=function(){for(var b in Object.prototype)I.o.ha(b+" should not be enumerable in Object.prototype.")};
I.o.ag=function(b){return b instanceof Function?b.displayName||b.name||"unknown type name":b instanceof Object?b.constructor.displayName||b.constructor.name||Object.prototype.toString.call(b):null===b?"null":typeof b};I.j={};I.Fa=I.Xc;I.j.Ca=!1;I.j.Lk=function(b){return b[b.length-1]};I.j.ps=I.j.Lk;I.j.indexOf=I.Fa&&(I.j.Ca||Array.prototype.indexOf)?function(b,c,d){return Array.prototype.indexOf.call(b,c,d)}:function(b,c,d){d=null==d?0:0>d?Math.max(0,b.length+d):d;if(I.N(b))return I.N(c)&&1==c.length?b.indexOf(c,d):-1;for(;d<b.length;d++)if(d in b&&b[d]===c)return d;return-1};
I.j.lastIndexOf=I.Fa&&(I.j.Ca||Array.prototype.lastIndexOf)?function(b,c,d){return Array.prototype.lastIndexOf.call(b,c,null==d?b.length-1:d)}:function(b,c,d){d=null==d?b.length-1:d;0>d&&(d=Math.max(0,b.length+d));if(I.N(b))return I.N(c)&&1==c.length?b.lastIndexOf(c,d):-1;for(;0<=d;d--)if(d in b&&b[d]===c)return d;return-1};
I.j.forEach=I.Fa&&(I.j.Ca||Array.prototype.forEach)?function(b,c,d){Array.prototype.forEach.call(b,c,d)}:function(b,c,d){for(var e=b.length,f=I.N(b)?b.split(""):b,g=0;g<e;g++)g in f&&c.call(d,f[g],g,b)};I.j.yf=function(b,c){for(var d=I.N(b)?b.split(""):b,e=b.length-1;0<=e;--e)e in d&&c.call(void 0,d[e],e,b)};
I.j.filter=I.Fa&&(I.j.Ca||Array.prototype.filter)?function(b,c,d){return Array.prototype.filter.call(b,c,d)}:function(b,c,d){for(var e=b.length,f=[],g=0,h=I.N(b)?b.split(""):b,l=0;l<e;l++)if(l in h){var m=h[l];c.call(d,m,l,b)&&(f[g++]=m)}return f};I.j.map=I.Fa&&(I.j.Ca||Array.prototype.map)?function(b,c,d){return Array.prototype.map.call(b,c,d)}:function(b,c,d){for(var e=b.length,f=Array(e),g=I.N(b)?b.split(""):b,h=0;h<e;h++)h in g&&(f[h]=c.call(d,g[h],h,b));return f};
I.j.reduce=I.Fa&&(I.j.Ca||Array.prototype.reduce)?function(b,c,d,e){e&&(c=I.bind(c,e));return Array.prototype.reduce.call(b,c,d)}:function(b,c,d,e){var f=d;I.j.forEach(b,function(d,h){f=c.call(e,f,d,h,b)});return f};I.j.reduceRight=I.Fa&&(I.j.Ca||Array.prototype.reduceRight)?function(b,c,d,e){e&&(c=I.bind(c,e));return Array.prototype.reduceRight.call(b,c,d)}:function(b,c,d,e){var f=d;I.j.yf(b,function(d,h){f=c.call(e,f,d,h,b)});return f};
I.j.some=I.Fa&&(I.j.Ca||Array.prototype.some)?function(b,c,d){return Array.prototype.some.call(b,c,d)}:function(b,c,d){for(var e=b.length,f=I.N(b)?b.split(""):b,g=0;g<e;g++)if(g in f&&c.call(d,f[g],g,b))return!0;return!1};I.j.every=I.Fa&&(I.j.Ca||Array.prototype.every)?function(b,c,d){return Array.prototype.every.call(b,c,d)}:function(b,c,d){for(var e=b.length,f=I.N(b)?b.split(""):b,g=0;g<e;g++)if(g in f&&!c.call(d,f[g],g,b))return!1;return!0};
I.j.count=function(b,c,d){var e=0;I.j.forEach(b,function(b,g,h){c.call(d,b,g,h)&&++e},d);return e};I.j.find=function(b,c,d){c=I.j.findIndex(b,c,d);return 0>c?null:I.N(b)?b.charAt(c):b[c]};I.j.findIndex=function(b,c,d){for(var e=b.length,f=I.N(b)?b.split(""):b,g=0;g<e;g++)if(g in f&&c.call(d,f[g],g,b))return g;return-1};I.j.Lq=function(b,c,d){c=I.j.tj(b,c,d);return 0>c?null:I.N(b)?b.charAt(c):b[c]};
I.j.tj=function(b,c,d){for(var e=I.N(b)?b.split(""):b,f=b.length-1;0<=f;f--)if(f in e&&c.call(d,e[f],f,b))return f;return-1};I.j.contains=function(b,c){return 0<=I.j.indexOf(b,c)};I.j.Qb=function(b){return 0==b.length};I.j.clear=function(b){if(!I.isArray(b))for(var c=b.length-1;0<=c;c--)delete b[c];b.length=0};I.j.Er=function(b,c){I.j.contains(b,c)||b.push(c)};I.j.hg=function(b,c,d){I.j.splice(b,d,0,c)};I.j.Gr=function(b,c,d){I.eb(I.j.splice,b,d,0).apply(null,c)};
I.j.insertBefore=function(b,c,d){var e;2==arguments.length||0>(e=I.j.indexOf(b,d))?b.push(c):I.j.hg(b,c,e)};I.j.remove=function(b,c){c=I.j.indexOf(b,c);var d;(d=0<=c)&&I.j.wb(b,c);return d};I.j.Zs=function(b,c){c=I.j.lastIndexOf(b,c);return 0<=c?(I.j.wb(b,c),!0):!1};I.j.wb=function(b,c){return 1==Array.prototype.splice.call(b,c,1).length};I.j.Ys=function(b,c,d){c=I.j.findIndex(b,c,d);return 0<=c?(I.j.wb(b,c),!0):!1};
I.j.Vs=function(b,c,d){var e=0;I.j.yf(b,function(f,g){c.call(d,f,g,b)&&I.j.wb(b,g)&&e++});return e};I.j.concat=function(b){return Array.prototype.concat.apply([],arguments)};I.j.join=function(b){return Array.prototype.concat.apply([],arguments)};I.j.jh=function(b){var c=b.length;if(0<c){for(var d=Array(c),e=0;e<c;e++)d[e]=b[e];return d}return[]};I.j.clone=I.j.jh;
I.j.extend=function(b,c){for(var d=1;d<arguments.length;d++){var e=arguments[d];if(I.Nb(e)){var f=b.length||0,g=e.length||0;b.length=f+g;for(var h=0;h<g;h++)b[f+h]=e[h]}else b.push(e)}};I.j.splice=function(b,c,d,e){return Array.prototype.splice.apply(b,I.j.slice(arguments,1))};I.j.slice=function(b,c,d){return 2>=arguments.length?Array.prototype.slice.call(b,c):Array.prototype.slice.call(b,c,d)};
I.j.Ws=function(b,c,d){function e(b){return I.ka(b)?"o"+I.bg(b):(typeof b).charAt(0)+b}c=c||b;d=d||e;for(var f={},g=0,h=0;h<b.length;){var l=b[h++],m=d(l);Object.prototype.hasOwnProperty.call(f,m)||(f[m]=!0,c[g++]=l)}c.length=g};I.j.Ze=function(b,c,d){return I.j.$e(b,d||I.j.Oa,!1,c)};I.j.Pp=function(b,c,d){return I.j.$e(b,c,!0,void 0,d)};I.j.$e=function(b,c,d,e,f){for(var g=0,h=b.length,l;g<h;){var m=g+h>>1;var r=d?c.call(f,b[m],m,b):c(e,b[m]);0<r?g=m+1:(h=m,l=!r)}return l?g:~g};
I.j.sort=function(b,c){b.sort(c||I.j.Oa)};I.j.St=function(b,c){for(var d=Array(b.length),e=0;e<b.length;e++)d[e]={index:e,value:b[e]};var f=c||I.j.Oa;I.j.sort(d,function(b,c){return f(b.value,c.value)||b.index-c.index});for(e=0;e<b.length;e++)b[e]=d[e].value};I.j.ml=function(b,c,d){var e=d||I.j.Oa;I.j.sort(b,function(b,d){return e(c(b),c(d))})};I.j.Pt=function(b,c,d){I.j.ml(b,function(b){return b[c]},d)};
I.j.ks=function(b,c,d){c=c||I.j.Oa;for(var e=1;e<b.length;e++){var f=c(b[e-1],b[e]);if(0<f||0==f&&d)return!1}return!0};I.j.Ib=function(b,c){if(!I.Nb(b)||!I.Nb(c)||b.length!=c.length)return!1;for(var d=b.length,e=I.j.gj,f=0;f<d;f++)if(!e(b[f],c[f]))return!1;return!0};I.j.eq=function(b,c,d){d=d||I.j.Oa;for(var e=Math.min(b.length,c.length),f=0;f<e;f++){var g=d(b[f],c[f]);if(0!=g)return g}return I.j.Oa(b.length,c.length)};I.j.Oa=function(b,c){return b>c?1:b<c?-1:0};
I.j.Ir=function(b,c){return-I.j.Oa(b,c)};I.j.gj=function(b,c){return b===c};I.j.Np=function(b,c,d){d=I.j.Ze(b,c,d);return 0>d?(I.j.hg(b,c,-(d+1)),!0):!1};I.j.Op=function(b,c,d){c=I.j.Ze(b,c,d);return 0<=c?I.j.wb(b,c):!1};I.j.Rp=function(b,c,d){for(var e={},f=0;f<b.length;f++){var g=b[f],h=c.call(d,g,f,b);I.W(h)&&(e[h]||(e[h]=[])).push(g)}return e};I.j.bu=function(b,c,d){var e={};I.j.forEach(b,function(f,g){e[c.call(d,f,g,b)]=f});return e};
I.j.Rs=function(b,c,d){var e=[],f=0,g=b;d=d||1;void 0!==c&&(f=b,g=c);if(0>d*(g-f))return[];if(0<d)for(b=f;b<g;b+=d)e.push(b);else for(b=f;b>g;b+=d)e.push(b);return e};I.j.repeat=function(b,c){for(var d=[],e=0;e<c;e++)d[e]=b;return d};I.j.flatten=function(b){for(var c=[],d=0;d<arguments.length;d++){var e=arguments[d];if(I.isArray(e))for(var f=0;f<e.length;f+=8192)for(var g=I.j.flatten.apply(null,I.j.slice(e,f,f+8192)),h=0;h<g.length;h++)c.push(g[h]);else c.push(e)}return c};
I.j.rotate=function(b,c){b.length&&(c%=b.length,0<c?Array.prototype.unshift.apply(b,b.splice(-c,c)):0>c&&Array.prototype.push.apply(b,b.splice(0,-c)));return b};I.j.Bs=function(b,c,d){c=Array.prototype.splice.call(b,c,1);Array.prototype.splice.call(b,d,0,c[0])};
I.j.ru=function(b){if(!arguments.length)return[];for(var c=[],d=arguments[0].length,e=1;e<arguments.length;e++)arguments[e].length<d&&(d=arguments[e].length);for(e=0;e<d;e++){for(var f=[],g=0;g<arguments.length;g++)f.push(arguments[g][e]);c.push(f)}return c};I.j.Ot=function(b,c){c=c||Math.random;for(var d=b.length-1;0<d;d--){var e=Math.floor(c()*(d+1)),f=b[d];b[d]=b[e];b[e]=f}};I.j.jq=function(b,c){var d=[];I.j.forEach(c,function(c){d.push(b[c])});return d};
I.j.gq=function(b,c,d){return I.j.concat.apply([],I.j.map(b,c,d))};I.async={};I.async.Xb=function(b,c,d){this.tk=d;this.dj=b;this.Pk=c;this.vc=0;this.qc=null};I.async.Xb.prototype.get=function(){if(0<this.vc){this.vc--;var b=this.qc;this.qc=b.next;b.next=null}else b=this.dj();return b};I.async.Xb.prototype.put=function(b){this.Pk(b);this.vc<this.tk&&(this.vc++,b.next=this.qc,this.qc=b)};I.debug.aa={};I.debug.Tm=C();I.debug.aa.vb=[];I.debug.aa.Qd=[];I.debug.aa.Mg=!1;I.debug.aa.register=function(b){I.debug.aa.vb[I.debug.aa.vb.length]=b;if(I.debug.aa.Mg)for(var c=I.debug.aa.Qd,d=0;d<c.length;d++)b(I.bind(c[d].Ml,c[d]))};I.debug.aa.As=function(b){I.debug.aa.Mg=!0;for(var c=I.bind(b.Ml,b),d=0;d<I.debug.aa.vb.length;d++)I.debug.aa.vb[d](c);I.debug.aa.Qd.push(b)};I.debug.aa.ku=function(b){var c=I.debug.aa.Qd;b=I.bind(b.s,b);for(var d=0;d<I.debug.aa.vb.length;d++)I.debug.aa.vb[d](b);c.length--};I.a.on=C();I.a.c=function(b){this.rl=b};I.a.c.prototype.toString=D("rl");I.a.c.Nl=new I.a.c("A");I.a.c.Ol=new I.a.c("ABBR");I.a.c.Ql=new I.a.c("ACRONYM");I.a.c.Rl=new I.a.c("ADDRESS");I.a.c.Vl=new I.a.c("APPLET");I.a.c.Wl=new I.a.c("AREA");I.a.c.Xl=new I.a.c("ARTICLE");I.a.c.Yl=new I.a.c("ASIDE");I.a.c.bm=new I.a.c("AUDIO");I.a.c.cm=new I.a.c("B");I.a.c.dm=new I.a.c("BASE");I.a.c.em=new I.a.c("BASEFONT");I.a.c.fm=new I.a.c("BDI");I.a.c.gm=new I.a.c("BDO");I.a.c.jm=new I.a.c("BIG");I.a.c.km=new I.a.c("BLOCKQUOTE");
I.a.c.lm=new I.a.c("BODY");I.a.c.re=new I.a.c("BR");I.a.c.mm=new I.a.c("BUTTON");I.a.c.nm=new I.a.c("CANVAS");I.a.c.om=new I.a.c("CAPTION");I.a.c.qm=new I.a.c("CENTER");I.a.c.rm=new I.a.c("CITE");I.a.c.sm=new I.a.c("CODE");I.a.c.tm=new I.a.c("COL");I.a.c.um=new I.a.c("COLGROUP");I.a.c.vm=new I.a.c("COMMAND");I.a.c.xm=new I.a.c("DATA");I.a.c.ym=new I.a.c("DATALIST");I.a.c.zm=new I.a.c("DD");I.a.c.Am=new I.a.c("DEL");I.a.c.Cm=new I.a.c("DETAILS");I.a.c.Dm=new I.a.c("DFN");I.a.c.Em=new I.a.c("DIALOG");
I.a.c.Fm=new I.a.c("DIR");I.a.c.Gm=new I.a.c("DIV");I.a.c.Hm=new I.a.c("DL");I.a.c.Km=new I.a.c("DT");I.a.c.Nm=new I.a.c("EM");I.a.c.Om=new I.a.c("EMBED");I.a.c.Vm=new I.a.c("FIELDSET");I.a.c.Wm=new I.a.c("FIGCAPTION");I.a.c.Xm=new I.a.c("FIGURE");I.a.c.Ym=new I.a.c("FONT");I.a.c.Zm=new I.a.c("FOOTER");I.a.c.$m=new I.a.c("FORM");I.a.c.an=new I.a.c("FRAME");I.a.c.bn=new I.a.c("FRAMESET");I.a.c.cn=new I.a.c("H1");I.a.c.dn=new I.a.c("H2");I.a.c.en=new I.a.c("H3");I.a.c.fn=new I.a.c("H4");I.a.c.gn=new I.a.c("H5");
I.a.c.hn=new I.a.c("H6");I.a.c.jn=new I.a.c("HEAD");I.a.c.kn=new I.a.c("HEADER");I.a.c.ln=new I.a.c("HGROUP");I.a.c.mn=new I.a.c("HR");I.a.c.nn=new I.a.c("HTML");I.a.c.pn=new I.a.c("I");I.a.c.sn=new I.a.c("IFRAME");I.a.c.tn=new I.a.c("IMG");I.a.c.un=new I.a.c("INPUT");I.a.c.vn=new I.a.c("INS");I.a.c.An=new I.a.c("ISINDEX");I.a.c.Dn=new I.a.c("KBD");I.a.c.En=new I.a.c("KEYGEN");I.a.c.Fn=new I.a.c("LABEL");I.a.c.Hn=new I.a.c("LEGEND");I.a.c.In=new I.a.c("LI");I.a.c.Jn=new I.a.c("LINK");I.a.c.Nn=new I.a.c("MAIN");
I.a.c.On=new I.a.c("MAP");I.a.c.Pn=new I.a.c("MARK");I.a.c.Qn=new I.a.c("MATH");I.a.c.Rn=new I.a.c("MENU");I.a.c.Sn=new I.a.c("MENUITEM");I.a.c.Tn=new I.a.c("META");I.a.c.Un=new I.a.c("METER");I.a.c.Wn=new I.a.c("NAV");I.a.c.Xn=new I.a.c("NOFRAMES");I.a.c.Yn=new I.a.c("NOSCRIPT");I.a.c.ao=new I.a.c("OBJECT");I.a.c.bo=new I.a.c("OL");I.a.c.co=new I.a.c("OPTGROUP");I.a.c.eo=new I.a.c("OPTION");I.a.c.fo=new I.a.c("OUTPUT");I.a.c.ho=new I.a.c("P");I.a.c.io=new I.a.c("PARAM");I.a.c.jo=new I.a.c("PICTURE");
I.a.c.lo=new I.a.c("PRE");I.a.c.no=new I.a.c("PROGRESS");I.a.c.Q=new I.a.c("Q");I.a.c.oo=new I.a.c("RP");I.a.c.po=new I.a.c("RT");I.a.c.qo=new I.a.c("RTC");I.a.c.ro=new I.a.c("RUBY");I.a.c.to=new I.a.c("S");I.a.c.wo=new I.a.c("SAMP");I.a.c.xo=new I.a.c(k);I.a.c.yo=new I.a.c("SECTION");I.a.c.zo=new I.a.c("SELECT");I.a.c.Bo=new I.a.c("SMALL");I.a.c.Co=new I.a.c("SOURCE");I.a.c.Do=new I.a.c("SPAN");I.a.c.Eo=new I.a.c("STRIKE");I.a.c.Fo=new I.a.c("STRONG");I.a.c.Go=new I.a.c("STYLE");I.a.c.Ho=new I.a.c("SUB");
I.a.c.Io=new I.a.c("SUMMARY");I.a.c.Jo=new I.a.c("SUP");I.a.c.Ko=new I.a.c("SVG");I.a.c.Lo=new I.a.c("TABLE");I.a.c.Mo=new I.a.c("TBODY");I.a.c.No=new I.a.c("TD");I.a.c.Oo=new I.a.c("TEMPLATE");I.a.c.Po=new I.a.c("TEXTAREA");I.a.c.Qo=new I.a.c("TFOOT");I.a.c.Ro=new I.a.c("TH");I.a.c.So=new I.a.c("THEAD");I.a.c.To=new I.a.c("TIME");I.a.c.Uo=new I.a.c("TITLE");I.a.c.Vo=new I.a.c("TR");I.a.c.Wo=new I.a.c("TRACK");I.a.c.$o=new I.a.c("TT");I.a.c.bp=new I.a.c("U");I.a.c.cp=new I.a.c("UL");I.a.c.ep=new I.a.c("VAR");
I.a.c.fp=new I.a.c("VIDEO");I.a.c.gp=new I.a.c("WBR");I.M={};I.M.Xi=function(b){return function(){return b}};I.M.Um=E(!1);I.M.Zo=E(!0);I.M.$n=E(null);I.M.Wj=B();I.M.error=function(b){return function(){throw Error(b);}};I.M.ha=function(b){return function(){throw b;}};I.M.lock=function(b,c){c=c||0;return function(){return b.apply(this,Array.prototype.slice.call(arguments,0,c))}};I.M.Is=function(b){return function(){return arguments[b]}};
I.M.Ns=function(b,c){var d=Array.prototype.slice.call(arguments,1);return function(){var c=Array.prototype.slice.call(arguments);c.push.apply(c,d);return b.apply(this,c)}};I.M.qu=function(b,c){return I.M.fl(b,I.M.Xi(c))};I.M.Hq=function(b,c){return function(d){return c?b==d:b===d}};I.M.fq=function(b,c){var d=arguments,e=d.length;return function(){var b;e&&(b=d[e-1].apply(this,arguments));for(var c=e-2;0<=c;c--)b=d[c].call(this,b);return b}};
I.M.fl=function(b){var c=arguments,d=c.length;return function(){for(var b,f=0;f<d;f++)b=c[f].apply(this,arguments);return b}};I.M.and=function(b){var c=arguments,d=c.length;return function(){for(var b=0;b<d;b++)if(!c[b].apply(this,arguments))return!1;return!0}};I.M.or=function(b){var c=arguments,d=c.length;return function(){for(var b=0;b<d;b++)if(c[b].apply(this,arguments))return!0;return!1}};I.M.Hs=function(b){return function(){return!b.apply(this,arguments)}};
I.M.create=function(b,c){function d(){}d.prototype=b.prototype;var e=new d;b.apply(e,Array.prototype.slice.call(arguments,1));return e};I.M.Bh=!0;I.M.Ri=function(b){var c=!1,d;return function(){if(!I.M.Bh)return b();c||(d=b(),c=!0);return d}};I.M.once=function(b){var c=b;return function(){if(c){var b=c;c=null;b()}}};I.M.vq=function(b,c,d){var e=0;return function(f){I.global.clearTimeout(e);var g=arguments;e=I.global.setTimeout(function(){b.apply(d,g)},c)}};
I.M.Zt=function(b,c,d){function e(){g=I.global.setTimeout(f,c);b.apply(d,l)}function f(){g=0;h&&(h=!1,e())}var g=0,h=!1,l=[];return function(b){l=arguments;g?h=!0:e()}};I.M.Ss=function(b,c,d){function e(){f=0}var f=0;return function(g){f||(f=I.global.setTimeout(e,c),b.apply(d,arguments))}};I.f={};I.f.Rc=!1;I.f.Ph=!1;I.f.Re={Fe:"\u00a0"};I.f.startsWith=function(b,c){return 0==b.lastIndexOf(c,0)};I.f.endsWith=function(b,c){var d=b.length-c.length;return 0<=d&&b.indexOf(c,d)==d};I.f.bd=function(b,c){return 0==I.f.af(c,b.substr(0,c.length))};I.f.Wp=function(b,c){return 0==I.f.af(c,b.substr(b.length-c.length,c.length))};I.f.Xp=function(b,c){return b.toLowerCase()==c.toLowerCase()};
I.f.Xt=function(b,c){for(var d=b.split("%s"),e="",f=Array.prototype.slice.call(arguments,1);f.length&&1<d.length;)e+=d.shift()+f.shift();return e+d.join("%s")};I.f.cq=function(b){return b.replace(/[\s\xa0]+/g," ").replace(/^\s+|\s+$/g,"")};I.f.Gd=function(b){return/^[\s\xa0]*$/.test(b)};I.f.Sr=function(b){return 0==b.length};I.f.Qb=I.f.Gd;I.f.$j=function(b){return I.f.Gd(I.f.Ek(b))};I.f.Rr=I.f.$j;I.f.Mr=function(b){return!/[^\t\n\r ]/.test(b)};I.f.Jr=function(b){return!/[^a-zA-Z]/.test(b)};
I.f.es=function(b){return!/[^0-9]/.test(b)};I.f.Kr=function(b){return!/[^a-zA-Z0-9]/.test(b)};I.f.ls=function(b){return" "==b};I.f.ms=function(b){return 1==b.length&&" "<=b&&"~">=b||"\u0080"<=b&&"\ufffd">=b};I.f.Vt=function(b){return b.replace(/(\r\n|\r|\n)+/g," ")};I.f.Ui=function(b){return b.replace(/(\r\n|\r|\n)/g,"\n")};I.f.Gs=function(b){return b.replace(/\xa0|\s/g," ")};I.f.Fs=function(b){return b.replace(/\xa0|[ \t]+/g," ")};
I.f.bq=function(b){return b.replace(/[\t\r\n ]+/g," ").replace(/^[\t\r\n ]+|[\t\r\n ]+$/g,"")};I.f.trim=I.Xc&&String.prototype.trim?function(b){return b.trim()}:function(b){return/^[\s\xa0]*([\s\S]*?)[\s\xa0]*$/.exec(b)[1]};I.f.trimLeft=function(b){return b.replace(/^[\s\xa0]+/,"")};I.f.trimRight=function(b){return b.replace(/[\s\xa0]+$/,"")};I.f.af=function(b,c){b=String(b).toLowerCase();c=String(c).toLowerCase();return b<c?-1:b==c?0:1};
I.f.Og=function(b,c,d){if(b==c)return 0;if(!b)return-1;if(!c)return 1;for(var e=b.toLowerCase().match(d),f=c.toLowerCase().match(d),g=Math.min(e.length,f.length),h=0;h<g;h++){d=e[h];var l=f[h];if(d!=l)return b=parseInt(d,10),!isNaN(b)&&(c=parseInt(l,10),!isNaN(c)&&b-c)?b-c:d<l?-1:1}return e.length!=f.length?e.length-f.length:b<c?-1:1};I.f.Hr=function(b,c){return I.f.Og(b,c,/\d+|\D+/g)};I.f.vj=function(b,c){return I.f.Og(b,c,/\d+|\.\d+|\D+/g)};I.f.Js=I.f.vj;I.f.mu=function(b){return encodeURIComponent(String(b))};
I.f.lu=function(b){return decodeURIComponent(b.replace(/\+/g," "))};I.f.Ng=function(b,c){return b.replace(/(\r\n|\r|\n)/g,c?"<br />":"<br>")};
I.f.va=function(b,c){if(c)b=b.replace(I.f.fe,"&amp;").replace(I.f.Ee,"&lt;").replace(I.f.Be,"&gt;").replace(I.f.Le,"&quot;").replace(I.f.Ne,"&#39;").replace(I.f.Ge,"&#0;"),I.f.Rc&&(b=b.replace(I.f.xe,"&#101;"));else{if(!I.f.th.test(b))return b;-1!=b.indexOf("&")&&(b=b.replace(I.f.fe,"&amp;"));-1!=b.indexOf("<")&&(b=b.replace(I.f.Ee,"&lt;"));-1!=b.indexOf(">")&&(b=b.replace(I.f.Be,"&gt;"));-1!=b.indexOf('"')&&(b=b.replace(I.f.Le,"&quot;"));-1!=b.indexOf("'")&&(b=b.replace(I.f.Ne,"&#39;"));-1!=b.indexOf("\x00")&&
(b=b.replace(I.f.Ge,"&#0;"));I.f.Rc&&-1!=b.indexOf("e")&&(b=b.replace(I.f.xe,"&#101;"))}return b};I.f.fe=/&/g;I.f.Ee=/</g;I.f.Be=/>/g;I.f.Le=/"/g;I.f.Ne=/'/g;I.f.Ge=/\x00/g;I.f.xe=/e/g;I.f.th=I.f.Rc?/[\x00&<>"'e]/:/[\x00&<>"']/;I.f.lh=function(b){return I.f.contains(b,"&")?!I.f.Ph&&"document"in I.global?I.f.mh(b):I.f.zl(b):b};I.f.iu=function(b,c){return I.f.contains(b,"&")?I.f.mh(b,c):b};
I.f.mh=function(b,c){var d={"&amp;":"&","&lt;":"<","&gt;":">","&quot;":'"'};var e=c?c.createElement("div"):I.global.document.createElement("div");return b.replace(I.f.Th,function(b,c){var f=d[b];if(f)return f;"#"==c.charAt(0)&&(c=Number("0"+c.substr(1)),isNaN(c)||(f=String.fromCharCode(c)));f||(e.innerHTML=b+" ",f=e.firstChild.nodeValue.slice(0,-1));return d[b]=f})};
I.f.zl=function(b){return b.replace(/&([^;]+);/g,function(b,d){switch(d){case "amp":return"&";case "lt":return"<";case "gt":return">";case "quot":return'"';default:return"#"!=d.charAt(0)||(d=Number("0"+d.substr(1)),isNaN(d))?b:String.fromCharCode(d)}})};I.f.Th=/&([^;\s<&]+);?/g;I.f.Jl=function(b){return I.f.Ng(b.replace(/  /g," &#160;"),void 0)};I.f.Os=function(b){return b.replace(/(^|[\n ]) /g,"$1"+I.f.Re.Fe)};
I.f.Wt=function(b,c){for(var d=c.length,e=0;e<d;e++){var f=1==d?c:c.charAt(e);if(b.charAt(0)==f&&b.charAt(b.length-1)==f)return b.substring(1,b.length-1)}return b};I.f.truncate=function(b,c,d){d&&(b=I.f.lh(b));b.length>c&&(b=b.substring(0,c-3)+"...");d&&(b=I.f.va(b));return b};I.f.gu=function(b,c,d,e){d&&(b=I.f.lh(b));e&&b.length>c?(e>c&&(e=c),b=b.substring(0,c-e)+"..."+b.substring(b.length-e)):b.length>c&&(e=Math.floor(c/2),b=b.substring(0,e+c%2)+"..."+b.substring(b.length-e));d&&(b=I.f.va(b));return b};
I.f.Zd={"\x00":"\\0","\b":"\\b","\f":"\\f","\n":"\\n","\r":"\\r","\t":"\\t","\x0B":"\\x0B",'"':'\\"',"\\":"\\\\","<":"<"};I.f.sc={"'":"\\'"};I.f.quote=function(b){b=String(b);for(var c=['"'],d=0;d<b.length;d++){var e=b.charAt(d),f=e.charCodeAt(0);c[d+1]=I.f.Zd[e]||(31<f&&127>f?e:I.f.pf(e))}c.push('"');return c.join("")};I.f.Iq=function(b){for(var c=[],d=0;d<b.length;d++)c[d]=I.f.pf(b.charAt(d));return c.join("")};
I.f.pf=function(b){if(b in I.f.sc)return I.f.sc[b];if(b in I.f.Zd)return I.f.sc[b]=I.f.Zd[b];var c=b.charCodeAt(0);if(31<c&&127>c)var d=b;else{if(256>c){if(d="\\x",16>c||256<c)d+="0"}else d="\\u",4096>c&&(d+="0");d+=c.toString(16).toUpperCase()}return I.f.sc[b]=d};I.f.contains=function(b,c){return-1!=b.indexOf(c)};I.f.bf=function(b,c){return I.f.contains(b.toLowerCase(),c.toLowerCase())};I.f.kq=function(b,c){return b&&c?b.split(c).length-1:0};I.f.wb=B();
I.f.remove=function(b,c){return b.replace(c,"")};I.f.Us=function(b,c){c=new RegExp(I.f.Rd(c),"g");return b.replace(c,"")};I.f.$s=function(b,c,d){c=new RegExp(I.f.Rd(c),"g");return b.replace(c,d.replace(/\$/g,"$$$$"))};I.f.Rd=function(b){return String(b).replace(/([-()\[\]{}+?*.$\^|,:#<!\\])/g,"\\$1").replace(/\x08/g,"\\x08")};I.f.repeat=String.prototype.repeat?function(b,c){return b.repeat(c)}:function(b,c){return Array(c+1).join(b)};
I.f.Ms=function(b,c,d){b=I.W(d)?b.toFixed(d):String(b);d=b.indexOf(".");-1==d&&(d=b.length);return I.f.repeat("0",Math.max(0,c-d))+b};I.f.Ek=function(b){return null==b?"":String(b)};I.f.Sp=function(b){return Array.prototype.join.call(arguments,"")};I.f.sr=function(){return Math.floor(2147483648*Math.random()).toString(36)+Math.abs(Math.floor(2147483648*Math.random())^I.now()).toString(36)};
I.f.Db=function(b,c){var d=0;b=I.f.trim(String(b)).split(".");c=I.f.trim(String(c)).split(".");for(var e=Math.max(b.length,c.length),f=0;0==d&&f<e;f++){var g=b[f]||"",h=c[f]||"";do{g=/(\d*)(\D*)(.*)/.exec(g)||["","","",""];h=/(\d*)(\D*)(.*)/.exec(h)||["","","",""];if(0==g[0].length&&0==h[0].length)break;d=I.f.cd(0==g[1].length?0:parseInt(g[1],10),0==h[1].length?0:parseInt(h[1],10))||I.f.cd(0==g[2].length,0==h[2].length)||I.f.cd(g[2],h[2]);g=g[3];h=h[3]}while(0==d)}return d};
I.f.cd=function(b,c){return b<c?-1:b>c?1:0};I.f.Br=function(b){for(var c=0,d=0;d<b.length;++d)c=31*c+b.charCodeAt(d)>>>0;return c};I.f.Al=2147483648*Math.random()|0;I.f.tq=function(){return"goog_"+I.f.Al++};I.f.au=function(b){var c=Number(b);return 0==c&&I.f.Gd(b)?NaN:c};I.f.Xr=function(b){return/^[a-z]+([A-Z][a-z]*)*$/.test(b)};I.f.ns=function(b){return/^([A-Z][a-z]*)+$/.test(b)};I.f.$t=function(b){return String(b).replace(/\-([a-z])/g,function(b,d){return d.toUpperCase()})};
I.f.cu=function(b){return String(b).replace(/([A-Z])/g,"-$1").toLowerCase()};I.f.du=function(b,c){c=I.N(c)?I.f.Rd(c):"\\s";return b.replace(new RegExp("(^"+(c?"|["+c+"]+":"")+")([a-z])","g"),function(b,c,f){return c+f.toUpperCase()})};I.f.Vp=function(b){return String(b.charAt(0)).toUpperCase()+String(b.substr(1)).toLowerCase()};I.f.parseInt=function(b){isFinite(b)&&(b=String(b));return I.N(b)?/^\s*-?0x/i.test(b)?parseInt(b,16):parseInt(b,10):NaN};
I.f.Qt=function(b,c,d){b=b.split(c);for(var e=[];0<d&&b.length;)e.push(b.shift()),d--;b.length&&e.push(b.join(c));return e};I.f.qs=function(b,c){if(c)typeof c==y&&(c=[c]);else return b;for(var d=-1,e=0;e<c.length;e++)if(""!=c[e]){var f=b.lastIndexOf(c[e]);f>d&&(d=f)}return-1==d?b:b.slice(d+1)};
I.f.Cq=function(b,c){var d=[],e=[];if(b==c)return 0;if(!b.length||!c.length)return Math.max(b.length,c.length);for(var f=0;f<c.length+1;f++)d[f]=f;for(f=0;f<b.length;f++){e[0]=f+1;for(var g=0;g<c.length;g++)e[g+1]=Math.min(e[g]+1,d[g+1]+1,d[g]+Number(b[f]!=c[g]));for(g=0;g<d.length;g++)d[g]=e[g]}return e[c.length]};I.g={};I.g.userAgent={};I.g.userAgent.A={};I.g.userAgent.A.Of=function(){var b=I.g.userAgent.A.Fj();return b&&(b=b.userAgent)?b:""};I.g.userAgent.A.Fj=function(){return I.global.navigator};I.g.userAgent.A.nh=I.g.userAgent.A.Of();I.g.userAgent.A.Lt=function(b){I.g.userAgent.A.nh=b||I.g.userAgent.A.Of()};I.g.userAgent.A.pb=function(){return I.g.userAgent.A.nh};I.g.userAgent.A.L=function(b){return I.f.contains(I.g.userAgent.A.pb(),b)};
I.g.userAgent.A.Jg=function(b){return I.f.bf(I.g.userAgent.A.pb(),b)};I.g.userAgent.A.sf=function(b){for(var c=/(\w[\w ]+)\/([^\s]+)\s*(?:\((.*?)\))?/g,d=[],e;e=c.exec(b);)d.push([e[1],e[2],e[3]||void 0]);return d};I.object={};I.object.is=function(b,c){return b===c?0!==b||1/b===1/c:b!==b&&c!==c};I.object.forEach=function(b,c,d){for(var e in b)c.call(d,b[e],e,b)};I.object.filter=function(b,c,d){var e={},f;for(f in b)c.call(d,b[f],f,b)&&(e[f]=b[f]);return e};I.object.map=function(b,c,d){var e={},f;for(f in b)e[f]=c.call(d,b[f],f,b);return e};I.object.some=function(b,c,d){for(var e in b)if(c.call(d,b[e],e,b))return!0;return!1};I.object.every=function(b,c,d){for(var e in b)if(!c.call(d,b[e],e,b))return!1;return!0};
I.object.fr=function(b){var c=0,d;for(d in b)c++;return c};I.object.dr=function(b){for(var c in b)return c};I.object.er=function(b){for(var c in b)return b[c]};I.object.contains=function(b,c){return I.object.Zi(b,c)};I.object.vr=function(b){var c=[],d=0,e;for(e in b)c[d++]=b[e];return c};I.object.Mf=function(b){var c=[],d=0,e;for(e in b)c[d++]=e;return c};I.object.ur=function(b,c){var d=I.Nb(c),e=d?c:arguments;for(d=d?0:1;d<e.length;d++){if(null==b)return;b=b[e[d]]}return b};
I.object.Yi=function(b,c){return null!==b&&c in b};I.object.Zi=function(b,c){for(var d in b)if(b[d]==c)return!0;return!1};I.object.uj=function(b,c,d){for(var e in b)if(c.call(d,b[e],e,b))return e};I.object.Mq=function(b,c,d){return(c=I.object.uj(b,c,d))&&b[c]};I.object.Qb=function(b){for(var c in b)return!1;return!0};I.object.clear=function(b){for(var c in b)delete b[c]};I.object.remove=function(b,c){var d;(d=c in b)&&delete b[c];return d};
I.object.add=function(b,c,d){if(null!==b&&c in b)throw Error('The object already contains the key "'+c+'"');I.object.set(b,c,d)};I.object.get=function(b,c,d){return null!==b&&c in b?b[c]:d};I.object.set=function(b,c,d){b[c]=d};I.object.yt=function(b,c,d){return c in b?b[c]:b[c]=d};I.object.Nt=function(b,c,d){if(c in b)return b[c];d=d();return b[c]=d};I.object.Ib=function(b,c){for(var d in b)if(!(d in c)||b[d]!==c[d])return!1;for(d in c)if(!(d in b))return!1;return!0};
I.object.clone=function(b){var c={},d;for(d in b)c[d]=b[d];return c};I.object.Bl=function(b){var c=I.ca(b);if(c==v||c==n){if(I.Ba(b.clone))return b.clone();c=c==n?[]:{};for(var d in b)c[d]=I.object.Bl(b[d]);return c}return b};I.object.fu=function(b){var c={},d;for(d in b)c[b[d]]=d;return c};I.object.Ke=["constructor",t,"isPrototypeOf",x,A,"toString","valueOf"];
I.object.extend=function(b,c){for(var d,e,f=1;f<arguments.length;f++){e=arguments[f];for(d in e)b[d]=e[d];for(var g=0;g<I.object.Ke.length;g++)d=I.object.Ke[g],Object.prototype.hasOwnProperty.call(e,d)&&(b[d]=e[d])}};I.object.create=function(b){var c=arguments.length;if(1==c&&I.isArray(arguments[0]))return I.object.create.apply(null,arguments[0]);if(c%2)throw Error("Uneven number of arguments");for(var d={},e=0;e<c;e+=2)d[arguments[e]]=arguments[e+1];return d};
I.object.bj=function(b){var c=arguments.length;if(1==c&&I.isArray(arguments[0]))return I.object.bj.apply(null,arguments[0]);for(var d={},e=0;e<c;e++)d[arguments[e]]=!0;return d};I.object.mq=function(b){var c=b;Object.isFrozen&&!Object.isFrozen(b)&&(c=Object.create(b),Object.freeze(c));return c};I.object.Ur=function(b){return!!Object.isFrozen&&Object.isFrozen(b)};
I.object.cr=function(b,c,d){if(!b)return[];if(!Object.getOwnPropertyNames||!Object.getPrototypeOf)return I.object.Mf(b);for(var e={};b&&(b!==Object.prototype||c)&&(b!==Function.prototype||d);){for(var f=Object.getOwnPropertyNames(b),g=0;g<f.length;g++)e[f[g]]=!0;b=Object.getPrototypeOf(b)}return I.object.Mf(e)};I.g.userAgent.w={};I.g.userAgent.w.Hg=function(){return I.g.userAgent.A.L("Opera")};I.g.userAgent.w.Hk=function(){return I.g.userAgent.A.L("Trident")||I.g.userAgent.A.L("MSIE")};I.g.userAgent.w.Od=function(){return I.g.userAgent.A.L("Edge")};I.g.userAgent.w.Pd=function(){return I.g.userAgent.A.L("Firefox")||I.g.userAgent.A.L("FxiOS")};
I.g.userAgent.w.Ig=function(){return I.g.userAgent.A.L("Safari")&&!(I.g.userAgent.w.Md()||I.g.userAgent.w.Nd()||I.g.userAgent.w.Hg()||I.g.userAgent.w.Od()||I.g.userAgent.w.Pd()||I.g.userAgent.w.Ag()||I.g.userAgent.A.L("Android"))};I.g.userAgent.w.Nd=function(){return I.g.userAgent.A.L("Coast")};I.g.userAgent.w.Ik=function(){return(I.g.userAgent.A.L("iPad")||I.g.userAgent.A.L("iPhone"))&&!I.g.userAgent.w.Ig()&&!I.g.userAgent.w.Md()&&!I.g.userAgent.w.Nd()&&!I.g.userAgent.w.Pd()&&I.g.userAgent.A.L("AppleWebKit")};
I.g.userAgent.w.Md=function(){return(I.g.userAgent.A.L("Chrome")||I.g.userAgent.A.L("CriOS"))&&!I.g.userAgent.w.Od()};I.g.userAgent.w.Gk=function(){return I.g.userAgent.A.L("Android")&&!(I.g.userAgent.w.og()||I.g.userAgent.w.ak()||I.g.userAgent.w.Kd()||I.g.userAgent.w.Ag())};I.g.userAgent.w.Kd=I.g.userAgent.w.Hg;I.g.userAgent.w.rc=I.g.userAgent.w.Hk;I.g.userAgent.w.Ra=I.g.userAgent.w.Od;I.g.userAgent.w.ak=I.g.userAgent.w.Pd;I.g.userAgent.w.js=I.g.userAgent.w.Ig;I.g.userAgent.w.Or=I.g.userAgent.w.Nd;
I.g.userAgent.w.Wr=I.g.userAgent.w.Ik;I.g.userAgent.w.og=I.g.userAgent.w.Md;I.g.userAgent.w.Lr=I.g.userAgent.w.Gk;I.g.userAgent.w.Ag=function(){return I.g.userAgent.A.L("Silk")};
I.g.userAgent.w.Lb=function(){function b(b){b=I.j.find(b,e);return d[b]||""}var c=I.g.userAgent.A.pb();if(I.g.userAgent.w.rc())return I.g.userAgent.w.Ej(c);c=I.g.userAgent.A.sf(c);var d={};I.j.forEach(c,function(b){d[b[0]]=b[1]});var e=I.eb(I.object.Yi,d);return I.g.userAgent.w.Kd()?b(["Version","Opera"]):I.g.userAgent.w.Ra()?b(["Edge"]):I.g.userAgent.w.og()?b(["Chrome","CriOS"]):(c=c[2])&&c[1]||""};I.g.userAgent.w.xa=function(b){return 0<=I.f.Db(I.g.userAgent.w.Lb(),b)};
I.g.userAgent.w.Ej=function(b){var c=/rv: *([\d\.]*)/.exec(b);if(c&&c[1])return c[1];c="";var d=/MSIE +([\d\.]+)/.exec(b);if(d&&d[1])if(b=/Trident\/(\d.\d)/.exec(b),"7.0"==d[1])if(b&&b[1])switch(b[1]){case "4.0":c="8.0";break;case "5.0":c="9.0";break;case "6.0":c="10.0";break;case "7.0":c="11.0"}else c="7.0";else c=d[1];return c};I.g.userAgent.V={};I.g.userAgent.V.jk=function(){return I.g.userAgent.A.L("Presto")};I.g.userAgent.V.mk=function(){return I.g.userAgent.A.L("Trident")||I.g.userAgent.A.L("MSIE")};I.g.userAgent.V.Ra=function(){return I.g.userAgent.A.L("Edge")};I.g.userAgent.V.Cg=function(){return I.g.userAgent.A.Jg("WebKit")&&!I.g.userAgent.V.Ra()};I.g.userAgent.V.bk=function(){return I.g.userAgent.A.L("Gecko")&&!I.g.userAgent.V.Cg()&&!I.g.userAgent.V.mk()&&!I.g.userAgent.V.Ra()};
I.g.userAgent.V.Lb=function(){var b=I.g.userAgent.A.pb();if(b){b=I.g.userAgent.A.sf(b);var c=I.g.userAgent.V.Cj(b);if(c)return"Gecko"==c[0]?I.g.userAgent.V.Nj(b):c[1];b=b[0];var d;if(b&&(d=b[2])&&(d=/Trident\/([^\s;]+)/.exec(d)))return d[1]}return""};I.g.userAgent.V.Cj=function(b){if(!I.g.userAgent.V.Ra())return b[1];for(var c=0;c<b.length;c++){var d=b[c];if("Edge"==d[0])return d}};I.g.userAgent.V.xa=function(b){return 0<=I.f.Db(I.g.userAgent.V.Lb(),b)};
I.g.userAgent.V.Nj=function(b){return(b=I.j.find(b,function(b){return"Firefox"==b[0]}))&&b[1]||""};I.async.gh=function(b){I.global.setTimeout(function(){throw b;},0)};I.async.qa=function(b,c,d){var e=b;c&&(e=I.bind(b,c));e=I.async.qa.ph(e);I.Ba(I.global.setImmediate)&&(d||I.async.qa.Fl())?I.global.setImmediate(e):(I.async.qa.Yg||(I.async.qa.Yg=I.async.qa.Jj()),I.async.qa.Yg(e))};I.async.qa.Fl=function(){return I.global.Window&&I.global.Window.prototype&&!I.g.userAgent.w.Ra()&&I.global.Window.prototype.setImmediate==I.global.setImmediate?!1:!0};
I.async.qa.Jj=function(){var b=I.global.MessageChannel;"undefined"===typeof b&&"undefined"!==typeof window&&window.postMessage&&window.addEventListener&&!I.g.userAgent.V.jk()&&(b=function(){var b=document.createElement("IFRAME");b.style.display="none";b.src="";document.documentElement.appendChild(b);var c=b.contentWindow;b=c.document;b.open();b.write("");b.close();var d="callImmediate"+Math.random(),e="file:"==c.location.protocol?"*":c.location.protocol+"//"+c.location.host;b=I.bind(function(b){if(("*"==
e||b.origin==e)&&b.data==d)this.port1.onmessage()},this);c.addEventListener("message",b,!1);this.port1={};this.port2={postMessage:function(){c.postMessage(d,e)}}});if("undefined"!==typeof b&&!I.g.userAgent.w.rc()){var c=new b,d={},e=d;c.port1.onmessage=function(){if(I.W(d.next)){d=d.next;var b=d.cf;d.cf=null;b()}};return function(b){e.next={cf:b};e=e.next;c.port2.postMessage(0)}}return"undefined"!==typeof document&&"onreadystatechange"in document.createElement(k)?function(b){var c=document.createElement(k);
c.onreadystatechange=function(){c.onreadystatechange=null;c.parentNode.removeChild(c);c=null;b();b=null};document.documentElement.appendChild(c)}:function(b){I.global.setTimeout(b,0)}};I.async.qa.ph=I.M.Wj;I.debug.aa.register(function(b){I.async.qa.ph=b});I.async.Ga=function(){this.Lc=this.yb=null};I.async.Ga.Qc=100;I.async.Ga.Kb=new I.async.Xb(function(){return new I.async.Yc},function(b){b.reset()},I.async.Ga.Qc);I.async.Ga.prototype.add=function(b,c){var d=I.async.Ga.Kb.get();d.set(b,c);this.Lc?this.Lc.next=d:this.yb=d;this.Lc=d};I.async.Ga.prototype.remove=function(){var b=null;this.yb&&(b=this.yb,this.yb=this.yb.next,this.yb||(this.Lc=null),b.next=null);return b};I.async.Yc=function(){this.next=this.scope=this.nd=null};
I.async.Yc.prototype.set=function(b,c){this.nd=b;this.scope=c;this.next=null};I.async.Yc.prototype.reset=function(){this.next=this.scope=this.nd=null};I.xh=!1;I.async.P=function(b,c){I.async.P.Ec||I.async.P.Xj();I.async.P.Kc||(I.async.P.Ec(),I.async.P.Kc=!0);I.async.P.ee.add(b,c)};I.async.P.Xj=function(){if(I.xh||I.global.Promise&&I.global.Promise.resolve){var b=I.global.Promise.resolve(void 0);I.async.P.Ec=function(){b.then(I.async.P.Ac)}}else I.async.P.Ec=function(){I.async.qa(I.async.P.Ac)}};I.async.P.Oq=function(b){I.async.P.Ec=function(){I.async.qa(I.async.P.Ac);b&&b(I.async.P.Ac)}};I.async.P.Kc=!1;I.async.P.ee=new I.async.Ga;
I.Z&&(I.async.P.ct=function(){I.async.P.Kc=!1;I.async.P.ee=new I.async.Ga});I.async.P.Ac=function(){for(var b;b=I.async.P.ee.remove();){try{b.nd.call(b.scope)}catch(c){I.async.gh(c)}I.async.Ga.Kb.put(b)}I.async.P.Kc=!1};I.a.o={};I.a.o.Gp=C();I.a.o.Zc=B();I.a.o.wp=C();I.a.o.Ei=function(b){return I.a.o.Zc(b)};I.a.o.Cp=C();I.a.o.Bp=C();I.a.o.xp=C();I.a.o.Fp=C();I.a.o.Gi=function(b){return I.a.o.Zc(b)};I.a.o.yp=C();I.a.o.Fi=function(b){return I.a.o.Zc(b)};I.a.o.zp=C();I.a.o.Ap=C();I.a.o.Dp=C();I.a.o.Ep=C();I.a.o.wq=function(b){return I.ka(b)?b.constructor.displayName||b.constructor.name||Object.prototype.toString.call(b):void 0===b?"undefined":null===b?"null":typeof b};
I.a.o.nc=function(b){return(b=b&&b.ownerDocument)&&(b.defaultView||b.parentWindow)||I.global};I.g.userAgent.platform={};I.g.userAgent.platform.ng=function(){return I.g.userAgent.A.L("Android")};I.g.userAgent.platform.xg=function(){return I.g.userAgent.A.L("iPod")};I.g.userAgent.platform.wg=function(){return I.g.userAgent.A.L("iPhone")&&!I.g.userAgent.A.L("iPod")&&!I.g.userAgent.A.L("iPad")};I.g.userAgent.platform.vg=function(){return I.g.userAgent.A.L("iPad")};I.g.userAgent.platform.ug=function(){return I.g.userAgent.platform.wg()||I.g.userAgent.platform.vg()||I.g.userAgent.platform.xg()};
I.g.userAgent.platform.yg=function(){return I.g.userAgent.A.L("Macintosh")};I.g.userAgent.platform.gk=function(){return I.g.userAgent.A.L("Linux")};I.g.userAgent.platform.Eg=function(){return I.g.userAgent.A.L("Windows")};I.g.userAgent.platform.pg=function(){return I.g.userAgent.A.L("CrOS")};I.g.userAgent.platform.Nr=function(){return I.g.userAgent.A.L("CrKey")};I.g.userAgent.platform.ek=function(){return I.g.userAgent.A.Jg("KaiOS")};
I.g.userAgent.platform.Lb=function(){var b=I.g.userAgent.A.pb(),c="";I.g.userAgent.platform.Eg()?(c=/Windows (?:NT|Phone) ([0-9.]+)/,c=(b=c.exec(b))?b[1]:"0.0"):I.g.userAgent.platform.ug()?(c=/(?:iPhone|iPod|iPad|CPU)\s+OS\s+(\S+)/,c=(b=c.exec(b))&&b[1].replace(/_/g,".")):I.g.userAgent.platform.yg()?(c=/Mac OS X ([0-9_.]+)/,c=(b=c.exec(b))?b[1].replace(/_/g,"."):"10"):I.g.userAgent.platform.ng()?(c=/Android\s+([^\);]+)(\)|;)/,c=(b=c.exec(b))&&b[1]):I.g.userAgent.platform.pg()&&(c=/(?:CrOS\s+(?:i686|x86_64)\s+([0-9.]+))/,
c=(b=c.exec(b))&&b[1]);return c||""};I.g.userAgent.platform.xa=function(b){return 0<=I.f.Db(I.g.userAgent.platform.Lb(),b)};I.Ia={};I.Ia.object=function(b,c){return c};I.Ia.Yd=function(b){I.Ia.Yd[" "](b);return b};I.Ia.Yd[" "]=I.cb;I.Ia.Tp=function(b,c){try{return I.Ia.Yd(b[c]),!0}catch(d){}return!1};I.Ia.cache=function(b,c,d,e){e=e?e(c):c;return Object.prototype.hasOwnProperty.call(b,e)?b[e]:b[e]=d(c)};I.userAgent={};I.userAgent.je=!1;I.userAgent.he=!1;I.userAgent.ie=!1;I.userAgent.oe=!1;I.userAgent.Pc=!1;I.userAgent.me=!1;I.userAgent.uh=!1;I.userAgent.zb=I.userAgent.je||I.userAgent.he||I.userAgent.ie||I.userAgent.Pc||I.userAgent.oe||I.userAgent.me;I.userAgent.Mj=function(){return I.g.userAgent.A.pb()};I.userAgent.zd=function(){return I.global.navigator||null};I.userAgent.pr=function(){return I.userAgent.zd()};I.userAgent.Ie=I.userAgent.zb?I.userAgent.me:I.g.userAgent.w.Kd();
I.userAgent.$=I.userAgent.zb?I.userAgent.je:I.g.userAgent.w.rc();I.userAgent.ve=I.userAgent.zb?I.userAgent.he:I.g.userAgent.V.Ra();I.userAgent.Mm=I.userAgent.ve||I.userAgent.$;I.userAgent.Tc=I.userAgent.zb?I.userAgent.ie:I.g.userAgent.V.bk();I.userAgent.Bb=I.userAgent.zb?I.userAgent.oe||I.userAgent.Pc:I.g.userAgent.V.Cg();I.userAgent.ik=function(){return I.userAgent.Bb&&I.g.userAgent.A.L("Mobile")};I.userAgent.Vn=I.userAgent.Pc||I.userAgent.ik();I.userAgent.uo=I.userAgent.Bb;
I.userAgent.ij=function(){var b=I.userAgent.zd();return b&&b.platform||""};I.userAgent.ko=I.userAgent.ij();I.userAgent.le=!1;I.userAgent.pe=!1;I.userAgent.ke=!1;I.userAgent.qe=!1;I.userAgent.ge=!1;I.userAgent.Nc=!1;I.userAgent.Mc=!1;I.userAgent.Oc=!1;I.userAgent.wh=!1;I.userAgent.za=I.userAgent.le||I.userAgent.pe||I.userAgent.ke||I.userAgent.qe||I.userAgent.ge||I.userAgent.Nc||I.userAgent.Mc||I.userAgent.Oc;I.userAgent.Mn=I.userAgent.za?I.userAgent.le:I.g.userAgent.platform.yg();
I.userAgent.hp=I.userAgent.za?I.userAgent.pe:I.g.userAgent.platform.Eg();I.userAgent.fk=function(){return I.g.userAgent.platform.gk()||I.g.userAgent.platform.pg()};I.userAgent.Kn=I.userAgent.za?I.userAgent.ke:I.userAgent.fk();I.userAgent.rk=function(){var b=I.userAgent.zd();return!!b&&I.f.contains(b.appVersion||"","X11")};I.userAgent.ip=I.userAgent.za?I.userAgent.qe:I.userAgent.rk();I.userAgent.Ul=I.userAgent.za?I.userAgent.ge:I.g.userAgent.platform.ng();
I.userAgent.yn=I.userAgent.za?I.userAgent.Nc:I.g.userAgent.platform.wg();I.userAgent.xn=I.userAgent.za?I.userAgent.Mc:I.g.userAgent.platform.vg();I.userAgent.zn=I.userAgent.za?I.userAgent.Oc:I.g.userAgent.platform.xg();I.userAgent.wn=I.userAgent.za?I.userAgent.Nc||I.userAgent.Mc||I.userAgent.Oc:I.g.userAgent.platform.ug();I.userAgent.Cn=I.userAgent.za?I.userAgent.wh:I.g.userAgent.platform.ek();
I.userAgent.jj=function(){var b="",c=I.userAgent.Oj();c&&(b=c?c[1]:"");return I.userAgent.$&&(c=I.userAgent.Ff(),null!=c&&c>parseFloat(b))?String(c):b};I.userAgent.Oj=function(){var b=I.userAgent.Mj();if(I.userAgent.Tc)return/rv:([^\);]+)(\)|;)/.exec(b);if(I.userAgent.ve)return/Edge\/([\d\.]+)/.exec(b);if(I.userAgent.$)return/\b(?:MSIE|rv)[: ]([^\);]+)(\)|;)/.exec(b);if(I.userAgent.Bb)return/WebKit\/(\S+)/.exec(b);if(I.userAgent.Ie)return/(?:Version)[ \/]?(\S+)/.exec(b)};
I.userAgent.Ff=function(){var b=I.global.document;return b?b.documentMode:void 0};I.userAgent.VERSION=I.userAgent.jj();I.userAgent.compare=function(b,c){return I.f.Db(b,c)};I.userAgent.pk={};I.userAgent.xa=function(b){return I.userAgent.uh||I.Ia.cache(I.userAgent.pk,b,function(){return 0<=I.f.Db(I.userAgent.VERSION,b)})};I.userAgent.os=I.userAgent.xa;I.userAgent.Pb=function(b){return Number(I.userAgent.Oh)>=b};I.userAgent.Qr=I.userAgent.Pb;var J;var K=I.global.document,ba=I.userAgent.Ff();
J=K&&I.userAgent.$?ba||("CSS1Compat"==K.compatMode?parseInt(I.userAgent.VERSION,10):5):void 0;I.userAgent.Oh=J;I.a.gb={Ch:!I.userAgent.$||I.userAgent.Pb(9),Dh:!I.userAgent.Tc&&!I.userAgent.$||I.userAgent.$&&I.userAgent.Pb(9)||I.userAgent.Tc&&I.userAgent.xa("1.9.1"),se:I.userAgent.$&&!I.userAgent.xa("9"),Eh:I.userAgent.$||I.userAgent.Ie||I.userAgent.Bb,Uh:I.userAgent.$,Gn:I.userAgent.$&&!I.userAgent.Pb(9)};I.a.tags={};I.a.tags.yi={area:!0,base:!0,br:!0,col:!0,command:!0,embed:!0,hr:!0,img:!0,input:!0,keygen:!0,link:!0,meta:!0,param:!0,source:!0,track:!0,wbr:!0};I.a.tags.qk=function(b){return!0===I.a.tags.yi[b]};I.f.ap=C();I.f.H=function(b,c){this.be=b===I.f.H.Ae&&c||"";this.ni=I.f.H.Pe};I.f.H.prototype.wa=!0;I.f.H.prototype.ja=D("be");I.f.H.prototype.toString=function(){return"Const{"+this.be+"}"};I.f.H.s=function(b){if(b instanceof I.f.H&&b.constructor===I.f.H&&b.ni===I.f.H.Pe)return b.be;I.o.ha("expected object of type Const, got '"+b+"'");return"type_error:Const"};I.f.H.from=function(b){return new I.f.H(I.f.H.Ae,b)};I.f.H.Pe={};I.f.H.Ae={};I.f.H.EMPTY=I.f.H.from("");I.b={};I.b.O=function(){this.wc="";this.di=I.b.O.da};I.b.O.prototype.wa=!0;I.b.O.da={};I.b.O.jc=function(b){b=I.f.H.s(b);return 0===b.length?I.b.O.EMPTY:I.b.O.Eb(b)};I.b.O.Sq=function(b,c){for(var d=[],e=1;e<arguments.length;e++)d.push(I.b.O.dh(arguments[e]));return I.b.O.Eb("("+I.f.H.s(b)+")("+d.join(", ")+");")};I.b.O.Wq=function(b){return I.b.O.Eb(I.b.O.dh(b))};I.b.O.prototype.ja=D("wc");I.Z&&(I.b.O.prototype.toString=function(){return"SafeScript{"+this.wc+"}"});
I.b.O.s=function(b){if(b instanceof I.b.O&&b.constructor===I.b.O&&b.di===I.b.O.da)return b.wc;I.o.ha("expected object of type SafeScript, got '"+b+a+I.ca(b));return"type_error:SafeScript"};I.b.O.dh=function(b){return JSON.stringify(b).replace(/</g,"\\x3c")};I.b.O.Eb=function(b){return(new I.b.O).ab(b)};I.b.O.prototype.ab=function(b){this.wc=b;return this};I.b.O.EMPTY=I.b.O.Eb("");I.ua={};I.ua.url={};I.ua.url.$i=function(b){return I.ua.url.cg().createObjectURL(b)};I.ua.url.dt=function(b){I.ua.url.cg().revokeObjectURL(b)};I.ua.url.cg=function(){var b=I.ua.url.wf();if(null!=b)return b;throw Error("This browser doesn't seem to support blob URLs");};I.ua.url.wf=function(){return I.W(I.global.URL)&&I.W(I.global.URL.createObjectURL)?I.global.URL:I.W(I.global.webkitURL)&&I.W(I.global.webkitURL.createObjectURL)?I.global.webkitURL:I.W(I.global.createObjectURL)?I.global:null};
I.ua.url.Qp=function(){return null!=I.ua.url.wf()};I.h={};I.h.i={};I.h.i.Rh=!1;
I.h.i.De=I.h.i.Rh||("ar"==I.I.substring(0,2).toLowerCase()||"fa"==I.I.substring(0,2).toLowerCase()||"he"==I.I.substring(0,2).toLowerCase()||"iw"==I.I.substring(0,2).toLowerCase()||"ps"==I.I.substring(0,2).toLowerCase()||"sd"==I.I.substring(0,2).toLowerCase()||"ug"==I.I.substring(0,2).toLowerCase()||"ur"==I.I.substring(0,2).toLowerCase()||"yi"==I.I.substring(0,2).toLowerCase())&&(2==I.I.length||"-"==I.I.substring(2,3)||"_"==I.I.substring(2,3))||3<=I.I.length&&"ckb"==I.I.substring(0,3).toLowerCase()&&
(3==I.I.length||"-"==I.I.substring(3,4)||"_"==I.I.substring(3,4))||7<=I.I.length&&("-"==I.I.substring(2,3)||"_"==I.I.substring(2,3))&&("adlm"==I.I.substring(3,7).toLowerCase()||"arab"==I.I.substring(3,7).toLowerCase()||"hebr"==I.I.substring(3,7).toLowerCase()||"nkoo"==I.I.substring(3,7).toLowerCase()||"rohg"==I.I.substring(3,7).toLowerCase()||"thaa"==I.I.substring(3,7).toLowerCase())||8<=I.I.length&&("-"==I.I.substring(3,4)||"_"==I.I.substring(3,4))&&("adlm"==I.I.substring(4,8).toLowerCase()||"arab"==
I.I.substring(4,8).toLowerCase()||"hebr"==I.I.substring(4,8).toLowerCase()||"nkoo"==I.I.substring(4,8).toLowerCase()||"rohg"==I.I.substring(4,8).toLowerCase()||"thaa"==I.I.substring(4,8).toLowerCase());I.h.i.kb={Wh:"\u202a",$h:"\u202b",Je:"\u202c",Xh:"\u200e",ai:"\u200f"};I.h.i.S={Ta:1,Ua:-1,sa:0};I.h.i.$b="right";I.h.i.Yb="left";I.h.i.rn=I.h.i.De?I.h.i.Yb:I.h.i.$b;I.h.i.qn=I.h.i.De?I.h.i.$b:I.h.i.Yb;
I.h.i.ul=function(b){return typeof b==u?0<b?I.h.i.S.Ta:0>b?I.h.i.S.Ua:I.h.i.S.sa:null==b?null:b?I.h.i.S.Ua:I.h.i.S.Ta};I.h.i.sb="A-Za-z\u00c0-\u00d6\u00d8-\u00f6\u00f8-\u02b8\u0300-\u0590\u0900-\u1fff\u200e\u2c00-\ud801\ud804-\ud839\ud83c-\udbff\uf900-\ufb1c\ufe00-\ufe6f\ufefd-\uffff";I.h.i.xb="\u0591-\u06ef\u06fa-\u08ff\u200f\ud802-\ud803\ud83a-\ud83b\ufb1d-\ufdff\ufe70-\ufefc";I.h.i.Uj=/<[^>]*>|&[^;]+;/g;I.h.i.Sa=function(b,c){return c?b.replace(I.h.i.Uj,""):b};
I.h.i.Uk=new RegExp("["+I.h.i.xb+"]");I.h.i.Ak=new RegExp("["+I.h.i.sb+"]");I.h.i.Bd=function(b,c){return I.h.i.Uk.test(I.h.i.Sa(b,c))};I.h.i.zr=I.h.i.Bd;I.h.i.fg=function(b){return I.h.i.Ak.test(I.h.i.Sa(b,void 0))};I.h.i.Dk=new RegExp("^["+I.h.i.sb+"]");I.h.i.Zk=new RegExp("^["+I.h.i.xb+"]");I.h.i.kk=function(b){return I.h.i.Zk.test(b)};I.h.i.hk=function(b){return I.h.i.Dk.test(b)};I.h.i.bs=function(b){return!I.h.i.hk(b)&&!I.h.i.kk(b)};I.h.i.Bk=new RegExp("^[^"+I.h.i.xb+"]*["+I.h.i.sb+"]");
I.h.i.Wk=new RegExp("^[^"+I.h.i.sb+"]*["+I.h.i.xb+"]");I.h.i.ah=function(b,c){return I.h.i.Wk.test(I.h.i.Sa(b,c))};I.h.i.hs=I.h.i.ah;I.h.i.nl=function(b,c){return I.h.i.Bk.test(I.h.i.Sa(b,c))};I.h.i.Zr=I.h.i.nl;I.h.i.zg=/^http:\/\/.*/;I.h.i.cs=function(b,c){b=I.h.i.Sa(b,c);return I.h.i.zg.test(b)||!I.h.i.fg(b)&&!I.h.i.Bd(b)};I.h.i.Ck=new RegExp("["+I.h.i.sb+"][^"+I.h.i.xb+"]*$");I.h.i.Xk=new RegExp("["+I.h.i.xb+"][^"+I.h.i.sb+"]*$");I.h.i.mj=function(b,c){return I.h.i.Ck.test(I.h.i.Sa(b,c))};
I.h.i.Yr=I.h.i.mj;I.h.i.nj=function(b,c){return I.h.i.Xk.test(I.h.i.Sa(b,c))};I.h.i.fs=I.h.i.nj;I.h.i.Yk=/^(ar|ckb|dv|he|iw|fa|nqo|ps|sd|ug|ur|yi|.*[-_](Adlm|Arab|Hebr|Nkoo|Rohg|Thaa))(?!.*[-_](Latn|Cyrl)($|-|_))($|-|_)/i;I.h.i.gs=function(b){return I.h.i.Yk.test(b)};I.h.i.Pi=/(\(.*?\)+)|(\[.*?\]+)|(\{.*?\}+)|(<.*?>+)/g;I.h.i.yr=function(b,c){c=(void 0===c?I.h.i.Bd(b):c)?I.h.i.kb.ai:I.h.i.kb.Xh;return b.replace(I.h.i.Pi,c+"$&"+c)};
I.h.i.Fq=function(b){return"<"==b.charAt(0)?b.replace(/<\w+/,"$& dir=rtl"):"\n<span dir=rtl>"+b+"</span>"};I.h.i.Gq=function(b){return I.h.i.kb.$h+b+I.h.i.kb.Je};I.h.i.Dq=function(b){return"<"==b.charAt(0)?b.replace(/<\w+/,"$& dir=ltr"):"\n<span dir=ltr>"+b+"</span>"};I.h.i.Eq=function(b){return I.h.i.kb.Wh+b+I.h.i.kb.Je};I.h.i.kj=/:\s*([.\d][.\w]*)\s+([.\d][.\w]*)\s+([.\d][.\w]*)\s+([.\d][.\w]*)/g;I.h.i.sk=/left/gi;I.h.i.Tk=/right/gi;I.h.i.sl=/%%%%/g;
I.h.i.ys=function(b){return b.replace(I.h.i.kj,":$1 $4 $3 $2").replace(I.h.i.sk,"%%%%").replace(I.h.i.Tk,I.h.i.Yb).replace(I.h.i.sl,I.h.i.$b)};I.h.i.lj=/([\u0591-\u05f2])"/g;I.h.i.ll=/([\u0591-\u05f2])'/g;I.h.i.Ds=function(b){return b.replace(I.h.i.lj,"$1\u05f4").replace(I.h.i.ll,"$1\u05f3")};I.h.i.Kl=/\s+/;I.h.i.Tj=/[\d\u06f0-\u06f9]/;I.h.i.Vk=.4;
I.h.i.qf=function(b,c){var d=0,e=0,f=!1;b=I.h.i.Sa(b,c).split(I.h.i.Kl);for(c=0;c<b.length;c++){var g=b[c];I.h.i.ah(g)?(d++,e++):I.h.i.zg.test(g)?f=!0:I.h.i.fg(g)?e++:I.h.i.Tj.test(g)&&(f=!0)}return 0==e?f?I.h.i.S.Ta:I.h.i.S.sa:d/e>I.h.i.Vk?I.h.i.S.Ua:I.h.i.S.Ta};I.h.i.yq=function(b,c){return I.h.i.qf(b,c)==I.h.i.S.Ua};I.h.i.st=function(b,c){b&&(c=I.h.i.ul(c))&&(b.style.textAlign=c==I.h.i.S.Ua?I.h.i.$b:I.h.i.Yb,b.dir=c==I.h.i.S.Ua?"rtl":"ltr")};
I.h.i.tt=function(b,c){switch(I.h.i.qf(c)){case I.h.i.S.Ta:b.dir="ltr";break;case I.h.i.S.Ua:b.dir="rtl";break;default:b.removeAttribute("dir")}};I.h.i.Lm=C();I.b.C=function(){this.zc="";this.pi=I.b.C.da};I.b.C.prototype.wa=!0;I.b.C.prototype.ja=D("zc");I.b.C.prototype.Dd=!0;I.b.C.prototype.Za=function(){return I.h.i.S.Ta};I.Z&&(I.b.C.prototype.toString=function(){return"TrustedResourceUrl{"+this.zc+"}"});I.b.C.s=function(b){if(b instanceof I.b.C&&b.constructor===I.b.C&&b.pi===I.b.C.da)return b.zc;I.o.ha("expected object of type TrustedResourceUrl, got '"+b+a+I.ca(b));return"type_error:TrustedResourceUrl"};
I.b.C.format=function(b,c){var d=I.f.H.s(b);if(!I.b.C.zh.test(d))throw Error("Invalid TrustedResourceUrl format: "+d);b=d.replace(I.b.C.Sh,function(b,f){if(!Object.prototype.hasOwnProperty.call(c,f))throw Error('Found marker, "'+f+'", in format string, "'+d+'", but no valid label mapping found in args: '+JSON.stringify(c));b=c[f];return b instanceof I.f.H?I.f.H.s(b):encodeURIComponent(String(b))});return I.b.C.Hb(b)};I.b.C.Sh=/%{(\w+)}/g;I.b.C.zh=/^((https:)?\/\/[0-9a-z.:[\]-]+\/|\/[^/\\]|[^:/\\]+\/|[^:/\\]*[?#]|about:blank#)/i;
I.b.C.ti=/^([^?#]*)(\?[^#]*)?(#[\s\S]*)?/;I.b.C.Pq=function(b,c,d,e){b=I.b.C.format(b,c);b=I.b.C.s(b);b=I.b.C.ti.exec(b);c=b[3]||"";return I.b.C.Hb(b[1]+I.b.C.bh("?",b[2]||"",d)+I.b.C.bh("#",c,e))};I.b.C.jc=function(b){return I.b.C.Hb(I.f.H.s(b))};I.b.C.Tq=function(b){for(var c="",d=0;d<b.length;d++)c+=I.f.H.s(b[d]);return I.b.C.Hb(c)};I.b.C.da={};I.b.C.Hb=function(b){var c=new I.b.C;c.zc=b;return c};
I.b.C.bh=function(b,c,d){if(null==d)return c;if(I.N(d))return d?b+encodeURIComponent(d):"";for(var e in d){var f=d[e];f=I.isArray(f)?f:[f];for(var g=0;g<f.length;g++){var h=f[g];null!=h&&(c||(c=b),c+=(c.length>b.length?"&":"")+encodeURIComponent(e)+"="+encodeURIComponent(String(h)))}}return c};I.b.l=function(){this.Ha="";this.gi=I.b.l.da};I.b.l.ga="about:invalid#zClosurez";I.b.l.prototype.wa=!0;I.b.l.prototype.ja=D("Ha");I.b.l.prototype.Dd=!0;I.b.l.prototype.Za=function(){return I.h.i.S.Ta};I.Z&&(I.b.l.prototype.toString=function(){return"SafeUrl{"+this.Ha+"}"});I.b.l.s=function(b){if(b instanceof I.b.l&&b.constructor===I.b.l&&b.gi===I.b.l.da)return b.Ha;I.o.ha("expected object of type SafeUrl, got '"+b+a+I.ca(b));return"type_error:SafeUrl"};I.b.l.jc=function(b){return I.b.l.oa(I.f.H.s(b))};
I.b.Me=/^(?:audio\/(?:3gpp2|3gpp|aac|L16|midi|mp3|mp4|mpeg|oga|ogg|opus|x-m4a|x-wav|wav|webm)|image\/(?:bmp|gif|jpeg|jpg|png|tiff|webp)|text\/csv|video\/(?:mpeg|mp4|ogg|webm|quicktime))$/i;I.b.l.Rq=function(b){b=I.b.Me.test(b.type)?I.ua.url.$i(b):I.b.l.ga;return I.b.l.oa(b)};I.b.Jh=/^data:([^;,]*);base64,[a-z0-9+\/]+=*$/i;I.b.l.Uq=function(b){b=b.replace(/(%0A|%0D)/g,"");var c=b.match(I.b.Jh);c=c&&I.b.Me.test(c[1]);return I.b.l.oa(c?b:I.b.l.ga)};
I.b.l.$q=function(b){I.f.bd(b,"tel:")||(b=I.b.l.ga);return I.b.l.oa(b)};I.b.ki=/^sip[s]?:[+a-z0-9_.!$%&'*\/=^`{|}~-]+@([a-z0-9-]+\.)+[a-z0-9]{2,63}$/i;I.b.l.Yq=function(b){I.b.ki.test(decodeURIComponent(b))||(b=I.b.l.ga);return I.b.l.oa(b)};I.b.l.Vq=function(b){I.f.bd(b,"fb-messenger://share")||(b=I.b.l.ga);return I.b.l.oa(b)};I.b.l.Zq=function(b){I.f.bd(b,"sms:")&&I.b.l.lk(b)||(b=I.b.l.ga);return I.b.l.oa(b)};
I.b.l.lk=function(b){var c=b.indexOf("#");0<c&&(b=b.substring(0,c));c=b.match(/[?&]body=/gi);if(!c)return!0;if(1<c.length)return!1;b=b.match(/[?&]body=([^&]*)/)[1];if(!b)return!0;try{decodeURIComponent(b)}catch(d){return!1}return/^(?:[a-z0-9\-_.~]|%[0-9a-f]{2})+$/i.test(b)};I.b.l.lt=function(b,c){return I.b.l.Ud(/^chrome-extension:\/\/([^\/]+)\//,b,c)};I.b.l.nt=function(b,c){return I.b.l.Ud(/^moz-extension:\/\/([^\/]+)\//,b,c)};
I.b.l.mt=function(b,c){return I.b.l.Ud(/^ms-browser-extension:\/\/([^\/]+)\//,b,c)};I.b.l.Ud=function(b,c,d){(b=b.exec(c))?(b=b[1],-1==(d instanceof I.f.H?[I.f.H.s(d)]:d.map(function(b){return I.f.H.s(b)})).indexOf(b)&&(c=I.b.l.ga)):c=I.b.l.ga;return I.b.l.oa(c)};I.b.l.ar=function(b){return I.b.l.oa(I.b.C.s(b))};I.b.Wc=/^(?:(?:https?|mailto|ftp):|[^:/?#]*(?:[/?#]|$))/i;I.b.l.vo=I.b.Wc;
I.b.l.Dc=function(b){if(b instanceof I.b.l)return b;b=typeof b==v&&b.wa?b.ja():String(b);I.b.Wc.test(b)||(b=I.b.l.ga);return I.b.l.oa(b)};I.b.l.ra=function(b){if(b instanceof I.b.l)return b;b=typeof b==v&&b.wa?b.ja():String(b);I.b.Wc.test(b)||(b=I.b.l.ga);return I.b.l.oa(b)};I.b.l.da={};I.b.l.oa=function(b){var c=new I.b.l;c.Ha=b;return c};I.b.l.Pl=I.b.l.oa("about:blank");I.b.v=function(){this.yc="";this.fi=I.b.v.da};I.b.v.prototype.wa=!0;I.b.v.da={};I.b.v.jc=function(b){b=I.f.H.s(b);return 0===b.length?I.b.v.EMPTY:I.b.v.Fb(b)};I.b.v.Zp=C();I.b.v.prototype.ja=D("yc");I.Z&&(I.b.v.prototype.toString=function(){return"SafeStyle{"+this.yc+"}"});I.b.v.s=function(b){if(b instanceof I.b.v&&b.constructor===I.b.v&&b.fi===I.b.v.da)return b.yc;I.o.ha("expected object of type SafeStyle, got '"+b+a+I.ca(b));return"type_error:SafeStyle"};I.b.v.Fb=function(b){return(new I.b.v).ab(b)};
I.b.v.prototype.ab=function(b){this.yc=b;return this};I.b.v.EMPTY=I.b.v.Fb("");I.b.v.ga="zClosurez";I.b.v.create=function(b){var c="",d;for(d in b){if(!/^[-_a-zA-Z0-9]+$/.test(d))throw Error("Name allows only [-_a-zA-Z0-9], got: "+d);var e=b[d];null!=e&&(e=I.isArray(e)?I.j.map(e,I.b.v.Ug).join(" "):I.b.v.Ug(e),c+=d+":"+e+";")}return c?I.b.v.Fb(c):I.b.v.EMPTY};
I.b.v.Ug=function(b){return b instanceof I.b.l?'url("'+I.b.l.s(b).replace(/</g,"%3c").replace(/[\\"]/g,"\\$&")+'")':b instanceof I.f.H?I.f.H.s(b):I.b.v.cl(String(b))};
I.b.v.cl=function(b){var c=b.replace(I.b.v.ze,"$1").replace(I.b.v.ze,"$1").replace(I.b.v.Qe,"url");if(I.b.v.vi.test(c)){if(I.b.v.Hh.test(b))return I.o.ha("String value disallows comments, got: "+b),I.b.v.ga;if(!I.b.v.Qj(b))return I.o.ha("String value requires balanced quotes, got: "+b),I.b.v.ga;if(!I.b.v.Rj(b))return I.o.ha("String value requires balanced square brackets and one identifier per pair of brackets, got: "+b),I.b.v.ga}else return I.o.ha("String value allows only "+I.b.v.Te+" and simple functions, got: "+
b),I.b.v.ga;return I.b.v.dl(b)};I.b.v.Qj=function(b){for(var c=!0,d=!0,e=0;e<b.length;e++){var f=b.charAt(e);"'"==f&&d?c=!c:'"'==f&&c&&(d=!d)}return c&&d};I.b.v.Rj=function(b){for(var c=!0,d=/^[-_a-zA-Z0-9]$/,e=0;e<b.length;e++){var f=b.charAt(e);if("]"==f){if(c)return!1;c=!0}else if("["==f){if(!c)return!1;c=!1}else if(!c&&!d.test(f))return!1}return c};I.b.v.Te="[-,.\"'%_!# a-zA-Z0-9\\[\\]]";I.b.v.vi=new RegExp("^"+I.b.v.Te+"+$");I.b.v.Qe=/\b(url\([ \t\n]*)('[ -&(-\[\]-~]*'|"[ !#-\[\]-~]*"|[!#-&*-\[\]-~]*)([ \t\n]*\))/g;
I.b.v.ze=/\b(hsl|hsla|rgb|rgba|matrix|calc|minmax|fit-content|repeat|(rotate|scale|translate)(X|Y|Z|3d)?)\([-+*/0-9a-z.%\[\], ]+\)/g;I.b.v.Hh=/\/\*/;I.b.v.dl=function(b){return b.replace(I.b.v.Qe,function(b,d,e,f){var c="";e=e.replace(/^(['"])(.*)\1$/,function(b,d,e){c=d;return e});b=I.b.l.Dc(e).ja();return d+c+b+c+f})};I.b.v.concat=function(b){function c(b){I.isArray(b)?I.j.forEach(b,c):d+=I.b.v.s(b)}var d="";I.j.forEach(arguments,c);return d?I.b.v.Fb(d):I.b.v.EMPTY};I.b.R=function(){this.xc="";this.ei=I.b.R.da};I.b.R.prototype.wa=!0;I.b.R.da={};
I.b.R.oq=function(b,c){if(I.f.contains(b,"<"))throw Error("Selector does not allow '<', got: "+b);var d=b.replace(/('|")((?!\1)[^\r\n\f\\]|\\[\s\S])*\1/g,"");if(!/^[-_a-zA-Z0-9#.:* ,>+~[\]()=^$|]+$/.test(d))throw Error("Selector allows only [-_a-zA-Z0-9#.:* ,>+~[\\]()=^$|] and strings, got: "+b);if(!I.b.R.Pj(d))throw Error("() and [] in selector must be balanced, got: "+b);c instanceof I.b.v||(c=I.b.v.create(c));b=b+"{"+I.b.v.s(c)+"}";return I.b.R.Gb(b)};
I.b.R.Pj=function(b){for(var c={"(":")","[":"]"},d=[],e=0;e<b.length;e++){var f=b[e];if(c[f])d.push(c[f]);else if(I.object.contains(c,f)&&d.pop()!=f)return!1}return 0==d.length};I.b.R.concat=function(b){function c(b){I.isArray(b)?I.j.forEach(b,c):d+=I.b.R.s(b)}var d="";I.j.forEach(arguments,c);return I.b.R.Gb(d)};I.b.R.jc=function(b){b=I.f.H.s(b);return 0===b.length?I.b.R.EMPTY:I.b.R.Gb(b)};I.b.R.prototype.ja=D("xc");I.Z&&(I.b.R.prototype.toString=function(){return"SafeStyleSheet{"+this.xc+"}"});
I.b.R.s=function(b){if(b instanceof I.b.R&&b.constructor===I.b.R&&b.ei===I.b.R.da)return b.xc;I.o.ha("expected object of type SafeStyleSheet, got '"+b+a+I.ca(b));return"type_error:SafeStyleSheet"};I.b.R.Gb=function(b){return(new I.b.R).ab(b)};I.b.R.prototype.ab=function(b){this.xc=b;return this};I.b.R.EMPTY=I.b.R.Gb("");I.b.m=function(){this.Ha="";this.ci=I.b.m.da;this.hc=null};I.b.m.prototype.Dd=!0;I.b.m.prototype.Za=D("hc");I.b.m.prototype.wa=!0;I.b.m.prototype.ja=D("Ha");I.Z&&(I.b.m.prototype.toString=function(){return"SafeHtml{"+this.Ha+"}"});I.b.m.s=function(b){if(b instanceof I.b.m&&b.constructor===I.b.m&&b.ci===I.b.m.da)return b.Ha;I.o.ha("expected object of type SafeHtml, got '"+b+a+I.ca(b));return"type_error:SafeHtml"};
I.b.m.va=function(b){if(b instanceof I.b.m)return b;var c=typeof b==v,d=null;c&&b.Dd&&(d=b.Za());return I.b.m.ta(I.f.va(c&&b.wa?b.ja():String(b)),d)};I.b.m.Cr=function(b){if(b instanceof I.b.m)return b;b=I.b.m.va(b);return I.b.m.ta(I.f.Ng(I.b.m.s(b)),b.Za())};I.b.m.Dr=function(b){if(b instanceof I.b.m)return b;b=I.b.m.va(b);return I.b.m.ta(I.f.Jl(I.b.m.s(b)),b.Za())};I.b.m.from=I.b.m.va;I.b.m.Se=/^[a-zA-Z0-9-]+$/;I.b.m.si={action:!0,cite:!0,data:!0,formaction:!0,href:!0,manifest:!0,poster:!0,src:!0};
I.b.m.Zh={APPLET:!0,BASE:!0,EMBED:!0,IFRAME:!0,LINK:!0,MATH:!0,META:!0,OBJECT:!0,SCRIPT:!0,STYLE:!0,SVG:!0,TEMPLATE:!0};I.b.m.create=function(b,c,d){I.b.m.Hl(String(b));return I.b.m.Ya(String(b),c,d)};I.b.m.Hl=function(b){if(!I.b.m.Se.test(b))throw Error("Invalid tag name <"+b+">.");if(b.toUpperCase()in I.b.m.Zh)throw Error("Tag name <"+b+"> is not allowed for SafeHtml.");};
I.b.m.lq=function(b,c,d,e){b&&I.b.C.s(b);var f={};f.src=b||null;f.srcdoc=c&&I.b.m.s(c);b=I.b.m.fc(f,{sandbox:""},d);return I.b.m.Ya("iframe",b,e)};I.b.m.pq=function(b,c,d,e){if(!I.b.m.Si())throw Error("The browser does not support sandboxed iframes.");var f={};f.src=b?I.b.l.s(I.b.l.Dc(b)):null;f.srcdoc=c||null;f.sandbox="";b=I.b.m.fc(f,{},d);return I.b.m.Ya("iframe",b,e)};I.b.m.Si=function(){return I.global.HTMLIFrameElement&&"sandbox"in I.global.HTMLIFrameElement.prototype};
I.b.m.rq=function(b,c){I.b.C.s(b);b=I.b.m.fc({src:b},{},c);return I.b.m.Ya("script",b)};I.b.m.qq=function(b,c){for(var d in c){var e=d.toLowerCase();if("language"==e||"src"==e||"text"==e||"type"==e)throw Error('Cannot set "'+e+'" attribute');}d="";b=I.j.concat(b);for(e=0;e<b.length;e++)d+=I.b.O.s(b[e]);b=I.b.m.ta(d,I.h.i.S.sa);return I.b.m.Ya("script",c,b)};
I.b.m.sq=function(b,c){c=I.b.m.fc({type:"text/css"},{},c);var d="";b=I.j.concat(b);for(var e=0;e<b.length;e++)d+=I.b.R.s(b[e]);b=I.b.m.ta(d,I.h.i.S.sa);return I.b.m.Ya("style",c,b)};I.b.m.nq=function(b,c){b=I.b.l.s(I.b.l.Dc(b));(I.g.userAgent.w.rc()||I.g.userAgent.w.Ra())&&I.f.contains(b,";")&&(b="'"+b.replace(/'/g,"%27")+"'");return I.b.m.Ya("meta",{"http-equiv":"refresh",content:(c||0)+"; url="+b})};
I.b.m.xj=function(b,c,d){if(d instanceof I.f.H)d=I.f.H.s(d);else if("style"==c.toLowerCase())d=I.b.m.Kj(d);else{if(/^on/i.test(c))throw Error('Attribute "'+c+'" requires goog.string.Const value, "'+d+'" given.');if(c.toLowerCase()in I.b.m.si)if(d instanceof I.b.C)d=I.b.C.s(d);else if(d instanceof I.b.l)d=I.b.l.s(d);else if(I.N(d))d=I.b.l.Dc(d).ja();else throw Error('Attribute "'+c+'" on tag "'+b+'" requires goog.html.SafeUrl, goog.string.Const, or string, value "'+d+'" given.');}d.wa&&(d=d.ja());
return c+'="'+I.f.va(String(d))+'"'};I.b.m.Kj=function(b){if(!I.ka(b))throw Error('The "style" attribute requires goog.html.SafeStyle or map of style properties, '+typeof b+" given: "+b);b instanceof I.b.v||(b=I.b.v.create(b));return I.b.v.s(b)};I.b.m.uq=function(b,c,d,e){c=I.b.m.create(c,d,e);c.hc=b;return c};
I.b.m.concat=function(b){function c(b){I.isArray(b)?I.j.forEach(b,c):(b=I.b.m.va(b),e+=I.b.m.s(b),b=b.Za(),d==I.h.i.S.sa?d=b:b!=I.h.i.S.sa&&d!=b&&(d=null))}var d=I.h.i.S.sa,e="";I.j.forEach(arguments,c);return I.b.m.ta(e,d)};I.b.m.hq=function(b,c){var d=I.b.m.concat(I.j.slice(arguments,1));d.hc=b;return d};I.b.m.da={};I.b.m.ta=function(b,c){return(new I.b.m).ab(b,c)};I.b.m.prototype.ab=function(b,c){this.Ha=b;this.hc=c;return this};
I.b.m.Ya=function(b,c,d){var e=null;var f="<"+b+I.b.m.pl(b,c);I.bb(d)?I.isArray(d)||(d=[d]):d=[];I.a.tags.qk(b.toLowerCase())?f+=">":(e=I.b.m.concat(d),f+=">"+I.b.m.s(e)+"</"+b+">",e=e.Za());(b=c&&c.dir)&&(e=/^(ltr|rtl|auto)$/i.test(b)?I.h.i.S.sa:null);return I.b.m.ta(f,e)};I.b.m.pl=function(b,c){var d="";if(c)for(var e in c){if(!I.b.m.Se.test(e))throw Error('Invalid attribute name "'+e+'".');var f=c[e];I.bb(f)&&(d+=" "+I.b.m.xj(b,e,f))}return d};
I.b.m.fc=function(b,c,d){var e={},f;for(f in b)e[f]=b[f];for(f in c)e[f]=c[f];for(f in d){var g=f.toLowerCase();if(g in b)throw Error('Cannot override "'+g+'" attribute, got "'+f+'" with value "'+d[f]+'"');g in c&&delete e[g];e[f]=d[f]}return e};I.b.m.Im=I.b.m.ta("<!DOCTYPE html>",I.h.i.S.sa);I.b.m.EMPTY=I.b.m.ta("",I.h.i.S.sa);I.b.m.re=I.b.m.ta("<br>",I.h.i.S.sa);I.a.J={};I.a.J.Bn={Sl:"afterbegin",Tl:"afterend",hm:"beforebegin",im:"beforeend"};I.a.J.Fr=function(b,c,d){b.insertAdjacentHTML(c,I.b.m.s(d))};I.a.J.ji={MATH:!0,SCRIPT:!0,STYLE:!0,SVG:!0,TEMPLATE:!0};I.a.J.dk=I.M.Ri(function(){if(I.Z&&"undefined"===typeof document)return!1;var b=document.createElement("div");b.innerHTML="<div><div></div></div>";if(I.Z&&!b.firstChild)return!1;var c=b.firstChild.firstChild;b.innerHTML="";return!c.parentElement});
I.a.J.Cl=function(b,c){if(I.a.J.dk())for(;b.lastChild;)b.removeChild(b.lastChild);b.innerHTML=c};I.a.J.Zg=function(b,c){if(I.o.ma&&I.a.J.ji[b.tagName.toUpperCase()])throw Error("goog.dom.safe.setInnerHtml cannot be used to set content of "+b.tagName+".");I.a.J.Cl(b,I.b.m.s(c))};I.a.J.Gt=function(b,c){b.outerHTML=I.b.m.s(c)};I.a.J.wt=function(b,c){c=c instanceof I.b.l?c:I.b.l.ra(c);I.a.o.Fi(b).action=I.b.l.s(c)};I.a.J.qt=function(b,c){c=c instanceof I.b.l?c:I.b.l.ra(c);I.a.o.Ei(b).formAction=I.b.l.s(c)};
I.a.J.Ct=function(b,c){c=c instanceof I.b.l?c:I.b.l.ra(c);I.a.o.Gi(b).formAction=I.b.l.s(c)};I.a.J.It=function(b,c){b.style.cssText=I.b.v.s(c)};I.a.J.Bq=function(b,c){b.write(I.b.m.s(c))};I.a.J.ot=function(b,c){c=c instanceof I.b.l?c:I.b.l.ra(c);b.href=I.b.l.s(c)};I.a.J.Bt=function(b,c){c=c instanceof I.b.l?c:I.b.l.ra(c);b.src=I.b.l.s(c)};I.a.J.pt=function(b,c){c=c instanceof I.b.l?c:I.b.l.ra(c);b.src=I.b.l.s(c)};I.a.J.Mt=function(b,c){c=c instanceof I.b.l?c:I.b.l.ra(c);b.src=I.b.l.s(c)};
I.a.J.ut=function(b,c){b.src=I.b.C.s(c)};I.a.J.xt=function(b,c){b.src=I.b.C.s(c)};I.a.J.zt=function(b,c){b.src=I.b.C.s(c)};I.a.J.At=function(b,c){b.srcdoc=I.b.m.s(c)};I.a.J.Dt=function(b,c,d){b.rel=d;I.f.bf(d,"stylesheet")?b.href=I.b.C.s(c):b.href=c instanceof I.b.C?I.b.C.s(c):c instanceof I.b.l?I.b.l.s(c):I.b.l.ra(c).ja()};I.a.J.Ft=function(b,c){b.data=I.b.C.s(c)};I.a.J.il=function(b,c){b.src=I.b.C.s(c);(c=I.$f())&&b.setAttribute("nonce",c)};
I.a.J.Ht=function(b,c){b.text=I.b.O.s(c);(c=I.$f())&&b.setAttribute("nonce",c)};I.a.J.Et=function(b,c){c=c instanceof I.b.l?c:I.b.l.ra(c);b.href=I.b.l.s(c)};I.a.J.Mp=function(b,c){c=c instanceof I.b.l?c:I.b.l.ra(c);b.assign(I.b.l.s(c))};I.a.J.at=function(b,c){c=c instanceof I.b.l?c:I.b.l.ra(c);b.replace(I.b.l.s(c))};I.a.J.Ls=function(b,c,d,e,f){b=b instanceof I.b.l?b:I.b.l.ra(b);return(c||window).open(I.b.l.s(b),d?I.f.H.s(d):"",e,f)};I.b.fb={};I.b.fb.$k=function(b,c){return I.b.m.ta(c,null)};I.b.fb.ht=function(b,c){return I.b.O.Eb(c)};I.b.fb.it=function(b,c){return I.b.v.Fb(c)};I.b.fb.jt=function(b,c){return I.b.R.Gb(c)};I.b.fb.kt=function(b,c){return I.b.l.oa(c)};I.b.fb.hu=function(b,c){return I.b.C.Hb(c)};I.u={};I.u.Qs=function(b){return Math.floor(Math.random()*b)};I.u.ju=function(b,c){return b+Math.random()*(c-b)};I.u.$p=function(b,c,d){return Math.min(Math.max(b,c),d)};I.u.Lg=function(b,c){b%=c;return 0>b*c?b+c:b};I.u.rs=function(b,c,d){return b+d*(c-b)};I.u.Cs=function(b,c,d){return Math.abs(b-c)<=(d||1E-6)};I.u.ae=function(b){return I.u.Lg(b,360)};I.u.Tt=function(b){return I.u.Lg(b,2*Math.PI)};I.u.kh=function(b){return b*Math.PI/180};I.u.tl=function(b){return 180*b/Math.PI};
I.u.op=function(b,c){return c*Math.cos(I.u.kh(b))};I.u.pp=function(b,c){return c*Math.sin(I.u.kh(b))};I.u.angle=function(b,c,d,e){return I.u.ae(I.u.tl(Math.atan2(e-c,d-b)))};I.u.np=function(b,c){b=I.u.ae(c)-I.u.ae(b);180<b?b-=360:-180>=b&&(b=360+b);return b};I.u.sign=function(b){return 0<b?1:0>b?-1:b};
I.u.vs=function(b,c,d,e){d=d||function(b,c){return b==c};e=e||function(c){return b[c]};for(var f=b.length,g=c.length,h=[],l=0;l<f+1;l++)h[l]=[],h[l][0]=0;for(var m=0;m<g+1;m++)h[0][m]=0;for(l=1;l<=f;l++)for(m=1;m<=g;m++)d(b[l-1],c[m-1])?h[l][m]=h[l-1][m-1]+1:h[l][m]=Math.max(h[l-1][m],h[l][m-1]);var r=[];l=f;for(m=g;0<l&&0<m;)d(b[l-1],c[m-1])?(r.unshift(e(l-1,m-1)),l--,m--):h[l-1][m]>h[l][m-1]?l--:m--;return r};I.u.ce=function(b){return I.j.reduce(arguments,function(b,d){return b+d},0)};
I.u.Ji=function(b){return I.u.ce.apply(null,arguments)/arguments.length};I.u.bl=function(b){var c=arguments.length;if(2>c)return 0;var d=I.u.Ji.apply(null,arguments);return I.u.ce.apply(null,I.j.map(arguments,function(b){return Math.pow(b-d,2)}))/(c-1)};I.u.Ut=function(b){return Math.sqrt(I.u.bl.apply(null,arguments))};I.u.Vr=function(b){return isFinite(b)&&0==b%1};I.u.Tr=function(b){return isFinite(b)};I.u.$r=function(b){return 0==b&&0>1/b};
I.u.us=function(b){if(0<b){var c=Math.round(Math.log(b)*Math.LOG10E);return c-(parseFloat("1e"+c)>b?1:0)}return 0==b?-Infinity:NaN};I.u.ft=function(b,c){return Math.floor(b+(c||2E-15))};I.u.et=function(b,c){return Math.ceil(b-(c||2E-15))};I.u.X=function(b,c){this.x=I.W(b)?b:0;this.y=I.W(c)?c:0};I.u.X.prototype.clone=function(){return new I.u.X(this.x,this.y)};I.Z&&(I.u.X.prototype.toString=function(){return"("+this.x+", "+this.y+")"});I.u.X.prototype.Ib=function(b){return b instanceof I.u.X&&I.u.X.Ib(this,b)};I.u.X.Ib=function(b,c){return b==c?!0:b&&c?b.x==c.x&&b.y==c.y:!1};I.u.X.Aq=function(b,c){var d=b.x-c.x;b=b.y-c.y;return Math.sqrt(d*d+b*b)};I.u.X.ws=function(b){return Math.sqrt(b.x*b.x+b.y*b.y)};
I.u.X.azimuth=function(b){return I.u.angle(0,0,b.x,b.y)};I.u.X.Rt=function(b,c){var d=b.x-c.x;b=b.y-c.y;return d*d+b*b};I.u.X.zq=function(b,c){return new I.u.X(b.x-c.x,b.y-c.y)};I.u.X.ce=function(b,c){return new I.u.X(b.x+c.x,b.y+c.y)};F=I.u.X.prototype;F.ceil=function(){this.x=Math.ceil(this.x);this.y=Math.ceil(this.y);return this};F.floor=function(){this.x=Math.floor(this.x);this.y=Math.floor(this.y);return this};F.round=function(){this.x=Math.round(this.x);this.y=Math.round(this.y);return this};
F.translate=function(b,c){b instanceof I.u.X?(this.x+=b.x,this.y+=b.y):(this.x+=Number(b),I.Rb(c)&&(this.y+=c));return this};F.scale=function(b,c){c=I.Rb(c)?c:b;this.x*=b;this.y*=c;return this};I.u.lb=function(b,c){this.width=b;this.height=c};I.u.lb.Ib=function(b,c){return b==c?!0:b&&c?b.width==c.width&&b.height==c.height:!1};I.u.lb.prototype.clone=function(){return new I.u.lb(this.width,this.height)};I.Z&&(I.u.lb.prototype.toString=function(){return"("+this.width+" x "+this.height+")"});F=I.u.lb.prototype;F.Ai=function(){return this.width*this.height};F.aspectRatio=function(){return this.width/this.height};F.Qb=function(){return!this.Ai()};
F.ceil=function(){this.width=Math.ceil(this.width);this.height=Math.ceil(this.height);return this};F.floor=function(){this.width=Math.floor(this.width);this.height=Math.floor(this.height);return this};F.round=function(){this.width=Math.round(this.width);this.height=Math.round(this.height);return this};F.scale=function(b,c){c=I.Rb(c)?c:b;this.width*=b;this.height*=c;return this};I.a.yh=!1;I.a.ne=!1;I.a.Ih=I.a.yh||I.a.ne;I.a.td=function(b){return b?new I.a.jb(I.a.Qa(b)):I.a.hj||(I.a.hj=new I.a.jb)};I.a.yj=function(){return document};I.a.ud=function(b){return I.a.xd(document,b)};I.a.xd=function(b,c){return I.N(c)?b.getElementById(c):c};I.a.Gj=function(b){return I.a.Zf(document,b)};I.a.Zf=function(b,c){return I.a.xd(b,c)};I.a.qh=I.a.ud;I.a.getElementsByTagName=function(b,c){return(c||document).getElementsByTagName(String(b))};
I.a.yd=function(b,c,d){return I.a.kc(document,b,c,d)};I.a.Bj=function(b,c,d){return I.a.wd(document,b,c,d)};I.a.If=function(b,c){var d=c||document;return I.a.ad(d)?d.querySelectorAll("."+b):I.a.kc(document,"*",b,c)};I.a.vd=function(b,c){var d=c||document;return(d.getElementsByClassName?d.getElementsByClassName(b)[0]:I.a.wd(document,"*",b,c))||null};I.a.Yf=function(b,c){return I.a.vd(b,c)};I.a.ad=function(b){return!(!b.querySelectorAll||!b.querySelector)};
I.a.kc=function(b,c,d,e){b=e||b;c=c&&"*"!=c?String(c).toUpperCase():"";if(I.a.ad(b)&&(c||d))return b.querySelectorAll(c+(d?"."+d:""));if(d&&b.getElementsByClassName){b=b.getElementsByClassName(d);if(c){e={};for(var f=0,g=0,h;h=b[g];g++)c==h.nodeName&&(e[f++]=h);e.length=f;return e}return b}b=b.getElementsByTagName(c||"*");if(d){e={};for(g=f=0;h=b[g];g++)c=h.className,typeof c.split==p&&I.j.contains(c.split(/\s+/),d)&&(e[f++]=h);e.length=f;return e}return b};
I.a.wd=function(b,c,d,e){var f=e||b,g=c&&"*"!=c?String(c).toUpperCase():"";return I.a.ad(f)&&(g||d)?f.querySelector(g+(d?"."+d:"")):I.a.kc(b,c,d,e)[0]||null};I.a.rh=I.a.yd;I.a.Gc=function(b,c){I.object.forEach(c,function(c,e){c&&typeof c==v&&c.wa&&(c=c.ja());"style"==e?b.style.cssText=c:"class"==e?b.className=c:"for"==e?b.htmlFor=c:I.a.ue.hasOwnProperty(e)?b.setAttribute(I.a.ue[e],c):I.f.startsWith(e,"aria-")||I.f.startsWith(e,"data-")?b.setAttribute(e,c):b[e]=c})};
I.a.ue={cellpadding:"cellPadding",cellspacing:"cellSpacing",colspan:"colSpan",frameborder:"frameBorder",height:"height",maxlength:"maxLength",nonce:"nonce",role:"role",rowspan:"rowSpan",type:"type",usemap:"useMap",valign:"vAlign",width:"width"};I.a.dg=function(b){return I.a.eg(b||window)};I.a.eg=function(b){b=b.document;b=I.a.Ob(b)?b.documentElement:b.body;return new I.u.lb(b.clientWidth,b.clientHeight)};I.a.zj=function(){return I.a.rd(window)};I.a.ir=function(b){return I.a.rd(b)};
I.a.rd=function(b){var c=b.document,d=0;if(c){d=c.body;var e=c.documentElement;if(!e||!d)return 0;b=I.a.eg(b).height;if(I.a.Ob(c)&&e.scrollHeight)d=e.scrollHeight!=b?e.scrollHeight:e.offsetHeight;else{c=e.scrollHeight;var f=e.offsetHeight;e.clientHeight!=f&&(c=d.scrollHeight,f=d.offsetHeight);d=c>b?c>f?c:f:c<f?c:f}}return d};I.a.qr=function(b){return I.a.td((b||I.global||window).document).Gf()};I.a.Gf=function(){return I.a.Hf(document)};
I.a.Hf=function(b){var c=I.a.sd(b);b=I.a.nc(b);return I.userAgent.$&&I.userAgent.xa("10")&&b.pageYOffset!=c.scrollTop?new I.u.X(c.scrollLeft,c.scrollTop):new I.u.X(b.pageXOffset||c.scrollLeft,b.pageYOffset||c.scrollTop)};I.a.Aj=function(){return I.a.sd(document)};I.a.sd=function(b){return b.scrollingElement?b.scrollingElement:!I.userAgent.Bb&&I.a.Ob(b)?b.documentElement:b.body||b.documentElement};I.a.qb=function(b){return b?I.a.nc(b):window};I.a.nc=function(b){return b.parentWindow||b.defaultView};
I.a.ed=function(b,c,d){return I.a.jf(document,arguments)};I.a.jf=function(b,c){var d=String(c[0]),e=c[1];if(!I.a.gb.Ch&&e&&(e.name||e.type)){d=["<",d];e.name&&d.push(' name="',I.f.va(e.name),'"');if(e.type){d.push(' type="',I.f.va(e.type),'"');var f={};I.object.extend(f,e);delete f.type;e=f}d.push(">");d=d.join("")}d=b.createElement(d);e&&(I.N(e)?d.className=e:I.isArray(e)?d.className=e.join(" "):I.a.Gc(d,e));2<c.length&&I.a.Ve(b,d,c,2);return d};
I.a.Ve=function(b,c,d,e){function f(d){d&&c.appendChild(I.N(d)?b.createTextNode(d):d)}for(;e<d.length;e++){var g=d[e];I.Nb(g)&&!I.a.Id(g)?I.j.forEach(I.a.Jd(g)?I.j.jh(g):g,f):f(g)}};I.a.sh=I.a.ed;I.a.createElement=function(b){return I.a.Na(document,b)};I.a.Na=function(b,c){return b.createElement(String(c))};I.a.createTextNode=function(b){return document.createTextNode(String(b))};I.a.cj=function(b,c,d){return I.a.kf(document,b,c,!!d)};
I.a.kf=function(b,c,d,e){for(var f=I.a.Na(b,"TABLE"),g=f.appendChild(I.a.Na(b,"TBODY")),h=0;h<c;h++){for(var l=I.a.Na(b,"TR"),m=0;m<d;m++){var r=I.a.Na(b,"TD");e&&I.a.Wd(r,I.f.Re.Fe);l.appendChild(r)}g.appendChild(l)}return f};I.a.iq=function(b){var c=I.j.map(arguments,I.f.H.s);c=I.b.fb.$k(I.f.H.from("Constant HTML string, that gets turned into a Node later, so it will be automatically balanced."),c.join(""));return I.a.Sg(c)};I.a.Sg=function(b){return I.a.Tg(document,b)};
I.a.Tg=function(b,c){var d=I.a.Na(b,"DIV");I.a.gb.Uh?(I.a.J.Zg(d,I.b.m.concat(I.b.m.re,c)),d.removeChild(d.firstChild)):I.a.J.Zg(d,c);return I.a.Vi(b,d)};I.a.Vi=function(b,c){if(1==c.childNodes.length)return c.removeChild(c.firstChild);for(b=b.createDocumentFragment();c.firstChild;)b.appendChild(c.firstChild);return b};I.a.Zj=function(){return I.a.Ob(document)};I.a.Ob=function(b){return I.a.Ih?I.a.ne:"CSS1Compat"==b.compatMode};I.a.canHaveChildren=function(b){if(b.nodeType!=I.a.fa.Ja)return!1;switch(b.tagName){case "APPLET":case "AREA":case "BASE":case "BR":case "COL":case "COMMAND":case "EMBED":case "FRAME":case "HR":case "IMG":case "INPUT":case "IFRAME":case "ISINDEX":case "KEYGEN":case "LINK":case "NOFRAMES":case "NOSCRIPT":case "META":case "OBJECT":case "PARAM":case k:case "SOURCE":case "STYLE":case "TRACK":case "WBR":return!1}return!0};
I.a.appendChild=function(b,c){b.appendChild(c)};I.a.append=function(b,c){I.a.Ve(I.a.Qa(b),b,arguments,1)};I.a.Td=function(b){for(var c;c=b.firstChild;)b.removeChild(c)};I.a.kg=function(b,c){c.parentNode&&c.parentNode.insertBefore(b,c)};I.a.jg=function(b,c){c.parentNode&&c.parentNode.insertBefore(b,c.nextSibling)};I.a.ig=function(b,c,d){b.insertBefore(c,b.childNodes[d]||null)};I.a.removeNode=function(b){return b&&b.parentNode?b.parentNode.removeChild(b):null};
I.a.Rg=function(b,c){var d=c.parentNode;d&&d.replaceChild(b,c)};I.a.xf=function(b){var c,d=b.parentNode;if(d&&d.nodeType!=I.a.fa.Nh){if(b.removeNode)return b.removeNode(!1);for(;c=b.firstChild;)d.insertBefore(c,b);return I.a.removeNode(b)}};I.a.Ef=function(b){return I.a.gb.Dh&&void 0!=b.children?b.children:I.j.filter(b.childNodes,function(b){return b.nodeType==I.a.fa.Ja})};I.a.Jf=function(b){return I.W(b.firstElementChild)?b.firstElementChild:I.a.lc(b.firstChild,!0)};
I.a.Nf=function(b){return I.W(b.lastElementChild)?b.lastElementChild:I.a.lc(b.lastChild,!1)};I.a.Pf=function(b){return I.W(b.nextElementSibling)?b.nextElementSibling:I.a.lc(b.nextSibling,!0)};I.a.Wf=function(b){return I.W(b.previousElementSibling)?b.previousElementSibling:I.a.lc(b.previousSibling,!1)};I.a.lc=function(b,c){for(;b&&b.nodeType!=I.a.fa.Ja;)b=c?b.nextSibling:b.previousSibling;return b};
I.a.Qf=function(b){if(!b)return null;if(b.firstChild)return b.firstChild;for(;b&&!b.nextSibling;)b=b.parentNode;return b?b.nextSibling:null};I.a.Xf=function(b){if(!b)return null;if(!b.previousSibling)return b.parentNode;for(b=b.previousSibling;b&&b.lastChild;)b=b.lastChild;return b};I.a.Id=function(b){return I.ka(b)&&0<b.nodeType};I.a.Fd=function(b){return I.ka(b)&&b.nodeType==I.a.fa.Ja};I.a.Dg=function(b){return I.ka(b)&&b.window==b};
I.a.Vf=function(b){var c;if(I.a.gb.Eh&&!(I.userAgent.$&&I.userAgent.xa("9")&&!I.userAgent.xa("10")&&I.global.SVGElement&&b instanceof I.global.SVGElement)&&(c=b.parentElement))return c;c=b.parentNode;return I.a.Fd(c)?c:null};I.a.contains=function(b,c){if(!b||!c)return!1;if(b.contains&&c.nodeType==I.a.fa.Ja)return b==c||b.contains(c);if("undefined"!=typeof b.compareDocumentPosition)return b==c||!!(b.compareDocumentPosition(c)&16);for(;c&&b!=c;)c=c.parentNode;return c==b};
I.a.df=function(b,c){if(b==c)return 0;if(b.compareDocumentPosition)return b.compareDocumentPosition(c)&2?1:-1;if(I.userAgent.$&&!I.userAgent.Pb(9)){if(b.nodeType==I.a.fa.Sc)return-1;if(c.nodeType==I.a.fa.Sc)return 1}if("sourceIndex"in b||b.parentNode&&"sourceIndex"in b.parentNode){var d=b.nodeType==I.a.fa.Ja,e=c.nodeType==I.a.fa.Ja;if(d&&e)return b.sourceIndex-c.sourceIndex;var f=b.parentNode,g=c.parentNode;return f==g?I.a.ff(b,c):!d&&I.a.contains(f,c)?-1*I.a.ef(b,c):!e&&I.a.contains(g,b)?I.a.ef(c,
b):(d?b.sourceIndex:f.sourceIndex)-(e?c.sourceIndex:g.sourceIndex)}e=I.a.Qa(b);d=e.createRange();d.selectNode(b);d.collapse(!0);b=e.createRange();b.selectNode(c);b.collapse(!0);return d.compareBoundaryPoints(I.global.Range.START_TO_END,b)};I.a.ef=function(b,c){var d=b.parentNode;if(d==c)return-1;for(;c.parentNode!=d;)c=c.parentNode;return I.a.ff(c,b)};I.a.ff=function(b,c){for(;c=c.previousSibling;)if(c==b)return-1;return 1};
I.a.tf=function(b){var c,d=arguments.length;if(!d)return null;if(1==d)return arguments[0];var e=[],f=Infinity;for(c=0;c<d;c++){for(var g=[],h=arguments[c];h;)g.unshift(h),h=h.parentNode;e.push(g);f=Math.min(f,g.length)}g=null;for(c=0;c<f;c++){h=e[0][c];for(var l=1;l<d;l++)if(h!=e[l][c])return g;g=h}return g};I.a.Qa=function(b){return b.nodeType==I.a.fa.Sc?b:b.ownerDocument||b.document};I.a.Kf=function(b){return b.contentDocument||b.contentWindow.document};
I.a.Lf=function(b){try{return b.contentWindow||(b.contentDocument?I.a.qb(b.contentDocument):null)}catch(c){}return null};I.a.Wd=function(b,c){if("textContent"in b)b.textContent=c;else if(b.nodeType==I.a.fa.ac)b.data=String(c);else if(b.firstChild&&b.firstChild.nodeType==I.a.fa.ac){for(;b.lastChild!=b.firstChild;)b.removeChild(b.lastChild);b.firstChild.data=String(c)}else{I.a.Td(b);var d=I.a.Qa(b);b.appendChild(d.createTextNode(String(c)))}};
I.a.Uf=function(b){if("outerHTML"in b)return b.outerHTML;var c=I.a.Qa(b);c=I.a.Na(c,"DIV");c.appendChild(b.cloneNode(!0));return c.innerHTML};I.a.uf=function(b,c){var d=[];return I.a.md(b,c,d,!0)?d[0]:void 0};I.a.vf=function(b,c){var d=[];I.a.md(b,c,d,!1);return d};I.a.md=function(b,c,d,e){if(null!=b)for(b=b.firstChild;b;){if(c(b)&&(d.push(b),e)||I.a.md(b,c,d,e))return!0;b=b.nextSibling}return!1};I.a.Oe={SCRIPT:1,STYLE:1,HEAD:1,IFRAME:1,OBJECT:1};I.a.Zb={IMG:" ",BR:"\n"};
I.a.Hd=function(b){return I.a.gg(b)&&I.a.Bg(b)};I.a.Xg=function(b,c){c?b.tabIndex=0:(b.tabIndex=-1,b.removeAttribute("tabIndex"))};I.a.qg=function(b){var c;return(c=I.a.Kk(b)?!b.disabled&&(!I.a.gg(b)||I.a.Bg(b)):I.a.Hd(b))&&I.userAgent.$?I.a.Sj(b):c};I.a.gg=function(b){return I.userAgent.$&&!I.userAgent.xa("9")?(b=b.getAttributeNode("tabindex"),I.bb(b)&&b.specified):b.hasAttribute("tabindex")};I.a.Bg=function(b){b=b.tabIndex;return I.Rb(b)&&0<=b&&32768>b};
I.a.Kk=function(b){return"A"==b.tagName||"INPUT"==b.tagName||"TEXTAREA"==b.tagName||"SELECT"==b.tagName||"BUTTON"==b.tagName};I.a.Sj=function(b){b=!I.Ba(b.getBoundingClientRect)||I.userAgent.$&&null==b.parentElement?{height:b.offsetHeight,width:b.offsetWidth}:b.getBoundingClientRect();return I.bb(b)&&0<b.height&&0<b.width};
I.a.mc=function(b){if(I.a.gb.se&&null!==b&&"innerText"in b)b=I.f.Ui(b.innerText);else{var c=[];I.a.Ad(b,c,!0);b=c.join("")}b=b.replace(/ \xAD /g," ").replace(/\xAD/g,"");b=b.replace(/\u200B/g,"");I.a.gb.se||(b=b.replace(/ +/g," "));" "!=b&&(b=b.replace(/^\s*/,""));return b};I.a.tr=function(b){var c=[];I.a.Ad(b,c,!1);return c.join("")};
I.a.Ad=function(b,c,d){if(!(b.nodeName in I.a.Oe))if(b.nodeType==I.a.fa.ac)d?c.push(String(b.nodeValue).replace(/(\r\n|\r|\n)/g,"")):c.push(b.nodeValue);else if(b.nodeName in I.a.Zb)c.push(I.a.Zb[b.nodeName]);else for(b=b.firstChild;b;)I.a.Ad(b,c,d),b=b.nextSibling};I.a.Sf=function(b){return I.a.mc(b).length};I.a.Tf=function(b,c){c=c||I.a.Qa(b).body;for(var d=[];b&&b!=c;){for(var e=b;e=e.previousSibling;)d.unshift(I.a.mc(e));b=b.parentNode}return I.f.trimLeft(d.join("")).replace(/ +/g," ").length};
I.a.Rf=function(b,c,d){b=[b];for(var e=0,f=null;0<b.length&&e<c;)if(f=b.pop(),!(f.nodeName in I.a.Oe))if(f.nodeType==I.a.fa.ac){var g=f.nodeValue.replace(/(\r\n|\r|\n)/g,"").replace(/ +/g," ");e+=g.length}else if(f.nodeName in I.a.Zb)e+=I.a.Zb[f.nodeName].length;else for(g=f.childNodes.length-1;0<=g;g--)b.push(f.childNodes[g]);I.ka(d)&&(d.Ts=f?f.nodeValue.length+c-e-1:0,d.node=f);return f};
I.a.Jd=function(b){if(b&&typeof b.length==u){if(I.ka(b))return typeof b.item==p||typeof b.item==y;if(I.Ba(b))return typeof b.item==p}return!1};I.a.pd=function(b,c,d,e){if(!c&&!d)return null;var f=c?String(c).toUpperCase():null;return I.a.od(b,function(b){return(!f||b.nodeName==f)&&(!d||I.N(b.className)&&I.j.contains(b.className.split(/\s+/),d))},!0,e)};I.a.Bf=function(b,c,d){return I.a.pd(b,null,c,d)};
I.a.od=function(b,c,d,e){b&&!d&&(b=b.parentNode);for(d=0;b&&(null==e||d<=e);){if(c(b))return b;b=b.parentNode;d++}return null};I.a.Af=function(b){try{var c=b&&b.activeElement;return c&&c.nodeName?c:null}catch(d){return null}};I.a.rr=function(){var b=I.a.qb();return I.W(b.devicePixelRatio)?b.devicePixelRatio:b.matchMedia?I.a.tc(3)||I.a.tc(2)||I.a.tc(1.5)||I.a.tc(1)||.75:1};
I.a.tc=function(b){return I.a.qb().matchMedia("(min-resolution: "+b+"dppx),(min--moz-device-pixel-ratio: "+b+"),(min-resolution: "+96*b+"dpi)").matches?b:0};I.a.Df=function(b){return b.getContext("2d")};I.a.jb=function(b){this.Y=b||I.global.document||document};F=I.a.jb.prototype;F.td=I.a.td;F.yj=D("Y");F.ud=function(b){return I.a.xd(this.Y,b)};F.Gj=function(b){return I.a.Zf(this.Y,b)};F.qh=I.a.jb.prototype.ud;F.getElementsByTagName=function(b,c){return(c||this.Y).getElementsByTagName(String(b))};
F.yd=function(b,c,d){return I.a.kc(this.Y,b,c,d)};F.Bj=function(b,c,d){return I.a.wd(this.Y,b,c,d)};F.If=function(b,c){return I.a.If(b,c||this.Y)};F.vd=function(b,c){return I.a.vd(b,c||this.Y)};F.Yf=function(b,c){return I.a.Yf(b,c||this.Y)};F.rh=I.a.jb.prototype.yd;F.Gc=I.a.Gc;F.dg=function(b){return I.a.dg(b||this.qb())};F.zj=function(){return I.a.rd(this.qb())};F.ed=function(b,c,d){return I.a.jf(this.Y,arguments)};F.sh=I.a.jb.prototype.ed;F.createElement=function(b){return I.a.Na(this.Y,b)};
F.createTextNode=function(b){return this.Y.createTextNode(String(b))};F.cj=function(b,c,d){return I.a.kf(this.Y,b,c,!!d)};F.Sg=function(b){return I.a.Tg(this.Y,b)};F.Zj=function(){return I.a.Ob(this.Y)};F.qb=function(){return I.a.nc(this.Y)};F.Aj=function(){return I.a.sd(this.Y)};F.Gf=function(){return I.a.Hf(this.Y)};F.Af=function(b){return I.a.Af(b||this.Y)};F.appendChild=I.a.appendChild;F.append=I.a.append;F.canHaveChildren=I.a.canHaveChildren;F.Td=I.a.Td;F.kg=I.a.kg;F.jg=I.a.jg;F.ig=I.a.ig;
F.removeNode=I.a.removeNode;F.Rg=I.a.Rg;F.xf=I.a.xf;F.Ef=I.a.Ef;F.Jf=I.a.Jf;F.Nf=I.a.Nf;F.Pf=I.a.Pf;F.Wf=I.a.Wf;F.Qf=I.a.Qf;F.Xf=I.a.Xf;F.Id=I.a.Id;F.Fd=I.a.Fd;F.Dg=I.a.Dg;F.Vf=I.a.Vf;F.contains=I.a.contains;F.df=I.a.df;F.tf=I.a.tf;F.Qa=I.a.Qa;F.Kf=I.a.Kf;F.Lf=I.a.Lf;F.Wd=I.a.Wd;F.Uf=I.a.Uf;F.uf=I.a.uf;F.vf=I.a.vf;F.Hd=I.a.Hd;F.Xg=I.a.Xg;F.qg=I.a.qg;F.mc=I.a.mc;F.Sf=I.a.Sf;F.Tf=I.a.Tf;F.Rf=I.a.Rf;F.Jd=I.a.Jd;F.pd=I.a.pd;F.Bf=I.a.Bf;F.od=I.a.od;F.Df=I.a.Df;I.Qg={};I.Qg.so=C();I.Thenable=C();I.Thenable.prototype.then=C();I.Thenable.Ce="$goog_Thenable";I.Thenable.Ue=function(b){b.prototype[I.Thenable.Ce]=!0};I.Thenable.rg=function(b){if(!b)return!1;try{return!!b[I.Thenable.Ce]}catch(c){return!1}};I.Promise=function(b,c){this.ba=I.Promise.T.ya;this.la=void 0;this.mb=this.Ma=this.ea=null;this.kd=!1;0<I.Promise.Wa?this.Jc=0:0==I.Promise.Wa&&(this.oc=!1);I.Promise.Da&&(this.$d=[],L(this,Error("created")),this.mf=0);if(b!=I.cb)try{var d=this;b.call(c,function(b){M(d,I.Promise.T.Ka,b)},function(b){if(I.Z&&!(b instanceof I.Promise.ib))try{if(b instanceof Error)throw b;throw Error("Promise rejected.");}catch(f){}M(d,I.Promise.T.na,b)})}catch(e){M(this,I.Promise.T.na,e)}};I.Promise.Da=!1;
I.Promise.Wa=0;I.Promise.T={ya:0,Ah:1,Ka:2,na:3};I.Promise.te=function(){this.next=this.context=this.tb=this.Tb=this.Xa=null;this.bc=!1};I.Promise.te.prototype.reset=function(){this.context=this.tb=this.Tb=this.Xa=null;this.bc=!1};I.Promise.Qc=100;I.Promise.Kb=new I.async.Xb(function(){return new I.Promise.te},function(b){b.reset()},I.Promise.Qc);I.Promise.Cf=function(b,c,d){var e=I.Promise.Kb.get();e.Tb=b;e.tb=c;e.context=d;return e};I.Promise.Sk=function(b){I.Promise.Kb.put(b)};
I.Promise.resolve=function(b){if(b instanceof I.Promise)return b;var c=new I.Promise(I.cb);M(c,I.Promise.T.Ka,b);return c};I.Promise.reject=function(b){return new I.Promise(function(c,d){d(b)})};I.Promise.Bc=function(b,c,d){I.Promise.Kg(b,c,d,null)||I.async.P(I.eb(c,b))};I.Promise.race=function(b){return new I.Promise(function(c,d){b.length||c(void 0);for(var e=0,f;e<b.length;e++)f=b[e],I.Promise.Bc(f,c,d)})};
I.Promise.all=function(b){return new I.Promise(function(c,d){var e=b.length,f=[];if(e)for(var g=function(b,d){e--;f[b]=d;0==e&&c(f)},h=function(b){d(b)},l=0,m;l<b.length;l++)m=b[l],I.Promise.Bc(m,I.eb(g,l),h);else c(f)})};I.Promise.mp=function(b){return new I.Promise(function(c){var d=b.length,e=[];if(d)for(var f=function(b,f,g){d--;e[b]=f?{wj:!0,value:g}:{wj:!1,reason:g};0==d&&c(e)},g=0,h;g<b.length;g++)h=b[g],I.Promise.Bc(h,I.eb(f,g,!0),I.eb(f,g,!1));else c(e)})};
I.Promise.Nq=function(b){return new I.Promise(function(c,d){var e=b.length,f=[];if(e)for(var g=function(b){c(b)},h=function(b,c){e--;f[b]=c;0==e&&d(f)},l=0,m;l<b.length;l++)m=b[l],I.Promise.Bc(m,g,I.eb(h,l));else c(void 0)})};I.Promise.pu=function(){var b,c,d=new I.Promise(function(d,f){b=d;c=f});return new I.Promise.bi(d,b,c)};I.Promise.prototype.then=function(b,c,d){I.Promise.Da&&L(this,Error("then"));return ca(this,I.Ba(b)?b:null,I.Ba(c)?c:null,d)};I.Thenable.Ue(I.Promise);
I.Promise.prototype.cancel=function(b){this.ba==I.Promise.T.ya&&I.async.P(function(){var c=new I.Promise.ib(b);N(this,c)},this)};function N(b,c){if(b.ba==I.Promise.T.ya)if(b.ea){var d=b.ea;if(d.Ma){for(var e=0,f=null,g=null,h=d.Ma;h&&(h.bc||(e++,h.Xa==b&&(f=h),!(f&&1<e)));h=h.next)f||(g=h);f&&(d.ba==I.Promise.T.ya&&1==e?N(d,c):(g?(e=g,e.next==d.mb&&(d.mb=e),e.next=e.next.next):O(d),P(d,f,I.Promise.T.na,c)))}b.ea=null}else M(b,I.Promise.T.na,c)}
function Q(b,c){b.Ma||b.ba!=I.Promise.T.Ka&&b.ba!=I.Promise.T.na||R(b);b.mb?b.mb.next=c:b.Ma=c;b.mb=c}function ca(b,c,d,e){var f=I.Promise.Cf(null,null,null);f.Xa=new I.Promise(function(b,h){f.Tb=c?function(d){try{var f=c.call(e,d);b(f)}catch(r){h(r)}}:b;f.tb=d?function(c){try{var f=d.call(e,c);!I.W(f)&&c instanceof I.Promise.ib?h(c):b(f)}catch(r){h(r)}}:h});f.Xa.ea=b;Q(b,f);return f.Xa}I.Promise.prototype.xl=function(b){this.ba=I.Promise.T.ya;M(this,I.Promise.T.Ka,b)};
I.Promise.prototype.yl=function(b){this.ba=I.Promise.T.ya;M(this,I.Promise.T.na,b)};function M(b,c,d){b.ba==I.Promise.T.ya&&(b===d&&(c=I.Promise.T.na,d=new TypeError("Promise cannot resolve to itself")),b.ba=I.Promise.T.Ah,I.Promise.Kg(d,b.xl,b.yl,b)||(b.la=d,b.ba=c,b.ea=null,R(b),c!=I.Promise.T.na||d instanceof I.Promise.ib||I.Promise.zi(b,d)))}
I.Promise.Kg=function(b,c,d,e){if(b instanceof I.Promise)return I.Promise.Da&&L(b,Error("then")),Q(b,I.Promise.Cf(c||I.cb,d||null,e)),!0;if(I.Thenable.rg(b))return b.then(c,d,e),!0;if(I.ka(b))try{var f=b.then;if(I.Ba(f))return I.Promise.vl(b,f,c,d,e),!0}catch(g){return d.call(e,g),!0}return!1};I.Promise.vl=function(b,c,d,e,f){function g(b){l||(l=!0,e.call(f,b))}function h(b){l||(l=!0,d.call(f,b))}var l=!1;try{c.call(b,h,g)}catch(m){g(m)}};function R(b){b.kd||(b.kd=!0,I.async.P(b.qj,b))}
function O(b){var c=null;b.Ma&&(c=b.Ma,b.Ma=c.next,c.next=null);b.Ma||(b.mb=null);return c}I.Promise.prototype.qj=function(){for(var b;b=O(this);)I.Promise.Da&&this.mf++,P(this,b,this.ba,this.la);this.kd=!1};
function P(b,c,d,e){if(d==I.Promise.T.na&&c.tb&&!c.bc)if(0<I.Promise.Wa)for(;b&&b.Jc;b=b.ea)I.global.clearTimeout(b.Jc),b.Jc=0;else if(0==I.Promise.Wa)for(;b&&b.oc;b=b.ea)b.oc=!1;if(c.Xa)c.Xa.ea=null,I.Promise.mg(c,d,e);else try{c.bc?c.Tb.call(c.context):I.Promise.mg(c,d,e)}catch(f){I.Promise.pc.call(null,f)}I.Promise.Sk(c)}I.Promise.mg=function(b,c,d){c==I.Promise.T.Ka?b.Tb.call(b.context,d):b.tb&&b.tb.call(b.context,d)};
function L(b,c){if(I.Promise.Da&&I.N(c.stack)){var d=c.stack.split("\n",4)[3];c=c.message;c+=Array(11-c.length).join(" ");b.$d.push(c+d)}}function S(b,c){if(I.Promise.Da&&c&&I.N(c.stack)&&b.$d.length){for(var d=["Promise trace:"],e=b;e;e=e.ea){for(var f=b.mf;0<=f;f--)d.push(e.$d[f]);d.push("Value: ["+(e.ba==I.Promise.T.na?"REJECTED":"FULFILLED")+"] <"+String(e.la)+">")}c.stack+="\n\n"+d.join("\n")}}
I.Promise.zi=function(b,c){0<I.Promise.Wa?b.Jc=I.global.setTimeout(function(){S(b,c);I.Promise.pc.call(null,c)},I.Promise.Wa):0==I.Promise.Wa&&(b.oc=!0,I.async.P(function(){b.oc&&(S(b,c),I.Promise.pc.call(null,c))}))};I.Promise.pc=I.async.gh;I.Promise.Kt=function(b){I.Promise.pc=b};I.Promise.ib=function(b){I.debug.Error.call(this,b)};I.$a(I.Promise.ib,I.debug.Error);I.Promise.ib.prototype.name="cancel";I.Promise.bi=function(b,c,d){this.Qg=b;this.resolve=c;this.reject=d};/*
 Portions of this code are from MochiKit, received by
 The Closure Authors under the MIT license. All other code is Copyright
 2005-2009 The Closure Authors. All Rights Reserved.
*/
I.async.B=function(b,c){this.Fc=[];this.Pg=b;this.nf=c||null;this.rb=this.nb=!1;this.la=void 0;this.Xd=this.Oi=this.$c=!1;this.Ic=0;this.ea=null;this.cc=0;I.async.B.Da&&(this.dd=null,Error.captureStackTrace&&(b={stack:""},Error.captureStackTrace(b,I.async.B),typeof b.stack==y&&(this.dd=b.stack.replace(/^[^\n]*\n/,""))))};I.async.B.li=!1;I.async.B.Da=!1;F=I.async.B.prototype;
F.cancel=function(b){if(this.nb)this.la instanceof I.async.B&&this.la.cancel();else{if(this.ea){var c=this.ea;delete this.ea;b?c.cancel(b):(c.cc--,0>=c.cc&&c.cancel())}this.Pg?this.Pg.call(this.nf,this):this.Xd=!0;this.nb||this.Pa(new I.async.B.hb(this))}};F.hf=function(b,c){this.$c=!1;T(this,b,c)};function T(b,c,d){b.nb=!0;b.la=d;b.rb=!c;U(b)}function V(b){if(b.nb){if(!b.Xd)throw new I.async.B.Ub(b);b.Xd=!1}}F.Cb=function(b){V(this);T(this,!0,b)};F.Pa=function(b){V(this);W(this,b);T(this,!1,b)};
function W(b,c){I.async.B.Da&&b.dd&&I.ka(c)&&c.stack&&/^[^\n]+(\n   [^\n]+)+/.test(c.stack)&&(c.stack=c.stack+"\nDEFERRED OPERATION:\n"+b.dd)}function X(b,c,d){return Y(b,c,null,d)}function da(b,c){Y(b,null,c,void 0)}function Y(b,c,d,e){b.Fc.push([c,d,e]);b.nb&&U(b);return b}F.then=function(b,c,d){var e,f,g=new I.Promise(function(b,c){e=b;f=c});Y(this,e,function(b){b instanceof I.async.B.hb?g.cancel():f(b)});return g.then(b,c,d)};I.Thenable.Ue(I.async.B);
I.async.B.prototype.Qi=function(){var b=new I.async.B;Y(this,b.Cb,b.Pa,b);b.ea=this;this.cc++;return b};function Z(b){return I.j.some(b.Fc,function(b){return I.Ba(b[1])})}
function U(b){b.Ic&&b.nb&&Z(b)&&(I.async.B.Dl(b.Ic),b.Ic=0);b.ea&&(b.ea.cc--,delete b.ea);for(var c=b.la,d=!1,e=!1;b.Fc.length&&!b.$c;){var f=b.Fc.shift(),g=f[0],h=f[1];f=f[2];if(g=b.rb?h:g)try{var l=g.call(f||b.nf,c);I.W(l)&&(b.rb=b.rb&&(l==c||l instanceof Error),b.la=c=l);if(I.Thenable.rg(c)||typeof I.global.Promise===p&&c instanceof I.global.Promise)e=!0,b.$c=!0}catch(m){c=m,b.rb=!0,W(b,c),Z(b)||(d=!0)}}b.la=c;e?(e=I.bind(b.hf,b,!0),l=I.bind(b.hf,b,!1),c instanceof I.async.B?(Y(c,e,l),c.Oi=!0):
c.then(e,l)):I.async.B.li&&c instanceof Error&&!(c instanceof I.async.B.hb)&&(d=b.rb=!0);d&&(b.Ic=I.async.B.el(c))}I.async.B.eh=function(b){var c=new I.async.B;c.Cb(b);return c};I.async.B.Xq=function(b){var c=new I.async.B;b.then(function(b){c.Cb(b)},function(b){c.Pa(b)});return c};I.async.B.ha=function(b){var c=new I.async.B;c.Pa(b);return c};I.async.B.Up=function(){var b=new I.async.B;b.cancel();return b};
I.async.B.ou=function(b,c,d){return b instanceof I.async.B?X(b.Qi(),c,d):X(I.async.B.eh(b),c,d)};I.async.B.Ub=function(){I.debug.Error.call(this)};I.$a(I.async.B.Ub,I.debug.Error);I.async.B.Ub.prototype.message="Deferred has already fired";I.async.B.Ub.prototype.name="AlreadyCalledError";I.async.B.hb=function(){I.debug.Error.call(this)};I.$a(I.async.B.hb,I.debug.Error);I.async.B.hb.prototype.message="Deferred was canceled";I.async.B.hb.prototype.name="CanceledError";
I.async.B.ye=function(b){this.Mb=I.global.setTimeout(I.bind(this.fh,this),0);this.oj=b};I.async.B.ye.prototype.fh=function(){delete I.async.B.Jb[this.Mb];throw this.oj;};I.async.B.Jb={};I.async.B.el=function(b){b=new I.async.B.ye(b);I.async.B.Jb[b.Mb]=b;return b.Mb};I.async.B.Dl=function(b){var c=I.async.B.Jb[b];c&&(I.global.clearTimeout(c.Mb),delete I.async.B.Jb[b])};I.async.B.Hp=function(){var b=I.async.B.Jb,c;for(c in b){var d=b[c];I.global.clearTimeout(d.Mb);d.fh()}};I.D={};I.D.F={};I.D.F.Uc="closure_verification";I.D.F.Lh=5E3;I.D.F.Vd=[];I.D.F.al=function(b,c){function d(){var e=b.shift();e=I.D.F.Cc(e,c);b.length&&Y(e,d,d,void 0);return e}if(!b.length)return I.async.B.eh(null);var e=I.D.F.Vd.length;I.j.extend(I.D.F.Vd,b);if(e)return I.D.F.Vg;b=I.D.F.Vd;I.D.F.Vg=d();return I.D.F.Vg};
I.D.F.Cc=function(b,c){var d=c||{};c=d.document||document;var e=I.b.C.s(b),f=I.a.createElement(k),g={Wg:f,ih:void 0},h=new I.async.B(I.D.F.Ti,g),l=null,m=I.bb(d.timeout)?d.timeout:I.D.F.Lh;0<m&&(l=window.setTimeout(function(){I.D.F.ec(f,!0);h.Pa(new I.D.F.Error(I.D.F.Wb.TIMEOUT,"Timeout reached for loading script "+e))},m),g.ih=l);f.onload=f.onreadystatechange=function(){f.readyState&&"loaded"!=f.readyState&&"complete"!=f.readyState||(I.D.F.ec(f,d.aq||!1,l),h.Cb(null))};f.onerror=function(){I.D.F.ec(f,
!0,l);h.Pa(new I.D.F.Error(I.D.F.Wb.Vh,"Error while loading script "+e))};g=d.attributes||{};I.object.extend(g,{type:z,charset:"UTF-8"});I.a.Gc(f,g);I.a.J.il(f,b);I.D.F.Ij(c).appendChild(f);return h};
I.D.F.gt=function(b,c,d){I.global[I.D.F.Uc]||(I.global[I.D.F.Uc]={});var e=I.global[I.D.F.Uc],f=I.b.C.s(b);if(I.W(e[c]))return I.async.B.ha(new I.D.F.Error(I.D.F.Wb.xi,"Verification object "+c+" already defined."));b=I.D.F.Cc(b,d);var g=new I.async.B(I.bind(b.cancel,b));X(b,function(){var b=e[c];I.W(b)?(g.Cb(b),delete e[c]):g.Pa(new I.D.F.Error(I.D.F.Wb.wi,"Script "+f+" loaded, but verification object "+c+" was not defined."))});da(b,function(b){I.W(e[c])&&delete e[c];g.Pa(b)});return g};
I.D.F.Ij=function(b){var c=I.a.getElementsByTagName("HEAD",b);return!c||I.j.Qb(c)?b.documentElement:c[0]};I.D.F.Ti=function(){if(this&&this.Wg){var b=this.Wg;b&&b.tagName==k&&I.D.F.ec(b,!0,this.ih)}};I.D.F.ec=function(b,c,d){I.bb(d)&&I.global.clearTimeout(d);b.onload=I.cb;b.onerror=I.cb;b.onreadystatechange=I.cb;c&&window.setTimeout(function(){I.a.removeNode(b)},0)};I.D.F.Wb={Vh:0,TIMEOUT:1,wi:2,xi:3};
I.D.F.Error=function(b,c){var d="Jsloader error (code #"+b+")";c&&(d+=": "+c);I.debug.Error.call(this,d);this.code=b};I.$a(I.D.F.Error,I.debug.Error);var google={G:{}};google.G.K={};google.G.K.Ea={};google.G.K.Ea.hh=3E4;google.G.K.Ea.xs=function(b,c){return{format:b,Bi:c}};google.G.K.Ea.Lj=function(b){return I.b.C.format(b.format,b.Bi)};google.G.K.Ea.load=function(b,c){b=I.b.C.format(b,c);var d=I.D.F.Cc(b,{timeout:google.G.K.Ea.hh,attributes:{async:!1,defer:!1}});return new Promise(function(b){X(d,b)})};
google.G.K.Ea.ss=function(b){b=I.j.map(b,google.G.K.Ea.Lj);if(I.j.Qb(b))return Promise.resolve();var c={timeout:google.G.K.Ea.hh,attributes:{async:!1,defer:!1}},d=[];!I.userAgent.$||I.userAgent.xa(11)?I.j.forEach(b,function(b){d.push(I.D.F.Cc(b,c))}):d.push(I.D.F.al(b,c));return Promise.all(I.j.map(d,function(b){return new Promise(function(c){return X(b,c)})}))};google.G.K.U={};if(I.ob(q))throw Error("Google Charts loader.js can only be loaded once.");google.G.K.U.Il={1:"1.0","1.0":"current","1.1":"upcoming",41:w,42:w,43:w,44:w,46:"46.1","46.1":"46.2",previous:"45.2",current:"46",upcoming:"46.2"};google.G.K.U.Fk=function(b){var c=b,d=b.match(/^testing-/);d&&(c=c.replace(/^testing-/,""));b=c;do{var e=google.G.K.U.Il[c];e&&(c=e)}while(e);d=(d?"testing-":"")+c;return{version:c==w?b:d,yk:d}};google.G.K.U.oh=null;
google.G.K.U.xk=function(b){var c=google.G.K.U.Fk(b),d=I.f.H.from("/vendor/gcharts/%{version}/loader.js");return google.G.K.Ea.load(d,{version:c.yk}).then(function(){var d=I.ob("google.charts.loader.VersionSpecific.load")||I.ob("google.charts.loader.publicLoad")||I.ob("google.charts.versionSpecific.load");if(!d)throw Error("Bad version: "+b);google.G.K.U.oh=function(b){b=d(c.version,b);if(null==b||null==b.then){var e=I.ob("google.charts.loader.publicSetOnLoadCallback")||I.ob("google.charts.versionSpecific.setOnLoadCallback");
b=new Promise(function(b){e(b)});b.then=e}return b}})};google.G.K.U.Ld=null;google.G.K.U.gc=null;google.G.K.U.vk=function(b,c){if(!google.G.K.U.Ld){if(c.enableUrlSettings&&window.URLSearchParams)try{b=(new URLSearchParams(top.location.search)).get("charts-version")||b}catch(d){console.info("Failed to get charts-version from top URL",d)}google.G.K.U.Ld=google.G.K.U.xk(b)}return google.G.K.U.gc=google.G.K.U.Ld.then(function(){return google.G.K.U.oh(c)})};
google.G.K.U.hl=function(b){if(!google.G.K.U.gc)throw Error("Must call google.charts.load before google.charts.setOnLoadCallback");return b?google.G.K.U.gc.then(b):google.G.K.U.gc};google.G.load=function(b){for(var c=[],d=0;d<arguments.length;++d)c[d-0]=arguments[d];d=0;"visualization"===c[d]&&d++;var e="current";I.N(c[d])&&(e=c[d],d++);var f={};I.ka(c[d])&&(f=c[d]);return google.G.K.U.vk(e,f)};I.rf(q,google.G.load);google.G.gl=google.G.K.U.hl;I.rf("google.charts.setOnLoadCallback",google.G.gl);}).call(this);

/*!
 * jQuery JavaScript Library v3.2.1
 * https://jquery.com/
 *
 * Includes Sizzle.js
 * https://sizzlejs.com/
 *
 * Copyright JS Foundation and other contributors
 * Released under the MIT license
 * https://jquery.org/license
 *
 * Date: 2017-03-20T18:59Z
 */
( function( global, factory ) {

	"use strict";

	if ( typeof module === "object" && typeof module.exports === "object" ) {

		// For CommonJS and CommonJS-like environments where a proper `window`
		// is present, execute the factory and get jQuery.
		// For environments that do not have a `window` with a `document`
		// (such as Node.js), expose a factory as module.exports.
		// This accentuates the need for the creation of a real `window`.
		// e.g. var jQuery = require("jquery")(window);
		// See ticket #14549 for more info.
		module.exports = global.document ?
			factory( global, true ) :
			function( w ) {
				if ( !w.document ) {
					throw new Error( "jQuery requires a window with a document" );
				}
				return factory( w );
			};
	} else {
		factory( global );
	}

// Pass this if window is not defined yet
} )( typeof window !== "undefined" ? window : this, function( window, noGlobal ) {

// Edge <= 12 - 13+, Firefox <=18 - 45+, IE 10 - 11, Safari 5.1 - 9+, iOS 6 - 9.1
// throw exceptions when non-strict code (e.g., ASP.NET 4.5) accesses strict mode
// arguments.callee.caller (trac-13335). But as of jQuery 3.0 (2016), strict mode should be common
// enough that all such attempts are guarded in a try block.
"use strict";

var arr = [];

var document = window.document;

var getProto = Object.getPrototypeOf;

var slice = arr.slice;

var concat = arr.concat;

var push = arr.push;

var indexOf = arr.indexOf;

var class2type = {};

var toString = class2type.toString;

var hasOwn = class2type.hasOwnProperty;

var fnToString = hasOwn.toString;

var ObjectFunctionString = fnToString.call( Object );

var support = {};



	function DOMEval( code, doc ) {
		doc = doc || document;

		var script = doc.createElement( "script" );

		script.text = code;
		doc.head.appendChild( script ).parentNode.removeChild( script );
	}
/* global Symbol */
// Defining this global in .eslintrc.json would create a danger of using the global
// unguarded in another place, it seems safer to define global only for this module



var
	version = "3.2.1",

	// Define a local copy of jQuery
	jQuery = function( selector, context ) {

		// The jQuery object is actually just the init constructor 'enhanced'
		// Need init if jQuery is called (just allow error to be thrown if not included)
		return new jQuery.fn.init( selector, context );
	},

	// Support: Android <=4.0 only
	// Make sure we trim BOM and NBSP
	rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,

	// Matches dashed string for camelizing
	rmsPrefix = /^-ms-/,
	rdashAlpha = /-([a-z])/g,

	// Used by jQuery.camelCase as callback to replace()
	fcamelCase = function( all, letter ) {
		return letter.toUpperCase();
	};

jQuery.fn = jQuery.prototype = {

	// The current version of jQuery being used
	jquery: version,

	constructor: jQuery,

	// The default length of a jQuery object is 0
	length: 0,

	toArray: function() {
		return slice.call( this );
	},

	// Get the Nth element in the matched element set OR
	// Get the whole matched element set as a clean array
	get: function( num ) {

		// Return all the elements in a clean array
		if ( num == null ) {
			return slice.call( this );
		}

		// Return just the one element from the set
		return num < 0 ? this[ num + this.length ] : this[ num ];
	},

	// Take an array of elements and push it onto the stack
	// (returning the new matched element set)
	pushStack: function( elems ) {

		// Build a new jQuery matched element set
		var ret = jQuery.merge( this.constructor(), elems );

		// Add the old object onto the stack (as a reference)
		ret.prevObject = this;

		// Return the newly-formed element set
		return ret;
	},

	// Execute a callback for every element in the matched set.
	each: function( callback ) {
		return jQuery.each( this, callback );
	},

	map: function( callback ) {
		return this.pushStack( jQuery.map( this, function( elem, i ) {
			return callback.call( elem, i, elem );
		} ) );
	},

	slice: function() {
		return this.pushStack( slice.apply( this, arguments ) );
	},

	first: function() {
		return this.eq( 0 );
	},

	last: function() {
		return this.eq( -1 );
	},

	eq: function( i ) {
		var len = this.length,
			j = +i + ( i < 0 ? len : 0 );
		return this.pushStack( j >= 0 && j < len ? [ this[ j ] ] : [] );
	},

	end: function() {
		return this.prevObject || this.constructor();
	},

	// For internal use only.
	// Behaves like an Array's method, not like a jQuery method.
	push: push,
	sort: arr.sort,
	splice: arr.splice
};

jQuery.extend = jQuery.fn.extend = function() {
	var options, name, src, copy, copyIsArray, clone,
		target = arguments[ 0 ] || {},
		i = 1,
		length = arguments.length,
		deep = false;

	// Handle a deep copy situation
	if ( typeof target === "boolean" ) {
		deep = target;

		// Skip the boolean and the target
		target = arguments[ i ] || {};
		i++;
	}

	// Handle case when target is a string or something (possible in deep copy)
	if ( typeof target !== "object" && !jQuery.isFunction( target ) ) {
		target = {};
	}

	// Extend jQuery itself if only one argument is passed
	if ( i === length ) {
		target = this;
		i--;
	}

	for ( ; i < length; i++ ) {

		// Only deal with non-null/undefined values
		if ( ( options = arguments[ i ] ) != null ) {

			// Extend the base object
			for ( name in options ) {
				src = target[ name ];
				copy = options[ name ];

				// Prevent never-ending loop
				if ( target === copy ) {
					continue;
				}

				// Recurse if we're merging plain objects or arrays
				if ( deep && copy && ( jQuery.isPlainObject( copy ) ||
					( copyIsArray = Array.isArray( copy ) ) ) ) {

					if ( copyIsArray ) {
						copyIsArray = false;
						clone = src && Array.isArray( src ) ? src : [];

					} else {
						clone = src && jQuery.isPlainObject( src ) ? src : {};
					}

					// Never move original objects, clone them
					target[ name ] = jQuery.extend( deep, clone, copy );

				// Don't bring in undefined values
				} else if ( copy !== undefined ) {
					target[ name ] = copy;
				}
			}
		}
	}

	// Return the modified object
	return target;
};

jQuery.extend( {

	// Unique for each copy of jQuery on the page
	expando: "jQuery" + ( version + Math.random() ).replace( /\D/g, "" ),

	// Assume jQuery is ready without the ready module
	isReady: true,

	error: function( msg ) {
		throw new Error( msg );
	},

	noop: function() {},

	isFunction: function( obj ) {
		return jQuery.type( obj ) === "function";
	},

	isWindow: function( obj ) {
		return obj != null && obj === obj.window;
	},

	isNumeric: function( obj ) {

		// As of jQuery 3.0, isNumeric is limited to
		// strings and numbers (primitives or objects)
		// that can be coerced to finite numbers (gh-2662)
		var type = jQuery.type( obj );
		return ( type === "number" || type === "string" ) &&

			// parseFloat NaNs numeric-cast false positives ("")
			// ...but misinterprets leading-number strings, particularly hex literals ("0x...")
			// subtraction forces infinities to NaN
			!isNaN( obj - parseFloat( obj ) );
	},

	isPlainObject: function( obj ) {
		var proto, Ctor;

		// Detect obvious negatives
		// Use toString instead of jQuery.type to catch host objects
		if ( !obj || toString.call( obj ) !== "[object Object]" ) {
			return false;
		}

		proto = getProto( obj );

		// Objects with no prototype (e.g., `Object.create( null )`) are plain
		if ( !proto ) {
			return true;
		}

		// Objects with prototype are plain iff they were constructed by a global Object function
		Ctor = hasOwn.call( proto, "constructor" ) && proto.constructor;
		return typeof Ctor === "function" && fnToString.call( Ctor ) === ObjectFunctionString;
	},

	isEmptyObject: function( obj ) {

		/* eslint-disable no-unused-vars */
		// See https://github.com/eslint/eslint/issues/6125
		var name;

		for ( name in obj ) {
			return false;
		}
		return true;
	},

	type: function( obj ) {
		if ( obj == null ) {
			return obj + "";
		}

		// Support: Android <=2.3 only (functionish RegExp)
		return typeof obj === "object" || typeof obj === "function" ?
			class2type[ toString.call( obj ) ] || "object" :
			typeof obj;
	},

	// Evaluates a script in a global context
	globalEval: function( code ) {
		DOMEval( code );
	},

	// Convert dashed to camelCase; used by the css and data modules
	// Support: IE <=9 - 11, Edge 12 - 13
	// Microsoft forgot to hump their vendor prefix (#9572)
	camelCase: function( string ) {
		return string.replace( rmsPrefix, "ms-" ).replace( rdashAlpha, fcamelCase );
	},

	each: function( obj, callback ) {
		var length, i = 0;

		if ( isArrayLike( obj ) ) {
			length = obj.length;
			for ( ; i < length; i++ ) {
				if ( callback.call( obj[ i ], i, obj[ i ] ) === false ) {
					break;
				}
			}
		} else {
			for ( i in obj ) {
				if ( callback.call( obj[ i ], i, obj[ i ] ) === false ) {
					break;
				}
			}
		}

		return obj;
	},

	// Support: Android <=4.0 only
	trim: function( text ) {
		return text == null ?
			"" :
			( text + "" ).replace( rtrim, "" );
	},

	// results is for internal usage only
	makeArray: function( arr, results ) {
		var ret = results || [];

		if ( arr != null ) {
			if ( isArrayLike( Object( arr ) ) ) {
				jQuery.merge( ret,
					typeof arr === "string" ?
					[ arr ] : arr
				);
			} else {
				push.call( ret, arr );
			}
		}

		return ret;
	},

	inArray: function( elem, arr, i ) {
		return arr == null ? -1 : indexOf.call( arr, elem, i );
	},

	// Support: Android <=4.0 only, PhantomJS 1 only
	// push.apply(_, arraylike) throws on ancient WebKit
	merge: function( first, second ) {
		var len = +second.length,
			j = 0,
			i = first.length;

		for ( ; j < len; j++ ) {
			first[ i++ ] = second[ j ];
		}

		first.length = i;

		return first;
	},

	grep: function( elems, callback, invert ) {
		var callbackInverse,
			matches = [],
			i = 0,
			length = elems.length,
			callbackExpect = !invert;

		// Go through the array, only saving the items
		// that pass the validator function
		for ( ; i < length; i++ ) {
			callbackInverse = !callback( elems[ i ], i );
			if ( callbackInverse !== callbackExpect ) {
				matches.push( elems[ i ] );
			}
		}

		return matches;
	},

	// arg is for internal usage only
	map: function( elems, callback, arg ) {
		var length, value,
			i = 0,
			ret = [];

		// Go through the array, translating each of the items to their new values
		if ( isArrayLike( elems ) ) {
			length = elems.length;
			for ( ; i < length; i++ ) {
				value = callback( elems[ i ], i, arg );

				if ( value != null ) {
					ret.push( value );
				}
			}

		// Go through every key on the object,
		} else {
			for ( i in elems ) {
				value = callback( elems[ i ], i, arg );

				if ( value != null ) {
					ret.push( value );
				}
			}
		}

		// Flatten any nested arrays
		return concat.apply( [], ret );
	},

	// A global GUID counter for objects
	guid: 1,

	// Bind a function to a context, optionally partially applying any
	// arguments.
	proxy: function( fn, context ) {
		var tmp, args, proxy;

		if ( typeof context === "string" ) {
			tmp = fn[ context ];
			context = fn;
			fn = tmp;
		}

		// Quick check to determine if target is callable, in the spec
		// this throws a TypeError, but we will just return undefined.
		if ( !jQuery.isFunction( fn ) ) {
			return undefined;
		}

		// Simulated bind
		args = slice.call( arguments, 2 );
		proxy = function() {
			return fn.apply( context || this, args.concat( slice.call( arguments ) ) );
		};

		// Set the guid of unique handler to the same of original handler, so it can be removed
		proxy.guid = fn.guid = fn.guid || jQuery.guid++;

		return proxy;
	},

	now: Date.now,

	// jQuery.support is not used in Core but other projects attach their
	// properties to it so it needs to exist.
	support: support
} );

if ( typeof Symbol === "function" ) {
	jQuery.fn[ Symbol.iterator ] = arr[ Symbol.iterator ];
}

// Populate the class2type map
jQuery.each( "Boolean Number String Function Array Date RegExp Object Error Symbol".split( " " ),
function( i, name ) {
	class2type[ "[object " + name + "]" ] = name.toLowerCase();
} );

function isArrayLike( obj ) {

	// Support: real iOS 8.2 only (not reproducible in simulator)
	// `in` check used to prevent JIT error (gh-2145)
	// hasOwn isn't used here due to false negatives
	// regarding Nodelist length in IE
	var length = !!obj && "length" in obj && obj.length,
		type = jQuery.type( obj );

	if ( type === "function" || jQuery.isWindow( obj ) ) {
		return false;
	}

	return type === "array" || length === 0 ||
		typeof length === "number" && length > 0 && ( length - 1 ) in obj;
}
var Sizzle =
/*!
 * Sizzle CSS Selector Engine v2.3.3
 * https://sizzlejs.com/
 *
 * Copyright jQuery Foundation and other contributors
 * Released under the MIT license
 * http://jquery.org/license
 *
 * Date: 2016-08-08
 */
(function( window ) {

var i,
	support,
	Expr,
	getText,
	isXML,
	tokenize,
	compile,
	select,
	outermostContext,
	sortInput,
	hasDuplicate,

	// Local document vars
	setDocument,
	document,
	docElem,
	documentIsHTML,
	rbuggyQSA,
	rbuggyMatches,
	matches,
	contains,

	// Instance-specific data
	expando = "sizzle" + 1 * new Date(),
	preferredDoc = window.document,
	dirruns = 0,
	done = 0,
	classCache = createCache(),
	tokenCache = createCache(),
	compilerCache = createCache(),
	sortOrder = function( a, b ) {
		if ( a === b ) {
			hasDuplicate = true;
		}
		return 0;
	},

	// Instance methods
	hasOwn = ({}).hasOwnProperty,
	arr = [],
	pop = arr.pop,
	push_native = arr.push,
	push = arr.push,
	slice = arr.slice,
	// Use a stripped-down indexOf as it's faster than native
	// https://jsperf.com/thor-indexof-vs-for/5
	indexOf = function( list, elem ) {
		var i = 0,
			len = list.length;
		for ( ; i < len; i++ ) {
			if ( list[i] === elem ) {
				return i;
			}
		}
		return -1;
	},

	booleans = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",

	// Regular expressions

	// http://www.w3.org/TR/css3-selectors/#whitespace
	whitespace = "[\\x20\\t\\r\\n\\f]",

	// http://www.w3.org/TR/CSS21/syndata.html#value-def-identifier
	identifier = "(?:\\\\.|[\\w-]|[^\0-\\xa0])+",

	// Attribute selectors: http://www.w3.org/TR/selectors/#attribute-selectors
	attributes = "\\[" + whitespace + "*(" + identifier + ")(?:" + whitespace +
		// Operator (capture 2)
		"*([*^$|!~]?=)" + whitespace +
		// "Attribute values must be CSS identifiers [capture 5] or strings [capture 3 or capture 4]"
		"*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + identifier + "))|)" + whitespace +
		"*\\]",

	pseudos = ":(" + identifier + ")(?:\\((" +
		// To reduce the number of selectors needing tokenize in the preFilter, prefer arguments:
		// 1. quoted (capture 3; capture 4 or capture 5)
		"('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|" +
		// 2. simple (capture 6)
		"((?:\\\\.|[^\\\\()[\\]]|" + attributes + ")*)|" +
		// 3. anything else (capture 2)
		".*" +
		")\\)|)",

	// Leading and non-escaped trailing whitespace, capturing some non-whitespace characters preceding the latter
	rwhitespace = new RegExp( whitespace + "+", "g" ),
	rtrim = new RegExp( "^" + whitespace + "+|((?:^|[^\\\\])(?:\\\\.)*)" + whitespace + "+$", "g" ),

	rcomma = new RegExp( "^" + whitespace + "*," + whitespace + "*" ),
	rcombinators = new RegExp( "^" + whitespace + "*([>+~]|" + whitespace + ")" + whitespace + "*" ),

	rattributeQuotes = new RegExp( "=" + whitespace + "*([^\\]'\"]*?)" + whitespace + "*\\]", "g" ),

	rpseudo = new RegExp( pseudos ),
	ridentifier = new RegExp( "^" + identifier + "$" ),

	matchExpr = {
		"ID": new RegExp( "^#(" + identifier + ")" ),
		"CLASS": new RegExp( "^\\.(" + identifier + ")" ),
		"TAG": new RegExp( "^(" + identifier + "|[*])" ),
		"ATTR": new RegExp( "^" + attributes ),
		"PSEUDO": new RegExp( "^" + pseudos ),
		"CHILD": new RegExp( "^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + whitespace +
			"*(even|odd|(([+-]|)(\\d*)n|)" + whitespace + "*(?:([+-]|)" + whitespace +
			"*(\\d+)|))" + whitespace + "*\\)|)", "i" ),
		"bool": new RegExp( "^(?:" + booleans + ")$", "i" ),
		// For use in libraries implementing .is()
		// We use this for POS matching in `select`
		"needsContext": new RegExp( "^" + whitespace + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" +
			whitespace + "*((?:-\\d)?\\d*)" + whitespace + "*\\)|)(?=[^-]|$)", "i" )
	},

	rinputs = /^(?:input|select|textarea|button)$/i,
	rheader = /^h\d$/i,

	rnative = /^[^{]+\{\s*\[native \w/,

	// Easily-parseable/retrievable ID or TAG or CLASS selectors
	rquickExpr = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,

	rsibling = /[+~]/,

	// CSS escapes
	// http://www.w3.org/TR/CSS21/syndata.html#escaped-characters
	runescape = new RegExp( "\\\\([\\da-f]{1,6}" + whitespace + "?|(" + whitespace + ")|.)", "ig" ),
	funescape = function( _, escaped, escapedWhitespace ) {
		var high = "0x" + escaped - 0x10000;
		// NaN means non-codepoint
		// Support: Firefox<24
		// Workaround erroneous numeric interpretation of +"0x"
		return high !== high || escapedWhitespace ?
			escaped :
			high < 0 ?
				// BMP codepoint
				String.fromCharCode( high + 0x10000 ) :
				// Supplemental Plane codepoint (surrogate pair)
				String.fromCharCode( high >> 10 | 0xD800, high & 0x3FF | 0xDC00 );
	},

	// CSS string/identifier serialization
	// https://drafts.csswg.org/cssom/#common-serializing-idioms
	rcssescape = /([\0-\x1f\x7f]|^-?\d)|^-$|[^\0-\x1f\x7f-\uFFFF\w-]/g,
	fcssescape = function( ch, asCodePoint ) {
		if ( asCodePoint ) {

			// U+0000 NULL becomes U+FFFD REPLACEMENT CHARACTER
			if ( ch === "\0" ) {
				return "\uFFFD";
			}

			// Control characters and (dependent upon position) numbers get escaped as code points
			return ch.slice( 0, -1 ) + "\\" + ch.charCodeAt( ch.length - 1 ).toString( 16 ) + " ";
		}

		// Other potentially-special ASCII characters get backslash-escaped
		return "\\" + ch;
	},

	// Used for iframes
	// See setDocument()
	// Removing the function wrapper causes a "Permission Denied"
	// error in IE
	unloadHandler = function() {
		setDocument();
	},

	disabledAncestor = addCombinator(
		function( elem ) {
			return elem.disabled === true && ("form" in elem || "label" in elem);
		},
		{ dir: "parentNode", next: "legend" }
	);

// Optimize for push.apply( _, NodeList )
try {
	push.apply(
		(arr = slice.call( preferredDoc.childNodes )),
		preferredDoc.childNodes
	);
	// Support: Android<4.0
	// Detect silently failing push.apply
	arr[ preferredDoc.childNodes.length ].nodeType;
} catch ( e ) {
	push = { apply: arr.length ?

		// Leverage slice if possible
		function( target, els ) {
			push_native.apply( target, slice.call(els) );
		} :

		// Support: IE<9
		// Otherwise append directly
		function( target, els ) {
			var j = target.length,
				i = 0;
			// Can't trust NodeList.length
			while ( (target[j++] = els[i++]) ) {}
			target.length = j - 1;
		}
	};
}

function Sizzle( selector, context, results, seed ) {
	var m, i, elem, nid, match, groups, newSelector,
		newContext = context && context.ownerDocument,

		// nodeType defaults to 9, since context defaults to document
		nodeType = context ? context.nodeType : 9;

	results = results || [];

	// Return early from calls with invalid selector or context
	if ( typeof selector !== "string" || !selector ||
		nodeType !== 1 && nodeType !== 9 && nodeType !== 11 ) {

		return results;
	}

	// Try to shortcut find operations (as opposed to filters) in HTML documents
	if ( !seed ) {

		if ( ( context ? context.ownerDocument || context : preferredDoc ) !== document ) {
			setDocument( context );
		}
		context = context || document;

		if ( documentIsHTML ) {

			// If the selector is sufficiently simple, try using a "get*By*" DOM method
			// (excepting DocumentFragment context, where the methods don't exist)
			if ( nodeType !== 11 && (match = rquickExpr.exec( selector )) ) {

				// ID selector
				if ( (m = match[1]) ) {

					// Document context
					if ( nodeType === 9 ) {
						if ( (elem = context.getElementById( m )) ) {

							// Support: IE, Opera, Webkit
							// TODO: identify versions
							// getElementById can match elements by name instead of ID
							if ( elem.id === m ) {
								results.push( elem );
								return results;
							}
						} else {
							return results;
						}

					// Element context
					} else {

						// Support: IE, Opera, Webkit
						// TODO: identify versions
						// getElementById can match elements by name instead of ID
						if ( newContext && (elem = newContext.getElementById( m )) &&
							contains( context, elem ) &&
							elem.id === m ) {

							results.push( elem );
							return results;
						}
					}

				// Type selector
				} else if ( match[2] ) {
					push.apply( results, context.getElementsByTagName( selector ) );
					return results;

				// Class selector
				} else if ( (m = match[3]) && support.getElementsByClassName &&
					context.getElementsByClassName ) {

					push.apply( results, context.getElementsByClassName( m ) );
					return results;
				}
			}

			// Take advantage of querySelectorAll
			if ( support.qsa &&
				!compilerCache[ selector + " " ] &&
				(!rbuggyQSA || !rbuggyQSA.test( selector )) ) {

				if ( nodeType !== 1 ) {
					newContext = context;
					newSelector = selector;

				// qSA looks outside Element context, which is not what we want
				// Thanks to Andrew Dupont for this workaround technique
				// Support: IE <=8
				// Exclude object elements
				} else if ( context.nodeName.toLowerCase() !== "object" ) {

					// Capture the context ID, setting it first if necessary
					if ( (nid = context.getAttribute( "id" )) ) {
						nid = nid.replace( rcssescape, fcssescape );
					} else {
						context.setAttribute( "id", (nid = expando) );
					}

					// Prefix every selector in the list
					groups = tokenize( selector );
					i = groups.length;
					while ( i-- ) {
						groups[i] = "#" + nid + " " + toSelector( groups[i] );
					}
					newSelector = groups.join( "," );

					// Expand context for sibling selectors
					newContext = rsibling.test( selector ) && testContext( context.parentNode ) ||
						context;
				}

				if ( newSelector ) {
					try {
						push.apply( results,
							newContext.querySelectorAll( newSelector )
						);
						return results;
					} catch ( qsaError ) {
					} finally {
						if ( nid === expando ) {
							context.removeAttribute( "id" );
						}
					}
				}
			}
		}
	}

	// All others
	return select( selector.replace( rtrim, "$1" ), context, results, seed );
}

/**
 * Create key-value caches of limited size
 * @returns {function(string, object)} Returns the Object data after storing it on itself with
 *	property name the (space-suffixed) string and (if the cache is larger than Expr.cacheLength)
 *	deleting the oldest entry
 */
function createCache() {
	var keys = [];

	function cache( key, value ) {
		// Use (key + " ") to avoid collision with native prototype properties (see Issue #157)
		if ( keys.push( key + " " ) > Expr.cacheLength ) {
			// Only keep the most recent entries
			delete cache[ keys.shift() ];
		}
		return (cache[ key + " " ] = value);
	}
	return cache;
}

/**
 * Mark a function for special use by Sizzle
 * @param {Function} fn The function to mark
 */
function markFunction( fn ) {
	fn[ expando ] = true;
	return fn;
}

/**
 * Support testing using an element
 * @param {Function} fn Passed the created element and returns a boolean result
 */
function assert( fn ) {
	var el = document.createElement("fieldset");

	try {
		return !!fn( el );
	} catch (e) {
		return false;
	} finally {
		// Remove from its parent by default
		if ( el.parentNode ) {
			el.parentNode.removeChild( el );
		}
		// release memory in IE
		el = null;
	}
}

/**
 * Adds the same handler for all of the specified attrs
 * @param {String} attrs Pipe-separated list of attributes
 * @param {Function} handler The method that will be applied
 */
function addHandle( attrs, handler ) {
	var arr = attrs.split("|"),
		i = arr.length;

	while ( i-- ) {
		Expr.attrHandle[ arr[i] ] = handler;
	}
}

/**
 * Checks document order of two siblings
 * @param {Element} a
 * @param {Element} b
 * @returns {Number} Returns less than 0 if a precedes b, greater than 0 if a follows b
 */
function siblingCheck( a, b ) {
	var cur = b && a,
		diff = cur && a.nodeType === 1 && b.nodeType === 1 &&
			a.sourceIndex - b.sourceIndex;

	// Use IE sourceIndex if available on both nodes
	if ( diff ) {
		return diff;
	}

	// Check if b follows a
	if ( cur ) {
		while ( (cur = cur.nextSibling) ) {
			if ( cur === b ) {
				return -1;
			}
		}
	}

	return a ? 1 : -1;
}

/**
 * Returns a function to use in pseudos for input types
 * @param {String} type
 */
function createInputPseudo( type ) {
	return function( elem ) {
		var name = elem.nodeName.toLowerCase();
		return name === "input" && elem.type === type;
	};
}

/**
 * Returns a function to use in pseudos for buttons
 * @param {String} type
 */
function createButtonPseudo( type ) {
	return function( elem ) {
		var name = elem.nodeName.toLowerCase();
		return (name === "input" || name === "button") && elem.type === type;
	};
}

/**
 * Returns a function to use in pseudos for :enabled/:disabled
 * @param {Boolean} disabled true for :disabled; false for :enabled
 */
function createDisabledPseudo( disabled ) {

	// Known :disabled false positives: fieldset[disabled] > legend:nth-of-type(n+2) :can-disable
	return function( elem ) {

		// Only certain elements can match :enabled or :disabled
		// https://html.spec.whatwg.org/multipage/scripting.html#selector-enabled
		// https://html.spec.whatwg.org/multipage/scripting.html#selector-disabled
		if ( "form" in elem ) {

			// Check for inherited disabledness on relevant non-disabled elements:
			// * listed form-associated elements in a disabled fieldset
			//   https://html.spec.whatwg.org/multipage/forms.html#category-listed
			//   https://html.spec.whatwg.org/multipage/forms.html#concept-fe-disabled
			// * option elements in a disabled optgroup
			//   https://html.spec.whatwg.org/multipage/forms.html#concept-option-disabled
			// All such elements have a "form" property.
			if ( elem.parentNode && elem.disabled === false ) {

				// Option elements defer to a parent optgroup if present
				if ( "label" in elem ) {
					if ( "label" in elem.parentNode ) {
						return elem.parentNode.disabled === disabled;
					} else {
						return elem.disabled === disabled;
					}
				}

				// Support: IE 6 - 11
				// Use the isDisabled shortcut property to check for disabled fieldset ancestors
				return elem.isDisabled === disabled ||

					// Where there is no isDisabled, check manually
					/* jshint -W018 */
					elem.isDisabled !== !disabled &&
						disabledAncestor( elem ) === disabled;
			}

			return elem.disabled === disabled;

		// Try to winnow out elements that can't be disabled before trusting the disabled property.
		// Some victims get caught in our net (label, legend, menu, track), but it shouldn't
		// even exist on them, let alone have a boolean value.
		} else if ( "label" in elem ) {
			return elem.disabled === disabled;
		}

		// Remaining elements are neither :enabled nor :disabled
		return false;
	};
}

/**
 * Returns a function to use in pseudos for positionals
 * @param {Function} fn
 */
function createPositionalPseudo( fn ) {
	return markFunction(function( argument ) {
		argument = +argument;
		return markFunction(function( seed, matches ) {
			var j,
				matchIndexes = fn( [], seed.length, argument ),
				i = matchIndexes.length;

			// Match elements found at the specified indexes
			while ( i-- ) {
				if ( seed[ (j = matchIndexes[i]) ] ) {
					seed[j] = !(matches[j] = seed[j]);
				}
			}
		});
	});
}

/**
 * Checks a node for validity as a Sizzle context
 * @param {Element|Object=} context
 * @returns {Element|Object|Boolean} The input node if acceptable, otherwise a falsy value
 */
function testContext( context ) {
	return context && typeof context.getElementsByTagName !== "undefined" && context;
}

// Expose support vars for convenience
support = Sizzle.support = {};

/**
 * Detects XML nodes
 * @param {Element|Object} elem An element or a document
 * @returns {Boolean} True iff elem is a non-HTML XML node
 */
isXML = Sizzle.isXML = function( elem ) {
	// documentElement is verified for cases where it doesn't yet exist
	// (such as loading iframes in IE - #4833)
	var documentElement = elem && (elem.ownerDocument || elem).documentElement;
	return documentElement ? documentElement.nodeName !== "HTML" : false;
};

/**
 * Sets document-related variables once based on the current document
 * @param {Element|Object} [doc] An element or document object to use to set the document
 * @returns {Object} Returns the current document
 */
setDocument = Sizzle.setDocument = function( node ) {
	var hasCompare, subWindow,
		doc = node ? node.ownerDocument || node : preferredDoc;

	// Return early if doc is invalid or already selected
	if ( doc === document || doc.nodeType !== 9 || !doc.documentElement ) {
		return document;
	}

	// Update global variables
	document = doc;
	docElem = document.documentElement;
	documentIsHTML = !isXML( document );

	// Support: IE 9-11, Edge
	// Accessing iframe documents after unload throws "permission denied" errors (jQuery #13936)
	if ( preferredDoc !== document &&
		(subWindow = document.defaultView) && subWindow.top !== subWindow ) {

		// Support: IE 11, Edge
		if ( subWindow.addEventListener ) {
			subWindow.addEventListener( "unload", unloadHandler, false );

		// Support: IE 9 - 10 only
		} else if ( subWindow.attachEvent ) {
			subWindow.attachEvent( "onunload", unloadHandler );
		}
	}

	/* Attributes
	---------------------------------------------------------------------- */

	// Support: IE<8
	// Verify that getAttribute really returns attributes and not properties
	// (excepting IE8 booleans)
	support.attributes = assert(function( el ) {
		el.className = "i";
		return !el.getAttribute("className");
	});

	/* getElement(s)By*
	---------------------------------------------------------------------- */

	// Check if getElementsByTagName("*") returns only elements
	support.getElementsByTagName = assert(function( el ) {
		el.appendChild( document.createComment("") );
		return !el.getElementsByTagName("*").length;
	});

	// Support: IE<9
	support.getElementsByClassName = rnative.test( document.getElementsByClassName );

	// Support: IE<10
	// Check if getElementById returns elements by name
	// The broken getElementById methods don't pick up programmatically-set names,
	// so use a roundabout getElementsByName test
	support.getById = assert(function( el ) {
		docElem.appendChild( el ).id = expando;
		return !document.getElementsByName || !document.getElementsByName( expando ).length;
	});

	// ID filter and find
	if ( support.getById ) {
		Expr.filter["ID"] = function( id ) {
			var attrId = id.replace( runescape, funescape );
			return function( elem ) {
				return elem.getAttribute("id") === attrId;
			};
		};
		Expr.find["ID"] = function( id, context ) {
			if ( typeof context.getElementById !== "undefined" && documentIsHTML ) {
				var elem = context.getElementById( id );
				return elem ? [ elem ] : [];
			}
		};
	} else {
		Expr.filter["ID"] =  function( id ) {
			var attrId = id.replace( runescape, funescape );
			return function( elem ) {
				var node = typeof elem.getAttributeNode !== "undefined" &&
					elem.getAttributeNode("id");
				return node && node.value === attrId;
			};
		};

		// Support: IE 6 - 7 only
		// getElementById is not reliable as a find shortcut
		Expr.find["ID"] = function( id, context ) {
			if ( typeof context.getElementById !== "undefined" && documentIsHTML ) {
				var node, i, elems,
					elem = context.getElementById( id );

				if ( elem ) {

					// Verify the id attribute
					node = elem.getAttributeNode("id");
					if ( node && node.value === id ) {
						return [ elem ];
					}

					// Fall back on getElementsByName
					elems = context.getElementsByName( id );
					i = 0;
					while ( (elem = elems[i++]) ) {
						node = elem.getAttributeNode("id");
						if ( node && node.value === id ) {
							return [ elem ];
						}
					}
				}

				return [];
			}
		};
	}

	// Tag
	Expr.find["TAG"] = support.getElementsByTagName ?
		function( tag, context ) {
			if ( typeof context.getElementsByTagName !== "undefined" ) {
				return context.getElementsByTagName( tag );

			// DocumentFragment nodes don't have gEBTN
			} else if ( support.qsa ) {
				return context.querySelectorAll( tag );
			}
		} :

		function( tag, context ) {
			var elem,
				tmp = [],
				i = 0,
				// By happy coincidence, a (broken) gEBTN appears on DocumentFragment nodes too
				results = context.getElementsByTagName( tag );

			// Filter out possible comments
			if ( tag === "*" ) {
				while ( (elem = results[i++]) ) {
					if ( elem.nodeType === 1 ) {
						tmp.push( elem );
					}
				}

				return tmp;
			}
			return results;
		};

	// Class
	Expr.find["CLASS"] = support.getElementsByClassName && function( className, context ) {
		if ( typeof context.getElementsByClassName !== "undefined" && documentIsHTML ) {
			return context.getElementsByClassName( className );
		}
	};

	/* QSA/matchesSelector
	---------------------------------------------------------------------- */

	// QSA and matchesSelector support

	// matchesSelector(:active) reports false when true (IE9/Opera 11.5)
	rbuggyMatches = [];

	// qSa(:focus) reports false when true (Chrome 21)
	// We allow this because of a bug in IE8/9 that throws an error
	// whenever `document.activeElement` is accessed on an iframe
	// So, we allow :focus to pass through QSA all the time to avoid the IE error
	// See https://bugs.jquery.com/ticket/13378
	rbuggyQSA = [];

	if ( (support.qsa = rnative.test( document.querySelectorAll )) ) {
		// Build QSA regex
		// Regex strategy adopted from Diego Perini
		assert(function( el ) {
			// Select is set to empty string on purpose
			// This is to test IE's treatment of not explicitly
			// setting a boolean content attribute,
			// since its presence should be enough
			// https://bugs.jquery.com/ticket/12359
			docElem.appendChild( el ).innerHTML = "<a id='" + expando + "'></a>" +
				"<select id='" + expando + "-\r\\' msallowcapture=''>" +
				"<option selected=''></option></select>";

			// Support: IE8, Opera 11-12.16
			// Nothing should be selected when empty strings follow ^= or $= or *=
			// The test attribute must be unknown in Opera but "safe" for WinRT
			// https://msdn.microsoft.com/en-us/library/ie/hh465388.aspx#attribute_section
			if ( el.querySelectorAll("[msallowcapture^='']").length ) {
				rbuggyQSA.push( "[*^$]=" + whitespace + "*(?:''|\"\")" );
			}

			// Support: IE8
			// Boolean attributes and "value" are not treated correctly
			if ( !el.querySelectorAll("[selected]").length ) {
				rbuggyQSA.push( "\\[" + whitespace + "*(?:value|" + booleans + ")" );
			}

			// Support: Chrome<29, Android<4.4, Safari<7.0+, iOS<7.0+, PhantomJS<1.9.8+
			if ( !el.querySelectorAll( "[id~=" + expando + "-]" ).length ) {
				rbuggyQSA.push("~=");
			}

			// Webkit/Opera - :checked should return selected option elements
			// http://www.w3.org/TR/2011/REC-css3-selectors-20110929/#checked
			// IE8 throws error here and will not see later tests
			if ( !el.querySelectorAll(":checked").length ) {
				rbuggyQSA.push(":checked");
			}

			// Support: Safari 8+, iOS 8+
			// https://bugs.webkit.org/show_bug.cgi?id=136851
			// In-page `selector#id sibling-combinator selector` fails
			if ( !el.querySelectorAll( "a#" + expando + "+*" ).length ) {
				rbuggyQSA.push(".#.+[+~]");
			}
		});

		assert(function( el ) {
			el.innerHTML = "<a href='' disabled='disabled'></a>" +
				"<select disabled='disabled'><option/></select>";

			// Support: Windows 8 Native Apps
			// The type and name attributes are restricted during .innerHTML assignment
			var input = document.createElement("input");
			input.setAttribute( "type", "hidden" );
			el.appendChild( input ).setAttribute( "name", "D" );

			// Support: IE8
			// Enforce case-sensitivity of name attribute
			if ( el.querySelectorAll("[name=d]").length ) {
				rbuggyQSA.push( "name" + whitespace + "*[*^$|!~]?=" );
			}

			// FF 3.5 - :enabled/:disabled and hidden elements (hidden elements are still enabled)
			// IE8 throws error here and will not see later tests
			if ( el.querySelectorAll(":enabled").length !== 2 ) {
				rbuggyQSA.push( ":enabled", ":disabled" );
			}

			// Support: IE9-11+
			// IE's :disabled selector does not pick up the children of disabled fieldsets
			docElem.appendChild( el ).disabled = true;
			if ( el.querySelectorAll(":disabled").length !== 2 ) {
				rbuggyQSA.push( ":enabled", ":disabled" );
			}

			// Opera 10-11 does not throw on post-comma invalid pseudos
			el.querySelectorAll("*,:x");
			rbuggyQSA.push(",.*:");
		});
	}

	if ( (support.matchesSelector = rnative.test( (matches = docElem.matches ||
		docElem.webkitMatchesSelector ||
		docElem.mozMatchesSelector ||
		docElem.oMatchesSelector ||
		docElem.msMatchesSelector) )) ) {

		assert(function( el ) {
			// Check to see if it's possible to do matchesSelector
			// on a disconnected node (IE 9)
			support.disconnectedMatch = matches.call( el, "*" );

			// This should fail with an exception
			// Gecko does not error, returns false instead
			matches.call( el, "[s!='']:x" );
			rbuggyMatches.push( "!=", pseudos );
		});
	}

	rbuggyQSA = rbuggyQSA.length && new RegExp( rbuggyQSA.join("|") );
	rbuggyMatches = rbuggyMatches.length && new RegExp( rbuggyMatches.join("|") );

	/* Contains
	---------------------------------------------------------------------- */
	hasCompare = rnative.test( docElem.compareDocumentPosition );

	// Element contains another
	// Purposefully self-exclusive
	// As in, an element does not contain itself
	contains = hasCompare || rnative.test( docElem.contains ) ?
		function( a, b ) {
			var adown = a.nodeType === 9 ? a.documentElement : a,
				bup = b && b.parentNode;
			return a === bup || !!( bup && bup.nodeType === 1 && (
				adown.contains ?
					adown.contains( bup ) :
					a.compareDocumentPosition && a.compareDocumentPosition( bup ) & 16
			));
		} :
		function( a, b ) {
			if ( b ) {
				while ( (b = b.parentNode) ) {
					if ( b === a ) {
						return true;
					}
				}
			}
			return false;
		};

	/* Sorting
	---------------------------------------------------------------------- */

	// Document order sorting
	sortOrder = hasCompare ?
	function( a, b ) {

		// Flag for duplicate removal
		if ( a === b ) {
			hasDuplicate = true;
			return 0;
		}

		// Sort on method existence if only one input has compareDocumentPosition
		var compare = !a.compareDocumentPosition - !b.compareDocumentPosition;
		if ( compare ) {
			return compare;
		}

		// Calculate position if both inputs belong to the same document
		compare = ( a.ownerDocument || a ) === ( b.ownerDocument || b ) ?
			a.compareDocumentPosition( b ) :

			// Otherwise we know they are disconnected
			1;

		// Disconnected nodes
		if ( compare & 1 ||
			(!support.sortDetached && b.compareDocumentPosition( a ) === compare) ) {

			// Choose the first element that is related to our preferred document
			if ( a === document || a.ownerDocument === preferredDoc && contains(preferredDoc, a) ) {
				return -1;
			}
			if ( b === document || b.ownerDocument === preferredDoc && contains(preferredDoc, b) ) {
				return 1;
			}

			// Maintain original order
			return sortInput ?
				( indexOf( sortInput, a ) - indexOf( sortInput, b ) ) :
				0;
		}

		return compare & 4 ? -1 : 1;
	} :
	function( a, b ) {
		// Exit early if the nodes are identical
		if ( a === b ) {
			hasDuplicate = true;
			return 0;
		}

		var cur,
			i = 0,
			aup = a.parentNode,
			bup = b.parentNode,
			ap = [ a ],
			bp = [ b ];

		// Parentless nodes are either documents or disconnected
		if ( !aup || !bup ) {
			return a === document ? -1 :
				b === document ? 1 :
				aup ? -1 :
				bup ? 1 :
				sortInput ?
				( indexOf( sortInput, a ) - indexOf( sortInput, b ) ) :
				0;

		// If the nodes are siblings, we can do a quick check
		} else if ( aup === bup ) {
			return siblingCheck( a, b );
		}

		// Otherwise we need full lists of their ancestors for comparison
		cur = a;
		while ( (cur = cur.parentNode) ) {
			ap.unshift( cur );
		}
		cur = b;
		while ( (cur = cur.parentNode) ) {
			bp.unshift( cur );
		}

		// Walk down the tree looking for a discrepancy
		while ( ap[i] === bp[i] ) {
			i++;
		}

		return i ?
			// Do a sibling check if the nodes have a common ancestor
			siblingCheck( ap[i], bp[i] ) :

			// Otherwise nodes in our document sort first
			ap[i] === preferredDoc ? -1 :
			bp[i] === preferredDoc ? 1 :
			0;
	};

	return document;
};

Sizzle.matches = function( expr, elements ) {
	return Sizzle( expr, null, null, elements );
};

Sizzle.matchesSelector = function( elem, expr ) {
	// Set document vars if needed
	if ( ( elem.ownerDocument || elem ) !== document ) {
		setDocument( elem );
	}

	// Make sure that attribute selectors are quoted
	expr = expr.replace( rattributeQuotes, "='$1']" );

	if ( support.matchesSelector && documentIsHTML &&
		!compilerCache[ expr + " " ] &&
		( !rbuggyMatches || !rbuggyMatches.test( expr ) ) &&
		( !rbuggyQSA     || !rbuggyQSA.test( expr ) ) ) {

		try {
			var ret = matches.call( elem, expr );

			// IE 9's matchesSelector returns false on disconnected nodes
			if ( ret || support.disconnectedMatch ||
					// As well, disconnected nodes are said to be in a document
					// fragment in IE 9
					elem.document && elem.document.nodeType !== 11 ) {
				return ret;
			}
		} catch (e) {}
	}

	return Sizzle( expr, document, null, [ elem ] ).length > 0;
};

Sizzle.contains = function( context, elem ) {
	// Set document vars if needed
	if ( ( context.ownerDocument || context ) !== document ) {
		setDocument( context );
	}
	return contains( context, elem );
};

Sizzle.attr = function( elem, name ) {
	// Set document vars if needed
	if ( ( elem.ownerDocument || elem ) !== document ) {
		setDocument( elem );
	}

	var fn = Expr.attrHandle[ name.toLowerCase() ],
		// Don't get fooled by Object.prototype properties (jQuery #13807)
		val = fn && hasOwn.call( Expr.attrHandle, name.toLowerCase() ) ?
			fn( elem, name, !documentIsHTML ) :
			undefined;

	return val !== undefined ?
		val :
		support.attributes || !documentIsHTML ?
			elem.getAttribute( name ) :
			(val = elem.getAttributeNode(name)) && val.specified ?
				val.value :
				null;
};

Sizzle.escape = function( sel ) {
	return (sel + "").replace( rcssescape, fcssescape );
};

Sizzle.error = function( msg ) {
	throw new Error( "Syntax error, unrecognized expression: " + msg );
};

/**
 * Document sorting and removing duplicates
 * @param {ArrayLike} results
 */
Sizzle.uniqueSort = function( results ) {
	var elem,
		duplicates = [],
		j = 0,
		i = 0;

	// Unless we *know* we can detect duplicates, assume their presence
	hasDuplicate = !support.detectDuplicates;
	sortInput = !support.sortStable && results.slice( 0 );
	results.sort( sortOrder );

	if ( hasDuplicate ) {
		while ( (elem = results[i++]) ) {
			if ( elem === results[ i ] ) {
				j = duplicates.push( i );
			}
		}
		while ( j-- ) {
			results.splice( duplicates[ j ], 1 );
		}
	}

	// Clear input after sorting to release objects
	// See https://github.com/jquery/sizzle/pull/225
	sortInput = null;

	return results;
};

/**
 * Utility function for retrieving the text value of an array of DOM nodes
 * @param {Array|Element} elem
 */
getText = Sizzle.getText = function( elem ) {
	var node,
		ret = "",
		i = 0,
		nodeType = elem.nodeType;

	if ( !nodeType ) {
		// If no nodeType, this is expected to be an array
		while ( (node = elem[i++]) ) {
			// Do not traverse comment nodes
			ret += getText( node );
		}
	} else if ( nodeType === 1 || nodeType === 9 || nodeType === 11 ) {
		// Use textContent for elements
		// innerText usage removed for consistency of new lines (jQuery #11153)
		if ( typeof elem.textContent === "string" ) {
			return elem.textContent;
		} else {
			// Traverse its children
			for ( elem = elem.firstChild; elem; elem = elem.nextSibling ) {
				ret += getText( elem );
			}
		}
	} else if ( nodeType === 3 || nodeType === 4 ) {
		return elem.nodeValue;
	}
	// Do not include comment or processing instruction nodes

	return ret;
};

Expr = Sizzle.selectors = {

	// Can be adjusted by the user
	cacheLength: 50,

	createPseudo: markFunction,

	match: matchExpr,

	attrHandle: {},

	find: {},

	relative: {
		">": { dir: "parentNode", first: true },
		" ": { dir: "parentNode" },
		"+": { dir: "previousSibling", first: true },
		"~": { dir: "previousSibling" }
	},

	preFilter: {
		"ATTR": function( match ) {
			match[1] = match[1].replace( runescape, funescape );

			// Move the given value to match[3] whether quoted or unquoted
			match[3] = ( match[3] || match[4] || match[5] || "" ).replace( runescape, funescape );

			if ( match[2] === "~=" ) {
				match[3] = " " + match[3] + " ";
			}

			return match.slice( 0, 4 );
		},

		"CHILD": function( match ) {
			/* matches from matchExpr["CHILD"]
				1 type (only|nth|...)
				2 what (child|of-type)
				3 argument (even|odd|\d*|\d*n([+-]\d+)?|...)
				4 xn-component of xn+y argument ([+-]?\d*n|)
				5 sign of xn-component
				6 x of xn-component
				7 sign of y-component
				8 y of y-component
			*/
			match[1] = match[1].toLowerCase();

			if ( match[1].slice( 0, 3 ) === "nth" ) {
				// nth-* requires argument
				if ( !match[3] ) {
					Sizzle.error( match[0] );
				}

				// numeric x and y parameters for Expr.filter.CHILD
				// remember that false/true cast respectively to 0/1
				match[4] = +( match[4] ? match[5] + (match[6] || 1) : 2 * ( match[3] === "even" || match[3] === "odd" ) );
				match[5] = +( ( match[7] + match[8] ) || match[3] === "odd" );

			// other types prohibit arguments
			} else if ( match[3] ) {
				Sizzle.error( match[0] );
			}

			return match;
		},

		"PSEUDO": function( match ) {
			var excess,
				unquoted = !match[6] && match[2];

			if ( matchExpr["CHILD"].test( match[0] ) ) {
				return null;
			}

			// Accept quoted arguments as-is
			if ( match[3] ) {
				match[2] = match[4] || match[5] || "";

			// Strip excess characters from unquoted arguments
			} else if ( unquoted && rpseudo.test( unquoted ) &&
				// Get excess from tokenize (recursively)
				(excess = tokenize( unquoted, true )) &&
				// advance to the next closing parenthesis
				(excess = unquoted.indexOf( ")", unquoted.length - excess ) - unquoted.length) ) {

				// excess is a negative index
				match[0] = match[0].slice( 0, excess );
				match[2] = unquoted.slice( 0, excess );
			}

			// Return only captures needed by the pseudo filter method (type and argument)
			return match.slice( 0, 3 );
		}
	},

	filter: {

		"TAG": function( nodeNameSelector ) {
			var nodeName = nodeNameSelector.replace( runescape, funescape ).toLowerCase();
			return nodeNameSelector === "*" ?
				function() { return true; } :
				function( elem ) {
					return elem.nodeName && elem.nodeName.toLowerCase() === nodeName;
				};
		},

		"CLASS": function( className ) {
			var pattern = classCache[ className + " " ];

			return pattern ||
				(pattern = new RegExp( "(^|" + whitespace + ")" + className + "(" + whitespace + "|$)" )) &&
				classCache( className, function( elem ) {
					return pattern.test( typeof elem.className === "string" && elem.className || typeof elem.getAttribute !== "undefined" && elem.getAttribute("class") || "" );
				});
		},

		"ATTR": function( name, operator, check ) {
			return function( elem ) {
				var result = Sizzle.attr( elem, name );

				if ( result == null ) {
					return operator === "!=";
				}
				if ( !operator ) {
					return true;
				}

				result += "";

				return operator === "=" ? result === check :
					operator === "!=" ? result !== check :
					operator === "^=" ? check && result.indexOf( check ) === 0 :
					operator === "*=" ? check && result.indexOf( check ) > -1 :
					operator === "$=" ? check && result.slice( -check.length ) === check :
					operator === "~=" ? ( " " + result.replace( rwhitespace, " " ) + " " ).indexOf( check ) > -1 :
					operator === "|=" ? result === check || result.slice( 0, check.length + 1 ) === check + "-" :
					false;
			};
		},

		"CHILD": function( type, what, argument, first, last ) {
			var simple = type.slice( 0, 3 ) !== "nth",
				forward = type.slice( -4 ) !== "last",
				ofType = what === "of-type";

			return first === 1 && last === 0 ?

				// Shortcut for :nth-*(n)
				function( elem ) {
					return !!elem.parentNode;
				} :

				function( elem, context, xml ) {
					var cache, uniqueCache, outerCache, node, nodeIndex, start,
						dir = simple !== forward ? "nextSibling" : "previousSibling",
						parent = elem.parentNode,
						name = ofType && elem.nodeName.toLowerCase(),
						useCache = !xml && !ofType,
						diff = false;

					if ( parent ) {

						// :(first|last|only)-(child|of-type)
						if ( simple ) {
							while ( dir ) {
								node = elem;
								while ( (node = node[ dir ]) ) {
									if ( ofType ?
										node.nodeName.toLowerCase() === name :
										node.nodeType === 1 ) {

										return false;
									}
								}
								// Reverse direction for :only-* (if we haven't yet done so)
								start = dir = type === "only" && !start && "nextSibling";
							}
							return true;
						}

						start = [ forward ? parent.firstChild : parent.lastChild ];

						// non-xml :nth-child(...) stores cache data on `parent`
						if ( forward && useCache ) {

							// Seek `elem` from a previously-cached index

							// ...in a gzip-friendly way
							node = parent;
							outerCache = node[ expando ] || (node[ expando ] = {});

							// Support: IE <9 only
							// Defend against cloned attroperties (jQuery gh-1709)
							uniqueCache = outerCache[ node.uniqueID ] ||
								(outerCache[ node.uniqueID ] = {});

							cache = uniqueCache[ type ] || [];
							nodeIndex = cache[ 0 ] === dirruns && cache[ 1 ];
							diff = nodeIndex && cache[ 2 ];
							node = nodeIndex && parent.childNodes[ nodeIndex ];

							while ( (node = ++nodeIndex && node && node[ dir ] ||

								// Fallback to seeking `elem` from the start
								(diff = nodeIndex = 0) || start.pop()) ) {

								// When found, cache indexes on `parent` and break
								if ( node.nodeType === 1 && ++diff && node === elem ) {
									uniqueCache[ type ] = [ dirruns, nodeIndex, diff ];
									break;
								}
							}

						} else {
							// Use previously-cached element index if available
							if ( useCache ) {
								// ...in a gzip-friendly way
								node = elem;
								outerCache = node[ expando ] || (node[ expando ] = {});

								// Support: IE <9 only
								// Defend against cloned attroperties (jQuery gh-1709)
								uniqueCache = outerCache[ node.uniqueID ] ||
									(outerCache[ node.uniqueID ] = {});

								cache = uniqueCache[ type ] || [];
								nodeIndex = cache[ 0 ] === dirruns && cache[ 1 ];
								diff = nodeIndex;
							}

							// xml :nth-child(...)
							// or :nth-last-child(...) or :nth(-last)?-of-type(...)
							if ( diff === false ) {
								// Use the same loop as above to seek `elem` from the start
								while ( (node = ++nodeIndex && node && node[ dir ] ||
									(diff = nodeIndex = 0) || start.pop()) ) {

									if ( ( ofType ?
										node.nodeName.toLowerCase() === name :
										node.nodeType === 1 ) &&
										++diff ) {

										// Cache the index of each encountered element
										if ( useCache ) {
											outerCache = node[ expando ] || (node[ expando ] = {});

											// Support: IE <9 only
											// Defend against cloned attroperties (jQuery gh-1709)
											uniqueCache = outerCache[ node.uniqueID ] ||
												(outerCache[ node.uniqueID ] = {});

											uniqueCache[ type ] = [ dirruns, diff ];
										}

										if ( node === elem ) {
											break;
										}
									}
								}
							}
						}

						// Incorporate the offset, then check against cycle size
						diff -= last;
						return diff === first || ( diff % first === 0 && diff / first >= 0 );
					}
				};
		},

		"PSEUDO": function( pseudo, argument ) {
			// pseudo-class names are case-insensitive
			// http://www.w3.org/TR/selectors/#pseudo-classes
			// Prioritize by case sensitivity in case custom pseudos are added with uppercase letters
			// Remember that setFilters inherits from pseudos
			var args,
				fn = Expr.pseudos[ pseudo ] || Expr.setFilters[ pseudo.toLowerCase() ] ||
					Sizzle.error( "unsupported pseudo: " + pseudo );

			// The user may use createPseudo to indicate that
			// arguments are needed to create the filter function
			// just as Sizzle does
			if ( fn[ expando ] ) {
				return fn( argument );
			}

			// But maintain support for old signatures
			if ( fn.length > 1 ) {
				args = [ pseudo, pseudo, "", argument ];
				return Expr.setFilters.hasOwnProperty( pseudo.toLowerCase() ) ?
					markFunction(function( seed, matches ) {
						var idx,
							matched = fn( seed, argument ),
							i = matched.length;
						while ( i-- ) {
							idx = indexOf( seed, matched[i] );
							seed[ idx ] = !( matches[ idx ] = matched[i] );
						}
					}) :
					function( elem ) {
						return fn( elem, 0, args );
					};
			}

			return fn;
		}
	},

	pseudos: {
		// Potentially complex pseudos
		"not": markFunction(function( selector ) {
			// Trim the selector passed to compile
			// to avoid treating leading and trailing
			// spaces as combinators
			var input = [],
				results = [],
				matcher = compile( selector.replace( rtrim, "$1" ) );

			return matcher[ expando ] ?
				markFunction(function( seed, matches, context, xml ) {
					var elem,
						unmatched = matcher( seed, null, xml, [] ),
						i = seed.length;

					// Match elements unmatched by `matcher`
					while ( i-- ) {
						if ( (elem = unmatched[i]) ) {
							seed[i] = !(matches[i] = elem);
						}
					}
				}) :
				function( elem, context, xml ) {
					input[0] = elem;
					matcher( input, null, xml, results );
					// Don't keep the element (issue #299)
					input[0] = null;
					return !results.pop();
				};
		}),

		"has": markFunction(function( selector ) {
			return function( elem ) {
				return Sizzle( selector, elem ).length > 0;
			};
		}),

		"contains": markFunction(function( text ) {
			text = text.replace( runescape, funescape );
			return function( elem ) {
				return ( elem.textContent || elem.innerText || getText( elem ) ).indexOf( text ) > -1;
			};
		}),

		// "Whether an element is represented by a :lang() selector
		// is based solely on the element's language value
		// being equal to the identifier C,
		// or beginning with the identifier C immediately followed by "-".
		// The matching of C against the element's language value is performed case-insensitively.
		// The identifier C does not have to be a valid language name."
		// http://www.w3.org/TR/selectors/#lang-pseudo
		"lang": markFunction( function( lang ) {
			// lang value must be a valid identifier
			if ( !ridentifier.test(lang || "") ) {
				Sizzle.error( "unsupported lang: " + lang );
			}
			lang = lang.replace( runescape, funescape ).toLowerCase();
			return function( elem ) {
				var elemLang;
				do {
					if ( (elemLang = documentIsHTML ?
						elem.lang :
						elem.getAttribute("xml:lang") || elem.getAttribute("lang")) ) {

						elemLang = elemLang.toLowerCase();
						return elemLang === lang || elemLang.indexOf( lang + "-" ) === 0;
					}
				} while ( (elem = elem.parentNode) && elem.nodeType === 1 );
				return false;
			};
		}),

		// Miscellaneous
		"target": function( elem ) {
			var hash = window.location && window.location.hash;
			return hash && hash.slice( 1 ) === elem.id;
		},

		"root": function( elem ) {
			return elem === docElem;
		},

		"focus": function( elem ) {
			return elem === document.activeElement && (!document.hasFocus || document.hasFocus()) && !!(elem.type || elem.href || ~elem.tabIndex);
		},

		// Boolean properties
		"enabled": createDisabledPseudo( false ),
		"disabled": createDisabledPseudo( true ),

		"checked": function( elem ) {
			// In CSS3, :checked should return both checked and selected elements
			// http://www.w3.org/TR/2011/REC-css3-selectors-20110929/#checked
			var nodeName = elem.nodeName.toLowerCase();
			return (nodeName === "input" && !!elem.checked) || (nodeName === "option" && !!elem.selected);
		},

		"selected": function( elem ) {
			// Accessing this property makes selected-by-default
			// options in Safari work properly
			if ( elem.parentNode ) {
				elem.parentNode.selectedIndex;
			}

			return elem.selected === true;
		},

		// Contents
		"empty": function( elem ) {
			// http://www.w3.org/TR/selectors/#empty-pseudo
			// :empty is negated by element (1) or content nodes (text: 3; cdata: 4; entity ref: 5),
			//   but not by others (comment: 8; processing instruction: 7; etc.)
			// nodeType < 6 works because attributes (2) do not appear as children
			for ( elem = elem.firstChild; elem; elem = elem.nextSibling ) {
				if ( elem.nodeType < 6 ) {
					return false;
				}
			}
			return true;
		},

		"parent": function( elem ) {
			return !Expr.pseudos["empty"]( elem );
		},

		// Element/input types
		"header": function( elem ) {
			return rheader.test( elem.nodeName );
		},

		"input": function( elem ) {
			return rinputs.test( elem.nodeName );
		},

		"button": function( elem ) {
			var name = elem.nodeName.toLowerCase();
			return name === "input" && elem.type === "button" || name === "button";
		},

		"text": function( elem ) {
			var attr;
			return elem.nodeName.toLowerCase() === "input" &&
				elem.type === "text" &&

				// Support: IE<8
				// New HTML5 attribute values (e.g., "search") appear with elem.type === "text"
				( (attr = elem.getAttribute("type")) == null || attr.toLowerCase() === "text" );
		},

		// Position-in-collection
		"first": createPositionalPseudo(function() {
			return [ 0 ];
		}),

		"last": createPositionalPseudo(function( matchIndexes, length ) {
			return [ length - 1 ];
		}),

		"eq": createPositionalPseudo(function( matchIndexes, length, argument ) {
			return [ argument < 0 ? argument + length : argument ];
		}),

		"even": createPositionalPseudo(function( matchIndexes, length ) {
			var i = 0;
			for ( ; i < length; i += 2 ) {
				matchIndexes.push( i );
			}
			return matchIndexes;
		}),

		"odd": createPositionalPseudo(function( matchIndexes, length ) {
			var i = 1;
			for ( ; i < length; i += 2 ) {
				matchIndexes.push( i );
			}
			return matchIndexes;
		}),

		"lt": createPositionalPseudo(function( matchIndexes, length, argument ) {
			var i = argument < 0 ? argument + length : argument;
			for ( ; --i >= 0; ) {
				matchIndexes.push( i );
			}
			return matchIndexes;
		}),

		"gt": createPositionalPseudo(function( matchIndexes, length, argument ) {
			var i = argument < 0 ? argument + length : argument;
			for ( ; ++i < length; ) {
				matchIndexes.push( i );
			}
			return matchIndexes;
		})
	}
};

Expr.pseudos["nth"] = Expr.pseudos["eq"];

// Add button/input type pseudos
for ( i in { radio: true, checkbox: true, file: true, password: true, image: true } ) {
	Expr.pseudos[ i ] = createInputPseudo( i );
}
for ( i in { submit: true, reset: true } ) {
	Expr.pseudos[ i ] = createButtonPseudo( i );
}

// Easy API for creating new setFilters
function setFilters() {}
setFilters.prototype = Expr.filters = Expr.pseudos;
Expr.setFilters = new setFilters();

tokenize = Sizzle.tokenize = function( selector, parseOnly ) {
	var matched, match, tokens, type,
		soFar, groups, preFilters,
		cached = tokenCache[ selector + " " ];

	if ( cached ) {
		return parseOnly ? 0 : cached.slice( 0 );
	}

	soFar = selector;
	groups = [];
	preFilters = Expr.preFilter;

	while ( soFar ) {

		// Comma and first run
		if ( !matched || (match = rcomma.exec( soFar )) ) {
			if ( match ) {
				// Don't consume trailing commas as valid
				soFar = soFar.slice( match[0].length ) || soFar;
			}
			groups.push( (tokens = []) );
		}

		matched = false;

		// Combinators
		if ( (match = rcombinators.exec( soFar )) ) {
			matched = match.shift();
			tokens.push({
				value: matched,
				// Cast descendant combinators to space
				type: match[0].replace( rtrim, " " )
			});
			soFar = soFar.slice( matched.length );
		}

		// Filters
		for ( type in Expr.filter ) {
			if ( (match = matchExpr[ type ].exec( soFar )) && (!preFilters[ type ] ||
				(match = preFilters[ type ]( match ))) ) {
				matched = match.shift();
				tokens.push({
					value: matched,
					type: type,
					matches: match
				});
				soFar = soFar.slice( matched.length );
			}
		}

		if ( !matched ) {
			break;
		}
	}

	// Return the length of the invalid excess
	// if we're just parsing
	// Otherwise, throw an error or return tokens
	return parseOnly ?
		soFar.length :
		soFar ?
			Sizzle.error( selector ) :
			// Cache the tokens
			tokenCache( selector, groups ).slice( 0 );
};

function toSelector( tokens ) {
	var i = 0,
		len = tokens.length,
		selector = "";
	for ( ; i < len; i++ ) {
		selector += tokens[i].value;
	}
	return selector;
}

function addCombinator( matcher, combinator, base ) {
	var dir = combinator.dir,
		skip = combinator.next,
		key = skip || dir,
		checkNonElements = base && key === "parentNode",
		doneName = done++;

	return combinator.first ?
		// Check against closest ancestor/preceding element
		function( elem, context, xml ) {
			while ( (elem = elem[ dir ]) ) {
				if ( elem.nodeType === 1 || checkNonElements ) {
					return matcher( elem, context, xml );
				}
			}
			return false;
		} :

		// Check against all ancestor/preceding elements
		function( elem, context, xml ) {
			var oldCache, uniqueCache, outerCache,
				newCache = [ dirruns, doneName ];

			// We can't set arbitrary data on XML nodes, so they don't benefit from combinator caching
			if ( xml ) {
				while ( (elem = elem[ dir ]) ) {
					if ( elem.nodeType === 1 || checkNonElements ) {
						if ( matcher( elem, context, xml ) ) {
							return true;
						}
					}
				}
			} else {
				while ( (elem = elem[ dir ]) ) {
					if ( elem.nodeType === 1 || checkNonElements ) {
						outerCache = elem[ expando ] || (elem[ expando ] = {});

						// Support: IE <9 only
						// Defend against cloned attroperties (jQuery gh-1709)
						uniqueCache = outerCache[ elem.uniqueID ] || (outerCache[ elem.uniqueID ] = {});

						if ( skip && skip === elem.nodeName.toLowerCase() ) {
							elem = elem[ dir ] || elem;
						} else if ( (oldCache = uniqueCache[ key ]) &&
							oldCache[ 0 ] === dirruns && oldCache[ 1 ] === doneName ) {

							// Assign to newCache so results back-propagate to previous elements
							return (newCache[ 2 ] = oldCache[ 2 ]);
						} else {
							// Reuse newcache so results back-propagate to previous elements
							uniqueCache[ key ] = newCache;

							// A match means we're done; a fail means we have to keep checking
							if ( (newCache[ 2 ] = matcher( elem, context, xml )) ) {
								return true;
							}
						}
					}
				}
			}
			return false;
		};
}

function elementMatcher( matchers ) {
	return matchers.length > 1 ?
		function( elem, context, xml ) {
			var i = matchers.length;
			while ( i-- ) {
				if ( !matchers[i]( elem, context, xml ) ) {
					return false;
				}
			}
			return true;
		} :
		matchers[0];
}

function multipleContexts( selector, contexts, results ) {
	var i = 0,
		len = contexts.length;
	for ( ; i < len; i++ ) {
		Sizzle( selector, contexts[i], results );
	}
	return results;
}

function condense( unmatched, map, filter, context, xml ) {
	var elem,
		newUnmatched = [],
		i = 0,
		len = unmatched.length,
		mapped = map != null;

	for ( ; i < len; i++ ) {
		if ( (elem = unmatched[i]) ) {
			if ( !filter || filter( elem, context, xml ) ) {
				newUnmatched.push( elem );
				if ( mapped ) {
					map.push( i );
				}
			}
		}
	}

	return newUnmatched;
}

function setMatcher( preFilter, selector, matcher, postFilter, postFinder, postSelector ) {
	if ( postFilter && !postFilter[ expando ] ) {
		postFilter = setMatcher( postFilter );
	}
	if ( postFinder && !postFinder[ expando ] ) {
		postFinder = setMatcher( postFinder, postSelector );
	}
	return markFunction(function( seed, results, context, xml ) {
		var temp, i, elem,
			preMap = [],
			postMap = [],
			preexisting = results.length,

			// Get initial elements from seed or context
			elems = seed || multipleContexts( selector || "*", context.nodeType ? [ context ] : context, [] ),

			// Prefilter to get matcher input, preserving a map for seed-results synchronization
			matcherIn = preFilter && ( seed || !selector ) ?
				condense( elems, preMap, preFilter, context, xml ) :
				elems,

			matcherOut = matcher ?
				// If we have a postFinder, or filtered seed, or non-seed postFilter or preexisting results,
				postFinder || ( seed ? preFilter : preexisting || postFilter ) ?

					// ...intermediate processing is necessary
					[] :

					// ...otherwise use results directly
					results :
				matcherIn;

		// Find primary matches
		if ( matcher ) {
			matcher( matcherIn, matcherOut, context, xml );
		}

		// Apply postFilter
		if ( postFilter ) {
			temp = condense( matcherOut, postMap );
			postFilter( temp, [], context, xml );

			// Un-match failing elements by moving them back to matcherIn
			i = temp.length;
			while ( i-- ) {
				if ( (elem = temp[i]) ) {
					matcherOut[ postMap[i] ] = !(matcherIn[ postMap[i] ] = elem);
				}
			}
		}

		if ( seed ) {
			if ( postFinder || preFilter ) {
				if ( postFinder ) {
					// Get the final matcherOut by condensing this intermediate into postFinder contexts
					temp = [];
					i = matcherOut.length;
					while ( i-- ) {
						if ( (elem = matcherOut[i]) ) {
							// Restore matcherIn since elem is not yet a final match
							temp.push( (matcherIn[i] = elem) );
						}
					}
					postFinder( null, (matcherOut = []), temp, xml );
				}

				// Move matched elements from seed to results to keep them synchronized
				i = matcherOut.length;
				while ( i-- ) {
					if ( (elem = matcherOut[i]) &&
						(temp = postFinder ? indexOf( seed, elem ) : preMap[i]) > -1 ) {

						seed[temp] = !(results[temp] = elem);
					}
				}
			}

		// Add elements to results, through postFinder if defined
		} else {
			matcherOut = condense(
				matcherOut === results ?
					matcherOut.splice( preexisting, matcherOut.length ) :
					matcherOut
			);
			if ( postFinder ) {
				postFinder( null, results, matcherOut, xml );
			} else {
				push.apply( results, matcherOut );
			}
		}
	});
}

function matcherFromTokens( tokens ) {
	var checkContext, matcher, j,
		len = tokens.length,
		leadingRelative = Expr.relative[ tokens[0].type ],
		implicitRelative = leadingRelative || Expr.relative[" "],
		i = leadingRelative ? 1 : 0,

		// The foundational matcher ensures that elements are reachable from top-level context(s)
		matchContext = addCombinator( function( elem ) {
			return elem === checkContext;
		}, implicitRelative, true ),
		matchAnyContext = addCombinator( function( elem ) {
			return indexOf( checkContext, elem ) > -1;
		}, implicitRelative, true ),
		matchers = [ function( elem, context, xml ) {
			var ret = ( !leadingRelative && ( xml || context !== outermostContext ) ) || (
				(checkContext = context).nodeType ?
					matchContext( elem, context, xml ) :
					matchAnyContext( elem, context, xml ) );
			// Avoid hanging onto element (issue #299)
			checkContext = null;
			return ret;
		} ];

	for ( ; i < len; i++ ) {
		if ( (matcher = Expr.relative[ tokens[i].type ]) ) {
			matchers = [ addCombinator(elementMatcher( matchers ), matcher) ];
		} else {
			matcher = Expr.filter[ tokens[i].type ].apply( null, tokens[i].matches );

			// Return special upon seeing a positional matcher
			if ( matcher[ expando ] ) {
				// Find the next relative operator (if any) for proper handling
				j = ++i;
				for ( ; j < len; j++ ) {
					if ( Expr.relative[ tokens[j].type ] ) {
						break;
					}
				}
				return setMatcher(
					i > 1 && elementMatcher( matchers ),
					i > 1 && toSelector(
						// If the preceding token was a descendant combinator, insert an implicit any-element `*`
						tokens.slice( 0, i - 1 ).concat({ value: tokens[ i - 2 ].type === " " ? "*" : "" })
					).replace( rtrim, "$1" ),
					matcher,
					i < j && matcherFromTokens( tokens.slice( i, j ) ),
					j < len && matcherFromTokens( (tokens = tokens.slice( j )) ),
					j < len && toSelector( tokens )
				);
			}
			matchers.push( matcher );
		}
	}

	return elementMatcher( matchers );
}

function matcherFromGroupMatchers( elementMatchers, setMatchers ) {
	var bySet = setMatchers.length > 0,
		byElement = elementMatchers.length > 0,
		superMatcher = function( seed, context, xml, results, outermost ) {
			var elem, j, matcher,
				matchedCount = 0,
				i = "0",
				unmatched = seed && [],
				setMatched = [],
				contextBackup = outermostContext,
				// We must always have either seed elements or outermost context
				elems = seed || byElement && Expr.find["TAG"]( "*", outermost ),
				// Use integer dirruns iff this is the outermost matcher
				dirrunsUnique = (dirruns += contextBackup == null ? 1 : Math.random() || 0.1),
				len = elems.length;

			if ( outermost ) {
				outermostContext = context === document || context || outermost;
			}

			// Add elements passing elementMatchers directly to results
			// Support: IE<9, Safari
			// Tolerate NodeList properties (IE: "length"; Safari: <number>) matching elements by id
			for ( ; i !== len && (elem = elems[i]) != null; i++ ) {
				if ( byElement && elem ) {
					j = 0;
					if ( !context && elem.ownerDocument !== document ) {
						setDocument( elem );
						xml = !documentIsHTML;
					}
					while ( (matcher = elementMatchers[j++]) ) {
						if ( matcher( elem, context || document, xml) ) {
							results.push( elem );
							break;
						}
					}
					if ( outermost ) {
						dirruns = dirrunsUnique;
					}
				}

				// Track unmatched elements for set filters
				if ( bySet ) {
					// They will have gone through all possible matchers
					if ( (elem = !matcher && elem) ) {
						matchedCount--;
					}

					// Lengthen the array for every element, matched or not
					if ( seed ) {
						unmatched.push( elem );
					}
				}
			}

			// `i` is now the count of elements visited above, and adding it to `matchedCount`
			// makes the latter nonnegative.
			matchedCount += i;

			// Apply set filters to unmatched elements
			// NOTE: This can be skipped if there are no unmatched elements (i.e., `matchedCount`
			// equals `i`), unless we didn't visit _any_ elements in the above loop because we have
			// no element matchers and no seed.
			// Incrementing an initially-string "0" `i` allows `i` to remain a string only in that
			// case, which will result in a "00" `matchedCount` that differs from `i` but is also
			// numerically zero.
			if ( bySet && i !== matchedCount ) {
				j = 0;
				while ( (matcher = setMatchers[j++]) ) {
					matcher( unmatched, setMatched, context, xml );
				}

				if ( seed ) {
					// Reintegrate element matches to eliminate the need for sorting
					if ( matchedCount > 0 ) {
						while ( i-- ) {
							if ( !(unmatched[i] || setMatched[i]) ) {
								setMatched[i] = pop.call( results );
							}
						}
					}

					// Discard index placeholder values to get only actual matches
					setMatched = condense( setMatched );
				}

				// Add matches to results
				push.apply( results, setMatched );

				// Seedless set matches succeeding multiple successful matchers stipulate sorting
				if ( outermost && !seed && setMatched.length > 0 &&
					( matchedCount + setMatchers.length ) > 1 ) {

					Sizzle.uniqueSort( results );
				}
			}

			// Override manipulation of globals by nested matchers
			if ( outermost ) {
				dirruns = dirrunsUnique;
				outermostContext = contextBackup;
			}

			return unmatched;
		};

	return bySet ?
		markFunction( superMatcher ) :
		superMatcher;
}

compile = Sizzle.compile = function( selector, match /* Internal Use Only */ ) {
	var i,
		setMatchers = [],
		elementMatchers = [],
		cached = compilerCache[ selector + " " ];

	if ( !cached ) {
		// Generate a function of recursive functions that can be used to check each element
		if ( !match ) {
			match = tokenize( selector );
		}
		i = match.length;
		while ( i-- ) {
			cached = matcherFromTokens( match[i] );
			if ( cached[ expando ] ) {
				setMatchers.push( cached );
			} else {
				elementMatchers.push( cached );
			}
		}

		// Cache the compiled function
		cached = compilerCache( selector, matcherFromGroupMatchers( elementMatchers, setMatchers ) );

		// Save selector and tokenization
		cached.selector = selector;
	}
	return cached;
};

/**
 * A low-level selection function that works with Sizzle's compiled
 *  selector functions
 * @param {String|Function} selector A selector or a pre-compiled
 *  selector function built with Sizzle.compile
 * @param {Element} context
 * @param {Array} [results]
 * @param {Array} [seed] A set of elements to match against
 */
select = Sizzle.select = function( selector, context, results, seed ) {
	var i, tokens, token, type, find,
		compiled = typeof selector === "function" && selector,
		match = !seed && tokenize( (selector = compiled.selector || selector) );

	results = results || [];

	// Try to minimize operations if there is only one selector in the list and no seed
	// (the latter of which guarantees us context)
	if ( match.length === 1 ) {

		// Reduce context if the leading compound selector is an ID
		tokens = match[0] = match[0].slice( 0 );
		if ( tokens.length > 2 && (token = tokens[0]).type === "ID" &&
				context.nodeType === 9 && documentIsHTML && Expr.relative[ tokens[1].type ] ) {

			context = ( Expr.find["ID"]( token.matches[0].replace(runescape, funescape), context ) || [] )[0];
			if ( !context ) {
				return results;

			// Precompiled matchers will still verify ancestry, so step up a level
			} else if ( compiled ) {
				context = context.parentNode;
			}

			selector = selector.slice( tokens.shift().value.length );
		}

		// Fetch a seed set for right-to-left matching
		i = matchExpr["needsContext"].test( selector ) ? 0 : tokens.length;
		while ( i-- ) {
			token = tokens[i];

			// Abort if we hit a combinator
			if ( Expr.relative[ (type = token.type) ] ) {
				break;
			}
			if ( (find = Expr.find[ type ]) ) {
				// Search, expanding context for leading sibling combinators
				if ( (seed = find(
					token.matches[0].replace( runescape, funescape ),
					rsibling.test( tokens[0].type ) && testContext( context.parentNode ) || context
				)) ) {

					// If seed is empty or no tokens remain, we can return early
					tokens.splice( i, 1 );
					selector = seed.length && toSelector( tokens );
					if ( !selector ) {
						push.apply( results, seed );
						return results;
					}

					break;
				}
			}
		}
	}

	// Compile and execute a filtering function if one is not provided
	// Provide `match` to avoid retokenization if we modified the selector above
	( compiled || compile( selector, match ) )(
		seed,
		context,
		!documentIsHTML,
		results,
		!context || rsibling.test( selector ) && testContext( context.parentNode ) || context
	);
	return results;
};

// One-time assignments

// Sort stability
support.sortStable = expando.split("").sort( sortOrder ).join("") === expando;

// Support: Chrome 14-35+
// Always assume duplicates if they aren't passed to the comparison function
support.detectDuplicates = !!hasDuplicate;

// Initialize against the default document
setDocument();

// Support: Webkit<537.32 - Safari 6.0.3/Chrome 25 (fixed in Chrome 27)
// Detached nodes confoundingly follow *each other*
support.sortDetached = assert(function( el ) {
	// Should return 1, but returns 4 (following)
	return el.compareDocumentPosition( document.createElement("fieldset") ) & 1;
});

// Support: IE<8
// Prevent attribute/property "interpolation"
// https://msdn.microsoft.com/en-us/library/ms536429%28VS.85%29.aspx
if ( !assert(function( el ) {
	el.innerHTML = "<a href='#'></a>";
	return el.firstChild.getAttribute("href") === "#" ;
}) ) {
	addHandle( "type|href|height|width", function( elem, name, isXML ) {
		if ( !isXML ) {
			return elem.getAttribute( name, name.toLowerCase() === "type" ? 1 : 2 );
		}
	});
}

// Support: IE<9
// Use defaultValue in place of getAttribute("value")
if ( !support.attributes || !assert(function( el ) {
	el.innerHTML = "<input/>";
	el.firstChild.setAttribute( "value", "" );
	return el.firstChild.getAttribute( "value" ) === "";
}) ) {
	addHandle( "value", function( elem, name, isXML ) {
		if ( !isXML && elem.nodeName.toLowerCase() === "input" ) {
			return elem.defaultValue;
		}
	});
}

// Support: IE<9
// Use getAttributeNode to fetch booleans when getAttribute lies
if ( !assert(function( el ) {
	return el.getAttribute("disabled") == null;
}) ) {
	addHandle( booleans, function( elem, name, isXML ) {
		var val;
		if ( !isXML ) {
			return elem[ name ] === true ? name.toLowerCase() :
					(val = elem.getAttributeNode( name )) && val.specified ?
					val.value :
				null;
		}
	});
}

return Sizzle;

})( window );



jQuery.find = Sizzle;
jQuery.expr = Sizzle.selectors;

// Deprecated
jQuery.expr[ ":" ] = jQuery.expr.pseudos;
jQuery.uniqueSort = jQuery.unique = Sizzle.uniqueSort;
jQuery.text = Sizzle.getText;
jQuery.isXMLDoc = Sizzle.isXML;
jQuery.contains = Sizzle.contains;
jQuery.escapeSelector = Sizzle.escape;




var dir = function( elem, dir, until ) {
	var matched = [],
		truncate = until !== undefined;

	while ( ( elem = elem[ dir ] ) && elem.nodeType !== 9 ) {
		if ( elem.nodeType === 1 ) {
			if ( truncate && jQuery( elem ).is( until ) ) {
				break;
			}
			matched.push( elem );
		}
	}
	return matched;
};


var siblings = function( n, elem ) {
	var matched = [];

	for ( ; n; n = n.nextSibling ) {
		if ( n.nodeType === 1 && n !== elem ) {
			matched.push( n );
		}
	}

	return matched;
};


var rneedsContext = jQuery.expr.match.needsContext;



function nodeName( elem, name ) {

  return elem.nodeName && elem.nodeName.toLowerCase() === name.toLowerCase();

};
var rsingleTag = ( /^<([a-z][^\/\0>:\x20\t\r\n\f]*)[\x20\t\r\n\f]*\/?>(?:<\/\1>|)$/i );



var risSimple = /^.[^:#\[\.,]*$/;

// Implement the identical functionality for filter and not
function winnow( elements, qualifier, not ) {
	if ( jQuery.isFunction( qualifier ) ) {
		return jQuery.grep( elements, function( elem, i ) {
			return !!qualifier.call( elem, i, elem ) !== not;
		} );
	}

	// Single element
	if ( qualifier.nodeType ) {
		return jQuery.grep( elements, function( elem ) {
			return ( elem === qualifier ) !== not;
		} );
	}

	// Arraylike of elements (jQuery, arguments, Array)
	if ( typeof qualifier !== "string" ) {
		return jQuery.grep( elements, function( elem ) {
			return ( indexOf.call( qualifier, elem ) > -1 ) !== not;
		} );
	}

	// Simple selector that can be filtered directly, removing non-Elements
	if ( risSimple.test( qualifier ) ) {
		return jQuery.filter( qualifier, elements, not );
	}

	// Complex selector, compare the two sets, removing non-Elements
	qualifier = jQuery.filter( qualifier, elements );
	return jQuery.grep( elements, function( elem ) {
		return ( indexOf.call( qualifier, elem ) > -1 ) !== not && elem.nodeType === 1;
	} );
}

jQuery.filter = function( expr, elems, not ) {
	var elem = elems[ 0 ];

	if ( not ) {
		expr = ":not(" + expr + ")";
	}

	if ( elems.length === 1 && elem.nodeType === 1 ) {
		return jQuery.find.matchesSelector( elem, expr ) ? [ elem ] : [];
	}

	return jQuery.find.matches( expr, jQuery.grep( elems, function( elem ) {
		return elem.nodeType === 1;
	} ) );
};

jQuery.fn.extend( {
	find: function( selector ) {
		var i, ret,
			len = this.length,
			self = this;

		if ( typeof selector !== "string" ) {
			return this.pushStack( jQuery( selector ).filter( function() {
				for ( i = 0; i < len; i++ ) {
					if ( jQuery.contains( self[ i ], this ) ) {
						return true;
					}
				}
			} ) );
		}

		ret = this.pushStack( [] );

		for ( i = 0; i < len; i++ ) {
			jQuery.find( selector, self[ i ], ret );
		}

		return len > 1 ? jQuery.uniqueSort( ret ) : ret;
	},
	filter: function( selector ) {
		return this.pushStack( winnow( this, selector || [], false ) );
	},
	not: function( selector ) {
		return this.pushStack( winnow( this, selector || [], true ) );
	},
	is: function( selector ) {
		return !!winnow(
			this,

			// If this is a positional/relative selector, check membership in the returned set
			// so $("p:first").is("p:last") won't return true for a doc with two "p".
			typeof selector === "string" && rneedsContext.test( selector ) ?
				jQuery( selector ) :
				selector || [],
			false
		).length;
	}
} );


// Initialize a jQuery object


// A central reference to the root jQuery(document)
var rootjQuery,

	// A simple way to check for HTML strings
	// Prioritize #id over <tag> to avoid XSS via location.hash (#9521)
	// Strict HTML recognition (#11290: must start with <)
	// Shortcut simple #id case for speed
	rquickExpr = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]+))$/,

	init = jQuery.fn.init = function( selector, context, root ) {
		var match, elem;

		// HANDLE: $(""), $(null), $(undefined), $(false)
		if ( !selector ) {
			return this;
		}

		// Method init() accepts an alternate rootjQuery
		// so migrate can support jQuery.sub (gh-2101)
		root = root || rootjQuery;

		// Handle HTML strings
		if ( typeof selector === "string" ) {
			if ( selector[ 0 ] === "<" &&
				selector[ selector.length - 1 ] === ">" &&
				selector.length >= 3 ) {

				// Assume that strings that start and end with <> are HTML and skip the regex check
				match = [ null, selector, null ];

			} else {
				match = rquickExpr.exec( selector );
			}

			// Match html or make sure no context is specified for #id
			if ( match && ( match[ 1 ] || !context ) ) {

				// HANDLE: $(html) -> $(array)
				if ( match[ 1 ] ) {
					context = context instanceof jQuery ? context[ 0 ] : context;

					// Option to run scripts is true for back-compat
					// Intentionally let the error be thrown if parseHTML is not present
					jQuery.merge( this, jQuery.parseHTML(
						match[ 1 ],
						context && context.nodeType ? context.ownerDocument || context : document,
						true
					) );

					// HANDLE: $(html, props)
					if ( rsingleTag.test( match[ 1 ] ) && jQuery.isPlainObject( context ) ) {
						for ( match in context ) {

							// Properties of context are called as methods if possible
							if ( jQuery.isFunction( this[ match ] ) ) {
								this[ match ]( context[ match ] );

							// ...and otherwise set as attributes
							} else {
								this.attr( match, context[ match ] );
							}
						}
					}

					return this;

				// HANDLE: $(#id)
				} else {
					elem = document.getElementById( match[ 2 ] );

					if ( elem ) {

						// Inject the element directly into the jQuery object
						this[ 0 ] = elem;
						this.length = 1;
					}
					return this;
				}

			// HANDLE: $(expr, $(...))
			} else if ( !context || context.jquery ) {
				return ( context || root ).find( selector );

			// HANDLE: $(expr, context)
			// (which is just equivalent to: $(context).find(expr)
			} else {
				return this.constructor( context ).find( selector );
			}

		// HANDLE: $(DOMElement)
		} else if ( selector.nodeType ) {
			this[ 0 ] = selector;
			this.length = 1;
			return this;

		// HANDLE: $(function)
		// Shortcut for document ready
		} else if ( jQuery.isFunction( selector ) ) {
			return root.ready !== undefined ?
				root.ready( selector ) :

				// Execute immediately if ready is not present
				selector( jQuery );
		}

		return jQuery.makeArray( selector, this );
	};

// Give the init function the jQuery prototype for later instantiation
init.prototype = jQuery.fn;

// Initialize central reference
rootjQuery = jQuery( document );


var rparentsprev = /^(?:parents|prev(?:Until|All))/,

	// Methods guaranteed to produce a unique set when starting from a unique set
	guaranteedUnique = {
		children: true,
		contents: true,
		next: true,
		prev: true
	};

jQuery.fn.extend( {
	has: function( target ) {
		var targets = jQuery( target, this ),
			l = targets.length;

		return this.filter( function() {
			var i = 0;
			for ( ; i < l; i++ ) {
				if ( jQuery.contains( this, targets[ i ] ) ) {
					return true;
				}
			}
		} );
	},

	closest: function( selectors, context ) {
		var cur,
			i = 0,
			l = this.length,
			matched = [],
			targets = typeof selectors !== "string" && jQuery( selectors );

		// Positional selectors never match, since there's no _selection_ context
		if ( !rneedsContext.test( selectors ) ) {
			for ( ; i < l; i++ ) {
				for ( cur = this[ i ]; cur && cur !== context; cur = cur.parentNode ) {

					// Always skip document fragments
					if ( cur.nodeType < 11 && ( targets ?
						targets.index( cur ) > -1 :

						// Don't pass non-elements to Sizzle
						cur.nodeType === 1 &&
							jQuery.find.matchesSelector( cur, selectors ) ) ) {

						matched.push( cur );
						break;
					}
				}
			}
		}

		return this.pushStack( matched.length > 1 ? jQuery.uniqueSort( matched ) : matched );
	},

	// Determine the position of an element within the set
	index: function( elem ) {

		// No argument, return index in parent
		if ( !elem ) {
			return ( this[ 0 ] && this[ 0 ].parentNode ) ? this.first().prevAll().length : -1;
		}

		// Index in selector
		if ( typeof elem === "string" ) {
			return indexOf.call( jQuery( elem ), this[ 0 ] );
		}

		// Locate the position of the desired element
		return indexOf.call( this,

			// If it receives a jQuery object, the first element is used
			elem.jquery ? elem[ 0 ] : elem
		);
	},

	add: function( selector, context ) {
		return this.pushStack(
			jQuery.uniqueSort(
				jQuery.merge( this.get(), jQuery( selector, context ) )
			)
		);
	},

	addBack: function( selector ) {
		return this.add( selector == null ?
			this.prevObject : this.prevObject.filter( selector )
		);
	}
} );

function sibling( cur, dir ) {
	while ( ( cur = cur[ dir ] ) && cur.nodeType !== 1 ) {}
	return cur;
}

jQuery.each( {
	parent: function( elem ) {
		var parent = elem.parentNode;
		return parent && parent.nodeType !== 11 ? parent : null;
	},
	parents: function( elem ) {
		return dir( elem, "parentNode" );
	},
	parentsUntil: function( elem, i, until ) {
		return dir( elem, "parentNode", until );
	},
	next: function( elem ) {
		return sibling( elem, "nextSibling" );
	},
	prev: function( elem ) {
		return sibling( elem, "previousSibling" );
	},
	nextAll: function( elem ) {
		return dir( elem, "nextSibling" );
	},
	prevAll: function( elem ) {
		return dir( elem, "previousSibling" );
	},
	nextUntil: function( elem, i, until ) {
		return dir( elem, "nextSibling", until );
	},
	prevUntil: function( elem, i, until ) {
		return dir( elem, "previousSibling", until );
	},
	siblings: function( elem ) {
		return siblings( ( elem.parentNode || {} ).firstChild, elem );
	},
	children: function( elem ) {
		return siblings( elem.firstChild );
	},
	contents: function( elem ) {
        if ( nodeName( elem, "iframe" ) ) {
            return elem.contentDocument;
        }

        // Support: IE 9 - 11 only, iOS 7 only, Android Browser <=4.3 only
        // Treat the template element as a regular one in browsers that
        // don't support it.
        if ( nodeName( elem, "template" ) ) {
            elem = elem.content || elem;
        }

        return jQuery.merge( [], elem.childNodes );
	}
}, function( name, fn ) {
	jQuery.fn[ name ] = function( until, selector ) {
		var matched = jQuery.map( this, fn, until );

		if ( name.slice( -5 ) !== "Until" ) {
			selector = until;
		}

		if ( selector && typeof selector === "string" ) {
			matched = jQuery.filter( selector, matched );
		}

		if ( this.length > 1 ) {

			// Remove duplicates
			if ( !guaranteedUnique[ name ] ) {
				jQuery.uniqueSort( matched );
			}

			// Reverse order for parents* and prev-derivatives
			if ( rparentsprev.test( name ) ) {
				matched.reverse();
			}
		}

		return this.pushStack( matched );
	};
} );
var rnothtmlwhite = ( /[^\x20\t\r\n\f]+/g );



// Convert String-formatted options into Object-formatted ones
function createOptions( options ) {
	var object = {};
	jQuery.each( options.match( rnothtmlwhite ) || [], function( _, flag ) {
		object[ flag ] = true;
	} );
	return object;
}

/*
 * Create a callback list using the following parameters:
 *
 *	options: an optional list of space-separated options that will change how
 *			the callback list behaves or a more traditional option object
 *
 * By default a callback list will act like an event callback list and can be
 * "fired" multiple times.
 *
 * Possible options:
 *
 *	once:			will ensure the callback list can only be fired once (like a Deferred)
 *
 *	memory:			will keep track of previous values and will call any callback added
 *					after the list has been fired right away with the latest "memorized"
 *					values (like a Deferred)
 *
 *	unique:			will ensure a callback can only be added once (no duplicate in the list)
 *
 *	stopOnFalse:	interrupt callings when a callback returns false
 *
 */
jQuery.Callbacks = function( options ) {

	// Convert options from String-formatted to Object-formatted if needed
	// (we check in cache first)
	options = typeof options === "string" ?
		createOptions( options ) :
		jQuery.extend( {}, options );

	var // Flag to know if list is currently firing
		firing,

		// Last fire value for non-forgettable lists
		memory,

		// Flag to know if list was already fired
		fired,

		// Flag to prevent firing
		locked,

		// Actual callback list
		list = [],

		// Queue of execution data for repeatable lists
		queue = [],

		// Index of currently firing callback (modified by add/remove as needed)
		firingIndex = -1,

		// Fire callbacks
		fire = function() {

			// Enforce single-firing
			locked = locked || options.once;

			// Execute callbacks for all pending executions,
			// respecting firingIndex overrides and runtime changes
			fired = firing = true;
			for ( ; queue.length; firingIndex = -1 ) {
				memory = queue.shift();
				while ( ++firingIndex < list.length ) {

					// Run callback and check for early termination
					if ( list[ firingIndex ].apply( memory[ 0 ], memory[ 1 ] ) === false &&
						options.stopOnFalse ) {

						// Jump to end and forget the data so .add doesn't re-fire
						firingIndex = list.length;
						memory = false;
					}
				}
			}

			// Forget the data if we're done with it
			if ( !options.memory ) {
				memory = false;
			}

			firing = false;

			// Clean up if we're done firing for good
			if ( locked ) {

				// Keep an empty list if we have data for future add calls
				if ( memory ) {
					list = [];

				// Otherwise, this object is spent
				} else {
					list = "";
				}
			}
		},

		// Actual Callbacks object
		self = {

			// Add a callback or a collection of callbacks to the list
			add: function() {
				if ( list ) {

					// If we have memory from a past run, we should fire after adding
					if ( memory && !firing ) {
						firingIndex = list.length - 1;
						queue.push( memory );
					}

					( function add( args ) {
						jQuery.each( args, function( _, arg ) {
							if ( jQuery.isFunction( arg ) ) {
								if ( !options.unique || !self.has( arg ) ) {
									list.push( arg );
								}
							} else if ( arg && arg.length && jQuery.type( arg ) !== "string" ) {

								// Inspect recursively
								add( arg );
							}
						} );
					} )( arguments );

					if ( memory && !firing ) {
						fire();
					}
				}
				return this;
			},

			// Remove a callback from the list
			remove: function() {
				jQuery.each( arguments, function( _, arg ) {
					var index;
					while ( ( index = jQuery.inArray( arg, list, index ) ) > -1 ) {
						list.splice( index, 1 );

						// Handle firing indexes
						if ( index <= firingIndex ) {
							firingIndex--;
						}
					}
				} );
				return this;
			},

			// Check if a given callback is in the list.
			// If no argument is given, return whether or not list has callbacks attached.
			has: function( fn ) {
				return fn ?
					jQuery.inArray( fn, list ) > -1 :
					list.length > 0;
			},

			// Remove all callbacks from the list
			empty: function() {
				if ( list ) {
					list = [];
				}
				return this;
			},

			// Disable .fire and .add
			// Abort any current/pending executions
			// Clear all callbacks and values
			disable: function() {
				locked = queue = [];
				list = memory = "";
				return this;
			},
			disabled: function() {
				return !list;
			},

			// Disable .fire
			// Also disable .add unless we have memory (since it would have no effect)
			// Abort any pending executions
			lock: function() {
				locked = queue = [];
				if ( !memory && !firing ) {
					list = memory = "";
				}
				return this;
			},
			locked: function() {
				return !!locked;
			},

			// Call all callbacks with the given context and arguments
			fireWith: function( context, args ) {
				if ( !locked ) {
					args = args || [];
					args = [ context, args.slice ? args.slice() : args ];
					queue.push( args );
					if ( !firing ) {
						fire();
					}
				}
				return this;
			},

			// Call all the callbacks with the given arguments
			fire: function() {
				self.fireWith( this, arguments );
				return this;
			},

			// To know if the callbacks have already been called at least once
			fired: function() {
				return !!fired;
			}
		};

	return self;
};


function Identity( v ) {
	return v;
}
function Thrower( ex ) {
	throw ex;
}

function adoptValue( value, resolve, reject, noValue ) {
	var method;

	try {

		// Check for promise aspect first to privilege synchronous behavior
		if ( value && jQuery.isFunction( ( method = value.promise ) ) ) {
			method.call( value ).done( resolve ).fail( reject );

		// Other thenables
		} else if ( value && jQuery.isFunction( ( method = value.then ) ) ) {
			method.call( value, resolve, reject );

		// Other non-thenables
		} else {

			// Control `resolve` arguments by letting Array#slice cast boolean `noValue` to integer:
			// * false: [ value ].slice( 0 ) => resolve( value )
			// * true: [ value ].slice( 1 ) => resolve()
			resolve.apply( undefined, [ value ].slice( noValue ) );
		}

	// For Promises/A+, convert exceptions into rejections
	// Since jQuery.when doesn't unwrap thenables, we can skip the extra checks appearing in
	// Deferred#then to conditionally suppress rejection.
	} catch ( value ) {

		// Support: Android 4.0 only
		// Strict mode functions invoked without .call/.apply get global-object context
		reject.apply( undefined, [ value ] );
	}
}

jQuery.extend( {

	Deferred: function( func ) {
		var tuples = [

				// action, add listener, callbacks,
				// ... .then handlers, argument index, [final state]
				[ "notify", "progress", jQuery.Callbacks( "memory" ),
					jQuery.Callbacks( "memory" ), 2 ],
				[ "resolve", "done", jQuery.Callbacks( "once memory" ),
					jQuery.Callbacks( "once memory" ), 0, "resolved" ],
				[ "reject", "fail", jQuery.Callbacks( "once memory" ),
					jQuery.Callbacks( "once memory" ), 1, "rejected" ]
			],
			state = "pending",
			promise = {
				state: function() {
					return state;
				},
				always: function() {
					deferred.done( arguments ).fail( arguments );
					return this;
				},
				"catch": function( fn ) {
					return promise.then( null, fn );
				},

				// Keep pipe for back-compat
				pipe: function( /* fnDone, fnFail, fnProgress */ ) {
					var fns = arguments;

					return jQuery.Deferred( function( newDefer ) {
						jQuery.each( tuples, function( i, tuple ) {

							// Map tuples (progress, done, fail) to arguments (done, fail, progress)
							var fn = jQuery.isFunction( fns[ tuple[ 4 ] ] ) && fns[ tuple[ 4 ] ];

							// deferred.progress(function() { bind to newDefer or newDefer.notify })
							// deferred.done(function() { bind to newDefer or newDefer.resolve })
							// deferred.fail(function() { bind to newDefer or newDefer.reject })
							deferred[ tuple[ 1 ] ]( function() {
								var returned = fn && fn.apply( this, arguments );
								if ( returned && jQuery.isFunction( returned.promise ) ) {
									returned.promise()
										.progress( newDefer.notify )
										.done( newDefer.resolve )
										.fail( newDefer.reject );
								} else {
									newDefer[ tuple[ 0 ] + "With" ](
										this,
										fn ? [ returned ] : arguments
									);
								}
							} );
						} );
						fns = null;
					} ).promise();
				},
				then: function( onFulfilled, onRejected, onProgress ) {
					var maxDepth = 0;
					function resolve( depth, deferred, handler, special ) {
						return function() {
							var that = this,
								args = arguments,
								mightThrow = function() {
									var returned, then;

									// Support: Promises/A+ section 2.3.3.3.3
									// https://promisesaplus.com/#point-59
									// Ignore double-resolution attempts
									if ( depth < maxDepth ) {
										return;
									}

									returned = handler.apply( that, args );

									// Support: Promises/A+ section 2.3.1
									// https://promisesaplus.com/#point-48
									if ( returned === deferred.promise() ) {
										throw new TypeError( "Thenable self-resolution" );
									}

									// Support: Promises/A+ sections 2.3.3.1, 3.5
									// https://promisesaplus.com/#point-54
									// https://promisesaplus.com/#point-75
									// Retrieve `then` only once
									then = returned &&

										// Support: Promises/A+ section 2.3.4
										// https://promisesaplus.com/#point-64
										// Only check objects and functions for thenability
										( typeof returned === "object" ||
											typeof returned === "function" ) &&
										returned.then;

									// Handle a returned thenable
									if ( jQuery.isFunction( then ) ) {

										// Special processors (notify) just wait for resolution
										if ( special ) {
											then.call(
												returned,
												resolve( maxDepth, deferred, Identity, special ),
												resolve( maxDepth, deferred, Thrower, special )
											);

										// Normal processors (resolve) also hook into progress
										} else {

											// ...and disregard older resolution values
											maxDepth++;

											then.call(
												returned,
												resolve( maxDepth, deferred, Identity, special ),
												resolve( maxDepth, deferred, Thrower, special ),
												resolve( maxDepth, deferred, Identity,
													deferred.notifyWith )
											);
										}

									// Handle all other returned values
									} else {

										// Only substitute handlers pass on context
										// and multiple values (non-spec behavior)
										if ( handler !== Identity ) {
											that = undefined;
											args = [ returned ];
										}

										// Process the value(s)
										// Default process is resolve
										( special || deferred.resolveWith )( that, args );
									}
								},

								// Only normal processors (resolve) catch and reject exceptions
								process = special ?
									mightThrow :
									function() {
										try {
											mightThrow();
										} catch ( e ) {

											if ( jQuery.Deferred.exceptionHook ) {
												jQuery.Deferred.exceptionHook( e,
													process.stackTrace );
											}

											// Support: Promises/A+ section 2.3.3.3.4.1
											// https://promisesaplus.com/#point-61
											// Ignore post-resolution exceptions
											if ( depth + 1 >= maxDepth ) {

												// Only substitute handlers pass on context
												// and multiple values (non-spec behavior)
												if ( handler !== Thrower ) {
													that = undefined;
													args = [ e ];
												}

												deferred.rejectWith( that, args );
											}
										}
									};

							// Support: Promises/A+ section 2.3.3.3.1
							// https://promisesaplus.com/#point-57
							// Re-resolve promises immediately to dodge false rejection from
							// subsequent errors
							if ( depth ) {
								process();
							} else {

								// Call an optional hook to record the stack, in case of exception
								// since it's otherwise lost when execution goes async
								if ( jQuery.Deferred.getStackHook ) {
									process.stackTrace = jQuery.Deferred.getStackHook();
								}
								window.setTimeout( process );
							}
						};
					}

					return jQuery.Deferred( function( newDefer ) {

						// progress_handlers.add( ... )
						tuples[ 0 ][ 3 ].add(
							resolve(
								0,
								newDefer,
								jQuery.isFunction( onProgress ) ?
									onProgress :
									Identity,
								newDefer.notifyWith
							)
						);

						// fulfilled_handlers.add( ... )
						tuples[ 1 ][ 3 ].add(
							resolve(
								0,
								newDefer,
								jQuery.isFunction( onFulfilled ) ?
									onFulfilled :
									Identity
							)
						);

						// rejected_handlers.add( ... )
						tuples[ 2 ][ 3 ].add(
							resolve(
								0,
								newDefer,
								jQuery.isFunction( onRejected ) ?
									onRejected :
									Thrower
							)
						);
					} ).promise();
				},

				// Get a promise for this deferred
				// If obj is provided, the promise aspect is added to the object
				promise: function( obj ) {
					return obj != null ? jQuery.extend( obj, promise ) : promise;
				}
			},
			deferred = {};

		// Add list-specific methods
		jQuery.each( tuples, function( i, tuple ) {
			var list = tuple[ 2 ],
				stateString = tuple[ 5 ];

			// promise.progress = list.add
			// promise.done = list.add
			// promise.fail = list.add
			promise[ tuple[ 1 ] ] = list.add;

			// Handle state
			if ( stateString ) {
				list.add(
					function() {

						// state = "resolved" (i.e., fulfilled)
						// state = "rejected"
						state = stateString;
					},

					// rejected_callbacks.disable
					// fulfilled_callbacks.disable
					tuples[ 3 - i ][ 2 ].disable,

					// progress_callbacks.lock
					tuples[ 0 ][ 2 ].lock
				);
			}

			// progress_handlers.fire
			// fulfilled_handlers.fire
			// rejected_handlers.fire
			list.add( tuple[ 3 ].fire );

			// deferred.notify = function() { deferred.notifyWith(...) }
			// deferred.resolve = function() { deferred.resolveWith(...) }
			// deferred.reject = function() { deferred.rejectWith(...) }
			deferred[ tuple[ 0 ] ] = function() {
				deferred[ tuple[ 0 ] + "With" ]( this === deferred ? undefined : this, arguments );
				return this;
			};

			// deferred.notifyWith = list.fireWith
			// deferred.resolveWith = list.fireWith
			// deferred.rejectWith = list.fireWith
			deferred[ tuple[ 0 ] + "With" ] = list.fireWith;
		} );

		// Make the deferred a promise
		promise.promise( deferred );

		// Call given func if any
		if ( func ) {
			func.call( deferred, deferred );
		}

		// All done!
		return deferred;
	},

	// Deferred helper
	when: function( singleValue ) {
		var

			// count of uncompleted subordinates
			remaining = arguments.length,

			// count of unprocessed arguments
			i = remaining,

			// subordinate fulfillment data
			resolveContexts = Array( i ),
			resolveValues = slice.call( arguments ),

			// the master Deferred
			master = jQuery.Deferred(),

			// subordinate callback factory
			updateFunc = function( i ) {
				return function( value ) {
					resolveContexts[ i ] = this;
					resolveValues[ i ] = arguments.length > 1 ? slice.call( arguments ) : value;
					if ( !( --remaining ) ) {
						master.resolveWith( resolveContexts, resolveValues );
					}
				};
			};

		// Single- and empty arguments are adopted like Promise.resolve
		if ( remaining <= 1 ) {
			adoptValue( singleValue, master.done( updateFunc( i ) ).resolve, master.reject,
				!remaining );

			// Use .then() to unwrap secondary thenables (cf. gh-3000)
			if ( master.state() === "pending" ||
				jQuery.isFunction( resolveValues[ i ] && resolveValues[ i ].then ) ) {

				return master.then();
			}
		}

		// Multiple arguments are aggregated like Promise.all array elements
		while ( i-- ) {
			adoptValue( resolveValues[ i ], updateFunc( i ), master.reject );
		}

		return master.promise();
	}
} );


// These usually indicate a programmer mistake during development,
// warn about them ASAP rather than swallowing them by default.
var rerrorNames = /^(Eval|Internal|Range|Reference|Syntax|Type|URI)Error$/;

jQuery.Deferred.exceptionHook = function( error, stack ) {

	// Support: IE 8 - 9 only
	// Console exists when dev tools are open, which can happen at any time
	if ( window.console && window.console.warn && error && rerrorNames.test( error.name ) ) {
		window.console.warn( "jQuery.Deferred exception: " + error.message, error.stack, stack );
	}
};




jQuery.readyException = function( error ) {
	window.setTimeout( function() {
		throw error;
	} );
};




// The deferred used on DOM ready
var readyList = jQuery.Deferred();

jQuery.fn.ready = function( fn ) {

	readyList
		.then( fn )

		// Wrap jQuery.readyException in a function so that the lookup
		// happens at the time of error handling instead of callback
		// registration.
		.catch( function( error ) {
			jQuery.readyException( error );
		} );

	return this;
};

jQuery.extend( {

	// Is the DOM ready to be used? Set to true once it occurs.
	isReady: false,

	// A counter to track how many items to wait for before
	// the ready event fires. See #6781
	readyWait: 1,

	// Handle when the DOM is ready
	ready: function( wait ) {

		// Abort if there are pending holds or we're already ready
		if ( wait === true ? --jQuery.readyWait : jQuery.isReady ) {
			return;
		}

		// Remember that the DOM is ready
		jQuery.isReady = true;

		// If a normal DOM Ready event fired, decrement, and wait if need be
		if ( wait !== true && --jQuery.readyWait > 0 ) {
			return;
		}

		// If there are functions bound, to execute
		readyList.resolveWith( document, [ jQuery ] );
	}
} );

jQuery.ready.then = readyList.then;

// The ready event handler and self cleanup method
function completed() {
	document.removeEventListener( "DOMContentLoaded", completed );
	window.removeEventListener( "load", completed );
	jQuery.ready();
}

// Catch cases where $(document).ready() is called
// after the browser event has already occurred.
// Support: IE <=9 - 10 only
// Older IE sometimes signals "interactive" too soon
if ( document.readyState === "complete" ||
	( document.readyState !== "loading" && !document.documentElement.doScroll ) ) {

	// Handle it asynchronously to allow scripts the opportunity to delay ready
	window.setTimeout( jQuery.ready );

} else {

	// Use the handy event callback
	document.addEventListener( "DOMContentLoaded", completed );

	// A fallback to window.onload, that will always work
	window.addEventListener( "load", completed );
}




// Multifunctional method to get and set values of a collection
// The value/s can optionally be executed if it's a function
var access = function( elems, fn, key, value, chainable, emptyGet, raw ) {
	var i = 0,
		len = elems.length,
		bulk = key == null;

	// Sets many values
	if ( jQuery.type( key ) === "object" ) {
		chainable = true;
		for ( i in key ) {
			access( elems, fn, i, key[ i ], true, emptyGet, raw );
		}

	// Sets one value
	} else if ( value !== undefined ) {
		chainable = true;

		if ( !jQuery.isFunction( value ) ) {
			raw = true;
		}

		if ( bulk ) {

			// Bulk operations run against the entire set
			if ( raw ) {
				fn.call( elems, value );
				fn = null;

			// ...except when executing function values
			} else {
				bulk = fn;
				fn = function( elem, key, value ) {
					return bulk.call( jQuery( elem ), value );
				};
			}
		}

		if ( fn ) {
			for ( ; i < len; i++ ) {
				fn(
					elems[ i ], key, raw ?
					value :
					value.call( elems[ i ], i, fn( elems[ i ], key ) )
				);
			}
		}
	}

	if ( chainable ) {
		return elems;
	}

	// Gets
	if ( bulk ) {
		return fn.call( elems );
	}

	return len ? fn( elems[ 0 ], key ) : emptyGet;
};
var acceptData = function( owner ) {

	// Accepts only:
	//  - Node
	//    - Node.ELEMENT_NODE
	//    - Node.DOCUMENT_NODE
	//  - Object
	//    - Any
	return owner.nodeType === 1 || owner.nodeType === 9 || !( +owner.nodeType );
};




function Data() {
	this.expando = jQuery.expando + Data.uid++;
}

Data.uid = 1;

Data.prototype = {

	cache: function( owner ) {

		// Check if the owner object already has a cache
		var value = owner[ this.expando ];

		// If not, create one
		if ( !value ) {
			value = {};

			// We can accept data for non-element nodes in modern browsers,
			// but we should not, see #8335.
			// Always return an empty object.
			if ( acceptData( owner ) ) {

				// If it is a node unlikely to be stringify-ed or looped over
				// use plain assignment
				if ( owner.nodeType ) {
					owner[ this.expando ] = value;

				// Otherwise secure it in a non-enumerable property
				// configurable must be true to allow the property to be
				// deleted when data is removed
				} else {
					Object.defineProperty( owner, this.expando, {
						value: value,
						configurable: true
					} );
				}
			}
		}

		return value;
	},
	set: function( owner, data, value ) {
		var prop,
			cache = this.cache( owner );

		// Handle: [ owner, key, value ] args
		// Always use camelCase key (gh-2257)
		if ( typeof data === "string" ) {
			cache[ jQuery.camelCase( data ) ] = value;

		// Handle: [ owner, { properties } ] args
		} else {

			// Copy the properties one-by-one to the cache object
			for ( prop in data ) {
				cache[ jQuery.camelCase( prop ) ] = data[ prop ];
			}
		}
		return cache;
	},
	get: function( owner, key ) {
		return key === undefined ?
			this.cache( owner ) :

			// Always use camelCase key (gh-2257)
			owner[ this.expando ] && owner[ this.expando ][ jQuery.camelCase( key ) ];
	},
	access: function( owner, key, value ) {

		// In cases where either:
		//
		//   1. No key was specified
		//   2. A string key was specified, but no value provided
		//
		// Take the "read" path and allow the get method to determine
		// which value to return, respectively either:
		//
		//   1. The entire cache object
		//   2. The data stored at the key
		//
		if ( key === undefined ||
				( ( key && typeof key === "string" ) && value === undefined ) ) {

			return this.get( owner, key );
		}

		// When the key is not a string, or both a key and value
		// are specified, set or extend (existing objects) with either:
		//
		//   1. An object of properties
		//   2. A key and value
		//
		this.set( owner, key, value );

		// Since the "set" path can have two possible entry points
		// return the expected data based on which path was taken[*]
		return value !== undefined ? value : key;
	},
	remove: function( owner, key ) {
		var i,
			cache = owner[ this.expando ];

		if ( cache === undefined ) {
			return;
		}

		if ( key !== undefined ) {

			// Support array or space separated string of keys
			if ( Array.isArray( key ) ) {

				// If key is an array of keys...
				// We always set camelCase keys, so remove that.
				key = key.map( jQuery.camelCase );
			} else {
				key = jQuery.camelCase( key );

				// If a key with the spaces exists, use it.
				// Otherwise, create an array by matching non-whitespace
				key = key in cache ?
					[ key ] :
					( key.match( rnothtmlwhite ) || [] );
			}

			i = key.length;

			while ( i-- ) {
				delete cache[ key[ i ] ];
			}
		}

		// Remove the expando if there's no more data
		if ( key === undefined || jQuery.isEmptyObject( cache ) ) {

			// Support: Chrome <=35 - 45
			// Webkit & Blink performance suffers when deleting properties
			// from DOM nodes, so set to undefined instead
			// https://bugs.chromium.org/p/chromium/issues/detail?id=378607 (bug restricted)
			if ( owner.nodeType ) {
				owner[ this.expando ] = undefined;
			} else {
				delete owner[ this.expando ];
			}
		}
	},
	hasData: function( owner ) {
		var cache = owner[ this.expando ];
		return cache !== undefined && !jQuery.isEmptyObject( cache );
	}
};
var dataPriv = new Data();

var dataUser = new Data();



//	Implementation Summary
//
//	1. Enforce API surface and semantic compatibility with 1.9.x branch
//	2. Improve the module's maintainability by reducing the storage
//		paths to a single mechanism.
//	3. Use the same single mechanism to support "private" and "user" data.
//	4. _Never_ expose "private" data to user code (TODO: Drop _data, _removeData)
//	5. Avoid exposing implementation details on user objects (eg. expando properties)
//	6. Provide a clear path for implementation upgrade to WeakMap in 2014

var rbrace = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,
	rmultiDash = /[A-Z]/g;

function getData( data ) {
	if ( data === "true" ) {
		return true;
	}

	if ( data === "false" ) {
		return false;
	}

	if ( data === "null" ) {
		return null;
	}

	// Only convert to a number if it doesn't change the string
	if ( data === +data + "" ) {
		return +data;
	}

	if ( rbrace.test( data ) ) {
		return JSON.parse( data );
	}

	return data;
}

function dataAttr( elem, key, data ) {
	var name;

	// If nothing was found internally, try to fetch any
	// data from the HTML5 data-* attribute
	if ( data === undefined && elem.nodeType === 1 ) {
		name = "data-" + key.replace( rmultiDash, "-$&" ).toLowerCase();
		data = elem.getAttribute( name );

		if ( typeof data === "string" ) {
			try {
				data = getData( data );
			} catch ( e ) {}

			// Make sure we set the data so it isn't changed later
			dataUser.set( elem, key, data );
		} else {
			data = undefined;
		}
	}
	return data;
}

jQuery.extend( {
	hasData: function( elem ) {
		return dataUser.hasData( elem ) || dataPriv.hasData( elem );
	},

	data: function( elem, name, data ) {
		return dataUser.access( elem, name, data );
	},

	removeData: function( elem, name ) {
		dataUser.remove( elem, name );
	},

	// TODO: Now that all calls to _data and _removeData have been replaced
	// with direct calls to dataPriv methods, these can be deprecated.
	_data: function( elem, name, data ) {
		return dataPriv.access( elem, name, data );
	},

	_removeData: function( elem, name ) {
		dataPriv.remove( elem, name );
	}
} );

jQuery.fn.extend( {
	data: function( key, value ) {
		var i, name, data,
			elem = this[ 0 ],
			attrs = elem && elem.attributes;

		// Gets all values
		if ( key === undefined ) {
			if ( this.length ) {
				data = dataUser.get( elem );

				if ( elem.nodeType === 1 && !dataPriv.get( elem, "hasDataAttrs" ) ) {
					i = attrs.length;
					while ( i-- ) {

						// Support: IE 11 only
						// The attrs elements can be null (#14894)
						if ( attrs[ i ] ) {
							name = attrs[ i ].name;
							if ( name.indexOf( "data-" ) === 0 ) {
								name = jQuery.camelCase( name.slice( 5 ) );
								dataAttr( elem, name, data[ name ] );
							}
						}
					}
					dataPriv.set( elem, "hasDataAttrs", true );
				}
			}

			return data;
		}

		// Sets multiple values
		if ( typeof key === "object" ) {
			return this.each( function() {
				dataUser.set( this, key );
			} );
		}

		return access( this, function( value ) {
			var data;

			// The calling jQuery object (element matches) is not empty
			// (and therefore has an element appears at this[ 0 ]) and the
			// `value` parameter was not undefined. An empty jQuery object
			// will result in `undefined` for elem = this[ 0 ] which will
			// throw an exception if an attempt to read a data cache is made.
			if ( elem && value === undefined ) {

				// Attempt to get data from the cache
				// The key will always be camelCased in Data
				data = dataUser.get( elem, key );
				if ( data !== undefined ) {
					return data;
				}

				// Attempt to "discover" the data in
				// HTML5 custom data-* attrs
				data = dataAttr( elem, key );
				if ( data !== undefined ) {
					return data;
				}

				// We tried really hard, but the data doesn't exist.
				return;
			}

			// Set the data...
			this.each( function() {

				// We always store the camelCased key
				dataUser.set( this, key, value );
			} );
		}, null, value, arguments.length > 1, null, true );
	},

	removeData: function( key ) {
		return this.each( function() {
			dataUser.remove( this, key );
		} );
	}
} );


jQuery.extend( {
	queue: function( elem, type, data ) {
		var queue;

		if ( elem ) {
			type = ( type || "fx" ) + "queue";
			queue = dataPriv.get( elem, type );

			// Speed up dequeue by getting out quickly if this is just a lookup
			if ( data ) {
				if ( !queue || Array.isArray( data ) ) {
					queue = dataPriv.access( elem, type, jQuery.makeArray( data ) );
				} else {
					queue.push( data );
				}
			}
			return queue || [];
		}
	},

	dequeue: function( elem, type ) {
		type = type || "fx";

		var queue = jQuery.queue( elem, type ),
			startLength = queue.length,
			fn = queue.shift(),
			hooks = jQuery._queueHooks( elem, type ),
			next = function() {
				jQuery.dequeue( elem, type );
			};

		// If the fx queue is dequeued, always remove the progress sentinel
		if ( fn === "inprogress" ) {
			fn = queue.shift();
			startLength--;
		}

		if ( fn ) {

			// Add a progress sentinel to prevent the fx queue from being
			// automatically dequeued
			if ( type === "fx" ) {
				queue.unshift( "inprogress" );
			}

			// Clear up the last queue stop function
			delete hooks.stop;
			fn.call( elem, next, hooks );
		}

		if ( !startLength && hooks ) {
			hooks.empty.fire();
		}
	},

	// Not public - generate a queueHooks object, or return the current one
	_queueHooks: function( elem, type ) {
		var key = type + "queueHooks";
		return dataPriv.get( elem, key ) || dataPriv.access( elem, key, {
			empty: jQuery.Callbacks( "once memory" ).add( function() {
				dataPriv.remove( elem, [ type + "queue", key ] );
			} )
		} );
	}
} );

jQuery.fn.extend( {
	queue: function( type, data ) {
		var setter = 2;

		if ( typeof type !== "string" ) {
			data = type;
			type = "fx";
			setter--;
		}

		if ( arguments.length < setter ) {
			return jQuery.queue( this[ 0 ], type );
		}

		return data === undefined ?
			this :
			this.each( function() {
				var queue = jQuery.queue( this, type, data );

				// Ensure a hooks for this queue
				jQuery._queueHooks( this, type );

				if ( type === "fx" && queue[ 0 ] !== "inprogress" ) {
					jQuery.dequeue( this, type );
				}
			} );
	},
	dequeue: function( type ) {
		return this.each( function() {
			jQuery.dequeue( this, type );
		} );
	},
	clearQueue: function( type ) {
		return this.queue( type || "fx", [] );
	},

	// Get a promise resolved when queues of a certain type
	// are emptied (fx is the type by default)
	promise: function( type, obj ) {
		var tmp,
			count = 1,
			defer = jQuery.Deferred(),
			elements = this,
			i = this.length,
			resolve = function() {
				if ( !( --count ) ) {
					defer.resolveWith( elements, [ elements ] );
				}
			};

		if ( typeof type !== "string" ) {
			obj = type;
			type = undefined;
		}
		type = type || "fx";

		while ( i-- ) {
			tmp = dataPriv.get( elements[ i ], type + "queueHooks" );
			if ( tmp && tmp.empty ) {
				count++;
				tmp.empty.add( resolve );
			}
		}
		resolve();
		return defer.promise( obj );
	}
} );
var pnum = ( /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/ ).source;

var rcssNum = new RegExp( "^(?:([+-])=|)(" + pnum + ")([a-z%]*)$", "i" );


var cssExpand = [ "Top", "Right", "Bottom", "Left" ];

var isHiddenWithinTree = function( elem, el ) {

		// isHiddenWithinTree might be called from jQuery#filter function;
		// in that case, element will be second argument
		elem = el || elem;

		// Inline style trumps all
		return elem.style.display === "none" ||
			elem.style.display === "" &&

			// Otherwise, check computed style
			// Support: Firefox <=43 - 45
			// Disconnected elements can have computed display: none, so first confirm that elem is
			// in the document.
			jQuery.contains( elem.ownerDocument, elem ) &&

			jQuery.css( elem, "display" ) === "none";
	};

var swap = function( elem, options, callback, args ) {
	var ret, name,
		old = {};

	// Remember the old values, and insert the new ones
	for ( name in options ) {
		old[ name ] = elem.style[ name ];
		elem.style[ name ] = options[ name ];
	}

	ret = callback.apply( elem, args || [] );

	// Revert the old values
	for ( name in options ) {
		elem.style[ name ] = old[ name ];
	}

	return ret;
};




function adjustCSS( elem, prop, valueParts, tween ) {
	var adjusted,
		scale = 1,
		maxIterations = 20,
		currentValue = tween ?
			function() {
				return tween.cur();
			} :
			function() {
				return jQuery.css( elem, prop, "" );
			},
		initial = currentValue(),
		unit = valueParts && valueParts[ 3 ] || ( jQuery.cssNumber[ prop ] ? "" : "px" ),

		// Starting value computation is required for potential unit mismatches
		initialInUnit = ( jQuery.cssNumber[ prop ] || unit !== "px" && +initial ) &&
			rcssNum.exec( jQuery.css( elem, prop ) );

	if ( initialInUnit && initialInUnit[ 3 ] !== unit ) {

		// Trust units reported by jQuery.css
		unit = unit || initialInUnit[ 3 ];

		// Make sure we update the tween properties later on
		valueParts = valueParts || [];

		// Iteratively approximate from a nonzero starting point
		initialInUnit = +initial || 1;

		do {

			// If previous iteration zeroed out, double until we get *something*.
			// Use string for doubling so we don't accidentally see scale as unchanged below
			scale = scale || ".5";

			// Adjust and apply
			initialInUnit = initialInUnit / scale;
			jQuery.style( elem, prop, initialInUnit + unit );

		// Update scale, tolerating zero or NaN from tween.cur()
		// Break the loop if scale is unchanged or perfect, or if we've just had enough.
		} while (
			scale !== ( scale = currentValue() / initial ) && scale !== 1 && --maxIterations
		);
	}

	if ( valueParts ) {
		initialInUnit = +initialInUnit || +initial || 0;

		// Apply relative offset (+=/-=) if specified
		adjusted = valueParts[ 1 ] ?
			initialInUnit + ( valueParts[ 1 ] + 1 ) * valueParts[ 2 ] :
			+valueParts[ 2 ];
		if ( tween ) {
			tween.unit = unit;
			tween.start = initialInUnit;
			tween.end = adjusted;
		}
	}
	return adjusted;
}


var defaultDisplayMap = {};

function getDefaultDisplay( elem ) {
	var temp,
		doc = elem.ownerDocument,
		nodeName = elem.nodeName,
		display = defaultDisplayMap[ nodeName ];

	if ( display ) {
		return display;
	}

	temp = doc.body.appendChild( doc.createElement( nodeName ) );
	display = jQuery.css( temp, "display" );

	temp.parentNode.removeChild( temp );

	if ( display === "none" ) {
		display = "block";
	}
	defaultDisplayMap[ nodeName ] = display;

	return display;
}

function showHide( elements, show ) {
	var display, elem,
		values = [],
		index = 0,
		length = elements.length;

	// Determine new display value for elements that need to change
	for ( ; index < length; index++ ) {
		elem = elements[ index ];
		if ( !elem.style ) {
			continue;
		}

		display = elem.style.display;
		if ( show ) {

			// Since we force visibility upon cascade-hidden elements, an immediate (and slow)
			// check is required in this first loop unless we have a nonempty display value (either
			// inline or about-to-be-restored)
			if ( display === "none" ) {
				values[ index ] = dataPriv.get( elem, "display" ) || null;
				if ( !values[ index ] ) {
					elem.style.display = "";
				}
			}
			if ( elem.style.display === "" && isHiddenWithinTree( elem ) ) {
				values[ index ] = getDefaultDisplay( elem );
			}
		} else {
			if ( display !== "none" ) {
				values[ index ] = "none";

				// Remember what we're overwriting
				dataPriv.set( elem, "display", display );
			}
		}
	}

	// Set the display of the elements in a second loop to avoid constant reflow
	for ( index = 0; index < length; index++ ) {
		if ( values[ index ] != null ) {
			elements[ index ].style.display = values[ index ];
		}
	}

	return elements;
}

jQuery.fn.extend( {
	show: function() {
		return showHide( this, true );
	},
	hide: function() {
		return showHide( this );
	},
	toggle: function( state ) {
		if ( typeof state === "boolean" ) {
			return state ? this.show() : this.hide();
		}

		return this.each( function() {
			if ( isHiddenWithinTree( this ) ) {
				jQuery( this ).show();
			} else {
				jQuery( this ).hide();
			}
		} );
	}
} );
var rcheckableType = ( /^(?:checkbox|radio)$/i );

var rtagName = ( /<([a-z][^\/\0>\x20\t\r\n\f]+)/i );

var rscriptType = ( /^$|\/(?:java|ecma)script/i );



// We have to close these tags to support XHTML (#13200)
var wrapMap = {

	// Support: IE <=9 only
	option: [ 1, "<select multiple='multiple'>", "</select>" ],

	// XHTML parsers do not magically insert elements in the
	// same way that tag soup parsers do. So we cannot shorten
	// this by omitting <tbody> or other required elements.
	thead: [ 1, "<table>", "</table>" ],
	col: [ 2, "<table><colgroup>", "</colgroup></table>" ],
	tr: [ 2, "<table><tbody>", "</tbody></table>" ],
	td: [ 3, "<table><tbody><tr>", "</tr></tbody></table>" ],

	_default: [ 0, "", "" ]
};

// Support: IE <=9 only
wrapMap.optgroup = wrapMap.option;

wrapMap.tbody = wrapMap.tfoot = wrapMap.colgroup = wrapMap.caption = wrapMap.thead;
wrapMap.th = wrapMap.td;


function getAll( context, tag ) {

	// Support: IE <=9 - 11 only
	// Use typeof to avoid zero-argument method invocation on host objects (#15151)
	var ret;

	if ( typeof context.getElementsByTagName !== "undefined" ) {
		ret = context.getElementsByTagName( tag || "*" );

	} else if ( typeof context.querySelectorAll !== "undefined" ) {
		ret = context.querySelectorAll( tag || "*" );

	} else {
		ret = [];
	}

	if ( tag === undefined || tag && nodeName( context, tag ) ) {
		return jQuery.merge( [ context ], ret );
	}

	return ret;
}


// Mark scripts as having already been evaluated
function setGlobalEval( elems, refElements ) {
	var i = 0,
		l = elems.length;

	for ( ; i < l; i++ ) {
		dataPriv.set(
			elems[ i ],
			"globalEval",
			!refElements || dataPriv.get( refElements[ i ], "globalEval" )
		);
	}
}


var rhtml = /<|&#?\w+;/;

function buildFragment( elems, context, scripts, selection, ignored ) {
	var elem, tmp, tag, wrap, contains, j,
		fragment = context.createDocumentFragment(),
		nodes = [],
		i = 0,
		l = elems.length;

	for ( ; i < l; i++ ) {
		elem = elems[ i ];

		if ( elem || elem === 0 ) {

			// Add nodes directly
			if ( jQuery.type( elem ) === "object" ) {

				// Support: Android <=4.0 only, PhantomJS 1 only
				// push.apply(_, arraylike) throws on ancient WebKit
				jQuery.merge( nodes, elem.nodeType ? [ elem ] : elem );

			// Convert non-html into a text node
			} else if ( !rhtml.test( elem ) ) {
				nodes.push( context.createTextNode( elem ) );

			// Convert html into DOM nodes
			} else {
				tmp = tmp || fragment.appendChild( context.createElement( "div" ) );

				// Deserialize a standard representation
				tag = ( rtagName.exec( elem ) || [ "", "" ] )[ 1 ].toLowerCase();
				wrap = wrapMap[ tag ] || wrapMap._default;
				tmp.innerHTML = wrap[ 1 ] + jQuery.htmlPrefilter( elem ) + wrap[ 2 ];

				// Descend through wrappers to the right content
				j = wrap[ 0 ];
				while ( j-- ) {
					tmp = tmp.lastChild;
				}

				// Support: Android <=4.0 only, PhantomJS 1 only
				// push.apply(_, arraylike) throws on ancient WebKit
				jQuery.merge( nodes, tmp.childNodes );

				// Remember the top-level container
				tmp = fragment.firstChild;

				// Ensure the created nodes are orphaned (#12392)
				tmp.textContent = "";
			}
		}
	}

	// Remove wrapper from fragment
	fragment.textContent = "";

	i = 0;
	while ( ( elem = nodes[ i++ ] ) ) {

		// Skip elements already in the context collection (trac-4087)
		if ( selection && jQuery.inArray( elem, selection ) > -1 ) {
			if ( ignored ) {
				ignored.push( elem );
			}
			continue;
		}

		contains = jQuery.contains( elem.ownerDocument, elem );

		// Append to fragment
		tmp = getAll( fragment.appendChild( elem ), "script" );

		// Preserve script evaluation history
		if ( contains ) {
			setGlobalEval( tmp );
		}

		// Capture executables
		if ( scripts ) {
			j = 0;
			while ( ( elem = tmp[ j++ ] ) ) {
				if ( rscriptType.test( elem.type || "" ) ) {
					scripts.push( elem );
				}
			}
		}
	}

	return fragment;
}


( function() {
	var fragment = document.createDocumentFragment(),
		div = fragment.appendChild( document.createElement( "div" ) ),
		input = document.createElement( "input" );

	// Support: Android 4.0 - 4.3 only
	// Check state lost if the name is set (#11217)
	// Support: Windows Web Apps (WWA)
	// `name` and `type` must use .setAttribute for WWA (#14901)
	input.setAttribute( "type", "radio" );
	input.setAttribute( "checked", "checked" );
	input.setAttribute( "name", "t" );

	div.appendChild( input );

	// Support: Android <=4.1 only
	// Older WebKit doesn't clone checked state correctly in fragments
	support.checkClone = div.cloneNode( true ).cloneNode( true ).lastChild.checked;

	// Support: IE <=11 only
	// Make sure textarea (and checkbox) defaultValue is properly cloned
	div.innerHTML = "<textarea>x</textarea>";
	support.noCloneChecked = !!div.cloneNode( true ).lastChild.defaultValue;
} )();
var documentElement = document.documentElement;



var
	rkeyEvent = /^key/,
	rmouseEvent = /^(?:mouse|pointer|contextmenu|drag|drop)|click/,
	rtypenamespace = /^([^.]*)(?:\.(.+)|)/;

function returnTrue() {
	return true;
}

function returnFalse() {
	return false;
}

// Support: IE <=9 only
// See #13393 for more info
function safeActiveElement() {
	try {
		return document.activeElement;
	} catch ( err ) { }
}

function on( elem, types, selector, data, fn, one ) {
	var origFn, type;

	// Types can be a map of types/handlers
	if ( typeof types === "object" ) {

		// ( types-Object, selector, data )
		if ( typeof selector !== "string" ) {

			// ( types-Object, data )
			data = data || selector;
			selector = undefined;
		}
		for ( type in types ) {
			on( elem, type, selector, data, types[ type ], one );
		}
		return elem;
	}

	if ( data == null && fn == null ) {

		// ( types, fn )
		fn = selector;
		data = selector = undefined;
	} else if ( fn == null ) {
		if ( typeof selector === "string" ) {

			// ( types, selector, fn )
			fn = data;
			data = undefined;
		} else {

			// ( types, data, fn )
			fn = data;
			data = selector;
			selector = undefined;
		}
	}
	if ( fn === false ) {
		fn = returnFalse;
	} else if ( !fn ) {
		return elem;
	}

	if ( one === 1 ) {
		origFn = fn;
		fn = function( event ) {

			// Can use an empty set, since event contains the info
			jQuery().off( event );
			return origFn.apply( this, arguments );
		};

		// Use same guid so caller can remove using origFn
		fn.guid = origFn.guid || ( origFn.guid = jQuery.guid++ );
	}
	return elem.each( function() {
		jQuery.event.add( this, types, fn, data, selector );
	} );
}

/*
 * Helper functions for managing events -- not part of the public interface.
 * Props to Dean Edwards' addEvent library for many of the ideas.
 */
jQuery.event = {

	global: {},

	add: function( elem, types, handler, data, selector ) {

		var handleObjIn, eventHandle, tmp,
			events, t, handleObj,
			special, handlers, type, namespaces, origType,
			elemData = dataPriv.get( elem );

		// Don't attach events to noData or text/comment nodes (but allow plain objects)
		if ( !elemData ) {
			return;
		}

		// Caller can pass in an object of custom data in lieu of the handler
		if ( handler.handler ) {
			handleObjIn = handler;
			handler = handleObjIn.handler;
			selector = handleObjIn.selector;
		}

		// Ensure that invalid selectors throw exceptions at attach time
		// Evaluate against documentElement in case elem is a non-element node (e.g., document)
		if ( selector ) {
			jQuery.find.matchesSelector( documentElement, selector );
		}

		// Make sure that the handler has a unique ID, used to find/remove it later
		if ( !handler.guid ) {
			handler.guid = jQuery.guid++;
		}

		// Init the element's event structure and main handler, if this is the first
		if ( !( events = elemData.events ) ) {
			events = elemData.events = {};
		}
		if ( !( eventHandle = elemData.handle ) ) {
			eventHandle = elemData.handle = function( e ) {

				// Discard the second event of a jQuery.event.trigger() and
				// when an event is called after a page has unloaded
				return typeof jQuery !== "undefined" && jQuery.event.triggered !== e.type ?
					jQuery.event.dispatch.apply( elem, arguments ) : undefined;
			};
		}

		// Handle multiple events separated by a space
		types = ( types || "" ).match( rnothtmlwhite ) || [ "" ];
		t = types.length;
		while ( t-- ) {
			tmp = rtypenamespace.exec( types[ t ] ) || [];
			type = origType = tmp[ 1 ];
			namespaces = ( tmp[ 2 ] || "" ).split( "." ).sort();

			// There *must* be a type, no attaching namespace-only handlers
			if ( !type ) {
				continue;
			}

			// If event changes its type, use the special event handlers for the changed type
			special = jQuery.event.special[ type ] || {};

			// If selector defined, determine special event api type, otherwise given type
			type = ( selector ? special.delegateType : special.bindType ) || type;

			// Update special based on newly reset type
			special = jQuery.event.special[ type ] || {};

			// handleObj is passed to all event handlers
			handleObj = jQuery.extend( {
				type: type,
				origType: origType,
				data: data,
				handler: handler,
				guid: handler.guid,
				selector: selector,
				needsContext: selector && jQuery.expr.match.needsContext.test( selector ),
				namespace: namespaces.join( "." )
			}, handleObjIn );

			// Init the event handler queue if we're the first
			if ( !( handlers = events[ type ] ) ) {
				handlers = events[ type ] = [];
				handlers.delegateCount = 0;

				// Only use addEventListener if the special events handler returns false
				if ( !special.setup ||
					special.setup.call( elem, data, namespaces, eventHandle ) === false ) {

					if ( elem.addEventListener ) {
						elem.addEventListener( type, eventHandle );
					}
				}
			}

			if ( special.add ) {
				special.add.call( elem, handleObj );

				if ( !handleObj.handler.guid ) {
					handleObj.handler.guid = handler.guid;
				}
			}

			// Add to the element's handler list, delegates in front
			if ( selector ) {
				handlers.splice( handlers.delegateCount++, 0, handleObj );
			} else {
				handlers.push( handleObj );
			}

			// Keep track of which events have ever been used, for event optimization
			jQuery.event.global[ type ] = true;
		}

	},

	// Detach an event or set of events from an element
	remove: function( elem, types, handler, selector, mappedTypes ) {

		var j, origCount, tmp,
			events, t, handleObj,
			special, handlers, type, namespaces, origType,
			elemData = dataPriv.hasData( elem ) && dataPriv.get( elem );

		if ( !elemData || !( events = elemData.events ) ) {
			return;
		}

		// Once for each type.namespace in types; type may be omitted
		types = ( types || "" ).match( rnothtmlwhite ) || [ "" ];
		t = types.length;
		while ( t-- ) {
			tmp = rtypenamespace.exec( types[ t ] ) || [];
			type = origType = tmp[ 1 ];
			namespaces = ( tmp[ 2 ] || "" ).split( "." ).sort();

			// Unbind all events (on this namespace, if provided) for the element
			if ( !type ) {
				for ( type in events ) {
					jQuery.event.remove( elem, type + types[ t ], handler, selector, true );
				}
				continue;
			}

			special = jQuery.event.special[ type ] || {};
			type = ( selector ? special.delegateType : special.bindType ) || type;
			handlers = events[ type ] || [];
			tmp = tmp[ 2 ] &&
				new RegExp( "(^|\\.)" + namespaces.join( "\\.(?:.*\\.|)" ) + "(\\.|$)" );

			// Remove matching events
			origCount = j = handlers.length;
			while ( j-- ) {
				handleObj = handlers[ j ];

				if ( ( mappedTypes || origType === handleObj.origType ) &&
					( !handler || handler.guid === handleObj.guid ) &&
					( !tmp || tmp.test( handleObj.namespace ) ) &&
					( !selector || selector === handleObj.selector ||
						selector === "**" && handleObj.selector ) ) {
					handlers.splice( j, 1 );

					if ( handleObj.selector ) {
						handlers.delegateCount--;
					}
					if ( special.remove ) {
						special.remove.call( elem, handleObj );
					}
				}
			}

			// Remove generic event handler if we removed something and no more handlers exist
			// (avoids potential for endless recursion during removal of special event handlers)
			if ( origCount && !handlers.length ) {
				if ( !special.teardown ||
					special.teardown.call( elem, namespaces, elemData.handle ) === false ) {

					jQuery.removeEvent( elem, type, elemData.handle );
				}

				delete events[ type ];
			}
		}

		// Remove data and the expando if it's no longer used
		if ( jQuery.isEmptyObject( events ) ) {
			dataPriv.remove( elem, "handle events" );
		}
	},

	dispatch: function( nativeEvent ) {

		// Make a writable jQuery.Event from the native event object
		var event = jQuery.event.fix( nativeEvent );

		var i, j, ret, matched, handleObj, handlerQueue,
			args = new Array( arguments.length ),
			handlers = ( dataPriv.get( this, "events" ) || {} )[ event.type ] || [],
			special = jQuery.event.special[ event.type ] || {};

		// Use the fix-ed jQuery.Event rather than the (read-only) native event
		args[ 0 ] = event;

		for ( i = 1; i < arguments.length; i++ ) {
			args[ i ] = arguments[ i ];
		}

		event.delegateTarget = this;

		// Call the preDispatch hook for the mapped type, and let it bail if desired
		if ( special.preDispatch && special.preDispatch.call( this, event ) === false ) {
			return;
		}

		// Determine handlers
		handlerQueue = jQuery.event.handlers.call( this, event, handlers );

		// Run delegates first; they may want to stop propagation beneath us
		i = 0;
		while ( ( matched = handlerQueue[ i++ ] ) && !event.isPropagationStopped() ) {
			event.currentTarget = matched.elem;

			j = 0;
			while ( ( handleObj = matched.handlers[ j++ ] ) &&
				!event.isImmediatePropagationStopped() ) {

				// Triggered event must either 1) have no namespace, or 2) have namespace(s)
				// a subset or equal to those in the bound event (both can have no namespace).
				if ( !event.rnamespace || event.rnamespace.test( handleObj.namespace ) ) {

					event.handleObj = handleObj;
					event.data = handleObj.data;

					ret = ( ( jQuery.event.special[ handleObj.origType ] || {} ).handle ||
						handleObj.handler ).apply( matched.elem, args );

					if ( ret !== undefined ) {
						if ( ( event.result = ret ) === false ) {
							event.preventDefault();
							event.stopPropagation();
						}
					}
				}
			}
		}

		// Call the postDispatch hook for the mapped type
		if ( special.postDispatch ) {
			special.postDispatch.call( this, event );
		}

		return event.result;
	},

	handlers: function( event, handlers ) {
		var i, handleObj, sel, matchedHandlers, matchedSelectors,
			handlerQueue = [],
			delegateCount = handlers.delegateCount,
			cur = event.target;

		// Find delegate handlers
		if ( delegateCount &&

			// Support: IE <=9
			// Black-hole SVG <use> instance trees (trac-13180)
			cur.nodeType &&

			// Support: Firefox <=42
			// Suppress spec-violating clicks indicating a non-primary pointer button (trac-3861)
			// https://www.w3.org/TR/DOM-Level-3-Events/#event-type-click
			// Support: IE 11 only
			// ...but not arrow key "clicks" of radio inputs, which can have `button` -1 (gh-2343)
			!( event.type === "click" && event.button >= 1 ) ) {

			for ( ; cur !== this; cur = cur.parentNode || this ) {

				// Don't check non-elements (#13208)
				// Don't process clicks on disabled elements (#6911, #8165, #11382, #11764)
				if ( cur.nodeType === 1 && !( event.type === "click" && cur.disabled === true ) ) {
					matchedHandlers = [];
					matchedSelectors = {};
					for ( i = 0; i < delegateCount; i++ ) {
						handleObj = handlers[ i ];

						// Don't conflict with Object.prototype properties (#13203)
						sel = handleObj.selector + " ";

						if ( matchedSelectors[ sel ] === undefined ) {
							matchedSelectors[ sel ] = handleObj.needsContext ?
								jQuery( sel, this ).index( cur ) > -1 :
								jQuery.find( sel, this, null, [ cur ] ).length;
						}
						if ( matchedSelectors[ sel ] ) {
							matchedHandlers.push( handleObj );
						}
					}
					if ( matchedHandlers.length ) {
						handlerQueue.push( { elem: cur, handlers: matchedHandlers } );
					}
				}
			}
		}

		// Add the remaining (directly-bound) handlers
		cur = this;
		if ( delegateCount < handlers.length ) {
			handlerQueue.push( { elem: cur, handlers: handlers.slice( delegateCount ) } );
		}

		return handlerQueue;
	},

	addProp: function( name, hook ) {
		Object.defineProperty( jQuery.Event.prototype, name, {
			enumerable: true,
			configurable: true,

			get: jQuery.isFunction( hook ) ?
				function() {
					if ( this.originalEvent ) {
							return hook( this.originalEvent );
					}
				} :
				function() {
					if ( this.originalEvent ) {
							return this.originalEvent[ name ];
					}
				},

			set: function( value ) {
				Object.defineProperty( this, name, {
					enumerable: true,
					configurable: true,
					writable: true,
					value: value
				} );
			}
		} );
	},

	fix: function( originalEvent ) {
		return originalEvent[ jQuery.expando ] ?
			originalEvent :
			new jQuery.Event( originalEvent );
	},

	special: {
		load: {

			// Prevent triggered image.load events from bubbling to window.load
			noBubble: true
		},
		focus: {

			// Fire native event if possible so blur/focus sequence is correct
			trigger: function() {
				if ( this !== safeActiveElement() && this.focus ) {
					this.focus();
					return false;
				}
			},
			delegateType: "focusin"
		},
		blur: {
			trigger: function() {
				if ( this === safeActiveElement() && this.blur ) {
					this.blur();
					return false;
				}
			},
			delegateType: "focusout"
		},
		click: {

			// For checkbox, fire native event so checked state will be right
			trigger: function() {
				if ( this.type === "checkbox" && this.click && nodeName( this, "input" ) ) {
					this.click();
					return false;
				}
			},

			// For cross-browser consistency, don't fire native .click() on links
			_default: function( event ) {
				return nodeName( event.target, "a" );
			}
		},

		beforeunload: {
			postDispatch: function( event ) {

				// Support: Firefox 20+
				// Firefox doesn't alert if the returnValue field is not set.
				if ( event.result !== undefined && event.originalEvent ) {
					event.originalEvent.returnValue = event.result;
				}
			}
		}
	}
};

jQuery.removeEvent = function( elem, type, handle ) {

	// This "if" is needed for plain objects
	if ( elem.removeEventListener ) {
		elem.removeEventListener( type, handle );
	}
};

jQuery.Event = function( src, props ) {

	// Allow instantiation without the 'new' keyword
	if ( !( this instanceof jQuery.Event ) ) {
		return new jQuery.Event( src, props );
	}

	// Event object
	if ( src && src.type ) {
		this.originalEvent = src;
		this.type = src.type;

		// Events bubbling up the document may have been marked as prevented
		// by a handler lower down the tree; reflect the correct value.
		this.isDefaultPrevented = src.defaultPrevented ||
				src.defaultPrevented === undefined &&

				// Support: Android <=2.3 only
				src.returnValue === false ?
			returnTrue :
			returnFalse;

		// Create target properties
		// Support: Safari <=6 - 7 only
		// Target should not be a text node (#504, #13143)
		this.target = ( src.target && src.target.nodeType === 3 ) ?
			src.target.parentNode :
			src.target;

		this.currentTarget = src.currentTarget;
		this.relatedTarget = src.relatedTarget;

	// Event type
	} else {
		this.type = src;
	}

	// Put explicitly provided properties onto the event object
	if ( props ) {
		jQuery.extend( this, props );
	}

	// Create a timestamp if incoming event doesn't have one
	this.timeStamp = src && src.timeStamp || jQuery.now();

	// Mark it as fixed
	this[ jQuery.expando ] = true;
};

// jQuery.Event is based on DOM3 Events as specified by the ECMAScript Language Binding
// https://www.w3.org/TR/2003/WD-DOM-Level-3-Events-20030331/ecma-script-binding.html
jQuery.Event.prototype = {
	constructor: jQuery.Event,
	isDefaultPrevented: returnFalse,
	isPropagationStopped: returnFalse,
	isImmediatePropagationStopped: returnFalse,
	isSimulated: false,

	preventDefault: function() {
		var e = this.originalEvent;

		this.isDefaultPrevented = returnTrue;

		if ( e && !this.isSimulated ) {
			e.preventDefault();
		}
	},
	stopPropagation: function() {
		var e = this.originalEvent;

		this.isPropagationStopped = returnTrue;

		if ( e && !this.isSimulated ) {
			e.stopPropagation();
		}
	},
	stopImmediatePropagation: function() {
		var e = this.originalEvent;

		this.isImmediatePropagationStopped = returnTrue;

		if ( e && !this.isSimulated ) {
			e.stopImmediatePropagation();
		}

		this.stopPropagation();
	}
};

// Includes all common event props including KeyEvent and MouseEvent specific props
jQuery.each( {
	altKey: true,
	bubbles: true,
	cancelable: true,
	changedTouches: true,
	ctrlKey: true,
	detail: true,
	eventPhase: true,
	metaKey: true,
	pageX: true,
	pageY: true,
	shiftKey: true,
	view: true,
	"char": true,
	charCode: true,
	key: true,
	keyCode: true,
	button: true,
	buttons: true,
	clientX: true,
	clientY: true,
	offsetX: true,
	offsetY: true,
	pointerId: true,
	pointerType: true,
	screenX: true,
	screenY: true,
	targetTouches: true,
	toElement: true,
	touches: true,

	which: function( event ) {
		var button = event.button;

		// Add which for key events
		if ( event.which == null && rkeyEvent.test( event.type ) ) {
			return event.charCode != null ? event.charCode : event.keyCode;
		}

		// Add which for click: 1 === left; 2 === middle; 3 === right
		if ( !event.which && button !== undefined && rmouseEvent.test( event.type ) ) {
			if ( button & 1 ) {
				return 1;
			}

			if ( button & 2 ) {
				return 3;
			}

			if ( button & 4 ) {
				return 2;
			}

			return 0;
		}

		return event.which;
	}
}, jQuery.event.addProp );

// Create mouseenter/leave events using mouseover/out and event-time checks
// so that event delegation works in jQuery.
// Do the same for pointerenter/pointerleave and pointerover/pointerout
//
// Support: Safari 7 only
// Safari sends mouseenter too often; see:
// https://bugs.chromium.org/p/chromium/issues/detail?id=470258
// for the description of the bug (it existed in older Chrome versions as well).
jQuery.each( {
	mouseenter: "mouseover",
	mouseleave: "mouseout",
	pointerenter: "pointerover",
	pointerleave: "pointerout"
}, function( orig, fix ) {
	jQuery.event.special[ orig ] = {
		delegateType: fix,
		bindType: fix,

		handle: function( event ) {
			var ret,
				target = this,
				related = event.relatedTarget,
				handleObj = event.handleObj;

			// For mouseenter/leave call the handler if related is outside the target.
			// NB: No relatedTarget if the mouse left/entered the browser window
			if ( !related || ( related !== target && !jQuery.contains( target, related ) ) ) {
				event.type = handleObj.origType;
				ret = handleObj.handler.apply( this, arguments );
				event.type = fix;
			}
			return ret;
		}
	};
} );

jQuery.fn.extend( {

	on: function( types, selector, data, fn ) {
		return on( this, types, selector, data, fn );
	},
	one: function( types, selector, data, fn ) {
		return on( this, types, selector, data, fn, 1 );
	},
	off: function( types, selector, fn ) {
		var handleObj, type;
		if ( types && types.preventDefault && types.handleObj ) {

			// ( event )  dispatched jQuery.Event
			handleObj = types.handleObj;
			jQuery( types.delegateTarget ).off(
				handleObj.namespace ?
					handleObj.origType + "." + handleObj.namespace :
					handleObj.origType,
				handleObj.selector,
				handleObj.handler
			);
			return this;
		}
		if ( typeof types === "object" ) {

			// ( types-object [, selector] )
			for ( type in types ) {
				this.off( type, selector, types[ type ] );
			}
			return this;
		}
		if ( selector === false || typeof selector === "function" ) {

			// ( types [, fn] )
			fn = selector;
			selector = undefined;
		}
		if ( fn === false ) {
			fn = returnFalse;
		}
		return this.each( function() {
			jQuery.event.remove( this, types, fn, selector );
		} );
	}
} );


var

	/* eslint-disable max-len */

	// See https://github.com/eslint/eslint/issues/3229
	rxhtmlTag = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([a-z][^\/\0>\x20\t\r\n\f]*)[^>]*)\/>/gi,

	/* eslint-enable */

	// Support: IE <=10 - 11, Edge 12 - 13
	// In IE/Edge using regex groups here causes severe slowdowns.
	// See https://connect.microsoft.com/IE/feedback/details/1736512/
	rnoInnerhtml = /<script|<style|<link/i,

	// checked="checked" or checked
	rchecked = /checked\s*(?:[^=]|=\s*.checked.)/i,
	rscriptTypeMasked = /^true\/(.*)/,
	rcleanScript = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g;

// Prefer a tbody over its parent table for containing new rows
function manipulationTarget( elem, content ) {
	if ( nodeName( elem, "table" ) &&
		nodeName( content.nodeType !== 11 ? content : content.firstChild, "tr" ) ) {

		return jQuery( ">tbody", elem )[ 0 ] || elem;
	}

	return elem;
}

// Replace/restore the type attribute of script elements for safe DOM manipulation
function disableScript( elem ) {
	elem.type = ( elem.getAttribute( "type" ) !== null ) + "/" + elem.type;
	return elem;
}
function restoreScript( elem ) {
	var match = rscriptTypeMasked.exec( elem.type );

	if ( match ) {
		elem.type = match[ 1 ];
	} else {
		elem.removeAttribute( "type" );
	}

	return elem;
}

function cloneCopyEvent( src, dest ) {
	var i, l, type, pdataOld, pdataCur, udataOld, udataCur, events;

	if ( dest.nodeType !== 1 ) {
		return;
	}

	// 1. Copy private data: events, handlers, etc.
	if ( dataPriv.hasData( src ) ) {
		pdataOld = dataPriv.access( src );
		pdataCur = dataPriv.set( dest, pdataOld );
		events = pdataOld.events;

		if ( events ) {
			delete pdataCur.handle;
			pdataCur.events = {};

			for ( type in events ) {
				for ( i = 0, l = events[ type ].length; i < l; i++ ) {
					jQuery.event.add( dest, type, events[ type ][ i ] );
				}
			}
		}
	}

	// 2. Copy user data
	if ( dataUser.hasData( src ) ) {
		udataOld = dataUser.access( src );
		udataCur = jQuery.extend( {}, udataOld );

		dataUser.set( dest, udataCur );
	}
}

// Fix IE bugs, see support tests
function fixInput( src, dest ) {
	var nodeName = dest.nodeName.toLowerCase();

	// Fails to persist the checked state of a cloned checkbox or radio button.
	if ( nodeName === "input" && rcheckableType.test( src.type ) ) {
		dest.checked = src.checked;

	// Fails to return the selected option to the default selected state when cloning options
	} else if ( nodeName === "input" || nodeName === "textarea" ) {
		dest.defaultValue = src.defaultValue;
	}
}

function domManip( collection, args, callback, ignored ) {

	// Flatten any nested arrays
	args = concat.apply( [], args );

	var fragment, first, scripts, hasScripts, node, doc,
		i = 0,
		l = collection.length,
		iNoClone = l - 1,
		value = args[ 0 ],
		isFunction = jQuery.isFunction( value );

	// We can't cloneNode fragments that contain checked, in WebKit
	if ( isFunction ||
			( l > 1 && typeof value === "string" &&
				!support.checkClone && rchecked.test( value ) ) ) {
		return collection.each( function( index ) {
			var self = collection.eq( index );
			if ( isFunction ) {
				args[ 0 ] = value.call( this, index, self.html() );
			}
			domManip( self, args, callback, ignored );
		} );
	}

	if ( l ) {
		fragment = buildFragment( args, collection[ 0 ].ownerDocument, false, collection, ignored );
		first = fragment.firstChild;

		if ( fragment.childNodes.length === 1 ) {
			fragment = first;
		}

		// Require either new content or an interest in ignored elements to invoke the callback
		if ( first || ignored ) {
			scripts = jQuery.map( getAll( fragment, "script" ), disableScript );
			hasScripts = scripts.length;

			// Use the original fragment for the last item
			// instead of the first because it can end up
			// being emptied incorrectly in certain situations (#8070).
			for ( ; i < l; i++ ) {
				node = fragment;

				if ( i !== iNoClone ) {
					node = jQuery.clone( node, true, true );

					// Keep references to cloned scripts for later restoration
					if ( hasScripts ) {

						// Support: Android <=4.0 only, PhantomJS 1 only
						// push.apply(_, arraylike) throws on ancient WebKit
						jQuery.merge( scripts, getAll( node, "script" ) );
					}
				}

				callback.call( collection[ i ], node, i );
			}

			if ( hasScripts ) {
				doc = scripts[ scripts.length - 1 ].ownerDocument;

				// Reenable scripts
				jQuery.map( scripts, restoreScript );

				// Evaluate executable scripts on first document insertion
				for ( i = 0; i < hasScripts; i++ ) {
					node = scripts[ i ];
					if ( rscriptType.test( node.type || "" ) &&
						!dataPriv.access( node, "globalEval" ) &&
						jQuery.contains( doc, node ) ) {

						if ( node.src ) {

							// Optional AJAX dependency, but won't run scripts if not present
							if ( jQuery._evalUrl ) {
								jQuery._evalUrl( node.src );
							}
						} else {
							DOMEval( node.textContent.replace( rcleanScript, "" ), doc );
						}
					}
				}
			}
		}
	}

	return collection;
}

function remove( elem, selector, keepData ) {
	var node,
		nodes = selector ? jQuery.filter( selector, elem ) : elem,
		i = 0;

	for ( ; ( node = nodes[ i ] ) != null; i++ ) {
		if ( !keepData && node.nodeType === 1 ) {
			jQuery.cleanData( getAll( node ) );
		}

		if ( node.parentNode ) {
			if ( keepData && jQuery.contains( node.ownerDocument, node ) ) {
				setGlobalEval( getAll( node, "script" ) );
			}
			node.parentNode.removeChild( node );
		}
	}

	return elem;
}

jQuery.extend( {
	htmlPrefilter: function( html ) {
		return html.replace( rxhtmlTag, "<$1></$2>" );
	},

	clone: function( elem, dataAndEvents, deepDataAndEvents ) {
		var i, l, srcElements, destElements,
			clone = elem.cloneNode( true ),
			inPage = jQuery.contains( elem.ownerDocument, elem );

		// Fix IE cloning issues
		if ( !support.noCloneChecked && ( elem.nodeType === 1 || elem.nodeType === 11 ) &&
				!jQuery.isXMLDoc( elem ) ) {

			// We eschew Sizzle here for performance reasons: https://jsperf.com/getall-vs-sizzle/2
			destElements = getAll( clone );
			srcElements = getAll( elem );

			for ( i = 0, l = srcElements.length; i < l; i++ ) {
				fixInput( srcElements[ i ], destElements[ i ] );
			}
		}

		// Copy the events from the original to the clone
		if ( dataAndEvents ) {
			if ( deepDataAndEvents ) {
				srcElements = srcElements || getAll( elem );
				destElements = destElements || getAll( clone );

				for ( i = 0, l = srcElements.length; i < l; i++ ) {
					cloneCopyEvent( srcElements[ i ], destElements[ i ] );
				}
			} else {
				cloneCopyEvent( elem, clone );
			}
		}

		// Preserve script evaluation history
		destElements = getAll( clone, "script" );
		if ( destElements.length > 0 ) {
			setGlobalEval( destElements, !inPage && getAll( elem, "script" ) );
		}

		// Return the cloned set
		return clone;
	},

	cleanData: function( elems ) {
		var data, elem, type,
			special = jQuery.event.special,
			i = 0;

		for ( ; ( elem = elems[ i ] ) !== undefined; i++ ) {
			if ( acceptData( elem ) ) {
				if ( ( data = elem[ dataPriv.expando ] ) ) {
					if ( data.events ) {
						for ( type in data.events ) {
							if ( special[ type ] ) {
								jQuery.event.remove( elem, type );

							// This is a shortcut to avoid jQuery.event.remove's overhead
							} else {
								jQuery.removeEvent( elem, type, data.handle );
							}
						}
					}

					// Support: Chrome <=35 - 45+
					// Assign undefined instead of using delete, see Data#remove
					elem[ dataPriv.expando ] = undefined;
				}
				if ( elem[ dataUser.expando ] ) {

					// Support: Chrome <=35 - 45+
					// Assign undefined instead of using delete, see Data#remove
					elem[ dataUser.expando ] = undefined;
				}
			}
		}
	}
} );

jQuery.fn.extend( {
	detach: function( selector ) {
		return remove( this, selector, true );
	},

	remove: function( selector ) {
		return remove( this, selector );
	},

	text: function( value ) {
		return access( this, function( value ) {
			return value === undefined ?
				jQuery.text( this ) :
				this.empty().each( function() {
					if ( this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9 ) {
						this.textContent = value;
					}
				} );
		}, null, value, arguments.length );
	},

	append: function() {
		return domManip( this, arguments, function( elem ) {
			if ( this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9 ) {
				var target = manipulationTarget( this, elem );
				target.appendChild( elem );
			}
		} );
	},

	prepend: function() {
		return domManip( this, arguments, function( elem ) {
			if ( this.nodeType === 1 || this.nodeType === 11 || this.nodeType === 9 ) {
				var target = manipulationTarget( this, elem );
				target.insertBefore( elem, target.firstChild );
			}
		} );
	},

	before: function() {
		return domManip( this, arguments, function( elem ) {
			if ( this.parentNode ) {
				this.parentNode.insertBefore( elem, this );
			}
		} );
	},

	after: function() {
		return domManip( this, arguments, function( elem ) {
			if ( this.parentNode ) {
				this.parentNode.insertBefore( elem, this.nextSibling );
			}
		} );
	},

	empty: function() {
		var elem,
			i = 0;

		for ( ; ( elem = this[ i ] ) != null; i++ ) {
			if ( elem.nodeType === 1 ) {

				// Prevent memory leaks
				jQuery.cleanData( getAll( elem, false ) );

				// Remove any remaining nodes
				elem.textContent = "";
			}
		}

		return this;
	},

	clone: function( dataAndEvents, deepDataAndEvents ) {
		dataAndEvents = dataAndEvents == null ? false : dataAndEvents;
		deepDataAndEvents = deepDataAndEvents == null ? dataAndEvents : deepDataAndEvents;

		return this.map( function() {
			return jQuery.clone( this, dataAndEvents, deepDataAndEvents );
		} );
	},

	html: function( value ) {
		return access( this, function( value ) {
			var elem = this[ 0 ] || {},
				i = 0,
				l = this.length;

			if ( value === undefined && elem.nodeType === 1 ) {
				return elem.innerHTML;
			}

			// See if we can take a shortcut and just use innerHTML
			if ( typeof value === "string" && !rnoInnerhtml.test( value ) &&
				!wrapMap[ ( rtagName.exec( value ) || [ "", "" ] )[ 1 ].toLowerCase() ] ) {

				value = jQuery.htmlPrefilter( value );

				try {
					for ( ; i < l; i++ ) {
						elem = this[ i ] || {};

						// Remove element nodes and prevent memory leaks
						if ( elem.nodeType === 1 ) {
							jQuery.cleanData( getAll( elem, false ) );
							elem.innerHTML = value;
						}
					}

					elem = 0;

				// If using innerHTML throws an exception, use the fallback method
				} catch ( e ) {}
			}

			if ( elem ) {
				this.empty().append( value );
			}
		}, null, value, arguments.length );
	},

	replaceWith: function() {
		var ignored = [];

		// Make the changes, replacing each non-ignored context element with the new content
		return domManip( this, arguments, function( elem ) {
			var parent = this.parentNode;

			if ( jQuery.inArray( this, ignored ) < 0 ) {
				jQuery.cleanData( getAll( this ) );
				if ( parent ) {
					parent.replaceChild( elem, this );
				}
			}

		// Force callback invocation
		}, ignored );
	}
} );

jQuery.each( {
	appendTo: "append",
	prependTo: "prepend",
	insertBefore: "before",
	insertAfter: "after",
	replaceAll: "replaceWith"
}, function( name, original ) {
	jQuery.fn[ name ] = function( selector ) {
		var elems,
			ret = [],
			insert = jQuery( selector ),
			last = insert.length - 1,
			i = 0;

		for ( ; i <= last; i++ ) {
			elems = i === last ? this : this.clone( true );
			jQuery( insert[ i ] )[ original ]( elems );

			// Support: Android <=4.0 only, PhantomJS 1 only
			// .get() because push.apply(_, arraylike) throws on ancient WebKit
			push.apply( ret, elems.get() );
		}

		return this.pushStack( ret );
	};
} );
var rmargin = ( /^margin/ );

var rnumnonpx = new RegExp( "^(" + pnum + ")(?!px)[a-z%]+$", "i" );

var getStyles = function( elem ) {

		// Support: IE <=11 only, Firefox <=30 (#15098, #14150)
		// IE throws on elements created in popups
		// FF meanwhile throws on frame elements through "defaultView.getComputedStyle"
		var view = elem.ownerDocument.defaultView;

		if ( !view || !view.opener ) {
			view = window;
		}

		return view.getComputedStyle( elem );
	};



( function() {

	// Executing both pixelPosition & boxSizingReliable tests require only one layout
	// so they're executed at the same time to save the second computation.
	function computeStyleTests() {

		// This is a singleton, we need to execute it only once
		if ( !div ) {
			return;
		}

		div.style.cssText =
			"box-sizing:border-box;" +
			"position:relative;display:block;" +
			"margin:auto;border:1px;padding:1px;" +
			"top:1%;width:50%";
		div.innerHTML = "";
		documentElement.appendChild( container );

		var divStyle = window.getComputedStyle( div );
		pixelPositionVal = divStyle.top !== "1%";

		// Support: Android 4.0 - 4.3 only, Firefox <=3 - 44
		reliableMarginLeftVal = divStyle.marginLeft === "2px";
		boxSizingReliableVal = divStyle.width === "4px";

		// Support: Android 4.0 - 4.3 only
		// Some styles come back with percentage values, even though they shouldn't
		div.style.marginRight = "50%";
		pixelMarginRightVal = divStyle.marginRight === "4px";

		documentElement.removeChild( container );

		// Nullify the div so it wouldn't be stored in the memory and
		// it will also be a sign that checks already performed
		div = null;
	}

	var pixelPositionVal, boxSizingReliableVal, pixelMarginRightVal, reliableMarginLeftVal,
		container = document.createElement( "div" ),
		div = document.createElement( "div" );

	// Finish early in limited (non-browser) environments
	if ( !div.style ) {
		return;
	}

	// Support: IE <=9 - 11 only
	// Style of cloned element affects source element cloned (#8908)
	div.style.backgroundClip = "content-box";
	div.cloneNode( true ).style.backgroundClip = "";
	support.clearCloneStyle = div.style.backgroundClip === "content-box";

	container.style.cssText = "border:0;width:8px;height:0;top:0;left:-9999px;" +
		"padding:0;margin-top:1px;position:absolute";
	container.appendChild( div );

	jQuery.extend( support, {
		pixelPosition: function() {
			computeStyleTests();
			return pixelPositionVal;
		},
		boxSizingReliable: function() {
			computeStyleTests();
			return boxSizingReliableVal;
		},
		pixelMarginRight: function() {
			computeStyleTests();
			return pixelMarginRightVal;
		},
		reliableMarginLeft: function() {
			computeStyleTests();
			return reliableMarginLeftVal;
		}
	} );
} )();


function curCSS( elem, name, computed ) {
	var width, minWidth, maxWidth, ret,

		// Support: Firefox 51+
		// Retrieving style before computed somehow
		// fixes an issue with getting wrong values
		// on detached elements
		style = elem.style;

	computed = computed || getStyles( elem );

	// getPropertyValue is needed for:
	//   .css('filter') (IE 9 only, #12537)
	//   .css('--customProperty) (#3144)
	if ( computed ) {
		ret = computed.getPropertyValue( name ) || computed[ name ];

		if ( ret === "" && !jQuery.contains( elem.ownerDocument, elem ) ) {
			ret = jQuery.style( elem, name );
		}

		// A tribute to the "awesome hack by Dean Edwards"
		// Android Browser returns percentage for some values,
		// but width seems to be reliably pixels.
		// This is against the CSSOM draft spec:
		// https://drafts.csswg.org/cssom/#resolved-values
		if ( !support.pixelMarginRight() && rnumnonpx.test( ret ) && rmargin.test( name ) ) {

			// Remember the original values
			width = style.width;
			minWidth = style.minWidth;
			maxWidth = style.maxWidth;

			// Put in the new values to get a computed value out
			style.minWidth = style.maxWidth = style.width = ret;
			ret = computed.width;

			// Revert the changed values
			style.width = width;
			style.minWidth = minWidth;
			style.maxWidth = maxWidth;
		}
	}

	return ret !== undefined ?

		// Support: IE <=9 - 11 only
		// IE returns zIndex value as an integer.
		ret + "" :
		ret;
}


function addGetHookIf( conditionFn, hookFn ) {

	// Define the hook, we'll check on the first run if it's really needed.
	return {
		get: function() {
			if ( conditionFn() ) {

				// Hook not needed (or it's not possible to use it due
				// to missing dependency), remove it.
				delete this.get;
				return;
			}

			// Hook needed; redefine it so that the support test is not executed again.
			return ( this.get = hookFn ).apply( this, arguments );
		}
	};
}


var

	// Swappable if display is none or starts with table
	// except "table", "table-cell", or "table-caption"
	// See here for display values: https://developer.mozilla.org/en-US/docs/CSS/display
	rdisplayswap = /^(none|table(?!-c[ea]).+)/,
	rcustomProp = /^--/,
	cssShow = { position: "absolute", visibility: "hidden", display: "block" },
	cssNormalTransform = {
		letterSpacing: "0",
		fontWeight: "400"
	},

	cssPrefixes = [ "Webkit", "Moz", "ms" ],
	emptyStyle = document.createElement( "div" ).style;

// Return a css property mapped to a potentially vendor prefixed property
function vendorPropName( name ) {

	// Shortcut for names that are not vendor prefixed
	if ( name in emptyStyle ) {
		return name;
	}

	// Check for vendor prefixed names
	var capName = name[ 0 ].toUpperCase() + name.slice( 1 ),
		i = cssPrefixes.length;

	while ( i-- ) {
		name = cssPrefixes[ i ] + capName;
		if ( name in emptyStyle ) {
			return name;
		}
	}
}

// Return a property mapped along what jQuery.cssProps suggests or to
// a vendor prefixed property.
function finalPropName( name ) {
	var ret = jQuery.cssProps[ name ];
	if ( !ret ) {
		ret = jQuery.cssProps[ name ] = vendorPropName( name ) || name;
	}
	return ret;
}

function setPositiveNumber( elem, value, subtract ) {

	// Any relative (+/-) values have already been
	// normalized at this point
	var matches = rcssNum.exec( value );
	return matches ?

		// Guard against undefined "subtract", e.g., when used as in cssHooks
		Math.max( 0, matches[ 2 ] - ( subtract || 0 ) ) + ( matches[ 3 ] || "px" ) :
		value;
}

function augmentWidthOrHeight( elem, name, extra, isBorderBox, styles ) {
	var i,
		val = 0;

	// If we already have the right measurement, avoid augmentation
	if ( extra === ( isBorderBox ? "border" : "content" ) ) {
		i = 4;

	// Otherwise initialize for horizontal or vertical properties
	} else {
		i = name === "width" ? 1 : 0;
	}

	for ( ; i < 4; i += 2 ) {

		// Both box models exclude margin, so add it if we want it
		if ( extra === "margin" ) {
			val += jQuery.css( elem, extra + cssExpand[ i ], true, styles );
		}

		if ( isBorderBox ) {

			// border-box includes padding, so remove it if we want content
			if ( extra === "content" ) {
				val -= jQuery.css( elem, "padding" + cssExpand[ i ], true, styles );
			}

			// At this point, extra isn't border nor margin, so remove border
			if ( extra !== "margin" ) {
				val -= jQuery.css( elem, "border" + cssExpand[ i ] + "Width", true, styles );
			}
		} else {

			// At this point, extra isn't content, so add padding
			val += jQuery.css( elem, "padding" + cssExpand[ i ], true, styles );

			// At this point, extra isn't content nor padding, so add border
			if ( extra !== "padding" ) {
				val += jQuery.css( elem, "border" + cssExpand[ i ] + "Width", true, styles );
			}
		}
	}

	return val;
}

function getWidthOrHeight( elem, name, extra ) {

	// Start with computed style
	var valueIsBorderBox,
		styles = getStyles( elem ),
		val = curCSS( elem, name, styles ),
		isBorderBox = jQuery.css( elem, "boxSizing", false, styles ) === "border-box";

	// Computed unit is not pixels. Stop here and return.
	if ( rnumnonpx.test( val ) ) {
		return val;
	}

	// Check for style in case a browser which returns unreliable values
	// for getComputedStyle silently falls back to the reliable elem.style
	valueIsBorderBox = isBorderBox &&
		( support.boxSizingReliable() || val === elem.style[ name ] );

	// Fall back to offsetWidth/Height when value is "auto"
	// This happens for inline elements with no explicit setting (gh-3571)
	if ( val === "auto" ) {
		val = elem[ "offset" + name[ 0 ].toUpperCase() + name.slice( 1 ) ];
	}

	// Normalize "", auto, and prepare for extra
	val = parseFloat( val ) || 0;

	// Use the active box-sizing model to add/subtract irrelevant styles
	return ( val +
		augmentWidthOrHeight(
			elem,
			name,
			extra || ( isBorderBox ? "border" : "content" ),
			valueIsBorderBox,
			styles
		)
	) + "px";
}

jQuery.extend( {

	// Add in style property hooks for overriding the default
	// behavior of getting and setting a style property
	cssHooks: {
		opacity: {
			get: function( elem, computed ) {
				if ( computed ) {

					// We should always get a number back from opacity
					var ret = curCSS( elem, "opacity" );
					return ret === "" ? "1" : ret;
				}
			}
		}
	},

	// Don't automatically add "px" to these possibly-unitless properties
	cssNumber: {
		"animationIterationCount": true,
		"columnCount": true,
		"fillOpacity": true,
		"flexGrow": true,
		"flexShrink": true,
		"fontWeight": true,
		"lineHeight": true,
		"opacity": true,
		"order": true,
		"orphans": true,
		"widows": true,
		"zIndex": true,
		"zoom": true
	},

	// Add in properties whose names you wish to fix before
	// setting or getting the value
	cssProps: {
		"float": "cssFloat"
	},

	// Get and set the style property on a DOM Node
	style: function( elem, name, value, extra ) {

		// Don't set styles on text and comment nodes
		if ( !elem || elem.nodeType === 3 || elem.nodeType === 8 || !elem.style ) {
			return;
		}

		// Make sure that we're working with the right name
		var ret, type, hooks,
			origName = jQuery.camelCase( name ),
			isCustomProp = rcustomProp.test( name ),
			style = elem.style;

		// Make sure that we're working with the right name. We don't
		// want to query the value if it is a CSS custom property
		// since they are user-defined.
		if ( !isCustomProp ) {
			name = finalPropName( origName );
		}

		// Gets hook for the prefixed version, then unprefixed version
		hooks = jQuery.cssHooks[ name ] || jQuery.cssHooks[ origName ];

		// Check if we're setting a value
		if ( value !== undefined ) {
			type = typeof value;

			// Convert "+=" or "-=" to relative numbers (#7345)
			if ( type === "string" && ( ret = rcssNum.exec( value ) ) && ret[ 1 ] ) {
				value = adjustCSS( elem, name, ret );

				// Fixes bug #9237
				type = "number";
			}

			// Make sure that null and NaN values aren't set (#7116)
			if ( value == null || value !== value ) {
				return;
			}

			// If a number was passed in, add the unit (except for certain CSS properties)
			if ( type === "number" ) {
				value += ret && ret[ 3 ] || ( jQuery.cssNumber[ origName ] ? "" : "px" );
			}

			// background-* props affect original clone's values
			if ( !support.clearCloneStyle && value === "" && name.indexOf( "background" ) === 0 ) {
				style[ name ] = "inherit";
			}

			// If a hook was provided, use that value, otherwise just set the specified value
			if ( !hooks || !( "set" in hooks ) ||
				( value = hooks.set( elem, value, extra ) ) !== undefined ) {

				if ( isCustomProp ) {
					style.setProperty( name, value );
				} else {
					style[ name ] = value;
				}
			}

		} else {

			// If a hook was provided get the non-computed value from there
			if ( hooks && "get" in hooks &&
				( ret = hooks.get( elem, false, extra ) ) !== undefined ) {

				return ret;
			}

			// Otherwise just get the value from the style object
			return style[ name ];
		}
	},

	css: function( elem, name, extra, styles ) {
		var val, num, hooks,
			origName = jQuery.camelCase( name ),
			isCustomProp = rcustomProp.test( name );

		// Make sure that we're working with the right name. We don't
		// want to modify the value if it is a CSS custom property
		// since they are user-defined.
		if ( !isCustomProp ) {
			name = finalPropName( origName );
		}

		// Try prefixed name followed by the unprefixed name
		hooks = jQuery.cssHooks[ name ] || jQuery.cssHooks[ origName ];

		// If a hook was provided get the computed value from there
		if ( hooks && "get" in hooks ) {
			val = hooks.get( elem, true, extra );
		}

		// Otherwise, if a way to get the computed value exists, use that
		if ( val === undefined ) {
			val = curCSS( elem, name, styles );
		}

		// Convert "normal" to computed value
		if ( val === "normal" && name in cssNormalTransform ) {
			val = cssNormalTransform[ name ];
		}

		// Make numeric if forced or a qualifier was provided and val looks numeric
		if ( extra === "" || extra ) {
			num = parseFloat( val );
			return extra === true || isFinite( num ) ? num || 0 : val;
		}

		return val;
	}
} );

jQuery.each( [ "height", "width" ], function( i, name ) {
	jQuery.cssHooks[ name ] = {
		get: function( elem, computed, extra ) {
			if ( computed ) {

				// Certain elements can have dimension info if we invisibly show them
				// but it must have a current display style that would benefit
				return rdisplayswap.test( jQuery.css( elem, "display" ) ) &&

					// Support: Safari 8+
					// Table columns in Safari have non-zero offsetWidth & zero
					// getBoundingClientRect().width unless display is changed.
					// Support: IE <=11 only
					// Running getBoundingClientRect on a disconnected node
					// in IE throws an error.
					( !elem.getClientRects().length || !elem.getBoundingClientRect().width ) ?
						swap( elem, cssShow, function() {
							return getWidthOrHeight( elem, name, extra );
						} ) :
						getWidthOrHeight( elem, name, extra );
			}
		},

		set: function( elem, value, extra ) {
			var matches,
				styles = extra && getStyles( elem ),
				subtract = extra && augmentWidthOrHeight(
					elem,
					name,
					extra,
					jQuery.css( elem, "boxSizing", false, styles ) === "border-box",
					styles
				);

			// Convert to pixels if value adjustment is needed
			if ( subtract && ( matches = rcssNum.exec( value ) ) &&
				( matches[ 3 ] || "px" ) !== "px" ) {

				elem.style[ name ] = value;
				value = jQuery.css( elem, name );
			}

			return setPositiveNumber( elem, value, subtract );
		}
	};
} );

jQuery.cssHooks.marginLeft = addGetHookIf( support.reliableMarginLeft,
	function( elem, computed ) {
		if ( computed ) {
			return ( parseFloat( curCSS( elem, "marginLeft" ) ) ||
				elem.getBoundingClientRect().left -
					swap( elem, { marginLeft: 0 }, function() {
						return elem.getBoundingClientRect().left;
					} )
				) + "px";
		}
	}
);

// These hooks are used by animate to expand properties
jQuery.each( {
	margin: "",
	padding: "",
	border: "Width"
}, function( prefix, suffix ) {
	jQuery.cssHooks[ prefix + suffix ] = {
		expand: function( value ) {
			var i = 0,
				expanded = {},

				// Assumes a single number if not a string
				parts = typeof value === "string" ? value.split( " " ) : [ value ];

			for ( ; i < 4; i++ ) {
				expanded[ prefix + cssExpand[ i ] + suffix ] =
					parts[ i ] || parts[ i - 2 ] || parts[ 0 ];
			}

			return expanded;
		}
	};

	if ( !rmargin.test( prefix ) ) {
		jQuery.cssHooks[ prefix + suffix ].set = setPositiveNumber;
	}
} );

jQuery.fn.extend( {
	css: function( name, value ) {
		return access( this, function( elem, name, value ) {
			var styles, len,
				map = {},
				i = 0;

			if ( Array.isArray( name ) ) {
				styles = getStyles( elem );
				len = name.length;

				for ( ; i < len; i++ ) {
					map[ name[ i ] ] = jQuery.css( elem, name[ i ], false, styles );
				}

				return map;
			}

			return value !== undefined ?
				jQuery.style( elem, name, value ) :
				jQuery.css( elem, name );
		}, name, value, arguments.length > 1 );
	}
} );


function Tween( elem, options, prop, end, easing ) {
	return new Tween.prototype.init( elem, options, prop, end, easing );
}
jQuery.Tween = Tween;

Tween.prototype = {
	constructor: Tween,
	init: function( elem, options, prop, end, easing, unit ) {
		this.elem = elem;
		this.prop = prop;
		this.easing = easing || jQuery.easing._default;
		this.options = options;
		this.start = this.now = this.cur();
		this.end = end;
		this.unit = unit || ( jQuery.cssNumber[ prop ] ? "" : "px" );
	},
	cur: function() {
		var hooks = Tween.propHooks[ this.prop ];

		return hooks && hooks.get ?
			hooks.get( this ) :
			Tween.propHooks._default.get( this );
	},
	run: function( percent ) {
		var eased,
			hooks = Tween.propHooks[ this.prop ];

		if ( this.options.duration ) {
			this.pos = eased = jQuery.easing[ this.easing ](
				percent, this.options.duration * percent, 0, 1, this.options.duration
			);
		} else {
			this.pos = eased = percent;
		}
		this.now = ( this.end - this.start ) * eased + this.start;

		if ( this.options.step ) {
			this.options.step.call( this.elem, this.now, this );
		}

		if ( hooks && hooks.set ) {
			hooks.set( this );
		} else {
			Tween.propHooks._default.set( this );
		}
		return this;
	}
};

Tween.prototype.init.prototype = Tween.prototype;

Tween.propHooks = {
	_default: {
		get: function( tween ) {
			var result;

			// Use a property on the element directly when it is not a DOM element,
			// or when there is no matching style property that exists.
			if ( tween.elem.nodeType !== 1 ||
				tween.elem[ tween.prop ] != null && tween.elem.style[ tween.prop ] == null ) {
				return tween.elem[ tween.prop ];
			}

			// Passing an empty string as a 3rd parameter to .css will automatically
			// attempt a parseFloat and fallback to a string if the parse fails.
			// Simple values such as "10px" are parsed to Float;
			// complex values such as "rotate(1rad)" are returned as-is.
			result = jQuery.css( tween.elem, tween.prop, "" );

			// Empty strings, null, undefined and "auto" are converted to 0.
			return !result || result === "auto" ? 0 : result;
		},
		set: function( tween ) {

			// Use step hook for back compat.
			// Use cssHook if its there.
			// Use .style if available and use plain properties where available.
			if ( jQuery.fx.step[ tween.prop ] ) {
				jQuery.fx.step[ tween.prop ]( tween );
			} else if ( tween.elem.nodeType === 1 &&
				( tween.elem.style[ jQuery.cssProps[ tween.prop ] ] != null ||
					jQuery.cssHooks[ tween.prop ] ) ) {
				jQuery.style( tween.elem, tween.prop, tween.now + tween.unit );
			} else {
				tween.elem[ tween.prop ] = tween.now;
			}
		}
	}
};

// Support: IE <=9 only
// Panic based approach to setting things on disconnected nodes
Tween.propHooks.scrollTop = Tween.propHooks.scrollLeft = {
	set: function( tween ) {
		if ( tween.elem.nodeType && tween.elem.parentNode ) {
			tween.elem[ tween.prop ] = tween.now;
		}
	}
};

jQuery.easing = {
	linear: function( p ) {
		return p;
	},
	swing: function( p ) {
		return 0.5 - Math.cos( p * Math.PI ) / 2;
	},
	_default: "swing"
};

jQuery.fx = Tween.prototype.init;

// Back compat <1.8 extension point
jQuery.fx.step = {};




var
	fxNow, inProgress,
	rfxtypes = /^(?:toggle|show|hide)$/,
	rrun = /queueHooks$/;

function schedule() {
	if ( inProgress ) {
		if ( document.hidden === false && window.requestAnimationFrame ) {
			window.requestAnimationFrame( schedule );
		} else {
			window.setTimeout( schedule, jQuery.fx.interval );
		}

		jQuery.fx.tick();
	}
}

// Animations created synchronously will run synchronously
function createFxNow() {
	window.setTimeout( function() {
		fxNow = undefined;
	} );
	return ( fxNow = jQuery.now() );
}

// Generate parameters to create a standard animation
function genFx( type, includeWidth ) {
	var which,
		i = 0,
		attrs = { height: type };

	// If we include width, step value is 1 to do all cssExpand values,
	// otherwise step value is 2 to skip over Left and Right
	includeWidth = includeWidth ? 1 : 0;
	for ( ; i < 4; i += 2 - includeWidth ) {
		which = cssExpand[ i ];
		attrs[ "margin" + which ] = attrs[ "padding" + which ] = type;
	}

	if ( includeWidth ) {
		attrs.opacity = attrs.width = type;
	}

	return attrs;
}

function createTween( value, prop, animation ) {
	var tween,
		collection = ( Animation.tweeners[ prop ] || [] ).concat( Animation.tweeners[ "*" ] ),
		index = 0,
		length = collection.length;
	for ( ; index < length; index++ ) {
		if ( ( tween = collection[ index ].call( animation, prop, value ) ) ) {

			// We're done with this property
			return tween;
		}
	}
}

function defaultPrefilter( elem, props, opts ) {
	var prop, value, toggle, hooks, oldfire, propTween, restoreDisplay, display,
		isBox = "width" in props || "height" in props,
		anim = this,
		orig = {},
		style = elem.style,
		hidden = elem.nodeType && isHiddenWithinTree( elem ),
		dataShow = dataPriv.get( elem, "fxshow" );

	// Queue-skipping animations hijack the fx hooks
	if ( !opts.queue ) {
		hooks = jQuery._queueHooks( elem, "fx" );
		if ( hooks.unqueued == null ) {
			hooks.unqueued = 0;
			oldfire = hooks.empty.fire;
			hooks.empty.fire = function() {
				if ( !hooks.unqueued ) {
					oldfire();
				}
			};
		}
		hooks.unqueued++;

		anim.always( function() {

			// Ensure the complete handler is called before this completes
			anim.always( function() {
				hooks.unqueued--;
				if ( !jQuery.queue( elem, "fx" ).length ) {
					hooks.empty.fire();
				}
			} );
		} );
	}

	// Detect show/hide animations
	for ( prop in props ) {
		value = props[ prop ];
		if ( rfxtypes.test( value ) ) {
			delete props[ prop ];
			toggle = toggle || value === "toggle";
			if ( value === ( hidden ? "hide" : "show" ) ) {

				// Pretend to be hidden if this is a "show" and
				// there is still data from a stopped show/hide
				if ( value === "show" && dataShow && dataShow[ prop ] !== undefined ) {
					hidden = true;

				// Ignore all other no-op show/hide data
				} else {
					continue;
				}
			}
			orig[ prop ] = dataShow && dataShow[ prop ] || jQuery.style( elem, prop );
		}
	}

	// Bail out if this is a no-op like .hide().hide()
	propTween = !jQuery.isEmptyObject( props );
	if ( !propTween && jQuery.isEmptyObject( orig ) ) {
		return;
	}

	// Restrict "overflow" and "display" styles during box animations
	if ( isBox && elem.nodeType === 1 ) {

		// Support: IE <=9 - 11, Edge 12 - 13
		// Record all 3 overflow attributes because IE does not infer the shorthand
		// from identically-valued overflowX and overflowY
		opts.overflow = [ style.overflow, style.overflowX, style.overflowY ];

		// Identify a display type, preferring old show/hide data over the CSS cascade
		restoreDisplay = dataShow && dataShow.display;
		if ( restoreDisplay == null ) {
			restoreDisplay = dataPriv.get( elem, "display" );
		}
		display = jQuery.css( elem, "display" );
		if ( display === "none" ) {
			if ( restoreDisplay ) {
				display = restoreDisplay;
			} else {

				// Get nonempty value(s) by temporarily forcing visibility
				showHide( [ elem ], true );
				restoreDisplay = elem.style.display || restoreDisplay;
				display = jQuery.css( elem, "display" );
				showHide( [ elem ] );
			}
		}

		// Animate inline elements as inline-block
		if ( display === "inline" || display === "inline-block" && restoreDisplay != null ) {
			if ( jQuery.css( elem, "float" ) === "none" ) {

				// Restore the original display value at the end of pure show/hide animations
				if ( !propTween ) {
					anim.done( function() {
						style.display = restoreDisplay;
					} );
					if ( restoreDisplay == null ) {
						display = style.display;
						restoreDisplay = display === "none" ? "" : display;
					}
				}
				style.display = "inline-block";
			}
		}
	}

	if ( opts.overflow ) {
		style.overflow = "hidden";
		anim.always( function() {
			style.overflow = opts.overflow[ 0 ];
			style.overflowX = opts.overflow[ 1 ];
			style.overflowY = opts.overflow[ 2 ];
		} );
	}

	// Implement show/hide animations
	propTween = false;
	for ( prop in orig ) {

		// General show/hide setup for this element animation
		if ( !propTween ) {
			if ( dataShow ) {
				if ( "hidden" in dataShow ) {
					hidden = dataShow.hidden;
				}
			} else {
				dataShow = dataPriv.access( elem, "fxshow", { display: restoreDisplay } );
			}

			// Store hidden/visible for toggle so `.stop().toggle()` "reverses"
			if ( toggle ) {
				dataShow.hidden = !hidden;
			}

			// Show elements before animating them
			if ( hidden ) {
				showHide( [ elem ], true );
			}

			/* eslint-disable no-loop-func */

			anim.done( function() {

			/* eslint-enable no-loop-func */

				// The final step of a "hide" animation is actually hiding the element
				if ( !hidden ) {
					showHide( [ elem ] );
				}
				dataPriv.remove( elem, "fxshow" );
				for ( prop in orig ) {
					jQuery.style( elem, prop, orig[ prop ] );
				}
			} );
		}

		// Per-property setup
		propTween = createTween( hidden ? dataShow[ prop ] : 0, prop, anim );
		if ( !( prop in dataShow ) ) {
			dataShow[ prop ] = propTween.start;
			if ( hidden ) {
				propTween.end = propTween.start;
				propTween.start = 0;
			}
		}
	}
}

function propFilter( props, specialEasing ) {
	var index, name, easing, value, hooks;

	// camelCase, specialEasing and expand cssHook pass
	for ( index in props ) {
		name = jQuery.camelCase( index );
		easing = specialEasing[ name ];
		value = props[ index ];
		if ( Array.isArray( value ) ) {
			easing = value[ 1 ];
			value = props[ index ] = value[ 0 ];
		}

		if ( index !== name ) {
			props[ name ] = value;
			delete props[ index ];
		}

		hooks = jQuery.cssHooks[ name ];
		if ( hooks && "expand" in hooks ) {
			value = hooks.expand( value );
			delete props[ name ];

			// Not quite $.extend, this won't overwrite existing keys.
			// Reusing 'index' because we have the correct "name"
			for ( index in value ) {
				if ( !( index in props ) ) {
					props[ index ] = value[ index ];
					specialEasing[ index ] = easing;
				}
			}
		} else {
			specialEasing[ name ] = easing;
		}
	}
}

function Animation( elem, properties, options ) {
	var result,
		stopped,
		index = 0,
		length = Animation.prefilters.length,
		deferred = jQuery.Deferred().always( function() {

			// Don't match elem in the :animated selector
			delete tick.elem;
		} ),
		tick = function() {
			if ( stopped ) {
				return false;
			}
			var currentTime = fxNow || createFxNow(),
				remaining = Math.max( 0, animation.startTime + animation.duration - currentTime ),

				// Support: Android 2.3 only
				// Archaic crash bug won't allow us to use `1 - ( 0.5 || 0 )` (#12497)
				temp = remaining / animation.duration || 0,
				percent = 1 - temp,
				index = 0,
				length = animation.tweens.length;

			for ( ; index < length; index++ ) {
				animation.tweens[ index ].run( percent );
			}

			deferred.notifyWith( elem, [ animation, percent, remaining ] );

			// If there's more to do, yield
			if ( percent < 1 && length ) {
				return remaining;
			}

			// If this was an empty animation, synthesize a final progress notification
			if ( !length ) {
				deferred.notifyWith( elem, [ animation, 1, 0 ] );
			}

			// Resolve the animation and report its conclusion
			deferred.resolveWith( elem, [ animation ] );
			return false;
		},
		animation = deferred.promise( {
			elem: elem,
			props: jQuery.extend( {}, properties ),
			opts: jQuery.extend( true, {
				specialEasing: {},
				easing: jQuery.easing._default
			}, options ),
			originalProperties: properties,
			originalOptions: options,
			startTime: fxNow || createFxNow(),
			duration: options.duration,
			tweens: [],
			createTween: function( prop, end ) {
				var tween = jQuery.Tween( elem, animation.opts, prop, end,
						animation.opts.specialEasing[ prop ] || animation.opts.easing );
				animation.tweens.push( tween );
				return tween;
			},
			stop: function( gotoEnd ) {
				var index = 0,

					// If we are going to the end, we want to run all the tweens
					// otherwise we skip this part
					length = gotoEnd ? animation.tweens.length : 0;
				if ( stopped ) {
					return this;
				}
				stopped = true;
				for ( ; index < length; index++ ) {
					animation.tweens[ index ].run( 1 );
				}

				// Resolve when we played the last frame; otherwise, reject
				if ( gotoEnd ) {
					deferred.notifyWith( elem, [ animation, 1, 0 ] );
					deferred.resolveWith( elem, [ animation, gotoEnd ] );
				} else {
					deferred.rejectWith( elem, [ animation, gotoEnd ] );
				}
				return this;
			}
		} ),
		props = animation.props;

	propFilter( props, animation.opts.specialEasing );

	for ( ; index < length; index++ ) {
		result = Animation.prefilters[ index ].call( animation, elem, props, animation.opts );
		if ( result ) {
			if ( jQuery.isFunction( result.stop ) ) {
				jQuery._queueHooks( animation.elem, animation.opts.queue ).stop =
					jQuery.proxy( result.stop, result );
			}
			return result;
		}
	}

	jQuery.map( props, createTween, animation );

	if ( jQuery.isFunction( animation.opts.start ) ) {
		animation.opts.start.call( elem, animation );
	}

	// Attach callbacks from options
	animation
		.progress( animation.opts.progress )
		.done( animation.opts.done, animation.opts.complete )
		.fail( animation.opts.fail )
		.always( animation.opts.always );

	jQuery.fx.timer(
		jQuery.extend( tick, {
			elem: elem,
			anim: animation,
			queue: animation.opts.queue
		} )
	);

	return animation;
}

jQuery.Animation = jQuery.extend( Animation, {

	tweeners: {
		"*": [ function( prop, value ) {
			var tween = this.createTween( prop, value );
			adjustCSS( tween.elem, prop, rcssNum.exec( value ), tween );
			return tween;
		} ]
	},

	tweener: function( props, callback ) {
		if ( jQuery.isFunction( props ) ) {
			callback = props;
			props = [ "*" ];
		} else {
			props = props.match( rnothtmlwhite );
		}

		var prop,
			index = 0,
			length = props.length;

		for ( ; index < length; index++ ) {
			prop = props[ index ];
			Animation.tweeners[ prop ] = Animation.tweeners[ prop ] || [];
			Animation.tweeners[ prop ].unshift( callback );
		}
	},

	prefilters: [ defaultPrefilter ],

	prefilter: function( callback, prepend ) {
		if ( prepend ) {
			Animation.prefilters.unshift( callback );
		} else {
			Animation.prefilters.push( callback );
		}
	}
} );

jQuery.speed = function( speed, easing, fn ) {
	var opt = speed && typeof speed === "object" ? jQuery.extend( {}, speed ) : {
		complete: fn || !fn && easing ||
			jQuery.isFunction( speed ) && speed,
		duration: speed,
		easing: fn && easing || easing && !jQuery.isFunction( easing ) && easing
	};

	// Go to the end state if fx are off
	if ( jQuery.fx.off ) {
		opt.duration = 0;

	} else {
		if ( typeof opt.duration !== "number" ) {
			if ( opt.duration in jQuery.fx.speeds ) {
				opt.duration = jQuery.fx.speeds[ opt.duration ];

			} else {
				opt.duration = jQuery.fx.speeds._default;
			}
		}
	}

	// Normalize opt.queue - true/undefined/null -> "fx"
	if ( opt.queue == null || opt.queue === true ) {
		opt.queue = "fx";
	}

	// Queueing
	opt.old = opt.complete;

	opt.complete = function() {
		if ( jQuery.isFunction( opt.old ) ) {
			opt.old.call( this );
		}

		if ( opt.queue ) {
			jQuery.dequeue( this, opt.queue );
		}
	};

	return opt;
};

jQuery.fn.extend( {
	fadeTo: function( speed, to, easing, callback ) {

		// Show any hidden elements after setting opacity to 0
		return this.filter( isHiddenWithinTree ).css( "opacity", 0 ).show()

			// Animate to the value specified
			.end().animate( { opacity: to }, speed, easing, callback );
	},
	animate: function( prop, speed, easing, callback ) {
		var empty = jQuery.isEmptyObject( prop ),
			optall = jQuery.speed( speed, easing, callback ),
			doAnimation = function() {

				// Operate on a copy of prop so per-property easing won't be lost
				var anim = Animation( this, jQuery.extend( {}, prop ), optall );

				// Empty animations, or finishing resolves immediately
				if ( empty || dataPriv.get( this, "finish" ) ) {
					anim.stop( true );
				}
			};
			doAnimation.finish = doAnimation;

		return empty || optall.queue === false ?
			this.each( doAnimation ) :
			this.queue( optall.queue, doAnimation );
	},
	stop: function( type, clearQueue, gotoEnd ) {
		var stopQueue = function( hooks ) {
			var stop = hooks.stop;
			delete hooks.stop;
			stop( gotoEnd );
		};

		if ( typeof type !== "string" ) {
			gotoEnd = clearQueue;
			clearQueue = type;
			type = undefined;
		}
		if ( clearQueue && type !== false ) {
			this.queue( type || "fx", [] );
		}

		return this.each( function() {
			var dequeue = true,
				index = type != null && type + "queueHooks",
				timers = jQuery.timers,
				data = dataPriv.get( this );

			if ( index ) {
				if ( data[ index ] && data[ index ].stop ) {
					stopQueue( data[ index ] );
				}
			} else {
				for ( index in data ) {
					if ( data[ index ] && data[ index ].stop && rrun.test( index ) ) {
						stopQueue( data[ index ] );
					}
				}
			}

			for ( index = timers.length; index--; ) {
				if ( timers[ index ].elem === this &&
					( type == null || timers[ index ].queue === type ) ) {

					timers[ index ].anim.stop( gotoEnd );
					dequeue = false;
					timers.splice( index, 1 );
				}
			}

			// Start the next in the queue if the last step wasn't forced.
			// Timers currently will call their complete callbacks, which
			// will dequeue but only if they were gotoEnd.
			if ( dequeue || !gotoEnd ) {
				jQuery.dequeue( this, type );
			}
		} );
	},
	finish: function( type ) {
		if ( type !== false ) {
			type = type || "fx";
		}
		return this.each( function() {
			var index,
				data = dataPriv.get( this ),
				queue = data[ type + "queue" ],
				hooks = data[ type + "queueHooks" ],
				timers = jQuery.timers,
				length = queue ? queue.length : 0;

			// Enable finishing flag on private data
			data.finish = true;

			// Empty the queue first
			jQuery.queue( this, type, [] );

			if ( hooks && hooks.stop ) {
				hooks.stop.call( this, true );
			}

			// Look for any active animations, and finish them
			for ( index = timers.length; index--; ) {
				if ( timers[ index ].elem === this && timers[ index ].queue === type ) {
					timers[ index ].anim.stop( true );
					timers.splice( index, 1 );
				}
			}

			// Look for any animations in the old queue and finish them
			for ( index = 0; index < length; index++ ) {
				if ( queue[ index ] && queue[ index ].finish ) {
					queue[ index ].finish.call( this );
				}
			}

			// Turn off finishing flag
			delete data.finish;
		} );
	}
} );

jQuery.each( [ "toggle", "show", "hide" ], function( i, name ) {
	var cssFn = jQuery.fn[ name ];
	jQuery.fn[ name ] = function( speed, easing, callback ) {
		return speed == null || typeof speed === "boolean" ?
			cssFn.apply( this, arguments ) :
			this.animate( genFx( name, true ), speed, easing, callback );
	};
} );

// Generate shortcuts for custom animations
jQuery.each( {
	slideDown: genFx( "show" ),
	slideUp: genFx( "hide" ),
	slideToggle: genFx( "toggle" ),
	fadeIn: { opacity: "show" },
	fadeOut: { opacity: "hide" },
	fadeToggle: { opacity: "toggle" }
}, function( name, props ) {
	jQuery.fn[ name ] = function( speed, easing, callback ) {
		return this.animate( props, speed, easing, callback );
	};
} );

jQuery.timers = [];
jQuery.fx.tick = function() {
	var timer,
		i = 0,
		timers = jQuery.timers;

	fxNow = jQuery.now();

	for ( ; i < timers.length; i++ ) {
		timer = timers[ i ];

		// Run the timer and safely remove it when done (allowing for external removal)
		if ( !timer() && timers[ i ] === timer ) {
			timers.splice( i--, 1 );
		}
	}

	if ( !timers.length ) {
		jQuery.fx.stop();
	}
	fxNow = undefined;
};

jQuery.fx.timer = function( timer ) {
	jQuery.timers.push( timer );
	jQuery.fx.start();
};

jQuery.fx.interval = 13;
jQuery.fx.start = function() {
	if ( inProgress ) {
		return;
	}

	inProgress = true;
	schedule();
};

jQuery.fx.stop = function() {
	inProgress = null;
};

jQuery.fx.speeds = {
	slow: 600,
	fast: 200,

	// Default speed
	_default: 400
};


// Based off of the plugin by Clint Helfers, with permission.
// https://web.archive.org/web/20100324014747/http://blindsignals.com/index.php/2009/07/jquery-delay/
jQuery.fn.delay = function( time, type ) {
	time = jQuery.fx ? jQuery.fx.speeds[ time ] || time : time;
	type = type || "fx";

	return this.queue( type, function( next, hooks ) {
		var timeout = window.setTimeout( next, time );
		hooks.stop = function() {
			window.clearTimeout( timeout );
		};
	} );
};


( function() {
	var input = document.createElement( "input" ),
		select = document.createElement( "select" ),
		opt = select.appendChild( document.createElement( "option" ) );

	input.type = "checkbox";

	// Support: Android <=4.3 only
	// Default value for a checkbox should be "on"
	support.checkOn = input.value !== "";

	// Support: IE <=11 only
	// Must access selectedIndex to make default options select
	support.optSelected = opt.selected;

	// Support: IE <=11 only
	// An input loses its value after becoming a radio
	input = document.createElement( "input" );
	input.value = "t";
	input.type = "radio";
	support.radioValue = input.value === "t";
} )();


var boolHook,
	attrHandle = jQuery.expr.attrHandle;

jQuery.fn.extend( {
	attr: function( name, value ) {
		return access( this, jQuery.attr, name, value, arguments.length > 1 );
	},

	removeAttr: function( name ) {
		return this.each( function() {
			jQuery.removeAttr( this, name );
		} );
	}
} );

jQuery.extend( {
	attr: function( elem, name, value ) {
		var ret, hooks,
			nType = elem.nodeType;

		// Don't get/set attributes on text, comment and attribute nodes
		if ( nType === 3 || nType === 8 || nType === 2 ) {
			return;
		}

		// Fallback to prop when attributes are not supported
		if ( typeof elem.getAttribute === "undefined" ) {
			return jQuery.prop( elem, name, value );
		}

		// Attribute hooks are determined by the lowercase version
		// Grab necessary hook if one is defined
		if ( nType !== 1 || !jQuery.isXMLDoc( elem ) ) {
			hooks = jQuery.attrHooks[ name.toLowerCase() ] ||
				( jQuery.expr.match.bool.test( name ) ? boolHook : undefined );
		}

		if ( value !== undefined ) {
			if ( value === null ) {
				jQuery.removeAttr( elem, name );
				return;
			}

			if ( hooks && "set" in hooks &&
				( ret = hooks.set( elem, value, name ) ) !== undefined ) {
				return ret;
			}

			elem.setAttribute( name, value + "" );
			return value;
		}

		if ( hooks && "get" in hooks && ( ret = hooks.get( elem, name ) ) !== null ) {
			return ret;
		}

		ret = jQuery.find.attr( elem, name );

		// Non-existent attributes return null, we normalize to undefined
		return ret == null ? undefined : ret;
	},

	attrHooks: {
		type: {
			set: function( elem, value ) {
				if ( !support.radioValue && value === "radio" &&
					nodeName( elem, "input" ) ) {
					var val = elem.value;
					elem.setAttribute( "type", value );
					if ( val ) {
						elem.value = val;
					}
					return value;
				}
			}
		}
	},

	removeAttr: function( elem, value ) {
		var name,
			i = 0,

			// Attribute names can contain non-HTML whitespace characters
			// https://html.spec.whatwg.org/multipage/syntax.html#attributes-2
			attrNames = value && value.match( rnothtmlwhite );

		if ( attrNames && elem.nodeType === 1 ) {
			while ( ( name = attrNames[ i++ ] ) ) {
				elem.removeAttribute( name );
			}
		}
	}
} );

// Hooks for boolean attributes
boolHook = {
	set: function( elem, value, name ) {
		if ( value === false ) {

			// Remove boolean attributes when set to false
			jQuery.removeAttr( elem, name );
		} else {
			elem.setAttribute( name, name );
		}
		return name;
	}
};

jQuery.each( jQuery.expr.match.bool.source.match( /\w+/g ), function( i, name ) {
	var getter = attrHandle[ name ] || jQuery.find.attr;

	attrHandle[ name ] = function( elem, name, isXML ) {
		var ret, handle,
			lowercaseName = name.toLowerCase();

		if ( !isXML ) {

			// Avoid an infinite loop by temporarily removing this function from the getter
			handle = attrHandle[ lowercaseName ];
			attrHandle[ lowercaseName ] = ret;
			ret = getter( elem, name, isXML ) != null ?
				lowercaseName :
				null;
			attrHandle[ lowercaseName ] = handle;
		}
		return ret;
	};
} );




var rfocusable = /^(?:input|select|textarea|button)$/i,
	rclickable = /^(?:a|area)$/i;

jQuery.fn.extend( {
	prop: function( name, value ) {
		return access( this, jQuery.prop, name, value, arguments.length > 1 );
	},

	removeProp: function( name ) {
		return this.each( function() {
			delete this[ jQuery.propFix[ name ] || name ];
		} );
	}
} );

jQuery.extend( {
	prop: function( elem, name, value ) {
		var ret, hooks,
			nType = elem.nodeType;

		// Don't get/set properties on text, comment and attribute nodes
		if ( nType === 3 || nType === 8 || nType === 2 ) {
			return;
		}

		if ( nType !== 1 || !jQuery.isXMLDoc( elem ) ) {

			// Fix name and attach hooks
			name = jQuery.propFix[ name ] || name;
			hooks = jQuery.propHooks[ name ];
		}

		if ( value !== undefined ) {
			if ( hooks && "set" in hooks &&
				( ret = hooks.set( elem, value, name ) ) !== undefined ) {
				return ret;
			}

			return ( elem[ name ] = value );
		}

		if ( hooks && "get" in hooks && ( ret = hooks.get( elem, name ) ) !== null ) {
			return ret;
		}

		return elem[ name ];
	},

	propHooks: {
		tabIndex: {
			get: function( elem ) {

				// Support: IE <=9 - 11 only
				// elem.tabIndex doesn't always return the
				// correct value when it hasn't been explicitly set
				// https://web.archive.org/web/20141116233347/http://fluidproject.org/blog/2008/01/09/getting-setting-and-removing-tabindex-values-with-javascript/
				// Use proper attribute retrieval(#12072)
				var tabindex = jQuery.find.attr( elem, "tabindex" );

				if ( tabindex ) {
					return parseInt( tabindex, 10 );
				}

				if (
					rfocusable.test( elem.nodeName ) ||
					rclickable.test( elem.nodeName ) &&
					elem.href
				) {
					return 0;
				}

				return -1;
			}
		}
	},

	propFix: {
		"for": "htmlFor",
		"class": "className"
	}
} );

// Support: IE <=11 only
// Accessing the selectedIndex property
// forces the browser to respect setting selected
// on the option
// The getter ensures a default option is selected
// when in an optgroup
// eslint rule "no-unused-expressions" is disabled for this code
// since it considers such accessions noop
if ( !support.optSelected ) {
	jQuery.propHooks.selected = {
		get: function( elem ) {

			/* eslint no-unused-expressions: "off" */

			var parent = elem.parentNode;
			if ( parent && parent.parentNode ) {
				parent.parentNode.selectedIndex;
			}
			return null;
		},
		set: function( elem ) {

			/* eslint no-unused-expressions: "off" */

			var parent = elem.parentNode;
			if ( parent ) {
				parent.selectedIndex;

				if ( parent.parentNode ) {
					parent.parentNode.selectedIndex;
				}
			}
		}
	};
}

jQuery.each( [
	"tabIndex",
	"readOnly",
	"maxLength",
	"cellSpacing",
	"cellPadding",
	"rowSpan",
	"colSpan",
	"useMap",
	"frameBorder",
	"contentEditable"
], function() {
	jQuery.propFix[ this.toLowerCase() ] = this;
} );




	// Strip and collapse whitespace according to HTML spec
	// https://html.spec.whatwg.org/multipage/infrastructure.html#strip-and-collapse-whitespace
	function stripAndCollapse( value ) {
		var tokens = value.match( rnothtmlwhite ) || [];
		return tokens.join( " " );
	}


function getClass( elem ) {
	return elem.getAttribute && elem.getAttribute( "class" ) || "";
}

jQuery.fn.extend( {
	addClass: function( value ) {
		var classes, elem, cur, curValue, clazz, j, finalValue,
			i = 0;

		if ( jQuery.isFunction( value ) ) {
			return this.each( function( j ) {
				jQuery( this ).addClass( value.call( this, j, getClass( this ) ) );
			} );
		}

		if ( typeof value === "string" && value ) {
			classes = value.match( rnothtmlwhite ) || [];

			while ( ( elem = this[ i++ ] ) ) {
				curValue = getClass( elem );
				cur = elem.nodeType === 1 && ( " " + stripAndCollapse( curValue ) + " " );

				if ( cur ) {
					j = 0;
					while ( ( clazz = classes[ j++ ] ) ) {
						if ( cur.indexOf( " " + clazz + " " ) < 0 ) {
							cur += clazz + " ";
						}
					}

					// Only assign if different to avoid unneeded rendering.
					finalValue = stripAndCollapse( cur );
					if ( curValue !== finalValue ) {
						elem.setAttribute( "class", finalValue );
					}
				}
			}
		}

		return this;
	},

	removeClass: function( value ) {
		var classes, elem, cur, curValue, clazz, j, finalValue,
			i = 0;

		if ( jQuery.isFunction( value ) ) {
			return this.each( function( j ) {
				jQuery( this ).removeClass( value.call( this, j, getClass( this ) ) );
			} );
		}

		if ( !arguments.length ) {
			return this.attr( "class", "" );
		}

		if ( typeof value === "string" && value ) {
			classes = value.match( rnothtmlwhite ) || [];

			while ( ( elem = this[ i++ ] ) ) {
				curValue = getClass( elem );

				// This expression is here for better compressibility (see addClass)
				cur = elem.nodeType === 1 && ( " " + stripAndCollapse( curValue ) + " " );

				if ( cur ) {
					j = 0;
					while ( ( clazz = classes[ j++ ] ) ) {

						// Remove *all* instances
						while ( cur.indexOf( " " + clazz + " " ) > -1 ) {
							cur = cur.replace( " " + clazz + " ", " " );
						}
					}

					// Only assign if different to avoid unneeded rendering.
					finalValue = stripAndCollapse( cur );
					if ( curValue !== finalValue ) {
						elem.setAttribute( "class", finalValue );
					}
				}
			}
		}

		return this;
	},

	toggleClass: function( value, stateVal ) {
		var type = typeof value;

		if ( typeof stateVal === "boolean" && type === "string" ) {
			return stateVal ? this.addClass( value ) : this.removeClass( value );
		}

		if ( jQuery.isFunction( value ) ) {
			return this.each( function( i ) {
				jQuery( this ).toggleClass(
					value.call( this, i, getClass( this ), stateVal ),
					stateVal
				);
			} );
		}

		return this.each( function() {
			var className, i, self, classNames;

			if ( type === "string" ) {

				// Toggle individual class names
				i = 0;
				self = jQuery( this );
				classNames = value.match( rnothtmlwhite ) || [];

				while ( ( className = classNames[ i++ ] ) ) {

					// Check each className given, space separated list
					if ( self.hasClass( className ) ) {
						self.removeClass( className );
					} else {
						self.addClass( className );
					}
				}

			// Toggle whole class name
			} else if ( value === undefined || type === "boolean" ) {
				className = getClass( this );
				if ( className ) {

					// Store className if set
					dataPriv.set( this, "__className__", className );
				}

				// If the element has a class name or if we're passed `false`,
				// then remove the whole classname (if there was one, the above saved it).
				// Otherwise bring back whatever was previously saved (if anything),
				// falling back to the empty string if nothing was stored.
				if ( this.setAttribute ) {
					this.setAttribute( "class",
						className || value === false ?
						"" :
						dataPriv.get( this, "__className__" ) || ""
					);
				}
			}
		} );
	},

	hasClass: function( selector ) {
		var className, elem,
			i = 0;

		className = " " + selector + " ";
		while ( ( elem = this[ i++ ] ) ) {
			if ( elem.nodeType === 1 &&
				( " " + stripAndCollapse( getClass( elem ) ) + " " ).indexOf( className ) > -1 ) {
					return true;
			}
		}

		return false;
	}
} );




var rreturn = /\r/g;

jQuery.fn.extend( {
	val: function( value ) {
		var hooks, ret, isFunction,
			elem = this[ 0 ];

		if ( !arguments.length ) {
			if ( elem ) {
				hooks = jQuery.valHooks[ elem.type ] ||
					jQuery.valHooks[ elem.nodeName.toLowerCase() ];

				if ( hooks &&
					"get" in hooks &&
					( ret = hooks.get( elem, "value" ) ) !== undefined
				) {
					return ret;
				}

				ret = elem.value;

				// Handle most common string cases
				if ( typeof ret === "string" ) {
					return ret.replace( rreturn, "" );
				}

				// Handle cases where value is null/undef or number
				return ret == null ? "" : ret;
			}

			return;
		}

		isFunction = jQuery.isFunction( value );

		return this.each( function( i ) {
			var val;

			if ( this.nodeType !== 1 ) {
				return;
			}

			if ( isFunction ) {
				val = value.call( this, i, jQuery( this ).val() );
			} else {
				val = value;
			}

			// Treat null/undefined as ""; convert numbers to string
			if ( val == null ) {
				val = "";

			} else if ( typeof val === "number" ) {
				val += "";

			} else if ( Array.isArray( val ) ) {
				val = jQuery.map( val, function( value ) {
					return value == null ? "" : value + "";
				} );
			}

			hooks = jQuery.valHooks[ this.type ] || jQuery.valHooks[ this.nodeName.toLowerCase() ];

			// If set returns undefined, fall back to normal setting
			if ( !hooks || !( "set" in hooks ) || hooks.set( this, val, "value" ) === undefined ) {
				this.value = val;
			}
		} );
	}
} );

jQuery.extend( {
	valHooks: {
		option: {
			get: function( elem ) {

				var val = jQuery.find.attr( elem, "value" );
				return val != null ?
					val :

					// Support: IE <=10 - 11 only
					// option.text throws exceptions (#14686, #14858)
					// Strip and collapse whitespace
					// https://html.spec.whatwg.org/#strip-and-collapse-whitespace
					stripAndCollapse( jQuery.text( elem ) );
			}
		},
		select: {
			get: function( elem ) {
				var value, option, i,
					options = elem.options,
					index = elem.selectedIndex,
					one = elem.type === "select-one",
					values = one ? null : [],
					max = one ? index + 1 : options.length;

				if ( index < 0 ) {
					i = max;

				} else {
					i = one ? index : 0;
				}

				// Loop through all the selected options
				for ( ; i < max; i++ ) {
					option = options[ i ];

					// Support: IE <=9 only
					// IE8-9 doesn't update selected after form reset (#2551)
					if ( ( option.selected || i === index ) &&

							// Don't return options that are disabled or in a disabled optgroup
							!option.disabled &&
							( !option.parentNode.disabled ||
								!nodeName( option.parentNode, "optgroup" ) ) ) {

						// Get the specific value for the option
						value = jQuery( option ).val();

						// We don't need an array for one selects
						if ( one ) {
							return value;
						}

						// Multi-Selects return an array
						values.push( value );
					}
				}

				return values;
			},

			set: function( elem, value ) {
				var optionSet, option,
					options = elem.options,
					values = jQuery.makeArray( value ),
					i = options.length;

				while ( i-- ) {
					option = options[ i ];

					/* eslint-disable no-cond-assign */

					if ( option.selected =
						jQuery.inArray( jQuery.valHooks.option.get( option ), values ) > -1
					) {
						optionSet = true;
					}

					/* eslint-enable no-cond-assign */
				}

				// Force browsers to behave consistently when non-matching value is set
				if ( !optionSet ) {
					elem.selectedIndex = -1;
				}
				return values;
			}
		}
	}
} );

// Radios and checkboxes getter/setter
jQuery.each( [ "radio", "checkbox" ], function() {
	jQuery.valHooks[ this ] = {
		set: function( elem, value ) {
			if ( Array.isArray( value ) ) {
				return ( elem.checked = jQuery.inArray( jQuery( elem ).val(), value ) > -1 );
			}
		}
	};
	if ( !support.checkOn ) {
		jQuery.valHooks[ this ].get = function( elem ) {
			return elem.getAttribute( "value" ) === null ? "on" : elem.value;
		};
	}
} );




// Return jQuery for attributes-only inclusion


var rfocusMorph = /^(?:focusinfocus|focusoutblur)$/;

jQuery.extend( jQuery.event, {

	trigger: function( event, data, elem, onlyHandlers ) {

		var i, cur, tmp, bubbleType, ontype, handle, special,
			eventPath = [ elem || document ],
			type = hasOwn.call( event, "type" ) ? event.type : event,
			namespaces = hasOwn.call( event, "namespace" ) ? event.namespace.split( "." ) : [];

		cur = tmp = elem = elem || document;

		// Don't do events on text and comment nodes
		if ( elem.nodeType === 3 || elem.nodeType === 8 ) {
			return;
		}

		// focus/blur morphs to focusin/out; ensure we're not firing them right now
		if ( rfocusMorph.test( type + jQuery.event.triggered ) ) {
			return;
		}

		if ( type.indexOf( "." ) > -1 ) {

			// Namespaced trigger; create a regexp to match event type in handle()
			namespaces = type.split( "." );
			type = namespaces.shift();
			namespaces.sort();
		}
		ontype = type.indexOf( ":" ) < 0 && "on" + type;

		// Caller can pass in a jQuery.Event object, Object, or just an event type string
		event = event[ jQuery.expando ] ?
			event :
			new jQuery.Event( type, typeof event === "object" && event );

		// Trigger bitmask: & 1 for native handlers; & 2 for jQuery (always true)
		event.isTrigger = onlyHandlers ? 2 : 3;
		event.namespace = namespaces.join( "." );
		event.rnamespace = event.namespace ?
			new RegExp( "(^|\\.)" + namespaces.join( "\\.(?:.*\\.|)" ) + "(\\.|$)" ) :
			null;

		// Clean up the event in case it is being reused
		event.result = undefined;
		if ( !event.target ) {
			event.target = elem;
		}

		// Clone any incoming data and prepend the event, creating the handler arg list
		data = data == null ?
			[ event ] :
			jQuery.makeArray( data, [ event ] );

		// Allow special events to draw outside the lines
		special = jQuery.event.special[ type ] || {};
		if ( !onlyHandlers && special.trigger && special.trigger.apply( elem, data ) === false ) {
			return;
		}

		// Determine event propagation path in advance, per W3C events spec (#9951)
		// Bubble up to document, then to window; watch for a global ownerDocument var (#9724)
		if ( !onlyHandlers && !special.noBubble && !jQuery.isWindow( elem ) ) {

			bubbleType = special.delegateType || type;
			if ( !rfocusMorph.test( bubbleType + type ) ) {
				cur = cur.parentNode;
			}
			for ( ; cur; cur = cur.parentNode ) {
				eventPath.push( cur );
				tmp = cur;
			}

			// Only add window if we got to document (e.g., not plain obj or detached DOM)
			if ( tmp === ( elem.ownerDocument || document ) ) {
				eventPath.push( tmp.defaultView || tmp.parentWindow || window );
			}
		}

		// Fire handlers on the event path
		i = 0;
		while ( ( cur = eventPath[ i++ ] ) && !event.isPropagationStopped() ) {

			event.type = i > 1 ?
				bubbleType :
				special.bindType || type;

			// jQuery handler
			handle = ( dataPriv.get( cur, "events" ) || {} )[ event.type ] &&
				dataPriv.get( cur, "handle" );
			if ( handle ) {
				handle.apply( cur, data );
			}

			// Native handler
			handle = ontype && cur[ ontype ];
			if ( handle && handle.apply && acceptData( cur ) ) {
				event.result = handle.apply( cur, data );
				if ( event.result === false ) {
					event.preventDefault();
				}
			}
		}
		event.type = type;

		// If nobody prevented the default action, do it now
		if ( !onlyHandlers && !event.isDefaultPrevented() ) {

			if ( ( !special._default ||
				special._default.apply( eventPath.pop(), data ) === false ) &&
				acceptData( elem ) ) {

				// Call a native DOM method on the target with the same name as the event.
				// Don't do default actions on window, that's where global variables be (#6170)
				if ( ontype && jQuery.isFunction( elem[ type ] ) && !jQuery.isWindow( elem ) ) {

					// Don't re-trigger an onFOO event when we call its FOO() method
					tmp = elem[ ontype ];

					if ( tmp ) {
						elem[ ontype ] = null;
					}

					// Prevent re-triggering of the same event, since we already bubbled it above
					jQuery.event.triggered = type;
					elem[ type ]();
					jQuery.event.triggered = undefined;

					if ( tmp ) {
						elem[ ontype ] = tmp;
					}
				}
			}
		}

		return event.result;
	},

	// Piggyback on a donor event to simulate a different one
	// Used only for `focus(in | out)` events
	simulate: function( type, elem, event ) {
		var e = jQuery.extend(
			new jQuery.Event(),
			event,
			{
				type: type,
				isSimulated: true
			}
		);

		jQuery.event.trigger( e, null, elem );
	}

} );

jQuery.fn.extend( {

	trigger: function( type, data ) {
		return this.each( function() {
			jQuery.event.trigger( type, data, this );
		} );
	},
	triggerHandler: function( type, data ) {
		var elem = this[ 0 ];
		if ( elem ) {
			return jQuery.event.trigger( type, data, elem, true );
		}
	}
} );


jQuery.each( ( "blur focus focusin focusout resize scroll click dblclick " +
	"mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave " +
	"change select submit keydown keypress keyup contextmenu" ).split( " " ),
	function( i, name ) {

	// Handle event binding
	jQuery.fn[ name ] = function( data, fn ) {
		return arguments.length > 0 ?
			this.on( name, null, data, fn ) :
			this.trigger( name );
	};
} );

jQuery.fn.extend( {
	hover: function( fnOver, fnOut ) {
		return this.mouseenter( fnOver ).mouseleave( fnOut || fnOver );
	}
} );




support.focusin = "onfocusin" in window;


// Support: Firefox <=44
// Firefox doesn't have focus(in | out) events
// Related ticket - https://bugzilla.mozilla.org/show_bug.cgi?id=687787
//
// Support: Chrome <=48 - 49, Safari <=9.0 - 9.1
// focus(in | out) events fire after focus & blur events,
// which is spec violation - http://www.w3.org/TR/DOM-Level-3-Events/#events-focusevent-event-order
// Related ticket - https://bugs.chromium.org/p/chromium/issues/detail?id=449857
if ( !support.focusin ) {
	jQuery.each( { focus: "focusin", blur: "focusout" }, function( orig, fix ) {

		// Attach a single capturing handler on the document while someone wants focusin/focusout
		var handler = function( event ) {
			jQuery.event.simulate( fix, event.target, jQuery.event.fix( event ) );
		};

		jQuery.event.special[ fix ] = {
			setup: function() {
				var doc = this.ownerDocument || this,
					attaches = dataPriv.access( doc, fix );

				if ( !attaches ) {
					doc.addEventListener( orig, handler, true );
				}
				dataPriv.access( doc, fix, ( attaches || 0 ) + 1 );
			},
			teardown: function() {
				var doc = this.ownerDocument || this,
					attaches = dataPriv.access( doc, fix ) - 1;

				if ( !attaches ) {
					doc.removeEventListener( orig, handler, true );
					dataPriv.remove( doc, fix );

				} else {
					dataPriv.access( doc, fix, attaches );
				}
			}
		};
	} );
}
var location = window.location;

var nonce = jQuery.now();

var rquery = ( /\?/ );



// Cross-browser xml parsing
jQuery.parseXML = function( data ) {
	var xml;
	if ( !data || typeof data !== "string" ) {
		return null;
	}

	// Support: IE 9 - 11 only
	// IE throws on parseFromString with invalid input.
	try {
		xml = ( new window.DOMParser() ).parseFromString( data, "text/xml" );
	} catch ( e ) {
		xml = undefined;
	}

	if ( !xml || xml.getElementsByTagName( "parsererror" ).length ) {
		jQuery.error( "Invalid XML: " + data );
	}
	return xml;
};


var
	rbracket = /\[\]$/,
	rCRLF = /\r?\n/g,
	rsubmitterTypes = /^(?:submit|button|image|reset|file)$/i,
	rsubmittable = /^(?:input|select|textarea|keygen)/i;

function buildParams( prefix, obj, traditional, add ) {
	var name;

	if ( Array.isArray( obj ) ) {

		// Serialize array item.
		jQuery.each( obj, function( i, v ) {
			if ( traditional || rbracket.test( prefix ) ) {

				// Treat each array item as a scalar.
				add( prefix, v );

			} else {

				// Item is non-scalar (array or object), encode its numeric index.
				buildParams(
					prefix + "[" + ( typeof v === "object" && v != null ? i : "" ) + "]",
					v,
					traditional,
					add
				);
			}
		} );

	} else if ( !traditional && jQuery.type( obj ) === "object" ) {

		// Serialize object item.
		for ( name in obj ) {
			buildParams( prefix + "[" + name + "]", obj[ name ], traditional, add );
		}

	} else {

		// Serialize scalar item.
		add( prefix, obj );
	}
}

// Serialize an array of form elements or a set of
// key/values into a query string
jQuery.param = function( a, traditional ) {
	var prefix,
		s = [],
		add = function( key, valueOrFunction ) {

			// If value is a function, invoke it and use its return value
			var value = jQuery.isFunction( valueOrFunction ) ?
				valueOrFunction() :
				valueOrFunction;

			s[ s.length ] = encodeURIComponent( key ) + "=" +
				encodeURIComponent( value == null ? "" : value );
		};

	// If an array was passed in, assume that it is an array of form elements.
	if ( Array.isArray( a ) || ( a.jquery && !jQuery.isPlainObject( a ) ) ) {

		// Serialize the form elements
		jQuery.each( a, function() {
			add( this.name, this.value );
		} );

	} else {

		// If traditional, encode the "old" way (the way 1.3.2 or older
		// did it), otherwise encode params recursively.
		for ( prefix in a ) {
			buildParams( prefix, a[ prefix ], traditional, add );
		}
	}

	// Return the resulting serialization
	return s.join( "&" );
};

jQuery.fn.extend( {
	serialize: function() {
		return jQuery.param( this.serializeArray() );
	},
	serializeArray: function() {
		return this.map( function() {

			// Can add propHook for "elements" to filter or add form elements
			var elements = jQuery.prop( this, "elements" );
			return elements ? jQuery.makeArray( elements ) : this;
		} )
		.filter( function() {
			var type = this.type;

			// Use .is( ":disabled" ) so that fieldset[disabled] works
			return this.name && !jQuery( this ).is( ":disabled" ) &&
				rsubmittable.test( this.nodeName ) && !rsubmitterTypes.test( type ) &&
				( this.checked || !rcheckableType.test( type ) );
		} )
		.map( function( i, elem ) {
			var val = jQuery( this ).val();

			if ( val == null ) {
				return null;
			}

			if ( Array.isArray( val ) ) {
				return jQuery.map( val, function( val ) {
					return { name: elem.name, value: val.replace( rCRLF, "\r\n" ) };
				} );
			}

			return { name: elem.name, value: val.replace( rCRLF, "\r\n" ) };
		} ).get();
	}
} );


var
	r20 = /%20/g,
	rhash = /#.*$/,
	rantiCache = /([?&])_=[^&]*/,
	rheaders = /^(.*?):[ \t]*([^\r\n]*)$/mg,

	// #7653, #8125, #8152: local protocol detection
	rlocalProtocol = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/,
	rnoContent = /^(?:GET|HEAD)$/,
	rprotocol = /^\/\//,

	/* Prefilters
	 * 1) They are useful to introduce custom dataTypes (see ajax/jsonp.js for an example)
	 * 2) These are called:
	 *    - BEFORE asking for a transport
	 *    - AFTER param serialization (s.data is a string if s.processData is true)
	 * 3) key is the dataType
	 * 4) the catchall symbol "*" can be used
	 * 5) execution will start with transport dataType and THEN continue down to "*" if needed
	 */
	prefilters = {},

	/* Transports bindings
	 * 1) key is the dataType
	 * 2) the catchall symbol "*" can be used
	 * 3) selection will start with transport dataType and THEN go to "*" if needed
	 */
	transports = {},

	// Avoid comment-prolog char sequence (#10098); must appease lint and evade compression
	allTypes = "*/".concat( "*" ),

	// Anchor tag for parsing the document origin
	originAnchor = document.createElement( "a" );
	originAnchor.href = location.href;

// Base "constructor" for jQuery.ajaxPrefilter and jQuery.ajaxTransport
function addToPrefiltersOrTransports( structure ) {

	// dataTypeExpression is optional and defaults to "*"
	return function( dataTypeExpression, func ) {

		if ( typeof dataTypeExpression !== "string" ) {
			func = dataTypeExpression;
			dataTypeExpression = "*";
		}

		var dataType,
			i = 0,
			dataTypes = dataTypeExpression.toLowerCase().match( rnothtmlwhite ) || [];

		if ( jQuery.isFunction( func ) ) {

			// For each dataType in the dataTypeExpression
			while ( ( dataType = dataTypes[ i++ ] ) ) {

				// Prepend if requested
				if ( dataType[ 0 ] === "+" ) {
					dataType = dataType.slice( 1 ) || "*";
					( structure[ dataType ] = structure[ dataType ] || [] ).unshift( func );

				// Otherwise append
				} else {
					( structure[ dataType ] = structure[ dataType ] || [] ).push( func );
				}
			}
		}
	};
}

// Base inspection function for prefilters and transports
function inspectPrefiltersOrTransports( structure, options, originalOptions, jqXHR ) {

	var inspected = {},
		seekingTransport = ( structure === transports );

	function inspect( dataType ) {
		var selected;
		inspected[ dataType ] = true;
		jQuery.each( structure[ dataType ] || [], function( _, prefilterOrFactory ) {
			var dataTypeOrTransport = prefilterOrFactory( options, originalOptions, jqXHR );
			if ( typeof dataTypeOrTransport === "string" &&
				!seekingTransport && !inspected[ dataTypeOrTransport ] ) {

				options.dataTypes.unshift( dataTypeOrTransport );
				inspect( dataTypeOrTransport );
				return false;
			} else if ( seekingTransport ) {
				return !( selected = dataTypeOrTransport );
			}
		} );
		return selected;
	}

	return inspect( options.dataTypes[ 0 ] ) || !inspected[ "*" ] && inspect( "*" );
}

// A special extend for ajax options
// that takes "flat" options (not to be deep extended)
// Fixes #9887
function ajaxExtend( target, src ) {
	var key, deep,
		flatOptions = jQuery.ajaxSettings.flatOptions || {};

	for ( key in src ) {
		if ( src[ key ] !== undefined ) {
			( flatOptions[ key ] ? target : ( deep || ( deep = {} ) ) )[ key ] = src[ key ];
		}
	}
	if ( deep ) {
		jQuery.extend( true, target, deep );
	}

	return target;
}

/* Handles responses to an ajax request:
 * - finds the right dataType (mediates between content-type and expected dataType)
 * - returns the corresponding response
 */
function ajaxHandleResponses( s, jqXHR, responses ) {

	var ct, type, finalDataType, firstDataType,
		contents = s.contents,
		dataTypes = s.dataTypes;

	// Remove auto dataType and get content-type in the process
	while ( dataTypes[ 0 ] === "*" ) {
		dataTypes.shift();
		if ( ct === undefined ) {
			ct = s.mimeType || jqXHR.getResponseHeader( "Content-Type" );
		}
	}

	// Check if we're dealing with a known content-type
	if ( ct ) {
		for ( type in contents ) {
			if ( contents[ type ] && contents[ type ].test( ct ) ) {
				dataTypes.unshift( type );
				break;
			}
		}
	}

	// Check to see if we have a response for the expected dataType
	if ( dataTypes[ 0 ] in responses ) {
		finalDataType = dataTypes[ 0 ];
	} else {

		// Try convertible dataTypes
		for ( type in responses ) {
			if ( !dataTypes[ 0 ] || s.converters[ type + " " + dataTypes[ 0 ] ] ) {
				finalDataType = type;
				break;
			}
			if ( !firstDataType ) {
				firstDataType = type;
			}
		}

		// Or just use first one
		finalDataType = finalDataType || firstDataType;
	}

	// If we found a dataType
	// We add the dataType to the list if needed
	// and return the corresponding response
	if ( finalDataType ) {
		if ( finalDataType !== dataTypes[ 0 ] ) {
			dataTypes.unshift( finalDataType );
		}
		return responses[ finalDataType ];
	}
}

/* Chain conversions given the request and the original response
 * Also sets the responseXXX fields on the jqXHR instance
 */
function ajaxConvert( s, response, jqXHR, isSuccess ) {
	var conv2, current, conv, tmp, prev,
		converters = {},

		// Work with a copy of dataTypes in case we need to modify it for conversion
		dataTypes = s.dataTypes.slice();

	// Create converters map with lowercased keys
	if ( dataTypes[ 1 ] ) {
		for ( conv in s.converters ) {
			converters[ conv.toLowerCase() ] = s.converters[ conv ];
		}
	}

	current = dataTypes.shift();

	// Convert to each sequential dataType
	while ( current ) {

		if ( s.responseFields[ current ] ) {
			jqXHR[ s.responseFields[ current ] ] = response;
		}

		// Apply the dataFilter if provided
		if ( !prev && isSuccess && s.dataFilter ) {
			response = s.dataFilter( response, s.dataType );
		}

		prev = current;
		current = dataTypes.shift();

		if ( current ) {

			// There's only work to do if current dataType is non-auto
			if ( current === "*" ) {

				current = prev;

			// Convert response if prev dataType is non-auto and differs from current
			} else if ( prev !== "*" && prev !== current ) {

				// Seek a direct converter
				conv = converters[ prev + " " + current ] || converters[ "* " + current ];

				// If none found, seek a pair
				if ( !conv ) {
					for ( conv2 in converters ) {

						// If conv2 outputs current
						tmp = conv2.split( " " );
						if ( tmp[ 1 ] === current ) {

							// If prev can be converted to accepted input
							conv = converters[ prev + " " + tmp[ 0 ] ] ||
								converters[ "* " + tmp[ 0 ] ];
							if ( conv ) {

								// Condense equivalence converters
								if ( conv === true ) {
									conv = converters[ conv2 ];

								// Otherwise, insert the intermediate dataType
								} else if ( converters[ conv2 ] !== true ) {
									current = tmp[ 0 ];
									dataTypes.unshift( tmp[ 1 ] );
								}
								break;
							}
						}
					}
				}

				// Apply converter (if not an equivalence)
				if ( conv !== true ) {

					// Unless errors are allowed to bubble, catch and return them
					if ( conv && s.throws ) {
						response = conv( response );
					} else {
						try {
							response = conv( response );
						} catch ( e ) {
							return {
								state: "parsererror",
								error: conv ? e : "No conversion from " + prev + " to " + current
							};
						}
					}
				}
			}
		}
	}

	return { state: "success", data: response };
}

jQuery.extend( {

	// Counter for holding the number of active queries
	active: 0,

	// Last-Modified header cache for next request
	lastModified: {},
	etag: {},

	ajaxSettings: {
		url: location.href,
		type: "GET",
		isLocal: rlocalProtocol.test( location.protocol ),
		global: true,
		processData: true,
		async: true,
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",

		/*
		timeout: 0,
		data: null,
		dataType: null,
		username: null,
		password: null,
		cache: null,
		throws: false,
		traditional: false,
		headers: {},
		*/

		accepts: {
			"*": allTypes,
			text: "text/plain",
			html: "text/html",
			xml: "application/xml, text/xml",
			json: "application/json, text/javascript"
		},

		contents: {
			xml: /\bxml\b/,
			html: /\bhtml/,
			json: /\bjson\b/
		},

		responseFields: {
			xml: "responseXML",
			text: "responseText",
			json: "responseJSON"
		},

		// Data converters
		// Keys separate source (or catchall "*") and destination types with a single space
		converters: {

			// Convert anything to text
			"* text": String,

			// Text to html (true = no transformation)
			"text html": true,

			// Evaluate text as a json expression
			"text json": JSON.parse,

			// Parse text as xml
			"text xml": jQuery.parseXML
		},

		// For options that shouldn't be deep extended:
		// you can add your own custom options here if
		// and when you create one that shouldn't be
		// deep extended (see ajaxExtend)
		flatOptions: {
			url: true,
			context: true
		}
	},

	// Creates a full fledged settings object into target
	// with both ajaxSettings and settings fields.
	// If target is omitted, writes into ajaxSettings.
	ajaxSetup: function( target, settings ) {
		return settings ?

			// Building a settings object
			ajaxExtend( ajaxExtend( target, jQuery.ajaxSettings ), settings ) :

			// Extending ajaxSettings
			ajaxExtend( jQuery.ajaxSettings, target );
	},

	ajaxPrefilter: addToPrefiltersOrTransports( prefilters ),
	ajaxTransport: addToPrefiltersOrTransports( transports ),

	// Main method
	ajax: function( url, options ) {

		// If url is an object, simulate pre-1.5 signature
		if ( typeof url === "object" ) {
			options = url;
			url = undefined;
		}

		// Force options to be an object
		options = options || {};

		var transport,

			// URL without anti-cache param
			cacheURL,

			// Response headers
			responseHeadersString,
			responseHeaders,

			// timeout handle
			timeoutTimer,

			// Url cleanup var
			urlAnchor,

			// Request state (becomes false upon send and true upon completion)
			completed,

			// To know if global events are to be dispatched
			fireGlobals,

			// Loop variable
			i,

			// uncached part of the url
			uncached,

			// Create the final options object
			s = jQuery.ajaxSetup( {}, options ),

			// Callbacks context
			callbackContext = s.context || s,

			// Context for global events is callbackContext if it is a DOM node or jQuery collection
			globalEventContext = s.context &&
				( callbackContext.nodeType || callbackContext.jquery ) ?
					jQuery( callbackContext ) :
					jQuery.event,

			// Deferreds
			deferred = jQuery.Deferred(),
			completeDeferred = jQuery.Callbacks( "once memory" ),

			// Status-dependent callbacks
			statusCode = s.statusCode || {},

			// Headers (they are sent all at once)
			requestHeaders = {},
			requestHeadersNames = {},

			// Default abort message
			strAbort = "canceled",

			// Fake xhr
			jqXHR = {
				readyState: 0,

				// Builds headers hashtable if needed
				getResponseHeader: function( key ) {
					var match;
					if ( completed ) {
						if ( !responseHeaders ) {
							responseHeaders = {};
							while ( ( match = rheaders.exec( responseHeadersString ) ) ) {
								responseHeaders[ match[ 1 ].toLowerCase() ] = match[ 2 ];
							}
						}
						match = responseHeaders[ key.toLowerCase() ];
					}
					return match == null ? null : match;
				},

				// Raw string
				getAllResponseHeaders: function() {
					return completed ? responseHeadersString : null;
				},

				// Caches the header
				setRequestHeader: function( name, value ) {
					if ( completed == null ) {
						name = requestHeadersNames[ name.toLowerCase() ] =
							requestHeadersNames[ name.toLowerCase() ] || name;
						requestHeaders[ name ] = value;
					}
					return this;
				},

				// Overrides response content-type header
				overrideMimeType: function( type ) {
					if ( completed == null ) {
						s.mimeType = type;
					}
					return this;
				},

				// Status-dependent callbacks
				statusCode: function( map ) {
					var code;
					if ( map ) {
						if ( completed ) {

							// Execute the appropriate callbacks
							jqXHR.always( map[ jqXHR.status ] );
						} else {

							// Lazy-add the new callbacks in a way that preserves old ones
							for ( code in map ) {
								statusCode[ code ] = [ statusCode[ code ], map[ code ] ];
							}
						}
					}
					return this;
				},

				// Cancel the request
				abort: function( statusText ) {
					var finalText = statusText || strAbort;
					if ( transport ) {
						transport.abort( finalText );
					}
					done( 0, finalText );
					return this;
				}
			};

		// Attach deferreds
		deferred.promise( jqXHR );

		// Add protocol if not provided (prefilters might expect it)
		// Handle falsy url in the settings object (#10093: consistency with old signature)
		// We also use the url parameter if available
		s.url = ( ( url || s.url || location.href ) + "" )
			.replace( rprotocol, location.protocol + "//" );

		// Alias method option to type as per ticket #12004
		s.type = options.method || options.type || s.method || s.type;

		// Extract dataTypes list
		s.dataTypes = ( s.dataType || "*" ).toLowerCase().match( rnothtmlwhite ) || [ "" ];

		// A cross-domain request is in order when the origin doesn't match the current origin.
		if ( s.crossDomain == null ) {
			urlAnchor = document.createElement( "a" );

			// Support: IE <=8 - 11, Edge 12 - 13
			// IE throws exception on accessing the href property if url is malformed,
			// e.g. http://example.com:80x/
			try {
				urlAnchor.href = s.url;

				// Support: IE <=8 - 11 only
				// Anchor's host property isn't correctly set when s.url is relative
				urlAnchor.href = urlAnchor.href;
				s.crossDomain = originAnchor.protocol + "//" + originAnchor.host !==
					urlAnchor.protocol + "//" + urlAnchor.host;
			} catch ( e ) {

				// If there is an error parsing the URL, assume it is crossDomain,
				// it can be rejected by the transport if it is invalid
				s.crossDomain = true;
			}
		}

		// Convert data if not already a string
		if ( s.data && s.processData && typeof s.data !== "string" ) {
			s.data = jQuery.param( s.data, s.traditional );
		}

		// Apply prefilters
		inspectPrefiltersOrTransports( prefilters, s, options, jqXHR );

		// If request was aborted inside a prefilter, stop there
		if ( completed ) {
			return jqXHR;
		}

		// We can fire global events as of now if asked to
		// Don't fire events if jQuery.event is undefined in an AMD-usage scenario (#15118)
		fireGlobals = jQuery.event && s.global;

		// Watch for a new set of requests
		if ( fireGlobals && jQuery.active++ === 0 ) {
			jQuery.event.trigger( "ajaxStart" );
		}

		// Uppercase the type
		s.type = s.type.toUpperCase();

		// Determine if request has content
		s.hasContent = !rnoContent.test( s.type );

		// Save the URL in case we're toying with the If-Modified-Since
		// and/or If-None-Match header later on
		// Remove hash to simplify url manipulation
		cacheURL = s.url.replace( rhash, "" );

		// More options handling for requests with no content
		if ( !s.hasContent ) {

			// Remember the hash so we can put it back
			uncached = s.url.slice( cacheURL.length );

			// If data is available, append data to url
			if ( s.data ) {
				cacheURL += ( rquery.test( cacheURL ) ? "&" : "?" ) + s.data;

				// #9682: remove data so that it's not used in an eventual retry
				delete s.data;
			}

			// Add or update anti-cache param if needed
			if ( s.cache === false ) {
				cacheURL = cacheURL.replace( rantiCache, "$1" );
				uncached = ( rquery.test( cacheURL ) ? "&" : "?" ) + "_=" + ( nonce++ ) + uncached;
			}

			// Put hash and anti-cache on the URL that will be requested (gh-1732)
			s.url = cacheURL + uncached;

		// Change '%20' to '+' if this is encoded form body content (gh-2658)
		} else if ( s.data && s.processData &&
			( s.contentType || "" ).indexOf( "application/x-www-form-urlencoded" ) === 0 ) {
			s.data = s.data.replace( r20, "+" );
		}

		// Set the If-Modified-Since and/or If-None-Match header, if in ifModified mode.
		if ( s.ifModified ) {
			if ( jQuery.lastModified[ cacheURL ] ) {
				jqXHR.setRequestHeader( "If-Modified-Since", jQuery.lastModified[ cacheURL ] );
			}
			if ( jQuery.etag[ cacheURL ] ) {
				jqXHR.setRequestHeader( "If-None-Match", jQuery.etag[ cacheURL ] );
			}
		}

		// Set the correct header, if data is being sent
		if ( s.data && s.hasContent && s.contentType !== false || options.contentType ) {
			jqXHR.setRequestHeader( "Content-Type", s.contentType );
		}

		// Set the Accepts header for the server, depending on the dataType
		jqXHR.setRequestHeader(
			"Accept",
			s.dataTypes[ 0 ] && s.accepts[ s.dataTypes[ 0 ] ] ?
				s.accepts[ s.dataTypes[ 0 ] ] +
					( s.dataTypes[ 0 ] !== "*" ? ", " + allTypes + "; q=0.01" : "" ) :
				s.accepts[ "*" ]
		);

		// Check for headers option
		for ( i in s.headers ) {
			jqXHR.setRequestHeader( i, s.headers[ i ] );
		}

		// Allow custom headers/mimetypes and early abort
		if ( s.beforeSend &&
			( s.beforeSend.call( callbackContext, jqXHR, s ) === false || completed ) ) {

			// Abort if not done already and return
			return jqXHR.abort();
		}

		// Aborting is no longer a cancellation
		strAbort = "abort";

		// Install callbacks on deferreds
		completeDeferred.add( s.complete );
		jqXHR.done( s.success );
		jqXHR.fail( s.error );

		// Get transport
		transport = inspectPrefiltersOrTransports( transports, s, options, jqXHR );

		// If no transport, we auto-abort
		if ( !transport ) {
			done( -1, "No Transport" );
		} else {
			jqXHR.readyState = 1;

			// Send global event
			if ( fireGlobals ) {
				globalEventContext.trigger( "ajaxSend", [ jqXHR, s ] );
			}

			// If request was aborted inside ajaxSend, stop there
			if ( completed ) {
				return jqXHR;
			}

			// Timeout
			if ( s.async && s.timeout > 0 ) {
				timeoutTimer = window.setTimeout( function() {
					jqXHR.abort( "timeout" );
				}, s.timeout );
			}

			try {
				completed = false;
				transport.send( requestHeaders, done );
			} catch ( e ) {

				// Rethrow post-completion exceptions
				if ( completed ) {
					throw e;
				}

				// Propagate others as results
				done( -1, e );
			}
		}

		// Callback for when everything is done
		function done( status, nativeStatusText, responses, headers ) {
			var isSuccess, success, error, response, modified,
				statusText = nativeStatusText;

			// Ignore repeat invocations
			if ( completed ) {
				return;
			}

			completed = true;

			// Clear timeout if it exists
			if ( timeoutTimer ) {
				window.clearTimeout( timeoutTimer );
			}

			// Dereference transport for early garbage collection
			// (no matter how long the jqXHR object will be used)
			transport = undefined;

			// Cache response headers
			responseHeadersString = headers || "";

			// Set readyState
			jqXHR.readyState = status > 0 ? 4 : 0;

			// Determine if successful
			isSuccess = status >= 200 && status < 300 || status === 304;

			// Get response data
			if ( responses ) {
				response = ajaxHandleResponses( s, jqXHR, responses );
			}

			// Convert no matter what (that way responseXXX fields are always set)
			response = ajaxConvert( s, response, jqXHR, isSuccess );

			// If successful, handle type chaining
			if ( isSuccess ) {

				// Set the If-Modified-Since and/or If-None-Match header, if in ifModified mode.
				if ( s.ifModified ) {
					modified = jqXHR.getResponseHeader( "Last-Modified" );
					if ( modified ) {
						jQuery.lastModified[ cacheURL ] = modified;
					}
					modified = jqXHR.getResponseHeader( "etag" );
					if ( modified ) {
						jQuery.etag[ cacheURL ] = modified;
					}
				}

				// if no content
				if ( status === 204 || s.type === "HEAD" ) {
					statusText = "nocontent";

				// if not modified
				} else if ( status === 304 ) {
					statusText = "notmodified";

				// If we have data, let's convert it
				} else {
					statusText = response.state;
					success = response.data;
					error = response.error;
					isSuccess = !error;
				}
			} else {

				// Extract error from statusText and normalize for non-aborts
				error = statusText;
				if ( status || !statusText ) {
					statusText = "error";
					if ( status < 0 ) {
						status = 0;
					}
				}
			}

			// Set data for the fake xhr object
			jqXHR.status = status;
			jqXHR.statusText = ( nativeStatusText || statusText ) + "";

			// Success/Error
			if ( isSuccess ) {
				deferred.resolveWith( callbackContext, [ success, statusText, jqXHR ] );
			} else {
				deferred.rejectWith( callbackContext, [ jqXHR, statusText, error ] );
			}

			// Status-dependent callbacks
			jqXHR.statusCode( statusCode );
			statusCode = undefined;

			if ( fireGlobals ) {
				globalEventContext.trigger( isSuccess ? "ajaxSuccess" : "ajaxError",
					[ jqXHR, s, isSuccess ? success : error ] );
			}

			// Complete
			completeDeferred.fireWith( callbackContext, [ jqXHR, statusText ] );

			if ( fireGlobals ) {
				globalEventContext.trigger( "ajaxComplete", [ jqXHR, s ] );

				// Handle the global AJAX counter
				if ( !( --jQuery.active ) ) {
					jQuery.event.trigger( "ajaxStop" );
				}
			}
		}

		return jqXHR;
	},

	getJSON: function( url, data, callback ) {
		return jQuery.get( url, data, callback, "json" );
	},

	getScript: function( url, callback ) {
		return jQuery.get( url, undefined, callback, "script" );
	}
} );

jQuery.each( [ "get", "post" ], function( i, method ) {
	jQuery[ method ] = function( url, data, callback, type ) {

		// Shift arguments if data argument was omitted
		if ( jQuery.isFunction( data ) ) {
			type = type || callback;
			callback = data;
			data = undefined;
		}

		// The url can be an options object (which then must have .url)
		return jQuery.ajax( jQuery.extend( {
			url: url,
			type: method,
			dataType: type,
			data: data,
			success: callback
		}, jQuery.isPlainObject( url ) && url ) );
	};
} );


jQuery._evalUrl = function( url ) {
	return jQuery.ajax( {
		url: url,

		// Make this explicit, since user can override this through ajaxSetup (#11264)
		type: "GET",
		dataType: "script",
		cache: true,
		async: false,
		global: false,
		"throws": true
	} );
};


jQuery.fn.extend( {
	wrapAll: function( html ) {
		var wrap;

		if ( this[ 0 ] ) {
			if ( jQuery.isFunction( html ) ) {
				html = html.call( this[ 0 ] );
			}

			// The elements to wrap the target around
			wrap = jQuery( html, this[ 0 ].ownerDocument ).eq( 0 ).clone( true );

			if ( this[ 0 ].parentNode ) {
				wrap.insertBefore( this[ 0 ] );
			}

			wrap.map( function() {
				var elem = this;

				while ( elem.firstElementChild ) {
					elem = elem.firstElementChild;
				}

				return elem;
			} ).append( this );
		}

		return this;
	},

	wrapInner: function( html ) {
		if ( jQuery.isFunction( html ) ) {
			return this.each( function( i ) {
				jQuery( this ).wrapInner( html.call( this, i ) );
			} );
		}

		return this.each( function() {
			var self = jQuery( this ),
				contents = self.contents();

			if ( contents.length ) {
				contents.wrapAll( html );

			} else {
				self.append( html );
			}
		} );
	},

	wrap: function( html ) {
		var isFunction = jQuery.isFunction( html );

		return this.each( function( i ) {
			jQuery( this ).wrapAll( isFunction ? html.call( this, i ) : html );
		} );
	},

	unwrap: function( selector ) {
		this.parent( selector ).not( "body" ).each( function() {
			jQuery( this ).replaceWith( this.childNodes );
		} );
		return this;
	}
} );


jQuery.expr.pseudos.hidden = function( elem ) {
	return !jQuery.expr.pseudos.visible( elem );
};
jQuery.expr.pseudos.visible = function( elem ) {
	return !!( elem.offsetWidth || elem.offsetHeight || elem.getClientRects().length );
};




jQuery.ajaxSettings.xhr = function() {
	try {
		return new window.XMLHttpRequest();
	} catch ( e ) {}
};

var xhrSuccessStatus = {

		// File protocol always yields status code 0, assume 200
		0: 200,

		// Support: IE <=9 only
		// #1450: sometimes IE returns 1223 when it should be 204
		1223: 204
	},
	xhrSupported = jQuery.ajaxSettings.xhr();

support.cors = !!xhrSupported && ( "withCredentials" in xhrSupported );
support.ajax = xhrSupported = !!xhrSupported;

jQuery.ajaxTransport( function( options ) {
	var callback, errorCallback;

	// Cross domain only allowed if supported through XMLHttpRequest
	if ( support.cors || xhrSupported && !options.crossDomain ) {
		return {
			send: function( headers, complete ) {
				var i,
					xhr = options.xhr();

				xhr.open(
					options.type,
					options.url,
					options.async,
					options.username,
					options.password
				);

				// Apply custom fields if provided
				if ( options.xhrFields ) {
					for ( i in options.xhrFields ) {
						xhr[ i ] = options.xhrFields[ i ];
					}
				}

				// Override mime type if needed
				if ( options.mimeType && xhr.overrideMimeType ) {
					xhr.overrideMimeType( options.mimeType );
				}

				// X-Requested-With header
				// For cross-domain requests, seeing as conditions for a preflight are
				// akin to a jigsaw puzzle, we simply never set it to be sure.
				// (it can always be set on a per-request basis or even using ajaxSetup)
				// For same-domain requests, won't change header if already provided.
				if ( !options.crossDomain && !headers[ "X-Requested-With" ] ) {
					headers[ "X-Requested-With" ] = "XMLHttpRequest";
				}

				// Set headers
				for ( i in headers ) {
					xhr.setRequestHeader( i, headers[ i ] );
				}

				// Callback
				callback = function( type ) {
					return function() {
						if ( callback ) {
							callback = errorCallback = xhr.onload =
								xhr.onerror = xhr.onabort = xhr.onreadystatechange = null;

							if ( type === "abort" ) {
								xhr.abort();
							} else if ( type === "error" ) {

								// Support: IE <=9 only
								// On a manual native abort, IE9 throws
								// errors on any property access that is not readyState
								if ( typeof xhr.status !== "number" ) {
									complete( 0, "error" );
								} else {
									complete(

										// File: protocol always yields status 0; see #8605, #14207
										xhr.status,
										xhr.statusText
									);
								}
							} else {
								complete(
									xhrSuccessStatus[ xhr.status ] || xhr.status,
									xhr.statusText,

									// Support: IE <=9 only
									// IE9 has no XHR2 but throws on binary (trac-11426)
									// For XHR2 non-text, let the caller handle it (gh-2498)
									( xhr.responseType || "text" ) !== "text"  ||
									typeof xhr.responseText !== "string" ?
										{ binary: xhr.response } :
										{ text: xhr.responseText },
									xhr.getAllResponseHeaders()
								);
							}
						}
					};
				};

				// Listen to events
				xhr.onload = callback();
				errorCallback = xhr.onerror = callback( "error" );

				// Support: IE 9 only
				// Use onreadystatechange to replace onabort
				// to handle uncaught aborts
				if ( xhr.onabort !== undefined ) {
					xhr.onabort = errorCallback;
				} else {
					xhr.onreadystatechange = function() {

						// Check readyState before timeout as it changes
						if ( xhr.readyState === 4 ) {

							// Allow onerror to be called first,
							// but that will not handle a native abort
							// Also, save errorCallback to a variable
							// as xhr.onerror cannot be accessed
							window.setTimeout( function() {
								if ( callback ) {
									errorCallback();
								}
							} );
						}
					};
				}

				// Create the abort callback
				callback = callback( "abort" );

				try {

					// Do send the request (this may raise an exception)
					xhr.send( options.hasContent && options.data || null );
				} catch ( e ) {

					// #14683: Only rethrow if this hasn't been notified as an error yet
					if ( callback ) {
						throw e;
					}
				}
			},

			abort: function() {
				if ( callback ) {
					callback();
				}
			}
		};
	}
} );




// Prevent auto-execution of scripts when no explicit dataType was provided (See gh-2432)
jQuery.ajaxPrefilter( function( s ) {
	if ( s.crossDomain ) {
		s.contents.script = false;
	}
} );

// Install script dataType
jQuery.ajaxSetup( {
	accepts: {
		script: "text/javascript, application/javascript, " +
			"application/ecmascript, application/x-ecmascript"
	},
	contents: {
		script: /\b(?:java|ecma)script\b/
	},
	converters: {
		"text script": function( text ) {
			jQuery.globalEval( text );
			return text;
		}
	}
} );

// Handle cache's special case and crossDomain
jQuery.ajaxPrefilter( "script", function( s ) {
	if ( s.cache === undefined ) {
		s.cache = false;
	}
	if ( s.crossDomain ) {
		s.type = "GET";
	}
} );

// Bind script tag hack transport
jQuery.ajaxTransport( "script", function( s ) {

	// This transport only deals with cross domain requests
	if ( s.crossDomain ) {
		var script, callback;
		return {
			send: function( _, complete ) {
				script = jQuery( "<script>" ).prop( {
					charset: s.scriptCharset,
					src: s.url
				} ).on(
					"load error",
					callback = function( evt ) {
						script.remove();
						callback = null;
						if ( evt ) {
							complete( evt.type === "error" ? 404 : 200, evt.type );
						}
					}
				);

				// Use native DOM manipulation to avoid our domManip AJAX trickery
				document.head.appendChild( script[ 0 ] );
			},
			abort: function() {
				if ( callback ) {
					callback();
				}
			}
		};
	}
} );




var oldCallbacks = [],
	rjsonp = /(=)\?(?=&|$)|\?\?/;

// Default jsonp settings
jQuery.ajaxSetup( {
	jsonp: "callback",
	jsonpCallback: function() {
		var callback = oldCallbacks.pop() || ( jQuery.expando + "_" + ( nonce++ ) );
		this[ callback ] = true;
		return callback;
	}
} );

// Detect, normalize options and install callbacks for jsonp requests
jQuery.ajaxPrefilter( "json jsonp", function( s, originalSettings, jqXHR ) {

	var callbackName, overwritten, responseContainer,
		jsonProp = s.jsonp !== false && ( rjsonp.test( s.url ) ?
			"url" :
			typeof s.data === "string" &&
				( s.contentType || "" )
					.indexOf( "application/x-www-form-urlencoded" ) === 0 &&
				rjsonp.test( s.data ) && "data"
		);

	// Handle iff the expected data type is "jsonp" or we have a parameter to set
	if ( jsonProp || s.dataTypes[ 0 ] === "jsonp" ) {

		// Get callback name, remembering preexisting value associated with it
		callbackName = s.jsonpCallback = jQuery.isFunction( s.jsonpCallback ) ?
			s.jsonpCallback() :
			s.jsonpCallback;

		// Insert callback into url or form data
		if ( jsonProp ) {
			s[ jsonProp ] = s[ jsonProp ].replace( rjsonp, "$1" + callbackName );
		} else if ( s.jsonp !== false ) {
			s.url += ( rquery.test( s.url ) ? "&" : "?" ) + s.jsonp + "=" + callbackName;
		}

		// Use data converter to retrieve json after script execution
		s.converters[ "script json" ] = function() {
			if ( !responseContainer ) {
				jQuery.error( callbackName + " was not called" );
			}
			return responseContainer[ 0 ];
		};

		// Force json dataType
		s.dataTypes[ 0 ] = "json";

		// Install callback
		overwritten = window[ callbackName ];
		window[ callbackName ] = function() {
			responseContainer = arguments;
		};

		// Clean-up function (fires after converters)
		jqXHR.always( function() {

			// If previous value didn't exist - remove it
			if ( overwritten === undefined ) {
				jQuery( window ).removeProp( callbackName );

			// Otherwise restore preexisting value
			} else {
				window[ callbackName ] = overwritten;
			}

			// Save back as free
			if ( s[ callbackName ] ) {

				// Make sure that re-using the options doesn't screw things around
				s.jsonpCallback = originalSettings.jsonpCallback;

				// Save the callback name for future use
				oldCallbacks.push( callbackName );
			}

			// Call if it was a function and we have a response
			if ( responseContainer && jQuery.isFunction( overwritten ) ) {
				overwritten( responseContainer[ 0 ] );
			}

			responseContainer = overwritten = undefined;
		} );

		// Delegate to script
		return "script";
	}
} );




// Support: Safari 8 only
// In Safari 8 documents created via document.implementation.createHTMLDocument
// collapse sibling forms: the second one becomes a child of the first one.
// Because of that, this security measure has to be disabled in Safari 8.
// https://bugs.webkit.org/show_bug.cgi?id=137337
support.createHTMLDocument = ( function() {
	var body = document.implementation.createHTMLDocument( "" ).body;
	body.innerHTML = "<form></form><form></form>";
	return body.childNodes.length === 2;
} )();


// Argument "data" should be string of html
// context (optional): If specified, the fragment will be created in this context,
// defaults to document
// keepScripts (optional): If true, will include scripts passed in the html string
jQuery.parseHTML = function( data, context, keepScripts ) {
	if ( typeof data !== "string" ) {
		return [];
	}
	if ( typeof context === "boolean" ) {
		keepScripts = context;
		context = false;
	}

	var base, parsed, scripts;

	if ( !context ) {

		// Stop scripts or inline event handlers from being executed immediately
		// by using document.implementation
		if ( support.createHTMLDocument ) {
			context = document.implementation.createHTMLDocument( "" );

			// Set the base href for the created document
			// so any parsed elements with URLs
			// are based on the document's URL (gh-2965)
			base = context.createElement( "base" );
			base.href = document.location.href;
			context.head.appendChild( base );
		} else {
			context = document;
		}
	}

	parsed = rsingleTag.exec( data );
	scripts = !keepScripts && [];

	// Single tag
	if ( parsed ) {
		return [ context.createElement( parsed[ 1 ] ) ];
	}

	parsed = buildFragment( [ data ], context, scripts );

	if ( scripts && scripts.length ) {
		jQuery( scripts ).remove();
	}

	return jQuery.merge( [], parsed.childNodes );
};


/**
 * Load a url into a page
 */
jQuery.fn.load = function( url, params, callback ) {
	var selector, type, response,
		self = this,
		off = url.indexOf( " " );

	if ( off > -1 ) {
		selector = stripAndCollapse( url.slice( off ) );
		url = url.slice( 0, off );
	}

	// If it's a function
	if ( jQuery.isFunction( params ) ) {

		// We assume that it's the callback
		callback = params;
		params = undefined;

	// Otherwise, build a param string
	} else if ( params && typeof params === "object" ) {
		type = "POST";
	}

	// If we have elements to modify, make the request
	if ( self.length > 0 ) {
		jQuery.ajax( {
			url: url,

			// If "type" variable is undefined, then "GET" method will be used.
			// Make value of this field explicit since
			// user can override it through ajaxSetup method
			type: type || "GET",
			dataType: "html",
			data: params
		} ).done( function( responseText ) {

			// Save response for use in complete callback
			response = arguments;

			self.html( selector ?

				// If a selector was specified, locate the right elements in a dummy div
				// Exclude scripts to avoid IE 'Permission Denied' errors
				jQuery( "<div>" ).append( jQuery.parseHTML( responseText ) ).find( selector ) :

				// Otherwise use the full result
				responseText );

		// If the request succeeds, this function gets "data", "status", "jqXHR"
		// but they are ignored because response was set above.
		// If it fails, this function gets "jqXHR", "status", "error"
		} ).always( callback && function( jqXHR, status ) {
			self.each( function() {
				callback.apply( this, response || [ jqXHR.responseText, status, jqXHR ] );
			} );
		} );
	}

	return this;
};




// Attach a bunch of functions for handling common AJAX events
jQuery.each( [
	"ajaxStart",
	"ajaxStop",
	"ajaxComplete",
	"ajaxError",
	"ajaxSuccess",
	"ajaxSend"
], function( i, type ) {
	jQuery.fn[ type ] = function( fn ) {
		return this.on( type, fn );
	};
} );




jQuery.expr.pseudos.animated = function( elem ) {
	return jQuery.grep( jQuery.timers, function( fn ) {
		return elem === fn.elem;
	} ).length;
};




jQuery.offset = {
	setOffset: function( elem, options, i ) {
		var curPosition, curLeft, curCSSTop, curTop, curOffset, curCSSLeft, calculatePosition,
			position = jQuery.css( elem, "position" ),
			curElem = jQuery( elem ),
			props = {};

		// Set position first, in-case top/left are set even on static elem
		if ( position === "static" ) {
			elem.style.position = "relative";
		}

		curOffset = curElem.offset();
		curCSSTop = jQuery.css( elem, "top" );
		curCSSLeft = jQuery.css( elem, "left" );
		calculatePosition = ( position === "absolute" || position === "fixed" ) &&
			( curCSSTop + curCSSLeft ).indexOf( "auto" ) > -1;

		// Need to be able to calculate position if either
		// top or left is auto and position is either absolute or fixed
		if ( calculatePosition ) {
			curPosition = curElem.position();
			curTop = curPosition.top;
			curLeft = curPosition.left;

		} else {
			curTop = parseFloat( curCSSTop ) || 0;
			curLeft = parseFloat( curCSSLeft ) || 0;
		}

		if ( jQuery.isFunction( options ) ) {

			// Use jQuery.extend here to allow modification of coordinates argument (gh-1848)
			options = options.call( elem, i, jQuery.extend( {}, curOffset ) );
		}

		if ( options.top != null ) {
			props.top = ( options.top - curOffset.top ) + curTop;
		}
		if ( options.left != null ) {
			props.left = ( options.left - curOffset.left ) + curLeft;
		}

		if ( "using" in options ) {
			options.using.call( elem, props );

		} else {
			curElem.css( props );
		}
	}
};

jQuery.fn.extend( {
	offset: function( options ) {

		// Preserve chaining for setter
		if ( arguments.length ) {
			return options === undefined ?
				this :
				this.each( function( i ) {
					jQuery.offset.setOffset( this, options, i );
				} );
		}

		var doc, docElem, rect, win,
			elem = this[ 0 ];

		if ( !elem ) {
			return;
		}

		// Return zeros for disconnected and hidden (display: none) elements (gh-2310)
		// Support: IE <=11 only
		// Running getBoundingClientRect on a
		// disconnected node in IE throws an error
		if ( !elem.getClientRects().length ) {
			return { top: 0, left: 0 };
		}

		rect = elem.getBoundingClientRect();

		doc = elem.ownerDocument;
		docElem = doc.documentElement;
		win = doc.defaultView;

		return {
			top: rect.top + win.pageYOffset - docElem.clientTop,
			left: rect.left + win.pageXOffset - docElem.clientLeft
		};
	},

	position: function() {
		if ( !this[ 0 ] ) {
			return;
		}

		var offsetParent, offset,
			elem = this[ 0 ],
			parentOffset = { top: 0, left: 0 };

		// Fixed elements are offset from window (parentOffset = {top:0, left: 0},
		// because it is its only offset parent
		if ( jQuery.css( elem, "position" ) === "fixed" ) {

			// Assume getBoundingClientRect is there when computed position is fixed
			offset = elem.getBoundingClientRect();

		} else {

			// Get *real* offsetParent
			offsetParent = this.offsetParent();

			// Get correct offsets
			offset = this.offset();
			if ( !nodeName( offsetParent[ 0 ], "html" ) ) {
				parentOffset = offsetParent.offset();
			}

			// Add offsetParent borders
			parentOffset = {
				top: parentOffset.top + jQuery.css( offsetParent[ 0 ], "borderTopWidth", true ),
				left: parentOffset.left + jQuery.css( offsetParent[ 0 ], "borderLeftWidth", true )
			};
		}

		// Subtract parent offsets and element margins
		return {
			top: offset.top - parentOffset.top - jQuery.css( elem, "marginTop", true ),
			left: offset.left - parentOffset.left - jQuery.css( elem, "marginLeft", true )
		};
	},

	// This method will return documentElement in the following cases:
	// 1) For the element inside the iframe without offsetParent, this method will return
	//    documentElement of the parent window
	// 2) For the hidden or detached element
	// 3) For body or html element, i.e. in case of the html node - it will return itself
	//
	// but those exceptions were never presented as a real life use-cases
	// and might be considered as more preferable results.
	//
	// This logic, however, is not guaranteed and can change at any point in the future
	offsetParent: function() {
		return this.map( function() {
			var offsetParent = this.offsetParent;

			while ( offsetParent && jQuery.css( offsetParent, "position" ) === "static" ) {
				offsetParent = offsetParent.offsetParent;
			}

			return offsetParent || documentElement;
		} );
	}
} );

// Create scrollLeft and scrollTop methods
jQuery.each( { scrollLeft: "pageXOffset", scrollTop: "pageYOffset" }, function( method, prop ) {
	var top = "pageYOffset" === prop;

	jQuery.fn[ method ] = function( val ) {
		return access( this, function( elem, method, val ) {

			// Coalesce documents and windows
			var win;
			if ( jQuery.isWindow( elem ) ) {
				win = elem;
			} else if ( elem.nodeType === 9 ) {
				win = elem.defaultView;
			}

			if ( val === undefined ) {
				return win ? win[ prop ] : elem[ method ];
			}

			if ( win ) {
				win.scrollTo(
					!top ? val : win.pageXOffset,
					top ? val : win.pageYOffset
				);

			} else {
				elem[ method ] = val;
			}
		}, method, val, arguments.length );
	};
} );

// Support: Safari <=7 - 9.1, Chrome <=37 - 49
// Add the top/left cssHooks using jQuery.fn.position
// Webkit bug: https://bugs.webkit.org/show_bug.cgi?id=29084
// Blink bug: https://bugs.chromium.org/p/chromium/issues/detail?id=589347
// getComputedStyle returns percent when specified for top/left/bottom/right;
// rather than make the css module depend on the offset module, just check for it here
jQuery.each( [ "top", "left" ], function( i, prop ) {
	jQuery.cssHooks[ prop ] = addGetHookIf( support.pixelPosition,
		function( elem, computed ) {
			if ( computed ) {
				computed = curCSS( elem, prop );

				// If curCSS returns percentage, fallback to offset
				return rnumnonpx.test( computed ) ?
					jQuery( elem ).position()[ prop ] + "px" :
					computed;
			}
		}
	);
} );


// Create innerHeight, innerWidth, height, width, outerHeight and outerWidth methods
jQuery.each( { Height: "height", Width: "width" }, function( name, type ) {
	jQuery.each( { padding: "inner" + name, content: type, "": "outer" + name },
		function( defaultExtra, funcName ) {

		// Margin is only for outerHeight, outerWidth
		jQuery.fn[ funcName ] = function( margin, value ) {
			var chainable = arguments.length && ( defaultExtra || typeof margin !== "boolean" ),
				extra = defaultExtra || ( margin === true || value === true ? "margin" : "border" );

			return access( this, function( elem, type, value ) {
				var doc;

				if ( jQuery.isWindow( elem ) ) {

					// $( window ).outerWidth/Height return w/h including scrollbars (gh-1729)
					return funcName.indexOf( "outer" ) === 0 ?
						elem[ "inner" + name ] :
						elem.document.documentElement[ "client" + name ];
				}

				// Get document width or height
				if ( elem.nodeType === 9 ) {
					doc = elem.documentElement;

					// Either scroll[Width/Height] or offset[Width/Height] or client[Width/Height],
					// whichever is greatest
					return Math.max(
						elem.body[ "scroll" + name ], doc[ "scroll" + name ],
						elem.body[ "offset" + name ], doc[ "offset" + name ],
						doc[ "client" + name ]
					);
				}

				return value === undefined ?

					// Get width or height on the element, requesting but not forcing parseFloat
					jQuery.css( elem, type, extra ) :

					// Set width or height on the element
					jQuery.style( elem, type, value, extra );
			}, type, chainable ? margin : undefined, chainable );
		};
	} );
} );


jQuery.fn.extend( {

	bind: function( types, data, fn ) {
		return this.on( types, null, data, fn );
	},
	unbind: function( types, fn ) {
		return this.off( types, null, fn );
	},

	delegate: function( selector, types, data, fn ) {
		return this.on( types, selector, data, fn );
	},
	undelegate: function( selector, types, fn ) {

		// ( namespace ) or ( selector, types [, fn] )
		return arguments.length === 1 ?
			this.off( selector, "**" ) :
			this.off( types, selector || "**", fn );
	}
} );

jQuery.holdReady = function( hold ) {
	if ( hold ) {
		jQuery.readyWait++;
	} else {
		jQuery.ready( true );
	}
};
jQuery.isArray = Array.isArray;
jQuery.parseJSON = JSON.parse;
jQuery.nodeName = nodeName;




// Register as a named AMD module, since jQuery can be concatenated with other
// files that may use define, but not via a proper concatenation script that
// understands anonymous AMD modules. A named AMD is safest and most robust
// way to register. Lowercase jquery is used because AMD module names are
// derived from file names, and jQuery is normally delivered in a lowercase
// file name. Do this after creating the global so that if an AMD module wants
// to call noConflict to hide this version of jQuery, it will work.

// Note that for maximum portability, libraries that are not jQuery should
// declare themselves as anonymous modules, and avoid setting a global if an
// AMD loader is present. jQuery is a special case. For more information, see
// https://github.com/jrburke/requirejs/wiki/Updating-existing-libraries#wiki-anon

if ( typeof define === "function" && define.amd ) {
	define( "jquery", [], function() {
		return jQuery;
	} );
}




var

	// Map over jQuery in case of overwrite
	_jQuery = window.jQuery,

	// Map over the $ in case of overwrite
	_$ = window.$;

jQuery.noConflict = function( deep ) {
	if ( window.$ === jQuery ) {
		window.$ = _$;
	}

	if ( deep && window.jQuery === jQuery ) {
		window.jQuery = _jQuery;
	}

	return jQuery;
};

// Expose jQuery and $ identifiers, even in AMD
// (#7102#comment:10, https://github.com/jquery/jquery/pull/557)
// and CommonJS for browser emulators (#13566)
if ( !noGlobal ) {
	window.jQuery = window.$ = jQuery;
}




return jQuery;
} );

/*! Copyright (c) 2011 Piotr Rochala (http://rocha.la)
 * Dual licensed under the MIT (http://www.opensource.org/licenses/mit-license.php)
 * and GPL (http://www.opensource.org/licenses/gpl-license.php) licenses.
 *
 * Version: 1.3.8
 *
 */
(function(e){e.fn.extend({slimScroll:function(f){var a=e.extend({width:"auto",height:"250px",size:"7px",color:"#000",position:"right",distance:"1px",start:"top",opacity:.4,alwaysVisible:!1,disableFadeOut:!1,railVisible:!1,railColor:"#333",railOpacity:.2,railDraggable:!0,railClass:"slimScrollRail",barClass:"slimScrollBar",wrapperClass:"slimScrollDiv",allowPageScroll:!1,wheelStep:20,touchScrollStep:200,borderRadius:"7px",railBorderRadius:"7px"},f);this.each(function(){function v(d){if(r){d=d||window.event;
var c=0;d.wheelDelta&&(c=-d.wheelDelta/120);d.detail&&(c=d.detail/3);e(d.target||d.srcTarget||d.srcElement).closest("."+a.wrapperClass).is(b.parent())&&n(c,!0);d.preventDefault&&!k&&d.preventDefault();k||(d.returnValue=!1)}}function n(d,g,e){k=!1;var f=b.outerHeight()-c.outerHeight();g&&(g=parseInt(c.css("top"))+d*parseInt(a.wheelStep)/100*c.outerHeight(),g=Math.min(Math.max(g,0),f),g=0<d?Math.ceil(g):Math.floor(g),c.css({top:g+"px"}));l=parseInt(c.css("top"))/(b.outerHeight()-c.outerHeight());g=
l*(b[0].scrollHeight-b.outerHeight());e&&(g=d,d=g/b[0].scrollHeight*b.outerHeight(),d=Math.min(Math.max(d,0),f),c.css({top:d+"px"}));b.scrollTop(g);b.trigger("slimscrolling",~~g);w();p()}function x(){u=Math.max(b.outerHeight()/b[0].scrollHeight*b.outerHeight(),30);c.css({height:u+"px"});var a=u==b.outerHeight()?"none":"block";c.css({display:a})}function w(){x();clearTimeout(B);l==~~l?(k=a.allowPageScroll,C!=l&&b.trigger("slimscroll",0==~~l?"top":"bottom")):k=!1;C=l;u>=b.outerHeight()?k=!0:(c.stop(!0,
!0).fadeIn("fast"),a.railVisible&&m.stop(!0,!0).fadeIn("fast"))}function p(){a.alwaysVisible||(B=setTimeout(function(){a.disableFadeOut&&r||y||z||(c.fadeOut("slow"),m.fadeOut("slow"))},1E3))}var r,y,z,B,A,u,l,C,k=!1,b=e(this);if(b.parent().hasClass(a.wrapperClass)){var q=b.scrollTop(),c=b.siblings("."+a.barClass),m=b.siblings("."+a.railClass);x();if(e.isPlainObject(f)){if("height"in f&&"auto"==f.height){b.parent().css("height","auto");b.css("height","auto");var h=b.parent().parent().height();b.parent().css("height",
h);b.css("height",h)}else"height"in f&&(h=f.height,b.parent().css("height",h),b.css("height",h));if("scrollTo"in f)q=parseInt(a.scrollTo);else if("scrollBy"in f)q+=parseInt(a.scrollBy);else if("destroy"in f){c.remove();m.remove();b.unwrap();return}n(q,!1,!0)}}else if(!(e.isPlainObject(f)&&"destroy"in f)){a.height="auto"==a.height?b.parent().height():a.height;q=e("<div></div>").addClass(a.wrapperClass).css({position:"relative",overflow:"hidden",width:a.width,height:a.height});b.css({overflow:"hidden",
width:a.width,height:a.height});var m=e("<div></div>").addClass(a.railClass).css({width:a.size,height:"100%",position:"absolute",top:0,display:a.alwaysVisible&&a.railVisible?"block":"none","border-radius":a.railBorderRadius,background:a.railColor,opacity:a.railOpacity,zIndex:90}),c=e("<div></div>").addClass(a.barClass).css({background:a.color,width:a.size,position:"absolute",top:0,opacity:a.opacity,display:a.alwaysVisible?"block":"none","border-radius":a.borderRadius,BorderRadius:a.borderRadius,MozBorderRadius:a.borderRadius,
WebkitBorderRadius:a.borderRadius,zIndex:99}),h="right"==a.position?{right:a.distance}:{left:a.distance};m.css(h);c.css(h);b.wrap(q);b.parent().append(c);b.parent().append(m);a.railDraggable&&c.bind("mousedown",function(a){var b=e(document);z=!0;t=parseFloat(c.css("top"));pageY=a.pageY;b.bind("mousemove.slimscroll",function(a){currTop=t+a.pageY-pageY;c.css("top",currTop);n(0,c.position().top,!1)});b.bind("mouseup.slimscroll",function(a){z=!1;p();b.unbind(".slimscroll")});return!1}).bind("selectstart.slimscroll",
function(a){a.stopPropagation();a.preventDefault();return!1});m.hover(function(){w()},function(){p()});c.hover(function(){y=!0},function(){y=!1});b.hover(function(){r=!0;w();p()},function(){r=!1;p()});b.bind("touchstart",function(a,b){a.originalEvent.touches.length&&(A=a.originalEvent.touches[0].pageY)});b.bind("touchmove",function(b){k||b.originalEvent.preventDefault();b.originalEvent.touches.length&&(n((A-b.originalEvent.touches[0].pageY)/a.touchScrollStep,!0),A=b.originalEvent.touches[0].pageY)});
x();"bottom"===a.start?(c.css({top:b.outerHeight()-c.outerHeight()}),n(0,!0)):"top"!==a.start&&(n(e(a.start).position().top,null,!0),a.alwaysVisible||c.hide());window.addEventListener?(this.addEventListener("DOMMouseScroll",v,!1),this.addEventListener("mousewheel",v,!1)):document.attachEvent("onmousewheel",v)}});return this}});e.fn.extend({slimscroll:e.fn.slimScroll})})(jQuery);
/*!
 * Bootstrap v3.3.7 (http://getbootstrap.com)
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under the MIT license
 */

if (typeof jQuery === 'undefined') {
  throw new Error('Bootstrap\'s JavaScript requires jQuery')
}

+function ($) {
  'use strict';
  var version = $.fn.jquery.split(' ')[0].split('.')
  if ((version[0] < 2 && version[1] < 9) || (version[0] == 1 && version[1] == 9 && version[2] < 1) || (version[0] > 3)) {
    throw new Error('Bootstrap\'s JavaScript requires jQuery version 1.9.1 or higher, but lower than version 4')
  }
}(jQuery);

/* ========================================================================
 * Bootstrap: transition.js v3.3.7
 * http://getbootstrap.com/javascript/#transitions
 * ========================================================================
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // CSS TRANSITION SUPPORT (Shoutout: http://www.modernizr.com/)
  // ============================================================

  function transitionEnd() {
    var el = document.createElement('bootstrap')

    var transEndEventNames = {
      WebkitTransition : 'webkitTransitionEnd',
      MozTransition    : 'transitionend',
      OTransition      : 'oTransitionEnd otransitionend',
      transition       : 'transitionend'
    }

    for (var name in transEndEventNames) {
      if (el.style[name] !== undefined) {
        return { end: transEndEventNames[name] }
      }
    }

    return false // explicit for ie8 (  ._.)
  }

  // http://blog.alexmaccaw.com/css-transitions
  $.fn.emulateTransitionEnd = function (duration) {
    var called = false
    var $el = this
    $(this).one('bsTransitionEnd', function () { called = true })
    var callback = function () { if (!called) $($el).trigger($.support.transition.end) }
    setTimeout(callback, duration)
    return this
  }

  $(function () {
    $.support.transition = transitionEnd()

    if (!$.support.transition) return

    $.event.special.bsTransitionEnd = {
      bindType: $.support.transition.end,
      delegateType: $.support.transition.end,
      handle: function (e) {
        if ($(e.target).is(this)) return e.handleObj.handler.apply(this, arguments)
      }
    }
  })

}(jQuery);

/* ========================================================================
 * Bootstrap: alert.js v3.3.7
 * http://getbootstrap.com/javascript/#alerts
 * ========================================================================
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // ALERT CLASS DEFINITION
  // ======================

  var dismiss = '[data-dismiss="alert"]'
  var Alert   = function (el) {
    $(el).on('click', dismiss, this.close)
  }

  Alert.VERSION = '3.3.7'

  Alert.TRANSITION_DURATION = 150

  Alert.prototype.close = function (e) {
    var $this    = $(this)
    var selector = $this.attr('data-target')

    if (!selector) {
      selector = $this.attr('href')
      selector = selector && selector.replace(/.*(?=#[^\s]*$)/, '') // strip for ie7
    }

    var $parent = $(selector === '#' ? [] : selector)

    if (e) e.preventDefault()

    if (!$parent.length) {
      $parent = $this.closest('.alert')
    }

    $parent.trigger(e = $.Event('close.bs.alert'))

    if (e.isDefaultPrevented()) return

    $parent.removeClass('in')

    function removeElement() {
      // detach from parent, fire event then clean up data
      $parent.detach().trigger('closed.bs.alert').remove()
    }

    $.support.transition && $parent.hasClass('fade') ?
      $parent
        .one('bsTransitionEnd', removeElement)
        .emulateTransitionEnd(Alert.TRANSITION_DURATION) :
      removeElement()
  }


  // ALERT PLUGIN DEFINITION
  // =======================

  function Plugin(option) {
    return this.each(function () {
      var $this = $(this)
      var data  = $this.data('bs.alert')

      if (!data) $this.data('bs.alert', (data = new Alert(this)))
      if (typeof option == 'string') data[option].call($this)
    })
  }

  var old = $.fn.alert

  $.fn.alert             = Plugin
  $.fn.alert.Constructor = Alert


  // ALERT NO CONFLICT
  // =================

  $.fn.alert.noConflict = function () {
    $.fn.alert = old
    return this
  }


  // ALERT DATA-API
  // ==============

  $(document).on('click.bs.alert.data-api', dismiss, Alert.prototype.close)

}(jQuery);

/* ========================================================================
 * Bootstrap: button.js v3.3.7
 * http://getbootstrap.com/javascript/#buttons
 * ========================================================================
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // BUTTON PUBLIC CLASS DEFINITION
  // ==============================

  var Button = function (element, options) {
    this.$element  = $(element)
    this.options   = $.extend({}, Button.DEFAULTS, options)
    this.isLoading = false
  }

  Button.VERSION  = '3.3.7'

  Button.DEFAULTS = {
    loadingText: 'loading...'
  }

  Button.prototype.setState = function (state) {
    var d    = 'disabled'
    var $el  = this.$element
    var val  = $el.is('input') ? 'val' : 'html'
    var data = $el.data()

    state += 'Text'

    if (data.resetText == null) $el.data('resetText', $el[val]())

    // push to event loop to allow forms to submit
    setTimeout($.proxy(function () {
      $el[val](data[state] == null ? this.options[state] : data[state])

      if (state == 'loadingText') {
        this.isLoading = true
        $el.addClass(d).attr(d, d).prop(d, true)
      } else if (this.isLoading) {
        this.isLoading = false
        $el.removeClass(d).removeAttr(d).prop(d, false)
      }
    }, this), 0)
  }

  Button.prototype.toggle = function () {
    var changed = true
    var $parent = this.$element.closest('[data-toggle="buttons"]')

    if ($parent.length) {
      var $input = this.$element.find('input')
      if ($input.prop('type') == 'radio') {
        if ($input.prop('checked')) changed = false
        $parent.find('.active').removeClass('active')
        this.$element.addClass('active')
      } else if ($input.prop('type') == 'checkbox') {
        if (($input.prop('checked')) !== this.$element.hasClass('active')) changed = false
        this.$element.toggleClass('active')
      }
      $input.prop('checked', this.$element.hasClass('active'))
      if (changed) $input.trigger('change')
    } else {
      this.$element.attr('aria-pressed', !this.$element.hasClass('active'))
      this.$element.toggleClass('active')
    }
  }


  // BUTTON PLUGIN DEFINITION
  // ========================

  function Plugin(option) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.button')
      var options = typeof option == 'object' && option

      if (!data) $this.data('bs.button', (data = new Button(this, options)))

      if (option == 'toggle') data.toggle()
      else if (option) data.setState(option)
    })
  }

  var old = $.fn.button

  $.fn.button             = Plugin
  $.fn.button.Constructor = Button


  // BUTTON NO CONFLICT
  // ==================

  $.fn.button.noConflict = function () {
    $.fn.button = old
    return this
  }


  // BUTTON DATA-API
  // ===============

  $(document)
    .on('click.bs.button.data-api', '[data-toggle^="button"]', function (e) {
      var $btn = $(e.target).closest('.btn')
      Plugin.call($btn, 'toggle')
      if (!($(e.target).is('input[type="radio"], input[type="checkbox"]'))) {
        // Prevent double click on radios, and the double selections (so cancellation) on checkboxes
        e.preventDefault()
        // The target component still receive the focus
        if ($btn.is('input,button')) $btn.trigger('focus')
        else $btn.find('input:visible,button:visible').first().trigger('focus')
      }
    })
    .on('focus.bs.button.data-api blur.bs.button.data-api', '[data-toggle^="button"]', function (e) {
      $(e.target).closest('.btn').toggleClass('focus', /^focus(in)?$/.test(e.type))
    })

}(jQuery);

/* ========================================================================
 * Bootstrap: carousel.js v3.3.7
 * http://getbootstrap.com/javascript/#carousel
 * ========================================================================
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // CAROUSEL CLASS DEFINITION
  // =========================

  var Carousel = function (element, options) {
    this.$element    = $(element)
    this.$indicators = this.$element.find('.carousel-indicators')
    this.options     = options
    this.paused      = null
    this.sliding     = null
    this.interval    = null
    this.$active     = null
    this.$items      = null

    this.options.keyboard && this.$element.on('keydown.bs.carousel', $.proxy(this.keydown, this))

    this.options.pause == 'hover' && !('ontouchstart' in document.documentElement) && this.$element
      .on('mouseenter.bs.carousel', $.proxy(this.pause, this))
      .on('mouseleave.bs.carousel', $.proxy(this.cycle, this))
  }

  Carousel.VERSION  = '3.3.7'

  Carousel.TRANSITION_DURATION = 600

  Carousel.DEFAULTS = {
    interval: 5000,
    pause: 'hover',
    wrap: true,
    keyboard: true
  }

  Carousel.prototype.keydown = function (e) {
    if (/input|textarea/i.test(e.target.tagName)) return
    switch (e.which) {
      case 37: this.prev(); break
      case 39: this.next(); break
      default: return
    }

    e.preventDefault()
  }

  Carousel.prototype.cycle = function (e) {
    e || (this.paused = false)

    this.interval && clearInterval(this.interval)

    this.options.interval
      && !this.paused
      && (this.interval = setInterval($.proxy(this.next, this), this.options.interval))

    return this
  }

  Carousel.prototype.getItemIndex = function (item) {
    this.$items = item.parent().children('.item')
    return this.$items.index(item || this.$active)
  }

  Carousel.prototype.getItemForDirection = function (direction, active) {
    var activeIndex = this.getItemIndex(active)
    var willWrap = (direction == 'prev' && activeIndex === 0)
                || (direction == 'next' && activeIndex == (this.$items.length - 1))
    if (willWrap && !this.options.wrap) return active
    var delta = direction == 'prev' ? -1 : 1
    var itemIndex = (activeIndex + delta) % this.$items.length
    return this.$items.eq(itemIndex)
  }

  Carousel.prototype.to = function (pos) {
    var that        = this
    var activeIndex = this.getItemIndex(this.$active = this.$element.find('.item.active'))

    if (pos > (this.$items.length - 1) || pos < 0) return

    if (this.sliding)       return this.$element.one('slid.bs.carousel', function () { that.to(pos) }) // yes, "slid"
    if (activeIndex == pos) return this.pause().cycle()

    return this.slide(pos > activeIndex ? 'next' : 'prev', this.$items.eq(pos))
  }

  Carousel.prototype.pause = function (e) {
    e || (this.paused = true)

    if (this.$element.find('.next, .prev').length && $.support.transition) {
      this.$element.trigger($.support.transition.end)
      this.cycle(true)
    }

    this.interval = clearInterval(this.interval)

    return this
  }

  Carousel.prototype.next = function () {
    if (this.sliding) return
    return this.slide('next')
  }

  Carousel.prototype.prev = function () {
    if (this.sliding) return
    return this.slide('prev')
  }

  Carousel.prototype.slide = function (type, next) {
    var $active   = this.$element.find('.item.active')
    var $next     = next || this.getItemForDirection(type, $active)
    var isCycling = this.interval
    var direction = type == 'next' ? 'left' : 'right'
    var that      = this

    if ($next.hasClass('active')) return (this.sliding = false)

    var relatedTarget = $next[0]
    var slideEvent = $.Event('slide.bs.carousel', {
      relatedTarget: relatedTarget,
      direction: direction
    })
    this.$element.trigger(slideEvent)
    if (slideEvent.isDefaultPrevented()) return

    this.sliding = true

    isCycling && this.pause()

    if (this.$indicators.length) {
      this.$indicators.find('.active').removeClass('active')
      var $nextIndicator = $(this.$indicators.children()[this.getItemIndex($next)])
      $nextIndicator && $nextIndicator.addClass('active')
    }

    var slidEvent = $.Event('slid.bs.carousel', { relatedTarget: relatedTarget, direction: direction }) // yes, "slid"
    if ($.support.transition && this.$element.hasClass('slide')) {
      $next.addClass(type)
      $next[0].offsetWidth // force reflow
      $active.addClass(direction)
      $next.addClass(direction)
      $active
        .one('bsTransitionEnd', function () {
          $next.removeClass([type, direction].join(' ')).addClass('active')
          $active.removeClass(['active', direction].join(' '))
          that.sliding = false
          setTimeout(function () {
            that.$element.trigger(slidEvent)
          }, 0)
        })
        .emulateTransitionEnd(Carousel.TRANSITION_DURATION)
    } else {
      $active.removeClass('active')
      $next.addClass('active')
      this.sliding = false
      this.$element.trigger(slidEvent)
    }

    isCycling && this.cycle()

    return this
  }


  // CAROUSEL PLUGIN DEFINITION
  // ==========================

  function Plugin(option) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.carousel')
      var options = $.extend({}, Carousel.DEFAULTS, $this.data(), typeof option == 'object' && option)
      var action  = typeof option == 'string' ? option : options.slide

      if (!data) $this.data('bs.carousel', (data = new Carousel(this, options)))
      if (typeof option == 'number') data.to(option)
      else if (action) data[action]()
      else if (options.interval) data.pause().cycle()
    })
  }

  var old = $.fn.carousel

  $.fn.carousel             = Plugin
  $.fn.carousel.Constructor = Carousel


  // CAROUSEL NO CONFLICT
  // ====================

  $.fn.carousel.noConflict = function () {
    $.fn.carousel = old
    return this
  }


  // CAROUSEL DATA-API
  // =================

  var clickHandler = function (e) {
    var href
    var $this   = $(this)
    var $target = $($this.attr('data-target') || (href = $this.attr('href')) && href.replace(/.*(?=#[^\s]+$)/, '')) // strip for ie7
    if (!$target.hasClass('carousel')) return
    var options = $.extend({}, $target.data(), $this.data())
    var slideIndex = $this.attr('data-slide-to')
    if (slideIndex) options.interval = false

    Plugin.call($target, options)

    if (slideIndex) {
      $target.data('bs.carousel').to(slideIndex)
    }

    e.preventDefault()
  }

  $(document)
    .on('click.bs.carousel.data-api', '[data-slide]', clickHandler)
    .on('click.bs.carousel.data-api', '[data-slide-to]', clickHandler)

  $(window).on('load', function () {
    $('[data-ride="carousel"]').each(function () {
      var $carousel = $(this)
      Plugin.call($carousel, $carousel.data())
    })
  })

}(jQuery);

/* ========================================================================
 * Bootstrap: collapse.js v3.3.7
 * http://getbootstrap.com/javascript/#collapse
 * ========================================================================
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */

/* jshint latedef: false */

+function ($) {
  'use strict';

  // COLLAPSE PUBLIC CLASS DEFINITION
  // ================================

  var Collapse = function (element, options) {
    this.$element      = $(element)
    this.options       = $.extend({}, Collapse.DEFAULTS, options)
    this.$trigger      = $('[data-toggle="collapse"][href="#' + element.id + '"],' +
                           '[data-toggle="collapse"][data-target="#' + element.id + '"]')
    this.transitioning = null

    if (this.options.parent) {
      this.$parent = this.getParent()
    } else {
      this.addAriaAndCollapsedClass(this.$element, this.$trigger)
    }

    if (this.options.toggle) this.toggle()
  }

  Collapse.VERSION  = '3.3.7'

  Collapse.TRANSITION_DURATION = 350

  Collapse.DEFAULTS = {
    toggle: true
  }

  Collapse.prototype.dimension = function () {
    var hasWidth = this.$element.hasClass('width')
    return hasWidth ? 'width' : 'height'
  }

  Collapse.prototype.show = function () {
    if (this.transitioning || this.$element.hasClass('in')) return

    var activesData
    var actives = this.$parent && this.$parent.children('.panel').children('.in, .collapsing')

    if (actives && actives.length) {
      activesData = actives.data('bs.collapse')
      if (activesData && activesData.transitioning) return
    }

    var startEvent = $.Event('show.bs.collapse')
    this.$element.trigger(startEvent)
    if (startEvent.isDefaultPrevented()) return

    if (actives && actives.length) {
      Plugin.call(actives, 'hide')
      activesData || actives.data('bs.collapse', null)
    }

    var dimension = this.dimension()

    this.$element
      .removeClass('collapse')
      .addClass('collapsing')[dimension](0)
      .attr('aria-expanded', true)

    this.$trigger
      .removeClass('collapsed')
      .attr('aria-expanded', true)

    this.transitioning = 1

    var complete = function () {
      this.$element
        .removeClass('collapsing')
        .addClass('collapse in')[dimension]('')
      this.transitioning = 0
      this.$element
        .trigger('shown.bs.collapse')
    }

    if (!$.support.transition) return complete.call(this)

    var scrollSize = $.camelCase(['scroll', dimension].join('-'))

    this.$element
      .one('bsTransitionEnd', $.proxy(complete, this))
      .emulateTransitionEnd(Collapse.TRANSITION_DURATION)[dimension](this.$element[0][scrollSize])
  }

  Collapse.prototype.hide = function () {
    if (this.transitioning || !this.$element.hasClass('in')) return

    var startEvent = $.Event('hide.bs.collapse')
    this.$element.trigger(startEvent)
    if (startEvent.isDefaultPrevented()) return

    var dimension = this.dimension()

    this.$element[dimension](this.$element[dimension]())[0].offsetHeight

    this.$element
      .addClass('collapsing')
      .removeClass('collapse in')
      .attr('aria-expanded', false)

    this.$trigger
      .addClass('collapsed')
      .attr('aria-expanded', false)

    this.transitioning = 1

    var complete = function () {
      this.transitioning = 0
      this.$element
        .removeClass('collapsing')
        .addClass('collapse')
        .trigger('hidden.bs.collapse')
    }

    if (!$.support.transition) return complete.call(this)

    this.$element
      [dimension](0)
      .one('bsTransitionEnd', $.proxy(complete, this))
      .emulateTransitionEnd(Collapse.TRANSITION_DURATION)
  }

  Collapse.prototype.toggle = function () {
    this[this.$element.hasClass('in') ? 'hide' : 'show']()
  }

  Collapse.prototype.getParent = function () {
    return $(this.options.parent)
      .find('[data-toggle="collapse"][data-parent="' + this.options.parent + '"]')
      .each($.proxy(function (i, element) {
        var $element = $(element)
        this.addAriaAndCollapsedClass(getTargetFromTrigger($element), $element)
      }, this))
      .end()
  }

  Collapse.prototype.addAriaAndCollapsedClass = function ($element, $trigger) {
    var isOpen = $element.hasClass('in')

    $element.attr('aria-expanded', isOpen)
    $trigger
      .toggleClass('collapsed', !isOpen)
      .attr('aria-expanded', isOpen)
  }

  function getTargetFromTrigger($trigger) {
    var href
    var target = $trigger.attr('data-target')
      || (href = $trigger.attr('href')) && href.replace(/.*(?=#[^\s]+$)/, '') // strip for ie7

    return $(target)
  }


  // COLLAPSE PLUGIN DEFINITION
  // ==========================

  function Plugin(option) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.collapse')
      var options = $.extend({}, Collapse.DEFAULTS, $this.data(), typeof option == 'object' && option)

      if (!data && options.toggle && /show|hide/.test(option)) options.toggle = false
      if (!data) $this.data('bs.collapse', (data = new Collapse(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  var old = $.fn.collapse

  $.fn.collapse             = Plugin
  $.fn.collapse.Constructor = Collapse


  // COLLAPSE NO CONFLICT
  // ====================

  $.fn.collapse.noConflict = function () {
    $.fn.collapse = old
    return this
  }


  // COLLAPSE DATA-API
  // =================

  $(document).on('click.bs.collapse.data-api', '[data-toggle="collapse"]', function (e) {
    var $this   = $(this)

    if (!$this.attr('data-target')) e.preventDefault()

    var $target = getTargetFromTrigger($this)
    var data    = $target.data('bs.collapse')
    var option  = data ? 'toggle' : $this.data()

    Plugin.call($target, option)
  })

}(jQuery);

/* ========================================================================
 * Bootstrap: dropdown.js v3.3.7
 * http://getbootstrap.com/javascript/#dropdowns
 * ========================================================================
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // DROPDOWN CLASS DEFINITION
  // =========================

  var backdrop = '.dropdown-backdrop'
  var toggle   = '[data-toggle="dropdown"]'
  var Dropdown = function (element) {
    $(element).on('click.bs.dropdown', this.toggle)
  }

  Dropdown.VERSION = '3.3.7'

  function getParent($this) {
    var selector = $this.attr('data-target')

    if (!selector) {
      selector = $this.attr('href')
      selector = selector && /#[A-Za-z]/.test(selector) && selector.replace(/.*(?=#[^\s]*$)/, '') // strip for ie7
    }

    var $parent = selector && $(selector)

    return $parent && $parent.length ? $parent : $this.parent()
  }

  function clearMenus(e) {
    if (e && e.which === 3) return
    $(backdrop).remove()
    $(toggle).each(function () {
      var $this         = $(this)
      var $parent       = getParent($this)
      var relatedTarget = { relatedTarget: this }

      if (!$parent.hasClass('open')) return

      if (e && e.type == 'click' && /input|textarea/i.test(e.target.tagName) && $.contains($parent[0], e.target)) return

      $parent.trigger(e = $.Event('hide.bs.dropdown', relatedTarget))

      if (e.isDefaultPrevented()) return

      $this.attr('aria-expanded', 'false')
      $parent.removeClass('open').trigger($.Event('hidden.bs.dropdown', relatedTarget))
    })
  }

  Dropdown.prototype.toggle = function (e) {
    var $this = $(this)

    if ($this.is('.disabled, :disabled')) return

    var $parent  = getParent($this)
    var isActive = $parent.hasClass('open')

    clearMenus()

    if (!isActive) {
      if ('ontouchstart' in document.documentElement && !$parent.closest('.navbar-nav').length) {
        // if mobile we use a backdrop because click events don't delegate
        $(document.createElement('div'))
          .addClass('dropdown-backdrop')
          .insertAfter($(this))
          .on('click', clearMenus)
      }

      var relatedTarget = { relatedTarget: this }
      $parent.trigger(e = $.Event('show.bs.dropdown', relatedTarget))

      if (e.isDefaultPrevented()) return

      $this
        .trigger('focus')
        .attr('aria-expanded', 'true')

      $parent
        .toggleClass('open')
        .trigger($.Event('shown.bs.dropdown', relatedTarget))
    }

    return false
  }

  Dropdown.prototype.keydown = function (e) {
    if (!/(38|40|27|32)/.test(e.which) || /input|textarea/i.test(e.target.tagName)) return

    var $this = $(this)

    e.preventDefault()
    e.stopPropagation()

    if ($this.is('.disabled, :disabled')) return

    var $parent  = getParent($this)
    var isActive = $parent.hasClass('open')

    if (!isActive && e.which != 27 || isActive && e.which == 27) {
      if (e.which == 27) $parent.find(toggle).trigger('focus')
      return $this.trigger('click')
    }

    var desc = ' li:not(.disabled):visible a'
    var $items = $parent.find('.dropdown-menu' + desc)

    if (!$items.length) return

    var index = $items.index(e.target)

    if (e.which == 38 && index > 0)                 index--         // up
    if (e.which == 40 && index < $items.length - 1) index++         // down
    if (!~index)                                    index = 0

    $items.eq(index).trigger('focus')
  }


  // DROPDOWN PLUGIN DEFINITION
  // ==========================

  function Plugin(option) {
    return this.each(function () {
      var $this = $(this)
      var data  = $this.data('bs.dropdown')

      if (!data) $this.data('bs.dropdown', (data = new Dropdown(this)))
      if (typeof option == 'string') data[option].call($this)
    })
  }

  var old = $.fn.dropdown

  $.fn.dropdown             = Plugin
  $.fn.dropdown.Constructor = Dropdown


  // DROPDOWN NO CONFLICT
  // ====================

  $.fn.dropdown.noConflict = function () {
    $.fn.dropdown = old
    return this
  }


  // APPLY TO STANDARD DROPDOWN ELEMENTS
  // ===================================

  $(document)
    .on('click.bs.dropdown.data-api', clearMenus)
    .on('click.bs.dropdown.data-api', '.dropdown form', function (e) { e.stopPropagation() })
    .on('click.bs.dropdown.data-api', toggle, Dropdown.prototype.toggle)
    .on('keydown.bs.dropdown.data-api', toggle, Dropdown.prototype.keydown)
    .on('keydown.bs.dropdown.data-api', '.dropdown-menu', Dropdown.prototype.keydown)

}(jQuery);

/* ========================================================================
 * Bootstrap: modal.js v3.3.7
 * http://getbootstrap.com/javascript/#modals
 * ========================================================================
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // MODAL CLASS DEFINITION
  // ======================

  var Modal = function (element, options) {
    this.options             = options
    this.$body               = $(document.body)
    this.$element            = $(element)
    this.$dialog             = this.$element.find('.modal-dialog')
    this.$backdrop           = null
    this.isShown             = null
    this.originalBodyPad     = null
    this.scrollbarWidth      = 0
    this.ignoreBackdropClick = false

    if (this.options.remote) {
      this.$element
        .find('.modal-content')
        .load(this.options.remote, $.proxy(function () {
          this.$element.trigger('loaded.bs.modal')
        }, this))
    }
  }

  Modal.VERSION  = '3.3.7'

  Modal.TRANSITION_DURATION = 300
  Modal.BACKDROP_TRANSITION_DURATION = 150

  Modal.DEFAULTS = {
    backdrop: true,
    keyboard: true,
    show: true
  }

  Modal.prototype.toggle = function (_relatedTarget) {
    return this.isShown ? this.hide() : this.show(_relatedTarget)
  }

  Modal.prototype.show = function (_relatedTarget) {
    var that = this
    var e    = $.Event('show.bs.modal', { relatedTarget: _relatedTarget })

    this.$element.trigger(e)

    if (this.isShown || e.isDefaultPrevented()) return

    this.isShown = true

    this.checkScrollbar()
    this.setScrollbar()
    this.$body.addClass('modal-open')

    this.escape()
    this.resize()

    this.$element.on('click.dismiss.bs.modal', '[data-dismiss="modal"]', $.proxy(this.hide, this))

    this.$dialog.on('mousedown.dismiss.bs.modal', function () {
      that.$element.one('mouseup.dismiss.bs.modal', function (e) {
        if ($(e.target).is(that.$element)) that.ignoreBackdropClick = true
      })
    })

    this.backdrop(function () {
      var transition = $.support.transition && that.$element.hasClass('fade')

      if (!that.$element.parent().length) {
        that.$element.appendTo(that.$body) // don't move modals dom position
      }

      that.$element
        .show()
        .scrollTop(0)

      that.adjustDialog()

      if (transition) {
        that.$element[0].offsetWidth // force reflow
      }

      that.$element.addClass('in')

      that.enforceFocus()

      var e = $.Event('shown.bs.modal', { relatedTarget: _relatedTarget })

      transition ?
        that.$dialog // wait for modal to slide in
          .one('bsTransitionEnd', function () {
            that.$element.trigger('focus').trigger(e)
          })
          .emulateTransitionEnd(Modal.TRANSITION_DURATION) :
        that.$element.trigger('focus').trigger(e)
    })
  }

  Modal.prototype.hide = function (e) {
    if (e) e.preventDefault()

    e = $.Event('hide.bs.modal')

    this.$element.trigger(e)

    if (!this.isShown || e.isDefaultPrevented()) return

    this.isShown = false

    this.escape()
    this.resize()

    $(document).off('focusin.bs.modal')

    this.$element
      .removeClass('in')
      .off('click.dismiss.bs.modal')
      .off('mouseup.dismiss.bs.modal')

    this.$dialog.off('mousedown.dismiss.bs.modal')

    $.support.transition && this.$element.hasClass('fade') ?
      this.$element
        .one('bsTransitionEnd', $.proxy(this.hideModal, this))
        .emulateTransitionEnd(Modal.TRANSITION_DURATION) :
      this.hideModal()
  }

  Modal.prototype.enforceFocus = function () {
    $(document)
      .off('focusin.bs.modal') // guard against infinite focus loop
      .on('focusin.bs.modal', $.proxy(function (e) {
        if (document !== e.target &&
            this.$element[0] !== e.target &&
            !this.$element.has(e.target).length) {
          this.$element.trigger('focus')
        }
      }, this))
  }

  Modal.prototype.escape = function () {
    if (this.isShown && this.options.keyboard) {
      this.$element.on('keydown.dismiss.bs.modal', $.proxy(function (e) {
        e.which == 27 && this.hide()
      }, this))
    } else if (!this.isShown) {
      this.$element.off('keydown.dismiss.bs.modal')
    }
  }

  Modal.prototype.resize = function () {
    if (this.isShown) {
      $(window).on('resize.bs.modal', $.proxy(this.handleUpdate, this))
    } else {
      $(window).off('resize.bs.modal')
    }
  }

  Modal.prototype.hideModal = function () {
    var that = this
    this.$element.hide()
    this.backdrop(function () {
      that.$body.removeClass('modal-open')
      that.resetAdjustments()
      that.resetScrollbar()
      that.$element.trigger('hidden.bs.modal')
    })
  }

  Modal.prototype.removeBackdrop = function () {
    this.$backdrop && this.$backdrop.remove()
    this.$backdrop = null
  }

  Modal.prototype.backdrop = function (callback) {
    var that = this
    var animate = this.$element.hasClass('fade') ? 'fade' : ''

    if (this.isShown && this.options.backdrop) {
      var doAnimate = $.support.transition && animate

      this.$backdrop = $(document.createElement('div'))
        .addClass('modal-backdrop ' + animate)
        .appendTo(this.$body)

      this.$element.on('click.dismiss.bs.modal', $.proxy(function (e) {
        if (this.ignoreBackdropClick) {
          this.ignoreBackdropClick = false
          return
        }
        if (e.target !== e.currentTarget) return
        this.options.backdrop == 'static'
          ? this.$element[0].focus()
          : this.hide()
      }, this))

      if (doAnimate) this.$backdrop[0].offsetWidth // force reflow

      this.$backdrop.addClass('in')

      if (!callback) return

      doAnimate ?
        this.$backdrop
          .one('bsTransitionEnd', callback)
          .emulateTransitionEnd(Modal.BACKDROP_TRANSITION_DURATION) :
        callback()

    } else if (!this.isShown && this.$backdrop) {
      this.$backdrop.removeClass('in')

      var callbackRemove = function () {
        that.removeBackdrop()
        callback && callback()
      }
      $.support.transition && this.$element.hasClass('fade') ?
        this.$backdrop
          .one('bsTransitionEnd', callbackRemove)
          .emulateTransitionEnd(Modal.BACKDROP_TRANSITION_DURATION) :
        callbackRemove()

    } else if (callback) {
      callback()
    }
  }

  // these following methods are used to handle overflowing modals

  Modal.prototype.handleUpdate = function () {
    this.adjustDialog()
  }

  Modal.prototype.adjustDialog = function () {
    var modalIsOverflowing = this.$element[0].scrollHeight > document.documentElement.clientHeight

    this.$element.css({
      paddingLeft:  !this.bodyIsOverflowing && modalIsOverflowing ? this.scrollbarWidth : '',
      paddingRight: this.bodyIsOverflowing && !modalIsOverflowing ? this.scrollbarWidth : ''
    })
  }

  Modal.prototype.resetAdjustments = function () {
    this.$element.css({
      paddingLeft: '',
      paddingRight: ''
    })
  }

  Modal.prototype.checkScrollbar = function () {
    var fullWindowWidth = window.innerWidth
    if (!fullWindowWidth) { // workaround for missing window.innerWidth in IE8
      var documentElementRect = document.documentElement.getBoundingClientRect()
      fullWindowWidth = documentElementRect.right - Math.abs(documentElementRect.left)
    }
    this.bodyIsOverflowing = document.body.clientWidth < fullWindowWidth
    this.scrollbarWidth = this.measureScrollbar()
  }

  Modal.prototype.setScrollbar = function () {
    var bodyPad = parseInt((this.$body.css('padding-right') || 0), 10)
    this.originalBodyPad = document.body.style.paddingRight || ''
    if (this.bodyIsOverflowing) this.$body.css('padding-right', bodyPad + this.scrollbarWidth)
  }

  Modal.prototype.resetScrollbar = function () {
    this.$body.css('padding-right', this.originalBodyPad)
  }

  Modal.prototype.measureScrollbar = function () { // thx walsh
    var scrollDiv = document.createElement('div')
    scrollDiv.className = 'modal-scrollbar-measure'
    this.$body.append(scrollDiv)
    var scrollbarWidth = scrollDiv.offsetWidth - scrollDiv.clientWidth
    this.$body[0].removeChild(scrollDiv)
    return scrollbarWidth
  }


  // MODAL PLUGIN DEFINITION
  // =======================

  function Plugin(option, _relatedTarget) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.modal')
      var options = $.extend({}, Modal.DEFAULTS, $this.data(), typeof option == 'object' && option)

      if (!data) $this.data('bs.modal', (data = new Modal(this, options)))
      if (typeof option == 'string') data[option](_relatedTarget)
      else if (options.show) data.show(_relatedTarget)
    })
  }

  var old = $.fn.modal

  $.fn.modal             = Plugin
  $.fn.modal.Constructor = Modal


  // MODAL NO CONFLICT
  // =================

  $.fn.modal.noConflict = function () {
    $.fn.modal = old
    return this
  }


  // MODAL DATA-API
  // ==============

  $(document).on('click.bs.modal.data-api', '[data-toggle="modal"]', function (e) {
    var $this   = $(this)
    var href    = $this.attr('href')
    var $target = $($this.attr('data-target') || (href && href.replace(/.*(?=#[^\s]+$)/, ''))) // strip for ie7
    var option  = $target.data('bs.modal') ? 'toggle' : $.extend({ remote: !/#/.test(href) && href }, $target.data(), $this.data())

    if ($this.is('a')) e.preventDefault()

    $target.one('show.bs.modal', function (showEvent) {
      if (showEvent.isDefaultPrevented()) return // only register focus restorer if modal will actually get shown
      $target.one('hidden.bs.modal', function () {
        $this.is(':visible') && $this.trigger('focus')
      })
    })
    Plugin.call($target, option, this)
  })

}(jQuery);

/* ========================================================================
 * Bootstrap: tooltip.js v3.3.7
 * http://getbootstrap.com/javascript/#tooltip
 * Inspired by the original jQuery.tipsy by Jason Frame
 * ========================================================================
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // TOOLTIP PUBLIC CLASS DEFINITION
  // ===============================

  var Tooltip = function (element, options) {
    this.type       = null
    this.options    = null
    this.enabled    = null
    this.timeout    = null
    this.hoverState = null
    this.$element   = null
    this.inState    = null

    this.init('tooltip', element, options)
  }

  Tooltip.VERSION  = '3.3.7'

  Tooltip.TRANSITION_DURATION = 150

  Tooltip.DEFAULTS = {
    animation: true,
    placement: 'top',
    selector: false,
    template: '<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>',
    trigger: 'hover focus',
    title: '',
    delay: 0,
    html: false,
    container: false,
    viewport: {
      selector: 'body',
      padding: 0
    }
  }

  Tooltip.prototype.init = function (type, element, options) {
    this.enabled   = true
    this.type      = type
    this.$element  = $(element)
    this.options   = this.getOptions(options)
    this.$viewport = this.options.viewport && $($.isFunction(this.options.viewport) ? this.options.viewport.call(this, this.$element) : (this.options.viewport.selector || this.options.viewport))
    this.inState   = { click: false, hover: false, focus: false }

    if (this.$element[0] instanceof document.constructor && !this.options.selector) {
      throw new Error('`selector` option must be specified when initializing ' + this.type + ' on the window.document object!')
    }

    var triggers = this.options.trigger.split(' ')

    for (var i = triggers.length; i--;) {
      var trigger = triggers[i]

      if (trigger == 'click') {
        this.$element.on('click.' + this.type, this.options.selector, $.proxy(this.toggle, this))
      } else if (trigger != 'manual') {
        var eventIn  = trigger == 'hover' ? 'mouseenter' : 'focusin'
        var eventOut = trigger == 'hover' ? 'mouseleave' : 'focusout'

        this.$element.on(eventIn  + '.' + this.type, this.options.selector, $.proxy(this.enter, this))
        this.$element.on(eventOut + '.' + this.type, this.options.selector, $.proxy(this.leave, this))
      }
    }

    this.options.selector ?
      (this._options = $.extend({}, this.options, { trigger: 'manual', selector: '' })) :
      this.fixTitle()
  }

  Tooltip.prototype.getDefaults = function () {
    return Tooltip.DEFAULTS
  }

  Tooltip.prototype.getOptions = function (options) {
    options = $.extend({}, this.getDefaults(), this.$element.data(), options)

    if (options.delay && typeof options.delay == 'number') {
      options.delay = {
        show: options.delay,
        hide: options.delay
      }
    }

    return options
  }

  Tooltip.prototype.getDelegateOptions = function () {
    var options  = {}
    var defaults = this.getDefaults()

    this._options && $.each(this._options, function (key, value) {
      if (defaults[key] != value) options[key] = value
    })

    return options
  }

  Tooltip.prototype.enter = function (obj) {
    var self = obj instanceof this.constructor ?
      obj : $(obj.currentTarget).data('bs.' + this.type)

    if (!self) {
      self = new this.constructor(obj.currentTarget, this.getDelegateOptions())
      $(obj.currentTarget).data('bs.' + this.type, self)
    }

    if (obj instanceof $.Event) {
      self.inState[obj.type == 'focusin' ? 'focus' : 'hover'] = true
    }

    if (self.tip().hasClass('in') || self.hoverState == 'in') {
      self.hoverState = 'in'
      return
    }

    clearTimeout(self.timeout)

    self.hoverState = 'in'

    if (!self.options.delay || !self.options.delay.show) return self.show()

    self.timeout = setTimeout(function () {
      if (self.hoverState == 'in') self.show()
    }, self.options.delay.show)
  }

  Tooltip.prototype.isInStateTrue = function () {
    for (var key in this.inState) {
      if (this.inState[key]) return true
    }

    return false
  }

  Tooltip.prototype.leave = function (obj) {
    var self = obj instanceof this.constructor ?
      obj : $(obj.currentTarget).data('bs.' + this.type)

    if (!self) {
      self = new this.constructor(obj.currentTarget, this.getDelegateOptions())
      $(obj.currentTarget).data('bs.' + this.type, self)
    }

    if (obj instanceof $.Event) {
      self.inState[obj.type == 'focusout' ? 'focus' : 'hover'] = false
    }

    if (self.isInStateTrue()) return

    clearTimeout(self.timeout)

    self.hoverState = 'out'

    if (!self.options.delay || !self.options.delay.hide) return self.hide()

    self.timeout = setTimeout(function () {
      if (self.hoverState == 'out') self.hide()
    }, self.options.delay.hide)
  }

  Tooltip.prototype.show = function () {
    var e = $.Event('show.bs.' + this.type)

    if (this.hasContent() && this.enabled) {
      this.$element.trigger(e)

      var inDom = $.contains(this.$element[0].ownerDocument.documentElement, this.$element[0])
      if (e.isDefaultPrevented() || !inDom) return
      var that = this

      var $tip = this.tip()

      var tipId = this.getUID(this.type)

      this.setContent()
      $tip.attr('id', tipId)
      this.$element.attr('aria-describedby', tipId)

      if (this.options.animation) $tip.addClass('fade')

      var placement = typeof this.options.placement == 'function' ?
        this.options.placement.call(this, $tip[0], this.$element[0]) :
        this.options.placement

      var autoToken = /\s?auto?\s?/i
      var autoPlace = autoToken.test(placement)
      if (autoPlace) placement = placement.replace(autoToken, '') || 'top'

      $tip
        .detach()
        .css({ top: 0, left: 0, display: 'block' })
        .addClass(placement)
        .data('bs.' + this.type, this)

      this.options.container ? $tip.appendTo(this.options.container) : $tip.insertAfter(this.$element)
      this.$element.trigger('inserted.bs.' + this.type)

      var pos          = this.getPosition()
      var actualWidth  = $tip[0].offsetWidth
      var actualHeight = $tip[0].offsetHeight

      if (autoPlace) {
        var orgPlacement = placement
        var viewportDim = this.getPosition(this.$viewport)

        placement = placement == 'bottom' && pos.bottom + actualHeight > viewportDim.bottom ? 'top'    :
                    placement == 'top'    && pos.top    - actualHeight < viewportDim.top    ? 'bottom' :
                    placement == 'right'  && pos.right  + actualWidth  > viewportDim.width  ? 'left'   :
                    placement == 'left'   && pos.left   - actualWidth  < viewportDim.left   ? 'right'  :
                    placement

        $tip
          .removeClass(orgPlacement)
          .addClass(placement)
      }

      var calculatedOffset = this.getCalculatedOffset(placement, pos, actualWidth, actualHeight)

      this.applyPlacement(calculatedOffset, placement)

      var complete = function () {
        var prevHoverState = that.hoverState
        that.$element.trigger('shown.bs.' + that.type)
        that.hoverState = null

        if (prevHoverState == 'out') that.leave(that)
      }

      $.support.transition && this.$tip.hasClass('fade') ?
        $tip
          .one('bsTransitionEnd', complete)
          .emulateTransitionEnd(Tooltip.TRANSITION_DURATION) :
        complete()
    }
  }

  Tooltip.prototype.applyPlacement = function (offset, placement) {
    var $tip   = this.tip()
    var width  = $tip[0].offsetWidth
    var height = $tip[0].offsetHeight

    // manually read margins because getBoundingClientRect includes difference
    var marginTop = parseInt($tip.css('margin-top'), 10)
    var marginLeft = parseInt($tip.css('margin-left'), 10)

    // we must check for NaN for ie 8/9
    if (isNaN(marginTop))  marginTop  = 0
    if (isNaN(marginLeft)) marginLeft = 0

    offset.top  += marginTop
    offset.left += marginLeft

    // $.fn.offset doesn't round pixel values
    // so we use setOffset directly with our own function B-0
    $.offset.setOffset($tip[0], $.extend({
      using: function (props) {
        $tip.css({
          top: Math.round(props.top),
          left: Math.round(props.left)
        })
      }
    }, offset), 0)

    $tip.addClass('in')

    // check to see if placing tip in new offset caused the tip to resize itself
    var actualWidth  = $tip[0].offsetWidth
    var actualHeight = $tip[0].offsetHeight

    if (placement == 'top' && actualHeight != height) {
      offset.top = offset.top + height - actualHeight
    }

    var delta = this.getViewportAdjustedDelta(placement, offset, actualWidth, actualHeight)

    if (delta.left) offset.left += delta.left
    else offset.top += delta.top

    var isVertical          = /top|bottom/.test(placement)
    var arrowDelta          = isVertical ? delta.left * 2 - width + actualWidth : delta.top * 2 - height + actualHeight
    var arrowOffsetPosition = isVertical ? 'offsetWidth' : 'offsetHeight'

    $tip.offset(offset)
    this.replaceArrow(arrowDelta, $tip[0][arrowOffsetPosition], isVertical)
  }

  Tooltip.prototype.replaceArrow = function (delta, dimension, isVertical) {
    this.arrow()
      .css(isVertical ? 'left' : 'top', 50 * (1 - delta / dimension) + '%')
      .css(isVertical ? 'top' : 'left', '')
  }

  Tooltip.prototype.setContent = function () {
    var $tip  = this.tip()
    var title = this.getTitle()

    $tip.find('.tooltip-inner')[this.options.html ? 'html' : 'text'](title)
    $tip.removeClass('fade in top bottom left right')
  }

  Tooltip.prototype.hide = function (callback) {
    var that = this
    var $tip = $(this.$tip)
    var e    = $.Event('hide.bs.' + this.type)

    function complete() {
      if (that.hoverState != 'in') $tip.detach()
      if (that.$element) { // TODO: Check whether guarding this code with this `if` is really necessary.
        that.$element
          .removeAttr('aria-describedby')
          .trigger('hidden.bs.' + that.type)
      }
      callback && callback()
    }

    this.$element.trigger(e)

    if (e.isDefaultPrevented()) return

    $tip.removeClass('in')

    $.support.transition && $tip.hasClass('fade') ?
      $tip
        .one('bsTransitionEnd', complete)
        .emulateTransitionEnd(Tooltip.TRANSITION_DURATION) :
      complete()

    this.hoverState = null

    return this
  }

  Tooltip.prototype.fixTitle = function () {
    var $e = this.$element
    if ($e.attr('title') || typeof $e.attr('data-original-title') != 'string') {
      $e.attr('data-original-title', $e.attr('title') || '').attr('title', '')
    }
  }

  Tooltip.prototype.hasContent = function () {
    return this.getTitle()
  }

  Tooltip.prototype.getPosition = function ($element) {
    $element   = $element || this.$element

    var el     = $element[0]
    var isBody = el.tagName == 'BODY'

    var elRect    = el.getBoundingClientRect()
    if (elRect.width == null) {
      // width and height are missing in IE8, so compute them manually; see https://github.com/twbs/bootstrap/issues/14093
      elRect = $.extend({}, elRect, { width: elRect.right - elRect.left, height: elRect.bottom - elRect.top })
    }
    var isSvg = window.SVGElement && el instanceof window.SVGElement
    // Avoid using $.offset() on SVGs since it gives incorrect results in jQuery 3.
    // See https://github.com/twbs/bootstrap/issues/20280
    var elOffset  = isBody ? { top: 0, left: 0 } : (isSvg ? null : $element.offset())
    var scroll    = { scroll: isBody ? document.documentElement.scrollTop || document.body.scrollTop : $element.scrollTop() }
    var outerDims = isBody ? { width: $(window).width(), height: $(window).height() } : null

    return $.extend({}, elRect, scroll, outerDims, elOffset)
  }

  Tooltip.prototype.getCalculatedOffset = function (placement, pos, actualWidth, actualHeight) {
    return placement == 'bottom' ? { top: pos.top + pos.height,   left: pos.left + pos.width / 2 - actualWidth / 2 } :
           placement == 'top'    ? { top: pos.top - actualHeight, left: pos.left + pos.width / 2 - actualWidth / 2 } :
           placement == 'left'   ? { top: pos.top + pos.height / 2 - actualHeight / 2, left: pos.left - actualWidth } :
        /* placement == 'right' */ { top: pos.top + pos.height / 2 - actualHeight / 2, left: pos.left + pos.width }

  }

  Tooltip.prototype.getViewportAdjustedDelta = function (placement, pos, actualWidth, actualHeight) {
    var delta = { top: 0, left: 0 }
    if (!this.$viewport) return delta

    var viewportPadding = this.options.viewport && this.options.viewport.padding || 0
    var viewportDimensions = this.getPosition(this.$viewport)

    if (/right|left/.test(placement)) {
      var topEdgeOffset    = pos.top - viewportPadding - viewportDimensions.scroll
      var bottomEdgeOffset = pos.top + viewportPadding - viewportDimensions.scroll + actualHeight
      if (topEdgeOffset < viewportDimensions.top) { // top overflow
        delta.top = viewportDimensions.top - topEdgeOffset
      } else if (bottomEdgeOffset > viewportDimensions.top + viewportDimensions.height) { // bottom overflow
        delta.top = viewportDimensions.top + viewportDimensions.height - bottomEdgeOffset
      }
    } else {
      var leftEdgeOffset  = pos.left - viewportPadding
      var rightEdgeOffset = pos.left + viewportPadding + actualWidth
      if (leftEdgeOffset < viewportDimensions.left) { // left overflow
        delta.left = viewportDimensions.left - leftEdgeOffset
      } else if (rightEdgeOffset > viewportDimensions.right) { // right overflow
        delta.left = viewportDimensions.left + viewportDimensions.width - rightEdgeOffset
      }
    }

    return delta
  }

  Tooltip.prototype.getTitle = function () {
    var title
    var $e = this.$element
    var o  = this.options

    title = $e.attr('data-original-title')
      || (typeof o.title == 'function' ? o.title.call($e[0]) :  o.title)

    return title
  }

  Tooltip.prototype.getUID = function (prefix) {
    do prefix += ~~(Math.random() * 1000000)
    while (document.getElementById(prefix))
    return prefix
  }

  Tooltip.prototype.tip = function () {
    if (!this.$tip) {
      this.$tip = $(this.options.template)
      if (this.$tip.length != 1) {
        throw new Error(this.type + ' `template` option must consist of exactly 1 top-level element!')
      }
    }
    return this.$tip
  }

  Tooltip.prototype.arrow = function () {
    return (this.$arrow = this.$arrow || this.tip().find('.tooltip-arrow'))
  }

  Tooltip.prototype.enable = function () {
    this.enabled = true
  }

  Tooltip.prototype.disable = function () {
    this.enabled = false
  }

  Tooltip.prototype.toggleEnabled = function () {
    this.enabled = !this.enabled
  }

  Tooltip.prototype.toggle = function (e) {
    var self = this
    if (e) {
      self = $(e.currentTarget).data('bs.' + this.type)
      if (!self) {
        self = new this.constructor(e.currentTarget, this.getDelegateOptions())
        $(e.currentTarget).data('bs.' + this.type, self)
      }
    }

    if (e) {
      self.inState.click = !self.inState.click
      if (self.isInStateTrue()) self.enter(self)
      else self.leave(self)
    } else {
      self.tip().hasClass('in') ? self.leave(self) : self.enter(self)
    }
  }

  Tooltip.prototype.destroy = function () {
    var that = this
    clearTimeout(this.timeout)
    this.hide(function () {
      that.$element.off('.' + that.type).removeData('bs.' + that.type)
      if (that.$tip) {
        that.$tip.detach()
      }
      that.$tip = null
      that.$arrow = null
      that.$viewport = null
      that.$element = null
    })
  }


  // TOOLTIP PLUGIN DEFINITION
  // =========================

  function Plugin(option) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.tooltip')
      var options = typeof option == 'object' && option

      if (!data && /destroy|hide/.test(option)) return
      if (!data) $this.data('bs.tooltip', (data = new Tooltip(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  var old = $.fn.tooltip

  $.fn.tooltip             = Plugin
  $.fn.tooltip.Constructor = Tooltip


  // TOOLTIP NO CONFLICT
  // ===================

  $.fn.tooltip.noConflict = function () {
    $.fn.tooltip = old
    return this
  }

}(jQuery);

/* ========================================================================
 * Bootstrap: popover.js v3.3.7
 * http://getbootstrap.com/javascript/#popovers
 * ========================================================================
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // POPOVER PUBLIC CLASS DEFINITION
  // ===============================

  var Popover = function (element, options) {
    this.init('popover', element, options)
  }

  if (!$.fn.tooltip) throw new Error('Popover requires tooltip.js')

  Popover.VERSION  = '3.3.7'

  Popover.DEFAULTS = $.extend({}, $.fn.tooltip.Constructor.DEFAULTS, {
    placement: 'right',
    trigger: 'click',
    content: '',
    template: '<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'
  })


  // NOTE: POPOVER EXTENDS tooltip.js
  // ================================

  Popover.prototype = $.extend({}, $.fn.tooltip.Constructor.prototype)

  Popover.prototype.constructor = Popover

  Popover.prototype.getDefaults = function () {
    return Popover.DEFAULTS
  }

  Popover.prototype.setContent = function () {
    var $tip    = this.tip()
    var title   = this.getTitle()
    var content = this.getContent()

    $tip.find('.popover-title')[this.options.html ? 'html' : 'text'](title)
    $tip.find('.popover-content').children().detach().end()[ // we use append for html objects to maintain js events
      this.options.html ? (typeof content == 'string' ? 'html' : 'append') : 'text'
    ](content)

    $tip.removeClass('fade top bottom left right in')

    // IE8 doesn't accept hiding via the `:empty` pseudo selector, we have to do
    // this manually by checking the contents.
    if (!$tip.find('.popover-title').html()) $tip.find('.popover-title').hide()
  }

  Popover.prototype.hasContent = function () {
    return this.getTitle() || this.getContent()
  }

  Popover.prototype.getContent = function () {
    var $e = this.$element
    var o  = this.options

    return $e.attr('data-content')
      || (typeof o.content == 'function' ?
            o.content.call($e[0]) :
            o.content)
  }

  Popover.prototype.arrow = function () {
    return (this.$arrow = this.$arrow || this.tip().find('.arrow'))
  }


  // POPOVER PLUGIN DEFINITION
  // =========================

  function Plugin(option) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.popover')
      var options = typeof option == 'object' && option

      if (!data && /destroy|hide/.test(option)) return
      if (!data) $this.data('bs.popover', (data = new Popover(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  var old = $.fn.popover

  $.fn.popover             = Plugin
  $.fn.popover.Constructor = Popover


  // POPOVER NO CONFLICT
  // ===================

  $.fn.popover.noConflict = function () {
    $.fn.popover = old
    return this
  }

}(jQuery);

/* ========================================================================
 * Bootstrap: scrollspy.js v3.3.7
 * http://getbootstrap.com/javascript/#scrollspy
 * ========================================================================
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // SCROLLSPY CLASS DEFINITION
  // ==========================

  function ScrollSpy(element, options) {
    this.$body          = $(document.body)
    this.$scrollElement = $(element).is(document.body) ? $(window) : $(element)
    this.options        = $.extend({}, ScrollSpy.DEFAULTS, options)
    this.selector       = (this.options.target || '') + ' .nav li > a'
    this.offsets        = []
    this.targets        = []
    this.activeTarget   = null
    this.scrollHeight   = 0

    this.$scrollElement.on('scroll.bs.scrollspy', $.proxy(this.process, this))
    this.refresh()
    this.process()
  }

  ScrollSpy.VERSION  = '3.3.7'

  ScrollSpy.DEFAULTS = {
    offset: 10
  }

  ScrollSpy.prototype.getScrollHeight = function () {
    return this.$scrollElement[0].scrollHeight || Math.max(this.$body[0].scrollHeight, document.documentElement.scrollHeight)
  }

  ScrollSpy.prototype.refresh = function () {
    var that          = this
    var offsetMethod  = 'offset'
    var offsetBase    = 0

    this.offsets      = []
    this.targets      = []
    this.scrollHeight = this.getScrollHeight()

    if (!$.isWindow(this.$scrollElement[0])) {
      offsetMethod = 'position'
      offsetBase   = this.$scrollElement.scrollTop()
    }

    this.$body
      .find(this.selector)
      .map(function () {
        var $el   = $(this)
        var href  = $el.data('target') || $el.attr('href')
        var $href = /^#./.test(href) && $(href)

        return ($href
          && $href.length
          && $href.is(':visible')
          && [[$href[offsetMethod]().top + offsetBase, href]]) || null
      })
      .sort(function (a, b) { return a[0] - b[0] })
      .each(function () {
        that.offsets.push(this[0])
        that.targets.push(this[1])
      })
  }

  ScrollSpy.prototype.process = function () {
    var scrollTop    = this.$scrollElement.scrollTop() + this.options.offset
    var scrollHeight = this.getScrollHeight()
    var maxScroll    = this.options.offset + scrollHeight - this.$scrollElement.height()
    var offsets      = this.offsets
    var targets      = this.targets
    var activeTarget = this.activeTarget
    var i

    if (this.scrollHeight != scrollHeight) {
      this.refresh()
    }

    if (scrollTop >= maxScroll) {
      return activeTarget != (i = targets[targets.length - 1]) && this.activate(i)
    }

    if (activeTarget && scrollTop < offsets[0]) {
      this.activeTarget = null
      return this.clear()
    }

    for (i = offsets.length; i--;) {
      activeTarget != targets[i]
        && scrollTop >= offsets[i]
        && (offsets[i + 1] === undefined || scrollTop < offsets[i + 1])
        && this.activate(targets[i])
    }
  }

  ScrollSpy.prototype.activate = function (target) {
    this.activeTarget = target

    this.clear()

    var selector = this.selector +
      '[data-target="' + target + '"],' +
      this.selector + '[href="' + target + '"]'

    var active = $(selector)
      .parents('li')
      .addClass('active')

    if (active.parent('.dropdown-menu').length) {
      active = active
        .closest('li.dropdown')
        .addClass('active')
    }

    active.trigger('activate.bs.scrollspy')
  }

  ScrollSpy.prototype.clear = function () {
    $(this.selector)
      .parentsUntil(this.options.target, '.active')
      .removeClass('active')
  }


  // SCROLLSPY PLUGIN DEFINITION
  // ===========================

  function Plugin(option) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.scrollspy')
      var options = typeof option == 'object' && option

      if (!data) $this.data('bs.scrollspy', (data = new ScrollSpy(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  var old = $.fn.scrollspy

  $.fn.scrollspy             = Plugin
  $.fn.scrollspy.Constructor = ScrollSpy


  // SCROLLSPY NO CONFLICT
  // =====================

  $.fn.scrollspy.noConflict = function () {
    $.fn.scrollspy = old
    return this
  }


  // SCROLLSPY DATA-API
  // ==================

  $(window).on('load.bs.scrollspy.data-api', function () {
    $('[data-spy="scroll"]').each(function () {
      var $spy = $(this)
      Plugin.call($spy, $spy.data())
    })
  })

}(jQuery);

/* ========================================================================
 * Bootstrap: tab.js v3.3.7
 * http://getbootstrap.com/javascript/#tabs
 * ========================================================================
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // TAB CLASS DEFINITION
  // ====================

  var Tab = function (element) {
    // jscs:disable requireDollarBeforejQueryAssignment
    this.element = $(element)
    // jscs:enable requireDollarBeforejQueryAssignment
  }

  Tab.VERSION = '3.3.7'

  Tab.TRANSITION_DURATION = 150

  Tab.prototype.show = function () {
    var $this    = this.element
    var $ul      = $this.closest('ul:not(.dropdown-menu)')
    var selector = $this.data('target')

    if (!selector) {
      selector = $this.attr('href')
      selector = selector && selector.replace(/.*(?=#[^\s]*$)/, '') // strip for ie7
    }

    if ($this.parent('li').hasClass('active')) return

    var $previous = $ul.find('.active:last a')
    var hideEvent = $.Event('hide.bs.tab', {
      relatedTarget: $this[0]
    })
    var showEvent = $.Event('show.bs.tab', {
      relatedTarget: $previous[0]
    })

    $previous.trigger(hideEvent)
    $this.trigger(showEvent)

    if (showEvent.isDefaultPrevented() || hideEvent.isDefaultPrevented()) return

    var $target = $(selector)

    this.activate($this.closest('li'), $ul)
    this.activate($target, $target.parent(), function () {
      $previous.trigger({
        type: 'hidden.bs.tab',
        relatedTarget: $this[0]
      })
      $this.trigger({
        type: 'shown.bs.tab',
        relatedTarget: $previous[0]
      })
    })
  }

  Tab.prototype.activate = function (element, container, callback) {
    var $active    = container.find('> .active')
    var transition = callback
      && $.support.transition
      && ($active.length && $active.hasClass('fade') || !!container.find('> .fade').length)

    function next() {
      $active
        .removeClass('active')
        .find('> .dropdown-menu > .active')
          .removeClass('active')
        .end()
        .find('[data-toggle="tab"]')
          .attr('aria-expanded', false)

      element
        .addClass('active')
        .find('[data-toggle="tab"]')
          .attr('aria-expanded', true)

      if (transition) {
        element[0].offsetWidth // reflow for transition
        element.addClass('in')
      } else {
        element.removeClass('fade')
      }

      if (element.parent('.dropdown-menu').length) {
        element
          .closest('li.dropdown')
            .addClass('active')
          .end()
          .find('[data-toggle="tab"]')
            .attr('aria-expanded', true)
      }

      callback && callback()
    }

    $active.length && transition ?
      $active
        .one('bsTransitionEnd', next)
        .emulateTransitionEnd(Tab.TRANSITION_DURATION) :
      next()

    $active.removeClass('in')
  }


  // TAB PLUGIN DEFINITION
  // =====================

  function Plugin(option) {
    return this.each(function () {
      var $this = $(this)
      var data  = $this.data('bs.tab')

      if (!data) $this.data('bs.tab', (data = new Tab(this)))
      if (typeof option == 'string') data[option]()
    })
  }

  var old = $.fn.tab

  $.fn.tab             = Plugin
  $.fn.tab.Constructor = Tab


  // TAB NO CONFLICT
  // ===============

  $.fn.tab.noConflict = function () {
    $.fn.tab = old
    return this
  }


  // TAB DATA-API
  // ============

  var clickHandler = function (e) {
    e.preventDefault()
    Plugin.call($(this), 'show')
  }

  $(document)
    .on('click.bs.tab.data-api', '[data-toggle="tab"]', clickHandler)
    .on('click.bs.tab.data-api', '[data-toggle="pill"]', clickHandler)

}(jQuery);

/* ========================================================================
 * Bootstrap: affix.js v3.3.7
 * http://getbootstrap.com/javascript/#affix
 * ========================================================================
 * Copyright 2011-2016 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ======================================================================== */


+function ($) {
  'use strict';

  // AFFIX CLASS DEFINITION
  // ======================

  var Affix = function (element, options) {
    this.options = $.extend({}, Affix.DEFAULTS, options)

    this.$target = $(this.options.target)
      .on('scroll.bs.affix.data-api', $.proxy(this.checkPosition, this))
      .on('click.bs.affix.data-api',  $.proxy(this.checkPositionWithEventLoop, this))

    this.$element     = $(element)
    this.affixed      = null
    this.unpin        = null
    this.pinnedOffset = null

    this.checkPosition()
  }

  Affix.VERSION  = '3.3.7'

  Affix.RESET    = 'affix affix-top affix-bottom'

  Affix.DEFAULTS = {
    offset: 0,
    target: window
  }

  Affix.prototype.getState = function (scrollHeight, height, offsetTop, offsetBottom) {
    var scrollTop    = this.$target.scrollTop()
    var position     = this.$element.offset()
    var targetHeight = this.$target.height()

    if (offsetTop != null && this.affixed == 'top') return scrollTop < offsetTop ? 'top' : false

    if (this.affixed == 'bottom') {
      if (offsetTop != null) return (scrollTop + this.unpin <= position.top) ? false : 'bottom'
      return (scrollTop + targetHeight <= scrollHeight - offsetBottom) ? false : 'bottom'
    }

    var initializing   = this.affixed == null
    var colliderTop    = initializing ? scrollTop : position.top
    var colliderHeight = initializing ? targetHeight : height

    if (offsetTop != null && scrollTop <= offsetTop) return 'top'
    if (offsetBottom != null && (colliderTop + colliderHeight >= scrollHeight - offsetBottom)) return 'bottom'

    return false
  }

  Affix.prototype.getPinnedOffset = function () {
    if (this.pinnedOffset) return this.pinnedOffset
    this.$element.removeClass(Affix.RESET).addClass('affix')
    var scrollTop = this.$target.scrollTop()
    var position  = this.$element.offset()
    return (this.pinnedOffset = position.top - scrollTop)
  }

  Affix.prototype.checkPositionWithEventLoop = function () {
    setTimeout($.proxy(this.checkPosition, this), 1)
  }

  Affix.prototype.checkPosition = function () {
    if (!this.$element.is(':visible')) return

    var height       = this.$element.height()
    var offset       = this.options.offset
    var offsetTop    = offset.top
    var offsetBottom = offset.bottom
    var scrollHeight = Math.max($(document).height(), $(document.body).height())

    if (typeof offset != 'object')         offsetBottom = offsetTop = offset
    if (typeof offsetTop == 'function')    offsetTop    = offset.top(this.$element)
    if (typeof offsetBottom == 'function') offsetBottom = offset.bottom(this.$element)

    var affix = this.getState(scrollHeight, height, offsetTop, offsetBottom)

    if (this.affixed != affix) {
      if (this.unpin != null) this.$element.css('top', '')

      var affixType = 'affix' + (affix ? '-' + affix : '')
      var e         = $.Event(affixType + '.bs.affix')

      this.$element.trigger(e)

      if (e.isDefaultPrevented()) return

      this.affixed = affix
      this.unpin = affix == 'bottom' ? this.getPinnedOffset() : null

      this.$element
        .removeClass(Affix.RESET)
        .addClass(affixType)
        .trigger(affixType.replace('affix', 'affixed') + '.bs.affix')
    }

    if (affix == 'bottom') {
      this.$element.offset({
        top: scrollHeight - height - offsetBottom
      })
    }
  }


  // AFFIX PLUGIN DEFINITION
  // =======================

  function Plugin(option) {
    return this.each(function () {
      var $this   = $(this)
      var data    = $this.data('bs.affix')
      var options = typeof option == 'object' && option

      if (!data) $this.data('bs.affix', (data = new Affix(this, options)))
      if (typeof option == 'string') data[option]()
    })
  }

  var old = $.fn.affix

  $.fn.affix             = Plugin
  $.fn.affix.Constructor = Affix


  // AFFIX NO CONFLICT
  // =================

  $.fn.affix.noConflict = function () {
    $.fn.affix = old
    return this
  }


  // AFFIX DATA-API
  // ==============

  $(window).on('load', function () {
    $('[data-spy="affix"]').each(function () {
      var $spy = $(this)
      var data = $spy.data()

      data.offset = data.offset || {}

      if (data.offsetBottom != null) data.offset.bottom = data.offsetBottom
      if (data.offsetTop    != null) data.offset.top    = data.offsetTop

      Plugin.call($spy, data)
    })
  })

}(jQuery);

function inputMoneyWhitCurrency(currency_id){
	console.log('Entre a inputMoneyWhitCurrency(currency_id)');
	$('#modal-header').empty();
	$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
	$('#modal-header').append('<h4 class="modal-title">Ingresar dinero</h4>');
	loading();
	var url = 'http://'+document.domain+':'+location.port+'/ajax/currencies/'+currency_id;
	$.get(url, function(selected_currency){
		$('#modal-header').empty();
		$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
		$('#modal-header').append('<h4 class="modal-title">Ingresar dinero en '+ selected_currency.name+'</h4>');

		$('#modal-body').empty();
		$('#modal-body').append('<label>Seleccione la divisa</label>');
		url = 'http://'+document.domain+':'+location.port+'/ajax/currencies';
		$.get(url, function(currencies){
			console.log('Entre al select');
			console.log(currencies);
			$('#modal-body').append('<div class="form-group">');
			$('#modal-body').append('<select class="form-control" name="currency_id" id="currency_id" onchange="inputMoneyWhitCurrency(value)"></select>');
			$('#currency_id').append('<option value="' + selected_currency.id + ' selected">' + selected_currency.name + '</option>');			
			$.each(currencies,function(index,currency){
    	       if (currency.id != selected_currency.id) {
					$('#currency_id').append('<option value="' + currency.id + '">' + currency.name + '</option>');
				}
    	    });
			$('#modal-body').append('</div>');
			console.log('Sali del select')
			url = 'http://'+document.domain+':'+location.port+'/ajax/local-convert/usd/'+selected_currency.iso_code+'/5000';
			$.get(url, function(max_amount){
				$('#modal-body').append('<input type="hidden"  id="min_amount" value="'+selected_currency.min_value+'">');
				$('#modal-body').append('<input type="hidden"  id="max_amount" value="'+max_amount+'">');
				$('#modal-body').append('<input type="hidden"  id="currency-id" value="'+ selected_currency.id+'">');
				$('#modal-body').append('<div class="form-group">');

				url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+selected_currency.id+'/'+max_amount+'/1';
				$.get(url, function(max){
					$('#modal-body').append('<label>Ingrese el monto (máximo '+max+')<br></label>');
					url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+selected_currency.id+'/123.4567890/1';
					$.get(url, function(placeholder){
						$('#modal-body').append('<input type="text" class="form-control" id="amount" placeholder="Ejemplo: '+placeholder.replace(selected_currency.simbol+' ','')+'" autofocus onkeyup="amountVerifier()">');
						$('#modal-body').append('<label id="amount-label"></label>');
						$('#modal-body').append('</div>');

						$('#modal-footer').empty();
						$('#modal-footer').append('<button id="submit-button" type="button" class="btn btn-success" onclick="inputMoney()">Ingresar Dinero</button>');
						$('#submit-button').hide();
						$('#modal-footer').append('<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>');	
					});
				});
			});
		});
	});
}
function outputMoneyWhitCurrency(wallet_id){
	console.log('Entre a outputMoneyWhitCurrency(wallet_id)');
	$('#modal-header').empty();
	$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
	$('#modal-header').append('<h4 class="modal-title" >Retirar dinero</h4>');
	loading();
	var url = 'http://'+document.domain+':'+location.port+'/ajax/wallets/'+wallet_id;
	$.get(url, function(selected_wallet){
		url = 'http://'+document.domain+':'+location.port+'/ajax/currencies/'+selected_wallet.currency_id;
		$.get(url, function(selected_currency){
			$('#modal-header').empty();
			$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
			$('#modal-header').append('<h4 class="modal-title" >Retirar dinero en '+ selected_currency.name+'</h4>');
		
			$('#modal-body').empty();
			$('#modal-body').append('<label>Seleccione la divisa</label>');
			url = 'http://'+document.domain+':'+location.port+'/ajax/wallets-with-balance';
			$.get(url, function(wallets){
				console.log('Entre al select');
				console.log(wallets);
				$('#modal-body').append('<div class="form-group">');
				$('#modal-body').append('<select class="form-control" name="currency_id" id="currency_id" onchange="outputMoneyWhitCurrency(value)">');
				$('#currency_id').append('<option value="' + selected_wallet.id + ' selected">' + selected_currency.name + '</option>');			
				$.each(wallets,function(index,wallet){
					if (wallet.currency_id != selected_currency.id) {
						url = 'http://'+document.domain+':'+location.port+'/ajax/currencies/'+wallet.currency_id;
						$.get(url, function(currency){
							$('#currency_id').append('<option value="' + wallet.id + '">' + currency.name + '</option>');
						});
					}
	    	    });
				$('#modal-body').append('</select><br>');
				$('#modal-body').append('</div>');
				console.log('Sali del select')
				$('#modal-body').append('<input type="hidden"  id="min_amount" value="'+selected_currency.min_value+'">');
				$('#modal-body').append('<input type="hidden"  id="max_amount" value="'+selected_wallet.account_balance+'">');
				$('#modal-body').append('<input type="hidden"  id="currency-id" value="'+ selected_currency.id+'">');
				$('#modal-body').append('<div class="form-group">');
				url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+selected_currency.id+'/'+selected_wallet.account_balance+'/1';
				$.get(url, function(max){
					$('#modal-body').append('<label>Ingrese el monto (máximo '+max+')<br></label>');
					url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+selected_currency.id+'/123.4567890/1';
					$.get(url, function(placeholder){
						$('#modal-body').append('<input type="text" class="form-control" id="amount" placeholder="Ejemplo: '+placeholder.replace(selected_currency.simbol+' ','')+'" autofocus onkeyup="amountVerifier()">');
						$('#modal-body').append('<label id="amount-label"></label>');
						$('#modal-body').append('</div>');

						$('#modal-footer').empty();
						$('#modal-footer').append('<button id="submit-button" type="button" class="btn btn-danger" onclick="outputMoney()">Retirar Dinero</button>');
						$('#submit-button').hide();
						$('#modal-footer').append('<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>');	
					});				
				});
			});
		});
	});
}
function selectCurrencyToFinanciate(wallet_id){
	console.log('Entre a selectCurrencyToFinanciate(wallet_id)');
	$('#modal-header').empty();
	$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
	$('#modal-header').append('<h4 class="modal-title" >Financiar</h4>');
	loading();
	var url = 'http://'+document.domain+':'+location.port+'/ajax/wallets/'+wallet_id;
	$.get(url, function(selected_wallet){
		url = 'http://'+document.domain+':'+location.port+'/ajax/currencies/'+selected_wallet.currency_id;
		$.get(url, function(selected_currency){
			url = 'http://'+document.domain+':'+location.port+'/ajax/wallets-to-financiate';
			$.get(url, function(wallets){
				console.log('Entre al select');
				console.log(wallets);
				$('#modal-header').empty();
				$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
				$('#modal-header').append('<h4 class="modal-title" >Financiar en '+ selected_currency.name+'</h4>');
			
				$('#modal-body').empty();
				$('#modal-body').append('<label>Seleccione la divisa</label>');	
				$('#modal-body').append('<div class="form-group">');
				$('#modal-body').append('<select class="form-control" name="currency_id" id="currency_id" onchange="selectCurrencyToFinanciate(value)">');
				$('#currency_id').append('<option value="' + selected_wallet.id + ' selected">' + selected_currency.name + '</option>');			
				$.each(wallets,function(index,wallet){
					if (wallet.currency_id != selected_currency.id) {
						url = 'http://'+document.domain+':'+location.port+'/ajax/currencies/'+wallet.currency_id;
						$.get(url, function(currency){
							$('#currency_id').append('<option value="' + wallet.id + '">' + currency.name + '</option>');
						});
					}
	    	    });
				$('#modal-body').append('</select>');
				$('#modal-body').append('</div>');

				$('#modal-footer').empty();
				$('#modal-footer').append('<a href="http://'+document.domain+':'+location.port+'/financings/wallet='+selected_wallet.id+'" class="btn btn-success">Comenzar</a>');
				$('#modal-footer').append('<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>');
			});
		});
	});
}
function financiateWithCredit(credit_id){
	console.log('Entre a financiateWithCredit(credit_id)');
	$('#modal-header').empty();
	$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
	$('#modal-header').append('<h4 class="modal-title" >Financiar crédito</h4>');
	loading();
	var url = 'http://'+document.domain+':'+location.port+'/ajax/credits/'+credit_id;
	$.get(url,function(credit){
		$('#modal-header').empty();
		$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
		$('#modal-header').append('<h4 class="modal-title" >Financiar crédito '+credit.id+' con un perfil '+credit.rank+'</h4>');
		url = 'http://'+document.domain+':'+location.port+'/ajax/currencies/'+credit.wallet.currency_id;
		$.get(url, function(currency){
			url = 'http://'+document.domain+':'+location.port+'/ajax/financings/credit='+credit_id;
			$.get(url,function(financing){
				$('#modal-body').empty();
				if(financing != ''){
					console.log('El crédito tiene financiamiento previo')
					url = 'http://'+document.domain+':'+location.port+'/ajax/financings/'+financing.id+'/amount';
					$.get(url,function(financing_amount){
						url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+currency.id+'/'+financing_amount+'/1';
						$.get(url, function(amount_string){
							$('#modal-body').append('<label>Ya financiaste '+amount_string+' en este credito.</label>');
						});
					});
					$('#modal-body').append('<input type="hidden"  name="insurance" value="'+financing.have_insurance+'">');
					$('#modal-body').append('<input type="hidden"  id="financing-id" value="'+financing.id+'">');
				}else{
					$('#modal-body').append('<input type="hidden"  id="financing-id" value="0">');
				}
				var max = credit.financing_left;
				url = 'http://'+document.domain+':'+location.port+'/ajax/wallets/currency='+currency.id;
				$.get(url,function(wallet){
					if(Number(wallet.account_balance) < Number(max)){
						max = wallet.account_balance;
					}
					url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+currency.id+'/'+max+'/1';
					$.get(url,function(max_string){
						$('#modal-body').append('<input type="hidden"  id="is_abort_financing" value="0">');
						$('#modal-body').append('<input type="hidden"  id="min_amount" value="'+currency.min_value+'">');
						$('#modal-body').append('<input type="hidden"  id="max_amount" value="'+max+'">');
						$('#modal-body').append('<input type="hidden"  id="currency-id" value="'+currency.id+'">');
						$('#modal-body').append('<input type="hidden"  id="credit-id" value="'+credit.id+'">');
						$('#modal-body').append('<div class="form-group">');
						$('#modal-body').append('<label>Ingrese el monto (máximo '+max_string+')<br></label>');
						url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+currency.id+'/123.4567890/1';
						$.get(url, function(placeholder){
							$('#modal-body').append('<input type="text" class="form-control" id="amount" placeholder="Ejemplo: '+placeholder.replace(currency.simbol+' ','')+'" autofocus onkeyup="amountVerifierForFinancing()">');
							$('#modal-body').append('<label id="amount-label"></label>');
							$('#modal-body').append('</div>');
							if (financing == '') {
								$('#modal-body').append('<div class="form-group">');
								$('#modal-body').append('<label>Seguro</label><br>');
								$('#modal-body').append('<input required type="radio" name="insurance" value="1" checked onchange="simulateFinancing()"> Con seguro (si no te devuelven, nosotros lo hacemos)<br>');
								$('#modal-body').append('<input required type="radio" name="insurance" value="0" onchange="simulateFinancing()"> Sin seguro (pueden no devolverte el dinero)<br>');
								$('#modal-body').append('</div>');
							}else{
								if(financing.have_insurance == 1){
									$('#modal-body').append('<p><strong>Financiaste con seguro.</strong> Esta opcion ya no la puedes cambiar</p>');
									$('#modal-body').append('<input type="hidden"  id="insurace-id" value="1">');
								}else{
									$('#modal-body').append('<p><strong>Financiaste sin seguro.</strong> Esta opcion ya no la puedes cambiar</p>');
									$('#modal-body').append('<input type="hidden"  id="insurace-id" value="0">');
								}
							}
							$('#modal-body').append('<br><div id="simulate-div">');
							$('#modal-footer').empty();
							$('#modal-footer').append('<button id="submit-button" type="button" class="btn btn-success" onclick="financiate()">Financiar</button>');
							$('#submit-button').hide();
							$('#modal-footer').append('<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>');							
						});
					});
				});
			});
		});
	});
}
function cancelFinancingWithCredit(credit_id){
	console.log('Entre a cancelFinancingWithCredit(credit_id)');
	$('#modal-header').empty();
	$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
	$('#modal-header').append('<h4 class="modal-title" >Quitar dinero del financiamiento</h4>');
	loading();
	var url = 'http://'+document.domain+':'+location.port+'/ajax/credits/'+credit_id;
	$.get(url,function(credit){
		url = 'http://'+document.domain+':'+location.port+'/ajax/currencies/'+credit.wallet.currency_id;
		$.get(url, function(currency){
			url = 'http://'+document.domain+':'+location.port+'/ajax/financings/credit='+credit_id;
			$.get(url,function(financing){
				$('#modal-header').empty();
				$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
				$('#modal-header').append('<h4 class="modal-title" >Quitar dinero del financiamiento '+financing.id+'</h4>');
				$('#modal-body').empty();
				if(financing != ''){
					url = 'http://'+document.domain+':'+location.port+'/ajax/financings/'+financing.id+'/amount';
					$.get(url,function(financing_amount){
						url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+currency.id+'/'+financing_amount+'/1';
						$.get(url, function(amount_string){
							$('#modal-body').append('<input type="hidden"  id="is_abort_financing" value="1">');
							$('#modal-body').append('<input type="hidden"  id="max_amount" value="'+financing_amount+'">');
							$('#modal-body').append('<input type="hidden"  id="min_amount" value="'+currency.min_value+'">');
							$('#modal-body').append('<input type="hidden"  id="currency-id" value="'+currency.id+'">');
							$('#modal-body').append('<input type="hidden"  id="financing-id" value="'+financing.id+'">');
							$('#modal-body').append('<input type="hidden"  id="credit-id" value="'+credit.id+'">');
							$('#modal-body').append('<input type="hidden"  id="insurace-id" value="'+financing.have_insurance+'">');
							$('#modal-body').append('<div class="form-group">');
							$('#modal-body').append('<label>Ingresar el monto (maximo '+amount_string+')');
							url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+currency.id+'/123.4567890/1';
							$.get(url, function(placeholder){
								$('#modal-body').append('<input type="text" class="form-control" id="amount" placeholder="Ejemplo: '+placeholder.replace(currency.simbol+' ','')+'" autofocus onkeyup="amountVerifierForFinancing()">');
								$('#modal-body').append('<label id="amount-label"></label>');
								$('#modal-body').append('</div><br>');
								$('#modal-body').append('<div id="simulate-div">');
								$('#modal-footer').empty();
								$('#modal-footer').append('<button id="submit-button" type="button" class="btn btn-danger" onclick="cancelFinancing()">Quitar</button>');
								$('#submit-button').hide();
								$('#modal-footer').append('<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>');			
							});
						});
					});
				}
			});
		});
	});
}
function payWithCredit(credit_id,wallet_id){
	console.log('Entre a payWhitCredit(credit_id= '+credit_id+',wallet_id= '+wallet_id+')');
	$('#modal-header').empty();
	$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
	$('#modal-header').append('<h4 class="modal-title" >Realizar pago</h4>');
	loading();
	var url = 'http://'+document.domain+':'+location.port+'/ajax/credits/'+credit_id;
	$.get(url,function(credit){
		$('#modal-header').empty();
		$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
		$('#modal-header').append('<h4 class="modal-title" >Realizar pago del credito '+credit.id+'</h4>');
		url = 'http://'+document.domain+':'+location.port+'/ajax/wallets/'+wallet_id;
		$.get(url,function(selected_wallet){
			$('#modal-header').empty();
			$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
			$('#modal-header').append('<h4 class="modal-title" >Realizar pago del credito '+credit.id+' con '+selected_wallet.currency.name+'</h4>');
			url = 'http://'+document.domain+':'+location.port+'/ajax/payments/'+credit.first_unpayed_payment.id+'/unpayed-amount';
			$.get(url,function(payment_amount){
				url = 'http://'+document.domain+':'+location.port+'/ajax/wallets-with-balance';
				$.get(url, function(wallets){
					url = 'http://'+document.domain+':'+location.port+'/ajax/local-convert/'+selected_wallet.currency.iso_code+'/'+credit.currency.iso_code+'/'+selected_wallet.account_balance;
					$.get(url,function(selected_account_balance_in_currency){
						$('#modal-body').empty();
						$('#modal-body').append('<div class="form-group">');
						$('#modal-body').append('<label>Seleccione la divisa</label>');
						$('#modal-body').append('<select class="form-control" name="wallet_id" id="wallet_id" onchange="payWithCredit('+credit.id+',value)">');	
						url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+credit.currency.id+'/'+selected_account_balance_in_currency+'/1';
						$.get(url,function(selected_account_balance_in_currency_string){
							$('#wallet_id').append('<option value="' + selected_wallet.id + ' selected">' + selected_wallet.currency.name +' ('+credit.currency.iso_code+' '+selected_account_balance_in_currency_string+' disponibles)</option>');			
							$.each(wallets,function(index,wallet){
								if (wallet.currency_id != selected_wallet.currency_id) {
									url = 'http://'+document.domain+':'+location.port+'/ajax/wallets/'+wallet.id;
									$.get(url, function(wallet_getted){
										url = 'http://'+document.domain+':'+location.port+'/ajax/local-convert/'+wallet_getted.currency.iso_code+'/'+credit.currency.iso_code+'/'+wallet_getted.account_balance;
										$.get(url,function(account_balance_in_currency){
											if(account_balance_in_currency >= credit.currency.min_value){
												url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+credit.currency.id+'/'+account_balance_in_currency+'/1';
												$.get(url,function(account_balance_in_currency_string){
													$('#wallet_id').append('<option value="' + wallet_getted.id + '">' + wallet_getted.currency.name +' ('+credit.currency.iso_code+' '+account_balance_in_currency_string+' disponibles)</option>');
												});
											}
										});
									});
								}
							});
						});
						$('#modal-body').append('</select>');
						if(credit.currency.id != selected_wallet.currency_id){
							$('#modal-body').append('<input type="hidden"  id="from_currency_id" value="'+selected_wallet.currency_id+'">');
							$('#modal-body').append('<input type="hidden"  id="to_currency_id" value="'+credit.currency.id+'">');
							$('#modal-body').append('<div id=exchange-rate-areachart style="height: 150px;"></div>');
							drawExchangeRate();
						}
						var max = credit.unpayed_amount;
						if(Number(selected_account_balance_in_currency) < Number(max)){
							max = selected_account_balance_in_currency;
						}
						var min =credit.currency.min_value;
						url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+credit.currency.id+'/'+credit.unpayed_amount+'/1';
						$.get(url,function(unpayed_amount_string){
							url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+credit.currency.id+'/'+payment_amount+'/1';
							$.get(url,function(payment_amount_string){
								url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+credit.currency.id+'/'+max+'/1';
								$.get(url,function(max_string){
									$('#modal-body').append('<h4>');
									if(Number(max) == Number(credit.unpayed_amount)){
										$('#modal-body').append('<button class="btn btn-default" onclick="fillAmountId(\''+unpayed_amount_string+'\')">Crédito ('+unpayed_amount_string+')</button>');
									}
									url = 'http://'+document.domain+':'+location.port+'/ajax/payments/'+credit.first_unpayed_payment.id+'/payment-number';
									$.get(url,function(payment_number){
										if(Number(max) >= Number(payment_amount)){
											$('#modal-body').append('<button class="btn btn-default pull-right" onclick="fillAmountId(\''+payment_amount_string+'\')">Cuota '+payment_number+' ('+payment_amount_string+')</button>');
										}
										$('#modal-body').append('</h4>');
										$('#modal-body').append('<br>');
										$('#modal-body').append('<input type="hidden"  id="max_amount" value="'+max+'">');
										$('#modal-body').append('<input type="hidden"  id="min_amount" value="'+min+'">');
										$('#modal-body').append('<input type="hidden"  id="currency-id" value="'+credit.currency.id+'">');
										$('#modal-body').append('<input type="hidden"  id="from_wallet_id" value="'+selected_wallet.id+'">');
										$('#modal-body').append('<input type="hidden"  id="credit-id" value="'+credit.id+'">');
										$('#modal-body').append('<label>Ingrese el monto (máximo '+max_string+')<br></label>');
										url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+credit.currency.id+'/123.4567890/1';
										$.get(url, function(placeholder){
											$('#modal-body').append('<input type="text" class="form-control" id="amount" placeholder="Ejemplo: '+placeholder.replace(credit.currency.simbol+' ','')+'" autofocus onkeyup="amountVerifierForPayCredit()">');
											$('#modal-body').append('<label id="amount-label"></label>');
											$('#modal-body').append('</div><br>');
											$('#modal-body').append('<div id="simulate-div">');
											$('#modal-body').append('</div>');
											$('#modal-body').append('<br>');
											$('#modal-footer').empty();
											$('#modal-footer').append('<button id="submit-button" type="button" class="btn btn-success" onclick="payCredit()">Realizar pago</button>');
											$('#submit-button').hide();
											$('#modal-footer').append('<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>');										
										});
									});
								});
							});
						});
					});
				});
			});
		});
	});
}
function requestCreditWitCurrency(currency_id){
	console.log('Entre a requestCreditWitCurrency(currency_id)');
	$('#modal-header').empty();
	$('#modal-header').empty();
	$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
	$('#modal-header').append('<h4 class="modal-title" >Solicitar crédito</h4>');
	loading();
	var url = 'http://'+document.domain+':'+location.port+'/ajax/currencies/'+currency_id;
	$.get(url, function(selected_currency){
		$('#modal-header').empty();
		$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
		$('#modal-header').append('<h4 class="modal-title" >Solicitar crédito en '+ selected_currency.name+'</h4>');

		$('#modal-body').empty();
		$('#modal-body').append('<label>Seleccione la divisa</label>');
		url = 'http://'+document.domain+':'+location.port+'/ajax/currencies';
		$.get(url, function(currencies){
			console.log('Entre al select');
			console.log(currencies);
			$('#modal-body').append('<div class="form-group">');
			$('#modal-body').append('<select class="form-control" name="currency_id" id="currency_id" onchange="requestCreditWitCurrency(value)">');
			$('#currency_id').append('<option value="' + selected_currency.id + ' selected">' + selected_currency.name + '</option>');			
			$.each(currencies,function(index,currency){
    	       if (currency.id != selected_currency.id) {
					$('#currency_id').append('<option value="' + currency.id + '">' + currency.name + '</option>');
				}
    	    });
			$('#modal-body').append('</select><br>');
			$('#modal-body').append('</div>');
			console.log('Sali del select')
			url = 'http://'+document.domain+':'+location.port+'/ajax/currencies/'+currency_id+'/credit-margin';
			$.get(url,function(margin){
				url = 'http://'+document.domain+':'+location.port+'/ajax/currencies/'+currency_id+'/credit-min-amount';
				$.get(url,function(min){
					$('#modal-body').append('<input type="hidden"  id="min_amount" value="'+min+'">');
					$('#modal-body').append('<input type="hidden"  id="max_amount" value="'+margin+'">');
					$('#modal-body').append('<input type="hidden"  id="currency-id" value="'+ selected_currency.id+'">');
					$('#modal-body').append('<div class="form-group">');
					url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+selected_currency.id+'/'+margin+'/1';
					$.get(url,function(margin_string){
						url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+selected_currency.id+'/'+min+'/1';
						$.get(url,function(min_string){
							$('#modal-body').append('<label>Ingrese el monto (mínimo: '+min_string+' máximo: '+margin_string+')<br></label>');
							url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+selected_currency.id+'/123.4567890/1';
							$.get(url, function(placeholder){
								$('#modal-body').append('<input type="text" class="form-control" id="amount" placeholder="Ejemplo: '+placeholder.replace(selected_currency.simbol+' ','')+'" autofocus onkeyup="amountVerifierForRequestCredit()">');
								$('#modal-body').append('<label id="amount-label"></label>');
								$('#modal-body').append('</div>');
								url = 'http://'+document.domain+':'+location.port+'/ajax/payment-plans/currency='+selected_currency.id;
								$.get(url,function(payment_plans){
									$('#modal-body').append('<div class="form-group">');
									$('#modal-body').append('<label>Seleccione el plan de pago</label>');
									$('#modal-body').append('<select class="form-control" name="payment_plan_id" id="payment_plan_id" onchange="simulateCredit()">');
									$.each(payment_plans,function(index,payment_plan){
										$('#payment_plan_id').append('<option value="' + payment_plan.id + '">' + payment_plan.description + '</option>');
									});
									$('#modal-body').append('</select><br>');
									$('#modal-body').append('</div>');
									$('#modal-body').append('<div id="simulate-div">');
									$('#modal-body').append('</div>');

									$('#modal-footer').empty();
									$('#modal-footer').append('<button id="submit-button" type="button" class="btn btn-success" onclick="requestCredit()">Solicitar Crédito</button>');
									$('#submit-button').hide();
									$('#modal-footer').append('<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>');						
								});
							});
						});
					});
				});
			});
		});	
	});	
}
function exchangeCurrency(wallet_id){
	console.log('exchangeCurrency(wallet_id)');
	$('#modal-header').empty();
	$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
	$('#modal-header').append('<h4 class="modal-title" >Convertir divisa</h4>');
	loading();
	var url = 'http://'+document.domain+':'+location.port+'/ajax/wallets/'+wallet_id;
	$.get(url, function(selected_wallet){
		url = 'http://'+document.domain+':'+location.port+'/ajax/currencies/'+selected_wallet.currency_id;
		$.get(url, function(selected_currency){
			$('#modal-header').empty();
			$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
			$('#modal-header').append('<h4 class="modal-title" >Convertir divisa desde '+ selected_currency.name+'</h4>');			
			url = 'http://'+document.domain+':'+location.port+'/ajax/wallets-with-balance';
			$.get(url, function(wallets){
				console.log('Entre al select');
				console.log(wallets);
				$('#modal-body').empty();
				$('#modal-body').append('<div id=exchange-rate-areachart style="height: 150px;"></div>');
				$('#modal-body').append('<div class="form-group">');
				$('#modal-body').append('<label>Divisa origen</label>');
				$('#modal-body').append('<select class="form-control" name="from_wallet_id" id="from_wallet_id" onchange="exchangeCurrency(value)">');
				$('#from_wallet_id').append('<option value="' + selected_wallet.id + ' selected">' + selected_currency.name + '</option>');			
				$.each(wallets,function(index,wallet){
					if (wallet.currency_id != selected_currency.id) {
						url = 'http://'+document.domain+':'+location.port+'/ajax/currencies/'+wallet.currency_id;
						$.get(url, function(this_currency){
							$('#from_wallet_id').append('<option value="' + wallet.id + '">' + this_currency.name + '</option>');
						});
					}
	    	    });
				$('#modal-body').append('</select><br>');;

				url = 'http://'+document.domain+':'+location.port+'/ajax/currencies';
				$.get(url, function(currencies){
					$('#modal-body').append('<label>Divisa destino</label>');
					$('#modal-body').append('<select class="form-control" name="to_currency_id" id="to_currency_id" onchange="exchangeRateLoaderInExchange()">');
					$.each(currencies,function(index,currency){
						if(currency.id != selected_currency.id){
							$('#to_currency_id').append('<option value="' + currency.id + '">' + currency.name + '</option>');
						}
					});
					$('#modal-body').append('</select><br>');
					$('#modal-body').append('</div>');
					url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+selected_currency.id+'/'+selected_wallet.account_balance+'/1';
					$.get(url, function(max){
						$('#modal-body').append('<input type="hidden"  id="min_amount" value="'+selected_currency.min_value+'">');
						$('#modal-body').append('<input type="hidden"  id="max_amount" value="'+selected_wallet.account_balance+'">');
						$('#modal-body').append('<input type="hidden"  id="from_currency_id" value="'+ selected_currency.id+'">');
						$('#modal-body').append('<label>Ingrese el monto en '+selected_currency.name+' (máximo '+max+')<br></label>');
						url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+selected_currency.id+'/123.4567890/1';
						$.get(url, function(placeholder){
							$('#modal-body').append('<input type="text" class="form-control" id="amount" placeholder="Ejemplo: '+placeholder.replace(selected_currency.simbol+' ','')+'" autofocus onkeyup="amountVerifierForExchange()">');
							$('#modal-body').append('<label id="amount-label"></label>');
							$('#modal-body').append('</div>');
							exchangeRateLoaderInExchange();
							$('#modal-body').append('<div id="simulate-div">');
							$('#modal-body').append('</div>');
							$('#modal-body').append('</div>');

							$('#modal-footer').empty();
							$('#modal-footer').append('<button id="submit-button" type="button" class="btn btn-success" onclick="exchange()">Realizar conversión</button>');
							$('#submit-button').hide();
							$('#modal-footer').append('<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>');
						});
					});
				});
			});
		});
	});
}
function askGrantCredit(){
	console.log('Entre a askAbortCredit()');
	$('#modal-header').empty();
	$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
	$('#modal-header').append('<h4 class="modal-title">Transferir Crédito</h4>');

	$('#modal-body').empty();
	$('#modal-body').append('<i class="fa fa-question fa-5x justify-content-center" aria-hidden="true"></i>');
	$('#modal-body').append('<h5>¿Quieres el crédito a tu wallet?</h5>');
	$('#modal-body').append('<p>Se generarán las cuotas y tendrás el dinero listo para usarlo</p>');

	$('#modal-footer').empty();
	$('#modal-footer').append('<button id="submit-button" type="button" class="btn btn-success" onclick="grantCredit()">Transferir crédito</button>');
	$('#modal-footer').append('<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>');
	
}
function askAbortCredit(){
	console.log('Entre a askAbortCredit()');
	$('#modal-header').empty();
	$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
	$('#modal-header').append('<h4 class="modal-title">Cancelar Crédito</h4>');

	$('#modal-body').empty();
	$('#modal-body').append('<i class="fa fa-question fa-5x justify-content-center" aria-hidden="true"></i>');
	$('#modal-body').append('<h5>¿Quieres cancelar el crédito? Esta opción no se puede deshacer</h5>');
	$('#modal-body').append('<p>Pero no te preocupes, si querés, podés solicitar otro crédito del mismo monto en el futuro</p>');

	$('#modal-footer').empty();
	$('#modal-footer').append('<button id="submit-button" type="button" class="btn btn-danger" onclick="abortCredit()">Cancelar crédito</button>');
	$('#modal-footer').append('<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>');
	
}
function askRejectCredit(){
	console.log('Entre a askRejectCredit()');
	$('#modal-header').empty();
	$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
	$('#modal-header').append('<h4 class="modal-title">Rechazar Crédito</h4>');

	$('#modal-body').empty();
	$('#modal-body').append('<i class="fa fa-question fa-5x justify-content-center" aria-hidden="true"></i>');
	$('#modal-body').append('<h5>¿Quieres rechazar el crédito? Esta opción no se puede deshacer</h5>');

	$('#modal-footer').empty();
	$('#modal-footer').append('<button id="submit-button" type="button" class="btn btn-danger" onclick="rejectCredit()">Rechazar crédito</button>');
	$('#modal-footer').append('<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>');
	
}
function askApprovateCredit(){
	console.log('Entre a askApprovateCredit()');
	$('#modal-header').empty();
	$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
	$('#modal-header').append('<h4 class="modal-title">Aprobar Crédito</h4>');

	$('#modal-body').empty();
	$('#modal-body').append('<i class="fa fa-question fa-5x justify-content-center" aria-hidden="true"></i>');
	$('#modal-body').append('<h5>¿Quieres aprobar el crédito? Esta opción no se puede deshacer</h5>');

	$('#modal-footer').empty();
	$('#modal-footer').append('<button id="submit-button" type="button" class="btn btn-success" onclick="approvateCredit()">Aprobar crédito</button>');
	$('#modal-footer').append('<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>');
	
}
function askJudicialCitationForCredit(){
	console.log('Entre a askJudicialCitationForCredit()');
	$('#modal-header').empty();
	$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
	$('#modal-header').append('<h4 class="modal-title">Enviar citación judicial</h4>');

	$('#modal-body').empty();
	$('#modal-body').append('<i class="fa fa-question fa-5x justify-content-center" aria-hidden="true"></i>');
	$('#modal-body').append('<h5>¿Quieres enviar la citacón judicial para el crédito? Esta opción no se puede deshacer</h5>');

	$('#modal-footer').empty();
	$('#modal-footer').append('<button id="submit-button" type="button" class="btn btn-danger" onclick="juditialCitationForCredit()">Enviar citación</button>');
	$('#modal-footer').append('<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>');
	
}
function askLawsuitCredit(){
	console.log('Entre a askLawsuitCredit()');
	$('#modal-header').empty();
	$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
	$('#modal-header').append('<h4 class="modal-title">Comenzar litigio</h4>');

	$('#modal-body').empty();
	$('#modal-body').append('<i class="fa fa-question fa-5x justify-content-center" aria-hidden="true"></i>');
	$('#modal-body').append('<h5>¿Quieres comenzar el litigio para el crédito? Esta opción no se puede deshacer</h5>');

	$('#modal-footer').empty();
	$('#modal-footer').append('<button id="submit-button" type="button" class="btn btn-danger" onclick="lawsuitCredit()">Comenzar litigio</button>');
	$('#modal-footer').append('<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>');
	
}
function askBadCredit(){
	console.log('Entre a askBadCredit()');
	$('#modal-header').empty();
	$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
	$('#modal-header').append('<h4 class="modal-title">Declarar incobrable</h4>');

	$('#modal-body').empty();
	$('#modal-body').append('<i class="fa fa-question fa-5x justify-content-center" aria-hidden="true"></i>');
	$('#modal-body').append('<h5>¿Quieres marcar el crédito como incobrable? Esta opción no se puede deshacer</h5>');

	$('#modal-footer').empty();
	$('#modal-footer').append('<button id="submit-button" type="button" class="btn btn-danger" onclick="badCredit()">Declarar incobrable</button>');
	$('#modal-footer').append('<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>');
	
}


function badCredit(){
	console.log('Entre a badCredit()');
	var credit_id = document.getElementById('credit_id').value;
	loading();
	$.ajax({
		type: 'post',
		data: {
			_token: document.getElementsByName('_token')[0].value,
			credit_id: credit_id,
		},
		url: 'http://'+document.domain+':'+location.port+'/ajax/bad-credit',
		success: function(json){
			console.log('Entre a ajaxSuccess()');
			console.log(json);
			$('#modal-header').empty();
			$('#modal-header').append('<a onclick="redirect(\'/credits/lawsuit/currency='+json.currency_id+'\')" type="button" class="close"><span aria-hidden="true">&times;</span></a>');
			$('#modal-header').append('<h4 class="modal-title" >Crédito aprobado</h4>');
		
			$('#modal-body').empty();
			$('#modal-body').append('<i class="fa fa-check fa-5x" aria-hidden="true"></i>');
			$('#modal-body').append('<h2>Declaraste el crédito como incobrable, muchas gracias</h2>');
			$('#modal-body').append('<h3>Detalles</h3>');
			$('#modal-body').append('<table class="table table-hover table-striped">\n<thead>\n<th>Concepto</th>\n<th>Valor</th>\n</thead>\n<tbody>\n<tr>\n<td>ID de operacion</td>\n<td>'+json.credit_phase_id+'</td>\n</tr>\n<tr>\n<td>ID de credito</td>\n<td>'+json.credit_id+'</td>\n</tr>\n<tr>\n<td>Monto solicitado</td>\n<td>'+json.credit_amount_string+'</td>\n</tr>\n</tbody>\n</table>');
			
			$('#modal-footer').empty();
			$('#modal-footer').append('<button class="btn btn-primary" onclick="printDiv(\'modal\')">Generar PDF</button>');
			$('#modal-footer').append('<a onclick="redirect(\'/credits/lawsuit/currency='+json.currency_id+'\')" class="btn btn-primary">Cerrar</a>');
		},
		error: function(json){
			console.log('Entre a ajaxError()');
			console.log(json);
			$('#modal-header').empty();
			$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
			$('#modal-header').append('<h4 class="modal-title" >Error de conexión</h4>');
		
			$('#modal-body').empty();
			$('#modal-body').append('<i class="fa fa-times fa-5x" aria-hidden="true"></i>');
			$('#modal-body').append('<h2>No pudimos establecer conexión con nuestros sistemas</h2>');
			$('#modal-body').append('<h4>Por favor, recargá la página</h4>');
		
			$('#modal-footer').empty();
			$('#modal-footer').append('<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>');
		},
	});
}
function lawsuitCredit(){
	console.log('Entre a lawsuitCredit()');
	var credit_id = document.getElementById('credit_id').value;
	loading();
	$.ajax({
		type: 'post',
		data: {
			_token: document.getElementsByName('_token')[0].value,
			credit_id: credit_id,
		},
		url: 'http://'+document.domain+':'+location.port+'/ajax/lawsuit-credit',
		success: function(json){
			console.log('Entre a ajaxSuccess()');
			console.log(json);
			$('#modal-header').empty();
			$('#modal-header').append('<a onclick="redirect(\'/credits/judicial-citation/currency='+json.currency_id+'\')" type="button" class="close"><span aria-hidden="true">&times;</span></a>');
			$('#modal-header').append('<h4 class="modal-title" >Crédito aprobado</h4>');
		
			$('#modal-body').empty();
			$('#modal-body').append('<i class="fa fa-check fa-5x" aria-hidden="true"></i>');
			$('#modal-body').append('<h2>Comenzaste el litigio para el crédito, muchas gracias</h2>');
			$('#modal-body').append('<h3>Detalles</h3>');
			$('#modal-body').append('<table class="table table-hover table-striped">\n<thead>\n<th>Concepto</th>\n<th>Valor</th>\n</thead>\n<tbody>\n<tr>\n<td>ID de operacion</td>\n<td>'+json.credit_phase_id+'</td>\n</tr>\n<tr>\n<td>ID de credito</td>\n<td>'+json.credit_id+'</td>\n</tr>\n<tr>\n<td>Monto solicitado</td>\n<td>'+json.credit_amount_string+'</td>\n</tr>\n</tbody>\n</table>');
			
			$('#modal-footer').empty();
			$('#modal-footer').append('<button class="btn btn-primary" onclick="printDiv(\'modal\')">Generar PDF</button>');
			$('#modal-footer').append('<a onclick="redirect(\'/credits/judicial-citation/currency='+json.currency_id+'\')" class="btn btn-primary">Cerrar</a>');
		},
		error: function(json){
			console.log('Entre a ajaxError()');
			console.log(json);
			$('#modal-header').empty();
			$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
			$('#modal-header').append('<h4 class="modal-title" >Error de conexión</h4>');
		
			$('#modal-body').empty();
			$('#modal-body').append('<i class="fa fa-times fa-5x" aria-hidden="true"></i>');
			$('#modal-body').append('<h2>No pudimos establecer conexión con nuestros sistemas</h2>');
			$('#modal-body').append('<h4>Por favor, recargá la página</h4>');
		
			$('#modal-footer').empty();
			$('#modal-footer').append('<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>');
		},
	});
}
function juditialCitationForCredit(){
	console.log('Entre a juditialCitationForCredit()');
	var credit_id = document.getElementById('credit_id').value;
	loading();
	$.ajax({
		type: 'post',
		data: {
			_token: document.getElementsByName('_token')[0].value,
			credit_id: credit_id,
		},
		url: 'http://'+document.domain+':'+location.port+'/ajax/citation-credit',
		success: function(json){
			console.log('Entre a ajaxSuccess()');
			console.log(json);
			$('#modal-header').empty();
			$('#modal-header').append('<a onclick="redirect(\'/credits/defaulter/currency='+json.currency_id+'\')" type="button" class="close"><span aria-hidden="true">&times;</span></a>');
			$('#modal-header').append('<h4 class="modal-title" >Crédito aprobado</h4>');
		
			$('#modal-body').empty();
			$('#modal-body').append('<i class="fa fa-check fa-5x" aria-hidden="true"></i>');
			$('#modal-body').append('<h2>Se envió la citación para el crédito, muchas gracias</h2>');
			$('#modal-body').append('<h3>Detalles</h3>');
			$('#modal-body').append('<table class="table table-hover table-striped">\n<thead>\n<th>Concepto</th>\n<th>Valor</th>\n</thead>\n<tbody>\n<tr>\n<td>ID de operacion</td>\n<td>'+json.credit_phase_id+'</td>\n</tr>\n<tr>\n<td>ID de credito</td>\n<td>'+json.credit_id+'</td>\n</tr>\n<tr>\n<td>Monto solicitado</td>\n<td>'+json.credit_amount_string+'</td>\n</tr>\n</tbody>\n</table>');
			
			$('#modal-footer').empty();
			$('#modal-footer').append('<button class="btn btn-primary" onclick="printDiv(\'modal\')">Generar PDF</button>');
			$('#modal-footer').append('<a onclick="redirect(\'/credits/defaulter/currency='+json.currency_id+'\')" class="btn btn-primary">Cerrar</a>');
		},
		error: function(json){
			console.log('Entre a ajaxError()');
			console.log(json);
			$('#modal-header').empty();
			$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
			$('#modal-header').append('<h4 class="modal-title" >Error de conexión</h4>');
		
			$('#modal-body').empty();
			$('#modal-body').append('<i class="fa fa-times fa-5x" aria-hidden="true"></i>');
			$('#modal-body').append('<h2>No pudimos establecer conexión con nuestros sistemas</h2>');
			$('#modal-body').append('<h4>Por favor, recargá la página</h4>');
		
			$('#modal-footer').empty();
			$('#modal-footer').append('<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>');
		},
	});
}

function approvateCredit(){
	console.log('Entre a approvateCredit()');
	var credit_id = document.getElementById('credit_id').value;
	loading();
	$.ajax({
		type: 'post',
		data: {
			_token: document.getElementsByName('_token')[0].value,
			credit_id: credit_id,
		},
		url: 'http://'+document.domain+':'+location.port+'/ajax/approvate-credit',
		success: function(json){
			console.log('Entre a ajaxSuccess()');
			console.log(json);
			$('#modal-header').empty();
			$('#modal-header').append('<a onclick="redirect(\'/credits/to-approve/currency='+json.currency_id+'\')" type="button" class="close"><span aria-hidden="true">&times;</span></a>');
			$('#modal-header').append('<h4 class="modal-title" >Crédito aprobado</h4>');
		
			$('#modal-body').empty();
			$('#modal-body').append('<i class="fa fa-check fa-5x" aria-hidden="true"></i>');
			$('#modal-body').append('<h2>Aprobaste el credito, muchas gracias</h2>');
			$('#modal-body').append('<h3>Detalles</h3>');
			$('#modal-body').append('<table class="table table-hover table-striped">\n<thead>\n<th>Concepto</th>\n<th>Valor</th>\n</thead>\n<tbody>\n<tr>\n<td>ID de operacion</td>\n<td>'+json.credit_phase_id+'</td>\n</tr>\n<tr>\n<td>ID de credito</td>\n<td>'+json.credit_id+'</td>\n</tr>\n<tr>\n<td>Monto solicitado</td>\n<td>'+json.credit_amount_string+'</td>\n</tr>\n</tbody>\n</table>');
			
			$('#modal-footer').empty();
			$('#modal-footer').append('<button class="btn btn-primary" onclick="printDiv(\'modal\')">Generar PDF</button>');
			$('#modal-footer').append('<a onclick="redirect(\'/credits/to-approve/currency='+json.currency_id+'\')" class="btn btn-primary">Cerrar</a>');
		},
		error: function(json){
			console.log('Entre a ajaxError()');
			console.log(json);
			$('#modal-header').empty();
			$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
			$('#modal-header').append('<h4 class="modal-title" >Error de conexión</h4>');
		
			$('#modal-body').empty();
			$('#modal-body').append('<i class="fa fa-times fa-5x" aria-hidden="true"></i>');
			$('#modal-body').append('<h2>No pudimos establecer conexión con nuestros sistemas</h2>');
			$('#modal-body').append('<h4>Por favor, recargá la página</h4>');
		
			$('#modal-footer').empty();
			$('#modal-footer').append('<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>');
		},
	});
}
function rejectCredit(){
	console.log('Entre a rejectCredit()');
	var credit_id = document.getElementById('credit_id').value;
	loading();
	$.ajax({
		type: 'post',
		data: {
			_token: document.getElementsByName('_token')[0].value,
			credit_id: credit_id,
		},
		url: 'http://'+document.domain+':'+location.port+'/ajax/reject-credit',
		success: function(json){
			console.log('Entre a ajaxSuccess()');
			console.log(json);
			$('#modal-header').empty();
			$('#modal-header').append('<a onclick="redirect(\'/credits/to-approve/currency='+json.currency_id+'\')" type="button" class="close"><span aria-hidden="true">&times;</span></a>');
			$('#modal-header').append('<h4 class="modal-title" >Crédito rechazado</h4>');
		
			$('#modal-body').empty();
			$('#modal-body').append('<i class="fa fa-check fa-5x" aria-hidden="true"></i>');
			$('#modal-body').append('<h2>Rechazaste el credito, muchas gracias</h2>');
			$('#modal-body').append('<h3>Detalles</h3>');
			$('#modal-body').append('<table class="table table-hover table-striped">\n<thead>\n<th>Concepto</th>\n<th>Valor</th>\n</thead>\n<tbody>\n<tr>\n<td>ID de operacion</td>\n<td>'+json.credit_phase_id+'</td>\n</tr>\n<tr>\n<td>ID de credito</td>\n<td>'+json.credit_id+'</td>\n</tr>\n<tr>\n<td>Monto solicitado</td>\n<td>'+json.credit_amount_string+'</td>\n</tr>\n</tbody>\n</table>');
			
			$('#modal-footer').empty();
			$('#modal-footer').append('<button class="btn btn-primary" onclick="printDiv(\'modal\')">Generar PDF</button>');
			$('#modal-footer').append('<a  onclick="redirect(\'/credits/to-approve/currency='+json.currency_id+'\')" class="btn btn-primary">Cerrar</a>');
		},
		error: function(json){
			console.log('Entre a ajaxError()');
			console.log(json);
			$('#modal-header').empty();
			$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
			$('#modal-header').append('<h4 class="modal-title" >Error de conexión</h4>');
		
			$('#modal-body').empty();
			$('#modal-body').append('<i class="fa fa-times fa-5x" aria-hidden="true"></i>');
			$('#modal-body').append('<h2>No pudimos establecer conexión con nuestros sistemas</h2>');
			$('#modal-body').append('<h4>Por favor, recargá la página</h4>');
		
			$('#modal-footer').empty();
			$('#modal-footer').append('<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>');
		},
	});
}
//Ojo porque no funciona la impresion
function grantCredit(){
	console.log('Entre a grantCredit()');
	var wallet_id = document.getElementById('wallet_id').value;
	var credit_id = document.getElementById('credit_id').value;
	loading();
	$.ajax({
		type: 'post',
		data: {
			_token: document.getElementsByName('_token')[0].value,
			credit_id: credit_id,
		},
		url: 'http://'+document.domain+':'+location.port+'/ajax/grant-credit',
		success: function(json){
			console.log('Entre a ajaxSuccess()');
			console.log(json);
			$('#modal-header').empty();
			$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="location.reload()"><span aria-hidden="true">&times;</span></button>');
			$('#modal-header').append('<h4 class="modal-title" >Crédito transferido</h4>');
		
			$('#modal-body').empty();
			$('#modal-body').append('<i class="fa fa-check fa-5x" aria-hidden="true"></i>');
			$('#modal-body').append('<h2>Ya dispones del dinero en tu wallet, muchas gracias</h2>');

			$('#modal-body').append('<h3>Detalles</h3>');
			$('#modal-body').append('<table class="table table-hover table-striped">\n<thead>\n<th>Concepto</th>\n<th>Valor</th>\n</thead>\n<tbody>\n<tr>\n<td>ID de transaccion</td>\n<td>'+json.credit_phase_id+'</td>\n</tr>\n<tr>\n<td>ID de credito</td>\n<td>'+json.credit_id+'</td>\n</tr>\n<tr>\n<td>Monto de credito</td>\n<td>'+json.credit_amount_string+'</td>\n</tr>\n<tr>\n<td>Cantidad de cuotas</td>\n<td>'+json.payments_numer+'</td>\n</tr>\n<tr>\n<td>Monto de cada cuota</td>\n<td>'+json.payments_amount_string+'</td>\n</tr>\n<tr>\n<td>Vencimiento primer cuota</td>\n<td>'+json.due_date_for_first_payment+'</td>\n</tr>\n</tbody>\n</table>');
			
			$('#modal-footer').empty();
			$('#modal-footer').append('<button class="btn btn-primary" onclick="printDiv(\'modal\')">Generar PDF</button>');
			$('#modal-footer').append('<a onclick="redirect(\'/wallets/'+wallet_id+'\')" class="btn btn-success">Ir al Wallet</a>');
			$('#modal-footer').append('<button type="button" class="btn btn-primary" data-dismiss="modal" onclick="location.reload()">Cerrar</button>');
		},
		error: function(json){
			console.log('Entre a ajaxError()');
			console.log(json);
			$('#modal-header').empty();
			$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
			$('#modal-header').append('<h4 class="modal-title" >Error de conexión</h4>');
		
			$('#modal-body').empty();
			$('#modal-body').append('<i class="fa fa-times fa-5x" aria-hidden="true"></i>');
			$('#modal-body').append('<h2>No pudimos establecer conexión con nuestros sistemas</h2>');
			$('#modal-body').append('<h4>Por favor, recargá la página</h4>');
		
			$('#modal-footer').empty();
			$('#modal-footer').append('<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>');
		},
	});
}
//Ojo porque no funciona la impresion
function abortCredit(){
	console.log('Entre a abortCredit()');
	var credit_id = document.getElementById('credit_id').value;
	loading();
	$.ajax({
		type: 'post',
		data: {
			_token: document.getElementsByName('_token')[0].value,
			credit_id: credit_id,
		},
		url: 'http://'+document.domain+':'+location.port+'/ajax/abort-credit',
		success: function(json){
			console.log('Entre a ajaxSuccess()');
			console.log(json);
			$('#modal-header').empty();
			$('#modal-header').append('<button type="button" class="close" aria-label="Close" onclick="location.reload()"><span aria-hidden="true">&times;</span></button>');
			$('#modal-header').append('<h4 class="modal-title" >Crédito cancelado</h4>');
		
			$('#modal-body').empty();
			$('#modal-body').append('<i class="fa fa-check fa-5x" aria-hidden="true"></i>');
			$('#modal-body').append('<h2>Cancelaste el credito, muchas gracias</h2>');
			
			$('#modal-body').append('<h3>Detalles</h3>');
			$('#modal-body').append('<table class="table table-hover table-striped">\n<thead>\n<th>Concepto</th>\n<th>Valor</th>\n</thead>\n<tbody>\n<tr>\n<td>ID de transaccion</td>\n<td>'+json.credit_phase_id+'</td>\n</tr>\n<tr>\n<td>ID de credito</td>\n<td>'+json.credit_id+'</td>\n</tr>\n<tr>\n<td>Monto de credito</td>\n<td>'+json.credit_amount_string+'</td>\n</tr>\n</tbody>\n</table>');
		
			$('#modal-footer').empty();
			$('#modal-footer').append('<button class="btn btn-primary" onclick="printDiv(\'modal\')">Generar PDF</button>');
			$('#modal-footer').append('<button type="button" class="btn btn-primary" onclick="location.reload()">Cerrar</button>');
		},
		error: function(json){
			console.log('Entre a ajaxError()');
			console.log(json);
			$('#modal-header').empty();
			$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
			$('#modal-header').append('<h4 class="modal-title" >Error de conexión</h4>');
		
			$('#modal-body').empty();
			$('#modal-body').append('<i class="fa fa-times fa-5x" aria-hidden="true"></i>');
			$('#modal-body').append('<h2>No pudimos establecer conexión con nuestros sistemas</h2>');
			$('#modal-body').append('<h4>Por favor, recargá la página</h4>');
		
			$('#modal-footer').empty();
			$('#modal-footer').append('<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>');
		},
	});
}
function requestCredit(){
	console.log('Entre a requestCredit()');
	var currency_id = document.getElementById('currency-id').value;
	var amount_string = document.getElementById('amount').value;
	var payment_plan_id = document.getElementById('payment_plan_id').value;
	loading();
	amount_string = amount_string.replace(' ','');
	var url = 'http://'+document.domain+':'+location.port+'/ajax/string-to-number/'+currency_id+'/'+ amount_string;
	$.get(url,function(amount){
		$.ajax({
			type: 'post',
			data: {
				_token: document.getElementsByName('_token')[0].value,
				amount: amount,
				payment_plan_id: payment_plan_id,
			},
			url: 'http://'+document.domain+':'+location.port+'/ajax/request-credit',
			success: function(json){
				console.log('Entre a ajaxSuccess()');
				console.log(json);
				$('#modal-header').empty();
				$('#modal-header').append('<a onclick="redirect(\'/credits/currency='+currency_id+'\')" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></a>');
				$('#modal-header').append('<h4 class="modal-title" >Crédito solicitado</h4>');
			
				$('#modal-body').empty();
				$('#modal-body').append('<i class="fa fa-check fa-5x" aria-hidden="true"></i>');
				$('#modal-body').append('<h2>Solicitaste el credito, muchas gracias</h2>');

				$('#modal-body').append('<h3>Detalles</h3>');
				$('#modal-body').append('<table class="table table-hover table-striped">\n<thead>\n<th>Concepto</th>\n<th>Valor</th>\n</thead>\n<tbody>\n<tr>\n<td>ID de transaccion</td>\n<td>'+json.credit_phase_id+'</td>\n</tr>\n<tr>\n<td>ID de credito</td>\n<td>'+json.credit_id+'</td>\n</tr>\n<tr>\n<td>Monto solicitado</td>\n<td>'+json.credit_amount_string+'</td>\n</tr>\n</tbody>\n</table>');
	
				$('#modal-footer').empty();
				$('#modal-footer').append('<button class="btn btn-primary" onclick="printDiv(\'modal\')">Generar PDF</button>');
				$('#modal-footer').append('<a onclick="redirect(\'/credits/currency='+currency_id+'\')" class="btn btn-primary">Cerrar</a>');
			},
			error: function(json){
				console.log('Entre a ajaxError()');
				console.log(json);
				$('#modal-header').empty();
				$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
				$('#modal-header').append('<h4 class="modal-title" >Error de conexión</h4>');
			
				$('#modal-body').empty();
				$('#modal-body').append('<i class="fa fa-times fa-5x" aria-hidden="true"></i>');
				$('#modal-body').append('<h2>No pudimos establecer conexión con nuestros sistemas</h2>');
				$('#modal-body').append('<h4>Por favor, recargá la página</h4>');
			
				$('#modal-footer').empty();
				$('#modal-footer').append('<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>');
			},
		});
	});
}
//Ojo porque no funciona la impresion
function payCredit(){
	console.log('Entre a payCredit()');
	var currency_id       = document.getElementById('currency-id').value;
	var pay_amount_string = document.getElementById('amount').value;
	var from_amount       = document.getElementById('from_amount').value;
	var exchange_rate_id  = document.getElementById('exchange_rate_id').value;
	var wallet_id         = document.getElementById('wallet_id').value;
	var credit_id         = document.getElementById('credit-id').value;
	loading();
	amount_string = pay_amount_string.replace(' ','');
	var url = 'http://'+document.domain+':'+location.port+'/ajax/string-to-number/'+currency_id+'/'+ pay_amount_string;
	$.get(url,function(pay_amount){
		$.ajax({
			type: 'post',
			data: {
				_token: document.getElementsByName('_token')[0].value,
				credit_id: credit_id,	
				pay_amount: pay_amount,
				from_amount:from_amount,
				exchange_rate_id:exchange_rate_id,
				wallet_id:wallet_id,
			},
			url: 'http://'+document.domain+':'+location.port+'/ajax/pay-credit',
			success: function(json){
				console.log('Entre a ajaxSuccess()');
				console.log(json);
				$('#modal-header').empty();
				$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="location.reload()"><span aria-hidden="true">&times;</span></button>');
				$('#modal-header').append('<h4 class="modal-title" >Pago realizado</h4>');
			
				$('#modal-body').empty();
				$('#modal-body').append('<i class="fa fa-check fa-5x" aria-hidden="true"></i>');
				$('#modal-body').append('<h2>Realizaste el pago con éxito, muchas gracias</h2>');
				$('#modal-body').append('<h3>Detalles</h3>');
				$('#modal-body').append('<table class="table table-hover table-striped">\n<thead>\n<th>Concepto</th>\n<th>Valor</th>\n</thead>\n<tbody>\n<tr>\n<td>ID de credito</td>\n<td>'+json.credit_id+'</td>\n</tr>\n<tr>\n<td>Monto solicitado</td>\n<td>'+json.credit_amount_string+'</td>\n</tr>\n<tr>\n<td>Monto pagado</td>\n<td>'+json.payed_amount_string+'</td>\n</tr>\n</tbody>\n</table>');
			
				$('#modal-footer').empty();
				$('#modal-footer').append('<button class="btn btn-primary" onclick="printDiv(\'modal\')">Generar PDF</button>');
				$('#modal-footer').append('<button type="button" class="btn btn-primary" data-dismiss="modal" onclick="location.reload()">Cerrar</button>');
			},
			error: function(json){
				console.log('Entre a ajaxError()');
				console.log(json);
				$('#modal-header').empty();
				$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
				$('#modal-header').append('<h4 class="modal-title" >Error de conexión</h4>');
			
				$('#modal-body').empty();
				$('#modal-body').append('<i class="fa fa-times fa-5x" aria-hidden="true"></i>');
				$('#modal-body').append('<h2>No pudimos establecer conexión con nuestros sistemas</h2>');
				$('#modal-body').append('<h4>Por favor, recargá la página</h4>');
			
				$('#modal-footer').empty();
				$('#modal-footer').append('<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>');
			},
		});
	});
}
//Ojo porque no funciona la impresion
function cancelFinancing(){
	console.log('Entre a cancelFinancing()');
	var currency_id = document.getElementById('currency-id').value;
	var amount_string = document.getElementById('amount').value;
	var financing_id = document.getElementById('financing-id').value;
	loading();
	amount_string = amount_string.replace(' ','');
	var url = 'http://'+document.domain+':'+location.port+'/ajax/string-to-number/'+currency_id+'/'+ amount_string;
	$.get(url,function(amount){
		$.ajax({
			type: 'post',
			data: {
				_token: document.getElementsByName('_token')[0].value,
				financing_id: financing_id,	
				amount: amount,
			},
			url: 'http://'+document.domain+':'+location.port+'/ajax/cancel-financing',
			success: function(json){
				console.log('Entre a ajaxSuccess()');
				console.log(json);
				if(json != ''){
					$('#modal-header').empty();
					$('#modal-header').append('<button type="button" class="close" aria-label="Close" data-dismiss="modal" onclick="location.reload()" ><span aria-hidden="true">&times;</span></button>');
					$('#modal-header').append('<h4 class="modal-title" >Financiamiento cancelado</h4>');
				
					$('#modal-body').empty();
					$('#modal-body').append('<i class="fa fa-check fa-5x" aria-hidden="true"></i>');
					$('#modal-body').append('<h2>Quitaste dinero del financiamiento con éxito</h2>');
					$('#modal-body').append('<h3>Detalles</h3>');
					$('#modal-body').append('<table class="table table-hover table-striped">\n<thead>\n<th>Concepto</th>\n<th>Valor</th>\n</thead>\n<tbody>\n<tr>\n<td>ID de financiamiento</td>\n<td>'+json.financing_id+'</td>\n</tr>\n<tr>\n<td>ID de credito</td>\n<td>'+json.credit_id+'</td>\n</tr>\n<tr>\n<td>Monto cancelado</td>\n<td>'+json.financing_cancel_amount_string+'</td>\n</tr>\n<tr>\n<td>Monto financiado actualmente</td>\n<td>'+json.financing_amount_string+'</td>\n</tr>\n<tr>\n<td>'+json.financing_profits_title+'</td>\n<td>'+json.financing_profits_string+'</td>\n</tr>\n</tbody>\n</table>');

					$('#modal-footer').empty();
					$('#modal-footer').append('<button class="btn btn-primary" onclick="printDiv(\'modal\')">Generar PDF</button>');
					$('#modal-footer').append('<button type="button" class="btn btn-primary" data-dismiss="modal" onclick="location.reload()">Cerrar</button>');
				}else{
					$('#modal-header').empty();
					$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
					$('#modal-header').append('<h4 class="modal-title" >Detectamos un error</h4>');
				
					$('#modal-body').empty();
					$('#modal-body').append('<i class="fa fa-times fa-5x" aria-hidden="true"></i>');
					$('#modal-body').append('<h2>Hubo un error al procesar tu solicitud</h2>');
					$('#modal-body').append('<h4>Por favor, intentá nuevamente mas tarde</h4>');
					$('#modal-body').append('<h5>Si seguís teniendo este error, comunicate con nosotros</h5>');
				
					$('#modal-footer').empty();
					$('#modal-footer').append('<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>');
				
				}
			},
			error: function(json){
				console.log('Entre a ajaxError()');
				console.log(json);
				$('#modal-header').empty();
				$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
				$('#modal-header').append('<h4 class="modal-title" >Error de conexión</h4>');
			
				$('#modal-body').empty();
				$('#modal-body').append('<i class="fa fa-times fa-5x" aria-hidden="true"></i>');
				$('#modal-body').append('<h2>No pudimos establecer conexión con nuestros sistemas</h2>');
				$('#modal-body').append('<h4>Por favor, recargá la página</h4>');
			
				$('#modal-footer').empty();
				$('#modal-footer').append('<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>');
			},
		});
	});
}
function inputMoney(){
	console.log('Entre a inputMoney()');
	var currency_id = document.getElementById('currency-id').value;
	var amount_string = document.getElementById('amount').value;
	loading();
	amount_string = amount_string.replace(' ','');
	var url = 'http://'+document.domain+':'+location.port+'/ajax/string-to-number/'+currency_id+'/'+ amount_string;
	$.get(url,function(amount){
		$.ajax({
			type: 'post',
			data: {
				_token: document.getElementsByName('_token')[0].value,
				amount: amount,
				currency_id: currency_id,	
			},
			url: 'http://'+document.domain+':'+location.port+'/ajax/input-money',
			success: function(json){
				console.log('Entre a ajaxSuccess()');
				console.log(json);
				if(json != ''){
					$('#modal-header').empty();
					$('#modal-header').append('<a onclick="redirect(\'/wallets/'+json.wallet_id+'\')" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></a>');
					$('#modal-header').append('<h4 class="modal-title" >Transacción realizada</h4>');
				
					$('#modal-body').empty();
					$('#modal-body').append('<i class="fa fa-check fa-5x" aria-hidden="true"></i>');
					$('#modal-body').append('<h2>Ingresaste dinero con éxito</h2>');
					$('#modal-body').append('<h3>Detalles</h3>');
					$('#modal-body').append('<table class="table table-hover table-striped">\n<thead>\n<th>Concepto</th>\n<th>Valor</th>\n</thead>\n<tbody>\n<tr>\n<td>ID de transaccion</td>\n<td>'+json.wallet_transaction_id+'</td>\n</tr>\n<tr>\n<td>Divisa</td>\n<td>'+json.currency_name+'</td>\n</tr>\n<tr>\n<td>Monto que ingresaste</td>\n<td>'+json.input_amount_string+'</td>\n</tr>\n</tbody>\n</table>');
				
					$('#modal-footer').empty();
					$('#modal-footer').append('<button class="btn btn-primary" onclick="printDiv(\'modal\')">Generar PDF</button>');
					$('#modal-footer').append('<a onclick="redirect(\'/wallets/'+json.wallet_id+'\')" class="btn btn-primary">Cerrar</a>');
				}else{
					console.log('Entre a ajaxSuccess()');
					$('#modal-header').empty();
					$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
					$('#modal-header').append('<h4 class="modal-title" >Detectamos un error</h4>');
				
					$('#modal-body').empty();
					$('#modal-body').append('<i class="fa fa-times fa-5x" aria-hidden="true"></i>');
					$('#modal-body').append('<h2>Hubo un error al procesar tu solicitud</h2>');
					$('#modal-body').append('<h4>Por favor, intentá nuevamente mas tarde</h4>');
					$('#modal-body').append('<h5>Si seguís teniendo este error, comunicate con nosotros</h5>');
				
					$('#modal-footer').empty();
					$('#modal-footer').append('<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>');
				
				}
			},
			error: function(){
				$('#modal-header').empty();
				$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
				$('#modal-header').append('<h4 class="modal-title" >Error de conexión</h4>');
			
				$('#modal-body').empty();
				$('#modal-body').append('<i class="fa fa-times fa-5x" aria-hidden="true"></i>');
				$('#modal-body').append('<h2>No pudimos establecer conexión con nuestros sistemas</h2>');
				$('#modal-body').append('<h4>Por favor, recargá la página</h4>');
			
				$('#modal-footer').empty();
				$('#modal-footer').append('<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>');
			},
		});
	});
	
}
function outputMoney(){
	console.log('Entre a outputMoney()');
	var currency_id = document.getElementById('currency-id').value;
	var amount_string = document.getElementById('amount').value;
	loading();
	amount_string = amount_string.replace(' ','');
	var url = 'http://'+document.domain+':'+location.port+'/ajax/string-to-number/'+currency_id+'/'+ amount_string;
	$.get(url,function(amount){
		$.ajax({
			type: 'post',
			data: {
				_token: document.getElementsByName('_token')[0].value,
				amount: amount,
				currency_id: currency_id,	
			},
			url: 'http://'+document.domain+':'+location.port+'/ajax/output-money',
			success: function(json){
				console.log('Entre a ajaxSuccess()');
				console.log(json);
				if(json != ''){
					$('#modal-header').empty();
					$('#modal-header').append('<a onclick="redirect(\'/wallets/'+json.wallet_id+'\')" class="close" aria-label="Close"><span aria-hidden="true">&times;</span></a>');
					$('#modal-header').append('<h4 class="modal-title" >Transacción realizada</h4>');
				
					$('#modal-body').empty();
					$('#modal-body').append('<i class="fa fa-check fa-5x" aria-hidden="true"></i>');
					$('#modal-body').append('<h2>Retiraste dinero con éxito</h2>');
					$('#modal-body').append('<h3>Detalles</h3>');
					$('#modal-body').append('<table class="table table-hover table-striped">\n<thead>\n<th>Concepto</th>\n<th>Valor</th>\n</thead>\n<tbody>\n<tr>\n<td>ID de transaccion</td>\n<td>'+json.wallet_transaction_id+'</td>\n</tr>\n<tr>\n<td>Divisa</td>\n<td>'+json.currency_name+'</td>\n</tr>\n<tr>\n<td>Monto que retiraste</td>\n<td>'+json.output_amount_string+'</td>\n</tr>\n</tbody>\n</table>');
				
					$('#modal-footer').empty();
					$('#modal-footer').append('<button class="btn btn-primary" onclick="printDiv(\'modal\')">Generar PDF</button>');
					$('#modal-footer').append('<a onclick="redirect(\'/wallets/'+json.wallet_id+'\')" class="btn btn-primary">Cerrar</a>');
				}else{
					$('#modal-header').empty();
					$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
					$('#modal-header').append('<h4 class="modal-title" >Detectamos un error</h4>');
				
					$('#modal-body').empty();
					$('#modal-body').append('<i class="fa fa-times fa-5x" aria-hidden="true"></i>');
					$('#modal-body').append('<h2>Hubo un error al procesar tu solicitud</h2>');
					$('#modal-body').append('<h4>Por favor, intentá nuevamente mas tarde</h4>');
					$('#modal-body').append('<h5>Si seguís teniendo este error, comunicate con nosotros</h5>');
				
					$('#modal-footer').empty();
					$('#modal-footer').append('<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>');
				
				}
			},
			error: function(json){
				console.log('Entre a ajaxError()');
				console.log(json);
				$('#modal-header').empty();
				$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
				$('#modal-header').append('<h4 class="modal-title" >Error de conexión</h4>');
			
				$('#modal-body').empty();
				$('#modal-body').append('<i class="fa fa-times fa-5x" aria-hidden="true"></i>');
				$('#modal-body').append('<h2>No pudimos establecer conexión con nuestros sistemas</h2>');
				$('#modal-body').append('<h4>Por favor, recargá la página</h4>');
			
				$('#modal-footer').empty();
				$('#modal-footer').append('<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>');
			},
		});
	});
	
}
function financiate(){
	console.log('Entre a financiate()');
	var currency_id = document.getElementById('currency-id').value;
	var amount_string = document.getElementById('amount').value;
	var credit_id =document.getElementById('credit-id').value;
	var financing_id = document.getElementById('financing-id').value;
	var insurance = 3;
	if (Number(financing_id) == 0){
		insurance = $("input[name='insurance']:checked").val();
	}else{
		insurance =  document.getElementById('insurace-id').value;
	}
	console.log('Valor del seguro es (deberia ser 0 o 1: '+insurance);
	loading();
	amount_string = amount_string.replace(' ','');
	var url = 'http://'+document.domain+':'+location.port+'/ajax/string-to-number/'+currency_id+'/'+ amount_string;
	$.get(url,function(amount){
		$.ajax({
			type: 'post',
			data: {
				_token: document.getElementsByName('_token')[0].value,
				amount: amount,
				credit_id: credit_id,	
				insurance: insurance,
			},
			url: 'http://'+document.domain+':'+location.port+'/ajax/financiate',
			success: function(json){
				console.log('Entre a ajaxSuccess()');
				console.log(json);
				if(json != ''){
					$('#modal-header').empty();
					$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="location.reload()"><span aria-hidden="true">&times;</span></button>');
					$('#modal-header').append('<h4 class="modal-title" >Financiamiento realizado</h4>');
				
					$('#modal-body').empty();
					$('#modal-body').append('<i class="fa fa-check fa-5x" aria-hidden="true"></i>');
					$('#modal-body').append('<h2>Financiaste dinero con éxito</h2>');
					$('#modal-body').append('<h3>Detalles</h3>');
					$('#modal-body').append('<table class="table table-hover table-striped">\n<thead>\n<th>Concepto</th>\n<th>Valor</th>\n</thead>\n<tbody>\n<tr>\n<td>ID de transaccion</td>\n<td>'+json.financing_phase_id+'</td>\n</tr>\n<tr>\n<td>ID de financiamiento</td>\n<td>'+json.financing_id+'</td>\n</tr>\n<tr>\n<td>ID de credito</td>\n<td>'+json.credit_id+'</td>\n</tr>\n<tr>\n<td>Monto financiado total</td>\n<td>'+json.financing_amount_string+'</td>\n</tr>\n<tr>\n<td>'+json.financing_profits_title+'</td>\n<td>'+json.financing_profits_string+'</td>\n</tr>\n</tbody>\n</table>');
				
					$('#modal-footer').empty();
					$('#modal-footer').append('<button class="btn btn-primary" onclick="printDiv(\'modal\')">Generar PDF</button>');
					$('#modal-footer').append('<button type="button" class="btn btn-primary" data-dismiss="modal" onclick="location.reload()">Cerrar</button>');
				}else{
					$('#modal-header').empty();
					$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
					$('#modal-header').append('<h4 class="modal-title" >Detectamos un error</h4>');
				
					$('#modal-body').empty();
					$('#modal-body').append('<i class="fa fa-times fa-5x" aria-hidden="true"></i>');
					$('#modal-body').append('<h2>Hubo un error al procesar tu solicitud</h2>');
					$('#modal-body').append('<h4>Por favor, intentá nuevamente mas tarde</h4>');
					$('#modal-body').append('<h5>Si seguís teniendo este error, comunicate con nosotros</h5>');
				
					$('#modal-footer').empty();
					$('#modal-footer').append('<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>');
				
				}
			},
			error: function(){
				$('#modal-header').empty();
				$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
				$('#modal-header').append('<h4 class="modal-title" >Error de conexión</h4>');
			
				$('#modal-body').empty();
				$('#modal-body').append('<i class="fa fa-times fa-5x" aria-hidden="true"></i>');
				$('#modal-body').append('<h2>No pudimos establecer conexión con nuestros sistemas</h2>');
				$('#modal-body').append('<h4>Por favor, recargá la página</h4>');
			
				$('#modal-footer').empty();
				$('#modal-footer').append('<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>');
			},
		});
	});
}
function exchange(){
	console.log('Entre a exchange()');
	//Exchange::exchange($wallet, $currency, $amount, $exchange_rate, $date[0])
	var currency_from = document.getElementById('from_currency_id').value;
	var currency_to   = document.getElementById('to_currency_id').value;
	var amount_string = document.getElementById('amount').value;
	var exchange_rate = document.getElementById('exchange-rate-id').value;
	loading();
	amount_string = amount_string.replace(' ','');
	var url = 'http://'+document.domain+':'+location.port+'/ajax/string-to-number/'+currency_from+'/'+ amount_string;
	$.get(url,function(amount_from){
		$.ajax({
			type: 'post',
			data: {
				_token: document.getElementsByName('_token')[0].value,
				amount_from: amount_from,
				currency_from: currency_from,
				currency_to: currency_to,	
				exchange_rate : exchange_rate,
			},
			url: 'http://'+document.domain+':'+location.port+'/ajax/exchange',
			success: function(json){
				console.log('Entre a ajaxSuccess()');
				console.log(json);
				if(json != ''){
					$('#modal-header').empty();
					$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="location.reload()"><span aria-hidden="true">&times;</span></button>');
					$('#modal-header').append('<h4 class="modal-title" >Conversión de divisas</h4>');
				
					$('#modal-body').empty();
					$('#modal-body').append('<i class="fa fa-check fa-5x" aria-hidden="true"></i>');
					$('#modal-body').append('<h2>Realizaste la conversión con éxito</h2>');
					$('#modal-body').append('<h3>Detalles</h3>');
					$('#modal-body').append('<table class="table table-hover table-striped">\n<thead>\n<th>Concepto</th>\n<th>Valor</th>\n</thead>\n<tbody>\n<tr>\n<td>ID de transaccion</td>\n<td>'+json.exchange_id+'</td>\n</tr>\n<tr>\n<td>Divida de origen</td>\n<td>'+json.from_currency_name+'</td>\n</tr>\n<tr>\n<td>Divisa de destino</td>\n<td>'+json.to_currency_name+'</td>\n</tr>\n<tr>\n<td>Monto de egreso</td>\n<td>'+json.from_amount_string+'</td>\n</tr>\n<tr>\n<td>Monto de ingreso</td>\n<td>'+json.to_amount_string+'</td>\n</tr>\n<tr>\n<td>Monto taza de conversion</td>\n<td>'+json.exchange_rate+'</td>\n</tr>\n</tbody>\n</table>');
				
					$('#modal-footer').empty();
					$('#modal-footer').append('<button class="btn btn-primary" onclick="printDiv(\'modal\')">Generar PDF</button>');
					$('#modal-footer').append('<button type="button" class="btn btn-primary" data-dismiss="modal" onclick="location.reload()">Cerrar</button>');
				}else{
					console.log('Entre a ajaxSuccess()');
					$('#modal-header').empty();
					$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
					$('#modal-header').append('<h4 class="modal-title" >Detectamos un error</h4>');
				
					$('#modal-body').empty();
					$('#modal-body').append('<i class="fa fa-times fa-5x" aria-hidden="true"></i>');
					$('#modal-body').append('<h2>Hubo un error al procesar tu solicitud</h2>');
					$('#modal-body').append('<h4>Por favor, intentá nuevamente mas tarde</h4>');
					$('#modal-body').append('<h5>Si seguís teniendo este error, comunicate con nosotros</h5>');
				
					$('#modal-footer').empty();
					$('#modal-footer').append('<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>');
				
				}
			},
			error: function(json){
				console.log('Entre a ajaxError()');
				console.log(json);
				$('#modal-header').empty();
				$('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
				$('#modal-header').append('<h4 class="modal-title" >Error de conexión</h4>');
			
				$('#modal-body').empty();
				$('#modal-body').append('<i class="fa fa-times fa-5x" aria-hidden="true"></i>');
				$('#modal-body').append('<h2>No pudimos establecer conexión con nuestros sistemas</h2>');
				$('#modal-body').append('<h4>Por favor, recargá la página</h4>');
			
				$('#modal-footer').empty();
				$('#modal-footer').append('<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>');
			},
		});
	});
}



function amountVerifier(){
	console.log('Entre a amountVerifier()');
	$('#amount-label').empty();
	var show_button = true;
	var replace_amount = true;
	var amount = document.getElementById('amount').value;
	var max_amount = document.getElementById('max_amount').value;
	var min_amount = document.getElementById('min_amount').value;
	var currency_id = document.getElementById('currency-id').value;
	var url = 'http://'+document.domain+':'+location.port+'/ajax/string-to-number/'+currency_id+'/'+ amount.replace(' ','');
	$.get(url, function(answer){
		if(answer == 'error'){
			$('#submit-button').hide();
			show_button = false;
			replace_amount = false;
			$('#amount-label').append('Debe ingresar un numero valido<br>');
		}
		if(Number(answer) < Number(min_amount)){
			$('#submit-button').hide();
			show_button = false;
			var url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+currency_id+'/'+min_amount+'/1';
			$.get(url, function(min){
				var url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+currency_id+'/'+answer+'/1';
				$.get(url, function(string){
					$('#amount-label').append('Debe ingresar un numero mayor a '+min+' (ingresó '+string+')<br>');
				});
			});
		}
		if(Number(max_amount) != -1 && Number(answer) > Number(max_amount)){
			$('#submit-button').hide();
			show_button = false;
			var url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+currency_id+'/'+max_amount+'/1';
			$.get(url, function(max){
				var url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+currency_id+'/'+answer+'/1';
				$.get(url, function(string){
					$('#amount-label').append('Debe ingresar un numero menor a '+max+' (ingresó '+string+')<br>');
				});
			});
		}
		if(replace_amount){
			var url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+currency_id+'/'+answer+'/1';
			$.get(url, function(string){
				$("#amount").val(string);
				if(show_button && !isNaN(string.substr(-1))){
					$('#submit-button').show();
				}else{
					$('#submit-button').hide();
				}
			});
		}
	});
}
function amountVerifierForPayCredit(){
	console.log('Entre a amountVerifierForPayCredit()');
	$('#amount-label').empty();
	$('#simulate-div').empty();
	var show_button = true;
	var replace_amount = true;
	var amount = document.getElementById('amount').value;
	var max_amount = document.getElementById('max_amount').value;
	var min_amount = document.getElementById('min_amount').value;
	var currency_id = document.getElementById('currency-id').value;
	var url = 'http://'+document.domain+':'+location.port+'/ajax/string-to-number/'+currency_id+'/'+ amount.replace(' ','');
	$.get(url, function(answer){
		if(answer == 'error'){
			$('#submit-button').hide();
			show_button = false;
			replace_amount = false;
			$('#amount-label').append('Debe ingresar un numero valido<br>');
		}
		if(Number(answer) < Number(min_amount)){
			$('#submit-button').hide();
			show_button = false;
			var url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+currency_id+'/'+min_amount+'/1';
			$.get(url, function(min){
				var url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+currency_id+'/'+answer+'/1';
				$.get(url, function(string){
					$('#amount-label').append('Debe ingresar un numero mayor a '+min+' (ingresó '+string+')<br>');
				});
			});
		}
		if(Number(max_amount) != -1 && Number(answer) > Number(max_amount)){
			$('#submit-button').hide();
			show_button = false;
			var url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+currency_id+'/'+max_amount+'/1';
			$.get(url, function(max){
				var url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+currency_id+'/'+answer+'/1';
				$.get(url, function(string){
					$('#amount-label').append('Debe ingresar un numero menor a '+max+' (ingresó '+string+')<br>');
				});
			});
		}
		if(replace_amount){
			var url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+currency_id+'/'+answer+'/1';
			$.get(url, function(string){
				$("#amount").val(string);
				if(show_button && !isNaN(string.substr(-1))){
					$('#simulate-div').empty();
					$('#simulate-div').append('<input type="hidden"  id="from_amount" value="'+answer+'">');
					$('#simulate-div').append('<input type="hidden"  id="exchange_rate_id" value="0">');
					var from_wallet_id = document.getElementById('from_wallet_id').value;
					url = 'http://'+document.domain+':'+location.port+'/ajax/wallets/'+from_wallet_id;
					$.get(url, function(from_wallet){
						var credit_id = document.getElementById('credit-id').value;
						url = 'http://'+document.domain+':'+location.port+'/ajax/credits/'+credit_id;
						$.get(url, function(credit){
							if(credit.currency.id != from_wallet.currency.id){
								url = 'http://'+document.domain+':'+location.port+'/ajax/local-convert/'+credit.currency.iso_code+'/'+from_wallet.currency.iso_code+'/'+answer;
								$.get(url, function(exchange){
									url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+from_wallet.currency.id+'/'+exchange+'/1';
									$.get(url, function(exchange_string){
										url = 'http://'+document.domain+':'+location.port+'/ajax/last-exchange-rate/'+credit.currency.iso_code+'/'+from_wallet.currency.iso_code;
										$.get(url, function(exchange_rate){
											$('#simulate-div').empty();
											$('#simulate-div').append('<input type="hidden"  id="from_amount" value="'+exchange+'">');
											$('#simulate-div').append('<input type="hidden"  id="exchange_rate_id" value="'+exchange_rate.id+'">');

											$('#simulate-div').append('<button class="btn btn-default" onclick="amountVerifierForPayCredit()">Actualizar tasa de conversión</button>');
											$('#simulate-div').append('<br><strong>Resultado de la conversión</strong></br>');
											$('#simulate-div').append('<p>Se utilizarán <strong>'+exchange_string+'</strong> en su wallet de '+from_wallet.currency.name+' para pagar el credito en '+credit.currency.name+'</p>');
											$('#simulate-div').append('<p>Taza de conversión: '+exchange_rate.rate_in_currency+'</p>');
											$('#simulate-div').append('<p>Taza actualizada el '+exchange_rate.diff_for_humans+'</p>');
											$('#simulate-div').append('<p>Nota: mantendremos esta tasa de cambio durante 1 minuto de haber ingresdo el último dígito del monto. Si espera más tiempo, la conversión se realizará con la última tasa de cambio en vigencia. Para actualizar la tasa de cambio, simplemente actualice la tasa de cambio');
										});
									});
								});
							}
						});
					});										
					$('#submit-button').show();
				}else{
					$('#submit-button').hide();
				}
			});
		}
	});
}
function amountVerifierForRequestCredit(){
	console.log('Entre a amountVerifierForRequestCredit()');
	$('#amount-label').empty();
	$('#simulate-div').empty();
	var show_button = true;
	var replace_amount = true;
	var amount = document.getElementById('amount').value;
	var max_amount = document.getElementById('max_amount').value;
	var min_amount = document.getElementById('min_amount').value;
	var currency_id = document.getElementById('currency-id').value;
	var url = 'http://'+document.domain+':'+location.port+'/ajax/string-to-number/'+currency_id+'/'+ amount.replace(' ','');
	$.get(url, function(answer){
		if(answer == 'error'){
			$('#submit-button').hide();
			show_button = false;
			replace_amount = false;
			$('#amount-label').append('Debe ingresar un numero valido<br>');
		}
		if(Number(answer) < Number(min_amount)){
			$('#submit-button').hide();
			show_button = false;
			var url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+currency_id+'/'+min_amount+'/1';
			$.get(url, function(min){
				var url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+currency_id+'/'+answer+'/1';
				$.get(url, function(string){
					$('#amount-label').append('Debe ingresar un numero mayor a '+min+' (ingresó '+string+')<br>');
				});
			});
		}
		if(Number(max_amount) != -1 && Number(answer) > Number(max_amount)){
			$('#submit-button').hide();
			show_button = false;
			var url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+currency_id+'/'+max_amount+'/1';
			$.get(url, function(max){
				var url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+currency_id+'/'+answer+'/1';
				$.get(url, function(string){
					$('#amount-label').append('Debe ingresar un numero menor a '+max+' (ingresó '+string+')<br>');
				});
			});
		}
		if(replace_amount){
			var url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+currency_id+'/'+answer+'/1';
			$.get(url, function(string){
				//if(decimals_flag1){
				//	string = string + currency.decimal_point;
				//}
				//if(decimals_flag2){
				//	string = string + '0';
				//}
				$("#amount").val(string);
				if(show_button && !isNaN(string.substr(-1))){
					$('#submit-button').show();
					simulateCredit();
				}else{
					$('#submit-button').hide();
				}
			});
		}
	});
}
function amountVerifierForFinancing(){
	console.log('Entre a amountVerifierForFinancing()');
	$('#amount-label').empty();
	$('#simulate-div').empty();
	var show_button = true;
	var replace_amount = true;
	var amount = document.getElementById('amount').value;
	var max_amount = document.getElementById('max_amount').value;
	var min_amount = document.getElementById('min_amount').value;
	var currency_id = document.getElementById('currency-id').value;
	var url = 'http://'+document.domain+':'+location.port+'/ajax/string-to-number/'+currency_id+'/'+ amount.replace(' ','');
	$.get(url, function(answer){
		if(answer == 'error'){
			$('#submit-button').hide();
			show_button = false;
			replace_amount = false;
			$('#amount-label').append('Debe ingresar un numero valido<br>');
		}
		if(Number(answer) < Number(min_amount)){
			$('#submit-button').hide();
			show_button = false;
			var url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+currency_id+'/'+min_amount+'/1';
			$.get(url, function(min){
				var url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+currency_id+'/'+answer+'/1';
				$.get(url, function(string){
					$('#amount-label').append('Debe ingresar un numero mayor a '+min+' (ingresó '+string+')<br>');
				});
			});
		}
		if(Number(max_amount) != -1 && Number(answer) > Number(max_amount)){
			$('#submit-button').hide();
			show_button = false;
			var url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+currency_id+'/'+max_amount+'/1';
			$.get(url, function(max){
				var url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+currency_id+'/'+answer+'/1';
				$.get(url, function(string){
					$('#amount-label').append('Debe ingresar un numero menor a '+max+' (ingresó '+string+')<br>');
				});
			});
		}
		if(replace_amount){
			var url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+currency_id+'/'+answer+'/1';
			$.get(url, function(string){
				$("#amount").val(string);
				if(show_button && !isNaN(string.substr(-1))){
					$('#submit-button').show();
					simulateFinancing();
				}else{
					$('#submit-button').hide();
				}
			});
		}
	});
}
function amountVerifierForExchange(){
	console.log('Entre a amountVerifierForExchange()');
	$('#amount-label').empty();
	$('#simulate-div').empty();
	var show_button = true;
	var replace_amount = true;
	var amount = document.getElementById('amount').value;
	if (amount != '') {
		var max_amount = document.getElementById('max_amount').value;
		var min_amount = document.getElementById('min_amount').value;
		var currency_id = document.getElementById('from_currency_id').value;
		var url = 'http://'+document.domain+':'+location.port+'/ajax/string-to-number/'+currency_id+'/'+ amount.replace(' ','');
		$.get(url, function(answer){
			if(answer == 'error'){
				$('#submit-button').hide();
				show_button = false;
				replace_amount = false;
				$('#amount-label').append('Debe ingresar un numero valido<br>');
			}
			if(Number(answer) < Number(min_amount)){
				$('#submit-button').hide();
				show_button = false;
				var url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+currency_id+'/'+min_amount+'/1';
				$.get(url, function(min){
					var url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+currency_id+'/'+answer+'/1';
					$.get(url, function(string){
						$('#amount-label').append('Debe ingresar un numero mayor a '+min+' (ingresó '+string+')<br>');
					});
				});
			}
			if(Number(max_amount) != -1 && Number(answer) > Number(max_amount)){
				$('#submit-button').hide();
				show_button = false;
				var url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+currency_id+'/'+max_amount+'/1';
				$.get(url, function(max){
					var url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+currency_id+'/'+answer+'/1';
					$.get(url, function(string){
						$('#amount-label').append('Debe ingresar un numero menor a '+max+' (ingresó '+string+')<br>');
					});
				});
			}
			if(replace_amount){
				var url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+currency_id+'/'+answer+'/1';
				$.get(url, function(string){
					$("#amount").val(string);
					if(show_button && !isNaN(string.substr(-1))){
						$('#submit-button').show();
						url = 'http://'+document.domain+':'+location.port+'/ajax/currencies/'+currency_id;
						$.get(url, function(from_currency){
							url = 'http://'+document.domain+':'+location.port+'/ajax/currencies/'+document.getElementById('to_currency_id').value;
							$.get(url, function(to_currency){
								url = 'http://'+document.domain+':'+location.port+'/ajax/local-convert/'+from_currency.iso_code+'/'+to_currency.iso_code+'/'+answer;
								$.get(url, function(simulate_exchange){
									url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+to_currency.id+'/'+simulate_exchange+'/1';
									$.get(url, function(simulate_exchange_string){
										url = 'http://'+document.domain+':'+location.port+'/ajax/last-exchange-rate/'+from_currency.iso_code+'/'+to_currency.iso_code;
										$.get(url, function(exchange_rate){
											$('#simulate-div').empty();
											$('#simulate-div').append('<button class="btn btn-default" onclick="amountVerifierForExchange()">Actualizar tasa de conversión</button>');
											$('#simulate-div').append('<br><strong>Resultado de la conversión</strong></br>');
											$('#simulate-div').append('<p>Si realiza la conversión, obtendrá <strong>'+simulate_exchange_string+'</strong> en su wallet de '+to_currency.name+'</p>');
											$('#simulate-div').append('<p>Taza de conversión: '+exchange_rate.rate_in_currency+'</p>');
											$('#simulate-div').append('<p>Taza actualizada el '+exchange_rate.diff_for_humans+'</p>');
											$('#simulate-div').append('<p>Nota: mantendremos esta tasa de cambio durante 1 minuto de haber ingresdo el último dígito del monto. Si espera más tiempo, la conversión se realizará con la última tasa de cambio en vigencia. Para actualizar la tasa de cambio, simplemente actualice la tasa de cambio');
											$('#simulate-div').append('<input type="hidden"  id="exchange-rate-id" value="'+exchange_rate.id+'">');	
										});
									});
								});
							});
						});
						
					}else{
						$('#submit-button').hide();
					}
				});
			}
		});	
	}
}
function simulateCredit(){
	console.log('Entre a simulateCredit()');
	$('#simulate-div').empty();
	var payment_plan_id = document.getElementById('payment_plan_id').value;
	var amount_string = document.getElementById('amount').value;
	var currency_id = document.getElementById('currency-id').value;
	amount_string = amount_string.replace(' ','');
	var url = 'http://'+document.domain+':'+location.port+'/ajax/string-to-number/'+currency_id+'/'+amount_string;
	$.get(url,function(amount){
		url = 'http://'+document.domain+':'+location.port+'/ajax/simulate-credit/'+payment_plan_id;
		$.get(url,function(simulation){
			console.log(simulation);
			var percentage = simulation.interest_percentage + simulation.insurance_percentage.debtor;
			//(1 + $this->interest_percentage + $this->debtor_insurance_percentage);
			var amount_to_pay = amount * percentage;
			url = 'http://'+document.domain+':'+location.port+'/ajax/percentage/'+currency_id+'/'+percentage;
			$.get(url,function(percentage_string){
				url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+currency_id+'/'+(amount_to_pay/simulation.payments_number)+'/1';
				$.get(url,function(payment_amount_string){
					url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+currency_id+'/'+amount_to_pay+'/1';
					$.get(url,function(amount_to_pay_string){
						$('#simulate-div').empty();
						$('#simulate-div').append('<label>Detalles del credito:</label><br>');
						$('#simulate-div').append('<p>Interes del plan elegido: '+percentage_string+'</p>');
						$('#simulate-div').append('<p>Monto de cada cuota: '+payment_amount_string+'</p>');
						$('#simulate-div').append('<p>Total a pagar: '+amount_to_pay_string+'</p>');
					});
				});
			});
		});
	});
}
function simulateFinancing(){
	$('#simulate-div').empty();
	console.log('Entre a simulateFinancing()');
	var currency_id = document.getElementById('currency-id').value;
	var amount_string = document.getElementById('amount').value;
	var financing_id =  document.getElementById('financing-id').value; //0 o ID
	var abort_financing = document.getElementById('is_abort_financing').value; //0 o 1
	var credit_id = document.getElementById('credit-id').value;
	var insurance = 3;
	if (Number(financing_id) == 0){
		insurance = $("input[name='insurance']:checked").val();
	}else{
		insurance =  document.getElementById('insurace-id').value;
	}
	var profits = 0;
	var amount_of_financing = 0;
	var url = 'http://'+document.domain+':'+location.port+'/ajax/string-to-number/'+currency_id+'/'+ amount_string;
	$.get(url,function(amount){
		console.log('monto en ingresado: '+amount)
		url = 'http://'+document.domain+':'+location.port+'/ajax/financings/'+financing_id+'/amount';
		$.get(url,function(financing_amount){
			var url = 'http://'+document.domain+':'+location.port+'/ajax/credits/'+credit_id;
			$.get(url,function(credit){
				if(Number(abort_financing) == 0){//no es para cancelar financiamiento, sino para financiar
					
					if (financing_id == 0) {
						amount_of_financing = amount;
					}else{
						amount_of_financing = Number(amount) + Number(financing_amount);
						console.log('monto ya financiado: '+financing_amount);
					}
				}else{//abort financing
					amount_of_financing = Number(financing_amount) - Number(amount);
				}
				var profit_percentage = Number(credit.interest_percentage) - (Number(credit.interest_percentage) * Number(credit.commission_percentage));
				if (insurance == 1) {
					profit_percentage -= Number(credit.creditor_insurance_percentage);
				}
				profits =  Number(amount_of_financing) * Number(profit_percentage);
				console.log('ganacias'+profits);
				url = 'http://'+document.domain+':'+location.port+'/ajax/currencies/'+currency_id;
				$.get(url, function(currency){
					url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+currency_id+'/'+profits+'/1';
					$.get(url,function(profits_string){
						console.log('financiamiento total: '+amount_of_financing);
						url = 'http://'+document.domain+':'+location.port+'/ajax/number-to-string/'+currency_id+'/'+(Number(amount_of_financing) + Number(profits))+'/1';
						$.get(url,function(payout_back_string){
							if(Number(abort_financing) == 0){//financiar
								$('#simulate-div').empty();
								$('#simulate-div').append('<label>Ganancia esperada:</label><br>');
								if(financing_id == 0){
									$('#simulate-div').append('<p>Con tu financiamiento obtendrás '+payout_back_string+' <strong>(ganancia: '+profits_string+')</strong></p>');
									if(insurance == 0){
										$('#simulate-div').append('<p><strong>¡Ten en cuenta!</strong> Como estás financiando sin asegurar, no podemos garantizar un reintegro total de tu financiamiento, ya que depende de los pagos que realice el beneficiario del crédito</p>');
									}else{
										$('#simulate-div').append('<p><strong>Nota:</strong> Buena elección, asegurando tu financiamiento estarás protegido. Si el beneficiario del crédito no abona sus cuotas, nosotros nos encargaremos de abonar tu parte</p>');
									}
								}else{
									$('#simulate-div').append('<p>Con tu financiamiento, mas lo que ya financiaste, obtendrás '+payout_back_string+' <strong>(ganancia: '+profits_string+')</strong></p>');
									if(insurance == 0){
										$('#simulate-div').append('<p><strong>¡Recuerda!</strong> Financiaste sin asegurar, este valor es solo estimativo, ya que depende exclusivamente de como realice los pagos el beneficiario del crédito</p>');
									}else{
										$('#simulate-div').append('<p><strong>Nota:</strong> Tu financiamiento está asegurado, por lo que recibirás el dinero, pase lo que pase</p>');
									}
								}
								
							}else{
								if (Number(amount_of_financing) >= currency.min_value) {
									$('#simulate-div').empty();
									$('#simulate-div').append('<label>Nuevo monto del financiamiento:</label><br>');
									$('#simulate-div').append('<p>Con tu disminución del financiamiento, obtendrás '+payout_back_string+' <strong>(ganancia: '+profits_string+')</strong></p>');
									if(insurance == 0){
										$('#simulate-div').append('<p><strong>¡Recuerda!</strong> Financiaste sin asegurar, este valor es solo estimativo, ya que depende exclusivamente de como realice los pagos el beneficiario del crédito</p>');
									}else{
										$('#simulate-div').append('<p><strong>Nota:</strong> Tu financiamiento está asegurado, por lo que recibirás el dinero, pase lo que pase</p>');
									}
								}else{
									$('#simulate-div').empty();
									$('#simulate-div').append('<label>Financiamiento inactivo:</label><br>');
									$('#simulate-div').append('<p>Tu financiamiento se agrupará junto a los financiamienstos inactivos, por lo que no recibirás ingresos por este financiamiento</p>');
									$('#simulate-div').append('<p><strong>Nota:</strong> Puedes volver a agregar dinero al financiamiento mientras el crédito siga en su etapa de financiamiento, activándolo nuevamente. No puedes cambiar las preferencias del seguro</p>');
								}
							}
						});
					});
				});
			});
		});
	});
}
function exchangeRateLoaderInExchange(){
	$('#exchange-rate-areachart').empty();
	drawExchangeRate();
	amountVerifierForExchange();
}


function loading(){
	console.log('Entre a loading()');
	$('#modal-body').empty();
	$('#modal-body').append('<i class="fa fa-clock-o fa-5x" aria-hidden="true"></i>');
	$('#modal-body').append('<h2>Estamos procesando su solicitud</h2>');
	$('#modal-body').append('<h4>Por favor espere</h4>');

	$('#modal-footer').empty();
}
function fillAmountId(text){
	console.log('Entre a fillAmountId(text)');
	$("#amount").val(text);
	amountVerifierForPayCredit();
}
function printDiv(divName) {
	var printContents = document.getElementById(divName).innerHTML;
	var originalContents = document.body.innerHTML;
	document.body.innerHTML = printContents;
	window.print();
	document.body.innerHTML = originalContents;
}
function redirect(url){
	location.href = url;
}
/*
	Funciones ajax en las que no imprime la pantalla:
		grantCredit()
		abortCredit()
		payCredit()
		cancelFinancing()
*/
//function editPaymentPlan(payment_plan_id){
//    console.log('Entre a editPaymentPlan(payment_plan_id)');
//    $('#modal-header').empty();
//    $('#modal-header').append('<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>');
//    $('#modal-header').append('<h4 class="modal-title">Ingresar dinero en '+ selected_currency.name+'</h4>');
//
//    $('#modal-body').epmty();
//    var url = 'http://'+document.domain+':'+location.port+'/ajax/payment-plans/'+payment_plan_id;
//    $.get(url,function(payment_plan){
//        url = 'http://'+document.domain+':'+location.port+'/ajax/interests/'+payment_plan.interest_id;
//        $.get(url,function(interest){
//            url = 'http://'+document.domain+':'+location.port+'/ajax/insurances/'+payment_plan.insurance_id;
//            $.get(url,function(insurance){
//                url = 'http://'+document.domain+':'+location.port+'/ajax/custom-date-intervals/'+payment_plan.due_dates_interval_id;
//                $.get(url,function(due_date_interval){
//                    url = 'http://'+document.domain+':'+location.port+'/ajax/currencies';
//                    $.get(url, function(currencies){
//                        url = 'http://'+document.domain+':'+location.port+'/ajax/currencies/'+payment_plan.currency_id;
//                        $.get(url, function(selected_currency){
//                            $('#modal-body').append('<div class="form-group">');
//                            $('#modal-body').append('<label>Divisa del plan de pagos<br></label>');
//                            $('#modal-body').append('<select class="form-control" name="currency_id" id="currency_id" onchange="inputMoneyWhitCurrency(value)"></select>');
//                            $('#currency_id').append('<option value="' + selected_currency.id + ' selected">' + selected_currency.name + '</option>');			
//                            $.each(currencies,function(index,currency){
//                               if (currency.id != selected_currency.id) {
//                                    $('#currency_id').append('<option value="' + currency.id + '">' + currency.name + '</option>');
//                                }
//                            });
//                            $('#modal-body').append('</div>');
//                            $('#modal-body').append('<div class="form-group">');
//                            $('#modal-body').append('<label>Divisa del plan de pagos<br></label>');
//
//                            $('#modal-body').append('<label>Divisa del plan de pagos<br></label>');
//
//                            $('#modal-body').append('<label>Divisa del plan de pagos<br></label>');
//
//                            $('#modal-body').append('</div>');
//                        });
//                    });
//                });
//            });
//        });
//    });
//
//}
function drawExchangeRate(){
    var array = [];
    var currency_from = document.getElementById('from_currency_id').value;
	var currency_to   = document.getElementById('to_currency_id').value;
    var url = 'http://'+document.domain+':'+location.port+'/ajax/data-for-draw-exchange-rate-areachart/'+currency_from+'/'+currency_to;
    $.get(url, function(items){  

        console.log(items);
        $.each(items, function(index,item){
            array.push(item);
        });
        google.charts.load('current', {'packages':['corechart']});
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() {
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Fecha'); // Implicit domain label col.
            data.addColumn('number', 'Tasa de conversión'); // Implicit series 1 data col.
            data.addColumn({type:'string', role:'tooltip'});
            data.addRows(array);
            console.log(array);
            url = 'http://'+document.domain+':'+location.port+'/ajax/currencies/'+currency_from;
            $.get(url,function(from){
                url = 'http://'+document.domain+':'+location.port+'/ajax/currencies/'+currency_to;
                $.get(url,function(to){
                    var options = {
                        title: 'Tasa de conversión '+from.iso_code+'/'+to.iso_code,
                        hAxis: {title: 'Tiempo',  titleTextStyle: {color: '#333'}},
                        vAxis: {minValue: 0},
                        backgroundColor: 'transparent',
                    };
                    $('#exchange-rate-areachart').empty();
                    var chart = new google.visualization.AreaChart(document.getElementById('exchange-rate-areachart'));
                    chart.draw(data, options);
                });
            });
        }
    });
}
function exchangeDetailsAreachart(){
    var array = [];
    var exhange_id = document.getElementById('exchange_id').value;
    var currency_from = document.getElementById('from_currency_id').value;
	var currency_to   = document.getElementById('to_currency_id').value;
    var url = 'http://'+document.domain+':'+location.port+'/ajax/data-for-draw-exchange-details-areachart/'+exhange_id;
    $.get(url, function(items){
        console.log(items);
        $.each(items, function(index,item){
            array.push(item);
        });
        google.charts.load('current', {'packages':['corechart']});
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() {
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Fecha'); // Implicit domain label col.
            data.addColumn('number', 'Tasa de conversión'); // Implicit series 1 data col.
            data.addColumn({type:'string', role:'annotation'}); // annotation role col.
            data.addColumn({type:'string', role:'annotationText'}); // annotationText col.
            data.addColumn({type:'string', role:'tooltip'});
            data.addRows(array);
            console.log(array);
            url = 'http://'+document.domain+':'+location.port+'/ajax/currencies/'+currency_from;
            $.get(url,function(from){
                url = 'http://'+document.domain+':'+location.port+'/ajax/currencies/'+currency_to;
                $.get(url,function(to){
                    var options = {
                        title: 'Tasa de conversión '+from.iso_code+'/'+to.iso_code,
                        hAxis: {title: 'Tiempo',  titleTextStyle: {color: '#333'}},
                        vAxis: {minValue: 0},
                        backgroundColor: 'transparent',
                    };
                    var chart = new google.visualization.AreaChart(document.getElementById('exchange-details-areachart'));
                    chart.draw(data, options);
                });
            });
        }
    });
};
function walletsFoundsDonutchart(){
    var array = [];
    $.get('http://'+document.domain+':'+location.port+'/ajax/data-for-draw-wallets-founds-donutchart', function(items){  
        console.log(items);
        $.each(items, function(index,item){
            array.push(item);
        });
        google.charts.load("current", {packages:["corechart"]});
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() { 
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Wallet'); // Implicit domain label col.
            data.addColumn('number', 'Fondos'); // Implicit series 1 data col.
            data.addColumn({type:'string', role:'tooltip'});
            data.addRows(array);
            url = 'http://'+document.domain+':'+location.port+'/ajax/usd-amount-for-wallets';
            $.get(url, function(amount){
                var options = {
                    title: 'Montos totales en dólares: '+amount,
                    pieHole: 0.35,
                    backgroundColor: 'transparent',
                    chartArea:{width:'60%',height:'70%'},
                    animation: {
                        startup: true,
                        duration: 1000,
                    },
                };
                var chart = new google.visualization.PieChart(document.getElementById('wallets-founds-donutchart'));
                chart.draw(data, options);
            });
        }
    });
};
function walletTransactionsAreachart(){
    var array = [];
    var wallet_id = document.getElementById('wallet_id').value;
    var url = 'http://'+document.domain+':'+location.port+'/ajax/data-for-draw-wallet-transactions-areachart/'+wallet_id;
    $.get(url, function(items){  
        console.log(items);
        $.each(items, function(index,item){
            array.push(item);
        });
        google.charts.load('current', {'packages':['corechart']});
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() {
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Fecha'); // Implicit domain label col.
            data.addColumn('number', 'Balance'); // Implicit series 1 data col.
            data.addColumn({type:'string', role:'tooltip'});
            data.addRows(array);

            var options = {
              title: 'Historial del balande de la cuenta',
              hAxis: {title: 'Tiempo',  titleTextStyle: {color: '#333'}},
              vAxis: {minValue: 0},
              backgroundColor: 'transparent',
            };

            var chart = new google.visualization.AreaChart(document.getElementById('wallet-transactions-areachart'));
            chart.draw(data, options);
        }
    });
};
function creditsWalletDonutchart(){
    var array = [];
    var wallet_id = document.getElementById('wallet_id').value;
    var url = 'http://'+document.domain+':'+location.port+'/ajax/data-for-draw-credits-wallet-donutchart/'+wallet_id;
    $.get(url, function(items){  
        console.log(items);
        $.each(items, function(index,item){
            array.push(item);
        });
        google.charts.load("current", {packages:["corechart"]});
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() { 
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Estado'); // Implicit domain label col.
            data.addColumn('number', 'Cantidad'); // Implicit series 1 data col.
            data.addColumn({type:'string', role:'tooltip'});
            data.addRows(array);
            var options = {
                title: 'Ultimos estados de los créditos',
                pieHole: 0.35,
                backgroundColor: 'transparent',
                chartArea:{width:'60%',height:'70%'},
            };
            var chart = new google.visualization.PieChart(document.getElementById('credits-wallet-donutchart'));
            chart.draw(data, options);
        }
    });
};
function financingsWalletDonutchart(){
    var array = [];
    var wallet_id = document.getElementById('wallet_id').value;
    var url = 'http://'+document.domain+':'+location.port+'/ajax/data-for-draw-financings-wallet-donutchart/'+wallet_id;
    $.get(url, function(items){  
        console.log(items);
        $.each(items, function(index,item){
            array.push(item);
        });
        google.charts.load("current", {packages:["corechart"]});
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() { 
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Estado'); // Implicit domain label col.
            data.addColumn('number', 'Cantidad'); // Implicit series 1 data col.
            data.addColumn({type:'string', role:'tooltip'});
            data.addRows(array);
            var options = {
                title: 'Ultimos estados de los financiamientos',
                pieHole: 0.35,
                backgroundColor: 'transparent',
                chartArea:{width:'60%',height:'70%'},
            };
            var chart = new google.visualization.PieChart(document.getElementById('financings-wallet-donutchart'));
            chart.draw(data, options);
        }
    });
};
function creditsAllDonutchart(){
    var array = [];
    var url = 'http://'+document.domain+':'+location.port+'/ajax/data-for-draw-credits-all-donutchart';
    $.get(url, function(items){  
        console.log(items);
        $.each(items, function(index,item){
            array.push(item);
        });
        google.charts.load("current", {packages:["corechart"]});
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() { 
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Divisa'); // Implicit domain label col.
            data.addColumn('number', 'Cantidad'); // Implicit series 1 data col.
            data.addColumn({type:'string', role:'tooltip'});
            data.addRows(array);
            url = 'http://'+document.domain+':'+location.port+'/ajax/usd-amount-for-credits';
            $.get(url, function(amount){
                var options = {
                    title: 'Créditos totales en dólares: '+amount,
                    pieHole: 0.35,
                    backgroundColor: 'transparent',
                    animation: {
                        startup: true,
                        duration: 1000,
                    },
                    chartArea:{width:'60%',height:'70%'},
                };
                var chart = new google.visualization.PieChart(document.getElementById('credits-all-donutchart'));
                chart.draw(data, options);
            });
        }
    });
};
function financingsAllDonutchart(){
    var array = [];
    var url = 'http://'+document.domain+':'+location.port+'/ajax/data-for-draw-financings-all-donutchart';
    $.get(url, function(items){  
        console.log(items);
        $.each(items, function(index,item){
            array.push(item);
        });
        google.charts.load("current", {packages:["corechart"]});
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() { 
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Divisa'); // Implicit domain label col.
            data.addColumn('number', 'Cantidad'); // Implicit series 1 data col.
            data.addColumn({type:'string', role:'tooltip'});
            data.addRows(array);
            url = 'http://'+document.domain+':'+location.port+'/ajax/usd-amount-for-financings';
            $.get(url, function(amount){  
                var options = {
                    title: 'Financiaciones totales en dólares: '+amount,
                    pieHole: 0.35,
                    backgroundColor: 'transparent',
                    chartArea:{width:'60%',height:'70%'},
                };
                var chart = new google.visualization.PieChart(document.getElementById('financings-all-donutchart'));
                chart.draw(data, options);
            });
        }
    });
};
function creditFinancingAreachart(){
    var array = [];
    var credit_id = document.getElementById('credit_id').value;
    var url = 'http://'+document.domain+':'+location.port+'/ajax/data-for-draw-credit-financing-areachart/'+credit_id;
    $.get(url, function(items){  
        console.log(items);
        $.each(items, function(index,item){
            array.push(item);
        });
        google.charts.load('current', {'packages':['corechart']});
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() {
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Fecha'); // Implicit domain label col.
            data.addColumn('number', 'Financiamientos'); // Implicit series 1 data col.
            data.addColumn({type:'string', role:'tooltip'});
            data.addColumn({type:'boolean', role:'certainty'}); // certainty col.
            data.addRows(array);
            console.log(array);
            var options = {
                title: 'Financiamientos para el credito',
                hAxis: {title: 'Tiempo',  titleTextStyle: {color: '#333'}},
                vAxis: {minValue: 0},
                backgroundColor: 'transparent',
              };
          
              var chart = new google.visualization.AreaChart(document.getElementById('credit-financing-areachart'));
              chart.draw(data, options);
        }
    });
};
function creditPayLeftDonutchart(){
    var array = [];
    var credit_id = document.getElementById('credit_id').value;
    var url = 'http://'+document.domain+':'+location.port+'/ajax/data-for-draw-credit-pay-left-donutchart/'+credit_id;
    $.get(url, function(items){  
        console.log(items);
        $.each(items, function(index,item){
            array.push(item);
        });
        google.charts.load("current", {packages:["corechart"]});
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() { 
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Restante'); // Implicit domain label col.
            data.addColumn('number', 'Monto'); // Implicit series 1 data col.
            data.addColumn({type:'string', role:'tooltip'});
            data.addRows(array);
            var options = {
                title: 'Monto restante por pagar',
                pieHole: 0.35,
                backgroundColor: 'transparent',
                chartArea:{width:'60%',height:'70%'},
            };
            var chart = new google.visualization.PieChart(document.getElementById('credit-pay-left-donutchart'));
            chart.draw(data, options);
        }
    });
};
function walletsFoundsColumnchart(){
    var array = [];
    $.get('http://'+document.domain+':'+location.port+'/ajax/data-for-draw-wallets-founds-donutchart', function(items){  
        console.log(items);
        $.each(items, function(index,item){
            array.push(item);
        });
        google.charts.load('current', {'packages':['bar']});
        google.charts.setOnLoadCallback(drawStuff);
        function drawStuff() {
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Wallet'); // Implicit domain label col.
            data.addColumn('number', ''); // Implicit series 1 data col.
            data.addColumn({type:'string', role:'tooltip'});
            data.addRows(array);
            var options = {
                title: 'Monto en los wallets',
                backgroundColor: 'transparent',
                animation: {
                    startup: true,
                    duration: 1000,
                },
                bar: {
                    groupWidth: "50%"
                },
                chartArea:{width:'50%',height:'60%'}
            };
            var chart = new google.charts.Bar(document.getElementById('wallets-founds-columnchart'));
            // Convert the Classic options to Material options.
            chart.draw(data, google.charts.Bar.convertOptions(options));
        };
    });
};
function paymentsIndexCalendarChart(){
    var array = [];
    var credit_id = document.getElementById('credit_id').value;
    var url = 'http://'+document.domain+':'+location.port+'/ajax/data-for-draw-payments-index-calendarchart/'+credit_id;
    $.get(url, function(items){  
        console.log(items);
        $.each(items, function(index,item){
            var date = new Date(item[0][0],(item[0][1]) - 1,item[0][2]);
            array.push([
                date,
                item[1],
                item[2],
            ]);
        });
        google.charts.load('current', {'packages':['calendar']});
        google.charts.setOnLoadCallback(drawChart);
        
        function drawChart() {
            var dataTable = new google.visualization.DataTable();
            dataTable.addColumn({ type: 'date', id: 'Fecha' });
            dataTable.addColumn({ type: 'number', id: 'Estado' });
            dataTable.addColumn({type:'string', role:'tooltip'});
            dataTable.addRows(array);
        
            var chart = new google.visualization.Calendar(document.getElementById('payments-index-calendarchart'));
        
            var options = {
                subtitle: "Historial de estados de la cuota",
                //height: 350,
                backgroundColor: 'transparent',
                chartArea:{width:'50%',height:'60%'}
            };
        
            chart.draw(dataTable, options);
        };
    });
};
function paymentPhasesCalendarchart(){
    var array = [];
    var payment_id = document.getElementById('payment_id').value;
    var url = 'http://'+document.domain+':'+location.port+'/ajax/data-for-draw-payment-phases-calendarchart/'+payment_id;
    $.get(url, function(items){  
        console.log(items);
        $.each(items, function(index,item){
            var date = new Date(item[0][0],(item[0][1]) - 1,item[0][2]);

            array.push([
                date,
                item[1],
                item[2],
            ]);
        });
        google.charts.load('current', {'packages':['calendar']});
        google.charts.setOnLoadCallback(drawChart);
        
        function drawChart() {
            var dataTable = new google.visualization.DataTable();
            dataTable.addColumn({ type: 'date', id: 'Fecha' });
            dataTable.addColumn({ type: 'number', id: 'Estado' });
            dataTable.addColumn({type:'string', role:'tooltip'});
            dataTable.addRows(array);
        
            var chart = new google.visualization.Calendar(document.getElementById('payment-phases-calendarchart'));
        
            var options = {
                subtitle: "Historial de estados de la cuota",
                //height: 350,
                backgroundColor: 'transparent',
                chartArea:{width:'50%',height:'60%'}
            };
        
            chart.draw(dataTable, options);
        };
    });
};
function creditPhasesCalendarchart(){
    var array = [];
    var credit_id = document.getElementById('credit_id').value;
    var url = 'http://'+document.domain+':'+location.port+'/ajax/data-for-draw-credit-phases-calendarchart/'+credit_id;
    $.get(url, function(items){  
        console.log(items);
        $.each(items, function(index,item){
            var date = new Date(item[0][0],(item[0][1]) - 1,item[0][2]);

            array.push([
                date,
                item[1],
                item[2],
            ]);
        });
        google.charts.load('current', {'packages':['calendar']});
        google.charts.setOnLoadCallback(drawChart);
        
        function drawChart() {
            var dataTable = new google.visualization.DataTable();
            dataTable.addColumn({ type: 'date', id: 'Fecha' });
            dataTable.addColumn({ type: 'number', id: 'Estado' });
            dataTable.addColumn({type:'string', role:'tooltip'});
            dataTable.addRows(array);
        
            var chart = new google.visualization.Calendar(document.getElementById('credit-phases-calendarchart'));
        
            var options = {
                subtitle: "Historial de estados del credito",
                //height: 350,
                backgroundColor: 'transparent',
                chartArea:{width:'50%',height:'60%'}
            };
        
            chart.draw(dataTable, options);
        };
    });
};
function financingPhasesCalendarchart(){
    var array = [];
    var financing_id = document.getElementById('financing_id').value;
    var url = 'http://'+document.domain+':'+location.port+'/ajax/data-for-draw-financing-phases-calendarchart/'+financing_id;
    $.get(url, function(items){  
        console.log(items);
        $.each(items, function(index,item){
            var date = new Date(item[0][0],(item[0][1]) - 1,item[0][2]);

            array.push([
                date,
                item[1],
                item[2],
            ]);
        });
        google.charts.load('current', {'packages':['calendar']});
        google.charts.setOnLoadCallback(drawChart);
        
        function drawChart() {
            var dataTable = new google.visualization.DataTable();
            dataTable.addColumn({ type: 'date', id: 'Fecha' });
            dataTable.addColumn({ type: 'number', id: 'Estado' });
            dataTable.addColumn({type:'string', role:'tooltip'});
            dataTable.addRows(array);
        
            var chart = new google.visualization.Calendar(document.getElementById('financing-phases-calendarchart'));
        
            var options = {
                subtitle: "Historial de estados de la financiacion",
                //height: 350,
                backgroundColor: 'transparent',
                chartArea:{width:'50%',height:'60%'}
            };
        
            chart.draw(dataTable, options);
        };
    });
};
function marginAreachart(){
    var array = [];
    var reputation_id = document.getElementById('reputation_id').value;
    var url = 'http://'+document.domain+':'+location.port+'/ajax/data-for-draw-margin-areachart/'+reputation_id;
    console.log('Anttes de hacer la peticion get, url '+url);
    $.get(url, function(items){  
        console.log('Ya hice la peticion get');
        console.log(items);
        $.each(items, function(index,item){
            array.push(item);
        });
        google.charts.load('current', {'packages':['corechart']});
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() {
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Fecha'); // Implicit domain label col.
            data.addColumn('number', 'Margen'); // Implicit series 1 data col.
            data.addColumn({type:'string', role:'tooltip'});
            data.addRows(array);
            console.log(array);
            var options = {
                title: 'Historial del margen del usuario',
                hAxis: {title: 'Tiempo',  titleTextStyle: {color: '#333'}},
                vAxis: {minValue: 0},
                backgroundColor: 'transparent',
              };
          
              var chart = new google.visualization.AreaChart(document.getElementById('margin-areachart'));
              chart.draw(data, options);
        }
    });
};
function interestAreachart(){
    var array = [];
    var reputation_id = document.getElementById('reputation_id').value;
    var url = 'http://'+document.domain+':'+location.port+'/ajax/data-for-draw-interest-areachart/'+reputation_id;
    console.log('Anttes de hacer la peticion get, url '+url);
    $.get(url, function(items){  
        console.log('Ya hice la peticion get');
        console.log(items);
        $.each(items, function(index,item){
            array.push(item);
        });
        google.charts.load('current', {'packages':['corechart']});
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() {
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Fecha'); // Implicit domain label col.
            data.addColumn('number', 'Porcentaje'); // Implicit series 1 data col.
            data.addColumn({type:'string', role:'tooltip'});
            data.addRows(array);
            console.log(array);
            var options = {
                title: 'Historial de porcentajes de interes',
                hAxis: {title: 'Tiempo',  titleTextStyle: {color: '#333'}},
                vAxis: {minValue: 0},
                backgroundColor: 'transparent',
              };
          
              var chart = new google.visualization.AreaChart(document.getElementById('interest-areachart'));
              chart.draw(data, options);
        }
    });
};
function insuranceAreachart(){
    var array = [];
    var reputation_id = document.getElementById('reputation_id').value;
    var url = 'http://'+document.domain+':'+location.port+'/ajax/data-for-draw-insurance-areachart/'+reputation_id;
    console.log('Anttes de hacer la peticion get, url '+url);
    $.get(url, function(items){  
        console.log('Ya hice la peticion get');
        console.log(items);
        $.each(items, function(index,item){
            array.push(item);
        });
        google.charts.load('current', {'packages':['corechart']});
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() {
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Fecha'); // Implicit domain label col.
            data.addColumn('number', 'Porcentaje'); // Implicit series 1 data col.
            data.addColumn({type:'string', role:'tooltip'});
            data.addRows(array);
            console.log(array);
            var options = {
                title: 'Historial de porcentajes de seguro',
                hAxis: {title: 'Tiempo',  titleTextStyle: {color: '#333'}},
                vAxis: {minValue: 0},
                backgroundColor: 'transparent',
              };
          
              var chart = new google.visualization.AreaChart(document.getElementById('insurance-areachart'));
              chart.draw(data, options);
        }
    });
};
function countryChange(){
    var country_id = document.getElementById('country_id').value;
    var url = 'http://'+document.domain+':'+location.port+'/ajax/districts/'+country_id;
    $.get(url, function(data){
        console.log(data);
        $('#district_id').empty();
        $.each(data,function(index,districtObj){
            $('#district_id').append('<option value="' + districtObj.id + '">' + districtObj.name + '</option>');
        });
    });
}
function districtChange(){
    var district_id = document.getElementById('district_id').value;
    var url = 'http://'+document.domain+':'+location.port+'/ajax/cities/'+district_id;
    $.get(url, function(data){
        console.log(data);
        $('#city_id').empty();
        $.each(data,function(index,cityObj){
            $('#city_id').append('<option value="' + cityObj.id + '">' + cityObj.name + '</option>');
        });
    });

};
$('#initial_limit').on('focusout',function(e){
    var min = e.target.value;
    $('#max_limit_min').empty();
    $('#max_limit_min').append('<input required type="number" class="form-control" placeholder="Monto maximo" name="max_limit" id="max_limit"  min="'+min+'">');

});
$(document).ready(function() {
    $('.table-paginated').DataTable({
        "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"      
        }
    });
} );
//new Vue({
//    el: '#app',
//    data:{
//        credits: [],
//        currencies: [],
//        paymentPlans: [],
//    },
//    methods:{
//        getCredits: function() {
//            var urlCredits = 'credit';
//            axios.get(urlCredits).then(response => {
//                this.credits = response.data
//            });
//        },
//        getCurrencies: function() {
//            var urlCurrencies = 'currency';
//            axios.get(urlCurrencies).then(response => {
//                this.currencies = response.data
//            });
//        },
//        getPaymentPlansByCurrency: function(currency_id) {
//            var urlPaymentPlan = 'payment-plan-by-currency/' + currency_id;
//            axios.get(urlPaymentPlan).then(response => {
//                this.paymentPlans = response.data
//            });
//        },
//    },
//}),
//
//
//new Vue({
//    el: '#credits',
//    created: function(){
//        this.getCredits();
//    },
//    data: {
//        credits: []
//    },
//    methods:{
//        getCredits: function() {
//            var urlCredits = 'credit';
//            axios.get(urlCredits).then(response => {
//                this.credits = response.data
//            });
//        },
//        deleteCredit: function(){
//            toastr.success('Eliminado correctamente');
//        },
//    },
//}),
//new Vue({
//    el: '#payment-plans',
//    created: function(){
//        this.getPaymentPlans();
//    },
//    data: {
//        paymentPlans: []
//    },
//    methods:{
//        getPaymentPlans: function() {
//            var urlPaymentPlan = 'payment-plan';
//            axios.get(urlPaymentPlan).then(response => {
//                this.paymentPlans = response.data
//            });
//        },
//        
//    }
//}),
//new Vue({
//    el: '#currencies',
//    created: function(){
//        this.getCurrencies();
//    },
//    data: {
//        currencies: []
//    },
//    methods:{
//        getCurrencies: function() {
//            var urlCurrencies = 'currency';
//            axios.get(urlCurrencies).then(response => {
//                this.currencies = response.data
//            });
//        },
//    }
//}),
//new Vue({
//    el: '#wallets',
//    created: function(){
//        this.getWallets();
//    },
//    data: {
//        wallets: []
//    },
//    methods:{
//        getWallets: function() {
//            var urlWallets = 'wallet';
//            axios.get(urlWallets).then(response => {
//                this.wallets = response.data
//            });
//        },
//        getCurrency: function(wallet_id) {
//            var urlWallets = 'wallet/'+wallet_id+'/get-currency';
//            axios.get(urlWallets).then(response => {
//                return response.data
//            });
//        },
//    }
//})
//
//