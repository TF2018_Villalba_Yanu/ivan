let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.scripts([
   //'resources/assets/js/toastr.js',
   //'resources/assets/js/vue.js',
   //'resources/assets/js/axios.js',
   //'resources/assets/js/google-charts.js',
   'resources/assets/js/google-charts-offline.js',
   'public/vendor/adminlte/vendor/jquery/dist/jquery.js',
   'public/vendor/adminlte/vendor/jquery/dist/jquery.slimscroll.min.js',
   'public/vendor/adminlte/vendor/bootstrap/dist/js/bootstrap.js',
   'resources/assets/js/functions.js',
   'resources/assets/js/edits.js',
   //'resources/assets/js/chartkick.js',
   'resources/assets/js/charts.js',
   'resources/assets/js/app.js',
], 'public/js/app.js')
   .styles([
      'resources/assets/css/app.css',
   ], 'public/css/app.css');
