<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Payment;

class PaymentsInterests extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:payments-interests';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Encargado de generar intereses para pagos';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $payments = Payment::all();
        foreach ($payments as $payment) {
            $payment->paymentInterests();
        }
    }
}
