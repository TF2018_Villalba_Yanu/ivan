<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Reputation;

class UpdateReputationMargins extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:update-reputation-margins';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Ferifica i los las divisas superan el maximo de inflacion o deflacion y actualiza el margen';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $reputations = Reputation::all();
        foreach($reputations as $reputation){
            $reputation->adjustMargin();
        }
    }
}
