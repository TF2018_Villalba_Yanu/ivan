<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Payment;

class DuePayments extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:due-payments';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Encargado de el vencimiento de los pagos';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $payments = Payment::all();
        foreach($payments as $payment){
            $payment->paymentDue();
        }
        
    }
}
