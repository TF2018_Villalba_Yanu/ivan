<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use App\Models\Role;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        Gate::define('user', function ($user) {
            if ($user->role->id == Role::getUserRole()->id){
                return true;
            }
            return false;
        }); 
        Gate::define('admin', function ($user) {
            if ($user->role->id == Role::getAdminRole()->id){
                return true;
            }
            return false;
        });
        Gate::define('param-manager', function ($user) {
            if ($user->role->id == Role::getParamManagerRole()->id){
                return true;
            }
            return false;
        }); 
        Gate::define('insurance-manager', function ($user) {
            if ($user->role->id == Role::getInsuranceManagerRole()->id){
                return true;
            }
            return false;
        }); 
        Gate::define('commission-manager', function ($user) {
            if ($user->role->id == Role::getCommissionManagerRole()->id){
                return true;
            }
            return false;
        }); 
        Gate::define('lawsuit-manager', function ($user) {
            if ($user->role->id == Role::getLawsuitManagerRole()->id){
                return true;
            }
            return false;
        }); 
        Gate::define('defaulter-manager', function ($user) {
            if ($user->role->id == Role::getDefaulterManagerRole()->id){
                return true;
            }
            return false;
        }); 
        Gate::define('credit-manager', function ($user) {
            if ($user->role->id == Role::getCreditManagerRole()->id){
                return true;
            }
            return false;
        }); 
        Gate::define('audit', function ($user) {
            if ($user->role->id == Role::getAuditRole()->id){
                return true;
            }
            return false;
        }); 
    }
}
