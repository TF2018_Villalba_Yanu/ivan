<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Carbon\Carbon;

class User extends Authenticatable
{
    use Notifiable;

    protected $table = 'users';
    protected $fillable = [
        'name'         ,
        'phone'        ,//can be null
        'email'        ,
        'password'     ,
        'role_id'      ,
        'reputation_id',
        'address_id'   ,
        'profession_id',
        'birthdate'    ,//can be null
        'user_id'      ,//nullable. Usado para la auditoria
    ];
    protected $dates = [
        'birthdate',
    ];

    public function role()
    {
        return $this->belongsTo('App\Models\Role');
    }
    protected $hidden = [
        'password', 'remember_token'
    ];
    public $timestamps = false;

    public function reputation(){
        return $this->belongsTo('App\Models\Reputation');
     }
    public function address(){
        return $this->belongsTo('App\Models\Address');
     }
    public function profession(){
        return $this->belongsTo('App\Models\Profession');
     }
    public function documents(){
        return $this->hasMany('App\Models\Document');
     }
    public function wallets(){
        return $this->hasMany('App\Models\Wallet');
     }
    public function vouchers()
    {
        return $this->hasMany('App\Models\Voucher');
    }
    public function currency(){
        return $this->address->currency();
     }
    public function credits(){
        return Credit::whereIn('wallet_id',$this->wallets->pluck('id')->toArray())->get();
     }
    public function financings(){
        return Financing::whereIn('wallet_id',$this->wallets->pluck('id')->toArray())->get();
     }
    public function financingsInUSD(){
        $wallets = $this->wallets;
        $usd_currency = Currency::getCurrency('USD');
        $usd_amount = 0;
        foreach ($wallets as $wallet) {
            $usd_amount += ExchangeRate::localConvert($wallet->currency->iso_code,$usd_currency->iso_code,$wallet->financingsAmount());
        }
        return $usd_amount;
    }
    public function creditsInUSD(){
        $wallets = $this->wallets;
        $usd_currency = Currency::getCurrency('USD');
        $usd_amount = 0;
        foreach ($wallets as $wallet) {
            $usd_amount += ExchangeRate::localConvert($wallet->currency->iso_code,$usd_currency->iso_code,$wallet->creditsAmount());
        }
        return $usd_amount;
    }
    public function walletsInUSD(){
        $wallets = $this->wallets;
        $usd_currency = Currency::getCurrency('USD');
        $usd_amount = 0;
        foreach ($wallets as $wallet) {
            $usd_amount += ExchangeRate::localConvert($wallet->currency->iso_code,$usd_currency->iso_code,$wallet->accountBalance());
        }
        return $usd_amount;
    }
    public function exchanges(){
        $wallets = $this->wallets;
        $exchanges = new Collection();
        foreach($wallets as $wallet){
            $wallet_exhanges = $wallet->formExchanges;
            foreach($wallet_exhanges as $wallet_exhange){
                $exchanges->add($wallet_exhange);
            }
        }
        $exchanges = $exchanges->sortBy(function($exchange,$key){
            return $exchange->id;
        });
        return $exchanges;
    }
    public function userMargin(Carbon $date_time = null){
        if($date_time == null){
            $date_time = Carbon::now();
        }else{
            if (Carbon::now()->diffInSeconds($date_time) > 180) {
                echo($date_time.' $user->userMargin(..)
                ');
            }
        }
        $margin = $this->reputation->marginUSD($date_time);
        return $margin;
     }
    public function hasCredit(Credit $credit){
        return $this->hasWallet($credit->wallet);
     }
    public function hasVoucher(Voucher $voucher){
        if($voucher->walletTransaction != null){
            return $this->hasWallet($voucher->walletTransaction->wallet);
        }
        if($voucher->creditPhase != null){
            return $this->hasCredit($voucher->creditPhase->credit);
        }
        if($voucher->financingPhase != null){
            return $this->hasFinancing($voucher->financingPhase->financing);
        }
        return false;
    }
    public function hasFinancing(Financing $financing){
        return $this->hasWallet($financing->wallet);
     }
    public function hasPayment(Payment $payment){
        return $this->hasCredit($payment->credit);
     }
    public function hasWallet(Wallet $wallet){
        return $this->wallets->pluck('id')->contains($wallet->id);
     }
    public function hasExchange(Exchange $exchange){
        return $this->exchanges()->pluck('id')->contains($exchange->id);
     }
    public function walletsWithBalance(){
        $this->refresh();
        $return = new Collection();
        foreach(Wallet::where('user_id',$this->id)->cursor() as $wallet){
            if ($wallet->accountBalance() >= $wallet->currency->min_value)
                $return->add($wallet);
        }
        return $return;
     }
    public function creditsThatRetentsTheMargin(){
        $return_credits = new Collection();
        $credits = $this->credits();
        foreach ($credits as $credit){
            if($credit->retainsMargin())
                $return_credits->add($credit);
        }
        return $return_credits;
    }
    public function activeCredits(){
        $active_credits = new Collection;
        $credits = $this->credits();
        foreach ($credits as $credit) {
            if ($credit->isActive()) {
                $active_credits->add($credit);
            }
        }
        return $active_credits;
    }
}
