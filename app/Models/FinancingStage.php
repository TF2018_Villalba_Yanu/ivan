<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;

class FinancingStage extends Model
{
    protected $table = 'financing_stages';
    protected $fillable= [
        'name'       ,
        'description',
    ];
    public $timestamps = false;
    public function financingPhases()
    {
        return $this->hasMany('App\Models\FinancingPhase');
    }
    public function financingStageNexts()
    {
        return $this->hasMany('App\Models\FinancingStageNext');
    }

    public function nextFinancingStages():Collection{
        $next_financing_stages = $this->financingStageNexts;
        $return = new Collection();
        foreach ($next_financing_stages as $financing_stage_next){
            $return->add($financing_stage_next->financingStageNext);
        }
        return $return;
    }
    /*
         1 => Financiacion >>>>>>>>>>>>>> ::getFinance()
         2 => Financiacion cancelada >>>> ::getFinancingAbort()
         3 => Crédito cancelado >>>>>>>>> ::getCreditAbort()
         4 => Acreditado >>>>>>>>>>>>>>>> ::getAccredit()
         5 => Reintegro >>>>>>>>>>>>>>>>> ::getRepayment()
         6 => Reintegro por seguro >>>>>> ::getInsurnceRepayment()
         7 => Reintegrado >>>>>>>>>>>>>>> ::getReturned()
         8 => Incobrable >>>>>>>>>>>>>>>> ::getBad()
         9 => Financiamiento terminado >> ::getCloseFinancing()
        10 => Financiamiento reabierto >> ::getReopenFinancing()
        11 => Reintegro demorado >>>>>>>> ::getRepaymentLate()
    */
    public static function getFinance(){
        return CreditStage::find(1);
    }
    public static function getFinancingAbort(){
        return CreditStage::find(2);
    }
    public static function getCreditAbort(){
        return CreditStage::find(3);
    }
    public static function getAccredit(){
        return CreditStage::find(4);
    }
    public static function getRepayment(){
        return CreditStage::find(5);
    }
    public static function getInsurnceRepayment(){
        return CreditStage::find(6);
    }
    public static function getReturned(){
        return CreditStage::find(7);
    }
    public static function getBad(){
        return CreditStage::find(8);
    }
    public static function getCloseFinancing(){
        return CreditStage::find(9);
    }
    public static function getReopenFinancing(){
        return CreditStage::find(10);
    }
    public static function getRepaymentLate(){
        return CreditStage::find(11);
    }

}
