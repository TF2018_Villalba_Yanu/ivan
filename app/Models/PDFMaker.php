<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PDFMaker extends Model
{
    public static function pdfHeads($title){

    }
    
    /**
     * Este metodo genera un PDF con una tabla de datos. Recibe un par de parametros:
     * @param string $title Titulo principal del informe.
     * @param Collection $tables Coleccion con las colecciones que incluyen los datos de las tablas. La longitud. Se utiliza como guia
     * @param Collection $theads Coleccion que incluye los <thead> de las tablas 
     * @param Collection $table_titles Coleccion que incluye los titulos de los canales
     * @return $pdf
     */
    public static function dataTables($title, $tables, $theads, $table_titles){

    }
}
