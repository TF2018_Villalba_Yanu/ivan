<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InsuranceModifierStage extends Model
{
    protected $table = 'insurance_modifier_stages';
    protected $fillable= [
        'name'       ,
        'description',
    ];
    public $timestamps = false;
    public function insuranceModifier()
    {
        return $this->hasMany('App\Models\InsuranceModifier');
    }

    /*
        01 => 'Disminucion del seguro por comportamiento'-->::getInsuranceDecrease();
        02 => 'Aumento del seguro por comportamiento'------>::getInsuranceIncrease();
    */
    public static function getInsuranceDecrease(){
        return InsuranceModifierStage::find(1);
    }
    public static function getInsuranceIncrease(){
        return InsuranceModifierStage::find(2);
    }
}
