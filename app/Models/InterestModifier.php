<?php

namespace App\Models;
use Carbon\Carbon;

use Illuminate\Database\Eloquent\Model;

class InterestModifier extends Model
{
    protected $table = 'interest_modifiers';
    protected $fillable= [
        'reputation_id'             ,
        'date'                      ,
        'percentage'                ,
        'interest_modifier_stage_id',
        'user_id'                   ,//nullable. Usado para la auditoria
    ];
    protected $dates = [
        'date',
    ];
    public $timestamps = false;
    public function reputation(){
        return $this->belongsTo('App\Models\Reputation');
    }
    public function interestModifierStage(){
        return $this->belongsTo('App\Models\InterestModifierStage');
    }
    public static function percentage(Reputation $reputation){
        if($reputation != null){
            $modifiers = $reputation->interestModifiers;
            $percentage = 0;
            foreach ($modifiers as $modifier)
                $percentage += $modifier->percentage;
            return $percentage;
        }
        echo('Error 49665');
        dd();
     }
    public static function calculatePerformance(Payment $payment, Carbon $date_time){
        //Verifica si el pago existe y su sy última etapa es 'Pagada'
        if ( ($payment == null) || ($payment->lastPaymentPhase()->paymentStage->id != PaymentStage::getPayed()->id) ){
            echo('Error 9648');
            dd();
            return null;
        }
        //Obtiene datos necesarios y los almacena en variables.
        $reputation = $payment->credit->user()->reputation;
        $currency = $payment->credit->currency();
        $due_date = $payment->due_date;
        $payed_date = $payment->lastPaymentPhase()->date;
        $diff_in_days = $due_date->diffInDays($payed_date);
        //Verifica si se paga en el dia o antes
        if(($diff_in_days == 0) || ($payed_date < $due_date)){
            //USD35 * 365dias * 0,000019569variacion = -0,25%
            $modifier = ($diff_in_days + 1) * ExchangeRate::localConvert($currency->iso_code,'USD',$payment->amount()) * 0.000019569;
            $ponderation = abs(InterestModifier::percentage($reputation)) / 0.25;
            if($ponderation == 0){
                $ponderation = 0.01;
            }
            $modifier = ($modifier / $ponderation);
            $modifier = $modifier * -1;
            $new_percentage = InterestModifier::percentage($reputation) + $modifier;
            if($new_percentage >= -0.75){
                return InterestModifier::performanceDecrease($reputation,Number::fix($modifier),$date_time);
            }else{
                return $reputation->interestModifiers->last();
            }
        }
        if($payed_date > $due_date){//Si no, verifica si se paga despues del vencimiento
            //USD35 * 365dias * 0,000074363variacion = +0,95%
            $modifier = $diff_in_days * ExchangeRate::localConvert($currency->iso_code,'USD',$payment->amount()) * 0.000074363;
            $ponderation = abs(InterestModifier::percentage($reputation)) / 0.25;
            if($ponderation == 0){
                $ponderation = 0.01;
            }
            $modifier = ($modifier / $ponderation);
            if((InterestModifier::percentage($reputation) + $modifier) <= 0.75){
                return InterestModifier::performanceIncrease($reputation,Number::fix($modifier),$date_time);                
            }else{
                return $reputation->interestModifiers->last();
            }
        }
    }
    /**
     * NOTA: Este metodo debe ser llamado solamente por el metodocalculatePerformance de $this
     * Crea un InterestModifier negativo para que decremente el interes de la reputacion
     * Como
     *     1 Verifica que $reputation y $percentage no sean nulos.
     *     2 Si $percentage es positico, lo pasa a negativo
     *     3 crea el InterestModifier
     * @param Reputation $reputation
     * @param double $percentage
     * @param Carbon $date_time
     * @return InterestModifier
     */
    public static function performanceDecrease(Reputation $reputation, $percentage, Carbon $date_time){
        //1
        if( ($reputation != null) && ($percentage != 0) ){
            //2
            if($percentage > 0){
                $percentage = $percentage * -1;
            }
            //3
            if(auth()->user() != null){
                $user_id = auth()->user()->id;
            }else{
                $user_id = null;
            }
            $interest_modifier = InterestModifier::create([
                'user_id'                    => $user_id,//Usado para auditoria
                'reputation_id'              => $reputation->id,
                'date'                       => $date_time,
                'percentage'                 => $percentage,
                'interest_modifier_stage_id' => InterestModifierStage::getInterestDecrease()->id,
            ]);
            $interest_modifier->refresh();
            return $interest_modifier;
        }
        return null;
     }
    /**
     * NOTA: Este metodo debe ser llamado solamente por el metodocalculatePerformance de $this
     * Crea un interestModifier para que incremente el interes de la reputacion
     * Como
     *     1 Verifica que $reputation y $percentage no sean nulos.
     *     2 Si $percentage es negativo, lo pasa a positivo
     *     3 crea el InterestModifier
     * @param Reputation $reputation
     * @param double $percentage
     * @param Carbon $date_time
     * @return InterestModifier
     */
    public static function performanceIncrease(Reputation $reputation, $percentage, Carbon $date_time){
        //1
        if( ($reputation != null) && ($percentage != 0) ){
            //2
            if($percentage < 0){
                $percentage = $percentage * -1;
            }
            if(auth()->user() != null){
                $user_id = auth()->user()->id;
            }else{
                $user_id = null;
            }
            $interest_modifier = InterestModifier::create([
                'user_id'                    => $user_id,//Usado para auditoria
                'reputation_id'              => $reputation->id,
                'date'                       => $date_time,
                'percentage'                 => $percentage,
                'interest_modifier_stage_id' => InterestModifierStage::getInterestIncrease()->id,
            ]);
            $interest_modifier->refresh();
            return $interest_modifier;
        }
        return null;
     }
}
