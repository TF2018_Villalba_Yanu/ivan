<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InterestModifierStage extends Model
{
    protected $table = 'interest_modifier_stages';
    protected $fillable= [
        'name'       ,
        'description',
    ];
    public $timestamps = false;
    public function intesestModifier()
    {
        return $this->hasMany('App\Models\InterestModifier');
    }

    /*
        01 => 'Disminucion del interes por comportamiento'-->::getInterestDecrease();
        02 => 'Aumento del interes por comportamiento'------>::getInterestIncrease();
    */
    public static function getInterestDecrease(){
        return InterestModifierStage::find(1);
    }
    public static function getInterestIncrease(){
        return InterestModifierStage::find(2);
    }
}
