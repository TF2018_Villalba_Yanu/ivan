<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentPlan extends Model
{
    protected $table = 'payment_plans';
    protected $fillable = [
        'payments_number'      ,
        'due_dates_interval_id',
        'currency_id'          ,
        'interest_id'          ,
        'insurance_id'         ,
        'user_id'              ,//nullable. Usado para la auditoria
    ];
    protected $dates = [
        'due_dates_interval',
    ];
    public $timestamps = false;
    public function dueDatesInterval()
    {
        return $this->belongsTo('App\Models\CustomDateInterval', 'due_dates_interval_id', 'id');
    }
    public function currency()
    {
        return $this->belongsTo('App\Models\Currency');
    }
    public function interest()
    {
        return $this->belongsTo('App\Models\Interest');
    }
    public function insurance()
    {
        return $this->belongsTo('App\Models\Insurance');
    }

}
