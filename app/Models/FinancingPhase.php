<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FinancingPhase extends Model
{
    protected $table = 'financing_phases';
    protected $fillable= [
        'amount'            ,
        'date'              ,
        'financing_id'      ,
        'financing_stage_id',
        'repayment_id'      ,//nullable
        'user_id'           ,//nullable. Usado para la auditoria
    ];
    protected $dates = [
        'date',
    ];
    public $timestamps = false;
    public function financing()
    {
        return $this->belongsTo('App\Models\Financing');
    }
    public function financingStage()
    {
        return $this->belongsTo('App\Models\FinancingStage');
    }
    public function repayment()
    {
        return $this->belongsTo('App\Models\Repayment');
    }
    public function voucher()
    {
        $vouchers = $this->hasMany('App\Voucher');
        if($vouchers->count() == 1){
            return $vouchers->first();
        }
        if($vouchers->count() == 1){
            return null;
        }
        echo('Error 30303');
        dd();
    }
}
