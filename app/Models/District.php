<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    protected $table = 'districts';
    protected $fillable = [
        'name'      ,
        'country_id',
    ];
    public $timestamps = false;
    public function Country(){
        return $this->belongsTo('App\Models\Country');
    }

    public function cities(){
        return $this->hasMany('App\Models\City');
    }
}
