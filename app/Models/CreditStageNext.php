<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CreditStageNext extends Model
{
    protected $table = 'credit_stage_nexts';
    protected $fillable= [
        'credit_stage_id'     ,
        'credit_stage_next_id',
    ];
    public $timestamps = false;
    public function creditStage()
    {
        return $this->belongsTo('App\Models\CreditStage');
    }
    public function creditStageNext()
    {
        return $this->belongsTo('App\Models\CreditStage', 'credit_stage_next_id', 'id');
    }
}
