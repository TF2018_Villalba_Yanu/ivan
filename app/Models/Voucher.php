<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Voucher extends Model
{
    protected $table = 'vouchers';
    protected $fillable= [
        'voucher_type_id'      ,
        'currency_id'          ,
        'amount'               ,
        'date'                 ,
        'user_id'              ,
        'wallet_transaction_id',//nullable
        'credit_phase_id'      ,//nullable
        'financing_phase_id'   ,//nullable
    ];
    protected $dates = [
        'date',
    ];
    public $timestamps = false;
    public function voucherType()
    {
        return $this->belongsTo('App\Models\VoucherType');
    }
    public function walletTransaction()
    {
        return $this->belongsTo('App\Models\WalletTransaction');
    }
    public function creditPhase()
    {
        return $this->belongsTo('App\Models\CreditPhase');
    }
    public function financingPhase()
    {
        return $this->belongsTo('App\Models\FinancingPhase');
    }
    public function currency()
    {
        return $this->belongsTo('App\Models\Currency');
    }
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'foreign_key', 'other_key');
    }
    /*
        01 'Solicitud de crédito'----------> ::getCreditRequest()=============> ::creditRequest()*************> credit_phase_id
        02 'Pago de crédito'---------------> ::getCreditPay()=================> ::creditPay()*****************> wallet_transaction_id
        03 'Ingreso de dinero'-------------> ::getInputMoney()================> ::inputMoney()****************> wallet_transaction_id
        04 'Extraccion de dinero'----------> ::getOutputMoney()===============> ::outputMoney()***************> wallet_transaction_id
        05 'Crédito cancelado'-------------> ::getAbortCredit()===============> ::abortCredit()***************> credit_phase_id
        06 'Financiamiento'----------------> ::getFinancing()=================> ::financing()*****************> financing_phase_id
        07 'Financiamiento cancelado'------> ::getAbortFinancing()============> ::abortFinancing()************> financing_phase_id
        08 'Crédito financiado cancelado'--> ::getAbortFinancedCredit()=======> ::abortFinancedCredit()*******> financing_phase_id
        09 'Reintegro'---------------------> ::getFinancingReturn()===========> ::financingReturn()***********> financing_phase_id
        10 'Reintegro asegurado'-----------> ::getInsuranceFinancingReturn()==> ::insuranceFinancingReturn()**> financing_phase_id
        11 'Crédito rechazado'-------------> ::getRejectCredit()==============> ::rejectCredit()**************> credit_phase_id
        12 'Abono del seguro'--------------> ::getInsurancePay()==============> ::insurancePay()**************> wallet_transaction_id
        13 'Ingreso por conversión de divisas'--------> ::getExchangeInput()=============> ::exchangeInput()*************> wallet_transaction_id
        14 'Egreso por conversion'---------> ::getExchangeOutput()============> ::exchangeOutput()************> wallet_transaction_id

    */
    /**
     * Llamado desde el metodo estatico  Credit::createCredit()
     */
    public static function creditRequest(CreditPhase $credit_phase,Carbon $date_time){
        return Voucher::create([
            'voucher_type_id'      => VoucherType::getCreditRequest()->id,
            'currency_id'          => $credit_phase->credit->currency()->id,
            'amount'               => Number::fix($credit_phase->credit->amount),
            'date'                 => $date_time,
            'user_id'              => $credit_phase->credit->user()->id,
            'credit_phase_id'      => $credit_phase->id,
        ]);
    }
    /**
     * Llamado desde $wallet->payCredit(..)
     */
    public static function creditPay(WalletTransaction $wallet_transaction,Carbon $date_time){
        return Voucher::create([
            'voucher_type_id'      => VoucherType::getCreditPay()->id,
            'currency_id'          => $wallet_transaction->wallet->currency->id,
            'amount'               => Number::fix($wallet_transaction->amount),
            'date'                 => $date_time,
            'user_id'              => $wallet_transaction->wallet->user->id,
            'wallet_transaction_id'=> $wallet_transaction->id,
        ]);
    }
    /**
     * Llamado desde Wallet::inputMoney(..)
     */
    public static function inputMoney(WalletTransaction $wallet_transaction,Carbon $date_time){
        return Voucher::create([
            'voucher_type_id'      => VoucherType::getInputMoney()->id,
            'currency_id'          => $wallet_transaction->wallet->currency->id,
            'amount'               => Number::fix($wallet_transaction->amount),
            'date'                 => $date_time,
            'user_id'              => $wallet_transaction->wallet->user->id,
            'wallet_transaction_id'=> $wallet_transaction->id,
        ]);
    }
    /**
     * Llamado desde $wallet->outputMoney(..)
     */
    public static function outputMoney(WalletTransaction $wallet_transaction,Carbon $date_time){
        return Voucher::create([
            'voucher_type_id'      => VoucherType::getOutputMoney()->id,
            'currency_id'          => $wallet_transaction->wallet->currency->id,
            'amount'               => Number::fix($wallet_transaction->amount),
            'date'                 => $date_time,
            'user_id'              => $wallet_transaction->wallet->user->id,
            'wallet_transaction_id'=> $wallet_transaction->id,
        ]);
    }
    /**
     * Llamado desde $cridit->abort(..)
     */
    public static function abortCredit(CreditPhase $credit_phase,Carbon $date_time){
        return Voucher::create([
            'voucher_type_id'      => VoucherType::getAbortCredit()->id,
            'currency_id'          => $credit_phase->credit->currency()->id,
            'amount'               => Number::fix($credit_phase->credit->amount),
            'date'                 => $date_time,
            'user_id'              => $credit_phase->credit->user()->id,
            'credit_phase_id'      => $credit_phase->id,
        ]);
    }
    /**
     * Llamado desde Financing::financiate(..)
     */
    public static function financing(FinancingPhase $financing_phase,Carbon $date_time){
        return Voucher::create([
            'voucher_type_id'      => VoucherType::getFinancing()->id,
            'currency_id'          => $financing_phase->financing->wallet->currency->id,
            'amount'               => Number::fix($financing_phase->amount),
            'date'                 => $date_time,
            'user_id'              => $financing_phase->financing->wallet->user->id,
            'financing_phase_id'   => $financing_phase->id,
        ]);
    }
    /**
     * Llamado desde $financing->abortFinancing(..)
     */
    public static function abortFinancing(FinancingPhase $financing_phase,Carbon $date_time){
        return Voucher::create([
            'voucher_type_id'      => VoucherType::getAbortFinancing()->id,
            'currency_id'          => $financing_phase->financing->wallet->currency->id,
            'amount'               => Number::fix($financing_phase->amount),
            'date'                 => $date_time,
            'user_id'              => $financing_phase->financing->wallet->user->id,
            'financing_phase_id'   => $financing_phase->id,
        ]);
    }
    /**
     * Llamado desde $financing->abortCredit(..)
     */
    public static function abortFinancedCredit(FinancingPhase $financing_phase,Carbon $date_time){
        return Voucher::create([
            'voucher_type_id'      => VoucherType::getAbortFinancedCredit()->id,
            'currency_id'          => $financing_phase->financing->wallet->currency->id,
            'amount'               => Number::fix($financing_phase->amount),
            'date'                 => $date_time,
            'user_id'              => $financing_phase->financing->wallet->user->id,
            'financing_phase_id'   => $financing_phase->id,
        ]);
    }
    /**
     * Llamado desde $financing->return(..)
     */
    public static function financingReturn(FinancingPhase $financing_phase,Carbon $date_time){
        return Voucher::create([
            'voucher_type_id'      => VoucherType::getFinancingReturn()->id,
            'currency_id'          => $financing_phase->financing->wallet->currency->id,
            'amount'               => Number::fix($financing_phase->amount),
            'date'                 => $date_time,
            'user_id'              => $financing_phase->financing->wallet->user->id,
            'financing_phase_id'   => $financing_phase->id,
        ]);
    }
    /**
     * Llamado desde $financing->insuranceReturn(..)
     */
    public static function insuranceFinancingReturn(FinancingPhase $financing_phase,Carbon $date_time){
        return Voucher::create([
            'voucher_type_id'      => VoucherType::getInsuranceFinancingReturn()->id,
            'currency_id'          => $financing_phase->financing->wallet->currency->id,
            'amount'               => Number::fix($financing_phase->amount),
            'date'                 => $date_time,
            'user_id'              => $financing_phase->financing->wallet->user->id,
            'financing_phase_id'   => $financing_phase->id,
        ]);
    }
    /**
     * Llamado desde $credit->reject(..)
     */
    public static function rejectCredit(CreditPhase $credit_phase,Carbon $date_time){
        return Voucher::create([
            'voucher_type_id'      => VoucherType::getRejectCredit()->id,
            'currency_id'          => $credit_phase->credit->currency()->id,
            'amount'               => Number::fix($credit_phase->credit->amount),
            'date'                 => $date_time,
            'user_id'              => $credit_phase->credit->user()->id,
            'credit_phase_id'      => $credit_phase->id,
        ]);
    }
    /**
     * Llamado desde $wallet->insuranceCreditorPayment(..)
     */
    public static function insurancePay(WalletTransaction $wallet_transaction,Carbon $date_time){
        return Voucher::create([
            'voucher_type_id'      => VoucherType::getInsurancePay()->id,
            'currency_id'          => $wallet_transaction->wallet->currency->id,
            'amount'               => Number::fix($wallet_transaction->amount),
            'date'                 => $date_time,
            'user_id'              => $wallet_transaction->wallet->user->id,
            'wallet_transaction_id'=> $wallet_transaction->id,
        ]);
    }
    /**
     * Llamado desde
     */
    public static function exchangeInput(WalletTransaction $wallet_transaction,Carbon $date_time){
        return Voucher::create([
            'voucher_type_id'      => VoucherType::getExchangeInput()->id,
            'currency_id'          => $wallet_transaction->wallet->currency->id,
            'amount'               => Number::fix($wallet_transaction->amount),
            'date'                 => $date_time,
            'user_id'              => $wallet_transaction->wallet->user->id,
            'wallet_transaction_id'=> $wallet_transaction->id,
        ]);
    }
    /**
     * Llamado desde $wallet->exchangeOutput(..)
     */
    public static function exchangeOutput(WalletTransaction $wallet_transaction,Carbon $date_time){
        return Voucher::create([
            'voucher_type_id'      => VoucherType::getExchangeOutput()->id,
            'currency_id'          => $wallet_transaction->wallet->currency->id,
            'amount'               => Number::fix($wallet_transaction->amount),
            'date'                 => $date_time,
            'user_id'              => $wallet_transaction->wallet->user->id,
            'wallet_transaction_id'=> $wallet_transaction->id,
        ]);
    }
}
