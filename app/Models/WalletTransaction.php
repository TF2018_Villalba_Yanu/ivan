<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WalletTransaction extends Model
{
    protected $table = 'wallet_transactions';
    protected $fillable= [
        'wallet_id'                    ,
        'wallet_type_of_transaction_id',
        'amount'                       ,
        'date'                         ,
        'input_money_transaction_id'   ,//nullable
        'output_money_transaction_id'  ,//nullable
        'credit_phase_id'              ,//nullable
        'financing_phase_id'           ,//nullable
        'payment_phase_id'             ,//nullable
        'repayment_id'                 ,//nullable
        'exchange_id'                  ,//nullable
        'user_id'                      ,//nullable. Usado para la auditoria
    ];
    protected $dates = [
        'date',
    ];
    public $timestamps = false;
    public function wallet(){
        return $this->belongsTo('App\Models\Wallet');
     }
    public function walletTypeOfTransaction(){
        return $this->belongsTo('App\Models\WalletTypeOfTransaction');
     }
    public function inputMoneyTransaction(){
        return $this->belongsTo('App\Models\InputMoneyTransaction');
     }
    public function outputMoneyTransaction(){
        return $this->belongsTo('App\Models\OutputMoneyTransaction');
     }
    public function creditPhase(){
        return $this->belongsTo('App\Models\CreditPhase');
     }
    public function financingPhase(){
        return $this->belongsTo('App\Models\FinancingPhase');
     }
    public function paymentPhase(){
        return $this->belongsTo('App\Models\PaymentPhase');
     }
    public function repayment(){
        return $this->belongsTo('App\Models\Repayment');
    }
    public function exchange()
    {
        return $this->belongsTo('App\Models\Exchange');
    }
    public function credit(){
        $object = $this->creditPhase;
        if ($object != null) {
            return $object->credit;
        }
        $object = $this->paymentPhase;
        if ($object != null) {
            return $object->payment->credit;
        }
        return null;
    }
    public function financing(){
        $object = $this->financingPhase;
        if ($object != null) {
            return $object->financing;
        } 
        $object = $this->repayment;
        if ($object != null) {
            return $object->financing;
        }
        return null;
    }
    public function voucher()
    {
        $vouchers = $this->hasMany('App\Voucher');
        if($vouchers->count() == 1){
            return $vouchers->first();
        }
        if($vouchers->count() == 1){
            return null;
        }
        echo('Error 20152005632');
        dd();
    }
}
