<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentStageNext extends Model
{
    protected $table = 'payment_stage_nexts';
    protected $fillable= [
        'payment_stage_id'     ,
        'payment_stage_next_id',
    ];
    public $timestamps = false;
    public function paymentStage()
    {
        return $this->belongsTo('App\Models\PaymentStage');
    }
    public function paymentStageNext()
    {
        return $this->belongsTo('App\Models\PaymentStage', 'payment_stage_next_id', 'id');
    }
}
