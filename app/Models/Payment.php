<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
class Payment extends Model
{
    protected $table = 'payments';
    protected $fillable = [
        'due_date' ,
        'credit_id',
        'user_id'  ,//nullable. Usado para la auditoria
    ];
    protected $dates = [
        'due_date'    ,
    ];
    public $timestamps = false;
    public function credit(){
        return $this->belongsTo('App\Models\Credit');
     }
    public function paymentPhases(){
        return $this->hasMany('App\Models\PaymentPhase');
     }
    public function nextPaymentStages():Collection{
        $last_phase = $this->lastPaymentPhase();
        $return = new Collection();
        if($last_phase == null){
            $return->add(PaymentStage::getGenerated());
        }else{
            $return = $last_phase->paymentStage->nextPaymentStages();
        }
        return $return;
     }
    public function lastPaymentPhase(){
        return $this->paymentPhases->last();
     }
    public function amount(){
        $payment_stages = new Collection();
        $payment_stages->add(PaymentStage::getGenerated())->add(PaymentStage::getAddInterest());
        $payment_phases = $this->paymentPhases;
        $amount = 0;
        foreach($payment_phases as $payment_phase)
            if($payment_stages->pluck('id')->contains($payment_phase->paymentStage->id))
                $amount += $payment_phase->amount;
        return $amount;
     }
    public function payedAmount(){
        $payment_phases = $this->paymentPhases;
        $amount = 0;
        foreach($payment_phases as $payment_phase){
            if ($payment_phase->paymentStage->id == PaymentStage::getPay()->id)
                $amount += $payment_phase->amount;
        }
        return $this->credit->currency()->truncate($amount);
     }
    public function unpayedAmount(){
        $payment_phases = $this->paymentPhases;
        $amount = $this->amount();
        $amount -= $this->payedAmount();
        return $this->credit->currency()->truncate($amount);
     }
    public function hasDue(){
        $payment_phases = $this->paymentPhases;
        foreach($payment_phases as $payment_phase){
            if($payment_phase->paymentStage->id == PaymentStage::getDue()->id){
                return true;
            }
        }
        return false;
     }
    public function number(){
        $number = 0;
        $payments = $this->credit->payments;
        foreach ($payments as $payment) {
            $number++;
            if ($payment->id == $this->id) {
                return $number;
            }
        }
     }
    public function retainsMargin(){
        /*
            3 => 'Vencido' ----------> getDue()
            4 => 'Pagado temporalmente por el seguro' --> getInsurancePay()
            5 => 'Interes agregado' -> getAddInterest()
            7 => 'Incobrable' -------> getBad()
       */
       $payment_stages = new Collection();
       $payment_stages->add(PaymentStage::getDue())->add(PaymentStage::getInsurancePay())->add(PaymentStage::getAddInterest())->add(PaymentStage::getBad());
       return $payment_stages->pluck('id')->contains($this->lastPaymentPhase()->paymentStage->id);
     }
    public function lastPhaseGeneratedOrPayOrPayedOrDueDate(){
        $payment_phases = $this->paymentPhases;
        $item = $this->due_date;
        if ($payment_phases->count() > 0) {
            foreach ($payment_phases as $payment_phase) {
                if (($payment_phase->paymentStage->id == PaymentStage::getGenerated()->id) || ($payment_phase->paymentStage->id == PaymentStage::getPay()->id) || ($payment_phase->paymentStage->id == PaymentStage::getPayed()->id)) {
                    if($item < $payment_phase->date){
                        $item = $payment_phase->date;
                    }
                }
            }
        }
        return $item;
    }
    /*
        1 => 'Generado'---------------------------> getGenerated()----> generate(Crepayment)
        2 => 'Pago'-------------------------------> getPay()----------> pay($amount)
        3 => 'Vencido'----------------------------> getDue()----------> paymentDue()
        4 => 'Pagado temporalmente por el seguro'-> getInsurancePay()-> insuranceEffect()
        5 => 'Interes agregado'-------------------> getAddInterest()--> paymentInterests()
        6 => 'Saldado'----------------------------> getPayed()--------> payed()
        7 => 'Incobrable'-------------------------> getBad()----------> bad($credit)
     */
    /**
     * NOTA: Este metoro es invocado unicamente por el metodo grant() de Credit
     * Genera los pagos para un credito
     * Como:
     *     1 Verifica si se pueden generar los pagos.
     *         1.1 Calcula el monto a pagar por el credito.
     *     2 Crea los pagos 
     *         2.1 y le agrega la fase de generado
     *     3 Retorna un array con los pagos
     * @param Credit $credit
     * @param Carbon $date_time
     * @return Collection
     */
    public static function generate(Credit $credit,Carbon $date_time){
        //1
        if($credit->lastCreditPhase()->creditStage->id == CreditStage::getGrant()->id){
            $payments = new Collection;
            //1.1
            $amount_for_payments = ($credit->amount * $credit->debtorInterests())/$credit->payments_number;
            for ($i=0; $i<$credit->payments_number; $i++){
                //Calculo de fecha de vencimiento
                if($i==0){
                    $due_date = new Carbon($date_time->toDateTimeString());
                    $due_date->add($credit->dueDatesInterval->toDateInterval());
                }else{
                    $due_date = $payments->last()->due_date;
                    $due_date->add($credit->dueDatesInterval->toDateInterval());
                }
                //2
                $user_id = null;
                if(auth()->user() != null){
                    $user_id = auth()->user()->id;
                }
                $payment = Payment::create([
                    'due_date'  => $due_date,
                    'credit_id' => $credit->id,
                    'user_id'   => $user_id,
                ]);
                $payment->refresh();
                //2.1
                $user_id = null;
                if(auth()->user() != null){
                    $user_id = auth()->user()->id;
                }
                $payment_phase = PaymentPhase::create([
                    'amount'           => Number::fix(round($amount_for_payments,$credit->currency()->significant_decimals)),
                    'date'             => $date_time,
                    'payment_id'       => $payment->id,
                    'payment_stage_id' => PaymentStage::getGenerated()->id,
                    'user_id'          => $user_id,
                ]);
                $payment_phase->refresh();
                $payment->refresh();
                $payments->add($payment);
            }
            //3
            if($payments->isEmpty()){
                echo('Error 85248');
                dd();
            }
            return $payments;
        }
        return null;

     }
    /**
     * NOTA: Este metodo puede ser invocado solamente por el metodo payCredit(..) de Wallet
     * Registra el un pago para una cuota y realiza las operaciones correspondientes
     * Como:
     *      1 Verifica si se puede pagar
     *      2 Verifica si este es el pago siguiente a pagar
     *      3 Genera el PaymentPhase 
     *      4 Busca el PaymentPhase 'getInsurancePay' (si esta variable NO es nula, significa que el pago esta vencido)'
     *      5 Otione y recorre todos los financiamientos. Por cada financiamiento activo realiza lo siguiente:
     *          6 Calcula el mont que le corresponde al financiamiento ($repayment_amount)
     *          7 Verifica si $insurance_payment_phase (pago vencido) y si el financiamiento tiene seguro
     *              7.1 Busca el repayment del pago del seguro
     *              7.2 Envia un mensaje al metodo Insurance PaymentReplace
     *          ----
     *          8 Si el pago no esta vencido o el financiamiento no tiene seguro
     *              8.1 Envia un mensaje a payCommissionAndDebtorInsurance
     *              8.2 Se actualiza el $repayment_amount
     *              8.3 Se genera el Repayment
     *              8.4 Llama al metodo 'return' para que registre el pago 
     *      9 Verifica si la deuda del pago restante es menor al monto minimo
     *          9.1 Si es asi, envia un mensaje a payed para que genere el PaymentPhase
     *      10 Devuelve el ultimo estado generado
     * @param float amount
     * @param Carbon $date_time
     * @return PaymentPhase
     */
    public function pay($amount, Carbon $date_time){
        //1 Verifica si se puede pagar
        if ($this->nextPaymentStages()->pluck('id')->contains(PaymentStage::getPay()->id)) {
            if ($amount >= $this->credit->currency()->min_value) {
                if ($this->unpayedAmount() >= $this->credit->currency()->min_value) {
                    //2 Verifica si este es el pago siguiente a pagar
                    if ($this->credit->firstUnpayedPayment($date_time)->id == $this->id) {
                        //3 Genera el PaymentPhase 
                        $user_id = null;
                        if(auth()->user() != null){
                            $user_id = auth()->user()->id;
                        }
                        $payment_phase = PaymentPhase::create([
                            'amount'           => Number::fix($amount),
                            'date'             => $date_time,
                            'payment_id'       => $this->id,
                            'payment_stage_id' => PaymentStage::getPay()->id,
                            'user_id'          => $user_id,
                        ]);
                        $payment_phase->refresh();
                        $this->refresh();
                        //4 Busca el PaymentPhase 'getInsurancePay' (si esta variable es nula, significa que el pago no esta vencido)
                        $insurance_payment_phase = PaymentPhase::where('payment_id', $this->id)
                                            ->where('payment_stage_id',PaymentStage::getInsurancePay()->id)
                                            ->first();
                        //5 Otione y recorre todos los financiamientos. Por cada financiamiento activo realiza lo siguiente:
                        $financings = $this->credit->financings;
                        foreach($financings as $financing){
                            if($financing->isActive()){
                                //6 Calcula el monto que le corresponde al financiamiento ($repayment_amount)
                                $repayment_amount = ($financing->amount() / $this->credit->amount) * $amount;
                                //7 Verifica si $insurance_payment_phase (pago vencido) y si el financiamiento tiene seguro
                                if (($insurance_payment_phase != null) && ($financing->have_insurance)) {
                                    //7.1 Busca el repayment del pago del seguro
                                    $repayment = Repayment::where('financing_id', $financing->id)
                                                ->where('payment_phase_id',$insurance_payment_phase->id)
                                                ->first();
                                    //7.2 Envia un mensaje al metodo Insurance PaymentReplace
                                    if (Wallet::insurancePaymentReplace($repayment,$repayment_amount, $date_time) == null) {
                                        echo('Error 887422114');
                                        dd();
                                    } 
                                }else{//8 Si el pago no esta vencido o el financiamiento no tiene seguro
                                    //8.1 Envia un mensaje a payCommissionAndDebtorInsurance
                                    $amount_payed = $financing->wallet->payCommissionAndDebtorInsurance($payment_phase, $financing, $date_time);
                                    //8.2 Se actualiza el $repayment_amount
                                    $repayment_amount -= $amount_payed;
                                    //8.3 Se genera el Repayment
                                    if(auth()->user() != null){
                                        $user_id = auth()->user()->id;
                                    }else{
                                        $user_id = null;
                                    }
                                    $repayment = Repayment::create([
                                        'user_id'          => $user_id,
                                        'financing_id'     => $financing->id,
                                        'payment_phase_id' => $payment_phase->id,
                                        'amount'           => Number::fix($repayment_amount),
                                    ]);
                                    $financing->refresh();
                                    $payment_phase->refresh();
                                    //8.4 Llama al metodo 'return' para que registre el pago 
                                    if ($financing->return($repayment,$date_time) == null) {
                                        echo('Error 582505');
                                        dd();
                                    } 
                                }
                            }
                        }//END FOREACH
                        //9 Verifica si la deuda del pago restante es menor al monto minimo
                        if (($this->unpayedAmount()) < $this->credit->currency()->min_value) {
                            //9.1 Si es asi, envia un mensaje a payed para que genere el PaymentPhase
                            $payment_phase = $this->payed($date_time);
                            if($payment_phase == null){
                                echo('Error 85144');
                                dd();
                            }      
                        }       
                        //10 Devuelve el ultimo estado generado
                        return $payment_phase;
                    }
                    echo('Error 7595112');
                    dd();
                }
                echo('Error 885485');                
                dd();
            }
            echo('Error 5211144');
            dd();
        }
        echo('Error 2004520 '.($this->nextPaymentStages()->pluck('id')->contains(PaymentStage::getPay()->id)).' '.($this->unpayedAmount() > $this->credit->currency()->min_value).' '.($this->unpayedAmount() >= $amount).' '.($amount >= $this->credit->currency()->min_value).' '.$this->credit->currency()->name.' '.$this->credit->amount.' Etapa: '.$this->lastPaymentPhase()->paymentStage->name);
        dd();
        return null;
     }
    /**
     * NOTA: La idea de este metodo es ejecutarlo con un cron en un periodo de tiempo determinado
     * Busca pagos vencidos y actualiza el estado del pago y paga los seguros
     * Como:
     *     1 Calcula si el pago puede pasar a la etapa "vencido" y tiene deuda
     *     2 Calcula si esta vencido
     *         2.1 En caso de que si, crea la etapa vencido
     *         2.2 Si no hay un pago anterior retrasado, envia un mensaje al crédito para establecer pago retrasado
     *         2.3 Llama al metodo pagado temporalmente por seguro.
     * @return null
     * 
     */
    public function paymentDue(Carbon $date_time = null){
        if($date_time == null){
            $date_time = Carbon::now();
        }else{
            if (Carbon::now()->diffInSeconds($date_time) > 180) {
                echo($date_time.' $payment->paymentDue(..)
                ');
            }
        }
        //1
        if(($this->nextPaymentStages()->pluck('id')->contains(PaymentStage::getDue()->id)) && ($this->unpayedAmount() > $this->credit->currency()->min_value)){
            if (!$this->retainsMargin()) {
                //2 Calcula si esta vencido
                if($date_time > $this->due_date){
                    //2.1
                    $user_id = null;
                    if(auth()->user() != null){
                        $user_id = auth()->user()->id;
                    }
                    $payment_phase = PaymentPhase::create([
                        'amount'           => Number::fix(0),
                        'date'             => $date_time,
                        'payment_id'       => $this->id,
                        'payment_stage_id' => PaymentStage::getDue()->id,
                        'user_id'          => $user_id,
                    ]);
                    $this->refresh();

                    //2.2 Si no hay un pago anterior retrasado, envia un mensaje al crédito para establecer pago retrasado
                    if ((!$this->credit->retainsMargin()) && ($this->credit->firstUnpayedPayment($date_time)->hasDue())) {
                        if ($this->credit->latePayment($date_time) == null) {
                            echo('Error 5145');
                            dd();
                        }
                    }
                    //2.3 Llama al metodo pagado temporalmente por seguro.
                    if($this->insuranceEffect($date_time) == null){
                        echo('Error 5651897');
                        dd();
                    }
                    return $payment_phase;
                }

            }
        }
        return null;
     }
    /**
     * NOTA Este metodo solamente debe ser llamado por el metodo paymentDue de Payment cuando detecta créditos vencidos con financiamientos asegurados
     * Se encarga de pagar los créditos con seguro
     * Como:
     *      1 Verifica que se pueda crear el PaymentPhase
     *      2 Crea la fase pagado temporalmente por seguro
     *      3 Recorre los financiamientos. Por cada financiamiento activo:
     *      4 Si tiene seguro
     *          4.1 Envia un mensaje a payCommissionAndDebtorInsurance del wallet del financiamiento
     *          4.2 Calcula el monto para el Repayment
     *          4.3 Genera el repayment
     *          4.4 Agrega el repayment a una coleccion que va a retornar despues.
     *          4.5 Envia un mensaje a insurancePayment para que registre la salida de dinero para el pago del seguro
     *          4.6 Envia un mensaje a financing, llamando el metodo insuranceReturn
     *      ---
     *      5 Si no tiene seguro
     *          5.1 Envia un mensaje a lateReturn() para indicar el pago retrasado
     * @param Carbon $date_time
     * @return Collection
     */
    public function insuranceEffect(Carbon $date_time){
        //1
        if($this->nextPaymentStages()->pluck('id')->contains(PaymentStage::getInsurancePay()->id)){
            if($this->unpayedAmount() > $this->credit->currency()->min_value){
                if($date_time > $this->due_date){
                    //2 Crea la fase pagado temporalmente por seguro
                    $user_id = null;
                            if(auth()->user() != null){
                                $user_id = auth()->user()->id;
                            }
                    $payment_phase = PaymentPhase::create([
                        'amount'           => Number::fix($this->unpayedAmount()),
                        'date'             => $date_time,
                        'payment_id'       => $this->id,
                        'payment_stage_id' => PaymentStage::getInsurancePay()->id,
                        'user_id'          => $user_id,
                    ]);
                    $payment_phase->refresh();
                    $this->refresh();
                    //3 Recorre los financiamientos. Por cada financiamiento activo:
                    $financings = $this->credit->financings;
                    $repayments = new Collection();
                    foreach($financings as $financing){
                        if($financing->isActive()){
                            //4 Si tiene seguro
                            if($financing->have_insurance){
                                //4.1 Envia un mensaje a payCommissionAndDebtorInsurance del wallet del financiamiento
                                $amount_payed = $financing->wallet->payCommissionAndDebtorInsurance($payment_phase,$financing,$date_time);
                                //4.2 Calcula el monto para el Repayment
                                $repayment_amount = ($financing->amount()/$this->credit->amount)*$this->unpayedAmount();
                                //4.3 Genera el repayment
                                if(auth()->user() != null){
                                    $user_id = auth()->user()->id;
                                }else{
                                    $user_id = null;
                                }
                                $repayment = Repayment::create([
                                    'user_id'          => $user_id,
                                    'financing_id'     => $financing->id,
                                    'payment_phase_id' => $payment_phase->id,
                                    'amount'           => Number::fix($repayment_amount - $amount_payed),
                                ]);
                                $repayment->refresh();
                                $financing->refresh();
                                $payment_phase->refresh();
                                //4.4 Agrega el repayment a una coleccion que va a retornar despues.
                                $repayments->add($repayment);
                                //4.5 Envia un mensaje a insurancePayment para que registre la salida de dinero para el pago del seguro
                                if (Wallet::insurancePayment($repayment, $repayment_amount, $date_time) == null) {
                                    echo('Error 9623');
                                    dd();
                                }
                                //4.6 Envia un mensaje a financing, llamando el metodo insuranceReturn
                                if ($financing->insuranceReturn($repayment,$date_time) == null) {
                                    echo('Error 5217855'); 
                                    dd();
                                }
                            }else{//5 Si no tiene seguro
                                //5.1
                                if($financing->lateReturn($date_time) == null){
                                    echo('Error 118520');
                                    dd();
                                    return null;
                                }
                            }
                        }
                    }//foreach
                    return $repayments;
                }
                echo('Error 3029');
                dd();
            }
            echo('Error 025');
            dd();
        }
        echo('Error 545552');
        dd();
        return null;
     }
    /**
     * NOTA: Se pretende que se ejecute diariamente con un cron.
     * Busca los pagos vencidos, determina si se puede cobrar intereses y agrega intereses. Ademas verifica si esta moroso (muy atrasado), o comienza el litigio
     * Como:
     *      1 Verifica si puede aplicar intereses.
     *      2 Verifica si ya paso el dia de gracia..
     *          3 Genera la fase de pago con los intereses
     *          4 Verifica si esta muy vencido (90 dias)
     *             5 Llama al metodo defaulterCredit() de $credit
     *          6 Verifica si esta muy vencido (180 dias)
     *             6 Llama al metodo citation() de $credit
     * @return null
     */
    public function paymentInterests(Carbon $date_time = null){
        if($date_time == null){
            $date_time = Carbon::now();
        }else{
            if (Carbon::now()->diffInSeconds($date_time) > 180) {
                echo($date_time.' $payment->paymentInterests(..)
                ');
            }
        }
        //1
        if($this->nextPaymentStages()->pluck('id')->contains(PaymentStage::getAddInterest()->id)){
            //2
            if($date_time > $this->due_date->add($this->credit->gracePeriod->toDateInterval())){
                //3
                $user_id = null;
                if(auth()->user() != null){
                    $user_id = auth()->user()->id;
                }
                $payment_phase = PaymentPhase::create([
                    'amount'           => Number::fix($this->credit->interest_for_late_payment),
                    'date'             => $date_time,
                    'payment_id'       => $this->id,
                    'payment_stage_id' => PaymentStage::getAddInterest()->id, 
                    'user_id'          => $user_id,
                ]);
                $payment_phase->refresh();
                $this->refresh();
                //4
                if(($date_time->diffInDays($this->due_date) >= 90) && ($this->credit->nextCreditStages()->pluck('id')->contains(CreditStage::getDefaulter()->id))){
                    if($this->credit->defaulterCredit($date_time) == null){
                        echo('Error 8547');
                        dd();
                    }
                }
                if(($date_time->diffInDays($this->due_date) == 180) && ($this->credit->nextCreditStages()->pluck('id')->contains(CreditStage::getJudicialCitation()->id))){
                    if($this->credit->citation($date_time) == null){
                        echo('Error 8105');
                        dd();
                    }
                }
                return $payment_phase;   
            }
        }
        return null;
     }
    /**
     * NOTA Este metodo es llamado desde el metodo pay(..) de esta clase
     * Cambia el estado del pago a Pagado
     * Como:
     *      1 Determina si puede cambiarse el estado
     *      2 Consutla si esta saldado el pago
     *      3 Crea la fase del pago
     *      4 Si es la última cuota, envia un mensaje a crédito tara actualizar el estado
     *      5 Si el pago estaba vencido envia un mensaje para actualizar el estado del credito
     *      6 Calcula la reputacion del usuario
     *@param Carbon $date_time
     *@return PaymentPhase
     */
    public function payed(Carbon $date_time){
        //1 y 2
        if($this->nextPaymentStages()->pluck('id')->contains(PaymentStage::getPayed()->id)){
            if(bcdiv($this->unpayedAmount(),1,$this->credit->currency()->significant_decimals) < $this->credit->currency()->min_value){
                //3
                $user_id = null;
                if(auth()->user() != null){
                    $user_id = auth()->user()->id;
                }
                $payment_phase = PaymentPhase::create([
                'amount'           => Number::fix(0),
                'date'             => $date_time,
                'payment_id'       => $this->id,
                'payment_stage_id' => PaymentStage::getPayed()->id,
                'user_id'          => $user_id,
                ]);
                $this->refresh();
                $this->credit->refresh();
                //4
                if($this->id == $this->credit->payments->last()->id){
                    if($this->credit->liquidate($date_time) == null){
                        echo('Error 104753');
                        dd();
                    }
                }
                //5
                if($this->hasDue()){
                    //verifica si el crédito tiene otro pago vencido
                    if (($this->credit->firstUnpayedPayment($date_time) != null) && (!$this->credit->firstUnpayedPayment($date_time)->hasDue())){
                        //Envia un mensaje para actualizar el estado del crédito a 'Al dia'
                        $this->credit->good($date_time);
                    }
                }
                //6
                if($this->credit->user()->reputation->calculatePerformance($this,$date_time) == null){
                    echo('Error 85120058');
                    dd();
                }
                return $payment_phase;
            }
            echo('Error 7558995');
            dd();
        }
        echo('Error 785187844');
        dd();
        return null;
     }
    /**
     * NOTA: Este metodo estatico sera llamado desde el metodo bad de créditos, unicamente
     * Actualiza el estado del pago y de los financiamientos
     * Como
     *      1 Verifica si el crédito esta en bad
     *      2 Verifica si el pago tiene estado de pagado
     *          2.1 Si es asi, asigna $payment_phase al ultimo paymentPhase
     *      ----
     *      3 En caso contrario, crea el estado a incobrable.
     *      4 Retorna el $payment_phase
     *      
     * @param Carbon $date_time
     * @return PaymentPhase;
     */
    public function bad(Carbon $date_time){
        //1
        if($this->credit->lastCreditPhase()->creditStage->id == CreditStage::getBad()->id){
            //2
            if ($this->lastPaymentPhase()->paymentStage->id == PaymentStage::getPayed()->id) {
                //2.1
                $payment_phase = $this->lastPaymentPhase();
            }else{
                //3
                $user_id = null;
                if(auth()->user() != null){
                    $user_id = auth()->user()->id;
                }
                $payment_phase = PaymentPhase::create([
                    'amount'           => Number::fix(0),
                    'date'             => $date_time,
                    'payment_id'       => $this->id,
                    'payment_stage_id' => PaymentStage::getBad()->id,
                    'user_id'          => $user_id,
                ]);
            }
            //4
            return $payment_phase;
        }
        echo('Error 14478555, el crédito no esta en bad '.$this->credit->lastCreditPhase()->creditStage->name);
        dd();
        return null;
     }
}
