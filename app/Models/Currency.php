<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use Carbon\Carbon;

class Currency extends Model
{
    protected $table = 'currencies';
    protected $fillable = [
        'name'                    ,
        'iso_code'                ,
        'simbol'                  ,
        'significant_decimals'    , 
        'min_value'               ,
        'decimal_point'           ,
        'thousands_separator'     ,
        'max_inflation_percentage',
        'max_deflation_percentage',
        'user_id'                 ,//nullable. Usado para la auditoria
    ];
    public $timestamps = false;
    public function paymentPlans(){
        return $this->hasMany('App\Models\PaymentPlan');
    }
    public function countries(){
        return $this->hasMany('App\Models\Country');
    }
    public function wallets(){
        return $this->hasMany('App\Models\Wallet');
    }
    public function maxAmountCredits(){
        return $this->hasMany('App\Models\MaxAmountCredit');
    }
    public function formExchangeRates(){
        return $this->hasMany('App\Models\ExchangeRate', 'from_currency_id', 'id');
    }
    public function toExchangeRates(){
        return $this->hasMany('App\Models\ExchangeRate', 'to_currency_id', 'id');
    }
    public function credits(){
        $return = new Collection();
        $wallets = $this->wallets;
        foreach($wallets as $wallet)
            if(count($wallet->credits) > 0){
                $credits = $wallet->credits;
                foreach($credits as $credit)
                    $return->add($credit);
             }
        return $return;
     }
    public function creditsToApprove(){
        $credits = $this->credits();
        $items = new Collection();
        foreach ($credits as $credit) {
            if ($credit->lastCreditPhase()->creditStage->id == CreditStage::getFirst()->id) {
                $items->add($credit);
            }
        }
        return $items;
    }
    public function creditsLawsuit(){
        $credits = $this->credits();
        $items = new Collection();
        foreach ($credits as $credit) {
            if ($credit->lastCreditPhase()->creditStage->id == CreditStage::getLawsuit()->id) {
                $items->add($credit);
            }
        }
        return $items;
    }
    public function creditsLatePayment(){
        $credits = $this->credits();
        $items = new Collection();
        foreach ($credits as $credit) {
            if ($credit->lastCreditPhase()->creditStage->id == CreditStage::getPaymentLate()->id) {
                $items->add($credit);
            }
        }
        return $items;
    }
    public function creditsDefaulter(){
        $credits = $this->credits();
        $items = new Collection();
        foreach ($credits as $credit) {
            if ($credit->lastCreditPhase()->creditStage->id == CreditStage::getDefaulter()->id) {
                $items->add($credit);
            }
        }
        return $items;
    }
    public function creditsJudicialCitation(){
        $credits = $this->credits();
        $items = new Collection();
        foreach ($credits as $credit) {
            if ($credit->lastCreditPhase()->creditStage->id == CreditStage::getJudicialCitation()->id) {
                $items->add($credit);
            }
        }
        return $items;
    }
    public function creditsBad(){
        $credits = $this->credits();
        $items = new Collection();
        foreach ($credits as $credit) {
            if ($credit->lastCreditPhase()->creditStage->id == CreditStage::getBad()->id) {
                $items->add($credit);
            }
        }
        return $items;
    }
    public function financings(){
        $financings = new Collection();
        foreach(Financing::cursor() as $financing){
            if ($financing->wallet->currency->id == $this->id) {
                $financings->add($financing);
            }
        }
        return $financings;
     }
    public function creditMargin(Carbon $date_time = null){
        if($date_time == null){
            $date_time = Carbon::now();
        }else{
            if (Carbon::now()->diffInSeconds($date_time) > 180) {
                echo($date_time.' $currency->creditMargin(..)
                ');
            }
        }
        $user = auth()->user();
        if ($user != null) {
            return ExchangeRate::localConvert('USD', $this->iso_code, $user->userMargin($date_time));
        }
     }
    public function creditMinAmount(){
        return $this->truncate(ExchangeRate::localConvert('USD',$this->iso_code,5));
    }
    public function financingRemaining($amount_remaining){
        $last_100_financings = $this->financings()->reverse()->take(100);
        if($last_100_financings->count() > 0) {
            $financed_amount = 0;
            foreach ($last_100_financings as $financing) {
                $financed_amount += $financing->amount();
            }
            $seconds = $last_100_financings->first()->financingPhases->first()->date->diffInSeconds($last_100_financings->last()->financingPhases->last()->date);
            $seconds_remaining = ($amount_remaining * $seconds) / $financed_amount;
            return (int) $seconds_remaining;
        }else{
            return null;
        }
    }
    public function financingCredits(){
        $credits = $this->credits();
        $return = new Collection();
        foreach($credits as $credit)
            if($credit->lastCreditPhase()->creditStage->id == CreditStage::getFinancing()->id)
                $return->add($credit);
            return $return;
    }
    public function truncate($number, $dec = -1000){
        Number::fix($number);
        if ($number == 0)
            return 0;
        if ($dec == -1000){
            $dec = Number::decimalCount($number);
            if ($this->significant_decimals < $dec){
                $dec = $this->significant_decimals;
            }
        }
        $dec = abs($dec);    
        while ((abs($number) < 1/pow(10,$dec)) && ($dec < 12)) {
            $dec++;
        }
        return bcdiv($number,1,$dec);
    }
    public function format($number, $dec = -1000){
        Number::fix($number);
        if ($number == 0)
            return 0;
        if ($dec == -1000){
            $dec = $this->significant_decimals;
        }
        $dec = abs($dec);
        while ((abs($number) < 1/pow(10,$dec)) &&($dec < 12)) {
            $dec++;
        }
        return number_format($number, $dec, $this->decimal_point, $this->thousands_separator);
    }
    public function numberToString($number, $truncate = 1){
        while(substr_count($number,'.') > 1){
            $number = substr_replace($number,'',strrpos($number, '.'), strlen('.'));
        }
        $original_number = $number;
        if(!is_numeric($number)){
            return $number;
        }
        $decimals = '';
        if(substr_count($number,'.') == 1){
            $decimals = substr($number,strrpos($number, '.')+1);
            $number = substr_replace($number,'',strrpos($number, '.')+1);
            if($truncate > 0){
                $decimals = substr_replace($decimals,'',$this->significant_decimals);
            }else{
                if(strlen($decimals) > $this->significant_decimals){
                    $dec = substr_replace($decimals,'',$this->significant_decimals-1);
                    if($decimals[$this->significant_decimals] >= 5){
                        $dec .= ($decimals[$this->significant_decimals-1]+1);
                    }else{
                        $dec .= $decimals[$this->significant_decimals-1];
                    }
                    $decimals = $dec;
                }
            }
        }
        $num = $number;
        if (substr($number, -1) == '.'){
            $num = str_replace('.','',$num);
        }
        if(strlen($decimals) > 0){
            $num .= '.'.$decimals;
        }
        if (substr($original_number, -1) == '.'){
            return $this->simbol.' '.number_format($num, strlen($decimals), $this->decimal_point, $this->thousands_separator).$this->decimal_point; 
        }else{
            return $this->simbol.' '.number_format($num, strlen($decimals), $this->decimal_point, $this->thousands_separator); 
        }
    }
    public function stringToNumber($number){
        if($number[strlen($number)-1] == '.'){
            $number[strlen($number)-1] = $this->decimal_point;
        }
        while(substr_count($number,$this->decimal_point) > 1){
            $number = substr_replace($number,'',strrpos($number, $this->decimal_point), strlen($this->decimal_point));
        }
        $decimals = '';
        if(substr_count($number,$this->decimal_point) == 1){
            $decimals = substr($number,strrpos($number, $this->decimal_point)+1);
            $number = substr_replace($number,'',strrpos($number, $this->decimal_point)+1);
            $decimals = substr_replace($decimals,'',$this->significant_decimals);
        }
        $return = str_replace($this->simbol,'',$number);//quita el simbolo
        $return = str_replace(' ','',$return);//quita espacios en blanco
        $return = str_replace($this->thousands_separator,'',$return);//Quita el separador de miles
        $return = str_replace($this->decimal_point,'.',$return);//Cambia el separador de decimales
        $return .= $decimals;
        return $return;
    }
    public function percentage($number, $dec = -1000){
        $number *= 100;
        $number = $this->format($number, $dec);
        return $number." %";
    }
    public function isInflationBig($old_one_usd_in_currency){
        return (($old_one_usd_in_currency * ($this->max_inflation_percentage + 1)) < ExchangeRate::localConvert('USD',$this->iso_code, 1));
    }
    public function isDeflationBig($old_one_usd_in_currency){
        return (($old_one_usd_in_currency * ( 1 - $this->max_deflation_percentage)) > ExchangeRate::localConvert('USD',$this->iso_code, 1));
    }
    /**
     * Devuelve una currency por el codigo ISO
     * @param string $iso_code
     * @return Currency
     */
    public static function getCurrency($iso_code){
        return Currency::where('iso_code',strtoupper($iso_code))->first();
    }
}
