<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;

class MarginModifier extends Model
{
    protected $table = 'margin_modifiers';
    protected $fillable= [
        'reputation_id'           ,
        'margin_modifier_stage_id',
        'date'                    ,
        'amount'                  ,
        'user_id'                 ,//nullable. Usado para la auditoria
    ];
    protected $dates = [
        'date',
    ];
    public $timestamps = false;
    public function reputation(){
        return $this->belongsTo('App\Models\Reputation');
    }
    public function marginModifierStage(){
        return $this->belongsTo('App\Models\MarginModifierStage');
    }
    public static function calculatePerformance(Payment $payment, Carbon $date_time){
        //Verifica si el pago existe y su sy última etapa es 'Pagada'
        if ( ($payment == null) || ($payment->lastPaymentPhase()->paymentStage->id != PaymentStage::getPayed()->id) ){
            echo('Error 34815554 ');
            dd();
            return null;
        }
        //Obtiene datos necesarios y los almacena en variables.
        $reputation = $payment->credit->user()->reputation;
        $currency = $payment->credit->currency();
        $due_date = $payment->due_date;
        $payed_date = $payment->lastPaymentPhase()->date;
        $diff_in_days = $due_date->diffInDays($payed_date);
        //Verifica si se paga en el dia o antes
        if(($diff_in_days == 0) || ($payed_date < $due_date)){
            //USD35 * 365dias * 0,00391variacion = -50USD
            $modifier = ($diff_in_days + 1) * ExchangeRate::localConvert($currency->iso_code,'USD',$payment->amount()) * 0.00391;

            $new_amount = $reputation->marginUSD() + $modifier;

            if($new_amount > $reputation->maxAmountCredit->maxLimitUSD()){
                $modifier = $reputation->maxAmountCredit->maxLimitUSD() - $reputation->marginUSDWithoutBaseAmountCall();
                if(abs($modifier) <= Currency::getCurrency('usd')->min_value){
                    return $reputation->marginModifiers->last();
                }
            }
            if ($reputation->retainedMargin == null) {
                return MarginModifier::performanceIncrease($reputation,Number::fix($modifier),$date_time);
            }else{
                return $reputation->marginModifiers->last();
            }
        }elseif($payed_date > $due_date){//Si no, verifica si se paga despues del vencimiento
            //USD35 * 365dias * 0,00939variacion = 120USD
            $modifier = $diff_in_days * ExchangeRate::localConvert($currency->iso_code,'USD',$payment->amount()) * 0.00939;
            $modifier = $modifier * -1;

            return MarginModifier::performanceDecrease($reputation,Number::fix($modifier),$date_time);
        }
    }
    /*
        ::getBaseAmountEstablished()---------> ::establishBaseAmount($reputation,$max_amount_credit)
        ::getBaseAmountDeleted()-------------> ::deleteBaseAmount($reputation,$max_amount_credit)
        ::getRetentionForCredit()------------> ::retentMargin($reputation, $amount_usd)
        ::getLiberationForCredit()-----------> ::liberateMargin($credit)
        ::getRetentionForDelayedPayment()----> ::retentForDelayedPayment($reputation)
        ::getLiberationForDelayedPayment()---> ::liberateForDelayedPayment($reputation)
        ::getDecreasePerformanceInPayments()-> ::performanceDecrease($reputation, $amount_usd)
        ::getIncreasePerformanceInPayments()-> ::performanceIncrease($reputation, $amount_usd)
        ::getDecreaseForInflation()----------> ::decreaseForInflation($reputation, $amount_usd)
        ::getIncreaseForDeflation()----------> ::increaseForDeflation($reputation, $amount_usd)
        ::getManualIncrease()----------------> ::manualIncrease($reputation, $amount_usd)
    */
    /**
     * NOTA este metodo debe llamado desde el metodo baseAmount() de Reputation
     * Crea una instancia de MarginModifier para establecer el limite inicial de un margen
     * Como
     *      1 Verifica que $reputation y $max_amount_credit no sean nulos.
     *      2 Verifica que $max_amount_credit->id == $reputation->maxAmountCredit->id
     *      3 crea el MarginModifier
     * @param Reputation $reputation
     * @param MaxAmountCredit $max_amount_credit
     * @param Carbon $date_time
     * @return MarginModifier
     */
    public static function establishBaseAmount(Reputation $reputation,MaxAmountCredit $max_amount_credit, Carbon $date_time){ 
        //1 y 2
        if (($reputation == null) || ($max_amount_credit == null) || ($max_amount_credit->id != $reputation->maxAmountCredit->id)) {
            return null;
        }
        //3
        if(auth()->user() != null){
            $user_id = auth()->user()->id;
        }else{
            $user_id = null;
        } 
        $margin_modifier = MarginModifier::create([
            'user_id'                  => $user_id,//Usado para auditoria
            'reputation_id'            => $reputation->id,
            'margin_modifier_stage_id' => MarginModifierStage::getBaseAmountEstablished()->id,
            'date'                     => $date_time,
            'amount'                   => Number::fix($max_amount_credit->initialLimitUSD()),
        ]);
        $margin_modifier->refresh();
        $reputation->refresh();
        return $margin_modifier;
     }
    /**
     * NOTA este metodo debe llamado desde el metodo deleteBaseAmount() de Reputation
     * Crea una instancia de MarginModifier para eliminar el limite inicial de un margen
     * Como
     *      1 Verifica que $reputation y $max_amount_credit no sean nulos.
     *      2 Verifica que $max_amount_credit->id == $reputation->maxAmountCredit->id
     *      3 crea el MarginModifier
     * @param Reputation $reputation
     * @param MaxAmountCredit $max_amount_credit
     * @param Carbon $date_time
     * @return MarginModifier
     */
    public static function deleteBaseAmount(Reputation $reputation,MaxAmountCredit $max_amount_credit, Carbon $date_time){
        //1 y 2
        if (($reputation == null) || ($max_amount_credit == null) || ($max_amount_credit->id != $reputation->maxAmountCredit->id)) {
            return null;
        }
        //3
        if(auth()->user() != null){
            $user_id = auth()->user()->id;
        }else{
            $user_id = null;
        }   
        $margin_modifier = MarginModifier::create([
            'user_id'                  => $user_id,//Usado para auditoria
            'reputation_id'            => $reputation->id,
            'margin_modifier_stage_id' => MarginModifierStage::getBaseAmountDeleted()->id,
            'date'                     => $date_time,
            'amount'                   => Number::fix(($max_amount_credit->initialLimitUSD() * -1)),
        ]);
        $margin_modifier->refresh();
        $reputation->refresh();
        return $margin_modifier;
     }
    /**
     * NOTA Este metdo solamente debe ser llamado por el metodo retent() de Reputation
     * Retiene margen ocupado por la solicitud de un credito
     * Como
     *      1 Verifica que $reputation y $amount_usd no sean nulos.
     *      2 crea el MarginModifier
     * @param Reputation $reputation
     * @param double $amount_usd
     * @param Carbon $date_time
     * @return MarginModifier
     */
    public static function retentMargin(Reputation $reputation, $amount_usd, Carbon $date_time){
        //1
        if (($reputation != null) && ($amount_usd != 0)) {
            if ($amount_usd > 0)
                $amount_usd *= -1;
            //2
            if(auth()->user() != null){
                $user_id = auth()->user()->id;
            }else{
                $user_id = null;
            }
            $new_margin = $amount_usd + $reputation->marginUSDWithoutBaseAmountCall();
            if($new_margin > $reputation->maxAmountCredit->maxLimitUSD()){
                echo('Error 558601444 '.$amount_usd.' + '.$reputation->marginUSDWithoutBaseAmountCall().' >  '.$reputation->maxAmountCredit->maxLimitUSD());
                dd();
            }
            $margin_modifier = MarginModifier::create([
                'user_id'                  => $user_id,//Usado para auditoria
                'reputation_id'            => $reputation->id,
                'margin_modifier_stage_id' => MarginModifierStage::getRetentionForCredit()->id,
                'date'                     => $date_time,
                'amount'                   => Number::fix($amount_usd),
            ]);
            $margin_modifier->refresh();
            $reputation->refresh();
            return $margin_modifier;
        }
            return null;
     }
    /**
     * NOTA Este metdo solamente debe ser llamado por el metodo liberate() de Reputation
     * Libera margen ocupado por la solicitud de un credito
     * Como
     *      1 Verifica que $reputation y $amount_usd no sean nulos.
     *      2 crea el MarginModifier
     * @param Reputation $reputation
     * @param double $amount_usd
     * @param Carbon $date_time
     * @return MarginModifier
     */
    public static function liberateMargin(Reputation $reputation, $amount_usd, Carbon $date_time){
        //1
        if (($reputation != null) && ($amount_usd != 0)) {
            if ($amount_usd < 0)
                $amount_usd *= -1;
            //2
            if(auth()->user() != null){
                $user_id = auth()->user()->id;
            }else{
                $user_id = null;
            }
            $new_margin = $amount_usd + $reputation->marginUSDWithoutBaseAmountCall();
            if($new_margin > $reputation->maxAmountCredit->maxLimitUSD()){
                echo('Error 026656 '.$amount_usd.' + '.$reputation->marginUSDWithoutBaseAmountCall().' >  '.$reputation->maxAmountCredit->maxLimitUSD());
                dd();
            }
            $margin_modifier = MarginModifier::create([
                'user_id'                  => $user_id,//Usado para auditoria
                'reputation_id'            => $reputation->id,
                'margin_modifier_stage_id' => MarginModifierStage::getLiberationForCredit()->id,
                'date'                     => $date_time,
                'amount'                   => Number::fix($amount_usd),
            ]);
            $margin_modifier->refresh();
            $reputation->refresh();
            return $margin_modifier;
        }
            return null;
     }
    /**
     * NOTA este metodo es llamado solamente desde el metodo slowPayer() de Reputation
     * Crea el MarginModifier que disminuye el margen por pagos atrasados
     * Como
     *     1 Verifica que $reputation y $amount_usd no sean nulos.
     *     2 crea el MarginModifier
     * @param Reputation $reputation
     * @param double $amount_usd
     * @param Carbon $date_time
     * @return MarginModifier
     */
    public static function retentForDelayedPayment(Reputation $reputation, $amount_usd, Carbon $date_time){
        //1
        if (($reputation != null) && ($amount_usd != 0)) {
            if ($amount_usd > 0){
                $amount_usd *= -1;
            }
            //2
            if(auth()->user() != null){
                $user_id = auth()->user()->id;
            }else{
                $user_id = null;
            }
            $new_margin = $amount_usd + $reputation->marginUSDWithoutBaseAmountCall();
            if($new_margin > $reputation->maxAmountCredit->maxLimitUSD()){
                echo('Error 325661488 '.$amount_usd.' + '.$reputation->marginUSDWithoutBaseAmountCall().' >  '.$reputation->maxAmountCredit->maxLimitUSD());
                dd();
            }
            if ($amount_usd > 0){
                echo('Error 24245110562515');
            }
            $margin_modifier = MarginModifier::create([
                'user_id'                  => $user_id,//Usado para auditoria
                'reputation_id'            => $reputation->id,
                'margin_modifier_stage_id' => MarginModifierStage::getRetentionForDelayedPayment()->id,
                'date'                     => $date_time,
                'amount'                   => Number::fix($amount_usd),
            ]);
            $margin_modifier->refresh();
            $reputation->refresh();
            return $margin_modifier;
        }
        return null;
     }
    /**
     * NOTA este metodo es llamado solamente desde el metodo slowPayer() de Reputation
     * Crea el MarginModifier que devuelve el margen antes retenido por pagos atrasados
     * Como
     *     1 Verifica que $reputation y $amount_usd no sean nulos.
     *     2 crea el MarginModifier
     * @param Reputation $reputation
     * @param double $amount_usd
     * @param Carbon $date_time
     * @return MarginModifier
     */
    public static function liberateForDelayedPayment(Reputation $reputation, $amount_usd, Carbon $date_time){
        //1
        if (($reputation != null) && ($amount_usd != 0)) {
            if ($amount_usd < 0){
                $amount_usd *= -1;
            }
            //2
            if(auth()->user() != null){
                $user_id = auth()->user()->id;
            }else{
                $user_id = null;
            }
            $new_margin = $amount_usd + $reputation->marginUSDWithoutBaseAmountCall();
            if($new_margin > $reputation->maxAmountCredit->maxLimitUSD()){
                echo('Error 123363248622 '.$amount_usd.' + '.$reputation->marginUSDWithoutBaseAmountCall().' >  '.$reputation->maxAmountCredit->maxLimitUSD());
                dd();
            }
            $margin_modifier = MarginModifier::create([
                'user_id'                  => $user_id,//Usado para auditoria
                'reputation_id'            => $reputation->id,
                'margin_modifier_stage_id' => MarginModifierStage::getLiberationForDelayedPayment()->id,
                'date'                     => $date_time,
                'amount'                   => Number::fix($amount_usd),
            ]);
            $margin_modifier->refresh();
            $reputation->refresh();
            return $margin_modifier;
        }
        return null;
     }
    /**
     * NOTA: Este metodo debe ser llamado solamente por el metodocalculatePerformance de $this
     * Crea un marginModifier negativo para que decremente la cuenta del usuario
     * Como
     *     1 Verifica que $reputation y $amount_usd no sean nulos.
     *     2 Si $amount_usd es positico, lo pasa a negativo
     *     3 crea el MarginModifier
     * @param Reputation $reputation
     * @param double $amount_usd
     * @param Carbon $date_time
     * @return MarginModifier
     */
    public static function performanceDecrease(Reputation $reputation, $amount_usd, Carbon $date_time){
        //1
        if (($reputation != null) && ($amount_usd != 0)) {
            //2
            if($amount_usd > 0)
                $amount_usd = $amount_usd * -1;
            //3
            if(auth()->user() != null){
                $user_id = auth()->user()->id;
            }else{
                $user_id = null;
            }
            $new_margin = $amount_usd + $reputation->marginUSDWithoutBaseAmountCall();
            if($new_margin > $reputation->maxAmountCredit->maxLimitUSD()){
                echo('Error 31485211 '.$amount_usd.' + '.$reputation->marginUSDWithoutBaseAmountCall().' >  '.$reputation->maxAmountCredit->maxLimitUSD());
                dd();
            }
            $margin_modifier = MarginModifier::create([
                'user_id'                  => $user_id,//Usado para auditoria
                'reputation_id'            => $reputation->id,
                'margin_modifier_stage_id' => MarginModifierStage::getDecreasePerformanceInPayments()->id,
                'date'                     => $date_time,
                'amount'                   => Number::fix($amount_usd),
            ]);
            $margin_modifier->refresh();
            $reputation->refresh();
            return $margin_modifier;
        }
        return null;
     }
    /**
     * NOTA: Este metodo debe ser llamado solamente por el metodocalculatePerformance de $this
     * Crea un marginModifier para que incremente la cuenta del usuario
     * Como
     *     1 Verifica que $reputation y $amount_usd no sean nulos.
     *     2 Si $amount_usd es negativo, lo pasa a positivo
     *     3 crea el MarginModifier
     * @param Reputation $reputation
     * @param double $amount_usd
     * @param Carbon $date_time
     * @return MarginModifier
     */
    public static function performanceIncrease(Reputation $reputation, $amount_usd, Carbon $date_time){
        //1
        if (($reputation != null) && ($amount_usd != 0)) {
            if($amount_usd < 0)
                $amount_usd = $amount_usd * -1;
            //2
            if(auth()->user() != null){
                $user_id = auth()->user()->id;
            }else{
                $user_id = null;
            }
            $new_margin = $amount_usd + $reputation->marginUSDWithoutBaseAmountCall();
            if($new_margin > $reputation->maxAmountCredit->maxLimitUSD()){
                echo('Error 3114477885 '.$amount_usd.' + '.$reputation->marginUSDWithoutBaseAmountCall().' >  '.$reputation->maxAmountCredit->maxLimitUSD());
                dd();
            }
            $margin_modifier = MarginModifier::create([
                'user_id'                  => $user_id,//Usado para auditoria
                'reputation_id'            => $reputation->id,
                'margin_modifier_stage_id' => MarginModifierStage::getIncreasePerformanceInPayments()->id,
                'date'                     => $date_time,
                'amount'                   => Number::fix($amount_usd),
            ]);
            $margin_modifier->refresh();
            $reputation->refresh();
            return $margin_modifier;
        }
        return null;
     }
    /**
     * NOTA Este metodo se debe llamar desde el metodo ajustMargin() y baseAmount() (el ultimo, en caso de que cambie de max_amountCredit, por la divisa y esas cosas)
     * Crea una instancia de MarginModifier que decrementa el margen de usuario
     * Como
     *      1 Verifica si el monto es mayor a cero y la reputacio no es nula
     *      2 Crea el margin modifier
     * @param Reputation $reputation
     * @param double $amount_usd
     * @param Carbon $date_time
     * @return MarginModifier
     */
    public static function decreaseForInflation(Reputation $reputation, $amount_usd, Carbon $date_time){
        //1
        if (($reputation != null) && ($amount_usd != 0)) {
            if ($amount_usd > 0)
                $amount_usd *= -1;
            //2
            if(auth()->user() != null){
                $user_id = auth()->user()->id;
            }else{
                $user_id = null;
            }
            $new_margin = $amount_usd + $reputation->marginUSDWithoutBaseAmountCall();
            if($new_margin > $reputation->maxAmountCredit->maxLimitUSD()){
                echo('Error 100223245 '.$amount_usd.' + '.$reputation->marginUSDWithoutBaseAmountCall().' >  '.$reputation->maxAmountCredit->maxLimitUSD());
                dd();
            }
            $margin_modifier = MarginModifier::create([
                'user_id'                  => $user_id,//Usado para auditoria
                'reputation_id'            => $reputation->id,
                'margin_modifier_stage_id' => MarginModifierStage::getDecreaseForInflation()->id,
                'date'                     => $date_time,
                'amount'                   => Number::fix($amount_usd),
            ]);
            $reputation->refresh();
            return $margin_modifier;
        }
        return null;
     }
    /**
     * NOTA Este metodo se debe llamar desde el metodo ajustMargin() y baseAmount() (el ultimo, en caso de que cambie de max_amountCredit, por la divisa y esas cosas)
     * Crea una instancia de MarginModifier que incrementa el margen de usuario
     * Como
     *      1 Verifica si el monto es mayor a cero y la reputacio no es nula
     *      2 Crea el margin modifier
     * @param Reputation $reputation
     * @param double $amount_usd
     * @param Carbon $date_time
     * @return MarginModifier
     */
    public static function increaseForDeflation(Reputation $reputation, $amount_usd, Carbon $date_time){
        //1
        if (($reputation != null) && ($amount_usd != 0)) {
            if ($amount_usd < 0)
                $amount_usd *= -1;
            //2
            if(auth()->user() != null){
                $user_id = auth()->user()->id;
            }else{
                $user_id = null;
            }
            $new_margin = $amount_usd + $reputation->marginUSDWithoutBaseAmountCall();
            if($new_margin > $reputation->maxAmountCredit->maxLimitUSD()){
                echo('Error 01214452112 '.$amount_usd.' + '.$reputation->marginUSDWithoutBaseAmountCall().' >  '.$reputation->maxAmountCredit->maxLimitUSD());
                dd();
            }
            $margin_modifier = MarginModifier::create([
                'user_id'                  => $user_id,//Usado para auditoria
                'reputation_id'            => $reputation->id,
                'margin_modifier_stage_id' => MarginModifierStage::getIncreaseForDeflation()->id,
                'date'                     => $date_time,
                'amount'                   => Number::fix($amount_usd),
            ]);
            $margin_modifier->refresh();
            $reputation->refresh();
            return $margin_modifier;
        }
        return null;
     }
    /**
     * Incrementa manualmente el margen del usuario (en el caso de solicitud o algo asi)
     * Como
     *      1 Verifica si el monto es mayor a cero y la reputacio no es nula
     *      2 Crea el margin modifier
     * @param Reputation $reputation
     * @param double $amount_usd
     * @param Carbon $date_time
     * @return MarginModifier
     */
    public static function manualIncrease(Reputation $reputation, $amount_usd, Carbon $date_time){
        //1
        if (($reputation != null) && ($amount_usd != 0)) {
            if($amount_usd < 0)
                $amount_usd = $amount_usd * -1;
            //2
            if(auth()->user() != null){
                $user_id = auth()->user()->id;
            }else{
                $user_id = null;
            }
            $new_margin = $amount_usd + $reputation->marginUSDWithoutBaseAmountCall();
            if($new_margin > $reputation->maxAmountCredit->maxLimitUSD()){
                echo('Error 115224555 '.$amount_usd.' + '.$reputation->marginUSDWithoutBaseAmountCall().' >  '.$reputation->maxAmountCredit->maxLimitUSD());
                dd();
            }
            $margin_modifier = MarginModifier::create([
                'user_id'                  => $user_id,//Usado para auditoria
                'reputation_id'            => $reputation->id,
                'margin_modifier_stage_id' => MarginModifierStage::getManualIncrease()->id,
                'date'                     => $date_time,
                'amount'                   => Number::fix($amount_usd),
            ]);
            $margin_modifier->refresh();
            $reputation->refresh();
            return $margin_modifier;
        }
        return null;
     }
} 
