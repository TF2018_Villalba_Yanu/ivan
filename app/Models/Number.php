<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Number extends Model
{
    public static function fix($number){
        return round($number,10);
    }
    public static function decimalCount($number) {
        $num = 0;
        while (true) {
            if ((string)$number === (string)round($number)) {
                break;
            }
            if (is_infinite($number)) {
                break;
            }
    
            $number *= 10;
            $num++;
            if($num == 20){
                break;
            }
        }
        return $num;
    }
    
}
