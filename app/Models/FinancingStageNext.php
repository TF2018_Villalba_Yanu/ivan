<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FinancingStageNext extends Model
{
    protected $table = 'financing_stage_nexts';
    protected $fillable= [
        'financing_stage_id'     ,
        'financing_stage_next_id',
    ];
    public $timestamps = false;
    public function financingStage()
    {
        return $this->belongsTo('App\Models\FinancingStage');
    }
    public function financingStageNext()
    {
        return $this->belongsTo('App\Models\FinancingStage', 'financing_stage_next_id', 'id');
    }
}
