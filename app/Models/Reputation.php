<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;

class Reputation extends Model
{
    protected $table = 'reputations';
    protected $fillable = [
        'retained_margin_id'         ,//can be null
        'max_amount_credit_id'       ,//can be null
        'default_currency_id'        ,//can be null
        'one_usd_in_default_currency',//can be null
    ];
    public $timestamps = false;
    public function retainedMargin(){
        return $this->belongsTo('App\Models\MarginModifier', 'retained_margin_id', 'id');
     }
    public function maxAmountCredit(){
         return $this->belongsTo('App\Models\MaxAmountCredit');
      }
    public function defaultCurrency()
     {
         return $this->belongsTo('App\Models\Currency','default_currency_id', 'id');
      }
    public function interestModifiers()
     {
         return $this->hasMany('App\Models\InterestModifier');
      }
    public function insuranceModifiers(){
         return $this->hasMany('App\Models\InsuranceModifier');
      }
    public function marginModifiers(){
         return $this->hasMany('App\Models\MarginModifier');
      }
    public function user(){
         $user = User::where('reputation_id', $this->id)
             ->first();
         return $user;
      }

    public function activeCreditsLimit(Carbon $date_time = null){
        if($date_time == null){
            $date_time = Carbon::now();
        }else{
            if (Carbon::now()->diffInSeconds($date_time) > 180) {
                echo($date_time.' $reputation->activeCreditsLimit(..)
                ');
            }
        }
        $this->baseAmount($date_time);
        return $this->maxAmountCredit->active_credits_limit;
     }
    public function marginUSDWithoutBaseAmountCall(){
        $margin_modifiers = $this->marginModifiers;
        $modifier = 0;
        foreach ($margin_modifiers as $margin_modifier){
            $modifier += $margin_modifier->amount;
        }
        return $modifier;
     }
    public function marginUSD(Carbon $date_time = null){
        if($date_time == null){
            $date_time = Carbon::now();
        }else{
            if (Carbon::now()->diffInSeconds($date_time) > 180) {
                echo($date_time.' $reputation->marginUSD(..)
                ');
            }
        }
        $this->baseAmount($date_time);
        $margin = $this->marginUSDWithoutBaseAmountCall();
        
        if($margin > $this->maxAmountCredit->maxLimitUSD()){
            $decrease = $margin - $this->maxAmountCredit->maxLimitUSD();
            MarginModifier::decreaseForInflation($this,$decrease,$date_time);
            $margin = $this->marginUSDWithoutBaseAmountCall();
        }
        return $margin;
     }
    public function marginCurrency(Carbon $date_time = null){
        if($date_time == null){
            $date_time = Carbon::now();
        }else{
            if (Carbon::now()->diffInSeconds($date_time) > 180) {
                echo($date_time.' $reputation->marginCurrency(..)
                ');
            }
        }
        $amount_currency = ExchangeRate::localConvert('USD',$this->defaultCurrency->iso_code,$this->marginUSD($date_time));
        if($amount_currency > $this->maxAmountCredit->max_limit){
            echo('Error 62257');
            dd();
        }
        return $amount_currency;
     }
    /**
     * Este metodo se encarga de establecer un ranking dependiendo de la forma en la que el usuario paga las cuotas.
     * Como dunciona:
     *  1 Se obtienen los modificadores de interes del usuario.
     *  2 Se discriminan los modificadores negativos (buenos) y los positivos (malo) y se suma el total
     *  3 Si el total de ambos modificadores da 0, se retorna "A".
     *  4 Si los modificadores negativos da 0, se retorna "D".
     *  5 Se calcula el porcentaje de los modificadores positivos con respecto a los negativos.
     *  6 Si el porcentaje se encuentra entre 0 y 5, retorna AAA.
     *  7 Si el porcentaje se encuentra entre 5 y 15, retorna AA.
     *  8 Si el porcentaje se encuentra entre 15 y 35, retorna A.
     *  9 Si el porcentaje se encuentra entre 35 y 60, retorna B.
     *  10  Si el porcentaje se encuentra entre 60 y 85, retorna C.
     *  11  Si el porcentaje es mayor a 85, retorna D.
     */
    public function rank(){
        //1 Se obtienen los modificadores de interes del usuario.
        $modifiers = InterestModifier::where('reputation_id',$this->id)->get();
        //2 Se discriminan los modificadores negativos (buenos) y los positivos (malo) y se suma el total
        $good = 0;
        $bad  = 0;
        foreach($modifiers as $modifier){
            if($modifier->percentage >= 0){
                $bad += $modifier->percentage;
            }else{
                $good  += abs($modifier->percentage);
            }
        }
        //3 Si el total de ambos modificadores da 0, se retorna "A".
        if(($good + $bad) == 0){
            return "A";
        }
        //4 Si los modificadores negativos da 0, se retorna "D".
        if($good == 0){
            return "D";
        }
        //5 Se calcula el porcentaje de los modificadores positivos con respecto a los negativos.
        $bad_percentage = ($bad * 100)/$good;
        //6 Si el porcentaje se encuentra entre 0 y 5, retorna AAA.
        if(($bad_percentage >= 0) && ($bad_percentage < 5)){
            return "AAA";
        }
        //7 Si el porcentaje se encuentra entre 5 y 15, retorna AA.
        if(($bad_percentage >= 5) && ($bad_percentage < 15)){
            return "AA";
        }
        //8 Si el porcentaje se encuentra entre 15 y 35, retorna A.
        if(($bad_percentage >= 10) && ($bad_percentage < 35)){
            return "A";
        }
        //9 Si el porcentaje se encuentra entre 35 y 60, retorna B.
        if(($bad_percentage >= 35) && ($bad_percentage < 60)){
            return "B";
        }
        //10  Si el porcentaje se encuentra entre 60 y 85, retorna C.
        if(($bad_percentage >= 60) && ($bad_percentage < 85)){
            return "C";
        }
        //11  Si el porcentaje es mayor a 85, retorna D.
        if($bad_percentage >= 85){
            return "D";
        }
        echo('Error 852155217525');
        dd();
    }
    /*
        ::establishBaseAmount($reputation,$max_amount_credit)--> baseAmount()
        ::deleteBaseAmount($reputation,$max_amount_credit)-----> deleteBaseAmount()
        ::retentMargin($reputation, $amount_usd)---------------> retent($credit)
        ::liberateMargin($credit)------------------------------> liberate($credit)
        ::retentForDelayedPayment($reputation)-----------------> slowPayer()
        ::liberateForDelayedPayment($reputation)---------------> slowPayer()
        ::performanceDecrease($reputation, $amount_usd)--------> calculatePerformance($payment)
        ::performanceIncrease($reputation, $amount_usd)--------> calculatePerformance($payment)
        ::decreaseForInflation($reputation, $amount_usd)-------> adjustMargin() //VERIFICAR
        ::increaseForDeflation($reputation, $amount_usd)-------> adjustMargin() //VERIFICAR
        ::manualIncrease($reputation, $amount_usd)-------------> manualIncrease($amount_usd) //VERIFICAR
     */
    /**
     * Establece o reestablece un monto base para una reputacion
     * Como
     *      1 Obtiene el usuario del credito
     *      2 Busca el MaxAmount Credit del usuario obtenido
     *      3 Verifica si $this->maxAmountCredit != null 
     *          3.1 Verifica si $this->maxAmountCredit->id == $max_amount_credit->id
     *              3.1a Retorna null;
     *          3.2 En caso de que sean distintos
     *              3.2a Elimina el monto base
     *              3.2b Calcula el margen manualmente, no llamar al metodo baseAmount()
     *              3.2c Compara si el margen mas el nuevo base amount es mayor al maximo
     *                  3.2c1 Si es asi, disminuye el margen
     *              3.2d Fija los nuevos valores de reputacion
     *              3.2e Establece el nuevo margen
     *              3.2f Verifica si el usuario posee retencion por pago retrasado
     *                  3.2f1 Si es asi, disminuye el margen a cero
     *      4 En caso de que $this->maxAmountCredit == null
     *          4.1 Fija los nuevos valores de reputacion
     *          4.2 Establece el nuevo margen     * 
     * @param Carbon $date_time
     * @return null
     */
    public function baseAmount(Carbon $date_time = null){
        if($date_time == null){
            $date_time = Carbon::now();
        }else{
            if (Carbon::now()->diffInSeconds($date_time) > 180) {
                echo($date_time.' $reputation->baseAmount(..)
                ');
            }
        }
        //1
        $user = $this->user();
        //2 Busca el MaxAmount Credit del usuario obtenido
        $max_amount_credit = MaxAmountCredit::where('currency_id',$user->currency()->id)
            ->where('profession_id',$user->profession->id)
            ->first();
        //3 Verifica si $reputation->maxAmountCredit != null 
        if($this->maxAmountCredit != null){
            //3.1 Verifica si $this->maxAmountCredit->id == $max_amount_credit->id
            if($this->maxAmountCredit->id == $max_amount_credit->id){
                //3.1a
                return null;
            }else{//3.2
                //3.2a
                $prev_margin = $this->marginUSDWithoutBaseAmountCall();
                $this->deleteBaseAmount($date_time);
                //3.2b Calcula el margen manualmente, no llamar al metodo baseAmount()
                $margin = $this->marginUSDWithoutBaseAmountCall();
                //3.2c
                $new_margin = $margin + $max_amount_credit->initial_limit;
                if($new_margin > $max_amount_credit->max_limit){
                    // 3.2c1 Si es asi, disminuye el margen
                    $decrease = $new_margin - $max_amount_credit->max_limit;
                    MarginModifier::decreaseForInflation($this, $decrease,$date_time);
                }
                //3.2d
                $this->max_amount_credit_id = $max_amount_credit->id;
                $this->default_currency_id = $max_amount_credit->currency->id;
                $this->one_usd_in_default_currency = ExchangeRate::localConvert('USD',$max_amount_credit->currency->iso_code, 1);
                $this->save();
                $this->refresh();
                //3.2e
                MarginModifier::establishBaseAmount($this,$max_amount_credit,$date_time);
                if(($prev_margin > 0) && ($this->marginUSDWithoutBaseAmountCall() <= 0)){
                    $increase = abs($prev_margin) + abs($this->marginUSDWithoutBaseAmountCall());
                    $new_amount = $this->marginUSDWithoutBaseAmountCall() + $increase;
                    if($new_amount > $this->maxAmountCredit->maxLimitUSD()){
                        $increase = $this->maxAmountCredit->maxLimitUSD() - $this->marginUSDWithoutBaseAmountCall();
                    }
                    $increase -= 0.1;
                    if(abs($increase) > Currency::getCurrency('usd')->min_value){
                        MarginModifier::increaseForDeflation($this,$increase,$date_time);                       
                    }
                }
                //3.2f Verifica si el usuario posee retencion por pago retrasado
                if($this->retainedMargin != null){
                    //3.2f1 Si es asi, disminuye el margen a cero
                    $margin = $this->marginUSDWithoutBaseAmountCall();
                    if($margin > 0){
                        $margin_modifier = MarginModifier::retentForDelayedPayment($this,$margin,$date_time);
                        $this->retained_margin_id = $margin_modifier->id;
                        $this->save();
                        $this->refresh();
                    }
                }
                return null;
            }
        }else{//4 En caso de que $this->maxAmountCredit == null
            //4.1
            $this->max_amount_credit_id = $max_amount_credit->id;
            $this->default_currency_id = $max_amount_credit->currency->id;
            $this->one_usd_in_default_currency = ExchangeRate::localConvert('USD',$max_amount_credit->currency->iso_code, 1);
            $this->save();
            $this->refresh();
            //4.2
            MarginModifier::establishBaseAmount($this,$max_amount_credit,$date_time);
            return null;
        }
     }
    /**
     * NOTA Este metodosolamente debe ser llamado por baseAmount() de $this
     * Decrementa el margen inicial para una reputacion
     * Como
     *      1 Verifica que la reputacio tenga margen inicial
     *      2 Crea el marginModifier que da de baja el margen inicial
     *      3 Establece en null el margen inicial del usuario
     * @param Carbon $date_time
     * @return MarginModifier
     */
    public function deleteBaseAmount(Carbon $date_time){
        if($this->maxAmountCredit != null){
            $mf = MarginModifier::deleteBaseAmount($this, $this->maxAmountCredit, $date_time);
            $this->max_amount_credit_id = null;
            $this->save();
            $this->refresh();
            return $mf;
        }
        return null;
     }
    /**
     * NOTA: Este metodo es invocado solamente por el metodo createCredit(..) de Credit
     * Retiene el margen del usuario cuando se solicita un credito
     * Como:
     *      1 Verifica que el $credit no sea nulo y que este recien creado 
     *      2 Obtiene el usuario de la reputacion
     *      3 Verifica que el crédito le pertenezca al usuario de la peticion
     *      4 Verifica si el monto en dolares es mayor a cero y menor o igual al margen
     *      5 Calcula el monto del crédito en usd
     *      6 llama a marginModifier para retener el margen
     * @param Credit $credit
     * @param Carbon $date_time
     * @return MarginModifier
     */
    public function retent(Credit $credit, Carbon $date_time){
        //1
        if ( ($credit != null) || ($credit->lastCreditPhase()->creditStage->id == CreditStage::getFirst()->id) ){
            //2
            $user = $this->user();
            //3
            if($user->hasCredit($credit)){
                //4
                if(($credit->amount > 0) && (ExchangeRate::localConvert($credit->currency()->iso_code,'USD',$credit->amount) < $this->marginUSD($date_time))){
                    $amount_usd = ExchangeRate::localConvert($credit->currency()->iso_code,'USD',$credit->amount);
                    //5
                    return MarginModifier::retentMargin($this, $amount_usd, $date_time);
                }
            }
        }
        echo('wedasads');
        dd();
        return null;
     }
    /**
     * NOTA Este metdo es invocado desde los metodos liquidate(), abort() y reject() de Credit
     * Libera el margen retenido por un credito
     * Como
     *      1 Comprueba si el crédito no es nulo
     *      2 Comprueba que el ultimo estado del credto sea 'Saldado', 'Cancelado' o 'Rechazado'
     *      3 Verifica que el crédito le pertenezca al usuario
     *      4 Calcula el monto en USD
     *      5 Llama a MarginModifier para liberar el margen
     * @param Credit $credit
     * @param Carbon $date_time
     * @return MarginModifier
     */
    public function liberate(Credit $credit, Carbon $date_time){
        //1
        if ($credit != null){
            //2
            $credit_stages = new Collection();
            $credit_stages->add(CreditStage::getLiquidate())->add(CreditStage::getAbort())->add(CreditStage::getReject());
            if($credit_stages->pluck('id')->contains($credit->lastCreditPhase()->creditStage->id)){
                //3
                $user = $this->user();
                if($user->hasCredit($credit)){
                    //4
                    $amount_usd = ExchangeRate::localConvert($credit->currency()->iso_code,'USD',$credit->amount);
                    //5
                    $new_amount = $this->marginUSD() + $amount_usd;
                    if($new_amount > $this->maxAmountCredit->maxLimitUSD()){
                        $amount_usd = $this->maxAmountCredit->maxLimitUSD() - $this->marginUSDWithoutBaseAmountCall();
                        $amount_usd -= 0.1;
                        if((abs($amount_usd) <= Currency::getCurrency('usd')->min_value) || ($this->retainedMargin != null)){
                            return $this->marginModifiers->last();
                        }
                    }
                    return MarginModifier::liberateMargin($this,$amount_usd, $date_time);
                }
                echo('dsfdgfgjdsrfdzxvc');
                dd();
            }
            echo('dfgdrtyfgh');
            dd();
        }
        echo('dfhdfythf'); 
        dd();
        return null;
     }
    /**
     * NOTA Este metodo es llamado desde los metodos good() y latePayment() de Credit
     * Retirne el margen del usuario o lo libera, dependiendo de cual sea el caso
     * Como funciona
     *      1 Obtiene el usuario
     *      2 Verifica si el usuario debe retener el margen
     *          2.1 En el vaso de que si, verifica si $this->retainedMargin == null
     *              2.1a Determina el monto a retener
     *              2.1b Crea un MarginModifier para retener el margen
     *              2.1c Asigna el $margin_moifier a $this->retainedMargin
     *              2.1c Guarda la reputacion y retorna
     *          ----
     *          2.2 En el caso de que $this->retainedMargin != null
     *              2.2a Verifica si el margen de la reputacon sea > 0
     *                  2.2a1 En caso de que sea asi, disminuye el margen por inflacion
     *              2.2b Retorna $this->retainedMargin
     *      ----
     *      3 Si hay que liberar margen
     *          4.1 Verifica si $this->retainedMargin != null
     *              3.1a Obtiene el margen a descontar
     *              3.1b Crea el MarginModifier
     *              3.1c Retorna el MarginModifier
     *          ----
     *          3.2 Si no, retorna null.
     * @param Carbon $date_time
     * @return MarginModifier
     */
    public function slowPayer(Carbon $date_time){  
        //1
        $user = $this->user();
        $user->creditsThatRetentsTheMargin();
        //2 Verifica si el usuario debe retener el margen
        if ($user->creditsThatRetentsTheMargin()->count() > 0){
            //2.1 En el vaso de que si, verifica si $this->retainedMargin == null
            if($this->retainedMargin == null){
                //2.1a Determina el monto a retener
                $margin = $this->marginUSD();
                //2.1b Crea un MarginModifier para retener el margen  
                if($margin > 0){
                    $margin_modifier = MarginModifier::retentForDelayedPayment($this,$margin,$date_time);
                    //2.1c Asigna el $margin_moifier a $this->retainedMargin
                    $this->retained_margin_id = $margin_modifier->id;
                    //2.1c Guarda la reputacion y retorna
                    $this->save();
                    $this->refresh();
                    return $margin_modifier;
                }else{
                    return $this->marginModifiers->last();
                }
            }else{//2.2 En el caso de que $this->retainedMargin != null
                //2.2a Verifica si el margen de la reputacion sea > 0
                $margin = $this->marginUSD($date_time);
                if($margin > 0){
                    //2.2a1 En caso de que sea asi, disminuye el margen por inflacion
                    MarginModifier::decreaseForInflation($this,$margin,$date_time);
                }
                //2.2b Retorna $this->retainedMargin
                return $this->retainedMargin;
            }
        }else{//3 Si hay que liberar margen
            //4.1 Verifica si $this->retainedMargin != null
            if($this->retainedMargin != null){
                //3.1a Obtiene el margen a descontar
                $margin_to_liberate = $this->retainedMargin->amount * -1;
                if($margin_to_liberate < 0){
                    echo('Error 85630153210');
                    dd();
                }
                //3.1b Crea el MarginModifier
                $new_amount = $this->marginUSD() + abs($margin_to_liberate);
                if($new_amount > $this->maxAmountCredit->maxLimitUSD()){
                    $margin_to_liberate = $this->maxAmountCredit->maxLimitUSD() - $this->marginUSDWithoutBaseAmountCall();
                    $margin_to_liberate -= 0.1;
                    if(abs($margin_to_liberate) <= Currency::getCurrency('usd')->min_value){
                        return $this->marginModifiers->last();
                    }
                }
                $margin_modifier = MarginModifier::liberateForDelayedPayment($this,$margin_to_liberate,$date_time);
                $this->retained_margin_id = null;
                $this->save();
                $this->refresh();
                //3.1c Retorna el MarginModifier
                return $margin_modifier;
            }else{//3.2 Si no, retorna null.
                return -1;
            }            
        }
     }
    /**
     * NOTA Este medomentodo solamente sera llamado desde el metodo payed de Payment
     * Como lo hace
     *      1 Verifica si el pago existe y su sy última etapa es 'Pagada'
     *      2 Obtiene el currency del paymert, y los dias de dias de diferencia entre las fechas de vencimiento y pago.
     *      3 Verifica si se pago en el dia del vencimiento
     *          3.1 Obtiene los modificadores
     *          3.2 Se calcula si el nuevo margen podria superar el limite de margen. Esto no se hace para Interes Y Seguro, porque el excedente se trata en el metodo calculate() de cada uno
     *          3.3 Llama a los metodos para crear los modificadores
     *      ----
     *      4 Si no, verifica si se paga antes del vencimientoto
     *          4.1 Calculo del margin
     *          4.2 Calculo de intereses
     *              4.2a Si la reputacion tiene intereses por seguro previos (MALO)
     *              4.2b Si la reputacion tiene descuentros previos en intereses (BUENO)
     *              4.2c Si la reputacion no tiene datos previos de intereses
     *          4.3 Calculo de seguro
     *              4.3a Si la reputacion tiene seguro positivo (MALO)
     *              4.3b Si la reputacion tiene seguro negativo (BUENO)
     *              4.3c Si la reputacion no tiene datos previos de seguro
     *          4.4 Idem punto 3.2
     *          4.5 Llama a los metodos para crear los modificadores
     *      ----
     *      5 Si no, verifica si se paga despues del vencimiento
     *          5.1 Calcula los modificadores
     *          5.2 Llama al los metodos para crear los modificadores
     * @param Payment $payment
     * @param Carbon $date_time
     * @return string
     */
    public function calculatePerformance(Payment $payment, Carbon $date_time){
        if (InterestModifier::calculatePerformance($payment,$date_time) == null) {
            echo('Error 1012248');
            dd();
        }
        if (InsuranceModifier::calculatePerformance($payment,$date_time) == null) {
            echo('Error 79445314');
            dd();
        }
        if (MarginModifier::calculatePerformance($payment,$date_time) == null) {
            echo('Error 612318');
            dd();
        }
        return "OK";
     }
    /**
     * NOTA: Este metodo esta declarado como estatico, la idea es que se ejecute desde un CRON
     * Recorre todas las reputaciones verificando si la divisa  vario mas del porcentaje prefijado y adapta el margen en la divisa CURRENCY
     * Como:
     *          1 Verifica si la reputacion tiene un usuario asociado
     *              1.1 Si no, elimina la reputacion
     *          2 Envia un mensaje a baseAmount(), para que vea el credio (por las dydas)
     *          3 Obtiene la divisa por defecto
     *          4 Verifica si el valor de la divisa con respecto al dolar aumentó
     *              4.1 Determina el nuevo monto del margen a incrementar
     *              4.2 Actualiza el valor de la cuenta
     *              4.3 Crea el margin modifier para incrementar el monto
     *          ----
     *          5 Verifica si el  valor de la divisa con respecto al dolar disminuyó
     *              5.1 Determina el nuevo monto del margen a disminuir
     *              5.2 Crea el MarginModifier
     *              5.3 Actualiza el valor de la cuenta
     * @return null                 
     */
    public function adjustMargin(Carbon $date_time = null){
        if($date_time == null){
            $date_time = Carbon::now();
        }else{
            if (Carbon::now()->diffInSeconds($date_time) > 180) {
                echo($date_time.' $reputation->adjustMargin(..)
                ');
            }
        }
        //1 Verifica si reputacion tiene un usuario asociado
        if($this->user() == null){
            //1.1 Si no, elimina la reputacion
            $this->delete();
            return null;
        }
        //2
        $this->baseAmount($date_time);
        //3 Obtiene la divisa por defecto
        $currency = $this->defaultCurrency;
        //4 Verifica si el valor de la divisa con respecto al dolar aumentó
        if ($this->retainedMargin != null) {
            if($currency->isDeflationBig($this->one_usd_in_default_currency)){
                //4.1 Determina el nuevo monto del margen a incrementar
                $new_amount_in_usd = $this->marginUSD($date_time) - (ExchangeRate::localConvert($currency->iso_code,'USD',($this->marginUSD($date_time) * $this->one_usd_in_default_currency)));
                if ($new_amount_in_usd != 0) {
                    //4.2 Actualiza el valor de la cuenta
                    $this->one_usd_in_default_currency = ExchangeRate::localConvert('USD',$currency->iso_code, 1);
                    $this->save();
                    $this->user()->refresh();
                    //4.3 Crea el margin modifier para incrementar el monto
                    $new_amount = $this->marginUSD() + abs($new_amount_in_usd);
                    $new_amount = $this->marginUSD() + $new_amount_in_usd;
                    if($new_amount > $this->maxAmountCredit->maxLimitUSD()){
                        $new_amount_in_usd = $this->maxAmountCredit->maxLimitUSD() - $this->marginUSDWithoutBaseAmountCall();
                        $new_amount_in_usd -= 0.1;
                        if (abs($new_amount_in_usd) <= Currency::getCurrency('usd')->min_value) {
                            return $this->marginModifiers->last();
                        }
                    }
                    $margin_modifier = MarginModifier::increaseForDeflation($this, $new_amount_in_usd, $date_time);
                    if($margin_modifier == null){
                        echo('Error 820045');
                        dd();
                    }
                }
                if ($new_amount_in_usd != 0){
                    return $margin_modifier;
                }else{
                    return null;
                }
            }else if($currency->isInflationBig($this->one_usd_in_default_currency)){//5 Verifica si el  valor de la divisa con respecto al dolar disminuyó
                //5.1 Determina el nuevo monto del margen a disminuir
                $new_amount_in_usd = (ExchangeRate::localConvert($currency->iso_code,'USD',($this->marginUSD($date_time) * $this->one_usd_in_default_currency))) - $this->marginUSD($date_time);
                if ($new_amount_in_usd != 0) {
                    //5.2 Crea el MarginModifier
                    $margin_modifier = MarginModifier::decreaseForInflation($this, $new_amount_in_usd , $date_time);
                    if($margin_modifier == null){
                        echo('Error 8200485');
                        dd();
                    }
                }

                //5.3 Actualiza el valor de la cuenta
                $this->one_usd_in_default_currency = ExchangeRate::localConvert('USD',$currency->iso_code, 1);
                $this->save();
                $this->user()->refresh();

                if ($new_amount_in_usd != 0){
                    return $margin_modifier;
                }else{
                    return null;
                }
            } 
        }   
        return null;
     }
    /**
     * Nota este metodo deberia ser llamado desde el front end
     * Incrementa manualmente ell margen de una reputacion
     * Como:
     *      1 Verifica que el monto sea positivo
     *      2 Convierte el monto a usd
     *      3  Si el monto es mayor, adapta el margen
     *      4 Crea la transaccion
     * @param float $amount_in_currency
     * @return MarginModifier
     */
    public function manualIncrease($amount_in_currency, Carbon $date_time = null){
        if($date_time == null){
            $date_time = Carbon::now();
        }else{
            if (Carbon::now()->diffInSeconds($date_time) > 180) {
                echo($date_time.' $reputation->manualIncrease(..)
                ');
            }
        }
        //1
        if ($this->maxAmountCredit > 0){
            //2
            $amount_usd = ExchangeRate::localConvert($this->defaultCurrency->iso_code,'USD',$amount_in_currency);
            //3
            $new_amount = $this->marginUSD() + abs($amount_usd);
            if($new_amount > $this->maxAmountCredit->maxLimitUSD()){
                $amount_usd = $this->maxAmountCredit->maxLimitUSD() - $this->marginUSDWithoutBaseAmountCall();
                $amount_usd -= 0.1;
                if((abs($amount_usd) <= Currency::getCurrency('usd')->min_value) || ($this->retainedMargin != null)){
                    return $this->marginModifiers->last();
                }
            }
            //4
            return MarginModifier::manualIncrease($this, $amount_usd, $date_time);
        }
        return null;
        
     }
}
