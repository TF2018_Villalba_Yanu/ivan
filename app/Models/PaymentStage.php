<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;

class PaymentStage extends Model
{
    protected $table = 'payment_stages';
    protected $fillable= [
        'name'       ,
        'description',
    ];
    public $timestamps = false;
    public function paymentPhases()
    {
        return $this->hasMany('App\Models\PhaymentPhase');
    }
    public function paymentStageNexts()
    {
        return $this->hasMany('App\Models\PaymentStageNext');
    }
    public function nextPaymentStages():Collection{
        $next_payment_stages = $this->paymentStageNexts;
        $return = new Collection();
        foreach ($next_payment_stages as $payment_stage_next){
            $return->add($payment_stage_next->paymentStageNext);
        }
        return $return;
    }
    /*
        1 => 'Generado' ---------> getGenerated()
        2 => 'Pago' -------------> getPay()
        3 => 'Vencido' ----------> getDue()
        4 => 'Pagado temporalmente por el seguro' --> getInsurancePay()
        5 => 'Interes agregado' -> getAddInterest()
        6 => 'Saldado' ----------> getPayed()
        7 => 'Incobrable' -------> getBad()
    */
    public static function getGenerated(){
        return PaymentStage::find(1);
    }
    public static function getPay(){
        return PaymentStage::find(2);
    }
    public static function getDue(){
        return PaymentStage::find(3);
    }
    public static function getInsurancePay(){
        return PaymentStage::find(4);
    }
    public static function getAddInterest(){
        return PaymentStage::find(5);
    }
    public static function getPayed(){
        return PaymentStage::find(6);
    }
    public static function getBad(){
        return PaymentStage::find(7);
    }
}
