<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;

class Financing extends Model
{
    protected $table = 'financings';
    protected $fillable = [
        'have_insurance',
        'wallet_id'     ,
        'credit_id'     ,
        'user_id'       ,//nullable. Usado para la auditoria
    ];
    public $timestamps = false;
    public function wallet(){
        return $this->belongsTo('App\Models\Wallet');
     }
    public function credit(){
        return $this->belongsTo('App\Models\Credit');
     }  
    public function repayments(){
        return $this->hasMany('App\Models\Repayment');
     }
    public function financingPhases(){
        return $this->hasMany('App\Models\FinancingPhase');
     }
    public function currency(){
        return $this->credit->currency();
     }
    public function amount(){
        $amount = 0;
        $financing_phases = $this->financingPhases;
        if(!$financing_phases->isEmpty())
            foreach($financing_phases as $financing_phase){
                $stage_id = $financing_phase->financingStage->id;
                if(($stage_id == FinancingStage::getFinance()->id) || ($stage_id == FinancingStage::getFinancingAbort()->id) || ($stage_id == FinancingStage::getCreditAbort()->id)){
                    $amount += $financing_phase->amount;
                }
            }
        return $amount;
     }
    public function amountWithoutAborts(){
        $amount = 0;
        $financing_phases = $this->financingPhases;
        if(!$financing_phases->isEmpty())
            foreach($financing_phases as $financing_phase){
                $stage_id = $financing_phase->financingStage->id;
                if($stage_id == FinancingStage::getFinance()->id){
                    $amount += $financing_phase->amount;
                }
            }
        return $amount;
     }
    public function lastFinancingPhase(){
        $financing_phases = $this->financingPhases;
        if($financing_phases->count() == 0){
            return null;
        }
        return $financing_phases->last();
     }
    public function nextFinancingStages(){
        $last_phase = $this->lastFinancingPhase();
        $return = new Collection();
        if($last_phase == null){
            $return->add(FinancingStage::getFinance());
            FinancingStage::getFinance()->refresh();
        }else{
            $return = $last_phase->financingStage->nextFinancingStages();
        }
        return $return;
     }
    
    public function financingProfitability(){
        $financing_amount = $this->amount();
        $profit_percentage = $this->credit->interest_percentage - ($this->credit->interest_percentage * $this->credit->commission_percentage);//porcentage de interes total menos el porcentaje de comision
        $profit_percentage += 1;
        if($this->have_insurance){
            $profit_percentage -= $this->credit->creditor_insurance_percentage;
        }
        return $financing_amount * $profit_percentage;
     }
    public function hasFinancingReturned(){
        $credit =$this->credit;
        $last_payment = $credit->payments->last();
        if($last_payment != null) {
            if ($this->have_insurance) {
                if($last_payment->lastPaymentPhase()->paymentStage->id == PaymentStage::getInsurancePay()->id) {
                    return true;
                }
            }
            if ($last_payment->unpayedAmount() < $this->wallet->currency->min_value) {
                return true;
            }
        }
        return false;
     }
    public function isActive(){
        if ($this->nextFinancingStages()->count() > 0) {
            if ($this->amount() >= $this->wallet->currency->min_value) {
                return true;
            }
        }            
        return false;
     }
    public function amountReturned(){
        /*
            5 => Reintegro >>>>>>>>>>>>>>>>> ::getRepayment()
            6 => Reintegro por seguro >>>>>> ::getInsurnceRepayment()
        */
        $amount = 0;
        $financing_phases = $this->financingPhases;
        foreach($financing_phases as $financing_phase){
            if(($financing_phase->financingStage->id == FinancingStage::getRepayment()->id) || ($financing_phase->financingStage->id == FinancingStage::getInsurnceRepayment()->id)){
                $amount += $financing_phase->amount;
                if($this->have_insurance){
                    $insurance_payment_transaction = WalletTransaction::where('repayment_id',$financing_phase->repayment->id)
                                                    ->where('wallet_type_of_transaction_id',WalletTypeOfTransaction::getInsurancePayment()->id)
                                                    ->first();
                    $amount -= $insurance_payment_transaction->amount;
                }
            }
        }
        return $amount;
     }
    public function hasDelayedReturn(){
        /*
             8 => Incobrable >>>>>>>>>>>>>>>> ::getBad()
            11 => Reintegro demorado >>>>>>>> ::getRepaymentLate()
        */
        $financing_stages = new Collection();
        $financing_stages->add(FinancingStage::getRepaymentLate())->add(FinancingStage::getBad());
        return $financing_stages->pluck('id')->contains($this->lastFinancingPhase()->financingStage->id);
     }
    public function canAddMoney($amount = 0){
        if ($amount == 0){
            $amount = $this->wallet->currency->min_value;
        }
        if ($this->nextFinancingStages()->pluck('id')->contains(FinancingStage::getFinance()->id)) {
            if ($this->wallet->accountBalance() >= $amount) {
                if (($this->credit->financedAmount() + $amount) < ($this->credit->amount + $this->wallet->currency->min_value)) {
                    return true;
                }
            }
        }
        return false;
    }
    /*
        Etapas de financiacion
        1  => Financiacion >>>>>>>>>>>>>> ::getFinance()------------> ::financiate(Credit $credit, Wallet $wallet, $amount)
        2  => Financiacion cancelada >>>> ::getFinancingAbort()-----> abortFinancing($amount)
        3  => Crédito cancelado >>>>>>>>> ::getCreditAbort()--------> abortCredit()
        4  => Acreditado >>>>>>>>>>>>>>>> ::getAccredit() ----------> accredit()
        5  => Reintegro >>>>>>>>>>>>>>>>> ::getRepayment()----------> return()
        6  => Reintegro por seguro >>>>>> ::getInsurnceRepayment()--> insuranceReturn()
        7  => Reintegrado >>>>>>>>>>>>>>> ::getReturned()-----------> financingReturned()
        8  => Incobrable >>>>>>>>>>>>>>>> ::getBad()----------------> bad()
        9  => Financiamiento terminado >> ::getCloseFinancing()-----> financingClose()
        10 => Financiamiento reabierto >> ::getReopenFinancing()----> financingReopened()
        11 => Reintegro demorado >>>>>>>> ::getRepaymentLate()------> lateReturn()
     */
    /**
     * NOTA Este metodo debe ser llamado desde el front end
     * Financia un credito
     * Como:
     *      1 Verifica si los parametros son correctos
     *      2 Verifica si la divisa del crédito es igual a la del wallet
     *          2.1 Si el monto de la financiacion es mayor al balance del wallet, disminuye el monto.
     *      3 busca el financiamiento y si no existe, lo crea
     *      4 VErifica si el monto es mayor a lo que queda por financiar, si es asim lo adapta
     *      5 Llama al metodo canAddMoney para verificar que se pueda agregar dinero al financiamiento
     *      6 Crea la fase de financiamiento
     *      7 Envia un mensaje a wallet para que cree la transaccion
     *      8 Verifica si el crédito se financio totalmente. Si es asi, Llamaa a metodo de Credit para generar la etapa
     *      9 Envia un mensaje a Voucher::financing(..) para generar el comprobante
     * @param Credit $credit
     * @param Wallet $wallet
     * @param bool $insurance
     * @param double $amount
     * @return FinancingPhase 
     */
    public static function financiate(Credit $credit, Wallet $wallet, bool $insurance, $amount, Carbon $date_time = null){
        if($date_time == null){
            $date_time = Carbon::now();
        }else{
            if (Carbon::now()->diffInSeconds($date_time) > 180) {
                echo($date_time.' Financing::financiate(..)
                ');
            }
        }
        //1
        if ($credit != null){
            if ($wallet != null){
                if ($amount > 0){
                    //2
                    if($wallet->currency->id == $credit->currency()->id){
                        //2.1
                        if($amount > $wallet->accountBalance()){
                            $amount = $wallet->accountBalance();
                        }
                        //3
                        $financing = Financing::where('wallet_id',$wallet->id)
                        ->where('credit_id',$credit->id)
                        ->first();
                        if($financing == null){
                            $user_id = null;
                            if(auth()->user() != null){
                                $user_id = auth()->user()->id;
                            }            
                            $financing = Financing::create([
                                'have_insurance' => $insurance,
                                'wallet_id'      => $wallet->id,
                                'credit_id'      => $credit->id,
                                'user_id'        => $user_id,
                            ]);
                            $financing->refresh();
                            $wallet->refresh();
                            $credit->refresh();
                        }
                        //4
                        if ($amount > ($credit->amount - $credit->financedAmount())){
                            $amount = $credit->amount - $credit->financedAmount();
                        }
                        //5 Llama al metodo canAddMoney para verificar que se pueda agregar dinero al financiamiento
                        if ($financing->canAddMoney($amount) != null) {
                            //6
                            if(auth()->user() != null){
                                $user_id = auth()->user()->id;
                            }else{
                                $user_id = null;
                            }
                            $financing_phase = FinancingPhase::create([
                                'user_id'            => $user_id,
                                'amount'             => Number::fix($amount),
                                'date'               => $date_time,
                                'financing_id'       => $financing->id,
                                'financing_stage_id' => FinancingStage::getFinance()->id,
                            ]);
                            $financing->refresh();
                            $credit->refresh();
                            //7
                            if($wallet->financiate($financing_phase,$date_time) == null){
                                echo('Error 94287');
                                dd();
                            }
                            //8
                            if (($credit->amount - $credit->financedAmount()) <= 0){
                                if($credit->endFinancing($date_time) == null){
                                    echo('Error 24451');
                                    dd();
                                }
                            }
                            //9 
                            if(Voucher::financing($financing_phase,$date_time) == null){
                                echo('Error 347925058');
                                dd();
                            }
                            return $financing_phase; 
                        }
                        echo('Error 4581 '.$financing->nextFinancingStages());
                        dd();
                    }
                    echo('Error 151513');
                    dd();
                }
                echo('Error 5518221 ID de credito '.$credit->id.' Momto que se intenta financiar '.$amount.'usuario '.$credit->wallet->user->name);
                dd();
            }
            echo('Error 85147511 wallet null');
            dd();
        }
        echo('Error 1973 credit null');
        dd();
        
     }
    /**
     * NOTA: Se supone que este metodo seria llamado desde el front end
     * Cancela un financiamiento, una parte o todo
     * Como:
     *      1: Verifica si se puede cancelar la financiacion.
     *      2: Crea la fase de financiacion
     *      3: Envia un mensaje al wallet para que actualice su balance de cuenta
     *      4: Verifica si el estafo del crédito es financiacion finalizada
     *          4.1 Si es Asi, envia un mensaje al crédito para que actualice su estado
     *      5: Envia un mensaje a Voucher::abortFinancing(..) para generar el comprobante
     * @param float $amount
     * @return FinancingPhase
     */
    public function abortFinancing($amount, Carbon $date_time = null){
        if($date_time == null){
            $date_time = Carbon::now();
        }else{
            if (Carbon::now()->diffInSeconds($date_time) > 180) {
                echo($date_time.' $financing->abortFinancing(..)
                ');
            }
        }
        //1
        if ($this->nextFinancingStages()->pluck('id')->contains(FinancingStage::getFinancingAbort()->id)){
            if ($this->amount() >= $amount){
                if ($amount >= $this->wallet->currency->min_value){
                    //2
                    if(auth()->user() != null){
                        $user_id = auth()->user()->id;
                    }else{
                        $user_id = null;
                    }
                    $financing_phase = FinancingPhase::create([
                        'user_id'            => $user_id,
                        'amount'             => Number::fix($amount * -1),
                        'date'               => $date_time,
                        'financing_id'       => $this->id,
                        'financing_stage_id' => FinancingStage::getFinancingAbort()->id,
                    ]);
                    $this->refresh();
                    $this->wallet->refresh();
                    $this->credit->refresh();
                    //3
                    $this->wallet->financingAbort($financing_phase, $date_time);
                    //4
                    if($this->credit->lastCreditPhase()->creditStage->id == CreditStage::getFinanced()->id){
                        //4.1
                        if($this->credit->reopened($date_time) == null){
                            echo('Error 85130');
                            dd();
                        }
                    }
                    //5
                    if(Voucher::abortFinancing($financing_phase,$date_time) == null){
                        echo('Error 347925058');
                        dd();
                    }
                    return $financing_phase;
                }
                echo('Error 2342325');
                dd();
            }
            echo('Error 3452');
            dd();
        }
        echo('Error 84578');
        dd();
        return null;
     }
    /**
     * NOTA: Este medotodo deberia ser llamado por el metodo abort() de Credit
     * Cancela un financiamiento cuando el crédito es cancelado
     * Como:
     *     1: Crea la fase de financiamiento
     *     2: Envia un mensaje al wallet para crear la trabsaccion
     *     3: Envia un mensaje a abortFinancedCredit(..) para generar el comprobante
     * @param Carbon $date_time
     * @return FinancingPhase
     */
    public function abortCredit(Carbon $date_time){
        if(auth()->user() != null){
            $user_id = auth()->user()->id;
        }else{
            $user_id = null;
        }
        $financing_phase = FinancingPhase::create([
            'user_id'            => $user_id,
            'amount'             => Number::fix($this->amount() * -1),//negativo
            'date'               => $date_time,
            'financing_id'       => $this->id,
            'financing_stage_id' => FinancingStage::getCreditAbort()->id,
        ]);
        $this->refresh();
        if($this->wallet->cancelFinancingCredit($financing_phase, $date_time) == null){
            echo('Error 1806');
            dd();
        }
        //3
        if(Voucher::abortFinancing($financing_phase,$date_time) == null){
            echo('Error 8014563156');
            dd();
        }
        return $financing_phase;
     }
    /**
     * NOTA: Esta funcion es invocada desde el metodo grant() de Credit
     * Registra que el financiamiento se acrédito (es decir, se transfirio al wallet del deudor)
     * Como:
     *      1 Verifica si se puede financiar
     *      2 Crea la fase de financiacion
     * @param Carbon $date_time
     * @return FinancingPhase
     */
    public function accredit(Carbon $date_time){
        //1
        if ($this->nextFinancingStages()->pluck('id')->contains(FinancingStage::getAccredit()->id)){
            if((($this->credit->amount + $this->wallet->currency->min_value) >= $this->credit->financedAmount()) || (($this->credit->amount + $this->wallet->currency->min_value) <= $this->credit->financedAmount())){
                //2
                if(auth()->user() != null){
                    $user_id = auth()->user()->id;
                }else{
                    $user_id = null;
                }
                $financing_phase = FinancingPhase::create([
                    'user_id'            => $user_id,
                    'amount'             => Number::fix(0),
                    'date'               => $date_time,
                    'financing_id'       => $this->id,
                    'financing_stage_id' => FinancingStage::getAccredit()->id,
                ]);
                $this->refresh();
                return $financing_phase;    
            }
            echo('Error 45687');
            dd();
        }
        echo('Error 7694');
        dd();
        return null;        
     }
    /**
     * NOTA: Este metodo se debe llamar desde el metodo pay($amount) de Payment
     * Registra el reintegro de las financiaciones
     * Como:
     *      1 Verifica si puede recibir el pago
     *      1.1 Crea una bandera para verificar si el pago estaba retrasado. 
     *      2 Crea la face de financiamiento
     *      3 Envia un mensaje a wallet para que registre la transaccion
     *      4 Si el financiamiento tiene seguro, enviara un mensaje al wallet para que registre la transaccion
     *      5 Si se completo la devolucion para el financiamiento, invoca al metodo financingReturned, de esta clase
     *      6 Verifica si la bandera del punto 1.1 esta activada y si el pago tiene saldo.
     *      7 Envia un mensaje a financingReturn(..) para generar el comprobante
     * @param Repayment $repayment
     * @param Carbon $date_time
     * @return FinancingPhase
     */
    public function return(Repayment $repayment, Carbon $date_time){
        //1
        if ($repayment != null){
            if($repayment->financing->id == $this->id){
                if($this->nextFinancingStages()->pluck('id')->contains(FinancingStage::getRepayment()->id)){
                    //1.1
                    $late_return_flag = ($this->lastFinancingPhase()->financingStage->id == FinancingStage::getRepaymentLate()->id);
                    //2
                    if(auth()->user() != null){
                        $user_id = auth()->user()->id;
                    }else{
                        $user_id = null;
                    }
                    $financing_phase = FinancingPhase::create([
                        'user_id'            => $user_id,
                        'amount'             => Number::fix($repayment->amount),
                        'date'               => $date_time,
                        'financing_id'       => $this->id,
                        'financing_stage_id' => FinancingStage::getRepayment()->id,
                        'repayment_id'       => $repayment->id,
                    ]);
                    $this->refresh();
                    //3
                    if($this->wallet->returnForFinancing($repayment,$date_time) == null){
                        echo('Error 6007');
                        dd();
                    }
                    //4
                    if($this->have_insurance){
                        if($this->wallet->insuranceCreditorPayment($repayment,$date_time) == null){
                            echo('Error 852450');
                            dd();
                        }
                    }
                    //5 Si se completo la devolucion para el financiamiento, invoca al metodo financingReturned, de esta clase
                    if ($this->hasFinancingReturned()){
                        if ($this->financingReturned($date_time) == null) {
                            echo('Error 884225721');
                            dd();
                        }
                    }
                    //6
                    if (($late_return_flag) && ($repayment->paymentPhase->payment->unpayedAmount() >= $repayment->currency()->min_value)){
                        if($this->lateReturn($date_time) == null){
                            echo('Error 3105628');
                            dd();
                        }
                    }
                    //7
                    if(Voucher::financingReturn($financing_phase,$date_time) == null){
                        echo('Error 04862896');
                        dd();
                    }
                    return $financing_phase;  
                }
                echo('Error 85100');
                dd();
            }
            echo('Error 62104');
            dd();
        }
        echo('Error 65506');
        dd();
        return null;
     }
    /**
     * NOTA: Este metodo es llamado desde el metodo insuranceEffect de Payment
     * Registra el reintegro por seguro para un financiamieto
     * Como:
     *     1 Verifica si puede recibir el pago
     *     2 Crea la fase de financiamiento
     *     3 Envia un mensaje a wallet del acreedor para que registre la transaccion
     *     4 Envia un mensaje al wallet del acreedor para que registre el descuento por seguro (abonar el seguro)
     *     5 Si se completo la devolucion para el financiamiento, invoca al metodo financingReturned, de esta clase
     *     6 Llama a insuranceFinancingReturn(..) para generar el comprobante
     * @param Repayment $repayment
     * @param Carbon $date_time
     * @return FinancingPhase
     */
    public function insuranceReturn(Repayment $repayment, Carbon $date_time){
        //1 Verifica si puede recibir el pago
        if ($repayment != null){
            if($repayment->financing->id == $this->id){
                if($this->nextFinancingStages()->pluck('id')->contains(FinancingStage::getInsurnceRepayment()->id)){
                    //2 Crea la fase de financiamiento
                    if(auth()->user() != null){
                        $user_id = auth()->user()->id;
                    }else{
                        $user_id = null;
                    }
                    $financing_phase = FinancingPhase::create([
                        'user_id'            => $user_id,
                        'amount'             => Number::fix($repayment->amount),
                        'date'               => $date_time,
                        'financing_id'       => $this->id,
                        'financing_stage_id' => FinancingStage::getInsurnceRepayment()->id,
                        'repayment_id'       => $repayment->id,
                    ]);
                    $this->refresh();
                    //3 Envia un mensaje a wallet del acreedor para que registre la transaccion
                    if ($this->wallet->insuranceReturnForFinancing($repayment,$date_time) != null) {
                        //4 Envia un mensaje al wallet del acreedor para que registre el descuento por seguro (abonar el seguro)
                        if ($this->wallet->insuranceCreditorPayment($repayment,$date_time) != null) {
                            //5 Si se completo la devolucion para el financiamiento, invoca al metodo financingReturned, de esta clase
                            if ($this->hasFinancingReturned()){
                                if ($this->financingReturned($date_time) == null) {
                                    echo('Error 88227721');
                                    dd();
                                }
                            }
                            //6
                            if(Voucher::insuranceFinancingReturn($financing_phase,$date_time) == null){
                                echo('Error 3058518952');
                                dd();
                            }
                            return $financing_phase;  
                        }
                        echo('Error 58114');
                        dd();
                    }
                    echo('Error 8547');
                    dd();
                }
                echo('Error 851150 id del financiamieto: '.$this->id);
                dd();
            }
            echo('Error 98205');
            dd();
        }
        echo('Error 699248');
        dd();
        return null;
     }
    /**
     * NOTA: Metodo llamado desde insuranceReturn y return de esta clase
     * Crea la etapa de devuelto para un financiamiento
     * Como:
     *      1 Veridica si puede pasar a ese estado
     *      2 Verifica si se se ha devuelto todo el financiamiento
     *      3 Crea la etapa de devuelto
     * @param Carbon $date_time
     * @return FinancingPhase
     */
    public function financingReturned(Carbon $date_time){
        //1
        if ($this->nextFinancingStages()->pluck('id')->contains(FinancingStage::getReturned()->id)) {
            //2
            if ($this->hasFinancingReturned()) {
                //3
                if(auth()->user() != null){
                    $user_id = auth()->user()->id;
                }else{
                    $user_id = null;
                }
                $financing_phase = FinancingPhase::create([
                    'user_id'            => $user_id,
                    'amount'             => Number::fix(0),
                    'date'               => $date_time,
                    'financing_id'       => $this->id,
                    'financing_stage_id' => FinancingStage::getReturned()->id,
                ]);
                $this->refresh();
                return $financing_phase;
            }
        }
        return null;
     }
    /**
     * NOTA: Este financiamiento solamente se debe llamar desde el metodo bad de Payment
     * Actualiza el estado del financiamiento a "Incobrable"
     * Como
     *      1 Verifica si el financiamiento puede avanzar a ese estado.
     *      2 Crea la instancia de FinancingPhase
     * @param Carbon $date_time
     * @return FinancingPhase
     */
    public function bad(Carbon $date_time){
        //1
        if ($this->nextFinancingStages()->pluck('id')->contains(FinancingStage::getBad()->id)) {
            //2
            if(auth()->user() != null){
                $user_id = auth()->user()->id;
            }else{
                $user_id = null;
            }
            $financing_phase = FinancingPhase::create([
                'user_id'            => $user_id,
                'amount'             => Number::fix(0),
                'date'               => $date_time,
                'financing_id'       => $this->id,
                'financing_stage_id' => FinancingStage::getBad()->id,
            ]);
            $this->refresh();
            return $financing_phase;
        }
        echo('Error 767878899234 Ultimo estado del financiamiento: '.$this->lastFinancingPhase()->financingStage->name);
        dd();
        return null;
     }
    /**
     * NOTA Este metodo solamente debe ser llamado por el metodo endFinancing() de Credit
     * Cierra el financimiento una vez que el la financiacion del crédito se haya finalizado
     * Como:
     *      1 Verifica si la financiacion puede cerrarse
     *      2 Verifica si el estado actual del crédito es 'Financiacion Finalizada'
     *      3 Crea el FinancingPhase
     *      4 Retorna
     * @param Carbon $date_time
     * @return FinancingPhase
     */
    public function financingClose(Carbon $date_time){
        //1
        if($this->nextFinancingStages()->pluck('id')->contains(FinancingStage::getCloseFinancing()->id)){
            //2
            if($this->credit->lastCreditPhase()->creditStage->id == creditStage::getFinanced()->id){
                //3
                if(auth()->user() != null){
                    $user_id = auth()->user()->id;
                }else{
                    $user_id = null;
                }
                $financing_phase = FinancingPhase::create([
                    'user_id'            => $user_id,
                    'amount'             => Number::fix(0),
                    'date'               => $date_time,
                    'financing_id'       => $this->id,
                    'financing_stage_id' => FinancingStage::getCloseFinancing()->id,
                ]);
                $this->refresh();
                //4
                return $financing_phase;
            }
        }
        return null;
     }
    /**
     * NOTA Solo debe ser llamado desde el metodo reopened() de Credit
     * Vuelve a abrir el financiamiento para un credito
     * Como
     *      1 Controla que se puede pasar al estado
     *      2 Controla que el financiamiento del crédito es menor al monto
     *      3 Crea la etapa del Financiamiento
     *      4 Retorna
     * @param Carbon $date_time
     * @return FinancingPhase
     */
    public function financingReopened(Carbon $date_time){
        //1
        if($this->nextFinancingStages()->pluck('id')->contains(FinancingStage::getReopenFinancing()->id)){
            //2
            if(Number::fix($this->credit->amount - $this->credit->financedAmount()) >= Number::fix($this->credit->currency()->min_value)){
                //3
                if(auth()->user() != null){
                    $user_id = auth()->user()->id;
                }else{
                    $user_id = null;
                }
                $financing_phase = FinancingPhase::create([
                    'user_id'            => $user_id,
                    'amount'             => Number::fix(0),
                    'date'               => $date_time,
                    'financing_id'       => $this->id,
                    'financing_stage_id' => FinancingStage::getReopenFinancing()->id,
                ]);
                $this->refresh();
                //4
                return $financing_phase;
            }
            echo('Error 95278 '.$this->credit->amount.' '.$this->credit->financedAmount().' '.Number::fix($this->credit->amount - $this->credit->financedAmount()).' '.Number::fix($this->credit->currency()->min_value));
            dd();
        }
        echo('Error 0568 '.$this);
        dd();
        return null;
     }
    /**
     * NOTA: este metodo solamente debe ser llamado desde el metodo insuranceEffect de Payment
     * O desde payment cuando se reintegra un pago retrasado pero no se paga todo
     * Acrualiza el estado de la devolucion a "Retrasado"
     * Como
     *      1 Controla que se puede pasar al estado
     *      2 Verifica si el seguro esta desactivado
     *      3 Crea la etapa del Financiamiento
     *      3 Retorna 
     * @param Carbon $date_time
     * @return FinancingPhase
     */
    public function lateReturn(Carbon $date_time){
        if($this->nextFinancingStages()->pluck('id')->contains(FinancingStage::getRepaymentLate()->id)){
            if(!$this->have_insurance){
                if(auth()->user() != null){
                    $user_id = auth()->user()->id;
                }else{
                    $user_id = null;
                }
                $financing_phase = FinancingPhase::create([
                    'user_id'            => $user_id,
                    'amount'             => Number::fix(0),
                    'date'               => $date_time,
                    'financing_id'       => $this->id,
                    'financing_stage_id' => FinancingStage::getRepaymentLate()->id,
                ]);
                $this->refresh();
                //4
                return $financing_phase;
            }
            echo('Error 81694');            
            dd();
        }
        echo('Error 301530'); 
        dd();
        return null;
    }
}
