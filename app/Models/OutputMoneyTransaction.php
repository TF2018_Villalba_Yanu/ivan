<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OutputMoneyTransaction extends Model
{
    protected $fillable = [
        'user_id',//nullable. Usado para la auditoria
    ];
    public $timestamps = false;
    
    public function walletTransaction(){
        return WalletTransaction::where('output_money_transaction_id',$this->id)
            ->first();
    }
}
