<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VoucherType extends Model
{
    protected $table = 'voucher_types';
    protected $fillable= [
        'name',
        'description',
    ];
    public $timestamps = false;

    /*
        01 'Solicitud de crédito'----------> ::getCreditRequest()
        02 'Pago de crédito'---------------> ::getCreditPay()
        03 'Ingreso de dinero'-------------> ::getInputMoney()
        04 'Extraccion de dinero'----------> ::getOutputMoney()
        05 'Crédito cancelado'-------------> ::getAbortCredit()
        06 'Financiamiento'----------------> ::getFinancing()
        07 'Financiamiento cancelado'------> ::getAbortFinancing()
        08 'Crédito financiado cancelado'--> ::getAbortFinancedCredit()
        09 'Reintegro'---------------------> ::getFinancingReturn()
        10 'Reintegro asegurado'-----------> ::getInsuranceFinancingReturn()
        11 'Crédito rechazado'-------------> ::getRejectCredit()
        12 'Abono del seguro'--------------> ::getInsurancePay()
        13 'Ingreso por conversión de divisas'--------> ::getExchangeInput()
        14 'Egreso por conversion'---------> ::getExchangeOutput()
    */
    public static function getCreditRequest(){
        return VoucherType::find(1);
    }
    public static function getCreditPay(){
        return VoucherType::find(2);
    }
    public static function getInputMoney(){
        return VoucherType::find(3);
    }
    public static function getOutputMoney(){
        return VoucherType::find(4);
    }
    public static function getAbortCredit(){
        return VoucherType::find(5);
    }
    public static function getFinancing(){
        return VoucherType::find(6);
    }
    public static function getAbortFinancing(){
        return VoucherType::find(7);
    }
    public static function getAbortFinancedCredit(){
        return VoucherType::find(8);
    }
    public static function getFinancingReturn(){
        return VoucherType::find(9);
    }
    public static function getInsuranceFinancingReturn(){
        return VoucherType::find(10);
    }
    public static function getRejectCredit(){
        return VoucherType::find(11);
    }
    public static function getInsurancePay(){
        return VoucherType::find(12);
    }
    public static function getExchangeInput(){
        return VoucherType::find(13);
    }
    public static function getExchangeOutput(){
        return VoucherType::find(14);
    }
}
