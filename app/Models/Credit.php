<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class Credit extends Model
{
    protected $table = 'credits';
    protected $fillable= [
        'amount'                       ,
        'interest_percentage'          ,
        'commission_percentage'        ,
        'creditor_insurance_percentage',
        'debtor_insurance_percentage'  ,
        'payments_number'              ,
        'wallet_id'                    ,
        'grace_period_id'              ,
        'interest_for_late_payment'    ,        
        'due_dates_interval_id'        ,
        'user_id'                      ,//llanuusado para la auditoria
     ];
    //Porcentage de comision del interes, es decir, si el interes es 0.45% y la comision = 0.43%, la comision real seria = 0.45*0.43 = 0,1935%
    //El el ejemplo anterior, el acreedor cobraria 0.45 - 0,1935 = 0,2565%
    // 0,1935% + 0,2565% = 0.45%
    public $timestamps = false;
    public function wallet(){
        return $this->belongsTo('App\Models\Wallet');
     }
    public function gracePeriod(){
        return $this->belongsTo('App\Models\CustomDateInterval', 'grace_period_id', 'id');
     }
    public function dueDatesInterval(){
        return $this->belongsTo('App\Models\CustomDateInterval', 'due_dates_interval_id', 'id');
     }
    public function financings(){
        return $this->hasMany('App\Models\Financing');
     }
    public function payments(){
        return $this->hasMany('App\Models\Payment');
     }
    public function creditPhases(){
        return $this->hasMany('App\Models\CreditPhase');
     }
    public function currency(){
        return $this->wallet->currency;
     }
    public function user(){
        return $this->wallet->user;
     }
    public function lastCreditPhase(){
        return $this->creditPhases->last();
     }
    public function nextCreditStages():Collection{
        $last_phase = $this->lastCreditPhase();
        $return = new Collection();
        if($last_phase == null){
            $return->add(CreditStage::getFirst());
        }else{
            $return = $last_phase->creditStage->nextCreditStages();
        }
        return $return;
     }
    public function financedAmount(){
        $financings = $this->financings;
        $amount = 0.0;
        foreach ($financings as $financing){
            $amount += $financing->amount();
        }
        return $amount;
     }
    public function unpayedAmount(){
        $payments = $this->payments;
        $amount = 0;
        foreach ($payments as $payment) {
            if ($payment->lastPaymentPhase()->paymentStage->id != PaymentStage::getPayed()->id) {
                $amount += $payment->unpayedAmount();
            }
        }
        return (double) $this->currency()->truncate($amount);
     }
    public function payedAmount(){
        $payments = $this->payments;
        $amount = 0;
        foreach ($payments as $payment) {
            if ($payment->lastPaymentPhase()->paymentStage->id != PaymentStage::getPayed()->id) {
                $amount += $payment->payedAmount();
            }
        }
        return (double) $this->currency()->truncate($amount);
     }
    public function firstUnpayedPayment(Carbon $date_time = null){
        if($date_time == null){
            $date_time = Carbon::now();
        }else{
            if (Carbon::now()->diffInSeconds($date_time) > 180) {
                echo($date_time.' credit->firstUnpayedPayment(..)
                ');
            }
        }
        $payments = $this->payments;
        foreach ($payments as $payment){
            if ($payment->lastPaymentPhase()->paymentStage->id != PaymentStage::getPayed()->id) {
                if($payment->unpayedAmount() >= $this->currency()->min_value){
                    return $payment;
                }else{
                    $user_id = null;
                    if(auth()->user() != null){
                        $user_id = auth()->user()->id;
                    }
                    $payment_phase = PaymentPhase::create([
                        'amount'           => Number::fix(0),
                        'date'             => $date_time,
                        'payment_id'       => $payment->id,
                        'payment_stage_id' => PaymentStage::getPayed()->id,
                        'user_id'          => $user_id,
                    ]);
                    //4 Si es la última cuota, envia un mensaje a crédito tara actualizar el estado
                }
            }
        }
        return null;
     }
    public function isActive(){
        if( $this->nextCreditStages()->count() > 0)
            return true;
        return false;
     }
    public function retainsMargin(){
         /*
            6  => 'Pago retrasado' >>>>>>>>>>>>>> ::getPaymentLate()
            7  => 'Mora' >>>>>>>>>>>>>>>>>>>>>>>> ::getDefaulter()
            8  => 'Citado' >>>>>>>>>>>>>>>>>>>>>> ::getJudicialCitation()
            9  => 'Litigio' >>>>>>>>>>>>>>>>>>>>> ::getLawsuit()
            13 => 'Incobrable' >>>>>>>>>>>>>>>>>> ::getBad()
        */
        $credit_stages = new Collection();
        $credit_stages->add(CreditStage::getPaymentLate())->add(CreditStage::getDefaulter())->add(CreditStage::getJudicialCitation())->add(CreditStage::getLawsuit())->add(CreditStage::getBad());
        return $credit_stages->pluck('id')->contains($this->lastCreditPhase()->creditStage->id);
     }
    public function financingRemaining(){
        if ($this->lastCreditPhase()->creditStage->id == CreditStage::getFinancing()->id) {
            $amount = $this->financedAmount();
            $currency = $this->currency();
            $amount_remaining = $this->amount - $this->financedAmount();
            if ($amount >= $this->amount*0.25) {
                $seconds = $this->financings->last()->financingPhases->last()->date->diffInSeconds($this->financings->first()->financingPhases->first()->date);
                $seconds_remaining = ($amount_remaining * $seconds) / $this->financedAmount();
                $seconds_remaining = (int) $seconds_remaining;
            }else{
                $seconds_remaining = $currency->financingRemaining($amount_remaining);
            }
            if ($this->financings->count() > 0) {
                $financed_date = new Carbon($this->financings->last()->financingPhases->last()->date->toDateTimeString());
            }else{
                $financed_date = new Carbon($this->lastCreditPhase()->date->toDateTimeString());                
            }
            if ($seconds_remaining != null) {
                $financed_date->addSeconds($seconds_remaining);
                $array = [
                    $financed_date->format('d/m/Y'),
                    $this->amount,
                    $currency->numberToString($this->amount),
                    false,
                ];
                return $array;
            }
            return null;
        }
     }
    public function debtorInterests(){
        return (1 + $this->interest_percentage + $this->debtor_insurance_percentage);
    }
    /**
     * Nota: este metodo es llamado desde el front end
     * Este metodo recibe una coleccion de objetos. La estructura de datos es una Collection.
     * Cada item de la Collection tiene una nueva collection con los siguientes items: [0]wallet con que se paga [1]Monto a pagar en ese wallet [2]Tasa de conversion utilizada en ese momento.
     */
    public function pay(Collection $objects, Carbon $date_time = null){
        if($date_time == null){
            $date_time = Carbon::now();
        }else{
            if (Carbon::now()->diffInSeconds($date_time) > 180) {
            echo($date_time.' $credit->pay(..)
            ');
            }
        }
        //Contenido de $objects
        //es un Collection con estos datos
        //[0](wallet que pagaria el credito)
        //[1](Monto para pagar con ese wallet en el monto del wallet)
        //[2](tasa de cambio)
        if ($objects->count() >= 1){
            $new_amount = $objects[0][1];
            if (($objects->count() > 1) || (($objects->count() == 1) && ($objects->first()[0]->currency->id != $this->currency()->id))){
                $new_amount = 0;
                foreach($objects as $object){
                    if($object[0]->currency->id != $this->currency()->id){
                        if($object[2]->toCurrency->id == $this->currency()->id){
                            $exchange = Exchange::exchange($object[0],$this->currency(),$object[1],$object[2],$date_time);
                            $new_amount += $exchange->convert(1);
                        }else{
                            echo('Error 204548548 '.$object[0].' '.$object[2].' '.$this);
                            dd();
                        }
                    }else{
                        $new_amount += $object->amount;
                    }
                }
                while(($new_amount < $this->currency()->min_value) && ($objects[0][0]->accountBalance() >= $objects[0][0]->currency->min_value)){
                    $exchange = Exchange::exchange($object[0],$this->currency(),$objects[0][0]->currency->min_value,$object[2],$date_time);
                    $new_amount += $exchange->convert(1);
                }
            }
            if($new_amount >= $this->currency()->min_value){
                return $this->wallet->payCredit($this,$new_amount,$date_time);
            }
        }
        echo('Error 862555224');
        dd();
    }
    /*
        'Solicitado'--------------> ::getFirst()=============> ::createCredit($user, $payment_plan, $amount)
        'Financiando'-------------> ::getFinancing()=========> approvate() 
        'Financiacion Finalizada'-> ::getFinanced()==========> endFinancing()
        'Otorgado'----------------> ::getGrant()=============> grant()
        'Al dia'------------------> ::getGood()==============> good()
        'Pago retrasado'----------> ::getPaymentLate()=======> latePayment()
        'Mora'--------------------> ::getDefaulter()=========> defaulterCredit()
        'Citado'------------------> ::getJudicialCitation()==> citation() 
        'Litigio'-----------------> ::getLawsuit()===========> lawsuit()
        'Saldado'-----------------> ::getLiquidate()=========> liquidate()
        'Cancelado'---------------> ::getAbort()=============> abort()
        'Rechazado'---------------> ::getReject()============> reject()
        'Incobrable'--------------> ::getBad()===============> bad()
        'Financiacion rehabierta'-> ::getFinancingReopened()=> reopened()
     */
    /**
     * NOTA Este metodo sera llamado desde createCredit y desde el controller
     * Simula los valores obtenidos en un credito
     *     1: Verificar que los parametros son correctos y el usuario puede solicitar el credito.
     *     2: Busca el wallet
     *         2.1: Verifica si existe.
     *         2.2: Si no existe uno, lo crea
     *     3: Calcula porcentaje de seguro 
     *     4: Calcula el porcentaje de interes
     *     5: Calcula el interes por pago atrasado
     *     6: Obtiene los dias de gracia
     *     7: Art=ma todo en un lindo arreglo
     *     8: Lo devuelve.
     * @param User $user
     * @param PaymentPlan $payment_plan
     * @return array
     */
    public static function simutalteCredit(User $user, PaymentPlan $payment_plan, Carbon $date_time = null){
        if($date_time == null){
            $date_time = Carbon::now();
        }else{
            if (Carbon::now()->diffInSeconds($date_time) > 180) {
                echo($date_time.' Credit::simulateCredit(..)
                ');
            }
        }
        //1
        if ( ($user != null) && ($payment_plan != null)){
            if($user->activeCredits()->count() < $user->reputation->activeCreditsLimit($date_time)){
                $currency = $payment_plan->currency; 
                //2
                $wallet = Wallet::where('currency_id',$payment_plan->currency->id)
                    ->where('user_id',$user->id)
                    ->first();
                //2.1
                if($wallet == null){
                    //2.2
                    $wallet = Wallet::create([
                        'user_id'         => $user->id,              
                        'currency_id'     => $currency->id,
                    ]);
                    $wallet->refresh();
                }
                //3
                $insurance_percentage = $payment_plan->insurance->calculate($user->reputation);
                //4
                $interest_percentage  = $payment_plan->interest->calculate($user->reputation);
                //5
                $daily_interest_percentage = $payment_plan->interest->dailyInterestForLatePayment($user->reputation);
                //6
                $grace_days = $payment_plan->interest->gracePeriod($user->reputation);
                if($grace_days == null){
                    echo('Error 4851');
                    dd();
                }
                //7
                $return =array(
                    'interest_percentage'       => $interest_percentage,
                    'insurance_percentage'      => $insurance_percentage,//array asociativo con 'debtor' y 'creditor'
                    'wallet'                    => $wallet,
                    'grace_days'                => $grace_days,
                    'daily_interest_percentage' => $daily_interest_percentage ,
                    'payments_number'           => $payment_plan->payments_number,
                    'commission_percentage'     => $payment_plan->interest->commission_percentage,
                    'due_dates_interval'        => CustomDateInterval::find($payment_plan->due_dates_interval_id),
                );
                return $return;
            }
        }
        return null;
     }
    /**
     * NOTA Este metodo sera llamado desde el front end
     * Crea un crédito siguiendo los siguientes pasos:
     *     1: Verifica que todo este bien.
     *     2: Simula el credito.
     *     3: Verifica si el resultano no es nulo, si es nulo, cancela.
     *     4: Copia los resultados de la funcion en variables.
     *     5: Crear el credito
     *     6: Crear la fase de credito
     *     7: Descontar del margen de usuario
     *     8: Envia un mensaje a creditRequest para generar el comprobante
     * @param User $user
     * @param PaymentPlan $payment_plan
     * @param float $amount
     * @return CreditPhase
     */
    
    public static function createCredit(User $user, PaymentPlan $payment_plan, $amount, Carbon $date_time = null){
        if($date_time == null){
            $date_time = Carbon::now();
        }else{
            if (Carbon::now()->diffInSeconds($date_time) > 180) {
                echo($date_time.' Credit::createCredit(..)
                ');
            }
        }
        DB::beginTransaction();
        //1
        if ( ($user != null) && ($payment_plan != null)){
            if($amount >= $payment_plan->currency->min_value){
                if(ExchangeRate::localConvert('USD',$payment_plan->currency->iso_code,$user->userMargin($date_time)) >= $amount){
                    if($user->activeCredits()->count() < $user->reputation->activeCreditsLimit($date_time)){
                        //2
                        $return = Credit::simutalteCredit($user,$payment_plan,$date_time);
                        //3
                        if($return == null){
                            DB::rollback();
                            return null;
                        }
                        //4
                        $interest_percentage       = $return['interest_percentage'];
                        $insurance_percentage      = $return['insurance_percentage'];
                        $wallet                    = $return['wallet'];
                        $grace_days                = $return['grace_days'];
                        $daily_interest_percentage = $return['daily_interest_percentage'];
                        $payments_number           = $return['payments_number'];
                        $commission_percentage     = $return['commission_percentage'];
                        $due_dates_interval        = $return['due_dates_interval'];
                        $user_id = null;
                        if(auth()->user() != null){
                            $user_id = auth()->user()->id;
                        }
                        //5
                        $credit = Credit::create([
                            'amount'                       => $amount,
                            'interest_percentage'          => $interest_percentage,
                            'commission_percentage'        => $commission_percentage,
                            'creditor_insurance_percentage'=> $insurance_percentage['creditor'],
                            'debtor_insurance_percentage'  => $insurance_percentage['debtor'],
                            'payments_number'              => $payments_number,
                            'wallet_id'                    => $wallet->id,
                            'grace_period_id'              => $grace_days->id,
                            'interest_for_late_payment'    => $daily_interest_percentage,
                            'due_dates_interval_id'        => $due_dates_interval->id,
                            'user_id'                      => $user_id,
                        ]);
                        $credit->refresh();
                        //6
                        $credit_phase = CreditPhase::create([
                            'credit_id'       => $credit->id,
                            'credit_stage_id' => (CreditStage::getFirst())->id,
                            'date'            => $date_time,
                            'user_id'         => $user_id,
                        ]);
                        $credit_phase->refresh();
                        $credit->refresh();
                        //7
                        if($wallet->user->reputation->retent($credit, $date_time) == null){
                            DB::rollback();
                            echo('Error 7484. No se puede retener el margen');
                            dd();
                        }
                        //8: Envia un mensaje a creditRequest para generar el comprobante
                        if(Voucher::creditRequest($credit_phase,$date_time) == null){
                            DB::rollback();
                            echo('Error 347925058');
                            dd();
                        }
                        DB::commit();
                        return $credit_phase;
                    }
                    DB::rollback();
                    echo('Error 9125');
                    dd();
                }
                DB::rollback();
                echo('Error 2347');
                dd();
            }
            DB::rollback();
            echo('Error 345357');
            dd();
        }
        DB::rollback();
        echo('Error 02505');
        dd();
        return null;
     }
    /**
     * NOTA Este metodo sera llamado desde el front end
     * Actualiza el estado de un crédito a Aprovado
     * Como lo hace
     *     1 Verifica se puede aprovar
     *     2 Crea la etapa "Financiando"
     * @return CreditPhase
     */
    public function approvate(Carbon $date_time = null){
        if($date_time == null){
            $date_time = Carbon::now();
        }else{
            if (Carbon::now()->diffInSeconds($date_time) > 180) {
                echo($date_time.' $credit->approvate(..)
                ');
            }
        }
        DB::beginTransaction();
        if($this->nextCreditStages()->pluck('id')->contains(CreditStage::getFinancing()->id)){
            $user_id = null;
            if(auth()->user() != null){
                $user_id = auth()->user()->id;
            }
            $credit_phase = CreditPhase::create([
                'credit_id'       => $this->id,
                'credit_stage_id' => (CreditStage::getFinancing())->id,
                'date'            => $date_time,
                'user_id'         => $user_id,
            ]);
            $credit_phase->refresh();
            $this->refresh();
            DB::commit();
            return $credit_phase;
        }
        DB::rollback();
        echo('Error 25500'.$this->nextCreditStages()->pluck('id','name'));
        return null;
        return null;
     }
    /**
     * NOTA: Se llama desde el metodo financiate(...) de Financing. No se encarga de financiar créditos, solamente de actualizar el estado
     * Actualiza la etapa del crédito a "Financiacion Finalizada"
     * Como:
     *     1: Verifica si la etapa es correcta y si el monto financiado es igual al monto del credito
     *     2: Crea la fase de financiacion
     *     3: Obtiene los financiamientos y los recorre
     *     4: A cada financiamiento envia la orden financingClose()
     * @param Carbon $date_time
     * @return CreditPhase
     */
    public function endFinancing(Carbon $date_time){
        //1
        if($this->nextCreditStages()->pluck('id')->contains(CreditStage::getFinanced()->id)){
            if($this->financedAmount() >= $this->amount){
                if(auth()->user() != null){
                    $user_id = auth()->user()->id;
                }else{
                    $user_id = null;
                }
                //2
                $credit_phase = CreditPhase::create([
                    'credit_id'       => $this->id,
                    'credit_stage_id' => CreditStage::getFinanced()->id,
                    'date'            => $date_time,
                    'user_id'         => $user_id,
                ]);
                $this->refresh();
                //3
                $financings = $this->financings;
                foreach($financings as $financing){
                    if ($financing->isActive()) {
                        //4
                        if($financing->financingClose($date_time) == null){
                            echo('Error 84455');
                            dd();
                        }
                    }
                }
                $this->refresh();
                return $credit_phase;
            }
            echo('Error 74520');
            dd();
        }
        echo('Error 0504');
        dd();
        return null;
     }
    /**
     * NOTA: Este metodo es llamado desde el front end, cuando el deudor selecciona el dinero a su wallet
     * Actualiza el estado del creadito a "Otorgado"
     * Como:
     *      1: verifica si se puede otorgar.
     *      2: Crea la fase de financiamiento
     *      3: envia un mensaje a todos los financiamientos para acreditar el financiamiento
     *      4: envia un mensaje al wallet para crear la transaccion
     *      5: genera los pagos
     *      6: Llama al metodo good
     * @return CreditPhase
     */
    public function grant(Carbon $date_time = null){
        if($date_time == null){
            $date_time = Carbon::now();
        }else{
            if (Carbon::now()->diffInSeconds($date_time) > 180) {
                echo($date_time.' $credit->grant(..)
                ');
            }
        }
        DB::beginTransaction();
        //1
        if($this->nextCreditStages()->pluck('id')->contains(CreditStage::getGrant()->id)){
            if (($this->amount + $this->currency()->min_value) > $this->financedAmount()){
                if(auth()->user() != null){
                    $user_id = auth()->user()->id;
                }else{
                    $user_id = null;
                }
                //2
                $credit_phase = CreditPhase::create([
                    'credit_id'       => $this->id,
                    'credit_stage_id' => CreditStage::getGrant()->id,
                    'date'            => $date_time,
                    'user_id'         => $user_id,
                ]);
                $credit_phase->refresh();
                $this->refresh();
                //3
                $financings = $this->financings;
                foreach ($financings as $financing){
                    if($financing->isActive()){
                        if($financing->accredit($date_time) == null){
                            DB::rollback();
                            echo('Error 8711');
                            dd();
                        }
                    }
                }
                //4
                if($this->wallet->grantCredit($credit_phase,$date_time) == null){
                    DB::rollback();
                    echo('Error 8448');
                    dd();
                }
                //5
                $payments_date = new Carbon($date_time->toDateTimeString());
                if(Payment::generate($this,$payments_date) == null){
                    DB::rollback();
                    echo('Error 1024');
                    dd();
                }
                //6
                $credit_phase = $this->good($date_time);
                if($credit_phase == null){
                    DB::rollback();
                    echo(' Error 3423458');
                    return null;
                }
                DB::commit();
                return $credit_phase;
            }
            DB::rollback();
            echo('Error 928820');
            dd();
        }
        DB::rollback();
        echo('Error 8947');
        dd();
        return null;
     }
    /**
     * NOTA: esta funcion puede ser llamada desde el metodo payed() de Payment o desde el metodo grant de $this
     * Actualiza el estado del crédito a "Al dia"
     * Como
     *     1 Verifica si puede pasar al estado de good
     *     2 Obtiene la siguiente cuota a pagar
     *     3 Controla si hay cuota por pagar
     *     4 Verifica si esta vencita
     *          4.1 Si es así, retorna null
     *     5 Cambia el estado del credito
     *     6 Libera el margen (el metodo controla si hay algun otro creido atrasado o si en verdad hay que liberar)
     * @param Carbon $date_time
     * @return CreditPhase
     */
    public function good(Carbon $date_time){

        //1 Verifica si puede pasar al estado de good
        if($this->nextCreditStages()->pluck('id')->contains(CreditStage::getGood()->id)){
            //2 Obtiene la siguiente cuota a pagar
            $unpayed_payment = $this->firstUnpayedPayment($date_time);
            //3 Controla si hay cuota por pagar
            if ($unpayed_payment != null) {
                //4 Verifica si esta vencita
                if ($unpayed_payment->hasDue()) {
                    //4.1 Si es así, retorna null
                    echo('Error 745884125');
                    dd();
                    return null;
                }
            }            
            if(auth()->user() != null){
                $user_id = auth()->user()->id;
            }else{
                $user_id = null;
            }
            //5 Cambia el estado del credito
            $credit_phase = CreditPhase::create([
                'credit_id'       => $this->id,
                'credit_stage_id' => CreditStage::getGood()->id,
                'date'            => $date_time,
                'user_id'         => $user_id,
            ]);
            $credit_phase->refresh();
            $this->refresh();
            //6 Libera el margen (el metodo controla si hay algun otro creido atrasado o si en verdad hay que liberar)
            if($this->user()->reputation->slowPayer($date_time) == null){
                echo('Error 81520');
                dd();
            }
            return $credit_phase;
        }
        return $this->lastCreditPhase();
     }
    /**
     * NOTA: Este metodo se ejecuta desde paymentDue de Payment, cuando detecta que un pago esta vencido
     * Actualiza el estado del crédito a "Pago retrasado"
     * Como:
     *     1 Verifica si el siguiente estado puede ser pago retrasado (En el caso d e que haya pagos retrasados o de que este en una fase mayor, como ser moroso o con litigio)
     *     2 Obtiene el procimo pago a pagar.
     *     3 Verifica si el pago esta vencido
     *         3.1 Si es asi, cambia el estado del credito
     *         3.2 Envia un mensaje a reputacion para bloquear los fondos
     * @param Carbon $date_time
     * @return CreditPhase
     */
    public function latePayment(Carbon $date_time){
        //1
        if(!$this->nextCreditStages()->pluck('id')->contains(CreditStage::getPaymentLate()->id))
            return null;
        //2
        $payment = $this->firstUnpayedPayment($date_time);
        //3
        if (($payment->hasDue()) && (($payment->lastPaymentPhase()->paymentStage->id != PaymentStage::getPayed()->id))) {
            if(auth()->user() != null){
                $user_id = auth()->user()->id;
            }else{
                $user_id = null;
            }
            //3.1
            $credit_phase = CreditPhase::create([
                'credit_id'       => $this->id,
                'credit_stage_id' => CreditStage::getPaymentLate()->id,
                'date'            => $date_time,
                'user_id'         => $user_id,
            ]);
            $credit_phase->refresh();
            $this->refresh();
            //3.2
            if($this->user()->reputation->slowPayer($date_time) == null){
                echo('Error 425441');
                dd();
            }
            return $credit_phase;
        }
        echo('Error 852166 '.$payment->lastPaymentPhase()->paymentStage->name.' '.$payment->hasDue()."pago ".$payment->id."crédito ".$this->id);
        dd();
        return null;
     }   
    /**
     * NOTA: Este metodo solamente debe ser llamado desde el metodo paymentsInterests() de Payment
     * Actualiza el estado del crédito a "Moroso"
     * Como lo hace
     *      1 Verifica si se puede pasar de estado a due
     *      2 genera el estado
     *      3 Retorna el CreditPhase
     * @param Carbon $date_time
     * @return CreditPhase
     */
    public function defaulterCredit(Carbon $date_time){
        //1
        if($this->nextCreditStages()->pluck('id')->contains(CreditStage::getDefaulter()->id)){
            if(auth()->user() != null){
                $user_id = auth()->user()->id;
            }else{
                $user_id = null;
            }
            //2
            $credit_phase = CreditPhase::create([
                'credit_id'       => $this->id,
                'credit_stage_id' => CreditStage::getDefaulter()->id,
                'date'            => $date_time,
                'user_id'         => $user_id,
            ]);
            $credit_phase->refresh();
            $this->refresh();
            //3
            return $credit_phase;
        }
        return null;
     }
    /**
     * NOTA Este metodo solamente debe ser llamado desde el metodo paymentsInterests() de Payment y desde el front end
     * Actualiza el estado del crédito a "Citado"
     * Como
     *  1 Verifica si se puede pasar al estado
     *      2 genera el estado
     *      3 Retorna el CreditPhase
     * @param Carbon $date_time
     * @return CreditPhase
     */
    public function citation(Carbon $date_time = null){
        if($date_time == null){
            $date_time = Carbon::now();
        }else{
            if (Carbon::now()->diffInSeconds($date_time) > 180) {
                echo($date_time.' $credit->citation(..)
                ');
            }
        }
        //1
        if($this->nextCreditStages()->pluck('id')->contains(CreditStage::getJudicialCitation()->id)){
            if(auth()->user() != null){
                $user_id = auth()->user()->id;
            }else{
                $user_id = null;
            }
            //2
            $credit_phase = CreditPhase::create([
                'credit_id'       => $this->id,
                'credit_stage_id' => CreditStage::getJudicialCitation()->id,
                'date'            => $date_time,
                'user_id'         => $user_id,
            ]);
            $credit_phase->refresh();
            $this->refresh();
            //3
            return $credit_phase;
        }
        echo('Error 8874552148 '.$this->lastCreditPhase()->creditStage->name);
        dd();
        return null;
     }
    /**
     * NOTA Este metodo debe ser llamado solamente desde el controller (Se supone que desde el rol de abogado)
     * Actualiza el estado del crédito a juicio
     * Como
     *      1 Verifica si se puede pasar a ese estado
     *      2 Verifica si todas las cuotas tengan en su ultimo estado, o bien pagado, o intereses
     *      3 Genera el estado
     *      4 devuelve el CreditPhase
     * @return CreditPhase
     */
    public function lawsuit(Carbon $date_time = null){
        if($date_time == null){
            $date_time = Carbon::now();
        }else{
            if (Carbon::now()->diffInSeconds($date_time) > 180) {            
                echo($date_time.' $credit->lawsuit(..)
                ');
            }
        }
        //1
        if($this->nextCreditStages()->pluck('id')->contains(CreditStage::getLawsuit()->id)){
            //2
            $payments = $this->payments;
            $stages = new Collection();
            $stages->add(PaymentStage::getPayed());
            $stages->add(PaymentStage::getPay());
            $stages->add(PaymentStage::getAddInterest());
            $stages->add(PaymentStage::getGenerated());
            foreach($payments as $payment){
                if(!$stages->pluck('id')->contains($payment->lastPaymentPhase()->paymentStage->id)){
                    echo('Error 82586580 '.$payment->id.' '.$payment->lastPaymentPhase()->paymentStage->name);
                    dd();
                    return null;
                }
            }
            if(auth()->user() != null){
                $user_id = auth()->user()->id;
            }else{
                $user_id = null;
            }
            //3
            $credit_phase = CreditPhase::create([
                'credit_id'       => $this->id,
                'credit_stage_id' => CreditStage::getLawsuit()->id,
                'date'            => $date_time,
                'user_id'         => $user_id,
            ]);
            $credit_phase->refresh();
            $this->refresh();
            //4
            return $credit_phase;
        }
        echo('Error 06541564');
        dd();
        return null;
     }
    /**
     * NOTA Este metodo debe invocarse solamente desde el metodo payed() de Payment
     * Actualiza el estado del crédito a 'Saldado'
     * Como
     *     1 Verifica si puede pasar a ese estado.
     *     2 Verifica que todas las cuotas esten pagadas.
     *     3 Crea la etapa del credito.
     *     4 envia un mensaje a la reputacion del usuario para que libere el margen del credito
     * @param Carbon $date_time
     * @return CreditPhase
     */
    public function liquidate(Carbon $date_time){
        if($this->nextCreditStages()->pluck('id')->contains(CreditStage::getLiquidate()->id)){
            //2
            $payments = $this->payments;
            foreach($payments as $payment){
                if($payment->lastPaymentPhase()->paymentStage->id != PaymentStage::getPayed()->id){
                    echo('Error 23456435');
                    dd();
                    return null;
                }
            }
            if(auth()->user() != null){
                $user_id = auth()->user()->id;
            }else{
                $user_id = null;
            }
            //3
            $credit_phase = CreditPhase::create([
                'credit_id'       => $this->id,
                'credit_stage_id' => CreditStage::getLiquidate()->id,
                'date'            => $date_time,
                'user_id'         => $user_id,
            ]);
            $credit_phase->refresh();
            $this->refresh();
            //4
            if($this->user()->reputation->liberate($this, $date_time) != null){
                return $credit_phase;
            }
            echo('Error 774158852');
            dd();
        }
        echo('Error 8524552');
        dd();
        return null;
     }
    /**
     * NOTA: Este es un metodo invocado directamente desde el front end
     * Permite cancelar un credito, desencadenando las acciones correspondientes
     * Como
     *      1 Verifica si se puede cancelar
     *      2 Cancela el credito
     *      3 Verifica si tiene financiamientos
     *          4 Si es asi, recorre los financiamientos, cancelandolos.
     *      5 Libera el margen
     *      6 Envia un mensaje a abortCredit() para generar el comprobante
     * @return CreditPhase
     */
    public function abort(Carbon $date_time = null){
        if($date_time == null){
            $date_time = Carbon::now();
        }else{
            if (Carbon::now()->diffInSeconds($date_time) > 180) {
                echo($date_time.' $credit->abort(..)
                ');
            }
        }
        DB::beginTransaction();
        //1
        if ($this->nextCreditStages()->pluck('id')->contains(CreditStage::getAbort()->id)){
            if(auth()->user() != null){
                $user_id = auth()->user()->id;
            }else{
                $user_id = null;
            }
            //2
            $credit_phase = CreditPhase::create([
                'credit_id'       => $this->id,
                'credit_stage_id' => CreditStage::getAbort()->id,
                'date'            => $date_time,
                'user_id'         => $user_id,
            ]);
            $credit_phase->refresh();
            $this->refresh();
            //3
            $financings = $this->financings;
            //4
            foreach ($financings as $financing){
                if($financing->isActive()){
                    if($financing->abortCredit($date_time) == null){
                        DB::rollback();
                        echo('Error 82014');
                        dd();
                    }         
                }
            }
            //5
            if($this->user()->reputation->liberate($this, $date_time) == null){
                DB::rollback();
                echo('Error 52156 Al liberar el credito');
                dd();
            }
            //6
            if (Voucher::abortCredit($credit_phase,$date_time) == null) {
                echo('Error 71005862114');
                dd();
            }
            DB::commit();
            return $credit_phase;
        }
        DB::rollback();
        return null;
     }
    /**
     * NOTA: Este es un metodo invocado directamente desde el front end
     * Permite cancelar un credito, desencadenando las acciones correspondientes
     * Como
     *      1 Verifica si se puede rechazar
     *      2 Rechaza el credito
     *      3 Libera el margen
     *      4 Envia un mensaje a rejectCredit para generar el comprobante
     * Nota: aca no se libera el margen pirque solamente se puede rechazar antes del financiamiento;
     * @return CreditPhase
     */
    public function reject(Carbon $date_time = null){
        if($date_time == null){
            $date_time = Carbon::now();
        }else{
            if (Carbon::now()->diffInSeconds($date_time) > 180) {
                echo($date_time.' $credit->reject(..)
                ');
            }
        }
        DB::beginTransaction();
        //1
        if ($this->nextCreditStages()->pluck('id')->contains(CreditStage::getReject()->id)){
            if(auth()->user() != null){
                $user_id = auth()->user()->id;
            }else{
                $user_id = null;
            }
            //2
            $credit_phase = CreditPhase::create([
                'credit_id'       => $this->id,
                'credit_stage_id' => CreditStage::getReject()->id,
                'date'            => $date_time,
                'user_id'         => $user_id,
            ]);
            $credit_phase->refresh();
            $this->refresh();
            //3
            if($this->user()->reputation->liberate($this, $date_time) == null){
                DB::rollback();
                echo('Error 4585210');
                dd();
            }
            //4
            if(Voucher::rejectCredit($credit_phase,$date_time) == null){
                DB::rollback();
                echo('Error 93452185');
                dd();
            }
            DB::commit();
            return $credit_phase;
        }
        DB::rollback();
        return null;
     }
    /**
     * NOTA: Este es un metodo invocado directamente desde el front end
     * Cambia la fase de un crédito a 'Incobrable'
     * Como
     *      1 Verifica si se puede cambiar de estado (osea esta en litigio)
     *      2 Cambia estado del credito
     *      3 Recorre los pagos llamando a bad
     *      4 Recorre los financiamientos activos llamando a bad
     * @return CreditPhase
     */
    public function bad(Carbon $date_time = null){
        if($date_time == null){
            $date_time = Carbon::now();
        }else{
            if (Carbon::now()->diffInSeconds($date_time) > 180) {
                echo($date_time.' $credit->bad(..)
                ');
            }
        }
        //1
        if ($this->nextCreditStages()->pluck('id')->contains(CreditStage::getBad()->id)){
            if(auth()->user() != null){
                $user_id = auth()->user()->id;
            }else{
                $user_id = null;
            }
            //2
            $credit_phase = CreditPhase::create([
                'credit_id'       => $this->id,
                'credit_stage_id' => CreditStage::getBad()->id,
                'date'            => $date_time,
                'user_id'         => $user_id,
            ]);
            $credit_phase->refresh();
            $this->refresh();
            //3
            $payments = $this->payments;
            foreach ($payments as $payment) {
                if ($payment->bad($date_time) == null) {
                    echo('Error 7102');
                    dd();
                }
            }
            //4
            $financings = $this->financings;
            foreach($financings as $financing){
                if ($financing->isActive()) {
                    if(!$financing->have_insurance){
                        if($financing->bad($date_time) == null){
                            echo('Error 1545685');
                            dd();
                        }
                    }
                }
            }
            return $credit_phase;
        }
        return null;

     }
    /**
     * NOTA Este metodo debe ser llamado desde el metodo abortFinancing() de Financing
     * Vuelve a poner un crédito en fase de financiamiento. Esto sucede cuando estaba en fase de "Financiacion Finalizada", pero se cancelo un financiamiento
     * Como
     *      1 Verifica si el estado actual del crédito es "Financiacion Finalizada"
     *      2 Verifica que el monto financiado sea menor al monto del credito
     *      3 Crea la nueva fase del credito
     *      4 Obtiene todos los finaciamientos
     *      5 Envia un mensaje a cada financiamiento activo para que se reabra
     *      6 Llama al metodo approvate() para que cambie el estado a 'Financiando'
     * @param Carbon $date_time
     * @return CreditPhase
     */
    public function reopened(Carbon $date_time){
        //1
        if($this->nextCreditStages()->pluck('id')->contains(CreditStage::getFinancingReopened()->id)){
            //2
            if(Number::fix($this->amount - $this->financedAmount()) >= $this->currency()->min_value){
                if(auth()->user() != null){
                    $user_id = auth()->user()->id;
                }else{
                    $user_id = null;
                }
                //3
                $credit_phase = CreditPhase::create([
                    'credit_id'       => $this->id,
                    'credit_stage_id' => CreditStage::getFinancingReopened()->id,
                    'date'            => $date_time,
                    'user_id'         => $user_id,
                ]);
                $credit_phase->refresh();
                $this->refresh();
                //4
                $financings = $this->financings;
                foreach ($financings as $financing){
                    if ($financing->isActive()) {
                        //5
                        if ($financing->financingReopened($date_time) == null) {
                            echo('Error 8732411');
                            dd();
                        }
                    }
                }
                //6
                if($this->approvate($date_time) == null){
                    echo('Error 8221');
                    dd();
                }
                return $credit_phase;
            }
            echo('Error 346870 '.$this->amount.' '.$this->financedAmount());
            dd();
        }
        echo('Error 075365 '.$this->nextCreditStages()->pluck('id','name'));
        dd();
        return null;
     }
}
