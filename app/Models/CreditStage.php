<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;

class CreditStage extends Model
{
    protected $table = 'credit_stages';
    protected $fillable= [
        'name'       ,
        'description',
    ];
    public $timestamps = false;
    public function creditPhases()
    {
        return $this->hasMany('App\Models\CreditPhase');
    }
    public function creditStageNexts()
    {
        return $this->hasMany('App\Models\CreditStageNext');
    }

    public function nextCreditStages():Collection{
        $next_credit_stages = $this->creditStageNexts;
        $return = new Collection();
        foreach ($next_credit_stages as $credit_stage_next){
            $return->add($credit_stage_next->creditStageNext);
        }
        return $return;
    }
    /*
        1  => 'Solicitado' >>>>>>>>>>>>>>>>>> ::getFirst()
        2  => 'Financiando' >>>>>>>>>>>>>>>>> ::getFinancing()
        3  => 'Financiacion Finalizada' >>>>> ::getFinanced()
        4  => 'Otorgado' >>>>>>>>>>>>>>>>>>>> ::getGrant()
        5  => 'Al dia' >>>>>>>>>>>>>>>>>>>>>> ::getGood()
        6  => 'Pago retrasado' >>>>>>>>>>>>>> ::getPaymentLate()
        7  => 'Mora' >>>>>>>>>>>>>>>>>>>>>>>> ::getDefaulter()
        8  => 'Citado' >>>>>>>>>>>>>>>>>>>>>> ::getJudicialCitation()
        9  => 'Litigio' >>>>>>>>>>>>>>>>>>>>> ::getLawsuit()
        10 => 'Saldado' >>>>>>>>>>>>>>>>>>>>> ::getLiquidate()
        11 => 'Cancelado' >>>>>>>>>>>>>>>>>>> ::getAbort()
        12 => 'Rechazado' >>>>>>>>>>>>>>>>>>> ::getReject()
        13 => 'Incobrable' >>>>>>>>>>>>>>>>>> ::getBad()
        14 >> 'Financiacion rehabierta'>>>>>> ::getFinancingReopened()
    */
    public static function getFirst(){
        return CreditStage::find(1);
    }
    public static function getFinancing(){
        return CreditStage::find(2);
    }
    public static function getFinanced(){
        return CreditStage::find(3);
    }
    public static function getGrant(){
        return CreditStage::find(4);
    }
    public static function getGood(){
        return CreditStage::find(5);
    }
    public static function getPaymentLate(){
        return CreditStage::find(6);
    }
    public static function getDefaulter(){
        return CreditStage::find(7);
    }
    public static function getJudicialCitation(){
        return CreditStage::find(8);
    }
    public static function getLawsuit(){
        return CreditStage::find(9);
    }
    public static function getLiquidate(){
        return CreditStage::find(10);
    }
    public static function getAbort(){
        return CreditStage::find(11);
    }
    public static function getReject(){
        return CreditStage::find(12);
    }
    public static function getBad(){
        return CreditStage::find(13);
    }
    public static function getFinancingReopened(){
        return CreditStage::find(14);
    }
}
