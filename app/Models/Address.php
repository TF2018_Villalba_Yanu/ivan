<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $table = 'addresses';
    protected $fillable = [
        'street'    ,
        'number'    ,
        'department',
        'floor'     ,
        'city_id'   ,//can be null
    ];
    public $timestamps = false;
    public function city(){
        return $this->belongsTo('App\Models\City');
    }
    public function users(){
        return $this->hasMany('App\Models\User');
    }
    public function currency(){
        return $this->city->district->country->currency;
    }
}
