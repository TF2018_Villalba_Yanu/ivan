<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Exchange extends Model
{
    protected $fillable = [
        'from_wallet_id'  ,
        'to_wallet_id'    ,
        'exchange_rate_id',
        'date'            ,
        'from_amount'     ,
        'user_id'         ,//nullable. Usado para la auditoria
    ];
    protected $dates = [
        'date',
    ];
    public $timestamps = false;
    public function fromWallet(){
        return $this->belongsTo('App\Models\Wallet','from_wallet_id', 'id');
    }
    public function toWallet(){
        return $this->belongsTo('App\Models\Wallet','to_wallet_id', 'id');
    }
    public function exchangeRate(){
        return $this->belongsTo('App\Models\ExchangeRate');
    }
    public function convert($reverse){
        if($reverse){
            return $this->exchangeRate->convert($this->from_amount,$reverse);
        }else{
            return $this->from_amount;
        }
    }
    /**
     * NOTA: Este metodo es llamado desde el front end
     * Crea una conversion de divisas
     *  1 Verifica que el monto sea mayor o igual al monto minimo de la divisa.
     *  2 Verifica si $from_wallet tiene los fondos.
     *  3 Verifica si la divisa de destino es diferente a la divisa de $from_wallet
     *  4 Verifica si el usuario posee un wallet con la divisa de destino.
     *      4.1 Si no posee, crea uno.
     *  5 Crea el objeto exchange
     *  6 Envia un mensaje a $from_wallet para que registre la transaccion.
     *  7 Envia un mensaje a $to_wallet para que registre la transaccion.
     *  8 Retorna el $exchange.
     * @param Wallet $from_wallet
     * @param Currency $to_currency
     * @param $from_amount
     * @param ExchangeRate $exchange_rate = null 
     * @param Carbon $date_time = null
     * @return Exchange
     */
    public static function exchange(Wallet $from_wallet, Currency $to_currency, $from_amount, ExchangeRate $exchange_rate = null, Carbon $date_time = null){
        if($date_time == null){
            $date_time = Carbon::now();
        }else{
            if (Carbon::now()->diffInSeconds($date_time) > 180) {
                echo($date_time.' Exchange::exchange(..)
                ');
            }
        }
        if(($exchange_rate == null) || ($exchange_rate->date->diffInSeconds($date_time) > 60) || ($exchange_rate->fromCurrency->id != $from_wallet->currency->id) || ($exchange_rate->toCurrency->id != $to_currency->id)){
            $exchange_rate = ExchangeRate::lastExchangeRate($from_wallet->currency,$to_currency);
        }
        //1 Verifica que el monto sea mayor o igual al monto minimo de la divisa.
        if(($from_amount >= $from_wallet->currency->min_value) || ($exchange_rate->convert($from_amount,0) >= $to_currency->min_value)){
            //2 Verifica si $from_wallet tiene los fondos.
            if($from_wallet->accountBalance() >= $from_amount){
                //3 Verifica si la divisa de destino es diferente a la divisa de $from_wallet
                if($to_currency->id != $from_wallet->currency->id){
                    //4 Verifica si el usuario posee un wallet con la divisa de destino.
                    $to_wallet = Wallet::where('user_id',$from_wallet->user->id)->where('currency_id',$to_currency->id)->first();
                    if($to_wallet == null){
                        //4.1 Si no posee, crea uno.
                        $to_wallet = Wallet::create([
                            'user_id'     => $from_wallet->user->id,              
                            'currency_id' => $to_currency->id,
                        ]);
                        $from_wallet->user->refresh();
                        $to_currency->refresh();
                    }
                    //5 Crea el objeto exchange
                    if(auth()->user() != null){
                        $user_id = auth()->user()->id;
                    }else{
                        $user_id = null;
                    }
                    $exchange = Exchange::create([
                        'user_id'          => $user_id,//Usado para auditoria
                        'from_wallet_id'   => $from_wallet->id,
                        'to_wallet_id'     => $to_wallet->id,
                        'exchange_rate_id' => $exchange_rate->id,
                        'date'             => $date_time,
                        'from_amount'      => $from_amount,
                    ]);
                    $from_wallet->refresh();
                    $to_wallet->refresh();
                    //6 Envia un mensaje a $from_wallet para que registre la transaccion.
                    if($from_wallet->exchangeOutput($exchange,$date_time) == null){
                        echo('Error 3115521455');
                        dd();
                    }
                    //7 Envia un mensaje a $to_wallet para que registre la transaccion.
                    if($to_wallet->exchangeInput($exchange,$date_time) == null){
                        echo('Error 14275544');
                        dd();
                    }
                    //8 Retorna el $exchange.
                    return $exchange;
                }
                echo('Error 9111004552');
                dd();
            }
            echo('Error 0254236633');
            dd();
        }
        echo('Error 545326322 '.$from_amount.' '.$from_wallet);
        dd();
    }
}
