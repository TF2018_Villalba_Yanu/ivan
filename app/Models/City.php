<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $table = 'cities';
    protected $fillable = [
        'name'       ,
        'zip_code'   ,
        'district_id',
    ];
    public $timestamps = false;
    
    public function district(){
        return $this->belongsTo('App\Models\District');
    }
    public function addresses(){
        return $this->hasMany('App\Models\Address');
    }
}
