<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MarginModifierStage extends Model
{
    protected $table = 'margin_modifier_stages';
    protected $fillable= [
        'name'       ,
        'description',
        'type'       ,
    ];
    public $timestamps = false;
    public function marginModifier()
    {
        return $this->hasMany('App\Models\MarginModifier');
    }
    /*
        01 => 'Categoria actualizada'---------------------->::getBaseAmountEstablished()
        02 => 'Categoria dada de baja'--------------------->::getBaseAmountDeleted()
        03 => 'Retencion por credito'---------------------->::getRetentionForCredit()
        04 => 'Liberacion por credito'--------------------->::getLiberationForCredit()
        05 => 'Retencion por irregularidad en pagos'------->::getRetentionForDelayedPayment()
        06 => 'Liberacion por regularizar pagos'----------->::getLiberationForDelayedPayment()
        07 => 'Disminucion del margen por comportamiento'-->::getDecreasePerformanceInPayments()
        08 => 'Aumento del margen por comportamiento'------>::getIncreasePerformanceInPayments()
        09 => 'Disminucion del margen por inflacion'------->::getDecreaseForInflation()
        10 => 'Aumento del margen por deflacion'----------->::getIncreaseForDeflation()
        11 => 'Incremento del margen manualmente'---------->::getManualIncrease()
    */
    public static function getBaseAmountEstablished(){
        return MarginModifierStage::find(1);
    }
    public static function getBaseAmountDeleted(){
        return MarginModifierStage::find(2);
    }
    public static function getRetentionForCredit(){
        return MarginModifierStage::find(3);
    }
    public static function getLiberationForCredit(){
        return MarginModifierStage::find(4);
    }
    public static function getRetentionForDelayedPayment(){
        return MarginModifierStage::find(5);
    }
    public static function getLiberationForDelayedPayment(){
        return MarginModifierStage::find(6);
    }
    public static function getDecreasePerformanceInPayments(){
        return MarginModifierStage::find(7);
    }
    public static function getIncreasePerformanceInPayments(){
        return MarginModifierStage::find(8);
    }
    public static function getDecreaseForInflation(){
        return MarginModifierStage::find(9);
    }
    public static function getIncreaseForDeflation(){
        return MarginModifierStage::find(10);
    }
    public static function getManualIncrease(){
        return MarginModifierStage::find(11);
    }

}
