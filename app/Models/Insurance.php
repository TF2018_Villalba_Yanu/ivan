<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Insurance extends Model
{
    protected $table = 'insurances';
    protected $fillable = [
        'initial_percentage',
        'min_percentage'    ,
        'max_percentage'    ,
        'debtor_percentage' ,// debtor_percentage = $whatever_insurance_percentage * $this->debtor_percentage
        'user_id'           ,//nullable. Usado para la auditoria
    ];
    //ALWAYS: $this->debtor_percentage <= 1
    public $timestamps = false;
    public function calculate(Reputation $reputation){
        $insurance_percentage = $this->initial_percentage + InsuranceModifier::percentage($reputation);

        if($insurance_percentage > $this->max_percentage)
            $insurance_percentage =$this->max_percentage;

        if($insurance_percentage < $this->min_percentage)
            $insurance_percentage =$this->min_percentage;
       
        $insurance_return = array(
            'percentage' => $insurance_percentage,
            'creditor'   => $insurance_percentage * (1 - $this->debtor_percentage), 
            'debtor'     => $insurance_percentage * $this->debtor_percentage,
        );
        
        return $insurance_return;
    }
}