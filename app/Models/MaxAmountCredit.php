<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MaxAmountCredit extends Model
{
    protected $table = 'max_amount_credits';
    protected $fillable = [
        'profession_id'       ,
        'currency_id'         ,
        'initial_limit'       ,//Montos en la moneda currency
        'max_limit'           ,
        'active_credits_limit',
        'user_id'             ,//nullable. Usado para la auditoria
    ];
    public $timestamps = false;
    
    public function profession()
    {
        return $this->belongsTo('App\Models\Profession');
    }
    public function currency()
    {
        return $this->belongsTo('App\Models\Currency');
    }
    public function initialLimitUSD(){
        return ExchangeRate::localConvert($this->currency->iso_code,'USD',$this->initial_limit);
    }
    public function maxLimitUSD(){
        return ExchangeRate::localConvert($this->currency->iso_code,'USD',$this->max_limit);
    }
}
