<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use Carbon\Carbon;

class Wallet extends Model
{
    protected $table = 'wallets';
    protected $fillable = [
        'user_id'                 ,
        'currency_id'             ,
    ];
    public $timestamps = false;

    public function user(){
        return $this->belongsTo('App\Models\User');
     }
    public function currency(){
        return $this->belongsTo('App\Models\Currency');
     }
    public function credits(){
        return $this->hasMany('App\Models\Credit');
     }
    public function walletTransactions(){
        return $this->hasMany('App\Models\WalletTransaction');
     }
    public function financings(){
        return $this->hasMany('App\Models\Financing');
     }
    public function formExchanges(){
        return $this->hasMany('App\Models\Exchange', 'from_wallet_id', 'id');
    }
    public function toExchanges(){
        return $this->hasMany('App\Models\Exchange', 'to_wallet_id', 'id');
    }
    public function hasCredit(Credit $credit){
        return $this->credits->pluck('id')->contains($credit->id);
     }
    public function hasFinancing(Financing $financing){
        return $this->financings->pluck('id')->contains($financing->id);
     }
    public function accountBalance(){
        $sum = 0;
        $wallet_transactions = $this->walletTransactions;
        foreach ($wallet_transactions as $wallet_transaction){
            $sum += $wallet_transaction->amount;
        }
        return $this->currency->truncate($sum);
    }
    public function financingsAmountReturned(){
        $amount = 0;
        $financings = $this->financings;
        foreach($financings as $financing){
            $amount += $financing->amountReturned();
        }
        return $amount;
     }
    public function financingsAmount(){
        $amount = 0;
        $financings = $this->financings;
        foreach($financings as $financing){
            $amount += $financing->amount();
        }
        return $amount;
     }
    public function creditsAmount(){
        $amount = 0;
        $credits = $this->credits;
        foreach($credits as $credit){
            $amount += $credit->amount;
        }
        return $amount;
     }
    public function activeCredits(){
        $active_credits = new Collection;
        $credits = $this->credits;
        foreach ($credits as $credit)
            if( $credit->isActive())
                $active_credits->add($credit);
        return $active_credits;
     }
     public function activeFinancings(){
        $active_financings = new Collection;
        $financings = $this->financings;
        foreach ($financings as $financing)
            if( $financing->isActive())
                $active_financings->add($financing);
        return $active_financings;
     }
    public function retainsMargin(){
        $credits = $this->credits;
        foreach ($credits as $credit) {
            if ($credit->retainsMargin()) {
                return true;
            }
        }
        return false;
    }
    public static function walletsToFinancing(){
        $wallets_to_financiate = new Collection();
        $wallets = auth()->user()->wallets;
        foreach($wallets as $wallet){
            if(($wallet->accountBalance() >= $wallet->currency->min_value) && ($wallet->currency->financingCredits()->take(1)->count() > 0)){
                $wallets_to_financiate->add($wallet);
            }
        }
        return $wallets_to_financiate;
    }
    public function creditsInStage(CreditStage $credit_stage){
        $items = new Collection();
        $credits = $this->credits;
        foreach ($credits as $credit) {
            if ($credit->lastCreditPhase()->creditStage->id == $credit_stage->id) {
                $items->add($credit);
            }
        }
        return $items;
    }
    public function financingsInStage(FinancingStage $financing_stage){
        $items = new Collection();
        $financings = $this->financings;
        foreach ($financings as $financing) {
            if ($financing->lastFinancingPhase()->financingStage->id == $financing_stage->id) {
                $items->add($financing);
            }
        }
        return $items;
    }
    public function hasFinancingDelayedReturn(){
        $financings = $this->financings;
        foreach ($financings as $financing) {
            if($financing->hasDelayedReturn()){
                return true;
            }
        }
        return false;
    }
    /*
        'Ingreso de dinero'-----------> ::getIncome()=================> ::inputMoney($user, $currency, $amount)
        'Retiro de dinero'------------> ::getWithdraw()===============> outputMoney($amount)
        'Efectivización del crédito'--> ::getGrantCredit()============> grantCredit($credit)
        'Financiamiento'--------------> ::getFinancing()==============> financiate($financing_phase)
        'Financiamiento cancelado'----> ::getFinancingAborted()=======> financingAbort($financing_phase)
        'Crédito cancelado'-----------> ::getCreditAborted()==========> cancelFinancingCredit($financing_phase)
        'Pago de cuota'---------------> ::getCreditPayment()==========> payCredit($credit, $amount)
        'Reintegro del crédito'-------> ::getCreditReturn()===========> returnForFinancing($repayment)
        'Reintegro asegurado'---------> ::getCreditInsuranceReturn()==> insuranceReturnForFinancing($repayment)
        'Abono del seguro acreedor'---> ::getInsurancePayment()=======> insuranceCreditorPayment($repayment)
        'Ingreso por conversión de divisas'--> ::getExchangeInput()===> exchangeInput($exchange)
        'Egreso por conversion de divisas'---> ::getExchangeOutput()==> exchangeOutnput($exchange)
        Metodo que se engarga de pagar comision y seguro deudor-------> payCommissionAndDebtorInsurance($payment_phase, $financing)
     */
    /**
     * NOTA Este metodo se llama desde el front
     * Registra el ingreso de dinero de un Wallet
     * Como
     *      1 Verifica si existe el wallet
     *          1.1 Si no, lo crea
     *      2 Modifica el account_balance del Wallet
     *      3 Crea la transaccion
     *      4 Llama a inputMoney() para crear el comprobante
     *      5 Retorna
     * @param User $user
     * @param Currency $currency
     * @param float $amount
     * @return WalletTransaction
     */
    public static function inputMoney(User $user, Currency $currency, $amount, Carbon $date_time = null){
        if($date_time == null){
            $date_time = Carbon::now();
        }else{
            if (Carbon::now()->diffInSeconds($date_time) > 180) {
                echo($date_time.' Wallet::inputMoney('.$user->id.', '.$currency->id.', '.$amount.')
                ');
            }
        }
        if ($user != null) {
            if ($currency != null) {
                if ($amount >= $currency->min_value){
                    //1
                    $wallet = Wallet::where('currency_id',$currency->id)
                        ->where('user_id',$user->id)
                        ->first();
                    if($wallet == null){
                        $wallet = Wallet::create([
                            'user_id'         => $user->id,              
                            'currency_id'     => $currency->id,
                        ]);
                        $wallet->refresh();
                    }
                    //2
                    //3
                    if(auth()->user() != null){
                        $user_id = auth()->user()->id;
                    }else{
                        $user_id = null;
                    }
                    $input_money_transaction = InputMoneyTransaction::create([
                        'user_id' => $user_id,
                    ]);
                    $wallet_transaction = WalletTransaction::create([
                        'user_id'                       => $user_id,
                        'wallet_id'                     => $wallet->id,
                        'wallet_type_of_transaction_id' => WalletTypeOfTransaction::getIncome()->id,
                        'amount'                        => Number::fix($amount),
                        'date'                          => $date_time,
                        'input_money_transaction_id'    => $input_money_transaction->id,
                    ]);
                    $wallet_transaction->refresh();
                    $wallet->refresh();
                    //4 Llama a inputMoney() para crear el comprobante
                    if (Voucher::inputMoney($wallet_transaction,$date_time) == null) {
                        echo('Error 71005862114');
                        dd();
                    }
                    //5
                    return $wallet_transaction;
                }
                echo('Error 85118962');
                dd();
            }
            echo('Error 029106');
            dd();
        }
        echo('Error 9688512');
        dd();
        return null;
     }
    /**
     * NOTA Este metodo se llama desde el front
     * Registra el retiro de dinero de un Wallet
     * Como
     *      1 Verifica que se puede hacer
     *          1.1 Si el monto a retirar es mayor al balance de la cuenta, disminuye el monto
     *      2 Modifica el account_balance del Wallet
     *      3 Crea la transaccion
     *      4 Llama a outputMoney() para crear el comprobante
     *      5 Retorna
     * @param float $amount
     * @return WalletTransaction
     */
    public function outputMoney($amount, Carbon $date_time = null){
        if($date_time == null){
            $date_time = Carbon::now();
        }else{
            if (Carbon::now()->diffInSeconds($date_time) > 180) {
                echo($date_time.' $wallet->outputMoney(..)
                ');
            }
        }
        //1
        if ($amount >= $this->currency->min_value) {
            //1.1
            if ($this->accountBalance() < $amount) {
                $amount = $this->accountBalance();
            }
            //2
            //3
            if(auth()->user() != null){
                $user_id = auth()->user()->id;
            }else{
                $user_id = null;
            }
            $output_money_transaction = OutputMoneyTransaction::create([
                'user_id' => $user_id,
            ]);
            $wallet_transaction = WalletTransaction::create([
                'user_id'                       => $user_id,
                'wallet_id'                     => $this->id,
                'wallet_type_of_transaction_id' => WalletTypeOfTransaction::getWithdraw()->id,
                'amount'                        => Number::fix($amount * -1),
                'date'                          => $date_time,
                'output_money_transaction_id'   => $output_money_transaction->id,
            ]);
            $wallet_transaction->refresh();
            $this->refresh();
            //4 Llama a outputMoney() para crear el comprobante
            if (Voucher::outputMoney($wallet_transaction,$date_time) == null) {
                echo('Error 71005862114');
                dd();
            }
            //5
            return $wallet_transaction;
        }
        echo('Error 795245');
        dd();
        return null;
     }
    /**
     * NOTA: Este metodo se invoca unicamente desde el metodo grant() de Credit
     * Registra el ingreso de dinero al wallet cuando se efectiviza un credito
     * Como lo hace
     *      1 Verifica que todo este bien
     *      2 Actualiza el balance de cuenta
     *      3 Crea la transaccion
     * @param CreditPhase $credit_phase
     * @param Carbon $date_time
     * @return WalletTransaction
     */
    public function grantCredit(CreditPhase $credit_phase, Carbon $date_time){
        //1
        if ($credit_phase != null){
            if ($this->hasCredit($credit_phase->credit)){
                if($credit_phase->creditStage->id == CreditStage::getGrant()->id){
                    if($credit_phase->credit->lastCreditPhase()->id == $credit_phase->id){
                        //2
                        if(auth()->user() != null){
                            $user_id = auth()->user()->id;
                        }else{
                            $user_id = null;
                        }
                        $wallet_transaction = WalletTransaction::create([
                            'user_id'                       => $user_id,
                            'wallet_id'                     => $this->id,
                            'wallet_type_of_transaction_id' => WalletTypeOfTransaction::getGrantCredit()->id,
                            'amount'                        => Number::fix($credit_phase->credit->amount),
                            'date'                          => $date_time,
                            'credit_phase_id'               => $credit_phase->id,
                        ]);
                        $wallet_transaction->refresh();
                        $this->refresh();
                        return $wallet_transaction;
                    }
                }
                echo('Error 8515215');
                dd();
            }
            echo('Error 8515215');
            dd();
        }
        echo('Error 8515215');
        dd();
        return null;
     }
    /**
     * NOTA: Este metodo debe ser llamando desde el metodo financiate de Financing
     * Registra la transaccion en el wallet del acreedor
     * Como lo hace
     *     1 Verfica los datos recibidos
     *     2 Descuenta el monto al wallet
     *     3 Crea la transaccion del wallet
     * @param FinancingPhase $financing_phase
     * @param Carbon $date_time
     * @return WalletTransaction
     */
    public function financiate(FinancingPhase $financing_phase, Carbon $date_time){
        //1
        if($financing_phase != null){
            if($financing_phase->financingStage->id == FinancingStage::getFinance()->id){
                if($financing_phase->financing->wallet->id == $this->id){
                    //2
                    //3
                    if(auth()->user() != null){
                        $user_id = auth()->user()->id;
                    }else{
                        $user_id = null;
                    }
                    $transaction = WalletTransaction::create([
                        'user_id'                       => $user_id,
                        'wallet_id'                     => $this->id,
                        'wallet_type_of_transaction_id' => WalletTypeOfTransaction::getFinancing()->id,
                        'amount'                        => Number::fix($financing_phase->amount * -1),
                        'date'                          => $date_time,
                        'financing_phase_id'            => $financing_phase->id,
                    ]);
                    $transaction->refresh();
                    $this->refresh();
                    return $transaction;
                }
                echo('Error 5220145');
                dd();
            }
            echo('Error 9621');
            dd();
        }
        echo('Error 04785256');
        dd();
        return null;
        
     }
    /**
     * NOTA: Este metodo es llamado desde el metodo abortFinancing de Financing
     * Registra la transaccion del wallet cuando se cancela un financiamiento
     * Como lo hace
     *     1 Verfica los datos recibidos
     *     2 Suma el monto al wallet
     *     3 Crea la transaccion del wallet
     * @param FinancingPhase $financing_pase
     * @param Carbon $date_time
     * @return WalletTransaction
     */
    public function financingAbort(FinancingPhase $financing_phase, Carbon $date_time){
        //1
        if ($financing_phase != null) {
            if ($financing_phase->financingStage->id == FinancingStage::getFinancingAbort()->id) {
                if ($financing_phase->financing->wallet->id == $this->id) {
                    //2
                    //3
                    if(auth()->user() != null){
                        $user_id = auth()->user()->id;
                    }else{
                        $user_id = null;
                    }
                    $transaction = WalletTransaction::create([
                        'user_id'                       => $user_id,
                        'wallet_id'                     => $this->id,
                        'wallet_type_of_transaction_id' => WalletTypeOfTransaction::getFinancingAborted()->id,
                        'amount'                        => Number::fix($financing_phase->amount * -1),
                        'date'                          => $date_time,
                        'financing_phase_id'            => $financing_phase->id,
                    ]);
                    $transaction->refresh();
                    $this->refresh();
                    return $transaction;
                }
                echo('Error 056548');
                dd();
            }
            echo('Error 9654');
            dd();
        }
        echo('Error 814614');
        dd();
        return null;
     }
    /**
     * NOTA: Este metodo es llamado desde el metodo abortCredit de Financing
     * Registra el reintegro de dinero cuando se cancela un credito.
     * Como lo hace
     *     1 Verfica los datos recibidos
     *     2 Suma el monto al wallet
     *     3 Crea la transaccion del wallet
     * @param FinancingPhase $financing_pase
     * @param Carbon $date_time
     * @return WalletTransaction
     */
    public function cancelFinancingCredit(FinancingPhase $financing_phase, Carbon $date_time){
        //1
        if ($financing_phase != null) {
            if ($financing_phase->financingStage->id == FinancingStage::getCreditAbort()->id) {
                if ($financing_phase->financing->wallet->id == $this->id) {
                    //2
                    //3
                    if(auth()->user() != null){
                        $user_id = auth()->user()->id;
                    }else{
                        $user_id = null;
                    }
                    $transaction = WalletTransaction::create([
                        'user_id'                       => $user_id,
                        'wallet_id'                     => $this->id,
                        'wallet_type_of_transaction_id' => WalletTypeOfTransaction::getCreditAborted()->id,
                        'amount'                        => Number::fix($financing_phase->amount *-1),
                        'date'                          => $date_time,
                        'financing_phase_id'            => $financing_phase->id,
                    ]);
                    $transaction->refresh();
                    $this->refresh();
                    return $transaction;    
                }
                echo('Error 85147895');
                dd();
            }
            echo('Error 8514862');
            dd();
        }
        echo('Error 521189');
        dd();
        return null;
     }
    /**
     * NOTA: Este metodo es llamado desde el front
     * Registra el pago de una o varias cuotas de un credito
     * Como
     *      1 Verifica que  el crédito no sea nulo y que el usuario del wallet posea el crédito en cuestion.
     *      2 Verifica si el monto a pagar es mayor a la deuda total del credito.
     *          2.1 Si es asi, modifica el monto a pagar al monto de la deuda
     *      3 Obtiene la primer cuota impaga y la almacena en un variable
     *      4 Verifica que el monto a pagar sea menor o igual al de la cuota
     *          4.1 Si es asi, dupica la variable $amount en $amount_to_pay
     *          4.2 Si no, establece $amount_to_pay en la deuda de la cuota
     *      5 Llama al metodo pay de Payment
     *      6 Verifica que se haya ejecutado el pago
     *      7 Disminuye el balance de cuenta
     *      8 Crea la transaccion del wallet
     *      8.1 Envia un mensaje a creditPay para crear el comprobante
     *      9 Controla si el monto que se pago es diferente al monto original (en este caso, el usuario esta pagando mas de una cuota)
     *          9.1 Si es diferente, se llama a si mismo con el monto restante para pagar la/s cuotas siguientes.
     *          9.2 Si son iguales, retorna la transaccion del wallet (última cuota paga).
     * @param Credit $credit
     * @param float $amount
     * @return WalletTransaction
     */
    public function payCredit(Credit $credit, $amount, Carbon $date_time = null){
        if($date_time == null){
            $date_time = Carbon::now();
        }else{
            if (Carbon::now()->diffInSeconds($date_time) > 180) {
            echo($date_time.' $wallet->payCredit(..)
            ');
            }
        }
        //1
        if ($credit != null) {
            if ($this->hasCredit($credit)) {
                if($this->accountBalance() >= $credit->currency()->min_value) {
                    $amount = $this->currency->truncate($amount);
                    if ($amount > $this->accountBalance()) {
                        $amount = $this->accountBalance();
                    }
                    if ($credit->unpayedAmount() >= $credit->currency()->min_value) {
                        $amount = $credit->currency()->truncate($amount);
                        if ($credit->lastCreditPhase()->creditStage->id != CreditStage::getLiquidate()->id) {
                            //2 Verifica si el monto a pagar es mayor a la deuda total del credito.
                            if ($amount > $credit->unpayedAmount()) {
                                //2.1 Si es asi, modifica el monto a pagar al monto de la deuda
                                $amount = $credit->unpayedAmount();
                            }
                            //3 Obtiene la primer cuota impaga y la almacena en un variable
                            $payment_to_pay = $credit->firstUnpayedPayment($date_time);
                            //4 Verifica que el monto a pagar sea menor o igual al de la cuota
                            if ($payment_to_pay->unpayedAmount() >= $amount){
                                //4.1 Si es asi, dupica la variable $amount en $amount_to_pay
                                $amount_to_pay = $amount;
                            }else{
                                //4.2 Si no, establece $amount_to_pay en la deuda de la cuota
                                $amount_to_pay = $payment_to_pay->unpayedAmount();
                            }
                            if ($amount_to_pay < $credit->currency()->min_value){
                                echo('Error 6641475 '.$amount_to_pay);
                                dd();
                            }
                            //5 Llama al metodo pay de Payment
                            $payment_phase = $payment_to_pay->pay($amount_to_pay, $date_time);
                            //6 Verifica que se haya ejecutado el pago
                            if ($payment_phase != null) {
                                //7 Disminuye el balance de cuenta
                                //8 Crea la transaccion del wallet
                                if(auth()->user() != null){
                                    $user_id = auth()->user()->id;
                                }else{
                                    $user_id = null;
                                }
                                $wallet_transaction = WalletTransaction::create([
                                    'user_id'                       => $user_id,
                                    'wallet_id'                     => $this->id,
                                    'wallet_type_of_transaction_id' => WalletTypeOfTransaction::getCreditPayment()->id,
                                    'amount'                        => Number::fix($amount_to_pay * -1),
                                    'date'                          => $date_time,
                                    'payment_phase_id'              => $payment_phase->id,
                                ]);
                                $this->refresh();
                                WalletTypeOfTransaction::getCreditPayment()->refresh();
                                $payment_phase->refresh();
                                //8.1 Envia un mensaje a creditPay para crear el comprobante
                                if (Voucher::creditPay($wallet_transaction,$date_time) == null) {
                                    echo('Error 4038921');
                                    dd();
                                }
                                //9 Controla si el monto que se pago es diferente al monto original (en este caso, el usuario esta pagando mas de una cuota)
                                if(abs($amount_to_pay - $amount) >= $credit->currency()->min_value){
                                    //9.1 Si es diferente, se llama a si mismo con el monto restante para pagar la/s cuotas siguientes.
                                    $new_amount = $amount - $amount_to_pay;
                                    return $this->payCredit($credit,$new_amount, $date_time);
                                }else{
                                    //9.2 Si son iguales, retorna la transaccion del wallet (última cuota paga).
                                    return $wallet_transaction;
                                }
                                //Termina la funcion
                            }
                            echo('Error 9058');
                            dd();
                        }
                        echo('Error 87415555');
                        dd();
                    }
                    echo('Error 66412 '.$credit->unpayedAmount().' '.$amount);
                    dd();
                }
                echo('Error 63347488 '.$this->accountBalance());
                dd();
            }
            echo('Error 985520');
            dd();
        }
        echo('Error 678545');
        dd();
        return null;
     }
    /**
     * NOTA Este metodo es invocado solamente desde el metodo return(..) de financing
     * Registra el reintegro de un crédito en el wallet
     * Como
     *      1 Verifica que se puede hacer
     *      2 Modifica el account_balance del Wallet
     *      3 Crea la transaccion
     *      4 Retorna
     * @param Repayment $repayment
     * @param Carbon $date_time
     * @return WalletTransaction
     */
    public function returnForFinancing(Repayment $repayment, Carbon $date_time){
        //1
        if ($repayment != null){
            if($this->hasFinancing($repayment->financing)){
                //Verifica que la fase de pago sea la última para el pago.
                if($repayment->paymentPhase->payment->lastPaymentPhase()->id == $repayment->paymentPhase->id){
                    //2
                    //3
                    if(auth()->user() != null){
                        $user_id = auth()->user()->id;
                    }else{
                        $user_id = null;
                    }
                    $wallet_transaction = WalletTransaction::create([
                        'user_id'                       => $user_id,
                        'wallet_id'                     => $this->id,
                        'wallet_type_of_transaction_id' => WalletTypeOfTransaction::getCreditReturn()->id,
                        'amount'                        => Number::fix($repayment->amount),
                        'date'                          => $date_time,
                        'repayment_id'                  => $repayment->id,
                    ]);
                    $wallet_transaction->refresh();
                    $this->refresh();
                    //4
                    return $wallet_transaction;
                }
                echo('Error 80547');
                dd();
            }
            echo('Error 85185');
            dd();
        }
        echo('Error 43265689');
        dd();
        return null;
     }
    /**
     * NOTA Este metodo es invocado solamente desde el metodo insuranceReturn(..) de financing
     * Registra el reintegro por seguro de un crédito en el wallet
     * Como
     *      1 Verifica que se puede hacer
     *      2 Modifica el account_balance del Wallet
     *      3 Crea la transaccion
     *      4 Retorna
     * @param Repayment $repayment
     * @param Carbon $date_time
     * @return WalletTransaction
     */
    public function insuranceReturnForFinancing(Repayment $repayment, Carbon $date_time){
        //1
        if ($repayment != null){
            if($this->hasFinancing($repayment->financing)){
                //Verifica que la fase de pago sea la última para el pago.
                if($repayment->paymentPhase->payment->lastPaymentPhase()->id == $repayment->paymentPhase->id){
                    if ($repayment->financing->have_insurance){
                        //2
                        //3
                        if(auth()->user() != null){
                            $user_id = auth()->user()->id;
                        }else{
                            $user_id = null;
                        }
                        $transaction = WalletTransaction::create([
                            'user_id'                       => $user_id,
                            'wallet_id'                     => $this->id,
                            'wallet_type_of_transaction_id' => WalletTypeOfTransaction::getCreditInsuranceReturn()->id,
                            'amount'                        => Number::fix($repayment->amount),
                            'date'                          => $date_time,
                            'repayment_id'                  => $repayment->id,
                        ]);
                        $transaction->refresh();
                        $this->refresh();
                        //4
                        return $transaction;
                    }
                    echo('Error 12584');
                    dd();
                }
                echo('Error 751532');
                dd();
            }
            echo('Error 85525');
            dd();
        }
        echo('Error 7748851');
        dd();
        return null;
     }
    /**
     * NOTA: este metodo es llamado desde los metodos de payment
     * Registra la transaccion del pago del seguro
     * Como
     *     1 Verifica que se puede ejecutar el metodo
     *     2 Llama a la funcion insuranceCreditorIncome
     *     3 Descuenta el monto del wallet
     *     4 Crea la transaccion
     *     5 Envia el monto al wallet del usuario de rol de seguros
     * @param Repayment $repayment
     * @param Carbon $date_time
     * @return WalletTransaction
     */
    public function insuranceCreditorPayment(Repayment $repayment, Carbon $date_time){
        //1
        if ($repayment != null) {
            if ($repayment->financing->have_insurance) {
                //2
                $insurance_transaction = Wallet::insuranceCreditorIncome($repayment,$date_time);
                if ($insurance_transaction != null) {
                    //3
                    //4
                    if(auth()->user() != null){
                        $user_id = auth()->user()->id;
                    }else{
                        $user_id = null;
                    }
                    $wallet_transaction = WalletTransaction::create([
                        'user_id'                       => $user_id,
                        'wallet_id'                     => $this->id,
                        'wallet_type_of_transaction_id' => WalletTypeOfTransaction::getInsurancePayment()->id,
                        'amount'                        => Number::fix($insurance_transaction->amount * -1),
                        'date'                          => $date_time,
                        'repayment_id'                  => $repayment->id,
                    ]);
                    $wallet_transaction->refresh();
                    $this->refresh();
                    //5
                    if( Voucher::insurancePay($wallet_transaction,$date_time) == null){
                        echo('Error 248962099');
                        dd();
                    }
                    return $wallet_transaction;
                }
                echo('Error 722621552');
                dd();
            }
            echo('Error 0252385156');
            dd();
        }
        echo('Error 95110265');
        dd();
        return null;
     }
    /**
     * NOTA Este metodo es llamado desde el metodo estatico exchange, de la clase Exchange
     * Registra el ingreso de dinero en el wallet por uncambio de divisa
     *  1 Verifica  que exchange sea distinto a null
     *  2 Verifica que $to_wallet sea igual al wallet en cuestion.
     *  3 Obtinene el monto de la transaccion.
     *  4 Aumenta el balance de cuenta.
     *  5 Crea la transaccion.
     *  6 Llama a echangeInput de Voucher para generar el comprobante
     *  7 Retorna.
     * @param Exchange $exchange
     * @param Carbon $date_time
     * @return WalletTransaction
     */
    public function exchangeInput(Exchange $exchange, Carbon $date_time){
        //1 Verifica  que exchange sea distinto a null
        if($exchange != null){
            //2 Verifica que $to_wallet sea igual al wallet en cuestion.
            if($exchange->toWallet->id == $this->id){
                //3 Obtinene el monto de la transaccion.
                $amount = $exchange->convert(1);
                //4 Aumenta el balance de cuenta.
                //5 Crea la transaccion.
                if(auth()->user() != null){
                    $user_id = auth()->user()->id;
                }else{
                    $user_id = null;
                }
                $wallet_transaction = WalletTransaction::create([
                    'user_id'                       => $user_id,//Para auditoria
                    'wallet_id'                     => $this->id,
                    'wallet_type_of_transaction_id' => WalletTypeOfTransaction::getExchangeInput()->id,
                    'amount'                        => Number::fix($amount),
                    'date'                          => $date_time,
                    'exchange_id'                   => $exchange->id,
                ]);
                $wallet_transaction->refresh();
                $this->refresh();
                $exchange->refresh();
                //6 Llama a exchangeInput de Voucher para generar el comprobante
                if (Voucher::exchangeInput($wallet_transaction,$date_time) == null) {
                    echo('Error 2251262');
                    dd();
                }
                //7 Retorna.
                return $wallet_transaction;    
            }
            echo('Error 56212472'.$exchange.$this);
            dd();
        }
        echo('Error 442555321456');
        dd();
     }
    /**
     * NOTA Este metodo es llamado desde el metodo estatico exchange, de la clase Exchange
     * Registra el egreso de dinero en el wallet por uncambio de divisa
     *  1 Verifica  que exchange sea distinto a null
     *  2 Verifica que $from_wallet sea igual al wallet en cuestion.
     *  3 Obtinene el monto de la transaccion.
     *  4 Disminuye el balance de cuenta.
     *  5 Crea la transaccion.
     *  6 Llama a exhangeOutput para crear el comprobante
     *  7 Retorna.
     * @param Exchange $exchange
     * @param Carbon $date_time
     * @return WalletTransaction
     */
    public function exchangeOutput(Exchange $exchange, Carbon $date_time){
        //1 Verifica  que exchange sea distinto a null
        if($exchange != null){
            //2 Verifica que $from_wallet sea igual al wallet en cuestion.
            if($exchange->fromWallet->id == $this->id){
                //3 Obtinene el monto de la transaccion.
                $amount = $exchange->convert(0);
                //4 Disminuye el balance de cuenta.
                //5 Crea la transaccion.
                if(auth()->user() != null){
                    $user_id = auth()->user()->id;
                }else{
                    $user_id = null;
                }
                $wallet_transaction = WalletTransaction::create([
                    'user_id'                       => $user_id,//Para auditoria
                    'wallet_id'                     => $this->id,
                    'wallet_type_of_transaction_id' => WalletTypeOfTransaction::getExchangeOutput()->id,
                    'amount'                        => Number::fix($amount * -1),
                    'date'                          => $date_time,
                    'exchange_id'                   => $exchange->id,
                ]);
                $wallet_transaction->refresh();
                $this->refresh();
                $exchange->refresh();
                //6 Llama a exhangeOutput para crear el comprobante
                if (Voucher::exchangeOutput($wallet_transaction,$date_time) == null) {
                    echo('Error 156982245');
                    dd();
                }
                //Retorna.
                return $wallet_transaction;    
            }
            echo('Error 665862555');
            dd();
        }
        echo('Error 2145528');
        dd();
     }
    /**
     * Este metodo se disenio para unificar el pago del seguro acreedor y deudor.
     * El fin es evitar conflictos o inconsistencias en los montos de los pagos.
     * Que hace
     *      1 Verifica que los datos no sean nulos y el pago sea correcto
     *      2 Paga la comision
     *      3 Paga el seguro
     *      4 Retorna la suma de ambos
     * @param PaymentPhase $payment_phase
     * @param Financing $financing
     * @param Carbon $date_time
     * @return double
     */
    public function payCommissionAndDebtorInsurance(PaymentPhase $payment_phase, Financing $financing, Carbon $date_time){
        //1
        if ($payment_phase != null && $financing != null) {
            if (($financing->wallet->currency->id == $this->currency->id) && ($payment_phase->payment->credit->currency()->id == $this->currency->id)) {
                if (($payment_phase->paymentStage->id == PaymentStage::getInsurancePay()->id) || ($payment_phase->paymentStage->id == PaymentStage::getPay()->id)) {
                    //2
                    $commision_payment = Wallet::commissionIncome($payment_phase,$financing,$date_time);
                    //3
                    $insurance_payment = Wallet::insuranceDebtorIncome($payment_phase,$financing,$commision_payment,$date_time);
                    //4
                    return ($commision_payment->amount + $insurance_payment->amount);
                }
                echo('Error 8511744520');
                dd();
            }
            echo('Error 7751123');
            dd();
        }
        echo('Error 111222586');
        dd();
     }
    /*
        'Ingreso por abono del seguro acreedor'-> ::insuranceCreditorIncome($repayment)
        'Ingreso por abono del seguro deudor'---> ::insuranceDebtorIncome($payment_phase)
        'Efectivizacion de seguro'--------------> ::insurancePayment($repayment)
        'Reposicion del seguro'-----------------> ::insurancePaymentReplace($repayment)
        'Ingreso por comision-------------------> ::commissionIncome($payment_phase, $financing)
     */
    /**
     * NOTA: Este metodo es llamado unicamente desde insuranceCreditorPayment(..), de esta clase
     * Registra el ingreso de dinero por pago de seguro acreedor en el wallet del InsuranceManager
     * Como:
     *      1 Verifica que este todo bien
     *      2 Calcula el mondo del seguro.
     *      3 Obtiene la currency
     *      4 Busca el usuario que maneja los seguros
     *      5 Busca el wallet que tenga el usuario y la currency en cuestion
     *         5.1 Si no existe el wallet, lo crea
     *      6 Cambia el balance de la cuenta
     *      7 Registra la transaccion 
     *      8 Retorna
     * @param Repayment $repayment
     * @param Carbon $date_time
     * @return WalletTransaction
     */
    public static function insuranceCreditorIncome(Repayment $repayment, Carbon $date_time){
        //1
        if ($repayment != null) {
            if ($repayment->paymentPhase->payment->lastPaymentPhase()->id == $repayment->paymentPhase->id) {
                //2
                $amount = $repayment->amount * $repayment->financing->credit->creditor_insurance_percentage;
                //3
                $currency = $repayment->financing->wallet->currency;
                //4
                $insurance_manager = User::where('role_id',Role::getInsuranceManagerRole()->id)
                    ->first();
                if ($insurance_manager != null) {
                    //5
                    $wallet = Wallet::where('user_id',$insurance_manager->id)
                            ->where('currency_id',$currency->id)
                            ->first();
                    //5.1
                    if ($wallet == null) {
                        $wallet = Wallet::create([
                            'user_id'                  => $insurance_manager->id,
                            'currency_id'              => $currency->id,
                        ]);
                        $wallet->refresh();
                    }
                    //6
                    //7
                    if(auth()->user() != null){
                        $user_id = auth()->user()->id;
                    }else{
                        $user_id = null;
                    }
                    $wallet_transaction = WalletTransaction::create([
                        'user_id'                       => $user_id,
                        'wallet_id'                     => $wallet->id,
                        'wallet_type_of_transaction_id' => WalletTypeOfTransaction::getCreditorInsuranceIncome()->id,
                        'amount'                        => Number::fix($amount),
                        'date'                          => $date_time,
                        'repayment_id'                  => $repayment->id,
                    ]);
                    $wallet_transaction->refresh();
                    $wallet->refresh();
                    //8
                    return $wallet_transaction;
                }
                echo('Error 854757 Cree un usuario con rol insurance_manager');
                dd();
            }
            echo('Error 8812369');
            dd();
        }
        echo('Error 02086');
        dd();

     }
    /**
     * NOTA: Este metodo es llamado unicamente desde payCommissionAndDebtorInsurance(..) de esta clase.
     * Registra el ingreso de dinero por pago de seguro deudor en el wallet del InsuranceManager
     * Como:
     *      1 Calcula el monto por comision para esa financiacion
     *      2 Calcula el mondo del seguro.
     *      3 Obtiene la currency
     *      4 Busca el usuario que maneja los seguros
     *      5 Busca el wallet que tenga el usuario y la currency en cuestion
     *         5.1 Si no existe el wallet, lo crea
     *      6 Cambia el balance de la cuenta
     *      7 Registra la transaccion 
     *      8 Retorna
     * @param PaymentPhase $payment_phase
     * @param Financing $financing
     * @param WalletTransaction $commission_payment
     * @param Carbon $date_time
     * @return WalletTransaction
     */
    public static function insuranceDebtorIncome(PaymentPhase $payment_phase, Financing $financing, WalletTransaction $commission_payment, Carbon $date_time){
        if (($payment_phase != null) && ($commission_payment != null)) {
            if (($payment_phase->paymentStage->id == PaymentStage::getInsurancePay()->id) || ($payment_phase->paymentStage->id == PaymentStage::getPay()->id)) {
                //1
                $percentage_of_financing = $financing->amount()/$financing->credit->amount;
                $amount_for_this_financing = ($payment_phase->amount * $percentage_of_financing) - $commission_payment->amount;
                //2
                $insurance_amount = $amount_for_this_financing * $payment_phase->payment->credit->debtor_insurance_percentage;
                //3
                $currency = $payment_phase->payment->credit->currency();
                //4
                $insurance_manager = User::where('role_id',Role::getInsuranceManagerRole()->id)
                    ->first();
                if($insurance_manager != null) {
                    //5
                    $wallet = Wallet::where('user_id',$insurance_manager->id)
                            ->where('currency_id',$currency->id)
                            ->first();
                    //5.1
                    if ($wallet == null) {
                        $wallet = Wallet::create([
                            'user_id'                  => $insurance_manager->id,
                            'currency_id'              => $currency->id,
                        ]);
                        $wallet->refresh();
                    }
                    //6
                    //7
                    if(auth()->user() != null){
                        $user_id = auth()->user()->id;
                    }else{
                        $user_id = null;
                    }
                    $wallet_transaction = WalletTransaction::create([
                        'user_id'                       => $user_id,
                        'wallet_id'                     => $wallet->id,
                        'wallet_type_of_transaction_id' => WalletTypeOfTransaction::getDebtorInsuranceIncome()->id,
                        'amount'                        => Number::fix($insurance_amount),
                        'date'                          => $date_time,
                        'payment_phase_id'              => $payment_phase->id,
                    ]);
                    $wallet_transaction->refresh();
                    $wallet->refresh();
                    //8
                    return $wallet_transaction;
                }
                echo('Error 854757 Cree un usuario con rol insurance_manager');
                dd();
            }
            echo('Error 8841299 '.$payment_phase->paymentStage->id);
            dd();
        }
        echo('Error 55511005');
        dd();

     }    
    /**
     * NOTA: este metodo solamente debe ser llamado desde el metodo insuranceReturn(..) de Financing
     * Se encarga de disminuir el pago del seguro del wallet del InsuranceManager
     * Como:
     *     1 Busca el usuario que maneja los seguros
     *     2 Busca el wallet que tenga el usuario y la currency en cuestion
     *         2.1 Si no existe el wallet, lo crea
     *     3 Cambia el balance de la cuenta
     *     4 Registra la transaccion
     * @param Repayment $repayment
     * @param double $repayment_amount
     * @param Carbon $date_time
     * @return WalletTransaction
     */
    public static function insurancePayment(Repayment $repayment, $repayment_amount, Carbon $date_time){
        if($repayment != null) {
            //1
            $insurance_manager = User::where('role_id',Role::getInsuranceManagerRole()->id)
                ->first();
            if($insurance_manager != null) {
                //2
                $currency = $repayment->financing->wallet->currency;
                $wallet = Wallet::where('user_id',$insurance_manager->id)
                        ->where('currency_id',$currency->id)
                        ->first();
                //2.1
                if($wallet == null)
                    $wallet = Wallet::create([
                        'user_id'                  => $insurance_manager->id,
                        'currency_id'              => $currency->id,
                    ]);
                    $wallet->refresh();
                //3
                //4
                if(auth()->user() != null){
                    $user_id = auth()->user()->id;
                }else{
                    $user_id = null;
                }
                $wallet_transaction = WalletTransaction::create([
                    'user_id'                       => $user_id,
                    'wallet_id'                     => $wallet->id,
                    'wallet_type_of_transaction_id' => WalletTypeOfTransaction::getInsuranceEffect()->id,
                    'amount'                        => Number::fix($repayment_amount * -1),
                    'date'                          => $date_time,
                    'repayment_id'                  => $repayment->id,
                ]);
                $wallet_transaction->refresh();
                $wallet->refresh();
                return $wallet_transaction;
            }
            echo('Error 854757 Cree un usuario con rol insurance_manager');
            dd();
        }
        echo('Error 002556214');
        dd();
     }
    /**
     * NOTA: Este metodo solamente puede ser llamado desde el metodo pay(..) de Payment
     * Registra la devolucion del pago de seguro atrasado
     * Como:
     *     1 Busca el usuario que maneja los seguros
     *     2 Busca el wallet que tenga el usuario y la currency en cuestion
     *         2.1 Si no existe el wallet, lo crea
     *     3 Cambia el balance de la cuenta
     *     4 Registra la transaccion
     * @param Repayment $repayment
     * @param double $repayment_amount
     * @param Carbon $date_time
     * @return WalletTransaction
     */
    public static function insurancePaymentReplace(Repayment $repayment, $repayment_amount, Carbon $date_time){
        if ($repayment != null){
            if ($repayment_amount != null){
                //1
                $insurance_manager = User::where('role_id',Role::getInsuranceManagerRole()->id)
                    ->first();
                if($insurance_manager != null){
                    //2
                    $currency = $repayment->financing->wallet->currency;
                    $wallet = Wallet::where('user_id',$insurance_manager->id)
                            ->where('currency_id',$currency->id)
                            ->first();
                    //2.1
                    if($wallet == null)
                        $wallet = Wallet::create([
                            'user_id'                  => $insurance_manager->id,
                            'currency_id'              => $currency->id,
                        ]);
                        $wallet->refresh();
                    //3
                    //4
                    if(auth()->user() != null){
                        $user_id = auth()->user()->id;
                    }else{
                        $user_id = null;
                    }
                    $wallet_transaction = WalletTransaction::create([
                        'user_id'                       => $user_id,
                        'wallet_id'                     => $wallet->id,
                        'wallet_type_of_transaction_id' => WalletTypeOfTransaction::getInsurancePaymentReplace()->id,
                        'amount'                        => Number::fix($repayment_amount),
                        'date'                          => $date_time,
                        'repayment_id'                  => $repayment->id,
                    ]);
                    $wallet_transaction->refresh();
                    $wallet->refresh();
                    return $wallet_transaction;
                }
                echo('Error 854757 Cree un usuario con rol insurance_manager');
                dd();
            }
            echo('Error 8512221478 '.$repayment);
            dd();
        }
        echo('Error 5521225 '.$repayment_amount);
        dd();
     }
    /**
     * NOTA: Este metodo sera llamado solamente desde payCommissionAndDebtorInsurance(..) de esta clase
     * Que hace este metodo:
     *      1 Verifica que no sean nulos
     *      2 Controla el PaymentStage del PaymentPhase
     *      3 Calcula el porcentage financiado del credito.
     *      4 Calcula el monto financiado.
     *      5 Calcula la comision
     *      6 Busca el usuario que maneja las comisiones
     *      7 Busca el wallet que tenga el usuario y la currency en cuestion
     *         7.1 Si no existe el wallet, lo crea
     *      8 Cambia el balance de la cuenta
     *      9 Registra la transaccion
     * @param PaymentPhase $payment_phase
     * @param Financing $financing
     * @param Carbon $date_time
     * @return WalletTransaction
     */
    public static function commissionIncome(PaymentPhase $payment_phase, Financing $financing, Carbon $date_time){
        //1
        if ($payment_phase != null && $financing != null) {
            //2
            if (($payment_phase->paymentStage->id == PaymentStage::getInsurancePay()->id) || ($payment_phase->paymentStage->id == PaymentStage::getPay()->id)) {
                //3
                $percentage_of_financing = $financing->amount()/$financing->credit->amount;
                //4
                $amount_for_this_financing = $payment_phase->amount * $percentage_of_financing;
                //5
                $commission = ($financing->credit->interest_percentage * $financing->credit->commission_percentage) * $amount_for_this_financing;
                //6
                $commission_manager = User::where('role_id',Role::getCommissionManagerRole()->id)
                ->first();
                if ($commission_manager != null) {
                    //7
                    $currency = $financing->wallet->currency;
                    $wallet = Wallet::where('user_id',$commission_manager->id)
                            ->where('currency_id',$currency->id)
                            ->first();
                    //7.1
                    if($wallet == null)
                        $wallet = Wallet::create([
                            'user_id'                  => $commission_manager->id,
                            'currency_id'              => $currency->id,
                        ]);
                        $wallet->refresh();
                    //8
                    //9
                    if(auth()->user() != null){
                        $user_id = auth()->user()->id;
                    }else{
                        $user_id = null;
                    }
                    $wallet_transaction = WalletTransaction::create([
                        'user_id'                       => $user_id,
                        'wallet_id'                     => $wallet->id,
                        'wallet_type_of_transaction_id' => WalletTypeOfTransaction::getCommissionIncome()->id,
                        'amount'                        => Number::fix($commission),
                        'date'                          => $date_time,
                    ]);
                    $wallet_transaction->refresh();
                    $wallet->refresh();
                    return $wallet_transaction;
                }
            }
            echo('Error 877 Cree un usuario con rol commission_manager');
            dd();
        }
     }
}
