<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Audit extends Model
{
    protected $table = 'audits';
    protected $fillable = [
        '_db_user'        ,
        '_system_user_id' ,
        '_table'          ,
        '_action'         ,
        '_data'           ,
        '_old_data'       ,
        '_new_data'       ,
        '_date'           ,
    ];
    public $timestamps = false;
    public function user()
    {
        return $this->belongsTo('App\Models\User', '_system_user_id', 'id');
    }
}
