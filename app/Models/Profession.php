<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Profession extends Model
{
    protected $table = 'professions';
    protected $fillable = [
        'name'   ,
        'user_id',//nullable. Usado para la auditoria
    ];
    public $timestamps = false;
    public function users(){
        return $this->hasMany('app\Models\User');
    }
}
