<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DocumentType extends Model
{
    protected $table = 'document_types';
    protected $fillable = [
        'name',
    ];
    public $timestamps = false;
    public function documents(){
        return $this->hasMany('App\Models\Document');
    }
}
