<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\CarbonPeriod;

class Interest extends Model
{
    protected $table = 'interests';
    protected $fillable = [
        'initial_percentage'    ,
        'min_percentage'        ,
        'max_percentage'        ,
        'commission_percentage' ,
        'user_id'               ,//nullable. Usado para la auditoria
    ];
    public $timestamps = false;
    public function calculate(Reputation $reputation){
        $interest_percentage = $this->initial_percentage + InterestModifier::percentage($reputation);
        if($interest_percentage > $this->max_percentage)
            $interest_percentage = $this->max_percentage;

        if($interest_percentage < $this->min_percentage)
            $interest_percentage = $this->min_percentage;
        
        return $interest_percentage;
    }
    public function dailyInterestForLatePayment(Reputation $reputation){
        return $this->calculate($reputation)/20;
    }
    /**
     * Obtiene los dias de gracia para los pagos atrasados. Pueden ser 6,4,3,1 o 0 dias
     * @param Reputation $reputation
     * @return CustomDateInterval
     */
    public function gracePeriod(Reputation $reputation){
        /*

               min_percentage                   1/3                             2/3        initial_percentage        1/3              2/3          max_percentage/
                  ||                             |                               |                  ||                |                |                  ||
                  ||-----------6 dias------------|---------------4 dias----------|----------------3 dias---------------|--------------1 dias---------------||
        $values=  [0                             1                               2                   3                4                5                  6]
                   < $int>=val[0] && $int<val[1] >< $int>=val[1] && $int<val[2] ><    $int>=val[2] && $int<val[4]     ><    $int>=val[1]&&$int<val[2]     >
        */
        $limit_values =[
            '0' => $this->min_percentage,
            '1' => $this->min_percentage +(($this->initial_percentage - $this->min_percentage) * (1/3)),
            '2' => $this->min_percentage +(($this->initial_percentage - $this->min_percentage) * (2/3)),
            '3' => $this->initial_percentage,
            '4' => $this->initial_percentage+(($this->max_percentage - $this->initial_percentage) * (1/3)),
            '5' => $this->initial_percentage+(($this->max_percentage - $this->initial_percentage) * (2/3)),
            '6' => $this->max_percentage
        ];
        $user_interest = $this->calculate($reputation);
        //dd($user_interest,$limit_values,$limit_values[4]);

        if (($user_interest >= $limit_values[0]) && ($user_interest < $limit_values[1])){
            return CustomDateInterval::customCreate(0,6,0,0,0);
        }
        if (($user_interest >= $limit_values[1]) && ($user_interest < $limit_values[2])){
            return CustomDateInterval::customCreate(0,4,0,0,0);
        }
        if (($user_interest >= $limit_values[2]) && ($user_interest < $limit_values[4])){
            return CustomDateInterval::customCreate(0,3,0,0,0);
        }
        if (($user_interest >= $limit_values[4]) && ($user_interest <= $limit_values[6])){
            return CustomDateInterval::customCreate(0,1,0,0,0);
        }
        return null;
    }
}
