<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $table = 'countries';
    protected $fillable =[
        'name'       ,
        'phone_code' ,
        'currency_id',
    ];
    public $timestamps = false;
    public function currency(){
        return $this->belongsTo('App\Models\Currency');
    }
    public function documents(){
        return $this->hasMany('App\Models\Document');
    }
    public function districts(){
        return $this->hasMany('App\Models\District');
    }
    public function maxAmountCredits()
    {
        return $this->hasMany('App\Models\MaxAmountCredit');
    }
}
