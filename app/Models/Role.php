<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = 'roles';
    protected $fillable = [
        'name',
    ];
    public $timestamps = false;
    public function users()
    {
        return $this->hasMany('App\Models\User');
    }
    /*
    Default Roles:
        1 'Usuario'-----------------> getUserRole()---------------> 'user'
        2 'Administrador'-----------> getAdminRole()--------------> 'admin'
        3 'Gestor de parametros'----> getParamManagerRole()-------> 'param-manager'
        4 'Division de seguros'-----> getInsuranceManagerRole()---> 'insurance-manager'
        5 'Division de comisiones'--> getCommissionManagerRole()--> 'commission-manager'
        6 'Division de litigios'----> getLawsuitManagerRole()-----> 'lawsuit-manager'
        7 'Division de morosos'-----> getDefaulterManagerRole()---> 'defaulter-manager'
        8 'Division de créditos'----> getCreditManagerRole()------> 'credit-manager'
        9 'Auditor'-----------------> getAuditRole()--------------> 'audit'
    */
    public static function getUserRole(){
        return Role::find(1);
    }
    public static function getAdminRole(){
        return Role::find(2);
    }
    public static function getParamManagerRole(){
        return Role::find(3);
    }
    public static function getInsuranceManagerRole(){
        return Role::find(4);
    }
    public static function getCommissionManagerRole(){
        return Role::find(5);
    }
    public static function getLawsuitManagerRole(){
        return Role::find(6);
    }
    public static function getDefaulterManagerRole(){
        return Role::find(7);
    }
    public static function getCreditManagerRole(){
        return Role::find(8);
    }
    public static function getAuditRole(){
        return Role::find(9);
    }
    public function getUsers(){
        return User::where('role_id',$this->id)
                ->get();
    }
}
