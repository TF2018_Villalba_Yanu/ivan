<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Documents extends Model
{
    protected $table = 'documents';
    protected $fillable = [
        'document_type_id',
        'country_id'      ,
        'user_id'         ,
        'number'          ,
        'front_image'     ,
        'back_image'      ,
        'face_image'      ,
    ];
    public $timestamps = false;
    public function documentType(){
        return $this->belongsTo('App\Models\DocumentType');
    }
    public function Country()
    {
        return $this->belongsTo('App\Models\Country');
    }
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
