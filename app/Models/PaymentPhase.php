<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentPhase extends Model
{
    protected $table = 'payment_phases';
    protected $fillable= [
        'amount'          ,
        'date'            ,
        'payment_id'      ,
        'payment_stage_id',
        'user_id'         ,//nullable. Usado para la auditoria
    ];
    protected $dates = [
        'date',
    ];
    public $timestamps = false;
    public function payment()
    {
        return $this->belongsTo('App\Models\Payment');
    }
    public function paymentStage()
    {
        return $this->belongsTo('App\Models\PaymentStage');
    }
    public function repayments(){
        return $this->hasMany('App\Models\Repayment');
    }


}
