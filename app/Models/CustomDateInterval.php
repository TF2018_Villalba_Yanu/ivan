<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DateInterval;
use Carbon\CarbonInterval;
class CustomDateInterval extends Model
{
    protected $fillable = [
        'months' ,
        'days'   ,
        'hours'  ,
        'minutes',
        'seconds',
    ];
    public $timestamps = false;
    /**
     * Crea el date interval
     * NOTA: Uso esta funcion porque verica que no haya un date interval con esos datos, porque si hay, manda la referencia
     * @param int $months
     * @param int $days
     * @param int $hours
     * @param int $minutes
     * @param int $seconds
     * @return CustomDateInterval
     */
    public static function customCreate($months, $days, $hours, $minutes,$seconds){
        //$cdi = $custom_date_interval
        $cdi = CustomDateInterval::where('months',$months)
            ->where('days',$days)
            ->where('hours',$hours)
            ->where('minutes',$minutes)
            ->where('seconds',$seconds)
            ->first();
        if($cdi == null){
            $cdi = CustomDateInterval::create([
                'months'  => $months ,
                'days'    => $days   ,
                'hours'   => $hours  ,
                'minutes' => $minutes,
                'seconds' => $seconds,
            ]);
            $cdi->refresh();
        }
        return $cdi;
    }
    /**
     * Devuelve el DateInterval para el CustomDateInterval
     * @return DateInterval
     */
    public function toDateInterval(){
        return new DateInterval('P'.$this->months.'M'.$this->days.'DT'.$this->hours.'H'.$this->minutes.'M');
    }
    public function toCarbonInterval(){
        return CarbonInterval::instance($this->toDateInterval());
    }
    public function toDays(){
        $days  = 0;
        $days += $this->months * 30;
        $days += $this->days;
        return $days;
    }
}
