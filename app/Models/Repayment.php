<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Repayment extends Model
{
    protected $table = 'repayments';
    protected $fillable = [
        'financing_id'    ,
        'payment_phase_id',
        'amount'          ,
        'user_id'         ,//nullable. Usado para la auditoria
    ];
    public $timestamps = false;
    public function financing(){
        return $this->belongsTo('App\Models\Financing');
    }
    public function paymentPhase(){
        return $this->belongsTo('App\Models\PaymentPhase');    
    }
    public function currency(){
        return $this->paymentPhase->payment->credit->currency();
    }
}
