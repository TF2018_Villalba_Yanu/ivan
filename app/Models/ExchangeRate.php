<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class ExchangeRate extends Model
{
    protected $table = 'exchange_rates';
    protected $fillable = [
        'from_currency_id',
        'to_currency_id'  ,
        'date'            ,
        'rate'            ,
    ];
    protected $dates = [
        'date',
    ];
    public $timestamps = false;
    public function fromCurrency(){
        return $this->belongsTo('App\Models\Currency','from_currency_id', 'id');
    }
    public function toCurrency(){
        return $this->belongsTo('App\Models\Currency','to_currency_id', 'id');
    }
    public static function exchangeRates(Currency $from_currency, Currency $to_currency){
        $exchange_rates = ExchangeRate::where('from_currency_id',$from_currency->id)->where('to_currency_id',$to_currency->id)->get();
        $exchange_rates = $exchange_rates->sortBy(function($exchange_rate,$key){
            return $exchange_rate->id;
        });
        return $exchange_rates;
    }
    public static function lastExchangeRate(Currency $from_currency, Currency $to_currency){
        return ExchangeRate::exchangeRates($from_currency, $to_currency)->last();
    }
    /**
     * API de conversion de de divisas de Google
     * Se saco desde https://programacion.net/articulo/como_convertir_divisas_en_php_utilizando_la_api_de_google_1890
     * @param string $from_currency en ISO_CODE
     * @param string $to_currency en ISO_CODE
     * @return double
     */
    public static function cloudConvert(string $from_currency,string $to_currency, $amount, Carbon $date_time = null) {
        if($date_time == null){
            $date_time = Carbon::now();
        }else{
            if (Carbon::now()->diffInSeconds($date_time) > 180) {
                echo($date_time.' ExchangeRate::cloudConvert(..)
                ');
            }
        }
        $from = Currency::getCurrency($from_currency);
        $to   = Currency::getCurrency($to_currency);
        if($from->id == $to->id){
            return $amount;
        }
        if(($from == null) || ($to == null)){
            echo('Error 32114533');
            dd();
        }
        if(ExchangeRate::lastExchangeRate($from,$to) == null){
            $rate = mt_rand(100000,5000000)*0.0001;
            ExchangeRate::create([
                'from_currency_id' => $from->id,
                'to_currency_id'   => $to->id,
                'date'             => $date_time,
                'rate'             => $rate,
            ]);
            ExchangeRate::create([
                'from_currency_id' => $to->id,
                'to_currency_id'   => $from->id,
                'date'             => $date_time,
                'rate'             => (1/$rate),
            ]);
        }
        $convert_value = $amount * ExchangeRate::lastExchangeRate($from,$to)->rate;
        $variation_percentage = mt_rand(1,60)*0.001;
        if(mt_rand(0,1)){
            return ($convert_value + ($convert_value * $variation_percentage));
        }else{
            return ($convert_value - ($convert_value * $variation_percentage));
        }
    }
    public static function localConvert(string $from_currency,string $to_currency, $amount, Carbon $date_time = null) {
        if($date_time == null){
            $date_time = Carbon::now();
        }else{
            if (Carbon::now()->diffInSeconds($date_time) > 180) {
                echo($date_time.' ExchangeRate::localConvert(..)
                ');
            }
        }
        $from = Currency::getCurrency($from_currency);
        $to   = Currency::getCurrency($to_currency);
        if($from->id == $to->id){
            return ((double) $amount);
        }
        if(($from == null) || ($to == null)){
            echo('Error 821155');
            dd();
        }
        if(ExchangeRate::lastExchangeRate($from,$to) == null){
            return ExchangeRate::cloudConvert($from->iso_code, $to->iso_code, $amount, $date_time);
        }
        return ($amount * ExchangeRate::lastExchangeRate($from,$to)->rate);
    }
    public function convert($amount, $reverse) {
        if ($reverse){//to-currency >> form-currency
            return $amount * $this->rate;            
        }else{//form-currency >> to-currency
            return $amount * (1/$this->rate);
        }
    }
    public static function updateExchanges(Carbon $date_time = null){
        if($date_time == null){
            $date_time = Carbon::now();
        }else{
            if (Carbon::now()->diffInSeconds($date_time) > 180) {
                echo($date_time.' ExchangeRate::updateExchanges(..)
                ');
            }
        }
        $from_currencies = Currency::all();
        $to_currencies = Currency::all();
        foreach($from_currencies as $from_currency){
            foreach($to_currencies as $to_currency){
                if($from_currency->id != $to_currency->id){
                    $rate = ExchangeRate::cloudConvert($from_currency->iso_code, $to_currency->iso_code,1,$date_time);
                    ExchangeRate::create([
                        'from_currency_id' => $from_currency->id,
                        'to_currency_id'   => $to_currency->id,
                        'date'             => $date_time,
                        'rate'             => $rate,
                    ]);
                    ExchangeRate::create([
                        'from_currency_id' => $to_currency->id,
                        'to_currency_id'   => $from_currency->id,
                        'date'             => $date_time,
                        'rate'             => (1/$rate),
                    ]);
                }
            }
        }
    }
}
