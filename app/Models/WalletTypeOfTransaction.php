<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WalletTypeOfTransaction extends Model
{
    protected $table = 'wallet_type_of_transactions';
    protected $fillable= [
        'name'       ,
        'type'       ,
        'description',
    ];
    public $timestamps = false;
    public function walletTransactions()
    {
        return $this->hasMany('App\Models\WalletTransaction');
    }

    /*::
         1 => 'Ingreso de dinero'------------------> ::getIncome()-------------------> 'input_money_transaction_id'
         2 => 'Retiro de dinero'-------------------> ::getWithdraw()-----------------> 'output_money_transaction_id'
         3 => 'Efectivización del crédito'---------> ::getGrantCredit()--------------> 'credit_phase_id'
         4 => 'Financiamiento'---------------------> ::getFinancing()----------------> 'financing_phase_id'
         5 => 'Financiamiento cancelado'-----------> ::getFinancingAborted()---------> 'financing_phase_id'
         6 => 'Crédito cancelado'------------------> ::getCreditAborted()------------> 'financing_phase_id'
         7 => 'Pago de cuota'----------------------> ::getCreditPayment()------------> 'payment_phase_id'
         8 => 'Reintegro del crédito'--------------> ::getCreditReturn()-------------> 'repayment_id'
         9 => 'Abono del seguro'-------------------> ::getInsurancePayment()---------> 'repayment_id'
        10 => 'Reintegro asegurado'----------------> ::getCreditInsuranceReturn()----> 'repayment_id' 
        16 => 'Ingreso por conversión de divisas'--> ::getExchangeInput()------------> 'exchange_id'
        17 => 'Egreso por conversion de divisas'---> ::getExchangeOutput()-----------> 'exchange_id'
        *INSURANCE_MANAGER_ONLY*
        11 => 'Ingreso por por seguro acreedor'----> ::getCreditorInsuranceIncome()--> 'repayment_id' 
        15 => 'Ingreso por por seguro deudor'------> ::getDebtorInsuranceIncome()----> 'payment_phase_id'
        12 => 'Efectivizacion de seguro'-----------> ::getInsuranceEffect()----------> 'repayment_id'
        13 => 'Reposicion del seguro'--------------> ::getInsurancePaymentReplace()--> 'repayment_id'
        *COMMISSION_MANAGER_ONLY*
        14 => 'Ingreso por comision----------------> ::getCommissionIncome()---------> 'payment_phase_id','financing_id'
    */
    public static function getGrantCredit(){
        return WalletTypeOfTransaction::find(3);
    }
    public static function getWithdraw(){
        return WalletTypeOfTransaction::find(2);
    }
    public static function getIncome(){
        return WalletTypeOfTransaction::find(1);
    }
    public static function getFinancing(){
        return WalletTypeOfTransaction::find(4);
    }
    public static function getFinancingAborted(){
        return WalletTypeOfTransaction::find(5);
    }
    public static function getCreditAborted(){
        return WalletTypeOfTransaction::find(6);
    }
    public static function getCreditPayment(){
        return WalletTypeOfTransaction::find(7);
    }
    public static function getCreditReturn(){
        return WalletTypeOfTransaction::find(8);
    }
    public static function getInsurancePayment(){
        return WalletTypeOfTransaction::find(9);
    }
    public static function getCreditInsuranceReturn(){
        return WalletTypeOfTransaction::find(10);
    }
    public static function getCreditorInsuranceIncome(){
        return WalletTypeOfTransaction::find(11);
    }
    public static function getInsuranceEffect(){
        return WalletTypeOfTransaction::find(12);
    }
    public static function getInsurancePaymentReplace(){
        return WalletTypeOfTransaction::find(13);
    }
    public static function getCommissionIncome(){
        return WalletTypeOfTransaction::find(14);
    }
    public static function getDebtorInsuranceIncome(){
        return WalletTypeOfTransaction::find(15);
    }
    public static function getExchangeInput(){
        return WalletTypeOfTransaction::find(16);
    }
    public static function getExchangeOutput(){
        return WalletTypeOfTransaction::find(17);
    }
}
