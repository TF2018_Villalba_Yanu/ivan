<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CreditPhase extends Model
{
    protected $table = 'credit_phases';
    protected $fillable = [
        'credit_id'      ,
        'credit_stage_id',
        'date'           ,
        'user_id'        ,//nullable. Usado para la auditoria
    ];
    protected $dates = [
        'date',
    ];
    public $timestamps = false;
    public function credit()
    {
        return $this->belongsTo('App\Models\Credit');
    }
    public function creditStage()
    {
        return $this->belongsTo('App\Models\CreditStage');
    }
    public function voucher()
    {
        $vouchers = $this->hasMany('App\Voucher');
        if($vouchers->count() == 1){
            return $vouchers->first();
        }
        if($vouchers->count() == 1){
            return null;
        }
        echo('Error 722520266');
        dd();
    }
}
