<?php

namespace App\Http\Controllers;

use App\Models\WalletTransaction;
use Illuminate\Http\Request;
use App\Models\Wallet;
use App\Models\Role;

class WalletTransactionController extends Controller
{
    public function index($id)
    {
        //user, insurance_manager y commision_manager
        $role = auth()->user()->role;
        if(($role->id == Role::getUserRole()->id)||($role->id == Role::getInsuranceManagerRole()->id)||($role->id == Role::getCommissionManagerRole()->id)){
            $wallet_transaction = WalletTransaction::find($id);
            if ($wallet_transaction != null) {
                if(auth()->user()->hasWallet($wallet_transaction->wallet)){
                    return view('wallets.transaction-index',compact('wallet_transaction'));
                }
            }
        }
            return view('errors.404');
    }
}
