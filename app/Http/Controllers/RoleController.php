<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Role;

class RoleController extends Controller
{
    public function index(){
        //Admin
        if(auth()->user()->role->id == Role::getAdminRole()->id){
            $roles = Role::all();
            return view('site.roles.index',compact('roles'));
        }
        return view('errors.404');
    }
    public function show($role_id){
        //Admin
        if(auth()->user()->role->id == Role::getAdminRole()->id){
            $role = Role::find($role_id);
            if($role != null){
                return view('site.roles.show',compact('role'));

            }
        }
        return view('errors.404');
    }
    public function edit($role_id){
        //Admin
        if(auth()->user()->role->id == Role::getAdminRole()->id){
            $role = Role::find($role_id);
            if($role != null){
                return view('site.roles.edit',compact('role'));

            }
        }
        return view('errors.404');
    }
    public function update(Request $request, $role_id){
        //Admin
        if(auth()->user()->role->id == Role::getAdminRole()->id){
            $role = Role::find($role_id);
            if($role != null){
                $role->name = $request->name;
                $role->save();
                $role->refresh();
                return $this->show($role->id);
            
            }
        }
        return view('errors.404');
    }
    public function create(Request $request){
        //Admin
        if(auth()->user()->role->id == Role::getAdminRole()->id){
            Role::create([
                'name' => $request->name,
            ]);
            return $this->index();
        }
        return view('errors.404');
    }
}
