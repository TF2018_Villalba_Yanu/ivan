<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Currency;
use App\Models\Profession;
use App\Models\MaxAmountCredit;
use App\Models\Role;
use App\Models\ExchangeRate;

class CurrencyController extends Controller
{
    public function index(){
        //CreditManager role only
        if(auth()->user()->role->id == Role::getCreditManagerRole()->id){
            $currencies = Currency::all();
            return view('site.currencies.index',compact('currencies'));
        }
        return view('errors.404');
    }

    public function create(Request $request){
        //CreditManager role only
        $user = auth()->user();
        if(auth()->user()->role->id == Role::getCreditManagerRole()->id){
            $currency = Currency::create([
                'name'                     => $request->name,
                'iso_code'                 => strtoupper($request->iso_code),
                'simbol'                   => $request->simbol,
                'significant_decimals'     => $request->significant_decimals,
                'min_value'                => 1/pow(10,$request->significant_decimals),
                'decimal_point'            => $request->decimal_point,
                'thousands_separator'      => $request->thousands_separator,
                'max_inflation_percentage' => $request->max_inflation_percentage,
                'max_deflation_percentage' => $request->max_deflation_percentage,
                'user_id'                  => $user->id,
            ]);
            ExchangeRate::updateExchanges();
            $currency->refresh();
            $professions = Profession::all();
            foreach($professions as $profession){
                MaxAmountCredit::create([
                    'profession_id'        => $profession->id,
                    'currency_id'          => $currency->id,
                    'initial_limit'        => 0,
                    'max_limit'            => 0,
                    'active_credits_limit' => 0,
                    'user_id'              => $user->id,
                ]);
                $profession->refresh();
                $currency->refresh();
            }
            return $this->index();
        }
        return view('errors.404');
    }


    public function show($currency_id){
        //CreditManager role only
        if(auth()->user()->role->id == Role::getCreditManagerRole()->id){
            $currency = Currency::find($currency_id);
            $user = auth()->user();
            if($currency != null){
                $max_amount_credits = MaxAmountCredit::where('currency_id',$currency->id)->get();
                return view('site.currencies.show', compact('currency','max_amount_credits'));
            }
        }
        return view('errors.404');
    }

    public function edit($currency_id){
        //CreditManager role only
        if(auth()->user()->role->id == Role::getCreditManagerRole()->id){
            $currency = Currency::find($currency_id);
            if($currency != null){
                return view('site.currencies.edit',compact('currency'));
            }
        }
        return view('errors.404');
    }

    public function update(Request $request, $currency_id){
        //CreditManager role only
        $user = auth()->user();
        if($user->role->id == Role::getCreditManagerRole()->id){
            $currency = Currency::find($currency_id);
            if($currency != null){
                $currency->name                     = $request->name;
                $currency->iso_code                 = strtoupper($request->iso_code);
                $currency->simbol                   = $request->simbol;
                $currency->significant_decimals     = $request->significant_decimals;
                $currency->decimal_point            = $request->decimal_point;
                $currency->thousands_separator      = $request->thousands_separator;
                $currency->max_deflation_percentage = $request->max_deflation_percentage;
                $currency->max_inflation_percentage = $request->max_inflation_percentage;
                $currency->min_value                = 1/pow(10,$currency->significant_decimals);
                $currency->user_id                  = $user->id;
                $currency->save();
                $currency->refresh();
                return $this->show($currency_id);
            }
        }
        return view('errors.404');
    }

}
