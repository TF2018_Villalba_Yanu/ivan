<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MaxAmountCredit;
use App\Models\Role;

class MaxAmountCreditController extends Controller
{
    public function index(){
        //CreditManager role only
        if(auth()->user()->role->id == Role::getCreditManagerRole()->id){
            $max_amounts = MaxAmountCredit::all();
            return view('site.max_amounts.index',compact('max_amounts'));
        }
        return view('errors.404');
    }
    public function show($id){
        //CreditManager role only
        if(auth()->user()->role->id == Role::getCreditManagerRole()->id){
            $max_amount = MaxAmountCredit::find($id);
            if($max_amount != null){
                return view('site.max_amounts.show', compact('max_amount'));
            }
        }
        return view('errors.404');
    }
    public function edit($id){
        //CreditManager role only
        if(auth()->user()->role->id == Role::getCreditManagerRole()->id){
            $max_amount = MaxAmountCredit::find($id);
            if($max_amount != null){
                return view('site.max_amounts.edit',compact('max_amount'));
            }
        }
        return view('errors.404');
    }
    public function update(Request $request, $id){
        //CreditManager role only
        if(auth()->user()->role->id == Role::getCreditManagerRole()->id){
            $max_amount = MaxAmountCredit::find($id);
            if($max_amount != null){       
                $max_amount->initial_limit        = $request->initial_limit;
                $max_amount->max_limit            = $request->max_limit;
                $max_amount->active_credits_limit = $request->active_credits_limit;
                $max_amount->user_id              = auth()->user()->id;
                $max_amount->save();
                $max_amount->refresh();
                return $this->show($max_amount->id);
            }
        }
        return view('errors.404');
    }
}
