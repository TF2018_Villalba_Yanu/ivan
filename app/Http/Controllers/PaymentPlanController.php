<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PaymentPlan;
use App\Models\Currency;
use App\Models\Role;
use Illuminate\Database\Eloquent\Collection;
use App\Models\CustomDateInterval;
use App\Models\Interest;
use App\Models\Insurance;

class PaymentPlanController extends Controller
{
    public function index(){
        //CreditManager role only
        $user = auth()->user();
        if($user->role->id == Role::getCreditManagerRole()->id){
            $currencies = Currency::all();
            return view('payment-plans.index',compact('currencies')); 
        }
        return view('errors.404');
    }
    public function indexForCurrency($id){
        //CreditManager role only
        $user = auth()->user();
        if($user->role->id == Role::getCreditManagerRole()->id){
            $currency = Currency::find($id);
            if ($currency != null) {
                $payment_plans = $currency->paymentPlans;
                if ($payment_plans->count() != 0) {
                    return view('payment-plans.currency-index',compact('currency','payment_plans'));                    
                }else{
                    return view('payment-plans.empty-currency-index',compact('currency'));                        
                }
            }
        }
        return view('errors.404');
    }
    public function edit($id){
        //CreditManager role only
        $user = auth()->user();
        if($user->role->id == Role::getCreditManagerRole()->id){
            $payment_plan = PaymentPlan::find($id);
            if($payment_plan != null){
                return view('payment-plans.edit',compact('payment_plan'));
            }
        }
        return view('errors.404');
    }
    public function show($id)
    {
        //CreditManager role only
        $user = auth()->user();
        if($user->role->id == Role::getCreditManagerRole()->id){
            $payment_plan = PaymentPlan::find($id);
            if($payment_plan != null){
                return view('payment-plans.show',compact('payment_plan'));
            }
        }
        return view('errors.404');
    }
    public function update(Request $request, $id){
        /*
        Datos del $request
            payments_number
            due_days
            currency_id
            interest_min_percentage
            interest_initial_percentage
            interest_max_percentage
            interest_commission_percentage
            insurance_min_percentage
            insurance_initial_percentage
            insurance_max_percentage
            insurance_debtor_percentage
        */
        //CreditManager role only
        $user = auth()->user();
        if($user->role->id == Role::getCreditManagerRole()->id){
            $payment_plan = PaymentPlan::find($id);
            if($payment_plan != null){
                $months = 0;
                $days = $request->due_days;
                while($days >= 30){
                    $months++;
                    $days -= 30;
                }
                $due_dates_interval = CustomDateInterval::customCreate($months,$days,0,0,0);
                $payment_plan->interest->initial_percentage    = $request->interest_min_percentage;
                $payment_plan->interest->min_percentage        = $request->interest_initial_percentage;
                $payment_plan->interest->max_percentage        = $request->interest_max_percentage;
                $payment_plan->interest->commission_percentage = $request->interest_commission_percentage;
                $payment_plan->interest->user_id               = $user->id;
                $payment_plan->interest->save();

                $payment_plan->insurance->initial_percentage = $request->insurance_min_percentage;
                $payment_plan->insurance->min_percentage     = $request->insurance_initial_percentage;
                $payment_plan->insurance->max_percentage     = $request->insurance_max_percentage;
                $payment_plan->insurance->debtor_percentage  = $request->insurance_debtor_percentage;
                $payment_plan->insurance->user_id            = $user->id;
                $payment_plan->insurance->save();

                $payment_plan->payments_number       = $request->payments_number;
                $payment_plan->due_dates_interval_id = $due_dates_interval->id;
                $payment_plan->currency_id           = $request->currency_id;
                $payment_plan->user_id               = $user->id;
                $payment_plan->save();
                $payment_plan->refresh();
                return $this->show($id);
            }
        }
        return view('errors.404');
    }
    public function create(Request $request){
        /*
        Datos del $request
            payments_number
            due_days
            currency_id
            interest_min_percentage
            interest_initial_percentage
            interest_max_percentage
            interest_commission_percentage
            insurance_min_percentage
            insurance_initial_percentage
            insurance_max_percentage
            insurance_debtor_percentage
        */
        //CreditManager role only
        $user = auth()->user();
        if(auth()->user()->role->id == Role::getCreditManagerRole()->id){
            $interest = Interest::create([
                'initial_percentage'    => $request->interest_min_percentage,
                'min_percentage'        => $request->interest_initial_percentage,
                'max_percentage'        => $request->interest_max_percentage,
                'commission_percentage' => $request->interest_commission_percentage,
                'user_id'               => $user->id,
            ]);
            $insurance = Insurance::create([
                'initial_percentage' => $request->insurance_min_percentage,
                'min_percentage'     => $request->insurance_initial_percentage,
                'max_percentage'     => $request->insurance_max_percentage,
                'debtor_percentage'  => $request->insurance_debtor_percentage,
                'user_id'            => $user->id,
            ]);
            $months = 0;
            $days = $request->due_days;
            while($days >= 30){
                $months++;
                $days -= 30;
            }
            $due_dates_interval = CustomDateInterval::customCreate($months,$days,0,0,0);
            PaymentPlan::create([
                'payments_number'       => $request->payments_number,
                'due_dates_interval_id' => $due_dates_interval->id,
                'currency_id'           => $request->currency_id,
                'interest_id'           => $interest->id,
                'insurance_id'          => $insurance->id,
                'user_id'               => $user->id,
            ]);
            return $this->indexForCurrency($request->currency_id);
        }
        return view('errors.404');
    }
}
