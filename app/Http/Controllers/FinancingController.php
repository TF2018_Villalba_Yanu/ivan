<?php

namespace App\Http\Controllers;

use App\Models\Financing;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use App\Models\Wallet;
use App\Models\Currency;
use App\Models\Credit;
use App\Models\WalletTransaction;
use App\Models\WalletTypeOfTransaction;
use App\Models\CreditStage;
use App\Models\User;
use App\Models\Role;

class FinancingController extends Controller
{
    public function index(){
        //Solamente user
        $user = auth()->user();
        if($user->role->id == Role::getUserRole()->id){
            $items = $user->wallets;
            $wallets = new Collection();
            foreach ($items as $item) {
                if ($item->financings->count() > 0) {
                    $wallets->add($item);
                }
            }
            if($wallets->count() == 0){
                $wallets = $user->wallets;
                return view('financings.empty',compact('wallets'));
            }else if($wallets->count() == 1){
                 return $this->indexForCurrency($wallets->first()->currency->id);
            }else{
                return view('financings.index', compact('wallets'));
            }
        }
        return view('errors.404');
    }
    public function indexForCurrency($id){
        //Solamente user
        if(auth()->user()->role->id == Role::getUserRole()->id){
            $wallet = Wallet::where('currency_id',$id)
                    ->where('user_id',auth()->user()->id)
                    ->first();
            if (($wallet != null) && ($wallet->financings->count() > 0)) {
                return view('financings.wallet-index', compact('wallet'));
            }
        }
        return view('errors.404');
    }
    public function listOfFinancings($wallet_id){
        //Solamente user
        $user = auth()->user();
        if ($user->role->id == Role::getUserRole()->id) {
            $wallet = Wallet::find($wallet_id);
            if ($wallet != null) {
                if ($user->hasWallet($wallet)) {
                    $currency = $wallet->currency;
                    if($wallet->accountBalance() < $currency->min_value){
                        if($wallet->financings->count() > 0){
                            return $this->indexForCurrency($currency->id);
                        }else{
                            return $this->index();
                        }
                    }
                    $financing_credits = $wallet->currency->financingCredits();
                    if($financing_credits->count() >= 10){
                        $credits = $wallet->currency->financingCredits()->random(10);
                    }else{
                        $credits = $wallet->currency->financingCredits()->random($financing_credits->count());
                    }
                    if($financing_credits->count() == 0){
                        $wallets = $user->wallets;
                        
                        return view('financings.list-empty', compact('wallets','currency'));
                    }else{
                        return view('financings.list', compact('credits','wallet'));
                    }
                }
            }
        }
        return view('errors.404');
    }
    public function financiate(Request $request){
        /*
        _token: document.getElementsByName('_token')[0].value,
        amount: amount,
        credit_id: document.getElementById('credit-id').value,	
        insurance: document.getElementsByName('insurance')[0].value,
        */

        //Solamente user
        $user = auth()->user();
        if($user->role->id == Role::getUserRole()->id){
            $credit = Credit::find($request->credit_id);
            if ($credit != null) {
                $wallet = Wallet::where('user_id',$user->id)
                        ->where('currency_id',$credit->currency()->id)
                        ->first();
                if ($wallet != null) {
                    $financing_phase = Financing::financiate($credit,$wallet,$request->insurance,$request->amount);
                    if($financing_phase != null){
                    $profit_percentage = $financing_phase->financing->credit->interest_percentage - ($financing_phase->financing->credit->interest_percentage * $financing_phase->financing->credit->commission_percentage);
                    $profits_title = 'Ganancia esperada (estimado)';
                    if ($financing_phase->financing->have_insurance) {
                        $profit_percentage -= $financing_phase->financing->credit->creditor_insurance_percentage;
                        $profits_title = 'Ganancia esperada';
				    }
                    $profits =  $financing_phase->financing->amount() * $profit_percentage;
                    if($financing_phase->financing->amount() < $wallet->currency->min_value){
                        $profits = 0;
                    } 
                    return array(
                        'financing_phase_id'      => $financing_phase->id,
                        'financing_id'            => $financing_phase->financing->id,
                        'credit_id'               => $financing_phase->financing->credit->id,
                        'financing_amount_string' => $wallet->currency->numberToString($financing_phase->financing->amount()),
                        'financing_profits_title' => $profits_title,
                        'financing_profits_string'=> $wallet->currency->numberToString($profits),
                    );
                    }
                }
            }
        }
    }
    public function show($financing_id){
        //Solamente user
        $user = auth()->user();
        if($user->role->id == Role::getUserRole()->id){
            $financing = Financing::find($financing_id);
            if ($financing != null) {
                if ($user->hasFinancing($financing)) {
                    return view('financings.show',compact('financing'));
                }
            }
        }
    }
    public function abort(Request $request){
        /*
        $request:
        _token: document.getElementsByName('_token')[0].value,
		amount: amount,
        currency_id: currency_id,	
        */
        //Solamente user
        if(auth()->user()->role->id == Role::getUserRole()->id){
            $user = auth()->user();
            $financing = Financing::find($request->financing_id);
            if($financing != null){
                if($user->hasFinancing($financing)){
                    $financing_phase = $financing->abortFinancing($request->amount);
                    if($financing_phase != null){
                        $profit_percentage = $financing_phase->financing->credit->interest_percentage - ($financing_phase->financing->credit->interest_percentage * $financing_phase->financing->credit->commission_percentage);
                        $profits_title = 'Ganancia esperada (estimado)';
                        if ($financing_phase->financing->have_insurance) {
                            $profit_percentage -= $financing_phase->financing->credit->creditor_insurance_percentage;
                            $profits_title = 'Ganancia esperada';
                        }
                        $profits =  $financing_phase->financing->amount() * $profit_percentage;
                        if($financing_phase->financing->amount() < $financing->credit->currency()->min_value){
                            $profits = 0;
                        }                   
                        return array(
                            'financing_id'                   => $financing->id,
                            'credit_id'                      => $financing->credit->id,
                            'financing_cancel_amount_string' => $financing->credit->currency()->numberToString($request->amount),
                            'financing_amount_string'        => $financing->credit->currency()->numberToString($financing->amount()),
                            'financing_profits_title'        => $profits_title,
                            'financing_profits_string'       => $financing->credit->currency()->numberToString($profits),
                        );
                    }
                }
            }
        }
        return view('errors.404');
    }
}
