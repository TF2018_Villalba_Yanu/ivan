<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use App\Models\Country;
use Carbon\Carbon;
use App\Models\Role;
use App\Models\Profession;
use App\Models\Address;
use App\Models\Reputation;

class UserController extends Controller
{

    public function index(){
        //admin
        if(auth()->user()->role->id == Role::getAdminRole()->id){
            $users = User::all();
            return view('site.users.index',compact('users'));
        }
        return view('errors.404');
    }
    public function show($user_id){
        //admin
        if(auth()->user()->role->id == Role::getAdminRole()->id){
            $user = User::find($user_id);
            if($user != null){
                return view('site.users.show',compact('user'));
            }
        }
        return view('errors.404'); 
     }
    public function profile(){
        $user = auth()->user();
        return view('site.users.show', compact('user'));
    }
    public function edit($user_id){
        //admin para todos los user_id y cualquiera si ID== $user->id
        if((auth()->user()->role->id == Role::getAdminRole()->id)||(auth()->user()->id ==$user_id)){
            $user = User::find($user_id);
            if($user != null){
                $countries = Country::all();
                $birthdate_min = Carbon::now();
                $birthdate_min->addYears(-100);
                $birthdate_min = $birthdate_min->format('Y-m-d');
                $birthdate_max = Carbon::now();
                $birthdate_max->addYears(-15);
                $birthdate_max = $birthdate_max->format('Y-m-d');
                $professions = Profession::all();
                $roles = Role::all();
                return view('site.users.edit',compact('user','countries','birthdate_min','birthdate_max','roles','professions'));
            }
        }
        return view('errors.404');
    }

    public function store(Request $request, $user_id){
        //admin para todos los user_id y cualquiera si ID== $user->id
        if((auth()->user()->role->id == Role::getAdminRole()->id)||(auth()->user()->id ==$user_id)){
            $user = User::find($user_id);
            if($user != null){

                $user->address->city_id    = $request->city_id;
                $user->address->department = $request->address_department;
                $user->address->floor      = $request->address_floor;
                $user->address->street     = $request->address_street;
                $user->address->number     = $request->address_number;  
                $user->address->save();
                $user->address->refresh();
                
                $user->name          = $request->name;
                $user->phone         = $request->country_phone.$request->phone;
                $user->birthdate     = $request->birthdate;
                if ((auth()->user()->role->id == Role::getAdminRole()->id) && (auth()->user()->id != $user->id)){
                    $user->role_id = $request->role_id;
                }else{
                    $user->password      = bcrypt($request->password);
                }
                $user->profession_id = $request->profession_id;
                $user->user_id       = auth()->user()->id;
            
                if(auth()->user()->role->id == Role::getAdminRole()->id){
                    $user->role_id = $request->role_id;
                }
                $user->save();
                $user->refresh();
                $user->reputation->baseAmount();
                if(auth()->user()->role->id == Role::getAdminRole()->id){
                    return $this->show($user->id);
                }else{
                    return $this->profile();
                }
            }
        }
        return view('errors.404');
    }

    public function create(Request $request){
        //admin
        if(auth()->user()->role->id == Role::getAdminRole()->id){
            $address = Address::create([
                'street'     => $request->address_street,
                'number'     => $request->address_number,
                'department' => $request->address_department,
                'floor'      => $request->address_floor,
                'city_id'    => $request->city_id,
            ]);
            User::create([
                'name'          => $request->name,
                'phone'         => $request->phone,
                'email'         => $request->email,
                'password'      => bcrypt($request->password),
                'role_id'       => $request->role_id,
                'reputation_id' => Reputation::create()->id,
                'address_id'    => $address->id,
                'profession_id' => $request->profession_id,
                'birthdate'     => $request->birthdate,
            ]);
            return $this->index();
        }
        return view('errors.404');
    }   
}
