<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Reputation;
use App\Models\Role;
use App\Models\Credit;

class ReputationController extends Controller
{
    public function creditManagerShow($reputation_id,$credit_id){
        //CreditManager role only
        if(auth()->user()->role->id == Role::getCreditManagerRole()->id){
            $reputation = Reputation::find($reputation_id);
            if($reputation != null){
                $credit = Credit::find($credit_id);
                if ($credit != null) {
                    return view('reputations.show',compact('reputation','credit'));
                }
            }
        }
        return view('errors.404');
    }
    public function show(){
        //Solamente user
        $user = auth()->user();
        if($user->role->id == Role::getUserRole()->id){
            $reputation = $user->reputation;
            return view('reputations.show',compact('reputation'));
        }
        return view('errors.404');
    }
}
