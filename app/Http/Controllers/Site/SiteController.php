<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SiteController extends Controller
{
    public function index(){
        return view('site.welcome.welcome');
    }
    public function error404(Request $request=null){
        return view('errors.404');
    }
}
