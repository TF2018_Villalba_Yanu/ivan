<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Role;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /*
            getUserRole()---------------> /reputation
            getAdminRole()--------------> /users
            getParamManagerRole()-------> /profile
            getInsuranceManagerRole()---> /wallets
            getCommissionManagerRole()--> /wallets
            getLawsuitManagerRole()-----> /credits/judicial-citation
            getDefaulterManagerRole()---> /credits/defaulter
            getCreditManagerRole()------> /credits/to-approve
            getAuditRole()--------------> /audits/count=1000
        */
        if(auth()->user()->role->id == Role::getUserRole()->id){
            return redirect('/reputation');
        }
        if(auth()->user()->role->id == Role::getAdminRole()->id){
            return redirect('/users');
        }
        if(auth()->user()->role->id == Role::getParamManagerRole()->id){
            return redirect('/profile');
        }        
        if((auth()->user()->role->id == Role::getInsuranceManagerRole()->id) || (auth()->user()->role->id == Role::getCommissionManagerRole()->id)){
            return redirect('/wallets');
        }
        if(auth()->user()->role->id == Role::getLawsuitManagerRole()->id){
            return redirect('/credits/judicial-citation');
        }
        if(auth()->user()->role->id == Role::getDefaulterManagerRole()->id){
            return redirect('/credits/defaulter');
        }
        if(auth()->user()->role->id == Role::getCreditManagerRole()->id){
            return redirect('/credits/to-approve');
        }
        if(auth()->user()->role->id == Role::getAuditRole()->id){
            return redirect('/audits/count=1000');
        }
        return view('home');
    }
}
