<?php

namespace App\Http\Controllers;

use App\Models\Wallet;
use Illuminate\Http\Request;
use App\Models\Currency;
use App\Models\WalletTransaction;
use App\Models\WalletTypeOfTransaction;
use Illuminate\Database\Eloquent\Collection;
use App\Models\Role;
class WalletController extends Controller
{
    public function inputMoney(Request $request){
        //user, insurance_manager y commision_manager
        $role = auth()->user()->role;
        if(($role->id == Role::getUserRole()->id)||($role->id == Role::getInsuranceManagerRole()->id)||($role->id == Role::getCommissionManagerRole()->id)){
            $currency = Currency::find($request->currency_id);
            $wallet_transaction = Wallet::inputMoney(auth()->user(),$currency,$request->amount);
            if($wallet_transaction != null){
                return array(
                    'wallet_transaction_id' => $wallet_transaction->id,
                    'currency_name'         => ucfirst($currency->name),
                    'input_amount_string'   => $currency->numberToString($wallet_transaction->amount),
                    'wallet_id'             => $wallet_transaction->wallet->id,
                );
            }
        }
        return null;
     }
    public function outputMoney(Request $request){
        //user, insurance_manager y commision_manager
        $role = auth()->user()->role;
        if(($role->id == Role::getUserRole()->id)||($role->id == Role::getInsuranceManagerRole()->id)||($role->id == Role::getCommissionManagerRole()->id)){
            $wallet = Wallet::where('currency_id',$request->currency_id)
                            ->where('user_id', auth()->user()->id)
                            ->first();
            if($wallet != null){
                if(auth()->user()->hasWallet($wallet)){
                    $wallet_transaction = $wallet->outputMoney($request->amount);
                    if($wallet_transaction != null){
                        return array(
                            'wallet_transaction_id' => $wallet_transaction->id,
                            'currency_name'         => ucfirst($wallet_transaction->wallet->currency->name),
                            'output_amount_string'   => $wallet_transaction->wallet->currency->numberToString($wallet_transaction->amount * -1),
                            'wallet_id'             => $wallet_transaction->wallet->id,
                        );
                    }
                }
            }
        }
        return null;
     }
    public function index(){
        //user, insurance_manager y commision_manager
        $user = auth()->user();        
        $role = $user->role;
        if(($role->id == Role::getUserRole()->id)||($role->id == Role::getInsuranceManagerRole()->id)||($role->id == Role::getCommissionManagerRole()->id)){
            $items = $user->wallets;
            $wallets = new Collection();
            foreach ($items as $item) {
                if ($item->walletTransactions->count() > 0) {
                    $wallets->add($item);
                }
            }
            if($wallets->count() == 0){
                return view('wallets.empty');
            }else if($wallets->count() == 1){
                 return $this->show($wallets->first()->id);
            }else{
                return view('wallets.index', compact('wallets'));
            }
        }
        return view('errors.404');
     }
    public function show($wallet_id){
        //user, insurance_manager y commision_manager
        $role = auth()->user()->role;
        if(($role->id == Role::getUserRole()->id)||($role->id == Role::getInsuranceManagerRole()->id)||($role->id == Role::getCommissionManagerRole()->id)){
            $wallet = Wallet::find($wallet_id);
            if($wallet != null){
                if(auth()->user()->hasWallet($wallet)){
                    return view('wallets.show',compact('wallet'));
                }
            }
        }
        return view('errors.404');
     }
}
