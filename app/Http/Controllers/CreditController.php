<?php

namespace App\Http\Controllers;
use App\Models\Credit;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use App\Models\Wallet;
use App\Models\PaymentPlan;
use App\Models\CreditPhase;
use App\Models\Currency;
use App\Models\CreditStage;
use App\Models\WalletTypeOfTransaction;
use App\Models\WalletTransaction;
use App\Models\Role;
use App\Models\User;
use App\Models\ExchangeRate;
use App\Models\Exchange;
use Carbon\CarbonInterval;

class CreditController extends Controller
{
    public function index(){
        //Solamente user
        $user = auth()->user();
        if($user->role->id == Role::getUserRole()->id){
            $items = $user->wallets;
            $wallets = new Collection();
            foreach ($items as $item) {
                if ($item->credits->count() > 0) {
                    $wallets->add($item);
                }
            }
            if($wallets->count() == 0){
                return view('credits.empty');
            }else if($wallets->count() == 1){
                 return $this->indexForCurrency($wallets->first()->currency->id);
            }else{
                return view('credits.index', compact('wallets'));
            }
        }
        return view('errors.404');
     }
    public function indexForCurrency($id){
        //Solamente user
        if(auth()->user()->role->id == Role::getUserRole()->id){
            $wallet = Wallet::where('currency_id',$id)
                    ->where('user_id',auth()->user()->id)
                    ->first();
            if (($wallet != null) && ($wallet->credits->count() > 0)) {
                return view('credits.wallet-index', compact('wallet'));
            }
        }
        return view('errors.404');
    }
    public function indexToApprove(){
        //Solamente elcreditManager
        if(auth()->user()->role->id == Role::getCreditManagerRole()->id){
            $currencies = Currency::all();
            $items = new Collection();
            foreach ($currencies as $currency) {
                $credits = $currency->creditsToApprove();
                if ($credits->count() > 0) {
                    $items->add($credits);
                }
            }
            if($items->count() == 0){
                return view('credits.to-approve.empty');
            }else if($items->count() == 1){
                return $this->indexToApproveForCurrency($items->first()->first()->currency()->id);
            }else{
                return view('credits.to-approve.index',compact('items'));                
            }
        }
        return view('errors.404'); 
     }
    public function indexToApproveForCurrency($id){
        //Solamente elcreditManager
        if(auth()->user()->role->id == Role::getCreditManagerRole()->id){
            $currency = Currency::find($id);
            if ($currency != null) {
                $credits = $currency->creditsToApprove();
                if ($credits->count() > 0) {
                    return view('credits.to-approve.currency-index', compact('credits'));
                }else{
                    return $this->indexToApprove();
                }
            }
        }
        return view('errors.404');
    }
    public function showToApprove($credit_id){
        //Solamente elcreditManager
        if(auth()->user()->role->id == Role::getCreditManagerRole()->id){
           $credit = Credit::find($credit_id);
           if($credit != null){
               if($credit->lastCreditPhase()->creditStage->id == CreditStage::getFirst()->id){
                   return view('credits.to-approve.show',compact('credit'));
               }
           }
       }
       return view('errors.404'); 
    }
    public function indexLawsuit(){
        //Solamente lawsuitManagerRole
        if(auth()->user()->role->id == Role::getLawsuitManagerRole()->id){
            $currencies = Currency::all();
            $items = new Collection();
            foreach ($currencies as $currency) {
                $credits = $currency->creditsLawsuit();
                if ($credits->count() > 0) {
                    $items->add($credits);
                }
            }
            if($items->count() == 0){
                return view('credits.lawsuit.empty');
            }else if($items->count() == 1){
                return $this->indexLawsuitForCurrency($items->first()->first()->currency()->id);
            }else{
                return view('credits.lawsuit.index',compact('items'));                
            }
        }
        return view('errors.404'); 
     }
    public function indexLawsuitForCurrency($id){
        //Solamente lawsuitManagerRole
        if(auth()->user()->role->id == Role::getLawsuitManagerRole()->id){
            $currency = Currency::find($id);
            if ($currency != null) {
                $credits = $currency->creditsLawsuit();
                if ($credits->count() > 0) {
                    return view('credits.lawsuit.currency-index', compact('credits'));
                }else{
                    return $this->indexLawsuit();
                }
            }
        }
        return view('errors.404');
    }
    public function showLawsuit($credit_id){
        //Solamente lawsuitManagerRole
        if(auth()->user()->role->id == Role::getLawsuitManagerRole()->id){
           $credit = Credit::find($credit_id);
           if($credit != null){
               if($credit->lastCreditPhase()->creditStage->id == CreditStage::getLawsuit()->id){
                   return view('credits.lawsuit.show',compact('credit'));
               }
           }
       }
       return view('errors.404'); 
    }
    public function indexBad(){
        //Solamente lawsuitManagerRole
        if(auth()->user()->role->id == Role::getLawsuitManagerRole()->id){
            $currencies = Currency::all();
            $items = new Collection();
            foreach ($currencies as $currency) {
                $credits = $currency->creditsBad();
                if ($credits->count() > 0) {
                    $items->add($credits);
                }
            }
            if($items->count() == 0){
                return view('credits.bad.empty');
            }else if($items->count() == 1){
                return $this->indexBadForCurrency($items->first()->first()->currency()->id);
            }else{
                return view('credits.bad.index',compact('items'));                
            }
        }
        return view('errors.404'); 
     }
    public function indexBadForCurrency($id){
        //Solamente lawsuitManagerRole
        if(auth()->user()->role->id == Role::getLawsuitManagerRole()->id){
            $currency = Currency::find($id);
            if ($currency != null) {
                $credits = $currency->creditsBad();
                if ($credits->count() > 0) {
                    return view('credits.bad.currency-index', compact('credits'));
                }else{
                    return $this->indexBad();
                }
            }
        }
        return view('errors.404');
    }
    public function showBad($credit_id){
        //Solamente lawsuitManagerRole
        if(auth()->user()->role->id == Role::getLawsuitManagerRole()->id){
           $credit = Credit::find($credit_id);
           if($credit != null){
               if($credit->lastCreditPhase()->creditStage->id == CreditStage::getBad()->id){
                   return view('credits.bad.show',compact('credit'));
               }
           }
       }
       return view('errors.404'); 
    }
    public function indexJudicialCitation(){
        //Solamente lawsuitManagerRole
        if(auth()->user()->role->id == Role::getLawsuitManagerRole()->id){
            $currencies = Currency::all();
            $items = new Collection();
            foreach ($currencies as $currency) {
                $credits = $currency->creditsJudicialCitation();
                if ($credits->count() > 0) {
                    $items->add($credits);
                }
            }
            if($items->count() == 0){
                return view('credits.judicial-citation.empty');
            }else if($items->count() == 1){
                return $this->indexJudicialCitationForCurrency($items->first()->first()->currency()->id);
            }else{
                return view('credits.judicial-citation.index',compact('items'));                
            }
        }
        return view('errors.404'); 
     }
    public function indexJudicialCitationForCurrency($id){
        //Solamente lawsuitManagerRole
        if(auth()->user()->role->id == Role::getLawsuitManagerRole()->id){
            $currency = Currency::find($id);
            if ($currency != null) {
                $credits = $currency->creditsJudicialCitation();
                if ($credits->count() > 0) {
                    return view('credits.judicial-citation.currency-index', compact('credits'));
                }else{
                    return $this->indexJudicialCitation();
                }
            }
        }
        return view('errors.404');
    }
    public function showJudicialCitation($credit_id){
        //Solamente lawsuitManagerRole
        if(auth()->user()->role->id == Role::getLawsuitManagerRole()->id){
           $credit = Credit::find($credit_id);
           if($credit != null){
               if($credit->lastCreditPhase()->creditStage->id == CreditStage::getJudicialCitation()->id){
                   return view('credits.judicial-citation.show',compact('credit'));
               }
           }
       }
       return view('errors.404'); 
    }
    public function indexLatePayment(){
        //Solamente defaulterManagerRole
        if(auth()->user()->role->id == Role::getDefaulterManagerRole()->id){
            $currencies = Currency::all();
            $items = new Collection();
            foreach ($currencies as $currency) {
                $credits = $currency->creditsLatePayment();
                if ($credits->count() > 0) {
                    $items->add($credits);
                }
            }
            if($items->count() == 0){
                return view('credits.late-payment.empty');
            }else if($items->count() == 1){
                return $this->indexLatePaymentForCurrency($items->first()->first()->currency()->id);
            }else{
                return view('credits.late-payment.index',compact('items'));                
            }
        }
        return view('errors.404'); 
     }
    public function indexLatePaymentForCurrency($id){
        //Solamente defaulterManagerRole
        if(auth()->user()->role->id == Role::getDefaulterManagerRole()->id){
            $currency = Currency::find($id);
            if ($currency != null) {
                $credits = $currency->creditsLatePayment();
                if ($credits->count() > 0) {
                    return view('credits.late-payment.currency-index', compact('credits'));
                }else{
                    return $this->indexLatePayment();
                }
            }
        }
        return view('errors.404');
    }
    public function showLatePayment($credit_id){
        //Solamente defaulterManagerRole
        if(auth()->user()->role->id == Role::getDefaulterManagerRole()->id){
           $credit = Credit::find($credit_id);
           if($credit != null){
               if($credit->lastCreditPhase()->creditStage->id == CreditStage::getPaymentLate()->id){
                   return view('credits.late-payment.show',compact('credit'));
               }
           }
       }
       return view('errors.404'); 
    }
    public function indexDefaulter(){
        //Solamente defaulterManagerRole
        if(auth()->user()->role->id == Role::getDefaulterManagerRole()->id){
            $currencies = Currency::all();
            $items = new Collection();
            foreach ($currencies as $currency) {
                $credits = $currency->creditsDefaulter();
                if ($credits->count() > 0) {
                    $items->add($credits);
                }
            }
            if($items->count() == 0){
                return view('credits.defaulter.empty');
            }else if($items->count() == 1){
                return $this->indexDefaulterForCurrency($items->first()->first()->currency()->id);
            }else{
                return view('credits.defaulter.index',compact('items'));                
            }
        }
        return view('errors.404'); 
     }
    public function indexDefaulterForCurrency($id){
        //Solamente defaulterManagerRole
        if(auth()->user()->role->id == Role::getDefaulterManagerRole()->id){
            $currency = Currency::find($id);
            if ($currency != null) {
                $credits = $currency->creditsDefaulter();
                if ($credits->count() > 0) {
                    return view('credits.defaulter.currency-index', compact('credits'));
                }else{
                    return $this->indexDefaulter();
                }
            }
        }
        return view('errors.404');
    }
    public function showDefaulter($credit_id){
        //Solamente defaulterManagerRole
        if(auth()->user()->role->id == Role::getDefaulterManagerRole()->id){
           $credit = Credit::find($credit_id);
           if($credit != null){
               if($credit->lastCreditPhase()->creditStage->id == CreditStage::getDefaulter()->id){
                   return view('credits.defaulter.show',compact('credit'));
               }
           }
       }
       return view('errors.404'); 
    }
    public function pay(Request $request){
        /*
            Request:
        	_token
			credit_id
			pay_amount
			from_amount
			exchange_rate_id
			wallet_id
        */
        //Solamente user
        $user = auth()->user();
        if($user->role->id == Role::getUserRole()->id){
            $credit = Credit::find($request->credit_id);
            if ($credit != null) {
                if ($user->hasCredit($credit)) {
                    $exchange_rate = ExchangeRate::find($request->exchange_rate_id);
                    if($exchange_rate != null){
                        $wallet = Wallet::find($request->wallet_id);
                        if($wallet != null){
                            $exchange = Exchange::exchange($wallet,$credit->currency(),$request->from_amount);
                            if($exchange == null){
                                echo('Error 7100265');
                                dd();
                            }
                        }
                    }
                    $wallet_transaction = $credit->wallet->payCredit($credit,$request->pay_amount);
                    if($wallet_transaction != null){
                        return array(
                            'credit_id'            => $credit->id,
                            'credit_amount_string' => $credit->currency()->numberToString($credit->amount),
                            'payed_amount_string'  => $credit->currency()->numberToString($request->pay_amount),
                        );
                    }
                }
            }
        }
    }
    public function create(Request $request){
        /*
            Request:
            _token
			amount
			payment_plan_id
        */
        //Solamente user
        if(auth()->user()->role->id == Role::getUserRole()->id){
            $user = User::find(auth()->user()->id);
            $payment_plan = PaymentPlan::find($request->payment_plan_id);
            if($payment_plan != null){
                $credit_phase = Credit::createCredit($user,$payment_plan,$request->amount);
                if($credit_phase != null){
                    return array(
                        'credit_phase_id'      => $credit_phase->id,
                        'credit_id'            => $credit_phase->credit->id,
                        'credit_amount_string' => $credit_phase->credit->currency()->numberToString($credit_phase->credit->amount),
                    );
                }
            }
        }
        return view('errors.404');
     }
    public function grant(Request $request){ 
        //Solamente user
        $user = auth()->user();
        if($user->role->id == Role::getUserRole()->id){
            $credit = Credit::find($request->credit_id);
            if($credit != null){
                if ($user->hasCredit($credit)){
                    $credit_phase = $credit->grant();
                    if($credit_phase != null){
                        return array(
                            'credit_phase_id'            => $credit_phase->id,
                            'credit_id'                  => $credit_phase->credit->id,
                            'credit_amount_string'       => $credit_phase->credit->currency()->numberToString($credit_phase->credit->amount),
                            'payments_numer'             => $credit_phase->credit->payments->count(),
                            'payments_amount_string'     => $credit_phase->credit->currency()->numberToString($credit_phase->credit->payments->first()->amount()),
                            'due_date_for_first_payment' => $credit_phase->credit->payments->first()->due_date->format('d/m/Y').' ('.$credit_phase->credit->payments->first()->due_date->diffForHumans().')',
                        );
                    }
                }
            }
        }
     }
    public function show($credit_id){
        //user o creditManager
        if((auth()->user()->role->id == Role::getUserRole()->id)||(auth()->user()->role->id == Role::getCreditManagerRole()->id)){
            $credit = Credit::find($credit_id);
            $user = auth()->user();
            if($credit != null){
                if (($user->hasCredit($credit))||($user->role->id == Role::getCreditManagerRole()->id)){
                    return view('credits.show', compact('credit','user'));
                }
            }
        }
        return view('errors.404');
        
     }
    public function abort(Request $request){
        $user = auth()->user();
        //Solamente user
        if($user->role->id == Role::getUserRole()->id){
            $credit = Credit::find($request->credit_id);
            if($credit != null) {
                if ($user->hasCredit($credit)) {
                    $credit_phase = $credit->abort();
                    if($credit_phase != null){
                        return array(
                            'credit_phase_id'      => $credit_phase->id,
                            'credit_id'            => $credit_phase->credit->id,
                            'credit_amount_string' => $credit_phase->credit->currency()->numberToString($credit_phase->credit->amount),
                        );
                    }
                }
            }
        }           
    }
    public function approvate(Request $request){
         //Solamente elcreditManager
        if(auth()->user()->role->id == Role::getCreditManagerRole()->id){
            $credit = Credit::find($request->credit_id);
            if($credit != null){
                if($credit->lastCreditPhase()->creditStage->id == CreditStage::getFirst()->id){
                    $credit_phase = $credit->approvate();
                    if($credit_phase != null){
                        return array(
                            'credit_phase_id'      => $credit_phase->id,
                            'credit_id'            => $credit_phase->credit->id,
                            'credit_amount_string' => $credit_phase->credit->currency()->numberToString($credit_phase->credit->amount),
                            'currency_id'          => $credit_phase->credit->currency()->id,
                        );
                    }
                }
            }
        }
     }
    public function citation(Request $request){
        //Solamente defaulterManagerRole
        if(auth()->user()->role->id == Role::getDefaulterManagerRole()->id){
           $credit = Credit::find($request->credit_id);
           if($credit != null){
               if($credit->lastCreditPhase()->creditStage->id == CreditStage::getDefaulter()->id){
                   $credit_phase = $credit->citation();
                   if($credit_phase != null){
                       return array(
                           'credit_phase_id'      => $credit_phase->id,
                           'credit_id'            => $credit_phase->credit->id,
                           'credit_amount_string' => $credit_phase->credit->currency()->numberToString($credit_phase->credit->amount),
                           'currency_id'          => $credit_phase->credit->currency()->id,
                        );
                    }
                }
            }
        }
    }
    public function lawsuit(Request $request){
        //Solamente lawsuitManagerRole
        if(auth()->user()->role->id == Role::getLawsuitManagerRole()->id){
            $credit = Credit::find($request->credit_id);
            if($credit != null){
               if($credit->lastCreditPhase()->creditStage->id == CreditStage::getJudicialCitation()->id){
                   $credit_phase = $credit->lawsuit();
                   if($credit_phase != null){
                       return array(
                           'credit_phase_id'      => $credit_phase->id,
                           'credit_id'            => $credit_phase->credit->id,
                           'credit_amount_string' => $credit_phase->credit->currency()->numberToString($credit_phase->credit->amount),
                           'currency_id'          => $credit_phase->credit->currency()->id,
                        );
                    }
                }
            }
        }
    }
    public function bad(Request $request){
        //Solamente lawsuitManagerRole
        if(auth()->user()->role->id == Role::getLawsuitManagerRole()->id){
            $credit = Credit::find($request->credit_id);
            if($credit != null){
               if($credit->lastCreditPhase()->creditStage->id == CreditStage::getLawsuit()->id){
                   $credit_phase = $credit->bad();
                   if($credit_phase != null){
                       return array(
                           'credit_phase_id'      => $credit_phase->id,
                           'credit_id'            => $credit_phase->credit->id,
                           'credit_amount_string' => $credit_phase->credit->currency()->numberToString($credit_phase->credit->amount),
                           'currency_id'          => $credit_phase->credit->currency()->id,
                        );
                    }
                }
            }
        }
    }
    public function reject(Request $request){
        //Solamente elcreditManager
        if(auth()->user()->role->id == Role::getCreditManagerRole()->id){
            $credit = Credit::find($request->credit_id);
            if($credit != null){
                if($credit->lastCreditPhase()->creditStage->id == CreditStage::getFirst()->id){
                    $credit_phase = $credit->reject();
                    if($credit_phase != null){
                        return array(
                            'credit_phase_id'      => $credit_phase->id,
                            'credit_id'            => $credit_phase->credit->id,
                            'credit_amount_string' => $credit_phase->credit->currency()->numberToString($credit_phase->credit->amount),
                            'currency_id'          => $credit_phase->credit->currency()->id,
                        );
                    }
                }
            }
        }
     }
}
