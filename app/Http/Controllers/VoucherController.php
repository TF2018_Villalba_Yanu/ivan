<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Role;
use App\Models\Voucher;

class VoucherController extends Controller
{
    public function index(){
        //Solamente user
        $user = auth()->user();
        if($user->role->id == Role::getUserRole()->id){
            $vouchers = $user->vouchers;
            if($vouchers->count() == 0){
                return view('vouchers.empty');
            }else{
                return view('vouchers.index',compact('vouchers'));
            }
        }
        return view('errors.404');
    }
    public function show($id){
        //Solamente user
        $user = auth()->user();
        if($user->role->id == Role::getUserRole()->id){
            $voucher = Voucher::find($id);
            if($voucher != null){
                if($user->hasVoucher($voucher)){
                    return view('vouchers.show',compact('voucher'));
                }
            }
        }
        return view('errors.404');
    }
}
