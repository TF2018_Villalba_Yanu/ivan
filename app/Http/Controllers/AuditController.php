<?php

namespace App\Http\Controllers;

use App\Models\Audit;
use Illuminate\Http\Request;
use App\Models\Role;

class AuditController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($number)
    {
        //Audit role only
        $user = auth()->user();
        if(auth()->user()->role->id == Role::getAuditRole()->id){
            $audits = Audit::all()->reverse();
            if (($number == '1000') || ($number == '10000') || ($number == '100000')){
                if($audits->count() > $number){
                    $audits = $audits->take($number);
                }
                return view('audits.index', compact('audits'));
            }
            if ($number == 'all'){
                return view('audits.index', compact('audits'));
            }
        }
        return view('errors.404');
    }

    public function show($id)
    {
        //Audit role only
        $user = auth()->user();
        if(auth()->user()->role->id == Role::getAuditRole()->id){
            $audit = Audit::find($id);
            return view('audits.show', compact('audit'));
        }
        return view('errors.404');
    }
}
