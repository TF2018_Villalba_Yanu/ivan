<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Profession;
use App\Models\Role;
use App\Models\Currency;
use App\Models\MaxAmountCredit;

class ProfessionController extends Controller
{
    public function index(){
        //CreditManager role only
        if(auth()->user()->role->id == Role::getCreditManagerRole()->id){
            $professions = Profession::all();
            return view('site.professions.index',compact('professions'));
        }
        return view('errors.404');
    }
    public function create(Request $request){
        //CreditManager role only
        $user = auth()->user();
        if($user->role->id == Role::getCreditManagerRole()->id){
            $profession = Profession::create([
                'name'    => $request->name,
                'user_id' => $user->id,
            ]);
            $profession->refresh();
            $currencies = Currency::all();
            foreach($currencies as $currency){
                MaxAmountCredit::create([
                    'profession_id'        => $profession->id,
                    'currency_id'          => $currency->id,
                    'initial_limit'        => 0,
                    'max_limit'            => 0,
                    'active_credits_limit' => 0,
                    'user_id'              => $user->id,
                ]);
            }
            return $this->index();
        }
        return view('errors.404');
    }
    public function show($id){
        //creditManager
        if(auth()->user()->role->id == Role::getCreditManagerRole()->id){
            $user = auth()->user();
            $profession = Profession::find($id);
            if($profession != null){
                $max_amount_credits = MaxAmountCredit::where('profession_id',$profession->id)->get();
                return view('site.professions.show', compact('profession','max_amount_credits'));
            }
        }
        return view('errors.404');
    }
    public function edit($id){
        //CreditManager role only
        if(auth()->user()->role->id == Role::getCreditManagerRole()->id){
            $profession = Profession::find($id);
            if($profession != null){
                return view('site.professions.edit',compact('profession'));
            }
        }
        return view('errors.404');
    }
    public function update(Request $request,$id){
        //CreditManager role only
        $user = auth()->user();
        if($user->role->id == Role::getCreditManagerRole()->id){
            $profession = Profession::find($id);
            if($profession != null){
                $profession->name = $request->name;
                $profession->user_id = $user->id;
                $profession->save();
                $profession->refresh();
                return $this->show($profession->id);
            }
        }
        return view('errors.404');
    }
}
