<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Role;
use App\Models\Wallet;
use App\Models\Currency;
use App\Models\ExchangeRate;
use App\Models\Exchange;

class ExchangeController extends Controller
{
    public function index(){
        //Solamente user
        $user = auth()->user();
        if($user->role->id == Role::getUserRole()->id){
            $wallets   = $user->wallets;
            $exchanges = $user->exchanges();
            if($exchanges->count() == 0){
                return view('exchanges.empty',compact('wallets'));
            }else{
                return view('exchanges.index',compact('exchanges','wallets'));
            }
        }
    }
    public function create(Request $request){
        //data: {
        //    _token: document.getElementsByName('_token')[0].value,
        //    amount_from: amount_from,
        //    currency_from: currency_from,
        //    currency_to: currency_to,	
        //    exchange_rate : exchange_rate,
        //},
        //Solamente user
        $user = auth()->user();
        if($user->role->id == Role::getUserRole()->id){
            $wallet = Wallet::where('user_id',$user->id)->where('currency_id',$request->currency_from)->first();
            if(($wallet != null) && ($wallet->accountBalance() >= $request->amount_from)){
                $currency = Currency::find($request->currency_to);
                if($currency != null){
                    $exchange_rate = ExchangeRate::find($request->exchange_rate);
                    if($exchange_rate != null){
                        $exchange = Exchange::exchange($wallet, $currency, $request->amount_from, $exchange_rate);
                        if($exchange != null){
                            return array(
                                'exchange_id'        => $exchange->id,
                                'from_currency_name' => ucfirst($exchange->exchangeRate->fromCurrency->name),
                                'to_currency_name'   => ucfirst($exchange->exchangeRate->toCurrency->name),
                                'from_amount_string' => $exchange->exchangeRate->fromCurrency->numberToString($exchange->convert(0)),
                                'to_amount_string'   => $exchange->exchangeRate->toCurrency->numberToString($exchange->convert(1)),
                                'exchange_rate'      => '1 '.$exchange->exchangeRate->fromCurrency->iso_code.' = '.$exchange->exchangeRate->fromCurrency->truncate($exchange_rate->rate).' '.$exchange->exchangeRate->toCurrency->iso_code,
                            );
                        }
                    }
                }
            }
        }
    }
    public function show($id){
        //Solamente user
        $user = auth()->user();
        if($user->role->id == Role::getUserRole()->id){
            $exchange = Exchange::find($id);
            if($id != null){
                if($user->hasExchange($exchange)){
                    return view('exchanges.show',compact('exchange'));
                }
            }
        }
    }
}
