<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Payment;
use Illuminate\Database\Eloquent\Collection;
use App\Models\PaymentStage;
use App\Models\Role;
use App\Models\Credit;

class PaymentController extends Controller
{
    public function show($payment_id){
        //user o creditManager
        if((auth()->user()->role->id == Role::getUserRole()->id)||(auth()->user()->role->id == Role::getCreditManagerRole()->id)){
            $user = auth()->user();
            $payment = Payment::find($payment_id);
            if($payment != null){
                if($user->hasPayment($payment)){
                    return view('payments.show', compact('payment'));
                }
            }
        }
        return view('errors.404');
    }
    public function index($credit_id){
        //Solamente user
        $user = auth()->user();
        if($user->role->id == Role::getUserRole()->id){
            $credit = Credit::find($credit_id);
            if ($user->hasCredit($credit)) {
                $payments = $credit->payments;
                return view('payments.index', compact('payments','credit'));
            }
        }
        return view('errors.404');
    }
}
