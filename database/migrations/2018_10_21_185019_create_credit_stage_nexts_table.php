<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreditStageNextsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('credit_stage_nexts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('credit_stage_id')->unsigned();
            $table->foreign('credit_stage_id')->references('id')->on('credit_stages');
            $table->integer('credit_stage_next_id')->unsigned();
            $table->foreign('credit_stage_next_id')->references('id')->on('credit_stages');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('credit_stage_flows');
    }
}
