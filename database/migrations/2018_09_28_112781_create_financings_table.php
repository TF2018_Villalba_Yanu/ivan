<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFinancingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('financings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->boolean('have_insurance');
            //Foreign keys
            $table->integer('wallet_id')->unsigned();
            $table->integer('credit_id')->unsigned();
            //References
            $table->foreign('wallet_id')->references('id')->on('wallets');
            $table->foreign('credit_id')->references('id')->on('credits');
            $table->integer('user_id')->unsigned()->nullable();//Este campo se va a usar para el log de auditoria que esta en un trigger en la BD
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('financings');
    }
}
