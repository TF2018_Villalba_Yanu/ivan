<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWalletTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wallet_transactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('wallet_id')->unsigned();
            $table->foreign('wallet_id')->references('id')->on('wallets');
            $table->integer('wallet_type_of_transaction_id')->unsigned();
            $table->foreign('wallet_type_of_transaction_id')->references('id')->on('wallet_type_of_transactions');
            $table->double('amount');
            $table->dateTime('date');

            $table->bigInteger('input_money_transaction_id')->unsigned()->nullable();
            $table->foreign('input_money_transaction_id')->references('id')->on('input_money_transactions');

            $table->bigInteger('output_money_transaction_id')->unsigned()->nullable();
            $table->foreign('output_money_transaction_id')->references('id')->on('output_money_transactions');

            $table->bigInteger('credit_phase_id')->unsigned()->nullable();
            $table->foreign('credit_phase_id')->references('id')->on('credit_phases');

            $table->bigInteger('financing_phase_id')->unsigned()->nullable();
            $table->foreign('financing_phase_id')->references('id')->on('financing_phases');

            $table->bigInteger('payment_phase_id')->unsigned()->nullable();
            $table->foreign('payment_phase_id')->references('id')->on('payment_phases');

            $table->bigInteger('repayment_id')->unsigned()->nullable();
            $table->foreign('repayment_id')->references('id')->on('repayments');

            $table->bigInteger('exchange_id')->unsigned()->nullable();
            $table->foreign('exchange_id')->references('id')->on('exchanges');

            $table->integer('user_id')->unsigned()->nullable();//Este campo se va a usar para el log de auditoria que esta en un trigger en la BD
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wallet_transactions');
    }
}
