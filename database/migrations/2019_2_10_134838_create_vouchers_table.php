<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVouchersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vouchers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('voucher_type_id')->unsigned();
            $table->foreign('voucher_type_id')->references('id')->on('voucher_types');
            $table->integer('currency_id')->unsigned();
            $table->foreign('currency_id')->references('id')->on('currencies');
            $table->double('amount');
            $table->dateTime('date');

            $table->bigInteger('wallet_transaction_id')->unsigned()->nullable();
            $table->foreign('wallet_transaction_id')->references('id')->on('wallet_transactions');
            
            $table->bigInteger('credit_phase_id')->unsigned()->nullable();
            $table->foreign('credit_phase_id')->references('id')->on('credit_phases');
            
            $table->bigInteger('financing_phase_id')->unsigned()->nullable();
            $table->foreign('financing_phase_id')->references('id')->on('financing_phases');

            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vouchers');
    }
}
