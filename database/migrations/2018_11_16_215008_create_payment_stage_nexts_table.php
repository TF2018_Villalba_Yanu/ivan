<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentStageNextsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_stage_nexts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('payment_stage_id')->unsigned();
            $table->foreign('payment_stage_id')->references('id')->on('payment_stages');
            $table->integer('payment_stage_next_id')->unsigned();
            $table->foreign('payment_stage_next_id')->references('id')->on('payment_stages');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_stage_nexts');
    }
}
