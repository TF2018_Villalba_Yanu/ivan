<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInsuranceModifiersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('insurance_modifiers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('reputation_id')->unsigned();
            $table->foreign('reputation_id')->references('id')->on('reputations');
            $table->dateTime('date');
            $table->double('percentage');
            $table->integer('insurance_modifier_stage_id')->unsigned();
            $table->foreign('insurance_modifier_stage_id')->references('id')->on('insurance_modifier_stages');
            $table->integer('user_id')->unsigned()->nullable();//Este campo se va a usar para el log de auditoria que esta en un trigger en la BD
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('insurance_modifiers');
    }
}
