<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCurrenciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('currencies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('iso_code',3);
            $table->char('simbol',5)->default('$');
            $table->integer('significant_decimals')->unsigned();
            $table->double('min_value')->unsigned();
            $table->char('decimal_point',1)->default(',');
            $table->char('thousands_separator',1)->default('.');
            $table->double('max_inflation_percentage')->defaul(0.05);
            $table->double('max_deflation_percentage')->defaul(-0.1);
            $table->integer('user_id')->unsigned()->nullable();//Este campo se va a usar para el log de auditoria que esta en un trigger en la BD
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('currencies');
    }
}
