<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documents', function (Blueprint $table) {
            $table->increments('id');
            //Foreign keys
            $table->integer('document_type_id')->unsigned();
            $table->integer('country_id')->unsigned();
            $table->integer('user_id')->unsigned();

            $table->string('number');
            $table->string('front_image');
            $table->string('back_image');
            $table->string('face_image');
            //References
            $table->foreign('document_type_id')->references('id')->on('document_types');
            $table->foreign('country_id')->references('id')->on('countries');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documents');
    }
}
