<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExchangesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exchanges', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('from_wallet_id')->unsigned();
            $table->foreign('from_wallet_id')->references('id')->on('wallets');
            $table->integer('to_wallet_id')->unsigned();
            $table->foreign('to_wallet_id')->references('id')->on('wallets');
            $table->bigInteger('exchange_rate_id')->unsigned();
            $table->foreign('exchange_rate_id')->references('id')->on('exchange_rates');
            $table->dateTime('date');
            $table->double('from_amount');
            $table->integer('user_id')->unsigned()->nullable();//Este campo se va a usar para el log de auditoria que esta en un trigger en la BD
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exchanges');
    }
}
