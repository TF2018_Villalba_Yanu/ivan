<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMarginModifiersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('margin_modifiers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('reputation_id')->unsigned();
            $table->foreign('reputation_id')->references('id')->on('reputations');
            $table->integer('margin_modifier_stage_id')->unsigned();
            $table->foreign('margin_modifier_stage_id')->references('id')->on('margin_modifier_stages');
            $table->dateTime('date');
            $table->double('amount');
            $table->integer('user_id')->unsigned()->nullable();//Este campo se va a usar para el log de auditoria que esta en un trigger en la BD
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('margin_modifiers');
    }
}
