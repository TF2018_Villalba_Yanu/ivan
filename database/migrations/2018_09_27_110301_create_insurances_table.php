<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInsurancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('insurances', function (Blueprint $table) {
            $table->increments('id');
            $table->float('initial_percentage');
            $table->float('min_percentage');
            $table->float('max_percentage');
            /*el seguro por no pagar se abona un porcentage por el acreedor (persona que presta)
            * y otro porcentage por el deudor (persona que debe el dinero).
            * lo importante es que'debtor_percentage' <= 1
            * Si vale 1, el deudor abona todo.
            */
            $table->float('debtor_percentage')->default(0.3);
            $table->integer('user_id')->unsigned()->nullable()->default(null);//Este campo se va a usar para el log de auditoria que esta en un trigger en la BD
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('insurances');
    }
}
