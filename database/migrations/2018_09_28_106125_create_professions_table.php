<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfessionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('professions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('user_id')->unsigned()->nullable();//Este campo se va a usar para el log de auditoria que esta en un trigger en la BD
        });
    }


    /**
     * Reverse the migrations.
     *
     */
    public function down()
    {
        Schema::dropIfExists('professions');
    }
}
