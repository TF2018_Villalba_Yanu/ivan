<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFinancingStageNextsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('financing_stage_nexts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('financing_stage_id')->unsigned();
            $table->foreign('financing_stage_id')->references('id')->on('financing_stages');
            $table->integer('financing_stage_next_id')->unsigned();
            $table->foreign('financing_stage_next_id')->references('id')->on('financing_stages');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('financing_stage_nexts');
    }
}
