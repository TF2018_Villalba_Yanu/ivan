<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            //QUITAR LOS NULLABLES DESPUES DE ARREGLAR EL SIGNUP
            $table->increments('id');
            $table->string('name',30);
            $table->string('phone',14)->nullable();
            $table->string('email',100)->unique();
            $table->string('password');
            $table->integer('role_id')->unsigned()->defaul(1);// Rol 1 es usuario
            $table->foreign('role_id')->references('id')->on('roles');
            $table->integer('reputation_id')->unsigned();
            $table->foreign('reputation_id')->references('id')->on('reputations');
            $table->dateTime('birthdate')->nullable();
            $table->integer('user_id')->unsigned()->nullable();//Este campo se va a usar para el log de auditoria que esta en un trigger en la BD
            /**
             * interest_modifier: moficara los intereses de crédito y seguro.
             * Si vale 0.10, entonces 'el seguro' seria igual al MENOR de ($insurance->initial_percentage + $interest_modifier) o ($insurance->max_percentage)
             * Si, por el contrario, vale -0.25, seria igual al MAYOR de ($insurance->initial_percentage - $interest_modifier) o ($insurance->min_percentage)
             * Es decir, un valor negativo decrementa el porcentage y uno positivo lo aumenta
            **/
            
            $table->integer('address_id')->unsigned()->nullable();
            $table->integer('profession_id')->unsigned()->nullable();
            $table->rememberToken();
            //Reference
            $table->foreign('address_id')->references('id')->on('addresses');
            $table->foreign('profession_id')->references('id')->on('professions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
