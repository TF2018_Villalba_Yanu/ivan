<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReputationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reputations', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('retained_margin_id')->nullable()->unsigned();
            $table->integer('max_amount_credit_id')->unsigned()->nullable();
            $table->foreign('max_amount_credit_id')->references('id')->on('max_amount_credits');
            $table->integer('default_currency_id')->unsigned()->nullable();
            $table->foreign('default_currency_id')->references('id')->on('currencies');
            $table->double('one_usd_in_default_currency')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reputations');
    }
}
