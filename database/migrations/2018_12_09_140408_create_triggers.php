<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTriggers extends Migration
{

    public function up()
    {
        DB::unprepared('CREATE TRIGGER trigger_insurances_ai AFTER INSERT ON insurances FOR EACH ROW 
            INSERT INTO audits (_db_user, _system_user_id, _table, _data, _action, _new_data, _date) 
            VALUES (
                CURRENT_USER(),
                NEW.user_id,
                "insurances",
                CONCAT_WS(", ", "id", "initial_percentage", "min_percentage", "max_percentage", "debtor_percentage"),
                "insert",
                CONCAT_WS(", ", NEW.id, NEW.initial_percentage, NEW.min_percentage, NEW.max_percentage, NEW.debtor_percentage),
                NOW()
            )
         ');
        DB::unprepared('CREATE TRIGGER trigger_insurances_bu BEFORE UPDATE ON insurances FOR EACH ROW 
            INSERT INTO audits (_db_user, _system_user_id, _table, _data, _action, _old_data, _new_data, _date) 
            VALUES (
                CURRENT_USER(),
                NEW.user_id,
                "insurances",
                CONCAT_WS(", ", "id", "initial_percentage", "min_percentage", "max_percentage", "debtor_percentage"),
                "update",
                CONCAT_WS(", ", OLD.id, OLD.initial_percentage, OLD.min_percentage, OLD.max_percentage, OLD.debtor_percentage),
                CONCAT_WS(", ", NEW.id, NEW.initial_percentage, NEW.min_percentage, NEW.max_percentage, NEW.debtor_percentage),
                NOW()
            )
         ');
        DB::unprepared('CREATE TRIGGER trigger_interests_ai AFTER INSERT ON interests FOR EACH ROW 
            INSERT INTO audits (_db_user, _system_user_id, _table, _data, _action, _new_data, _date) 
            VALUES (
                CURRENT_USER(),
                NEW.user_id,
                "interests",
                CONCAT_WS(", ", "id", "initial_percentage", "min_percentage", "max_percentage", "commission_percentage"),
                "insert",
                CONCAT_WS(", ", NEW.id, NEW.initial_percentage, NEW.min_percentage, NEW.max_percentage, NEW.commission_percentage),
                NOW()
            )
         ');
        DB::unprepared('CREATE TRIGGER trigger_interests_bu BEFORE UPDATE ON interests FOR EACH ROW 
            INSERT INTO audits (_db_user, _system_user_id, _table, _data, _action, _old_data, _new_data, _date) 
            VALUES (
                CURRENT_USER(),
                NEW.user_id,
                "interests",
                CONCAT_WS(", ", "id", "initial_percentage", "min_percentage", "max_percentage", "commission_percentage"),
                "update",
                CONCAT_WS(", ", OLD.id, OLD.initial_percentage, OLD.min_percentage, OLD.max_percentage, OLD.commission_percentage),
                CONCAT_WS(", ", NEW.id, NEW.initial_percentage, NEW.min_percentage, NEW.max_percentage, NEW.commission_percentage),
                NOW()
            )
         ');
        DB::unprepared('CREATE TRIGGER trigger_currencies_ai AFTER INSERT ON currencies FOR EACH ROW 
            INSERT INTO audits (_db_user, _system_user_id, _table, _data, _action, _new_data, _date) 
            VALUES (
                CURRENT_USER(),
                NEW.user_id,
                "currencies",
                CONCAT_WS(", ", "id", "name", "iso_code", "simbol", "significant_decimals", "min_value", "decimal_point", "thousands_separator", "max_inflation_percentage", "max_deflation_percentage"),
                "insert",
                CONCAT_WS(", ", NEW.id, NEW.name, NEW.iso_code, NEW.simbol, NEW.significant_decimals, NEW.min_value, NEW.decimal_point, NEW.thousands_separator, NEW.max_inflation_percentage, NEW.max_deflation_percentage),
                NOW()
            )
         ');
        DB::unprepared('CREATE TRIGGER trigger_currencies_bu BEFORE UPDATE ON currencies FOR EACH ROW 
            INSERT INTO audits (_db_user, _system_user_id, _table, _data, _action, _old_data, _new_data, _date) 
            VALUES (
                CURRENT_USER(),
                NEW.user_id,
                "currencies",
                CONCAT_WS(", ", "id", "name", "iso_code", "simbol", "significant_decimals", "min_value", "decimal_point", "thousands_separator", "max_inflation_percentage", "max_deflation_percentage"),
                "update",
                CONCAT_WS(", ", OLD.id, OLD.name, OLD.iso_code, OLD.simbol, OLD.significant_decimals, OLD.min_value, OLD.decimal_point, OLD.thousands_separator, OLD.max_inflation_percentage, OLD.max_deflation_percentage),
                CONCAT_WS(", ", NEW.id, NEW.name, NEW.iso_code, NEW.simbol, NEW.significant_decimals, NEW.min_value, NEW.decimal_point, NEW.thousands_separator, NEW.max_inflation_percentage, NEW.max_deflation_percentage),
                NOW()
            )
         ');
        DB::unprepared('CREATE TRIGGER trigger_professions_ai AFTER INSERT ON professions FOR EACH ROW 
            INSERT INTO audits (_db_user, _system_user_id, _table, _data, _action, _new_data, _date) 
            VALUES (
                CURRENT_USER(),
                NEW.user_id,
                "professions",
                CONCAT_WS(", ", "id", "name"),
                "insert",
                CONCAT_WS(", ", NEW.id, NEW.name),
                NOW()
            )
         ');
        DB::unprepared('CREATE TRIGGER trigger_professions_bu BEFORE UPDATE ON professions FOR EACH ROW 
            INSERT INTO audits (_db_user, _system_user_id, _table, _data, _action, _old_data, _new_data, _date) 
            VALUES (
                CURRENT_USER(),
                NEW.user_id,
                "professions",
                CONCAT_WS(", ", "id", "name"),
                "update",
                CONCAT_WS(", ", OLD.id, OLD.name),
                CONCAT_WS(", ", NEW.id, NEW.name),
                NOW()
            )
         ');
        DB::unprepared('CREATE TRIGGER trigger_max_amount_credits_ai AFTER INSERT ON max_amount_credits FOR EACH ROW 
            INSERT INTO audits (_db_user, _system_user_id, _table, _data, _action, _new_data, _date) 
            VALUES (
                CURRENT_USER(),
                NEW.user_id,
                "max_amount_credits",
                CONCAT_WS(", ", "id", "profession_id", "currency_id", "initial_limit", "max_limit", "active_credits_limit"),
                "insert",
                CONCAT_WS(", ", NEW.id, NEW.profession_id, NEW.currency_id, NEW.initial_limit, NEW.max_limit, NEW.active_credits_limit),
                NOW()
            )
         ');
        DB::unprepared('CREATE TRIGGER trigger_max_amount_credits_bu BEFORE UPDATE ON max_amount_credits FOR EACH ROW 
            INSERT INTO audits (_db_user, _system_user_id, _table, _data, _action, _old_data, _new_data, _date) 
            VALUES (
                CURRENT_USER(),
                NEW.user_id,
                "max_amount_credits",
                CONCAT_WS(", ", "id", "profession_id", "currency_id", "initial_limit", "max_limit", "active_credits_limit"),
                "update",
                CONCAT_WS(", ", OLD.id, OLD.profession_id, OLD.currency_id, OLD.initial_limit, OLD.max_limit, OLD.active_credits_limit),
                CONCAT_WS(", ", NEW.id, NEW.profession_id, NEW.currency_id, NEW.initial_limit, NEW.max_limit, NEW.active_credits_limit),
                NOW()
            )
         ');
        DB::unprepared('CREATE TRIGGER trigger_payment_plans_ai AFTER INSERT ON payment_plans FOR EACH ROW 
            INSERT INTO audits (_db_user, _system_user_id, _table, _data, _action, _new_data, _date) 
            VALUES (
                CURRENT_USER(),
                NEW.user_id,
                "payment_plans",
                CONCAT_WS(", ", "id", "payments_number", "due_dates_interval_id", "currency_id", "interest_id", "insurance_id"),
                "insert",
                CONCAT_WS(", ", NEW.id, NEW.payments_number, NEW.due_dates_interval_id, NEW.currency_id, NEW.interest_id, NEW.insurance_id),
                NOW()
            )
         ');
        DB::unprepared('CREATE TRIGGER trigger_payment_plans_bu BEFORE UPDATE ON payment_plans FOR EACH ROW 
            INSERT INTO audits (_db_user, _system_user_id, _table, _data, _action, _old_data, _new_data, _date) 
            VALUES (
                CURRENT_USER(),
                NEW.user_id,
                "payment_plans",
                CONCAT_WS(", ", "id", "payments_number", "due_dates_interval_id", "currency_id", "interest_id", "insurance_id"),
                "update",
                CONCAT_WS(", ", OLD.id, OLD.payments_number, OLD.due_dates_interval_id, OLD.currency_id, OLD.interest_id, OLD.insurance_id),
                CONCAT_WS(", ", OLD.id, OLD.payments_number, OLD.due_dates_interval_id, OLD.currency_id, OLD.interest_id, OLD.insurance_id),
                NOW()
            )
         ');
        DB::unprepared('CREATE TRIGGER trigger_users_bu BEFORE UPDATE ON users FOR EACH ROW 
            INSERT INTO audits (_db_user, _system_user_id, _table, _data, _action, _old_data, _new_data, _date) 
            VALUES (
                CURRENT_USER(),
                NEW.user_id,
                "users",
                CONCAT_WS(", ", "id", "name", "phone", "email", "password", "role_id", "reputation_id", "birthdate"),
                "update",
                CONCAT_WS(", ", OLD.id, OLD.name, OLD.phone, OLD.email, OLD.password, OLD.role_id, OLD.reputation_id, OLD.birthdate),
                CONCAT_WS(", ", NEW.id, NEW.name, NEW.phone, NEW.email, NEW.password, NEW.role_id, NEW.reputation_id, NEW.birthdate),
                NOW()
            )
         ');
        DB::unprepared('CREATE TRIGGER trigger_credits_ai AFTER INSERT ON credits FOR EACH ROW 
            INSERT INTO audits (_db_user, _system_user_id, _table, _data, _action, _new_data, _date) 
            VALUES (
                CURRENT_USER(),
                NEW.user_id,
                "credits",
                CONCAT_WS(", ", "id", "commission_percentage", "creditor_insurance_percentage", "debtor_insurance_percentage", "payments_number", "wallet_id", "grace_period_id", "interest_for_late_payment", "due_dates_interval_id"),
                "insert",
                CONCAT_WS(", ", NEW.id, NEW.commission_percentage, NEW.creditor_insurance_percentage, NEW.debtor_insurance_percentage, NEW.payments_number, NEW.wallet_id, NEW.grace_period_id, NEW.interest_for_late_payment, NEW.due_dates_interval_id),
                NOW()
            )
         ');
        DB::unprepared('CREATE TRIGGER trigger_payments_ai AFTER INSERT ON payments FOR EACH ROW 
            INSERT INTO audits (_db_user, _system_user_id, _table, _data, _action, _new_data, _date) 
            VALUES (
                CURRENT_USER(),
                NEW.user_id,
                "payments",
                CONCAT_WS(", ", "id", "due_date", "credit_id"),

                "insert",
                CONCAT_WS(", ", NEW.id, NEW.due_date, NEW.credit_id),
                NOW()
            )
         ');
        DB::unprepared('CREATE TRIGGER trigger_financings_ai AFTER INSERT ON financings FOR EACH ROW 
            INSERT INTO audits (_db_user, _system_user_id, _table, _data, _action, _new_data, _date) 
            VALUES (
                CURRENT_USER(),
                NEW.user_id,
                "financings",
                CONCAT_WS(", ", "id", "have_insurance", "wallet_id", "credit_id"),
                "insert",
                CONCAT_WS(", ", NEW.id, NEW.have_insurance, NEW.wallet_id, NEW.credit_id),
                NOW()
            )
         ');
        DB::unprepared('CREATE TRIGGER trigger_payment_phases_ai AFTER INSERT ON payment_phases FOR EACH ROW 
            INSERT INTO audits (_db_user, _system_user_id, _table, _data, _action, _new_data, _date) 
            VALUES (
                CURRENT_USER(),
                NEW.user_id,
                "payment_phases",
                CONCAT_WS(", ", "id", "amount", "date", "payment_id", "payment_id", "payment_stage_id"),
                "insert",
                CONCAT_WS(", ", NEW.id, NEW.amount, NEW.date, NEW.payment_id, NEW.payment_id, NEW.payment_stage_id),
                NOW()
            )
         ');
        DB::unprepared('CREATE TRIGGER trigger_credit_phases_ai AFTER INSERT ON credit_phases FOR EACH ROW 
            INSERT INTO audits (_db_user, _system_user_id, _table, _data, _action, _new_data, _date) 
            VALUES (
                CURRENT_USER(),
                NEW.user_id,
                "credit_phases",
                CONCAT_WS(", ", "id", "credit_id", "credit_stage_id", "credit_stage_id", "date"),
                "insert",
                CONCAT_WS(", ", NEW.id, NEW.credit_id, NEW.credit_stage_id, NEW.credit_stage_id, NEW.date),
                NOW()
            )
         ');
        DB::unprepared('CREATE TRIGGER trigger_input_money_transactions_ai AFTER INSERT ON input_money_transactions FOR EACH ROW 
            INSERT INTO audits (_db_user, _system_user_id, _table, _data, _action, _new_data, _date) 
            VALUES (
                CURRENT_USER(),
                NEW.user_id,
                "input_money_transactions",
                CONCAT_WS(", ", "id"),
                "insert",
                CONCAT_WS(", ", NEW.id),
                NOW()
            )
         ');
        DB::unprepared('CREATE TRIGGER trigger_output_money_transactions_ai AFTER INSERT ON output_money_transactions FOR EACH ROW 
            INSERT INTO audits (_db_user, _system_user_id, _table, _data, _action, _new_data, _date) 
            VALUES (
                CURRENT_USER(),
                NEW.user_id,
                "output_money_transactions",
                CONCAT_WS(", ", "id"),
                "insert",
                CONCAT_WS(", ", NEW.id),
                NOW()
            )
         ');
        DB::unprepared('CREATE TRIGGER trigger_repayments_ai AFTER INSERT ON repayments FOR EACH ROW 
            INSERT INTO audits (_db_user, _system_user_id, _table, _data, _action, _new_data, _date) 
            VALUES (
                CURRENT_USER(),
                NEW.user_id,
                "repayments",
                CONCAT_WS(", ", "id", "financing_id", "payment_phase_id", "amount"),
                "insert",
                CONCAT_WS(", ", NEW.id, NEW.financing_id, NEW.payment_phase_id, NEW.amount),
                NOW()
            )
         ');
        DB::unprepared('CREATE TRIGGER trigger_financing_phases_ai AFTER INSERT ON financing_phases FOR EACH ROW 
            INSERT INTO audits (_db_user, _system_user_id, _table, _data, _action, _new_data, _date) 
            VALUES (
                CURRENT_USER(),
                NEW.user_id,
                "financing_phases",
                CONCAT_WS(", ", "id", "amount", "date", "financing_id", "financing_stage_id", "repayment_id"),
                "insert",
                CONCAT_WS(", ", NEW.id, NEW.amount, NEW.date, NEW.financing_id, NEW.financing_stage_id, NEW.repayment_id),
                NOW()
            )
         ');
        DB::unprepared('CREATE TRIGGER trigger_wallet_transactions_ai AFTER INSERT ON wallet_transactions FOR EACH ROW 
            INSERT INTO audits (_db_user, _system_user_id, _table, _data, _action, _new_data, _date) 
            VALUES (
                CURRENT_USER(),
                NEW.user_id,
                "wallet_transactions",
                CONCAT_WS(", ", "id", "wallet_id", "wallet_type_of_transaction_id", "amount", "date", "input_money_transaction_id", "output_money_transaction_id", "credit_phase_id", "financing_phase_id", "payment_phase_id", "repayment_id"),
                "insert",
                CONCAT_WS(", ", NEW.id, NEW.wallet_id, NEW.wallet_type_of_transaction_id, NEW.amount, NEW.date, NEW.input_money_transaction_id, NEW.output_money_transaction_id, NEW.credit_phase_id, NEW.financing_phase_id, NEW.payment_phase_id, NEW.repayment_id),
                NOW()
            )
         ');
        DB::unprepared('CREATE TRIGGER trigger_interest_modifiers_ai AFTER INSERT ON interest_modifiers FOR EACH ROW 
            INSERT INTO audits (_db_user, _system_user_id, _table, _data, _action, _new_data, _date) 
            VALUES (
                CURRENT_USER(),
                NEW.user_id,
                "interest_modifiers",
                CONCAT_WS(", ", "id", "reputation_id", "date", "percentage"),
                "insert",
                CONCAT_WS(", ", NEW.id, NEW.reputation_id, NEW.date, NEW.percentage),
                NOW()
            )
         ');
        DB::unprepared('CREATE TRIGGER trigger_insurance_modifiers_ai AFTER INSERT ON insurance_modifiers FOR EACH ROW 
            INSERT INTO audits (_db_user, _system_user_id, _table, _data, _action, _new_data, _date) 
            VALUES (
                CURRENT_USER(),
                NEW.user_id,
                "insurance_modifiers",
                CONCAT_WS(", ", "id", "reputation_id", "date", "percentage"),
                "insert",
                CONCAT_WS(", ", NEW.id, NEW.reputation_id, NEW.date, NEW.percentage),
                NOW()
            )
         ');
        DB::unprepared('CREATE TRIGGER trigger_margin_modifiers_ai AFTER INSERT ON margin_modifiers FOR EACH ROW 
            INSERT INTO audits (_db_user, _system_user_id, _table, _data, _action, _new_data, _date) 
            VALUES (
                CURRENT_USER(),
                NEW.user_id,
                "margin_modifiers",
                CONCAT_WS(", ", "id", "reputation_id", "margin_modifier_stage_id", "date", "amount"),
                "insert",
                CONCAT_WS(", ", NEW.id, NEW.reputation_id, NEW.margin_modifier_stage_id, NEW.date, NEW.amount),
                NOW()
            )
         ');
        DB::unprepared('CREATE TRIGGER trigger_exchanges_ai AFTER INSERT ON exchanges FOR EACH ROW 
            INSERT INTO audits (_db_user, _system_user_id, _table, _data, _action, _new_data, _date) 
            VALUES (
                CURRENT_USER(),
                NEW.user_id,
                "exchanges",
                CONCAT_WS(", ", "id", "from_wallet_id", "to_wallet_id", "exchange_rate_id", "date", "from_amount"),
                "insert",
                CONCAT_WS(", ", NEW.id, NEW.from_wallet_id, NEW.to_wallet_id, NEW.exchange_rate_id, NEW.date, NEW.from_amount),
                NOW()
            )
         ');     
    }
    public function down()
    {
        DB::unprepared('DROP TRIGGER trigger_insurances_ai');
        DB::unprepared('DROP TRIGGER trigger_insurances_bu');
        DB::unprepared('DROP TRIGGER trigger_interests_ai');
        DB::unprepared('DROP TRIGGER trigger_interests_bu');
        DB::unprepared('DROP TRIGGER trigger_currencies_ai');
        DB::unprepared('DROP TRIGGER trigger_currencies_bu');
        DB::unprepared('DROP TRIGGER trigger_professions_ai');
        DB::unprepared('DROP TRIGGER trigger_professions_bu');
        DB::unprepared('DROP TRIGGER trigger_max_amount_credits_ai');
        DB::unprepared('DROP TRIGGER trigger_max_amount_credits_bu');
        DB::unprepared('DROP TRIGGER trigger_payment_plans_ai');
        DB::unprepared('DROP TRIGGER trigger_payment_plans_bu');
        DB::unprepared('DROP TRIGGER trigger_users_bu');
        DB::unprepared('DROP TRIGGER trigger_credits_ai');
        DB::unprepared('DROP TRIGGER trigger_payments_ai');
        DB::unprepared('DROP TRIGGER trigger_financings_ai');
        DB::unprepared('DROP TRIGGER trigger_payment_phases_ai');
        DB::unprepared('DROP TRIGGER trigger_credit_phases_ai');
        DB::unprepared('DROP TRIGGER trigger_input_money_transactions');
        DB::unprepared('DROP TRIGGER trigger_output_money_transaction');
        DB::unprepared('DROP TRIGGER trigger_repayments_ai');
        DB::unprepared('DROP TRIGGER trigger_financing_phases_ai');
        DB::unprepared('DROP TRIGGER trigger_wallet_transactions_ai');
        DB::unprepared('DROP TRIGGER trigger_interest_modifiers_ai');
        DB::unprepared('DROP TRIGGER trigger_insurance_modifiers_ai');
        DB::unprepared('DROP TRIGGER trigger_margin_modifiers_ai');
        DB::unprepared('DROP TRIGGER trigger_exchanges_ai');
    }
}
