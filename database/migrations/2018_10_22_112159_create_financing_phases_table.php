<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFinancingPhasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('financing_phases', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->double('amount');
            $table->dateTime('date');
            $table->bigInteger('financing_id')->unsigned();
            $table->foreign('financing_id')->references('id')->on('financings');
            $table->integer('financing_stage_id')->unsigned();
            $table->foreign('financing_stage_id')->references('id')->on('financing_stages');
            $table->bigInteger('repayment_id')->unsigned()->nullable();
            $table->foreign('repayment_id')->references('id')->on('repayments');
            $table->integer('user_id')->unsigned()->nullable();//Este campo se va a usar para el log de auditoria que esta en un trigger en la BD
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('financing_phases');
    }
}
