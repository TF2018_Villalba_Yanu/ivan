<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreditsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('credits', function (Blueprint $table) {
            $table->increments('id');
            $table->double('amount');
            $table->double('interest_percentage');
            //Se copia de intereses, tal cual.
            //Porcentage de comision del interes, es decir, si el interes es 0.45% y la comision = 0.43%, la comision real seria = 0.45*0.43 = 0,1935%
            //El el ejemplo anterior, el acreedor cobraria 0.45 - 0,1935 = 0,2565%
            // 0,1935% + 0,2565% = 0.45%
            $table->double('commission_percentage');
            $table->double('creditor_insurance_percentage');
            $table->double('debtor_insurance_percentage');
            $table->string('payments_number');
            $table->integer('wallet_id')->unsigned();
            $table->foreign('wallet_id')->references('id')->on('wallets');
            $table->integer('grace_period_id')->unsigned();
            $table->foreign('grace_period_id')->references('id')->on('custom_date_intervals');                   
            $table->float('interest_for_late_payment');
            $table->integer('due_dates_interval_id')->unsigned();
            $table->foreign('due_dates_interval_id')->references('id')->on('custom_date_intervals');
            $table->integer('user_id')->unsigned()->nullable();//Este campo se va a usar para el log de auditoria que esta en un trigger en la BD
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('credits');
    }
}
