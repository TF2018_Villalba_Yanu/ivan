<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->dateTime('due_date');
            //FK 
            $table->integer('credit_id')->unsigned();
            //reference
            $table->foreign('credit_id')->references('id')->on('credits');
            $table->integer('user_id')->unsigned()->nullable();//Este campo se va a usar para el log de auditoria que esta en un trigger en la BD
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
