
<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Carbon\CarbonPeriod;


class CreatePaymentPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_plans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('payments_number')->unsigned();
            $table->integer('due_dates_interval_id')->unsigned();
            $table->foreign('due_dates_interval_id')->references('id')->on('custom_date_intervals');
            $table->integer('currency_id')->unsigned();
            $table->foreign('currency_id')->references('id')->on('currencies');
            $table->integer('interest_id')->unsigned();
            $table->foreign('interest_id')->references('id')->on('interests');
            $table->integer('insurance_id')->unsigned();
            $table->foreign('insurance_id')->references('id')->on('insurances');
            $table->integer('user_id')->unsigned()->nullable();//Este campo se va a usar para el log de auditoria que esta en un trigger en la BD           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_plans');
    }
}
