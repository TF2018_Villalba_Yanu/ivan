<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInterestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('interests', function (Blueprint $table) {
            $table->increments('id');
            $table->float('initial_percentage');
            $table->float('min_percentage');
            $table->float('max_percentage');
            //Porcentage de comision del interes, es decir, si el interes es 0.45% y la comision = 0.43%, la comision real seria = 0.45*0.43 = 0,1935%
            //El el ejemplo anterior, el acreedor cobraria 0.45 - 0,1935 = 0,2565%
            // 0,1935% + 0,2565% = 0.45%
            $table->float('commission_percentage');
            $table->integer('user_id')->unsigned()->nullable();//Este campo se va a usar para el log de auditoria que esta en un trigger en la BD
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('interests');
    }
}
