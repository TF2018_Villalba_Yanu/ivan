<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInterestModifiersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('interest_modifiers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('reputation_id')->unsigned();
            $table->foreign('reputation_id')->references('id')->on('reputations');
            $table->dateTime('date');
            $table->double('percentage');
            $table->integer('interest_modifier_stage_id')->unsigned();
            $table->foreign('interest_modifier_stage_id')->references('id')->on('interest_modifier_stages');
            $table->integer('user_id')->unsigned()->nullable();//Este campo se va a usar para el log de auditoria que esta en un trigger en la BD
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('interest_modifiers');
    }
}
