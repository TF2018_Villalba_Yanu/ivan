<?php

use Faker\Generator as Faker;
use App\Models\Address;
use App\Models\City;


$factory->define(Address::class, function (Faker $faker) {
    return [
        'street' => $faker->name. ' '.$faker->name,
        'number' => mt_rand(1,6000),
        'department'=> $faker->randomElement ([
            strval(mt_rand(1,30)),
            strval(mt_rand(1,10)),
            strval(mt_rand(1,20)),
            strval(mt_rand(1,5)),
            strval(mt_rand(1,15)),
            strval(mt_rand(1,3)),
            strtoupper(str_random(mt_rand(1,2)))
        ]),
        'floor'=> (integer)$faker->randomElement([
            mt_rand(1,1),
            mt_rand(1,1),
            mt_rand(1,1),
            mt_rand(1,2),
            mt_rand(1,2),
            mt_rand(1,3),
            mt_rand(1,5),
            mt_rand(1,10),
            mt_rand(1,15),
            mt_rand(1,30),
            mt_rand(1,50),
        ]),
        'city_id'=>City::all()->random()->id,
    ];
});
