<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class DueAndInterests extends Seeder
{
    public static function seeder($date)
    {
        $current_date     = new Carbon($date[0]->toDateTimeString());
        $next_due_credits = new Carbon($date[1]->toDateTimeString());
        $current_date->addHours(4);
        $next_due_credits->addHours(24);
        if ($date[0] < Carbon::now()){
            if($current_date > $next_due_credits){
                echo('Entrando a DueAnInterests
                ');
                $date[0] = new Carbon($current_date->toDateTimeString());
                $date[1] = new Carbon($next_due_credits->toDateTimeString());
                $date = DueCredits::seeder($date);
                $date = AddInterests::seeder($date);
                $date = UpdateCurrencyValues::seeder($date);
                $date = UpdateReputationMargins::seeder($date);
                if(mt_rand(1,6) == 2){
                    $date[0]->addHours(20);
                    $date = DueAndInterests::seeder($date);
                    return $date;
                }
                switch ($date[0]->format('h')) {
                    case '02':
                        $date[0]->addHours(mt_rand(5,6));//60 ~ 90
                        break;
                    case '03':
                        $date[0]->addHours(mt_rand(4,5));//60 ~ 90
                        break;
                    case '04':
                        $date[0]->addHours(mt_rand(3,4));//60 ~ 90
                        break;
                    case '05':
                        $date[0]->addHours(mt_rand(2,3));//60 ~ 90
                        break;
                    case '06':
                        $date[0]->addHours(mt_rand(1,2));//60 ~ 90
                        break;
                    case '07':
                        $date[0]->addHours(mt_rand(0,1));//60 ~ 90
                        break;
                }
            }
        }
        return $date;
    }
}
