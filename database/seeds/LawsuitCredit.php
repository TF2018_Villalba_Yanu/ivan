<?php

use Illuminate\Database\Seeder;
use App\Models\Credit;
use App\Models\CreditStage;
use Carbon\Carbon;

class LawsuitCredit extends Seeder
{
    public static function seeder($date){
        echo('Entre al seeder LawsuitCredit
        ');
        $credits = Credit::all();
        foreach ($credits as $credit) {
            if ($credit->nextCreditStages()->pluck('id')->contains(CreditStage::getLawsuit()->id)) {
                if($date[0] < Carbon::now()){
                    if ($credit->lawsuit($date[0]) == null) {
                        echo('Error 852259962');
                        dd();
                    }
                    $date[0]->addSeconds(1,2);
                    $date = DueAndInterests::seeder($date);
                }
            }
        }
        return $date;
    }
}
