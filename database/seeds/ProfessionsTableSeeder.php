<?php

use Illuminate\Database\Seeder;
use App\Models\Profession;

class ProfessionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $name = [
            'Otro',
            'Estudiante',
            'Medico',
            'Independiente',
            'Docente de primaria'
        ];
        for($i=0 ; $i<5 ; $i++){
            Profession::create([
                'name' => $name[$i],
            ]);
        }
    }
}
