<?php

use Illuminate\Database\Seeder;
use App\Models\Credit;
use Carbon\Carbon;

class PayCredit extends Seeder
{
    public static function seeder($date){
        for($i=0 ; $i<1 ; $i++){
            if($date[0] < Carbon::now()){
                echo('Entre al seeder PayCredit
                ');
                $loop = 0;
                do{
                    $credit = Credit::all()->random();
                    if($loop >= 50){
                        break;
                    }
                    $loop++;
                }while(($credit->unpayedAmount() <= 0) || ($credit->wallet->accountBalance() < $credit->currency()->min_value));
                if($loop >= 50){
                    break;
                }
                if (mt_rand(1,3) == 1) {
                    $max = $credit->unpayedAmount()*pow(10,$credit->currency()->significant_decimals);
                    $amount = mt_rand(1, $max);
                }else{
                    $max = $credit->unpayedAmount()*pow(10,$credit->currency()->significant_decimals);
                    $max = bcdiv($max,1,0);
                    $amount = mt_rand(1, $max);
                }
                $amount = $amount/pow(10,$credit->currency()->significant_decimals);
                if($amount >= $credit->currency()->min_value) {
                    if ($date[0]->isWeekend()) {
                        $random_int = mt_rand(1,10);
                        switch($random_int){
                            case 1:
                            case 2:
                            case 3:
                            case 4:
                            case 5:
                                $date[0]->addSeconds(mt_rand(3600,5400));//60 ~ 90
                                break;
                            case 6:
                            case 7:
                            case 8:
                                $date[0]->addSeconds(mt_rand(2100,3600));//35 ~ 60
                                break;
                            case 9:
                            case 10:
                                $date[0]->addSeconds(mt_rand(1500,2100));//25 ~ 35
                                break;    
                        }
                    }else{
                        $random_int = mt_rand(1,10);
                        switch ($random_int){
                            case 1:
                            case 2:
                                $date[0]->addSeconds(mt_rand(3600,5400));//60 ~ 90
                                break;
                            case 3:
                            case 4:
                            case 5:
                                $date[0]->addSeconds(mt_rand(2700,3600));//45 ~ 60
                                break;
                            case 6:
                            case 7:
                            case 8:
                            case 9:
                                $date[0]->addSeconds(mt_rand(1500,2700));//25 ~ 45
                                break;
                            case 10:
                                $date[0]->addSeconds(mt_rand(300,1500));//5 ~ 25
                                break;
                        }
                    }
                    if($amount > $credit->currency()->truncate($credit->wallet->accountBalance())){
                        $amount = $credit->currency()->truncate($credit->wallet->accountBalance());
                    }
                    if($credit->wallet->payCredit($credit, $amount, $date[0]) == null){
                        echo('Error 2255517'.$credit->unpayedAmount().$amount);
                        dd();
                    }
                }else{
                    echo('Error 8524556222');
                    dd();
                }
                $date = DueAndInterests::seeder($date);
            }
        }
        return $date;
    }
}
