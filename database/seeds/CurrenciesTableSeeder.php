<?php

use Illuminate\Database\Seeder;
use App\Models\Currency;

class CurrenciesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
    $names = [
        'pesos argentinos',
        'reales',
        'pesos chilenos',
        'euros',
        'dólares estadounidenses',
        'pesos uruguayos'
    ];
    $iso_codes = [
        'ARS',
        'BRL',
        'CLP',
        'EUR',
        'USD',
        'UYU',
    ];
    $simbols = [
        '$',
        'R$',
        '$',
        '€',
        '$',
        'UYU',
    ];
    $significant_decimals = [
        1,
        2,
        2,
        2,
        2,
        2
    ];
    for ($i=0 ; $i<6 ; $i++){
        $curr = Currency::create([
            'name'                     => $names[$i],
            'iso_code'                 => $iso_codes[$i],
            'simbol'                   => $simbols[$i],
            'significant_decimals'     => $significant_decimals[$i],
            'min_value'                => 1/pow(10,$significant_decimals[$i]),
            'max_inflation_percentage' => 0.05,
            'max_deflation_percentage' => 0.15,
        ]);
        $curr->refresh();
    }
    }
}
