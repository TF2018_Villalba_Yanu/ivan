<?php

use Illuminate\Database\Seeder;
use App\Models\PaymentPlan;
use App\Models\Currency;
use App\Models\Insurance;
use App\Models\Interest;
use App\Models\CustomDateInterval;

class PaymentPlansTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0 ; $i<30 ; $i++){
            $payments = mt_rand(1, 8) * 3;
            $currency = Currency::all()->random();
            $interest = Interest::all()->random();
            $insurance = Insurance::all()->random();
            $pp= PaymentPlan::create ([
                'payments_number'       => $payments,
                'due_dates_interval_id' => CustomDateInterval::customCreate(1,0,0,0,0)->id,
                'currency_id'           => $currency->id,
                'interest_id'           => $interest->id,
                'insurance_id'          => $insurance->id,
            ]);
            $pp->refresh();
        }
    }
}
