<?php

use Illuminate\Database\Seeder;
use App\Models\Country;

class CountriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $name = [
            'Argentina',
            'Brasil',
            'Chile',
            'España',
            'Estados Unidos',
            'Uruguay'
        ];
        $phone_code = [
            ' +54',
            ' +55',
            ' +56',
            ' +34',
            '  +1',
            '+598',
        ];
        for($i=0 ; $i<6 ; $i++){
            Country::create([
                'name'       => $name[$i],
                'phone_code' => $phone_code[$i],
                'currency_id'=> $i+1,
            ]);
        }
    }
}
