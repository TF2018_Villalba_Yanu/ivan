<?php

use Illuminate\Database\Seeder;
use App\Models\City;

class CitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
            'Misiones',
            'Corrientes',
            'Buenos Aires',
        */
        $name = [
            'Candelaria',
            'San Jose',
        ];
        for($i=0 ; $i<2 ; $i++){
            City::create([
                'name'        => $name[$i],
                'zip_code'    => strtoupper( str_random(mt_rand(3,8))),
                'district_id' => 1,
            ]);
        };
        $name = [
            'Goya',
            'San Carlos',
        ];
        for($i=0 ; $i<2 ; $i++){
            City::create([
                'name'        => $name[$i],
                'zip_code'    => strtoupper( str_random(mt_rand(3,8))),
                'district_id' => 2,
            ]);
        };
        $name = [
            'CABA',
            'San Telmpo',
        ];
        for($i=0 ; $i<2 ; $i++){
            City::create([
                'name'        => $name[$i],
                'zip_code'    => strtoupper( str_random(mt_rand(3,8))),
                'district_id' => 3,
            ]);
        };
        /*
            'Parana',
            'Santa Catarina',
            'Rio Grande',
        */
        $name = [
            'Curitiva',
            'Maringá',
        ];
        for($i=0 ; $i<2 ; $i++){
            City::create([
                'name'        => $name[$i],
                'zip_code'    => strtoupper( str_random(mt_rand(3,8))),
                'district_id' => 4,
            ]);
        };
        $name = [
            'Florianópolis',
            'Chapecó',
        ];
        for($i=0 ; $i<2 ; $i++){
            City::create([
                'name'        => $name[$i],
                'zip_code'    => strtoupper( str_random(mt_rand(3,8))),
                'district_id' => 5,
            ]);
        };
        $name = [
            'Rio Grande',
            'Pelotas',
        ];
        for($i=0 ; $i<2 ; $i++){
            City::create([
                'name'        => $name[$i],
                'zip_code'    => strtoupper( str_random(mt_rand(3,8))),
                'district_id' => 6,
            ]);
        };
        /*
            'Coquimbo',
            'Parinacota',
        */
        $name = [
            'Coquimbo',
            'Ovalle',
        ];
        for($i=0 ; $i<2 ; $i++){
            City::create([
                'name'        => $name[$i],
                'zip_code'    => strtoupper( str_random(mt_rand(3,8))),
                'district_id' => 7,
            ]);
        };
        $name = [
            'Japucucho',
            'Lauca',
        ];
        for($i=0 ; $i<2 ; $i++){
            City::create([
                'name'        => $name[$i],
                'zip_code'    => strtoupper( str_random(mt_rand(3,8))),
                'district_id' => 8,
            ]);
        };
        /*
            'Barcelona',
            'Alicante',
        */
        $name = [
            'Badalona',
            'Gracia',
        ];
        for($i=0 ; $i<2 ; $i++){
            City::create([
                'name'        => $name[$i],
                'zip_code'    => strtoupper( str_random(mt_rand(3,8))),
                'district_id' => 9,
            ]);
        };
        $name = [
            'Elche',
            'Elda',
        ];
        for($i=0 ; $i<2 ; $i++){
            City::create([
                'name'        => $name[$i],
                'zip_code'    => strtoupper( str_random(mt_rand(3,8))),
                'district_id' => 10,
            ]);
        };
        /*
            'California',
            'Texas',
        */
        $name = [
            'San Diego',
            'San Francisco',
        ];
        for($i=0 ; $i<2 ; $i++){
            City::create([
                'name'        => $name[$i],
                'zip_code'    => strtoupper( str_random(mt_rand(3,8))),
                'district_id' => 11,
            ]);
        };
        $name = [
            'Dallas',
            'Houston',
        ];
        for($i=0 ; $i<2 ; $i++){
            City::create([
                'name'        => $name[$i],
                'zip_code'    => strtoupper( str_random(mt_rand(3,8))),
                'district_id' => 12,
            ]);
        };
        /*
            'Montevideo',
            'Lavalleja',
        */
        $name = [
            'Montevideo',
            'Santiago Vazquez',
        ];
        for($i=0 ; $i<2 ; $i++){
            City::create([
                'name'        => $name[$i],
                'zip_code'    => strtoupper( str_random(mt_rand(3,8))),
                'district_id' => 13,
            ]);
        };
        $name = [
            'Minas',
            'Villa cerrana',
        ];
        for($i=0 ; $i<2 ; $i++){
            City::create([
                'name'        => $name[$i],
                'zip_code'    => strtoupper( str_random(mt_rand(3,8))),
                'district_id' => 14,
            ]);
        };

    }
}
