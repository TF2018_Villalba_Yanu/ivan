<?php

use Illuminate\Database\Seeder;
use App\Models\WalletTypeOfTransaction;

class WalletTypeOfTransactionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
             1 => 'Ingreso de dinero',
             2 => 'Retiro de dinero',
             3 => 'Efectivización del crédito',
             4 => 'Financiamiento',
             5 => 'Financiamiento cancelado',
             6 => 'Crédito cancelado'
             7 => 'Pago de cuota',
             8 => 'Reintegro del crédito',
             9 => 'Abono del seguro',
            10 => 'Reintegro asegurado',
            11 => 'Ingreso por por seguro acreedor' TRANSACCIONES DEL ROL INSURANCE_MANAGER SOLAMENTE
            12 => 'Efectivizacion de seguro' TRANSACCIONES DEL ROL INSURANCE_MANAGER SOLAMENTE
            13 => 'Reposicion del seguro' TRANSACCIONES DEL ROL INSURANCE_MANAGER SOLAMENTE
            14 => 'Cobro de comision' TRANSACCIONES DEL ROL COMMISSION_MANAGER SOLAMENTE
            15 => 'Ingreso por por seguro deudor' TRANSACCIONES DEL ROL INSURANCE_MANAGER SOLAMENTE
            16 => 'Ingreso por conversión de divisas'
            17 => 'Egreso por conversion de divisas'
        */
        $wtot = WalletTypeOfTransaction::create([
            'name'        => 'Ingreso de dinero',
            'type'        => 'credit',
            'description' => 'Ingreso de dinero en el Wallet',
        ]);
        $wtot->refresh();
        $wtot = WalletTypeOfTransaction::create([
            'name'        => 'Retiro de dinero',
            'type'        => 'debit',
            'description' => 'Retiro de dinero del Wallet',
        ]);
        $wtot->refresh();
        $wtot = WalletTypeOfTransaction::create([
            'name'        => 'Efectivización del crédito',
            'type'        => 'credit',
            'description' => 'Transferencia del crédito al wallet del deudor',
        ]);
        $wtot->refresh();
        $wtot = WalletTypeOfTransaction::create([
            'name'        => 'Financiamiento',
            'type'        => 'debit',
            'description' => 'Financiamiento de un crédito',
        ]);
        $wtot->refresh();
        $wtot = WalletTypeOfTransaction::create([
            'name'        => 'Financiamiento cancelado',
            'type'        => 'credit',
            'description' => 'Se canceló un un financiamiento o una parte del mismo',
        ]);
        $wtot->refresh();
        $wtot = WalletTypeOfTransaction::create([
            'name'        => 'Crédito cancelado',
            'type'        => 'credit',
            'description' => 'El deudor canceló un crédito en su fase de financiamiento, se devuelve el financiamiento al acreedor',
        ]);
        $wtot->refresh();
        $wtot = WalletTypeOfTransaction::create([
            'name'        => 'Pago de cuota',
            'type'        => 'debit',
            'description' => 'Se pagó una cuota de un crédito',
        ]);
        $wtot->refresh();
        $wtot = WalletTypeOfTransaction::create([
            'name'        => 'Reintegro del crédito',
            'type'        => 'credit',
            'description' => 'Se reintegró una cuota de una financiación',
        ]);
        $wtot->refresh();
        $wtot = WalletTypeOfTransaction::create([
            'name'        => 'Abono del seguro',
            'type'        => 'debit',
            'description' => 'Se abonó la póliza del crédito',
        ]);
        $wtot->refresh();
        $wtot = WalletTypeOfTransaction::create([
            'name'        => 'Reintegro asegurado',
            'type'        => 'credit',
            'description' => 'Se pagó una cuota atrasada por medio del seguro',
        ]);     
        $wtot->refresh();   
        $wtot = WalletTypeOfTransaction::create([
            'name'        => 'Ingreso por por seguro acreedor',
            'type'        => 'credit',
            'description' => 'Ingreso por seguro abonado por el usuario acreedor',
        ]);
        $wtot->refresh();
        $wtot = WalletTypeOfTransaction::create([
            'name'        => 'Efectivizacion de seguro',
            'type'        => 'debit',
            'description' => 'Efectivizacion del seguro sobre un credito',
        ]);
        $wtot->refresh();
        $wtot = WalletTypeOfTransaction::create([
            'name'        => 'Reposicion del seguro',
            'type'        => 'credit',
            'description' => 'Reposicion del seguro efectivizado previamente',
        ]);
        $wtot->refresh();
        $wtot = WalletTypeOfTransaction::create([
            'name'        => 'Cobro de comision',
            'type'        => 'credit',
            'description' => 'Cobro de comision por un credito',
        ]);
        $wtot->refresh();
        $wtot = WalletTypeOfTransaction::create([
            'name'        => 'Ingreso por por seguro deudor',
            'type'        => 'credit',
            'description' => 'Ingreso por seguro abonado por el usuario deudor',
        ]);
        $wtot->refresh();
        $wtot = WalletTypeOfTransaction::create([
            'name'        => 'Ingreso por conversión de divisas',
            'type'        => 'credit',
            'description' => 'Ingreso de dinero por conversion entre divisas',
        ]);
        $wtot->refresh();
        $wtot = WalletTypeOfTransaction::create([
            'name'        => 'Egreso por conversion de divisas',
            'type'        => 'debit',
            'description' => 'Egresode dinero por conversion entre divisas',
        ]);
        $wtot->refresh();
    }
}
