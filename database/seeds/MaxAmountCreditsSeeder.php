<?php

use Illuminate\Database\Seeder;
use App\Models\MaxAmountCredit;
use App\Models\Currency;
use App\Models\Profession;
use App\Models\ExchangeRate;


class MaxAmountCreditsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //6 currencies, 5 professions =>6*5=30
        for ($i=1 ; $i<=30 ; $i++){
            $currency_id = null;
            $profession_id = null;
            do{
                $currency = Currency::all()->random();
                $profession = Profession::all()->random();
                $max_amount_credit = MaxAmountCredit::where('currency_id',$currency->id)
                    ->where('profession_id',$profession->id)
                    ->first();
            }while($max_amount_credit != null);
            do{
                $initial_limit = mt_rand(10000,1800000)/1000;
                $max_limit = mt_rand($initial_limit + 500,2500000)/1000;
            }while($max_limit < ($initial_limit *  2));
            $mac = MaxAmountCredit::create([
                'profession_id'        => $profession->id,
                'currency_id'          => $currency->id,
                'initial_limit'        => round(ExchangeRate::localConvert('USD',$currency->iso_code, $initial_limit), $currency->significant_decimals),
                'max_limit'            => round(ExchangeRate::localConvert('USD',$currency->iso_code, $max_limit), $currency->significant_decimals),
                'active_credits_limit' => 20,//mt_rand(1,20),
            ]);
            $mac->refresh();
        }
    }
}
