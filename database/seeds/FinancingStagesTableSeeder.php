<?php

use Illuminate\Database\Seeder;
use App\Models\FinancingStage;

class FinancingStagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
             1 => Financiacion
             2 => Financiacion cancelada
             3 => Crédito cancelado
             4 => Acreditado
             5 => Reintegro
             6 => Reintegro por seguro
             7 => Reintegrado
             8 => Incobrable
             9 => Financiamiento terminado
            10 => Financiamiento reabierto
            11 => Reintegro demorado
        */
        $name = [
            'Financiacion',
            'Financiacion cancelada',
            'Crédito cancelado',
            'Acreditado',
            'Reintegro',
            'Reintegro por seguro',
            'Reintegrado',
            'Incobrable',
            'Financiamiento terminado',
            'Financiamiento reabierto',
            'Reintegro demorado',
        ];
        $description = [
            'Se financio un credito',
            'El acreedor canceló un monto de la financiacion',
            'El deudor cancelo la solicitud del credito',
            'Se acrédito la financiacion al wallet del deudor',
            'Reintegro de la financiacion por parte del deudor',
            'Reintegro de la financiacion por parte del seguro',
            'La financiacion se reintegro en su totalidad',
            'Luego de intenter por muchos medios cobrar el credito, determinamos que sera imposible cobrarlo',
            'El crédito se finanio en su totalidad',
            'El financiamiento del crédito se volvio a abrir, porque un usuario cancelo su financiamiento',
            'El deudor no pago el crédito y el financiamiento no tiene seguro, por lo que el reintegro se demoró',
        ];
        for ($i=0 ; $i<11 ; $i++){
            $fs = FinancingStage::create([
                'name'        =>$name[$i],
                'description' =>$description[$i],
            ]);
            $fs->refresh();
        }
    }
}
