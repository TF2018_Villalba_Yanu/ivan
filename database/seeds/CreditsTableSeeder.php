<?php
use Illuminate\Database\Seeder;
use App\Models\Credit;
use App\Models\User;
use App\Models\PaymentPlan;
use App\Models\Role;
use Carbon\Carbon;
use App\Models\ExchangeRate;

class CreditsTableSeeder extends Seeder
{
    public static function seeder($date){
        echo('Entre al seeder CreditsTableSeeder
        ');
        for ($j=0 ; $j<1; $j++){
            if($date[0] < Carbon::now()){
                $loop = 0;
                do{
                    $user = User::where('role_id',Role::getUserRole()->id)->get()->random();
                    if($loop >= 30){
                        break;
                    }
                    $loop++;
                }while(($user->userMargin($date[0]) <= 5) || ($user->reputation->activeCreditsLimit($date[0]) <= $user->activeCredits()->count()) || ($user->role->id != Role::getUserRole()->id));
                if($loop >= 30){
                    break;
                }
                $payment_plan = PaymentPlan::all()->random();
                if ($date[0]->isWeekend()) {
                    $random_int = mt_rand(1,10);
                    switch($random_int){
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                        case 5:
                            $date[0]->addSeconds(mt_rand(3600,5400));//60 ~ 90
                            break;
                        case 6:
                        case 7:
                        case 8:
                            $date[0]->addSeconds(mt_rand(2100,3600));//35 ~ 60
                            break;
                        case 9:
                        case 10:
                            $date[0]->addSeconds(mt_rand(1500,2100));//25 ~ 35
                            break;
                    }
                }else{
                    $random_int = mt_rand(1,10);
                    switch ($random_int){
                        case 1:
                        case 2:
                            $date[0]->addSeconds(mt_rand(3600,5400));//60 ~ 90
                            break;
                        case 3:
                        case 4:
                        case 5:
                            $date[0]->addSeconds(mt_rand(2700,3600));//45 ~ 60
                            break;
                        case 6:
                        case 7:
                        case 8:
                        case 9:
                            $date[0]->addSeconds(mt_rand(1500,2700));//25 ~ 45
                            break;
                        case 10:
                            $date[0]->addSeconds(mt_rand(300,1500));//5 ~ 25
                            break;
                    }
                }
                $loop = 0;
                do{
                    $min = 5000;
                    $max = (int) ($user->userMargin($date[0]) * 1000)/5;
                    if($max <= $min){
                        $amount_usd = mt_rand(5000,6500) * 0.0001;
                    }else{
                        $amount_usd = mt_rand($min,$max) * 0.0001;
                    }
                    if($loop >= 50){
                        break;
                    }
                    $loop++;
                }while(($amount_usd > $user->userMargin($date[0])) || ($amount_usd < 5));
                if($loop >= 50){
                    $date = DueAndInterests::seeder($date);
                    break;
                }
                $amount = ExchangeRate::localConvert('USD',$payment_plan->currency->iso_code,$amount_usd);
                $amount = round($amount,$payment_plan->currency->significant_decimals);
                if(Credit::createCredit($user, $payment_plan, $amount, $date[0]) == null){
                    echo('Error 34456');
                    dd();
                }
                $date = DueAndInterests::seeder($date);
            }
        }
        return $date;
    }
}
