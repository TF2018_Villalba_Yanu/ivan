<?php

use Illuminate\Database\Seeder;
use App\Models\CreditStage;

class CreditStagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
            1  => 'Solicitado',
            2  => 'Financiando',
            3  => 'Financiacion Finalizada',
            4  => 'Otorgado',
            5  => 'Al dia',
            6  => 'Pago retrasado',
            7  => 'Mora',
            8  => 'Citado',
            9  => 'Litigio',
            10 => 'Saldado',
            11 => 'Cancelado',
            12 => 'Rechazado',
            13 => 'Incobrable',
            14 => 'Vencido'
            14 => 'Financiacion rehabierta'
        */
        $name = [
            'Solicitado',
            'Financiando',
            'Financiacion Finalizada',
            'Otorgado',
            'Al dia',
            'Pago retrasado',
            'Mora',
            'Citado',
            'Litigio',
            'Saldado',
            'Cancelado',
            'Rechazado',
            'Incobrable',
            'Financiacion rehabierta',
        ];
        $desc = [
            'El crédito se ha solicitado',
            'El crédito se ha aprobado y esta en etapa de financiacion',
            'El crédito ha terminado de financiarse y esta a la espera de que el deudor transfiera los fondos a su cuenta',
            'El deudor recibio el crédito y se encuentra en el "periodo de gracia" antes de iniciar el reintegro.',
            'El deudor se encuentra al dia con su crédito',
            'El deudor se está retrasado con el pago de su crédito',
            'El usuario se atrasó mucho con la devolución del crédito',
            'Se ha enviado una carta documento al usuario para que regularice su situación',
            'Se estan llevando a cabo acciones legales para que el usuario abone la deuda contraída',
            'El deudor canceló el pago del crédito',
            'El deudor canceló el crédito antes de efectivizarse',
            'El crédito no se aprobó',
            'El crédito no se abonará. Representa en una pérdida para la empresa',
            'Debido a que se canceló un financiamiento, el financiamiento se volvio a abrir'
        ];
        for($i=0 ; $i<14 ; $i++){
            $cs = CreditStage::create([
                'name'        => $name[$i],
                'description' => $desc[$i],
            ]);
            $cs->refresh();
        }
    }
}
