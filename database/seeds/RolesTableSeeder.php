<?php

use Illuminate\Database\Seeder;
use App\Models\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
            Default Roles:
            'Usuario',
            'Administrador',
            'Gestor de parametros',
            'Division de seguros',
            'Division de comisiones',
            'Division de litigios',
            'Division de morosos',
            'Division de créditos',
            'Auditor'
        */
        $names = [
            'Usuario',
            'Administrador',
            'Gestor de parametros',
            'Division de seguros',
            'Division de comisiones',
            'Division de litigios',
            'Division de morosos',
            'Division de créditos',
            'Auditor',
        ];
        for($i=0 ; $i<9 ; $i++){
            $role = Role::create([
                'name' => $names[$i],
            ]);
            $role->refresh();
        }
    }
}
