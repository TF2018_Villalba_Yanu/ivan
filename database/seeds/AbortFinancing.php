<?php

use Illuminate\Database\Seeder;
use App\Models\Financing;
use App\Models\FinancingStage;
use Carbon\Carbon;

class AbortFinancing extends Seeder
{
    public static function seeder($date){
        echo('Entre al seeder AbortFinancing
        ');
        for($i=0 ; $i<1 ; $i++){
            if($date[0] < Carbon::now()){
                if (Financing::all()->count() == 0){
                    break;
                }
                $loop = 0;
                do{
                    $financing = Financing::all()->random();
                    if($loop >= 50){
                        break;
                    }
                    $loop++;
                }while((!$financing->isActive()) || (!$financing->nextFinancingStages()->pluck('id')->contains(FinancingStage::getFinancingAbort()->id)));
                if($loop >= 50){
                    break;
                }
                $amount = mt_rand(100000,1000000)*0.00001;
                $amount = round($amount,$financing->credit->currency()->significant_decimals);
                if($amount > $financing->amount()){
                    $amount = $financing->amount();
                }
                if ($date[0]->isWeekend()) {
                    $random_int = mt_rand(1,10);
                    switch($random_int){
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                        case 5:
                            $date[0]->addSeconds(mt_rand(3600,5400));//60 ~ 90
                            break;
                        case 6:
                        case 7:
                        case 8:
                            $date[0]->addSeconds(mt_rand(2100,3600));//35 ~ 60
                            break;
                        case 9:
                        case 10:
                            $date[0]->addSeconds(mt_rand(1500,2100));//25 ~ 35
                            break;
                    }
                }else{
                    $random_int = mt_rand(1,10);
                    switch ($random_int){
                        case 1:
                        case 2:
                            $date[0]->addSeconds(mt_rand(3600,5400));//60 ~ 90
                            break;
                        case 3:
                        case 4:
                        case 5:
                            $date[0]->addSeconds(mt_rand(2700,3600));//45 ~ 60
                            break;
                        case 6:
                        case 7:
                        case 8:
                        case 9:
                            $date[0]->addSeconds(mt_rand(1500,2700));//25 ~ 45
                            break;
                        case 10:
                            $date[0]->addSeconds(mt_rand(300,1500));//5 ~ 25
                            break;
                    }
                }
                if($financing->abortFinancing($amount, $date[0]) == null){
                    echo('Error 9841298');
                    dd();
                }
                $date = DueAndInterests::seeder($date);

            }
        }
        return $date;
    }
}
