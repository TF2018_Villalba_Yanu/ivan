<?php

use Illuminate\Database\Seeder;
use App\Models\Insurance;

class InsurancesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0 ; $i<15 ; $i++ ){
            $min = (mt_rand(800,4600))/10000;
            do{
                $initial = (mt_rand(3500,10000))/10000;
            }while($initial <= $min);
            do{
                $max = (mt_rand(1400,1800))/1000;
            }while($max <= $initial);
            $insurance = Insurance::create([
                'initial_percentage' => $initial,
                'min_percentage' => $min,
                'max_percentage' => $max,
                'debtor_percentage' => (mt_rand(1000,5000))/10000,
            ]);
            $insurance->refresh();
        }
    }
}
