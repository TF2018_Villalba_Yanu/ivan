<?php

use Illuminate\Database\Seeder;
use App\Models\Wallet;
use App\Models\Currency;
use App\Models\Exchange;
use Carbon\Carbon;
use App\Models\ExchangeRate;
use App\Models\Role;

class ExchangeSeeder extends Seeder
{
    public static function seeder($date){
        echo('Entre al seeder ExchangeSeeder
        '); 
        for($i=0 ; $i<1 ; $i++){
            if($date[0] < Carbon::now()){
                $loop = 0;
                do{
                    $wallet = Wallet::all()->random();
                    if($loop >= 50){
                        break;
                    }
                    $loop++;
                }while(($wallet->accountBalance() < $wallet->currency->min_value) || ($wallet->user->role->id != Role::getUserRole()->id));
                if($loop >= 50){
                    break;
                }
                $loop = 0;
                do{
                    $amount = mt_rand(100,($wallet->accountBalance() * 10000));
                    $amount /= 10000;
                    $amount = $wallet->currency->truncate($amount);
                    if($loop >= 50){
                        break;
                    }
                    $loop++;
                }while(($amount < $wallet->currency->min_value) || ($amount > $wallet->accountBalance()));
                if($loop >= 50){
                    break;
                }
                $loop = 0;
                do{
                    $currency = Currency::all()->random();
                    if($loop >= 20){
                        break;
                    }
                    $loop++;
                }while($currency->id == $wallet->currency->id);
                if($loop >= 20){
                    break;
                }
                if ($date[0]->isWeekend()) {
                    $random_int = mt_rand(1,10);
                    switch($random_int){
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                        case 5:
                            $date[0]->addSeconds(mt_rand(3600,5400));//60 ~ 90
                            break;
                        case 6:
                        case 7:
                        case 8:
                            $date[0]->addSeconds(mt_rand(2100,3600));//35 ~ 60
                            break;
                        case 9:
                        case 10:
                            $date[0]->addSeconds(mt_rand(1500,2100));//25 ~ 35
                            break;
                    }
                }else{
                    $random_int = mt_rand(1,10);
                    switch ($random_int){
                        case 1:
                        case 2:
                            $date[0]->addSeconds(mt_rand(3600,5400));//60 ~ 90
                            break;
                        case 3:
                        case 4:
                        case 5:
                            $date[0]->addSeconds(mt_rand(2700,3600));//45 ~ 60
                            break;
                        case 6:
                        case 7:
                        case 8:
                        case 9:
                            $date[0]->addSeconds(mt_rand(1500,2700));//25 ~ 45
                            break;
                        case 10:
                            $date[0]->addSeconds(mt_rand(300,1500));//5 ~ 25
                            break;
                    }
                }
                $exchange_rate = ExchangeRate::lastExchangeRate($wallet->currency,$currency);
                if (Exchange::exchange($wallet, $currency, $amount, $exchange_rate, $date[0]) == null){
                    echo('Error 74255515');
                    dd();
                }
                $date = DueAndInterests::seeder($date);
            }
        }
        return $date;
    }
}
