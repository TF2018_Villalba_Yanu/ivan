<?php

use Illuminate\Database\Seeder;
use App\Models\CreditStage;
use App\Models\Credit;
use Carbon\Carbon;

class BadCredit extends Seeder
{
    public static function seeder($date){
        echo('Entre al seeder BadCredit
        ');    
        $credits = Credit::all();
        foreach ($credits as $credit) {
            if ($credit->lastCreditPhase()->creditStage->id == CreditStage::getLawsuit()->id) {
                if($date[0] < Carbon::now()){
                    if ($credit->bad($date[0]) == null) {
                        echo('Error 00559962');
                        dd();
                    }
                    $date[0]->addSeconds(1,2);
                    $date = DueAndInterests::seeder($date);
                }
            }
        }
        return $date;
    }
}
