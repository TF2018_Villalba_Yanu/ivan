<?php

use Illuminate\Database\Seeder;
use App\Models\Wallet;
use App\Models\Currency;
use App\Models\User;
use App\Models\Role;
use Carbon\Carbon;

class WalletInputOutputMoneyTableSeeder extends Seeder
{
    public static function seeder($date){
        echo('Entre al seeder WalletInputOutputMoneyTableSeeder
        ');
        for($i=0 ; $i<3 ; $i++){
            if($date[0] < Carbon::now()){
                $currency = Currency::all()->random();
                $user = User::where('role_id',Role::getUserRole()->id)->get()->random();
                $wallet = Wallet::where('currency_id',$currency->id)
                        ->where('user_id',$user->id)
                        ->first();
                if ((mt_rand(1,3) == 3) && ($wallet != null) && ($wallet->accountBalance() >= $wallet->currency->min_value)){
                    //output
                    if ($wallet->accountBalance() >= 3){
                        $amount = mt_rand(10,round(($wallet->accountBalance() / 4),0)*10)*0.1;
                        $amount = round($amount,$wallet->currency->significant_decimals);
                    }else{
                        $amount = $wallet->accountBalance();
                    }
                    if ($date[0]->isWeekend()) {
                        $random_int = mt_rand(1,10);
                        switch($random_int){
                            case 1:
                            case 2:
                            case 3:
                            case 4:
                            case 5:
                                $date[0]->addSeconds(mt_rand(3600,5400));//60 ~ 90
                                break;
                            case 6:
                            case 7:
                            case 8:
                                $date[0]->addSeconds(mt_rand(2100,3600));//35 ~ 60
                                break;
                            case 9:
                            case 10:
                                $date[0]->addSeconds(mt_rand(1500,2100));//25 ~ 35
                                break;
                        }
                    }else{
                        $random_int = mt_rand(1,10);
                        switch ($random_int){
                            case 1:
                            case 2:
                                $date[0]->addSeconds(mt_rand(3600,5400));//60 ~ 90
                                break;
                            case 3:
                            case 4:
                            case 5:
                                $date[0]->addSeconds(mt_rand(2700,3600));//45 ~ 60
                                break;
                            case 6:
                            case 7:
                            case 8:
                            case 9:
                                $date[0]->addSeconds(mt_rand(1500,2700));//25 ~ 45
                                break;
                            case 10:
                                $date[0]->addSeconds(mt_rand(300,1500));//5 ~ 25
                                break;
                        }
                    }
                    if($wallet->outputMoney($amount, $date[0]) == null){
                        echo("Error 452");
                        dd();
                    }
                    $date = DueAndInterests::seeder($date);
                }else{
                    //input
                    $amount = mt_rand(300,3000);
                    $amount *= 0.1;
                    $amount = round($amount,$currency->significant_decimals);
                    if ($date[0]->isWeekend()) {
                        $random_int = mt_rand(1,10);
                        switch($random_int){
                            case 1:
                            case 2:
                            case 3:
                            case 4:
                            case 5:
                                $date[0]->addSeconds(mt_rand(3600,5400));//60 ~ 90
                                break;
                            case 6:
                            case 7:
                            case 8:
                                $date[0]->addSeconds(mt_rand(2100,3600));//35 ~ 60
                                break;
                            case 9:
                            case 10:
                                $date[0]->addSeconds(mt_rand(1500,2100));//25 ~ 35
                                break;    
                        }
                    }else{
                        $random_int = mt_rand(1,10);
                        switch ($random_int){
                            case 1:
                            case 2:
                                $date[0]->addSeconds(mt_rand(3600,5400));//60 ~ 90
                                break;
                            case 3:
                            case 4:
                            case 5:
                                $date[0]->addSeconds(mt_rand(2700,3600));//45 ~ 60
                                break;
                            case 6:
                            case 7:
                            case 8:
                            case 9:
                                $date[0]->addSeconds(mt_rand(1500,2700));//25 ~ 45
                                break;
                            case 10:
                                $date[0]->addSeconds(mt_rand(300,1500));//5 ~ 25
                                break;
                        }
                    }
                    if(Wallet::inputMoney($user,$currency,$amount, $date[0]) == null){
                        echo("Error 547");
                        dd();
                    }
                    $date = DueAndInterests::seeder($date);
                }
            }
            return $date;
        }
    }
}
