<?php

use Illuminate\Database\Seeder;
use App\Models\PaymentStageNext;

class PaymentStageNextsTableSeeder extends Seeder
{

    public function run()
    {
        /*
            1 => 'Generado'---------------------------> Orden de pago generada
            2 => 'Pago'-------------------------------> Pago registrado
            3 => 'Vencido'----------------------------> Se veció la orden de pago
            4 => 'Pagado temporalmente por el seguro'-> Se pago a los acreedores por medio del seguro
            5 => 'Interes agregado'-------------------> Interes adicional por mora
            6 => 'Saldado'----------------------------> Orden de pago cancelada
            7 => 'Incobrable'-------------------------> La orden de pago no puede ser cancelada
            Antes de Saldar una orden de pago, primero si o si debe pagarse, por lo que el camino Pagado >> Saldado
        */
        $psn = PaymentStageNext::create([
            'payment_stage_id'      => 1, //'Generado'
            'payment_stage_next_id' => 2, //'Pago'
        ]);
        $psn->refresh();
        $psn = PaymentStageNext::create([
            'payment_stage_id'      => 1, //'Generado'
            'payment_stage_next_id' => 3, //'Vencido'
        ]);
        $psn->refresh();
        $psn = PaymentStageNext::create([
            'payment_stage_id'      => 2, //'Pago'
            'payment_stage_next_id' => 2, //'Pago'
        ]);
        $psn->refresh();
        $psn = PaymentStageNext::create([
            'payment_stage_id'      => 2, //'Pago'
            'payment_stage_next_id' => 3, //'Vencido'
        ]);
        $psn->refresh();
        $psn = PaymentStageNext::create([
            'payment_stage_id'      => 2, //'Pago'
            'payment_stage_next_id' => 6, //'Saldado'
        ]);
        $psn->refresh();
//        $psn = PaymentStageNext::create([
//            'payment_stage_id'      => 3, //'Vencido'
//            'payment_stage_next_id' => 2, //'Pago'
//        ]);
//        $psn->refresh();
        $psn = PaymentStageNext::create([
            'payment_stage_id'      => 3, //'Vencido'
            'payment_stage_next_id' => 4, //'Pagado temporalmente por el seguro'
        ]);
        $psn->refresh();
//        $psn = PaymentStageNext::create([
//            'payment_stage_id'      => 3, //'Vencido'
//            'payment_stage_next_id' => 5, //'Interes agregado'
//        ]);
//        $psn->refresh();
        $psn = PaymentStageNext::create([
            'payment_stage_id'      => 4, //'Pagado temporalmente por el seguro'
            'payment_stage_next_id' => 2, //'Pago'
        ]);
        $psn->refresh();
        $psn = PaymentStageNext::create([
            'payment_stage_id'      => 4, //'Pagado temporalmente por el seguro'
            'payment_stage_next_id' => 5, //'Interes agregado'
        ]);
        $psn->refresh();
        $psn = PaymentStageNext::create([
            'payment_stage_id'      => 5, //'Interes agregado'
            'payment_stage_next_id' => 2, //'Pago'
        ]);
        $psn->refresh();
        $psn = PaymentStageNext::create([
            'payment_stage_id'      => 5, //'Interes agregado'
            'payment_stage_next_id' => 5, //'Interes agregado'
        ]);
        $psn->refresh();
        $psn = PaymentStageNext::create([
            'payment_stage_id'      => 5, //'Interes agregado'
            'payment_stage_next_id' => 7, //'Incobrable'
        ]);
        $psn->refresh();
        $psn = PaymentStageNext::create([
            'payment_stage_id'      => 7, //'Incobrable'
            'payment_stage_next_id' => 5, //'Interes agregado'
        ]);
        $psn->refresh();
    }
}
