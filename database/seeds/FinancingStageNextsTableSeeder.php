<?php

use Illuminate\Database\Seeder;
use App\Models\FinancingStageNext;

class FinancingStageNextsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
             1 => Financiacion
             2 => Financiacion cancelada
             3 => Crédito cancelado
             9 => Financiamiento terminado
            10 => Financiamiento reabierto
             4 => Acreditado
             5 => Reintegro
             6 => Reintegro por seguro
            11 => Reintegro demorado
             7 => Reintegrado
             8 => Incobrable
        */
        $fsn = FinancingStageNext::create([
            'financing_stage_id'      => 1, //1 => Financiacion
            'financing_stage_next_id' => 1, //1 => Financiacion
        ]);
        $fsn->refresh();
        $fsn = FinancingStageNext::create([
            'financing_stage_id'      => 1, //1 => Financiacion
            'financing_stage_next_id' => 2, //2 => Financiacion cancelada
        ]);
        $fsn->refresh();
        $fsn = FinancingStageNext::create([
            'financing_stage_id'      => 1, //1 => Financiacion
            'financing_stage_next_id' => 3, //3 => Crédito cancelado
        ]);
        $fsn->refresh();
        $fsn = FinancingStageNext::create([
            'financing_stage_id'      => 1, //1 => Financiacion
            'financing_stage_next_id' => 9, //9 => Financiamiento terminado
        ]);
        $fsn->refresh();
        $fsn = FinancingStageNext::create([
            'financing_stage_id'      => 2, //2 => Financiacion cancelada
            'financing_stage_next_id' => 1, //1 => Financiacion
        ]);
        $fsn->refresh();
        $fsn = FinancingStageNext::create([
            'financing_stage_id'      => 2, //2 => Financiacion cancelada
            'financing_stage_next_id' => 2, //2 => Financiacion cancelada
        ]);
        $fsn->refresh();
        $fsn = FinancingStageNext::create([
            'financing_stage_id'      => 2, //2 => Financiacion cancelada
            'financing_stage_next_id' => 3, //3 => Crédito cancelado
        ]); 
        $fsn->refresh();
        $fsn = FinancingStageNext::create([
            'financing_stage_id'      => 2, //2 => Financiacion cancelada
            'financing_stage_next_id' => 9, //9 => Financiamiento terminado
        ]);
        $fsn->refresh();
        $fsn = FinancingStageNext::create([
            'financing_stage_id'      => 2, //2 => Financiacion cancelada
            'financing_stage_next_id' => 10, //10 => Financiamiento reabierto
        ]);
        $fsn->refresh();
        $fsn = FinancingStageNext::create([
            'financing_stage_id'      => 9, //9 => Financiamiento terminado
            'financing_stage_next_id' => 2, //2 => Financiacion cancelada
        ]);
        $fsn->refresh();
        $fsn = FinancingStageNext::create([
            'financing_stage_id'      => 9, //9 => Financiamiento terminado
            'financing_stage_next_id' => 3, //3 => Crédito cancelado
        ]);
        $fsn->refresh();
        $fsn = FinancingStageNext::create([
            'financing_stage_id'      => 9, //9 => Financiamiento terminado
            'financing_stage_next_id' => 4, //4 => Acreditado
        ]);
        $fsn->refresh();
        $fsn = FinancingStageNext::create([
            'financing_stage_id'      => 9, //9 => Financiamiento terminado
            'financing_stage_next_id' => 10, //10 => Financiamiento reabierto
        ]);
        $fsn->refresh();
        $fsn = FinancingStageNext::create([
            'financing_stage_id'      => 10, //10 => Financiamiento reabierto
            'financing_stage_next_id' => 1, //1 => Financiacion        
        ]);
        $fsn->refresh();
        $fsn = FinancingStageNext::create([
            'financing_stage_id'      => 10, //10 => Financiamiento reabierto
            'financing_stage_next_id' => 2, //2 => Financiacion cancelada
        ]);
        $fsn->refresh();
        $fsn = FinancingStageNext::create([
            'financing_stage_id'      => 10, //10 => Financiamiento reabierto
            'financing_stage_next_id' => 3, //3 => Crédito cancelado
        ]);
        $fsn->refresh();
        $fsn = FinancingStageNext::create([
            'financing_stage_id'      => 10, //10 => Financiamiento reabierto
            'financing_stage_next_id' => 9, //9 => Financiamiento terminado
        ]);
        $fsn->refresh();
        $fsn = FinancingStageNext::create([
            'financing_stage_id'      => 4, //4 => Acreditado
            'financing_stage_next_id' => 5, //5 => Reintegro
        ]);
        $fsn->refresh();
        $fsn = FinancingStageNext::create([
            'financing_stage_id'      => 4, //4 => Acreditado
            'financing_stage_next_id' => 6, //6 => Reintegro por seguro
        ]);
        $fsn->refresh();
        $fsn = FinancingStageNext::create([
            'financing_stage_id'      => 4, //4 => Acreditado
            'financing_stage_next_id' => 11, //11 => Reintegro demorado
        ]);
        $fsn->refresh();
        $fsn = FinancingStageNext::create([
            'financing_stage_id'      => 5, //5 => Reintegro
            'financing_stage_next_id' => 5, //5 => Reintegro
        ]);
        $fsn->refresh();
        $fsn = FinancingStageNext::create([
            'financing_stage_id'      => 5, //5 => Reintegro
            'financing_stage_next_id' => 6, //6 => Reintegro por seguro
        ]);
        $fsn->refresh();
        $fsn = FinancingStageNext::create([
            'financing_stage_id'      => 5, //5 => Reintegro
            'financing_stage_next_id' => 7, //7 => Reintegrado
        ]);
        $fsn->refresh();
//        $fsn = FinancingStageNext::create([
//            'financing_stage_id'      => 5, //5 => Reintegro
//            'financing_stage_next_id' => 8, //8 => Incobrable
//        ]);
//        $fsn->refresh();
        $fsn = FinancingStageNext::create([
            'financing_stage_id'      => 5, //5 => Reintegro
            'financing_stage_next_id' => 11, //11 => Reintegro demorado
        ]);
        $fsn->refresh();
        $fsn = FinancingStageNext::create([
            'financing_stage_id'      => 6, //6 => Reintegro por seguro
            'financing_stage_next_id' => 5, //5 => Reintegro
        ]);
        $fsn->refresh();
        $fsn = FinancingStageNext::create([
            'financing_stage_id'      => 6, //6 => Reintegro por seguro
            'financing_stage_next_id' => 6, //6 => Reintegro por seguro
        ]);
        $fsn->refresh();
        $fsn = FinancingStageNext::create([
            'financing_stage_id'      => 6, //6 => Reintegro por seguro
            'financing_stage_next_id' => 7, //7 => Reintegrado
        ]);
        $fsn->refresh();
        $fsn = FinancingStageNext::create([
            'financing_stage_id'      => 11, //11 => Reintegro demorado
            'financing_stage_next_id' => 5, //5 => Reintegro
        ]);
        $fsn->refresh();
        $fsn = FinancingStageNext::create([
            'financing_stage_id'      => 11, //11 => Reintegro demorado
            'financing_stage_next_id' => 7, //7 => Reintegrado
        ]);
        $fsn->refresh();
        $fsn = FinancingStageNext::create([
            'financing_stage_id'      => 11, //11 => Reintegro demorado
            'financing_stage_next_id' => 8, //8 => Incobrable
        ]);
        $fsn->refresh();
        $fsn = FinancingStageNext::create([
            'financing_stage_id'      => 11, //11 => Reintegro demorado
            'financing_stage_next_id' => 11, //11 => Reintegro demorado
        ]);
        $fsn->refresh();
    }
}
