<?php

use Illuminate\Database\Seeder;
use App\Models\Wallet;
use App\Models\User;
use App\Models\Currency;
use App\Models\Role;


class WalletsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        echo('what?');
        dd();
        for ($i=1 ; $i<=3 ; $i++){
            do{
                $user = User::where('role_id',Role::getUserRole()->id)->get()->random();
                $currency = Currency::all()->random();
            }while((Wallet::where('user_id',$user->id)->where('currency_id',$currency->id)->first()) != null);
            $wallet = Wallet::create ([         
                'user_id'         => $user->id,              
                'currency_id'     => $currency->id,
            ]);
            $wallet->refresh();
        }
    }
}
