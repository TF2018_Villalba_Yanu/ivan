<?php

use Illuminate\Database\Seeder;
use App\Models\CreditStageNext;

class CreditStageNextsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
            1  >> 'Solicitado',
            2  >> 'Financiando',
            3  >> 'Financiacion Finalizada',
            14 >> 'Financiacion rehabierta'
            4  >> 'Otorgado',
            5  >> 'Al dia',
            6  >> 'Pago retrasado',
            7  >> 'Mora',
            8  >> 'Citado',
            9  >> 'Litigio',
            10 >> 'Saldado',
            11 >> 'Cancelado',
            12 >> 'Rechazado',
            13 >> 'Incobrable', 
        */
        $csn =CreditStageNext::create([
            'credit_stage_id'      => 1, //1 >> 'Solicitado',
            'credit_stage_next_id' => 2, //2 >> 'Financiando',
        ]);
        $csn->refresh();
        $csn =CreditStageNext::create([
            'credit_stage_id'      => 1,  //1  >> 'Solicitado',
            'credit_stage_next_id' => 11, //11 >> 'Cancelado',
        ]);
        $csn->refresh();
        $csn =CreditStageNext::create([
            'credit_stage_id'      => 1,  //1  >> 'Solicitado',
            'credit_stage_next_id' => 12, //12 >> 'Rechazado',
        ]);
        $csn->refresh();
        $csn =CreditStageNext::create([
            'credit_stage_id'      => 2, //2 >> 'Financiando',
            'credit_stage_next_id' => 3, //3 >> 'Financiacion Finalizada',
        ]);
        $csn->refresh();
        $csn =CreditStageNext::create([
            'credit_stage_id'      => 2, //2 >> 'Financiando',
            'credit_stage_next_id' => 11, //11 >> 'Cancelado',
        ]);
        $csn->refresh();
        $csn =CreditStageNext::create([
            'credit_stage_id'      => 3, ////3 >> 'Financiacion Finalizada',
            'credit_stage_next_id' => 4, //4 >> 'Otorgado',
        ]);
        $csn->refresh();
        $csn =CreditStageNext::create([
            'credit_stage_id'      => 3, //3 >> 'Financiacion Finalizada',
            'credit_stage_next_id' => 14, //14 >> 'Financiacion rehabierta'
        ]);
        $csn->refresh();
        $csn =CreditStageNext::create([
            'credit_stage_id'      => 14, //14 >> 'Financiacion rehabierta'
            'credit_stage_next_id' => 2, //2 >> 'Financiando',
        ]);
        $csn->refresh();
        $csn =CreditStageNext::create([
            'credit_stage_id'      => 14, //14 >> 'Financiacion rehabierta'
            'credit_stage_next_id' => 11, //11 >> 'Cancelado',
        ]);
        $csn->refresh();
        $csn =CreditStageNext::create([
            'credit_stage_id'      => 3, //3 >> 'Financiacion Finalizada',
            'credit_stage_next_id' => 11, //11 >> 'Cancelado',
        ]);
        $csn->refresh();
        $csn =CreditStageNext::create([
            'credit_stage_id'      => 4, //4 >> 'Otorgado',
            'credit_stage_next_id' => 5, //5 >> 'Al dia',
        ]);
        $csn->refresh();
        $csn =CreditStageNext::create([
            'credit_stage_id'      => 5, //5 >> 'Al dia',
            'credit_stage_next_id' => 6, //6 >> 'Pago retrasado',
        ]);
        $csn->refresh();
        $csn =CreditStageNext::create([
            'credit_stage_id'      => 5,  //5  >> 'Al dia',
            'credit_stage_next_id' => 10, //10 => 'Saldado',
        ]);
        $csn->refresh();
        $csn =CreditStageNext::create([
            'credit_stage_id'      => 6, //6 >> 'Pago retrasado',
            'credit_stage_next_id' => 5, //5 >> 'Al dia',
        ]);
        $csn->refresh();
        $csn =CreditStageNext::create([
            'credit_stage_id'      => 6, //6 >> 'Pago retrasado',
            'credit_stage_next_id' => 6, //6 >> 'Pago retrasado',
        ]);
        $csn->refresh();
        $csn =CreditStageNext::create([
            'credit_stage_id'      => 6, //6 >> 'Pago retrasado',
            'credit_stage_next_id' => 7, //7 => 'Mora',
        ]);
        $csn->refresh();
        $csn =CreditStageNext::create([
            'credit_stage_id'      => 6,  //6  >> 'Pago retrasado',
            'credit_stage_next_id' => 10, //10 => 'Saldado',
        ]);
        $csn->refresh();
        $csn =CreditStageNext::create([
            'credit_stage_id'      => 7, //7 => 'Mora',
            'credit_stage_next_id' => 5, //5 >> 'Al dia',
        ]);
        $csn->refresh();
        $csn =CreditStageNext::create([
            'credit_stage_id'      => 7, //7 >> 'Mora',
            'credit_stage_next_id' => 6, //6 >> 'Pago retrasado',
        ]);
        $csn->refresh();
        $csn =CreditStageNext::create([
            'credit_stage_id'      => 7, //7 >> 'Mora',
            'credit_stage_next_id' => 7, //7 >> 'Mora',
        ]);
        $csn->refresh();
        $csn =CreditStageNext::create([
            'credit_stage_id'      => 7, //7 >> 'Mora',
            'credit_stage_next_id' => 8, //8 >> 'Citado',
        ]);
        $csn->refresh();
        $csn =CreditStageNext::create([
            'credit_stage_id'      => 7, //7  >> 'Mora',
            'credit_stage_next_id' => 10, //10 => 'Saldado',
        ]);
        $csn->refresh();
        $csn =CreditStageNext::create([
            'credit_stage_id'      => 8, //8 >> 'Citado',
            'credit_stage_next_id' => 5, //5 >> 'Al dia',
        ]);
        $csn->refresh();
        $csn =CreditStageNext::create([
            'credit_stage_id'      => 8, //8 >> 'Citado',
            'credit_stage_next_id' => 6, //6 >> 'Pago retrasado',
        ]);
        $csn->refresh();
        $csn =CreditStageNext::create([
            'credit_stage_id'      => 8, //8 >> 'Citado',
            'credit_stage_next_id' => 7, //7 >> 'Mora',
        ]);
        $csn->refresh();
        $csn =CreditStageNext::create([
            'credit_stage_id'      => 8, //8 >> 'Citado',
            'credit_stage_next_id' => 9, //9 >> 'Litigio'
        ]);
        $csn->refresh();
        $csn =CreditStageNext::create([
            'credit_stage_id'      => 8,  //8  >> 'Citado',
            'credit_stage_next_id' => 10, //10 => 'Saldado',
        ]);
        $csn->refresh();
        $csn =CreditStageNext::create([
            'credit_stage_id'      => 9,  //9 >> 'Litigio'
            'credit_stage_next_id' => 10, //10 => 'Saldado',
        ]);
        $csn->refresh();
        $csn =CreditStageNext::create([
            'credit_stage_id'      => 9,  //9 >> 'Litigio'
            'credit_stage_next_id' => 13, //13 => 'Incobrable',
        ]);
        $csn->refresh();
    }
}
