<?php

use Illuminate\Database\Seeder;
use App\Models\Currency;
use App\Models\ExchangeRate;
use App\Models\Number;

class ExchangeRatesTableSeeder extends Seeder
{
    public static function seeder($date_time){
        echo('Entre al seeder ExchangeRatesTableSeeder
        ');
        $rate = 0.095;
        ExchangeRate::create([
            'from_currency_id' => Currency::getCurrency('ARS')->id,
            'to_currency_id'   => Currency::getCurrency('BRL')->id,
            'date'             => $date_time,
            'rate'             => Number::fix($rate),
        ]);
        ExchangeRate::create([
            'from_currency_id' => Currency::getCurrency('BRL')->id,
            'to_currency_id'   => Currency::getCurrency('ARS')->id,
            'date'             => $date_time,
            'rate'             => Number::fix(1/$rate),
        ]);

        $rate = 16.57;
        ExchangeRate::create([
            'from_currency_id' => Currency::getCurrency('ARS')->id,
            'to_currency_id'   => Currency::getCurrency('CLP')->id,
            'date'             => $date_time,
            'rate'             => Number::fix($rate),
        ]);
        ExchangeRate::create([
            'from_currency_id' => Currency::getCurrency('CLP')->id,
            'to_currency_id'   => Currency::getCurrency('ARS')->id,
            'date'             => $date_time,
            'rate'             => Number::fix(1/$rate),
        ]);

        $rate = 0.022;
        ExchangeRate::create([
            'from_currency_id' => Currency::getCurrency('ARS')->id,
            'to_currency_id'   => Currency::getCurrency('EUR')->id,
            'date'             => $date_time,
            'rate'             => Number::fix($rate),
        ]);
        ExchangeRate::create([
            'from_currency_id' => Currency::getCurrency('EUR')->id,
            'to_currency_id'   => Currency::getCurrency('ARS')->id,
            'date'             => $date_time,
            'rate'             => Number::fix(1/$rate),
        ]);

        $rate = 0.025;
        ExchangeRate::create([
            'from_currency_id' => Currency::getCurrency('ARS')->id,
            'to_currency_id'   => Currency::getCurrency('USD')->id,
            'date'             => $date_time,
            'rate'             => Number::fix($rate),
        ]);
        ExchangeRate::create([
            'from_currency_id' => Currency::getCurrency('USD')->id,
            'to_currency_id'   => Currency::getCurrency('ARS')->id,
            'date'             => $date_time,
            'rate'             => Number::fix(1/$rate),
        ]);

        $rate = 0.82;
        ExchangeRate::create([
            'from_currency_id' => Currency::getCurrency('ARS')->id,
            'to_currency_id'   => Currency::getCurrency('UYU')->id,
            'date'             => $date_time,
            'rate'             => Number::fix($rate),
        ]);
        ExchangeRate::create([
            'from_currency_id' => Currency::getCurrency('UYU')->id,
            'to_currency_id'   => Currency::getCurrency('ARS')->id,
            'date'             => $date_time,
            'rate'             => Number::fix(1/$rate),
        ]);

        $rate = 174.82;
        ExchangeRate::create([
            'from_currency_id' => Currency::getCurrency('BRL')->id,
            'to_currency_id'   => Currency::getCurrency('CLP')->id,
            'date'             => $date_time,
            'rate'             => Number::fix($rate),
        ]);
        ExchangeRate::create([
            'from_currency_id' => Currency::getCurrency('CLP')->id,
            'to_currency_id'   => Currency::getCurrency('BRL')->id,
            'date'             => $date_time,
            'rate'             => Number::fix(1/$rate),
        ]);

        $rate = 0.23;
        ExchangeRate::create([
            'from_currency_id' => Currency::getCurrency('BRL')->id,
            'to_currency_id'   => Currency::getCurrency('EUR')->id,
            'date'             => $date_time,
            'rate'             => Number::fix($rate),
        ]);
        ExchangeRate::create([
            'from_currency_id' => Currency::getCurrency('EUR')->id,
            'to_currency_id'   => Currency::getCurrency('BRL')->id,
            'date'             => $date_time,
            'rate'             => Number::fix(1/$rate),
        ]);


        $rate = 0.26;
        ExchangeRate::create([
            'from_currency_id' => Currency::getCurrency('BRL')->id,
            'to_currency_id'   => Currency::getCurrency('USD')->id,
            'date'             => $date_time,
            'rate'             => Number::fix($rate),
        ]);
        ExchangeRate::create([
            'from_currency_id' => Currency::getCurrency('USD')->id,
            'to_currency_id'   => Currency::getCurrency('BRL')->id,
            'date'             => $date_time,
            'rate'             => Number::fix(1/$rate),
        ]);

        $rate = 8.66;
        ExchangeRate::create([
            'from_currency_id' => Currency::getCurrency('BRL')->id,
            'to_currency_id'   => Currency::getCurrency('UYU')->id,
            'date'             => $date_time,
            'rate'             => Number::fix($rate),
        ]);
        ExchangeRate::create([
            'from_currency_id' => Currency::getCurrency('UYU')->id,
            'to_currency_id'   => Currency::getCurrency('BRL')->id,
            'date'             => $date_time,
            'rate'             => Number::fix(1/$rate),
        ]);

        $rate = 0.0013;
        ExchangeRate::create([
            'from_currency_id' => Currency::getCurrency('CLP')->id,
            'to_currency_id'   => Currency::getCurrency('EUR')->id,
            'date'             => $date_time,
            'rate'             => Number::fix($rate),
        ]);
        ExchangeRate::create([
            'from_currency_id' => Currency::getCurrency('EUR')->id,
            'to_currency_id'   => Currency::getCurrency('CLP')->id,
            'date'             => $date_time,
            'rate'             => Number::fix(1/$rate),
        ]);

        $rate = 0.0015;
        ExchangeRate::create([
            'from_currency_id' => Currency::getCurrency('CLP')->id,
            'to_currency_id'   => Currency::getCurrency('USD')->id,
            'date'             => $date_time,
            'rate'             => Number::fix($rate),
        ]);
        ExchangeRate::create([
            'from_currency_id' => Currency::getCurrency('USD')->id,
            'to_currency_id'   => Currency::getCurrency('CLP')->id,
            'date'             => $date_time,
            'rate'             => Number::fix(1/$rate),
        ]);

        $rate = 0.050;
        ExchangeRate::create([
            'from_currency_id' => Currency::getCurrency('CLP')->id,
            'to_currency_id'   => Currency::getCurrency('UYU')->id,
            'date'             => $date_time,
            'rate'             => Number::fix($rate),
        ]);
        ExchangeRate::create([
            'from_currency_id' => Currency::getCurrency('UYU')->id,
            'to_currency_id'   => Currency::getCurrency('CLP')->id,
            'date'             => $date_time,
            'rate'             => Number::fix(1/$rate),
        ]);
        
        $rate = 1.14;
        ExchangeRate::create([
            'from_currency_id' => Currency::getCurrency('EUR')->id,
            'to_currency_id'   => Currency::getCurrency('USD')->id,
            'date'             => $date_time,
            'rate'             => Number::fix($rate),
        ]);
        ExchangeRate::create([
            'from_currency_id' => Currency::getCurrency('USD')->id,
            'to_currency_id'   => Currency::getCurrency('EUR')->id,
            'date'             => $date_time,
            'rate'             => Number::fix(1/$rate),
        ]);

        $rate = 37.18;
        ExchangeRate::create([
            'from_currency_id' => Currency::getCurrency('EUR')->id,
            'to_currency_id'   => Currency::getCurrency('UYU')->id,
            'date'             => $date_time,
            'rate'             => Number::fix($rate),
        ]);
        ExchangeRate::create([
            'from_currency_id' => Currency::getCurrency('UYU')->id,
            'to_currency_id'   => Currency::getCurrency('EUR')->id,
            'date'             => $date_time,
            'rate'             => Number::fix(1/$rate),
        ]);

        $rate = 32.68;
        ExchangeRate::create([
            'from_currency_id' => Currency::getCurrency('USD')->id,
            'to_currency_id'   => Currency::getCurrency('UYU')->id,
            'date'             => $date_time,
            'rate'             => Number::fix($rate),
        ]);
        ExchangeRate::create([
            'from_currency_id' => Currency::getCurrency('UYU')->id,
            'to_currency_id'   => Currency::getCurrency('USD')->id,
            'date'             => $date_time,
            'rate'             => Number::fix(1/$rate),
        ]);
    }
}
