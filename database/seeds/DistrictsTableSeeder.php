<?php

use Illuminate\Database\Seeder;
use App\Models\District;

class DistrictsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
            'Argentina',
            'Brasil',
            'Chile',
            'España',
            'Estados Unidos',
            'Uruguay'
        */
        $name=[
            'Misiones',
            'Corrientes',
            'Buenos Aires',
        ];
        for($i=0 ; $i<3 ; $i++){
            District::create([
                'name' => $name[$i],
                'country_id' => 1,
            ]);
        };
        $name=[
            'Parana',
            'Santa Catarina',
            'Rio Grande',
        ];
        for($i=0 ; $i<3 ; $i++){
            District::create([
                'name' => $name[$i],
                'country_id' => 2,
            ]);
        };
        $name=[
            'Coquimbo',
            'Parinacota',
        ];
        for($i=0 ; $i<2 ; $i++){
            District::create([
                'name' => $name[$i],
                'country_id' => 3,
            ]);
        };
        $name=[
            'Barcelona',
            'Alicante',
        ];
        for($i=0 ; $i<2 ; $i++){
            District::create([
                'name' => $name[$i],
                'country_id' => 4,
            ]);
        };
        $name=[
            'California',
            'Texas',
        ];
        for($i=0 ; $i<2 ; $i++){
            District::create([
                'name' => $name[$i],
                'country_id' => 5,
            ]);
        };
        $name=[
            'Montevideo',
            'Lavalleja',
        ];
        for($i=0 ; $i<2 ; $i++){
            District::create([
                'name' => $name[$i],
                'country_id' => 5,
            ]);
        };
    }
}
