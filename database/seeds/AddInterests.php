<?php

use Illuminate\Database\Seeder;
use App\Models\Credit;
use Carbon\Carbon;
use App\Models\CreditStage;
use Illuminate\Database\Eloquent\Collection;

class AddInterests extends Seeder
{
    public static function seeder($date)
    {
        if ($date[0] < Carbon::now()) {
            $credits = Credit::all();
            $stages = new Collection();
            $stages->add(CreditStage::getPaymentLate());
            $stages->add(CreditStage::getDefaulter());
            $stages->add(CreditStage::getJudicialCitation());
            $stages->add(CreditStage::getLawsuit());
            foreach ($credits as $credit) {
                if (($credit->isActive()) && ($stages->pluck('id')->contains($credit->lastCreditPhase()->creditStage->id))){
                    $payments = $credit->payments;
                    foreach($payments as $payment){
                        $payment->paymentInterests($date[0]);
                        if(mt_rand(1,4) == 1){
                            $date[0]->addSeconds(1,2);
                        }
                    }
                }
            }
        }
        return $date;
    }
}
