<?php

use Illuminate\Database\Seeder;
use App\Models\Interest;

class InterestsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0 ; $i<15 ; $i++ ){
            $min = (mt_rand(2300,3500))/10000;
            do{
                $initial = (mt_rand(5300,7500))/10000;
            }while($initial <= $min);
            do{
                $max = (mt_rand(9500,13800))/10000;
            }while($max <= $initial);
            $interest = Interest::create([
                'initial_percentage'  => $initial,
                'min_percentage'      => $min,
                'max_percentage'      => $max,
                'commission_percentage' => (mt_rand(1000,2000))/10000,
            ]);
            $interest->refresh();
        }
    }
}
