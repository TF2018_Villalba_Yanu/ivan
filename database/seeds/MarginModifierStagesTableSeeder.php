<?php

use Illuminate\Database\Seeder;
use App\Models\MarginModifierStage;

class MarginModifierStagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
            01 => 'Categoria actualizada'
            02 => 'Categoria dada de baja'
            03 => 'Retencion por credito'
            04 => 'Liberacion por credito'
            05 => 'Retencion por irregularidad en pagos'
            06 => 'Liberacion por regularizar pagos'
            07 => 'Disminucion del margen por comportamiento'
            08 => 'Aumento del margen por comportamiento'
            09 => 'Disminucion del margen por inflacion'
            10 => 'Aumento del margen por deflacion'
            11 => 'Incremento del margen manualmente'
        */
        $name = [
            'Categoria actualizada',
            'Categoria dada de baja',
            'Retencion por credito',
            'Liberacion por credito',
            'Retencion por irregularidad en pagos',
            'Liberacion por regularizar pagos',
            'Disminucion del margen por comportamiento',
            'Aumento del margen por comportamiento',
            'Disminucion del margen por inflacion',
            'Aumento del margen por deflacion',
            'Incremento del margen manualmente',
        ];
        $description = [
            'Se establecio una categoria para el usuario',
            'Ajuste debido a un cambio de categoria',
            'Retencion de margen por solicitud de credito',
            'Se libero margen retenido por un credito',
            'Margen retenido a causa de irregularidad en los pagos',
            'Margen liberado por pagos regularizados',
            'Margen decrementado por comportamiento en pago de credito',
            'Margen aumentado por comportamiento en pago de credito',
            'Se disminuyo el margen a causa de inflacion de la moneda con respecto al dolar estadounidense',
            'Se incremento el margen a causa de deflacion de la moneda con respecto al dolar estadounidense',
            'Se incrementa el margen de forma manual por peticion del usuario y autorizacion de la empresa',
        ];
        $type = [
            'credit',
            'debit',
            'debit',
            'credit',
            'debit',
            'credit',
            'debit',
            'credit',
            'debit',
            'credit',
            'credit',
        ];
        for ($i=0 ; $i<11 ; $i++){
            $mms =MarginModifierStage::create([
                'name'        =>$name[$i],
                'description' =>$description[$i],
                'type'        =>$type[$i],
            ]);
            $mms->refresh();
        }
    }
}
