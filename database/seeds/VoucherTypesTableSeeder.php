<?php

use Illuminate\Database\Seeder;
use App\Models\VoucherType;

class VoucherTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
            01 'Solicitud de crédito'
            02 'Pago de crédito'
            03 'Ingreso de dinero'
            04 'Extraccion de dinero'
            05 'Crédito cancelado'
            06 'Financiamiento'
            07 'Financiamiento cancelado'
            08 'Crédito financiado cancelado'
            09 'Reintegro'
            10 'Reintegro asegurado'
            11 'Crédito rechazado'
            12 'Abono del seguro'
            13 'Ingreso por conversión de divisas'
            14 'Egreso por conversion'
        */
        $name = [
            'Solicitud de crédito',
            'Pago de crédito',
            'Ingreso de dinero',
            'Extraccion de dinero',
            'Crédito cancelado',
            'Financiamiento',
            'Financiamiento cancelado',
            'Crédito financiado cancelado',
            'Reintegro',
            'Reintegro asegurado',
            'Crédito rechazado',
            'Abono del seguro',
            'Ingreso por conversión de divisas',
            'Egreso por conversion',
        ];
        $desc = [
            'Solicitaste un crédito',
            'Realizaste el pago para un crédito',
            'Ingresaste dinero a tu wallet',
            'Quitaste dinero de tu wallet',
            'Cancelaste un crédito',
            'Financiaste un crédito',
            'Cancelaste un financiamiento',
            'Tu financiamiento se ha cancelado porque el beneficiario canceló el crédito',
            'Recibiste una devolución por un financiamiento que realizaste',
            'Recibiste una devolución por parte de la aseguradora para un financiamiento realizado',
            'El crédito que solicitaste fue rechazado',
            'Se pagó la póliza por un crédito asegurado',
            'Ingreso de dinero a tu wallet por una conversión de divisas',
            'Extraxión de dinero a tu wallet por una conversión de divisas',
        ];
        for($i=0 ; $i<14 ; $i++){
            VoucherType::create([
                'name'        => $name[$i],
                'description' => $desc[$i],
            ]);
        }
    }
}
