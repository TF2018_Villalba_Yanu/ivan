<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\Models\Reputation;

class UpdateReputationMargins extends Seeder
{
    public static function seeder($date){
        if ($date[0] < Carbon::now()) {
            $reputations = Reputation::all();
            foreach($reputations as $reputation){
                $reputation->adjustMargin($date[0]);
                if(mt_rand(1,4) == 1){
                    $date[0]->addSeconds(1,2);
                }
            }
        }
        return $date;
    }
}
