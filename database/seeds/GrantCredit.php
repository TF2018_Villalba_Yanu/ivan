<?php

use Illuminate\Database\Seeder;
use App\Models\Credit;
use App\Models\CreditStage;
use App\Models\Payment;
use Carbon\Carbon;

class GrantCredit extends Seeder
{
    public static function seeder($date){
        echo('Entre al seeder GrantCredit
        ');
        for($i=0 ; $i<1 ; $i++){
            if($date[0] < Carbon::now()){
                $loop = 0;
                do{
                    $credit = Credit::all()->random();
                    if($loop >= 50){
                        break;
                    }
                    $loop++;
                }while(!$credit->nextCreditStages()->pluck('id')->contains(CreditStage::getGrant()->id));
                if($loop >= 50){
                    break;
                }
                if ($date[0]->isWeekend()) {
                    $random_int = mt_rand(1,10);
                    switch($random_int){
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                        case 5:
                            $date[0]->addSeconds(mt_rand(3600,5400));//60 ~ 90
                            break;
                        case 6:
                        case 7:
                        case 8:
                            $date[0]->addSeconds(mt_rand(2100,3600));//35 ~ 60
                            break;
                        case 9:
                        case 10:
                            $date[0]->addSeconds(mt_rand(1500,2100));//25 ~ 35
                            break;
                    }
                }else{
                    $random_int = mt_rand(1,10);
                    switch ($random_int){
                        case 1:
                        case 2:
                            $date[0]->addSeconds(mt_rand(3600,5400));//60 ~ 90
                            break;
                        case 3:
                        case 4:
                        case 5:
                            $date[0]->addSeconds(mt_rand(2700,3600));//45 ~ 60
                            break;
                        case 6:
                        case 7:
                        case 8:
                        case 9:
                            $date[0]->addSeconds(mt_rand(1500,2700));//25 ~ 45
                            break;
                        case 10:
                            $date[0]->addSeconds(mt_rand(300,1500));//5 ~ 25
                            break;
                    }
                }
                if($credit->grant($date[0]) == null){
                    echo('Error 2502556');
                    dd();
                }
                $date = DueAndInterests::seeder($date);
            }
        }
        return $date;
    }
}
