<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\Models\ExchangeRate;

class UpdateCurrencyValues extends Seeder
{
    public static function seeder($date){
        if ($date[0] < Carbon::now()) {
            ExchangeRate::updateExchanges($date[0]);
            $date[0]->addSeconds(30,60);
        }
        return $date;
    }
}
