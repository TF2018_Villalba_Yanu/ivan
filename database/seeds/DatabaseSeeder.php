<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\Models\Financing;
use App\Models\Credit;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = 0;
        $this->call(VoucherTypesTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(MarginModifierStagesTableSeeder::class);
        $this->call(InsuranceModifierStagesTableSeeder::class);
        $this->call(InterestModifierStagesTableSeeder::class);
        $this->call(PaymentStagesTableSeeder::class);
        $this->call(PaymentStageNextsTableSeeder::class);
        $this->call(FinancingStagesTableSeeder::class);
        $this->call(FinancingStageNextsTableSeeder::class);
        $this->call(CreditStagesTableSeeder::class);
        $this->call(CreditStageNextsTableSeeder::class);
        $this->call(WalletTypeOfTransactionsTableSeeder::class);
        $this->call(InterestsTableSeeder::class);//15 entidades
        $this->call(InsurancesTableSeeder::class);//15 entidades
        $this->call(CurrenciesTableSeeder::class);//6 entidades. Predefinidas
        $date_time = Carbon::now();
        $date_time->subMonths(12);
        $date_time->setTime(7, 0, 0);   
        $last_due_credits = new Carbon($date_time->toDateTimeString());
        $last_due_credits->setTime(2, 0, 0);
        $date = [
            $date_time,
            $last_due_credits,
        ];
        $date_for_while = new Carbon($date[0]->toDateTimeString());
        ExchangeRatesTableSeeder::seeder($date[0]);
        $this->call(PaymentPlansTableSeeder::class);//20 entidades
        $this->call(CountriesTableSeeder::class);//6 entidades. Predefinidas
        $this->call(DistrictsTableSeeder::class);//8 entidades
        $this->call(CitiesTableSeeder::class);//12 entidades
        $this->call(AddressesTableSeeder::class);//15 entidades
        $this->call(ProfessionsTableSeeder::class);//5 entidades
        //6 currencies, 5 professions =>6*5=30
        $this->call(MaxAmountCreditsSeeder::class);
        $users = UsersTableSeeder::seeder($users); //5 role user
        echo('            ');
        while ($date_for_while < Carbon::now()) {
            if($date[0] < Carbon::now()){
                $date = WalletInputOutputMoneyTableSeeder::seeder($date);//3 instancias en cada iteracion
            }    

            if($date[0] < Carbon::now()){
                $date = CreditsTableSeeder::seeder($date);//1 crédito creados en cada iteracion
            }

            echo('Creditos: '.Credit::all()->count().'
                ');

            if (Credit::all()->count() > 0) {
                if(($date[0] < Carbon::now()) && (mt_rand(1,10) >= 9)){
                    $date = RejectCredit::seeder($date);//1 crédito rechazado. Break automatico
                }else if(($date[0] < Carbon::now()) && (mt_rand(1,10) >= 9)){
                    $date = AbortCredit::seeder($date);//1 crédito cancelado. Break automatico
                }else if(($date[0] < Carbon::now()) && (mt_rand(1,10) <= 9)){
                    $date = Approvate::seeder($date);//1 créditos aprobados. Break automatico
                }

                if($date[0] < Carbon::now()){
                    $date = Financiate::seeder($date);//maximo 7 iteraciones. Break automatico
                }

                if(($date[0] < Carbon::now()) && (mt_rand(1,10) >= 9)){
                    $date = AbortCredit::seeder($date);//1 crédito cancelado. Break automatico
                } 
            }
            
            if (Financing::all()->count() > 0) {
                if($date[0] < Carbon::now()){
                    $date = AbortFinancing::seeder($date);//1 financiacion cancelada. Break automatico
                }
            }

            if (Credit::all()->count() > 0) {
                if($date[0] < Carbon::now()){
                    $date = GrantCredit::seeder($date);//1 crédito trasferido. Break automatico
                }

                if(($date[0] < Carbon::now()) && (mt_rand(1,10) >= 6)){
                    $date = PayCredit::seeder($date);//1 pago. Break Automatico
                }

                if(($date[0] < Carbon::now()) && (mt_rand(1,30) == 30)){
                    $date = LawsuitCredit::seeder($date);
                }
                
                if(($date[0] < Carbon::now()) && (mt_rand(1,140) == 140)){
                    $date = BadCredit::seeder($date);
                }
                if(($date[0] < Carbon::now()) && (mt_rand(1,100) >= 80)){
                    $date = ExchangeSeeder::seeder($date);
                }
            }
            //if (mt_rand(1,150) == 150) {
            //    $users = UsersTableSeeder::seeder($users);
            //}
            $date_for_while = new Carbon($date[0]->toDateTimeString());
            $date_for_while->addHours(3);
            echo('
            ');
        }//while
        echo('Carga finalizada '.Carbon::now().'
        '); 
    }
}
