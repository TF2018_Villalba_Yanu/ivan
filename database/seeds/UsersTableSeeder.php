<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Profession;
use App\Models\MaxAmountCredit;
use App\Models\Address;
use App\Models\Role;
use App\Models\Reputation;

class UsersTableSeeder extends Seeder
{
    public static function seeder($users){
        echo('Entre al seeder UsersTableSeeder
        ');
        if ($users == 0){
            $names = [
                'Fernando Nahirñak',
                'Cristian Zborowski',
                'Martin Figueredo',
                'Maxi Rodriguez',
                'Lucas Makko',
                'Sebastian Acevedo',
                'Daniel Skromeda',
                'Ivan Yanuchauski',
            ];
            $emails = [
                'fernando@email.com',
                'cristian@email.com',
                'martin@email.com',
                'maxi@email.com',
                'lucas@email.com',
                'sebastian@email.com',
                'daniel@email.com',
                'ivan@email.com',
            ];
            for($i=0 ; $i<8 ; $i++){ 
                $users++;          
                $user = User::create([
                    'name'          => $names[$i],
                    'phone'         => null,//can be null
                    'email'         => $emails[$i],
                    'password'      => '$2y$10$wUjym3yw.Yxd815rSypfEOVtZmgsoUdIwq1huzqB8C4u.uLRaFBPS',//123456. Todavia no se cual es el metodo de encriptacion
                    'role_id'       => Role::getUserRole()->id,
                    'reputation_id' => Reputation::create()->id,
                    'address_id'    => Address::all()->random()->id,
                    'profession_id' => Profession::all()->random()->id,
                    'birthdate'     => null,//can be null
                ]);
            }//end for

            /*
                getUserRole()---------------> 
                getAdminRole()--------------> administrador@email.com
                getParamManagerRole()-------> parametros@email.com
                getInsuranceManagerRole()---> seguros@email.com
                getCommissionManagerRole()--> comisiones@email.com
                getLawsuitManagerRole()-----> litigios@email.com
                getDefaulterManagerRole()---> morosos@email.com
                getCreditManagerRole()------> creditos@email.com
                getAuditRole()--------------> auditor@email.com
            */
            //admin
            $user = User::create([
                'name'          => 'Administrador',
                'phone'         => null,//can be null
                'email'         => 'administrador@email.com',
                'password'      => '$2y$10$wUjym3yw.Yxd815rSypfEOVtZmgsoUdIwq1huzqB8C4u.uLRaFBPS',//123456. Todavia no se cual es el metodo de encriptacion
                'role_id'       => Role::getAdminRole()->id,
                'reputation_id' => Reputation::create()->id,
                'address_id'    => Address::all()->random()->id,
                'profession_id' => Profession::all()->random()->id,
                'birthdate'     => null,//can be null
            ]);
            //param-manager
            $user = User::create([
                'name'          => 'Administrador de parametros',
                'phone'         => null,//can be null
                'email'         => 'parametros@email.com',
                'password'      => '$2y$10$wUjym3yw.Yxd815rSypfEOVtZmgsoUdIwq1huzqB8C4u.uLRaFBPS',//123456. Todavia no se cual es el metodo de encriptacion
                'role_id'       => Role::getParamManagerRole()->id,
                'reputation_id' => Reputation::create()->id,
                'address_id'    => Address::all()->random()->id,
                'profession_id' => Profession::all()->random()->id,
                'birthdate'     => null,//can be null
            ]);
            //insurance-manager
            $user = User::create([
                'name'          => 'Administrador de seguros',
                'phone'         => null,//can be null
                'email'         => 'seguros@email.com',
                'password'      => '$2y$10$wUjym3yw.Yxd815rSypfEOVtZmgsoUdIwq1huzqB8C4u.uLRaFBPS',//123456. Todavia no se cual es el metodo de encriptacion
                'role_id'       => Role::getInsuranceManagerRole()->id,
                'reputation_id' => Reputation::create()->id,
                'address_id'    => Address::all()->random()->id,
                'profession_id' => Profession::all()->random()->id,
                'birthdate'     => null,//can be null
            ]);
            //commission-manager
            $user = User::create([
                'name'          => 'Administrador de comisiones',
                'phone'         => null,//can be null
                'email'         => 'comisiones@email.com',
                'password'      => '$2y$10$wUjym3yw.Yxd815rSypfEOVtZmgsoUdIwq1huzqB8C4u.uLRaFBPS',//123456. Todavia no se cual es el metodo de encriptacion
                'role_id'       => Role::getCommissionManagerRole()->id,
                'reputation_id' => Reputation::create()->id,
                'address_id'    => Address::all()->random()->id,
                'profession_id' => Profession::all()->random()->id,
                'birthdate'     => null,//can be null
            ]);
            //lawsuit-manager
            $user = User::create([
                'name'          => 'Abogado',
                'phone'         => null,//can be null
                'email'         => 'litigios@email.com',
                'password'      => '$2y$10$wUjym3yw.Yxd815rSypfEOVtZmgsoUdIwq1huzqB8C4u.uLRaFBPS',//123456. Todavia no se cual es el metodo de encriptacion
                'role_id'       => Role::getLawsuitManagerRole()->id,
                'reputation_id' => Reputation::create()->id,
                'address_id'    => Address::all()->random()->id,
                'profession_id' => Profession::all()->random()->id,
                'birthdate'     => null,//can be null
            ]);
            //defaulter-manager
            $user = User::create([
                'name'          => 'Administrador de morosos',
                'phone'         => null,//can be null
                'email'         => 'morosos@email.com',
                'password'      => '$2y$10$wUjym3yw.Yxd815rSypfEOVtZmgsoUdIwq1huzqB8C4u.uLRaFBPS',//123456. Todavia no se cual es el metodo de encriptacion
                'role_id'       => Role::getDefaulterManagerRole()->id,
                'reputation_id' => Reputation::create()->id,
                'address_id'    => Address::all()->random()->id,
                'profession_id' => Profession::all()->random()->id,
                'birthdate'     => null,//can be null
            ]);
            //getCreditManagerRole()
            $user = User::create([
                'name'          => 'Administrador de creditos',
                'phone'         => null,//can be null
                'email'         => 'creditos@email.com',
                'password'      => '$2y$10$wUjym3yw.Yxd815rSypfEOVtZmgsoUdIwq1huzqB8C4u.uLRaFBPS',//123456. Todavia no se cual es el metodo de encriptacion
                'role_id'       => Role::getCreditManagerRole()->id,
                'reputation_id' => Reputation::create()->id,
                'address_id'    => Address::all()->random()->id,
                'profession_id' => Profession::all()->random()->id,
                'birthdate'     => null,//can be null
            ]);
            //getAuditRole()
            $user = User::create([
                'name'          => 'Auditor',
                'phone'         => null,//can be null
                'email'         => 'auditor@email.com',
                'password'      => '$2y$10$wUjym3yw.Yxd815rSypfEOVtZmgsoUdIwq1huzqB8C4u.uLRaFBPS',//123456. Todavia no se cual es el metodo de encriptacion
                'role_id'       => Role::getAuditRole()->id,
                'reputation_id' => Reputation::create()->id,
                'address_id'    => Address::all()->random()->id,
                'profession_id' => Profession::all()->random()->id,
                'birthdate'     => null,//can be null
            ]);
        }else{
            $users++;
            $user = User::create([
                'name'          => 'User'.$users,
                'phone'         => null,//can be null
                'email'         => 'user'.$users.'@email.com',
                'password'      => '$2y$10$wUjym3yw.Yxd815rSypfEOVtZmgsoUdIwq1huzqB8C4u.uLRaFBPS',//123456. Todavia no se cual es el metodo de encriptacion
                'role_id'       => Role::getUserRole()->id,
                'reputation_id' => Reputation::create()->id,
                'address_id'    => Address::all()->random()->id,
                'profession_id' => Profession::all()->random()->id,
                'birthdate'     => null,//can be null
            ]);
        }
        return $users;
    }
}
