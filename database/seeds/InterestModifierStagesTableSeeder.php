<?php

use Illuminate\Database\Seeder;
use App\Models\InterestModifierStage;

class InterestModifierStagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
            01 => 'Disminucion del interes por comportamiento'
            02 => 'Aumento del interes por comportamiento'
        */
        $name = [
            'Disminucion del interes por comportamiento',
            'Aumento del interes por comportamiento',
        ];
        $description = [
            'Disminucion del interest debido a buen comportamiento con los pagos',
            'Aumento del interest debido a pagos fuera de termino',
        ];
        for($i=0 ; $i<2 ; $i++){
            InterestModifierStage::create([
                'name'        => $name[$i],
                'description' => $description[$i],
            ]);
        }
    }
}
