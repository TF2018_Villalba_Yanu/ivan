<?php

use Illuminate\Database\Seeder;
use App\Models\InsuranceModifierStage;

class InsuranceModifierStagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
            01 => 'Disminucion del seguro por comportamiento'
            02 => 'Aumento del seguro por comportamiento'
        */
        $name = [
            'Disminucion del seguro por comportamiento',
            'Aumento del seguro por comportamiento',            
        ];
        $description = [
            'Disminucion del seguro debido a buen comportamiento con los pagos',
            'Aumento del seguro debido a pagos fuera de termino',
        ];
        for($i=0 ; $i<2 ; $i++){
            InsuranceModifierStage::create([
                'name'        => $name[$i],
                'description' => $description[$i],
            ]);
        }
    }
}
