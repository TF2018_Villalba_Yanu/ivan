<?php

use Illuminate\Database\Seeder;
use App\Models\CreditStage;
use App\Models\Credit;
use App\Models\Wallet;
use App\Models\Financing;
use App\Models\User;
use App\Models\Role;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use App\Models\ExchangeRate;

class Financiate extends Seeder
{
    public static function seeder($date){
        echo('Entre al seeder Financiate
        ');
        for($i=0 ; $i<7 ; $i++){
            if($date[0] < Carbon::now()){
                $loop = 0;
                do{
                    $credit = Credit::all()->random();
                    if($loop >= 50){
                        break;
                    }
                    $loop++;
                }while($credit->lastCreditPhase()->creditStage->id != CreditStage::getFinancing()->id);
                if($loop >= 50){
                    break;
                }
                $wallets = new Collection();
                foreach (Wallet::cursor() as $item) {
                    if (($item->currency_id == $credit->currency()->id) && ($item->accountBalance() >= $credit->currency()->min_value) && ($item->user->role->id == Role::getUserRole()->id)) {
                        $wallets->add($item);
                    }
                }
                if($wallets->count() == 0){
                    break;
                }
                $wallet = $wallets->random();
                $loop = 0;
                do{
                    $amount = mt_rand(100000,1000000000)*0.00000001;
                    $amount = ExchangeRate::localConvert('USD',$credit->currency()->iso_code,$amount);
                    $amount = $credit->currency()->truncate($amount);
                    if($loop >= 50){
                        break;
                    }
                    $loop++;
                }while(($amount >= $wallet->accountBalance()) || ($amount < $credit->currency()->min_value));
                if($loop >= 50){
                    break;
                }
                if($amount > ($credit->amount - $credit->financedAmount())){
                    $amount = ($credit->amount - $credit->financedAmount());
                }
                if ($date[0]->isWeekend()) {
                    $random_int = mt_rand(1,10);
                    switch($random_int){
                        case 1:
                        case 2:
                        case 3:
                        case 4:
                        case 5:
                            $date[0]->addSeconds(mt_rand(3600,5400));//60 ~ 90
                            break;
                        case 6:
                        case 7:
                        case 8:
                            $date[0]->addSeconds(mt_rand(2100,3600));//35 ~ 60
                            break;
                        case 9:
                        case 10:
                            $date[0]->addSeconds(mt_rand(1500,2100));//25 ~ 35
                            break;
                    }
                }else{
                    $random_int = mt_rand(1,10);
                    switch ($random_int){
                        case 1:
                        case 2:
                            $date[0]->addSeconds(mt_rand(3600,5400));//60 ~ 90
                            break;
                        case 3:
                        case 4:
                        case 5:
                            $date[0]->addSeconds(mt_rand(2700,3600));//45 ~ 60
                            break;
                        case 6:
                        case 7:
                        case 8:
                        case 9:
                            $date[0]->addSeconds(mt_rand(1500,2700));//25 ~ 45
                            break;
                        case 10:
                            $date[0]->addSeconds(mt_rand(300,1500));//5 ~ 25
                            break;
                    }
                }
                $financing_phase = Financing::financiate($credit,$wallet,(bool) mt_rand(0,1),$amount, $date[0]);
                if($financing_phase == null){
                    echo('Error 55185');
                    dd();
                }
                $date = DueAndInterests::seeder($date);
            }
        }
        return $date;
    }
}
