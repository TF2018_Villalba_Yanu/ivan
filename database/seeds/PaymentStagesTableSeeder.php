<?php

use Illuminate\Database\Seeder;
use App\Models\PaymentStage;

class PaymentStagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
            1 => 'Generado' --------> Orden de pago generada
            2 => 'Pago' ------------> Pago registrado
            3 => 'Vencido' ---------> Se veció la orden de pago
            4 => 'Pagado temporalmente por el seguro' -> Se pagó a los acreedores por medio del seguro
            5 => 'Interes agregado' ----> Interes adicional por mora
            6 => 'Saldado' ---------> Orden de pago cancelada
            7 => 'Incobrable' ------> La orden de pago no pudo ser saldada
        */
        $name = [
            'Generado',
            'Pago',
            'Vencido',
            'Pagado temporalmente por el seguro',
            'Interes agregado',
            'Saldado',
            'Incobrable',
        ];
        $description = [
            'Orden de pago generada',
            'Pago registrado',
            'Se veció la orden de pago',
            'Se pagó a los acreedores por medio del seguro',
            'Interes adicional por mora',
            'Orden de pago cancelada',
            'La orden de pago no pudo ser saldada',
        ];
        for ($i=0 ; $i<7 ; $i++){
            $ps = PaymentStage::create([
                'name'        =>$name[$i],
                'description' =>$description[$i],
            ]);
            $ps->refresh();
        }
    }
}
