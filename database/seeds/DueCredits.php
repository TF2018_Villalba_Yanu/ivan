<?php

use Illuminate\Database\Seeder;
use App\Models\Credit;
use App\Models\CreditStage;

use Carbon\Carbon;

class DueCredits extends Seeder
{
    public static function seeder($date){
        if ($date[0] < Carbon::now()) {
            $credits = Credit::all();
            foreach ($credits as $credit) {
                if (($credit->isActive()) && ($credit->lastCreditPhase()->creditStage->id == CreditStage::getGood()->id) && ($credit->firstUnpayedPayment($date[0])->due_date < $date[0])){
                    $payments = $credit->payments; 
                    foreach($payments as $payment){
                        $payment->paymentDue($date[0]);
                        if(mt_rand(1,4) == 1){
                            $date[0]->addSeconds(1,2);
                        }
                    }
                }
            }
        }
        return $date;
    }
}
