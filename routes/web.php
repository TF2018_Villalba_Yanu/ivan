<?php
use App\Models\District;
use App\Models\City;
use App\Models\Role;
use App\Models\Wallet;
use Illuminate\Database\Eloquent\Collection;
use App\Models\Currency;
use App\Models\CreditStage;
use App\Models\FinancingStage;
use App\Models\Credit;
use App\Models\Payment;
use Carbon\Carbon;
use App\Models\PaymentStage;
use App\Models\Financing;
use Carbon\CarbonInterval;
use App\Models\PaymentPlan;
use App\Models\Reputation;
use App\Models\Interest;
use App\Models\Insurance;
use App\Models\CustomDateInterval;
use App\Models\User;
use App\Models\ExchangeRate;
use App\Models\Exchange;
//namespace App\Models\;
Route::group(['middleware'=> ['auth']],function(){
    Route::get ('/credits/bad',               'CreditController@indexBad'); //lawsuitManager
    Route::get ('/credits/bad/currency={id}', 'CreditController@indexBadForCurrency');//lawsuitManager
    Route::get ('/credits/bad/{credit_id}',   'CreditController@showBad'); //lawsuitManager

    Route::get ('/credits/lawsuit',               'CreditController@indexLawsuit'); //lawsuitManager
    Route::get ('/credits/lawsuit/currency={id}', 'CreditController@indexLawsuitForCurrency');//lawsuitManager
    Route::get ('/credits/lawsuit/{credit_id}',   'CreditController@showLawsuit'); //lawsuitManager

    Route::get ('/credits/judicial-citation',               'CreditController@indexJudicialCitation'); //lawsuitManager
    Route::get ('/credits/judicial-citation/currency={id}', 'CreditController@indexJudicialCitationForCurrency');//lawsuitManager
    Route::get ('/credits/judicial-citation/{credit_id}',   'CreditController@showJudicialCitation'); //lawsuitManager

    Route::get ('/credits/late-payment',               'CreditController@indexLatePayment'); //defaulter
    Route::get ('/credits/late-payment/currency={id}', 'CreditController@indexLatePaymentForCurrency');//defaulter
    Route::get ('/credits/late-payment/{credit_id}',   'CreditController@showLatePayment'); //defaulter

    Route::get ('/credits/defaulter',               'CreditController@indexDefaulter'); //defaulter
    Route::get ('/credits/defaulter/currency={id}', 'CreditController@indexDefaulterForCurrency');//defaulter
    Route::get ('/credits/defaulter/{credit_id}',   'CreditController@showDefaulter'); //defaulter

    Route::get ('/credits/to-approve',               'CreditController@indexToApprove'); //creditManager
    Route::get ('/credits/to-approve/currency={id}', 'CreditController@indexToApproveForCurrency');//creditManager
    Route::get ('/credits/to-approve/{credit_id}',   'CreditController@showToApprove'); //creditManager
    
    Route::get ('/profile',              'UserController@profile');//Cualquier usuario

    Route::get ('/users',                'UserController@index');//Admin solo
    Route::get ('/users/create',function(){
        if(auth()->user()->role->id == Role::getAdminRole()->id)
            return view('site.users.create-form'); 
        else
        return view('errors.404');
    });//Admin

    Route::post('/users',                'UserController@create');//admin
    Route::get ('/users/{user_id}',      'UserController@show');//admin
    Route::get ('/users/{user_id}/edit', 'UserController@edit');//admin para todos los user_id y cualquiera si ID== $user->id  
    Route::post('/users/{user_id}',      'UserController@store');//admin para todos los user_id y cualquiera si ID== $user->id  

    Route::get ('/currencies/create',function(){
        if(auth()->user()->role->id == Role::getCreditManagerRole()->id)
            return view('site.currencies.create-form'); 
        else
            return view('errors.404');

    });//Credit Manager solamente
    Route::post('/currencies',                    'CurrencyController@create');//Credit Manager solamente
    Route::get ('/currencies',                    'CurrencyController@index');//Credit Manager solamente
    Route::get ('/currencies/{currency_id}',      'CurrencyController@show');//Credit Manager solamente
    Route::get ('/currencies/{currency_id}/edit', 'CurrencyController@edit');//Credit Manager solamente
    Route::post('/currencies/{currency_id}',      'CurrencyController@update');//Credit Manager solamente

    Route::get ('/payment-plans/create', function(){
        if(auth()->user()->role->id == Role::getCreditManagerRole()->id)
            return view('payment-plans.create-form'); 
        else
            return view('errors.404');

    });//Credit Manager solamente
    Route::get ('/payment-plans',               'PaymentPlanController@index');//Credit Manager solamente
    Route::post('/payment-plans',               'PaymentPlanController@create');//Credit Manager solamente
    Route::get ('/payment-plans/currency={id}', 'PaymentPlanController@indexForCurrency');//Credit Manager solamente
    Route::get ('/payment-plans/{id}',          'PaymentPlanController@show');//Credit Manager solamente
    Route::get ('/payment-plans/{id}/edit',     'PaymentPlanController@edit');//Credit Manager solamente
    Route::post('/payment-plans/{id}',          'PaymentPlanController@update');//Credit Manager solamente
    
    Route::post('/ajax/exchange',  'ExchangeController@create');//User only
    Route::get ('/exchanges',      'ExchangeController@index');//User only
    Route::get ('/exchanges/{id}', 'ExchangeController@show');//User only
    
    Route::get ('/roles/create', function(){
        if(auth()->user()->role->id == Role::getAdminRole()->id)
            return view('site.roles.create-form');
        else
            return view('errors.404');
    });//Solamente Admin
    Route::post('/roles',                'RoleController@create');//Solamente Admin
    Route::get ('/roles',                'RoleController@index');//Solamente Admin
    Route::get ('/roles/{role_id}',      'RoleController@show');//Solamente Admin
    Route::get ('/roles/{role_id}/edit', 'RoleController@edit');//Solamente Admin
    Route::post('/roles/{role_id}',      'RoleController@update');//Solamente Admin
    
    Route::get ('/vouchers', 'VoucherController@index');
    Route::get ('/vouchers/{id}', 'VoucherController@show');

    Route::get ('/reputations/{reputation_id}/from-credit={credit_id}', 'ReputationController@creditManagershow');//Solamente creditManager
    Route::get ('/reputation', 'ReputationController@show');//Solamente user

    Route::get ('/audits/count={number}', 'AuditController@index');//audit
    Route::get ('/audits/{id}', 'AuditController@show');//audit
    
    Route::get ('/credits',                        'CreditController@index');//Solamente user
    Route::get ('/credits/currency={id}',          'CreditController@indexForCurrency');//solamente user
    Route::get ('/credits/{credit_id}',            'CreditController@show');//user o creditManager

    Route::get ('/financings',                   'FinancingController@index');//user
    Route::get ('/financings/wallet={wallet_id}',     'FinancingController@listOfFinancings');//user
    Route::get ('/financings/currency={id}',     'FinancingController@indexForCurrency');//user
    Route::get ('/financings/{id}',            'FinancingController@show');//user

    Route::get ('/wallets',             'WalletController@index');//user, insurance_manager y commision_manager
    Route::get ('/wallets/{wallet_id}', 'WalletController@show');//user, insurance_manager y commision_manager
    Route::get ('/transactions/{id}',   'WalletTransactionController@index');//user, insurance_manager y commision_manager
    
    Route::get ('/credits/{credit_id}/payments', 'PaymentController@index');//user
    Route::get ('/payments/{payment_id}',        'PaymentController@show');//user o creditManager
    
    Route::get ('/professions',              'ProfessionController@index');//Solamente creditManager
    Route::post('/professions',              'ProfessionController@create');//Solamente creditManager
    Route::get ('/professions/create',function(){
        if(auth()->user()->role->id == Role::getCreditManagerRole()->id)
            return view('site.professions.create-form'); 
        else
            return view('errors.404');
     });//Solamente creditManager
    Route::get ('/professions/{id}',                'ProfessionController@show');//Solamente creditManager
    Route::post('/professions/{id}',                'ProfessionController@update');//Solamente creditManager
    Route::get ('/professions/{id}/edit','ProfessionController@edit');//Solamente creditManager

    Route::get ('/max-amounts',              'MaxAmountCreditController@index');//Solamente creditManager
    Route::get ('/max-amounts/{id}',         'MaxAmountCreditController@show');//Solamente creditManager
    Route::get ('/max-amounts/{id}/edit',    'MaxAmountCreditController@edit');//Solamente creditManager
    Route::post('/max-amounts/{id}',         'MaxAmountCreditController@update');//Solamente creditManager
    
    Route::get ('/home', 'HomeController@index')->name('home');
    Route::get ('/404',  'Site\SiteController@error404');
    Route::post('/ajax/reject-credit', 'CreditController@reject'); //creditManager
    Route::post('/ajax/grant-credit', 'CreditController@grant');//user
    Route::post('/ajax/approvate-credit', 'CreditController@approvate'); //creditManager
    Route::post('/ajax/citation-credit', 'CreditController@citation'); //defaulter
    Route::post('/ajax/lawsuit-credit', 'CreditController@lawsuit'); //lawsuit
    Route::post('/ajax/bad-credit', 'CreditController@bad'); //lawsuit
    Route::post('/ajax/abort-credit', 'CreditController@abort');//User
    Route::post('/ajax/request-credit', 'CreditController@create');//user
    Route::get ('/ajax/currencies/{currency_id}/credit-margin', function($currency_id){
        $currency = Currency::find($currency_id);
        if ($currency != null) {
            return $currency->creditMargin();
        }
     });
    Route::get ('/ajax/currencies/{currency_id}/credit-min-amount', function($currency_id){
        $currency = Currency::find($currency_id);
        if ($currency != null) {
            return $currency->creditMinAmount();
        }
     });
    Route::post('/ajax/pay-credit','CreditController@pay');//user
    Route::post('/ajax/financiate','FinancingController@financiate');//user
    Route::post('/ajax/cancel-financing','FinancingController@abort');//user
    Route::post('/ajax/input-money','WalletController@inputMoney');//user, insurance_manager y commision_manager
    Route::post('/ajax/output-money','WalletController@outputMoney');//user, insurance_manager y commision_manager
    Route::get ('/ajax/districts/{country_id}', function($country_id){
        return District::where('country_id',$country_id)->get();
     });
    Route::get ('/ajax/cities/{district_id}', function($district_id){
        return City::where('district_id',$district_id)->get();
     });
    Route::get ('/ajax/wallets', function(){
        $wallets = auth()->user()->wallets;
        $return = new Collection();
        foreach ($wallets as $wallet){
            if ($wallet->walletTransactions->count() > 0) {
                $return->add($wallet);
            }
        }
        return $return;
     });
    Route::get ('/ajax/last-exchange-rate/{from_iso_code}/{to_iso_code}',function($from_iso_code, $to_iso_code){
        $from_currency = Currency::getCurrency($from_iso_code);
        $to_currency = Currency::getCurrency($to_iso_code);
        if(($from_currency != null) && ($to_currency != null)){
            $exchange_rate = ExchangeRate::lastExchangeRate($from_currency, $to_currency);
            if($exchange_rate != null){
                $return = array(
                    "id"               => $exchange_rate->id,
                    "from_currency_id" => $exchange_rate->from_currency_id,
                    "to_currency_id"   => $exchange_rate->to_currency_id,
                    "date"             => $exchange_rate->date,
                    "rate"             => $exchange_rate->rate,
                    "rate_in_currency" => '1 '.$from_currency->iso_code.' = '.$from_currency->truncate($exchange_rate->rate).' '.$to_currency->iso_code,
                    "diff_for_humans"  => $exchange_rate->date->format('d/m/Y').' a las '.$exchange_rate->date->format('h:m').' ('.$exchange_rate->date->diffForHumans().')',
                );
                return $return;
            } 
        }  
    });
    Route::get ('/ajax/wallets/currency={currency_id}', function($currency_id){
        return auth()->user()->wallets->where('currency_id',$currency_id)->first();
     });
    Route::get ('/ajax/wallets/{id}', function($id){
        $wallet = Wallet::find($id);
        if($wallet != null){
            $return = array(
                'id'              => $wallet->id,
                'user_id'         => $wallet->user_id,
                'currency_id'     => $wallet->currency_id,
                'account_balance' => $wallet->accountBalance(),
                'currency'        => $wallet->currency,
            );
            return $return;
        }
     });
    Route::get ('/ajax/payments/{id}/unpayed-amount', function($id){
        $payment = Payment::find($id);
        if ($payment != null) {
            return $payment->unpayedAmount();
        }
     });//user
    Route::get ('/ajax/payments/{id}/payment-number', function($id){
        $payment = Payment::find($id);
        if ($payment != null) {
            return $payment->number();
        }
     });//user
    Route::get ('/ajax/interests/{id}', function($id){
        return Interest::find($id);
     });
    Route::get ('/ajax/insurances/{id}', function($id){
        return Insurance::find($id);
     });
    Route::get ('/ajax/custom-date-intervals/{id}', function($id){
        return CustomDateInterval::find($id);
     });
    Route::get ('/ajax/payment-plans/currency={id}', function($id){
        $currency = Currency::find($id);
        if ($currency != null) {
            $payment_plans = $currency->paymentPlans;
            $items = new Collection();
            $user = User::find(auth()->user()->id);
            foreach ($payment_plans as $payment_plan) {
                $credit_data = Credit::simutalteCredit($user,$payment_plan);
                $interest = $credit_data['interest_percentage'] + $credit_data['insurance_percentage']['debtor'];
                $frequency = CarbonInterval::instance($payment_plan->dueDatesInterval->toDateInterval());
                $item = array(
                    'id'              => $payment_plan->id,
                    'description'     => $payment_plan->payments_number.' cuotas cada '.$frequency->diffForHumans().'. Interes: '.$currency->percentage($interest),
                    'interest'        => $interest,
                    'payments_number' => $payment_plan->payments_number,
                );
                $items->add($item);
            }
            return $items;
        }
     });//user
    Route::get ('/ajax/payment-plans/{id}', function($id){
        return PaymentPlan::find($id);
     });
    Route::get ('/ajax/simulate-credit/{payment_plan_id}', function($payment_plan_id){
        $payment_plan = PaymentPlan::find($payment_plan_id);
        if ($payment_plan != null) {
            $user = User::find(auth()->user()->id);
            return Credit::simutalteCredit($user,$payment_plan);
        }
     });//user
    Route::get ('/ajax/credits/{id}', function($id){
        $credit = Credit::find($id);
        if($credit != null){
            $array = array(
                'id'                            => $credit->id, 
                'amount'                        => $credit->amount,
                'interest_percentage'           => $credit->interest_percentage,
                'commission_percentage'         => $credit->commission_percentage,
                'creditor_insurance_percentage' => $credit->creditor_insurance_percentage,
                'debtor_insurance_percentage'   => $credit->debtor_insurance_percentage,
                'payments_number'               => $credit->payments_number,
                'wallet_id'                     => $credit->wallet_id,
                'grace_period_id'               => $credit->grace_period_id,
                'interest_for_late_payment'     => $credit->interest_for_late_payment,
                'due_dates_interval_id'         => $credit->due_dates_interval_id,
                'financing_left'                => $credit->amount - $credit->financedAmount(),
                'rank'                          => $credit->wallet->user->reputation->rank(),
                'unpayed_amount'                => $credit->unpayedAmount(),
                'first_unpayed_payment'         => $credit->firstUnpayedPayment(),
                'wallet'                        => $credit->wallet,
                'wallet-account-balance'        => $credit->wallet->accountBalance(),
                'currency'                      => $credit->currency(),
            );
            return $array;
        }
     });
    Route::get ('/ajax/financings/credit={credit_id}', function($credit_id){
        $credit = Credit::find($credit_id);
        if ($credit != null) {
            $wallet = auth()->user()->wallets->where('currency_id',$credit->currency()->id)->first();
            if ($wallet != null) {
                $financing = Financing::where('credit_id',$credit->id)
                            ->where('wallet_id',$wallet->id)
                            ->first();
                return $financing;
            }
        }
     });
    Route::get ('/ajax/financings/{id}/amount', function($id){
        $financing = Financing::find($id);
        if ($financing != null) {
            return $financing->amount();
        }
     });
    Route::get ('/ajax/currencies', function(){
        return Currency::all();
     });
    Route::get ('/ajax/currencies/{id}', function($id){
        return Currency::find($id);
     });
    Route::get ('/ajax/string-to-number/{currency_id}/{string}', function($currency_id, $string){
        $currency = Currency::find($currency_id);
        if ($currency != null) {
            $number = $currency->stringToNumber($string);
            if (is_numeric($number)) {
                return $number;
            } else {
                return "error";
            }
        }
     });
    Route::get ('/ajax/wallets-with-balance', function(){
        $items = new Collection();
        $wallets = auth()->user()->wallets;
        foreach ($wallets as $wallet) {
            if ($wallet->accountBalance() >= $wallet->currency->min_value) {
                $items->add($wallet);
            }
        }
        return $items;
     });
    Route::get ('/ajax/wallets-to-financiate', function(){
        return Wallet::walletsToFinancing();
        
     });
    Route::get ('/ajax/number-to-string/{currency_id}/{number}/{truncate}', function($currency_id, $number,$truncate){
        if (is_numeric($number)) {
            $currency = Currency::find($currency_id);
            if ($currency != null) {
                $string = $currency->numberToString($number,$truncate);
                return $string;
            }
        } else {
            return "error";
        }
     });
    Route::get ('/ajax/percentage/{currency_id}/{percentage}',function($currency_id,$percentage){
        if (is_numeric($percentage)) {
            $currency = Currency::find($currency_id);
            if ($currency != null) {
                return $currency->percentage($percentage);
            }
        } else {
            return "error";
        }
     });
    Route::get ('/ajax/data-for-draw-wallets-founds-donutchart', function(){
        $wallets = auth()->user()->wallets;
        $return = new Collection();
        $usd_currency = Currency::getCurrency('USD');
        $wallets_usd = auth()->user()->walletsInUSD();
        foreach ($wallets as $wallet){
            if (($wallet->walletTransactions->count() > 0) && ($wallet->accountBalance() >= $wallet->currency->min_value)) {
                $currency =Currency::find($wallet->currency_id);
                $usd_amount = ExchangeRate::localConvert($currency->iso_code,$usd_currency->iso_code,$wallet->accountBalance());
                if($usd_amount >= $wallets_usd * 0.001){
                    $return->add([
                        ucfirst($currency->name).': '.$currency->numberToString($wallet->accountBalance()),
                        $usd_amount,
                        ucfirst($currency->name).': '.$usd_currency->iso_code.' '.$usd_currency->numberToString($usd_amount),
                    ]);
                }
            }
        }
        return $return;
     });
    Route::get ('/ajax/data-for-draw-exchange-rate-areachart/{from_currency_id}/{to_currency_id}', function($from_currency_id,$to_currency_id){
        if ($from_currency_id != $to_currency_id) {
            $from = Currency::find($from_currency_id);
            $to = Currency::find($to_currency_id);
            if(($from != null) && ($to != null)){
                $exchange_rates = ExchangeRate::exchangeRates($from,$to);
                $return = new Collection();
                foreach ($exchange_rates as $exchange_rate) {
                    $return->add([
                        '',
                        $exchange_rate->rate,
                        $exchange_rate->date->format('d/m/Y').' a las '.$exchange_rate->date->format('h:m')."\n1 ".$from->iso_code." = ".$to->truncate($exchange_rate->rate)." ".$to->iso_code,
                    ]);
                }
                return $return;
            }
        }
     });
    Route::get ('/ajax/data-for-draw-exchange-details-areachart/{exchange_id}', function($exchange_id){
        $exchange = Exchange::find($exchange_id);
        if($exchange != null){
            $exchange_rates = ExchangeRate::exchangeRates($exchange->exchangeRate->fromCurrency,$exchange->exchangeRate->toCurrency);
            $return = new Collection();
            foreach ($exchange_rates as $exchange_rate) {
                if($exchange_rate->id != $exchange->exchangeRate->id){
                    $return->add([
                        '',
                        $exchange_rate->rate,
                        null,
                        null,
                        $exchange_rate->date->format('d/m/Y').' a las '.$exchange_rate->date->format('h:m')."\n1 ".$exchange->exchangeRate->fromCurrency->iso_code." = ".$exchange->exchangeRate->toCurrency->truncate($exchange_rate->rate)." ".$exchange->exchangeRate->toCurrency->iso_code,
                    ]);
                }else{
                    $return->add([
                        '',
                        $exchange_rate->rate,
                        'Conversión',
                        $exchange_rate->date->format('d/m/Y').' ('.$exchange->convert(0).' '.$exchange->exchangeRate->fromCurrency->iso_code.' = '.$exchange->exchangeRate->toCurrency->truncate($exchange->convert(1)).' '.$exchange->exchangeRate->toCurrency->iso_code.')',
                        $exchange_rate->date->format('d/m/Y').' a las '.$exchange_rate->date->format('h:m')."\n1 ".$exchange->exchangeRate->fromCurrency->iso_code." = ".$exchange->exchangeRate->toCurrency->truncate($exchange_rate->rate)." ".$exchange->exchangeRate->toCurrency->iso_code,
                    ]);
                }
            }
            return $return;
        }

     });
    Route::get ('/ajax/data-for-draw-wallet-transactions-areachart/{wallet_id}', function($wallet_id){
        $wallet = Wallet::find($wallet_id);
        if ($wallet != null) {
            if (auth()->user()->hasWallet($wallet)) {
                $currency = Currency::find($wallet->currency_id);
                if ($currency != null) {
                    $wallet_transactions = $wallet->walletTransactions;
                    $return = new Collection();
                    $account_balance = 0;
                    $amount = 'Wallet creado: '.$currency->numberToString(0,0);
                    $date = ucfirst($wallet_transactions->first()->date->diffForHumans());
                    $return->add([
                        '',
                        0,
                        $amount."\n".$date,
                    ]);
                    foreach ($wallet_transactions as $wallet_transaction) {
                        $account_balance += $wallet_transaction->amount;
                        $amount = $wallet_transaction->walletTypeOfTransaction->name.': '.$currency->numberToString($wallet_transaction->amount);
                        $date = ucfirst($wallet_transaction->date->diffForHumans());
                        $return->add([
                            '',
                            $account_balance,
                            $amount."\n".$date,
                        ]);
                    }
                    return $return;   
                }
            }
        }
     });
    Route::get ('/ajax/data-for-draw-credits-wallet-donutchart/{wallet_id}',function($wallet_id){
        $wallet = Wallet::find($wallet_id);
        if ($wallet != null) {
            if (auth()->user()->hasWallet($wallet)) {
                $currency = $wallet->currency;
                $items = new Collection();
                $credit_stages = CreditStage::all();
                foreach ($credit_stages as $credit_stage) {
                    $credits = $wallet->creditsInStage($credit_stage);
                    if ($credits->count() > 0) {
                        $amount = 0;
                        foreach ($credits as $credit) {
                            $amount += $credit->amount;
                        }
                        $items->add([
                            ucfirst($credit_stage->name).': '.$currency->numberToString($amount),
                            $amount,
                            $credit_stage->name.': '.$currency->numberToString($amount),
                        ]);
                    }
                }
                return $items;
            }
        }
     });
    Route::get ('/ajax/data-for-draw-financings-wallet-donutchart/{wallet_id}',function($wallet_id){
        $wallet = Wallet::find($wallet_id);
        if ($wallet != null) {
            if (auth()->user()->hasWallet($wallet)) {
                $currency = $wallet->currency;
                $items = new Collection();
                $financing_stages = FinancingStage::all();
                foreach ($financing_stages as $financing_stage) {
                    $financings = $wallet->financingsInStage($financing_stage);
                    if ($financings->count() > 0) {
                        $amount = 0;
                        foreach ($financings as $financing) {
                            if (($financing->lastFinancingPhase()->financingStage->id == FinancingStage::getFinancingAbort()->id) || ($financing->lastFinancingPhase()->financingStage->id == FinancingStage::getCreditAbort()->id)){
                                $amount += $financing->amountWithoutAborts();
                            }else{
                                $amount += $financing->amount();
                            }
                        }
                        $items->add([
                            ucfirst($financing_stage->name).': '.$currency->numberToString($amount),
                            $amount,
                            $financing_stage->name.': '.$currency->numberToString($amount),
                        ]);
                    }
                }
                return $items;
            }
        }
     });
    Route::get ('/ajax/data-for-draw-credits-all-donutchart',function(){
        $wallets = auth()->user()->wallets;
        $usd_currency = Currency::getCurrency('USD');
        $credits_usd = auth()->user()->creditsInUSD();
        $items = new Collection();
        foreach ($wallets as $wallet) {
            $amount = $wallet->creditsAmount();
            $currency = $wallet->currency;
            if ($amount >= $currency->min_value) {
                $usd_amount = ExchangeRate::localConvert($currency->iso_code,$usd_currency->iso_code,$amount);
                if($usd_amount >= $credits_usd * 0.001){
                    $items->add([
                        ucfirst($currency->name).$currency->numberToString($amount),
                        $usd_amount,
                        ucfirst($currency->name).': '.$usd_currency->iso_code.' '.$usd_currency->numberToString($usd_amount),
                    ]);
                }
            }
        }
        return $items;
     });
    Route::get ('/ajax/local-convert/{from}/{to}/{amount}', function($from, $to, $amount){
        return ExchangeRate::localConvert($from, $to, $amount);
    });
    Route::get ('/ajax/data-for-draw-financings-all-donutchart',function(){
        $wallets = auth()->user()->wallets;
        $usd_currency = Currency::getCurrency('USD');
        $financings_usd = auth()->user()->financingsInUSD();
        $items = new Collection();
        foreach ($wallets as $wallet) {
            $amount = $wallet->financingsAmount();
            $currency = $wallet->currency;
            if ($amount >= $currency->min_value) {
                $usd_amount = ExchangeRate::localConvert($currency->iso_code,$usd_currency->iso_code,$amount);
                if($usd_amount >= $financings_usd * 0.001){
                    $items->add([
                        ucfirst($currency->name).$currency->numberToString($amount),
                        $usd_amount,
                        ucfirst($currency->name).': '.$usd_currency->iso_code.' '.$usd_currency->numberToString($usd_amount),
                    ]);
                }
            }
        }
        return $items;
     });
    Route::get ('/ajax/usd-amount-for-wallets',function(){
        return Currency::getCurrency('USD')->numberToString(auth()->user()->walletsInUSD());
    });
    Route::get ('/ajax/usd-amount-for-credits',function(){
        return Currency::getCurrency('USD')->numberToString(auth()->user()->creditsInUSD());
    });
    Route::get ('/ajax/usd-amount-for-financings',function(){
        return Currency::getCurrency('USD')->numberToString(auth()->user()->financingsInUSD());
    });
    Route::get ('/ajax/data-for-draw-credit-financing-areachart/{credit_id}',function($credit_id){
        $credit = Credit::find($credit_id);
        if ($credit != null) {
            if (auth()->user()->hasCredit($credit)) {
                $currency = $credit->currency();
                $items = new Collection();
                if ($credit->lastCreditPhase()->creditStage->id == CreditStage::getFinancing()->id) {
                    $financings = $credit->financings;
                    $amount = 0;
                    $items->add([
                        $credit->lastCreditPhase()->date->format('d/m/Y'),
                        $amount,
                        $currency->numberToString($amount),
                        true,
                    ]);
                    foreach ($financings as $financing) {
                        if ($financing->isActive()) {
                            $amount += $financing->amount();
                            $items->add([
                                $financing->financingPhases->first()->date->format('d/m/Y'),
                                $amount,
                                $currency->numberToString($amount),
                                true,
                            ]);
                        }
                    }
                    $remaining = $credit->financingRemaining();
                    if ($remaining != null) {
                        $items->add($remaining);
                    }
                    return $items;
                }
            }
        }
     });
    Route::get ('/ajax/data-for-draw-margin-areachart/{reputation_id}',function($reputation_id){
        $reputation = Reputation::find($reputation_id);
        if ($reputation != null) {
            $margin_modifiers = $reputation->marginModifiers;
            $items = new Collection();
            $currency = Currency::where('iso_code','USD')->first();
            $amount = 0;
            foreach ($margin_modifiers as $margin_modifier) {
                $amount += $margin_modifier->amount;
                $items->add([
                    '',
                    $amount,
                    $margin_modifier->date->format('d/m/Y').":\n".$margin_modifier->marginModifierStage->name."\n".$currency->numberToString($amount),
                ]);
            }
            return $items;
        }
     });
    Route::get ('/ajax/data-for-draw-interest-areachart/{reputation_id}',function($reputation_id){
        $reputation = Reputation::find($reputation_id);
        if ($reputation != null) {
            $interest_modifiers = $reputation->interestModifiers;
            $items = new Collection();
            $currency = Currency::where('iso_code','USD')->first();
            $percentage = 0;
            foreach ($interest_modifiers as $interest_modifier) {
                $percentage += $interest_modifier->percentage;
                $items->add([
                    '',
                    $percentage,
                    $interest_modifier->date->format('d/m/Y').":\n".$interest_modifier->interestModifierStage->name."\n".$currency->percentage($percentage),
                ]);
            }
            return $items;
        }
     });
    Route::get ('/ajax/data-for-draw-insurance-areachart/{reputation_id}',function($reputation_id){
        $reputation = Reputation::find($reputation_id);
        if ($reputation != null) {
            $insurance_modifiers = $reputation->insuranceModifiers;
            $items = new Collection();
            $currency = Currency::where('iso_code','USD')->first();
            $percentage = 0;
            foreach ($insurance_modifiers as $insurance_modifier) {
                $percentage += $insurance_modifier->percentage;
                $items->add([
                    '',
                    $percentage,
                    $insurance_modifier->date->format('d/m/Y').":\n".$insurance_modifier->insuranceModifierStage->name."\n".$currency->percentage($percentage),
                ]);
            }
            return $items;
        }
     });
    Route::get ('/ajax/data-for-draw-credit-pay-left-donutchart/{credit_id}',function($credit_id){
        $credit = Credit::find($credit_id);
        if ($credit != null) {
            if (auth()->user()->hasCredit($credit)) {
                $currency = $credit->currency();
                $items = new Collection();
                if ($credit->unpayedAmount() > 0) {
                    $items->add([
                        'Pagado: '.$currency->numberToString($credit->payedAmount()),
                        $credit->payedAmount(),
                        'Pagado: '.$currency->numberToString($credit->payedAmount()),
                    ]);
                    $items->add([
                        'Deuda: '.$currency->numberToString($credit->unpayedAmount()),
                        $credit->unpayedAmount(),
                        'Deuda: '.$currency->numberToString($credit->unpayedAmount()),
                    ]);
                    return $items;
                }
            }
        }
     });
    Route::get ('/ajax/data-for-draw-payment-phases-calendarchart/{payment_id}',function($payment_id){
        $payment = Payment::find($payment_id);
        if ($payment != null) {
            if (auth()->user()->hasPayment($payment)) {
                $currency = $payment->credit->currency();
                $items = [];
                $payment_phases = $payment->paymentPhases;
                $item_date = new Carbon($payment_phases->first()->date->toDateString());
                $item_date->startOfYear();
                $max = new Carbon($payment_phases->last()->date->toDateString());
                $max->endOfYear();
                while ($item_date <= $max) {
                    switch($item_date->format('m')){
                        case "01":
                            $month = 'enero';
                            break;
                        case "02":
                            $month = 'febrero';
                            break;
                        case "03":
                            $month = 'marzo';
                            break;
                        case "04":
                            $month = 'abril';
                            break;
                        case "05":
                            $month = 'mayo';
                            break;
                        case "06":
                            $month = 'junio';
                            break;
                        case "07":
                            $month = 'julio';
                            break;
                        case "08":
                            $month = 'agosto';
                            break;
                        case "09":
                            $month = 'septiembre';
                            break;
                        case "10":
                            $month = 'octubre';
                            break;
                        case "11":
                            $month = 'noviembre';
                            break;
                        case "12":
                            $month = 'diciembre';
                            break;
                    } 	
                    $value = ([
                        [
                            (int) $item_date->format('Y'),
                            (int) $item_date->format('m'),
                            (int) $item_date->format('d'),
                        ],
                        0,
                        $item_date->format('d').' de '.$month. ' de '.$item_date->format('Y').':',
                    ]);
                    array_push ( $items , $value);
                    $item_date->addDay();
                }
                foreach ($payment_phases as $payment_phase) {
                    /*    
                        Estados a listar:       
                        1 => 'Generado' ---------> getGenerated() -->$
                        2 => 'Pago' -------------> getPay() -->$
                        3 => 'Vencido' ----------> getDue()
                        6 => 'Saldado' ----------> getPayed()
                    */
                    $stage = $payment_phase->paymentStage;    
                    if (($stage->id == PaymentStage::getGenerated()->id) || ($stage->id == PaymentStage::getPay()->id) || ($stage->id == PaymentStage::getDue()->id) ||($stage->id == PaymentStage::getPayed()->id)) {
                        foreach ($items as $key => $item){
                            if( new Carbon($item[0][0].'-'.$item[0][1].'-'.$item[0][2].' 00:00:00') == new Carbon($payment_phase->date->toDateString())) {
                                $items[$key][1] = 1;
                                $amount = '';
                                if (($stage->id == PaymentStage::getGenerated()->id) || ($stage->id == PaymentStage::getPay()->id)) {
                                    $amount = ' ('.$currency->numberToString($payment_phase->amount).')';
                                }
                                if (substr($items[$key][2], -1) == ':'){
                                    $items[$key][2] = $items[$key][2].' '.$stage->name.$amount;
                                }else{
                                    $items[$key][2] = $items[$key][2].'. '.$stage->name.$amount;
                                }
                                break;
                            }
                        }
                    }
                }//foreach ($payment_phases as $payment_phase) 
                return $items;
            }
        }
     });
    Route::get ('/ajax/data-for-draw-payments-index-calendarchart/{credit_id}',function($credit_id){
        $credit = Credit::find($credit_id);
        if ($credit != null) {
            if ((auth()->user()->hasCredit($credit)) && ($credit->payments->count() > 0)) {
                $currency = $credit->currency();
                $items = [];
                $payments = $credit->payments;
                $date_of_payments_generated = new Carbon($payments->first()->paymentPhases->first()->date->toDateString());
                $item_date = new Carbon($date_of_payments_generated->toDateString());
                $item_date->startOfYear();
                $last_due_date = new Carbon($payments->last()->due_date->toDateString());
                $last_payment_phase_date = new Carbon($payments->first()->paymentPhases->last()->date->toDateString());
                foreach($payments as $payment){
                    $to_compare = new Carbon($payment->lastPhaseGeneratedOrPayOrPayedOrDueDate()->toDateString());
                    if ($last_payment_phase_date < $to_compare) {
                        $last_payment_phase_date = $to_compare;
                    }
                }
                $max = $last_due_date;
                if ($last_due_date < $last_payment_phase_date){
                    $max = $last_payment_phase_date;
                }
                $max->endOfYear();
                while ($item_date <= $max) {
                    switch($item_date->format('m')){
                        case "01":
                            $month = 'enero';
                            break;
                        case "02":
                            $month = 'febrero';
                            break;
                        case "03":
                            $month = 'marzo';
                            break;
                        case "04":
                            $month = 'abril';
                            break;
                        case "05":
                            $month = 'mayo';
                            break;
                        case "06":
                            $month = 'junio';
                            break;
                        case "07":
                            $month = 'julio';
                            break;
                        case "08":
                            $month = 'agosto';
                            break;
                        case "09":
                            $month = 'septiembre';
                            break;
                        case "10":
                            $month = 'octubre';
                            break;
                        case "11":
                            $month = 'noviembre';
                            break;
                        case "12":
                            $month = 'diciembre';
                            break;
                    } 	
                    $value = ([
                        [
                            (int) $item_date->format('Y'),
                            (int) $item_date->format('m'),
                            (int) $item_date->format('d'),
                        ],
                        0,
                        $item_date->format('d').' de '.$month. ' de '.$item_date->format('Y').':',
                    ]);
                    array_push ( $items , $value);
                    $item_date->addDay();
                }
                foreach ($items as $key => $item){
                    if( new Carbon($item[0][0].'-'.$item[0][1].'-'.$item[0][2].' 00:00:00') == $date_of_payments_generated) {
                        $amount = $currency->numberToString($payments->first()->paymentPhases->first()->amount);
                        $items[$key][1] = 1;                       
                        $items[$key][2] = $items[$key][2].' Cuotas generadas ('.$amount.' cada cuota)';
                        break;
                    }
                }
                foreach ($payments as $payment) {
                    foreach ($items as $key => $item){
                        if( new Carbon($item[0][0].'-'.$item[0][1].'-'.$item[0][2].' 00:00:00') == new Carbon($payment->due_date->toDateString())) {
                            $items[$key][1] = 1;
                            if (substr($items[$key][2], -1) == ':'){
                                $items[$key][2] = $items[$key][2].' Vencimiento cuota '.$payment->number();
                            }else{
                                $items[$key][2] = $items[$key][2].'. Vencimiento cuota '.$payment->number();
                            }
                            break;
                        }
                    }
                    $payment_phases = $payment->paymentPhases;
                    foreach ($payment_phases as $payment_phase) {
                        if (($payment_phase->paymentStage->id == PaymentStage::getPay()->id) || ($payment_phase->paymentStage->id == PaymentStage::getPayed()->id)) {
                            foreach ($items as $key => $item){
                                if( new Carbon($item[0][0].'-'.$item[0][1].'-'.$item[0][2].' 00:00:00') == new Carbon($payment_phase->date->toDateString())) {
                                    if ($payment_phase->paymentStage->id == PaymentStage::getPay()->id) {
                                        $amount = $currency->numberToString($payment_phase->amount);
                                        $items[$key][1] = 1;
                                        if (substr($items[$key][2], -1) == ':'){
                                            $items[$key][2] = $items[$key][2].' Pago cuota '.$payment->number().' ('.$amount.')';
                                        }else{
                                            $items[$key][2] = $items[$key][2].'. Pago cuota '.$payment->number().' ('.$amount.')';
                                        }
                                        break;
                                    }
                                }
                            }//foreach ($items as $item){
                        }
                    }//foreach ($payment_phases as $payment_phase)
                }//foreach ($payments as $payment)
                return $items;
            }    
        }
     });
    Route::get ('/ajax/data-for-draw-credit-phases-calendarchart/{credit_id}',function($credit_id){
        $credit = Credit::find($credit_id);
        if ($credit != null) {
            if (auth()->user()->hasCredit($credit)) {
                $currency = $credit->currency();
                $items = [];
                $credit_phases = $credit->creditPhases;
                $item_date = new Carbon($credit_phases->first()->date->toDateString());
                $item_date->startOfYear();
                $max = new Carbon($credit_phases->last()->date->toDateString());
                $max->endOfYear();
                while ($item_date <= $max) {
                    switch($item_date->format('m')){
                        case "01":
                            $month = 'enero';
                            break;
                        case "02":
                            $month = 'febrero';
                            break;
                        case "03":
                            $month = 'marzo';
                            break;
                        case "04":
                            $month = 'abril';
                            break;
                        case "05":
                            $month = 'mayo';
                            break;
                        case "06":
                            $month = 'junio';
                            break;
                        case "07":
                            $month = 'julio';
                            break;
                        case "08":
                            $month = 'agosto';
                            break;
                        case "09":
                            $month = 'septiembre';
                            break;
                        case "10":
                            $month = 'octubre';
                            break;
                        case "11":
                            $month = 'noviembre';
                            break;
                        case "12":
                            $month = 'diciembre';
                            break;
                    } 	
                    $value = ([
                        [
                            (int) $item_date->format('Y'),
                            (int) $item_date->format('m'),
                            (int) $item_date->format('d'),
                        ],
                        0,
                        $item_date->format('d').' de '.$month. ' de '.$item_date->format('Y').':',
                    ]);
                    array_push ( $items , $value);
                    $item_date->addDay();
                }
                foreach ($credit_phases as $credit_phase) {
                    /*
                        Estados a listar: TODOS
                        1  => 'Solicitado' >>>>>>>>>>>>>>>>>> ::getFirst()
                        2  => 'Financiando' >>>>>>>>>>>>>>>>> ::getFinancing()
                        3  => 'Financiacion Finalizada' >>>>> ::getFinanced()
                        4  => 'Otorgado' >>>>>>>>>>>>>>>>>>>> ::getGrant()
                        5  => 'Al dia' >>>>>>>>>>>>>>>>>>>>>> ::getGood()
                        6  => 'Pago retrasado' >>>>>>>>>>>>>> ::getPaymentLate()
                        7  => 'Mora' >>>>>>>>>>>>>>>>>>>>>>>> ::getDefaulter()
                        8  => 'Citado' >>>>>>>>>>>>>>>>>>>>>> ::getJudicialCitation()
                        9  => 'Litigio' >>>>>>>>>>>>>>>>>>>>> ::getLawsuit()
                        10 => 'Saldado' >>>>>>>>>>>>>>>>>>>>> ::getLiquidate()
                        11 => 'Cancelado' >>>>>>>>>>>>>>>>>>> ::getAbort()
                        12 => 'Rechazado' >>>>>>>>>>>>>>>>>>> ::getReject()
                        13 => 'Incobrable' >>>>>>>>>>>>>>>>>> ::getBad()
                        14 >> 'Financiacion rehabierta'>>>>>> ::getFinancingReopened()
                    */
                    $stage = $credit_phase->creditStage;    
                    foreach ($items as $key => $item){
                        if( new Carbon($item[0][0].'-'.$item[0][1].'-'.$item[0][2].' 00:00:00') == new Carbon($credit_phase->date->toDateString())) {
                            $items[$key][1] = 1;
                            if (substr($items[$key][2], -1) == ':'){
                                $items[$key][2] = $items[$key][2].' '.$stage->name;
                            }else{
                                $items[$key][2] = $items[$key][2].'. '.$stage->name;
                            }
                            break;
                        }
                    }
                }//foreach ($credit_phases as $credit_phase) 
                return $items;
            }
        }
     });
    Route::get ('/ajax/data-for-draw-financing-phases-calendarchart/{financing_id}',function($financing_id){
        $financing = Financing::find($financing_id);
        if ($financing != null) {
            if (auth()->user()->hasFinancing($financing)) {
                $currency = $financing->currency();
                $items = [];
                $financing_phases = $financing->financingPhases;
                $item_date = new Carbon($financing_phases->first()->date->toDateString());
                $item_date->startOfYear();
                $max = new Carbon($financing_phases->last()->date->toDateString());
                $max->endOfYear();
                while ($item_date <= $max) {
                    switch($item_date->format('m')){
                        case "01":
                            $month = 'enero';
                            break;
                        case "02":
                            $month = 'febrero';
                            break;
                        case "03":
                            $month = 'marzo';
                            break;
                        case "04":
                            $month = 'abril';
                            break;
                        case "05":
                            $month = 'mayo';
                            break;
                        case "06":
                            $month = 'junio';
                            break;
                        case "07":
                            $month = 'julio';
                            break;
                        case "08":
                            $month = 'agosto';
                            break;
                        case "09":
                            $month = 'septiembre';
                            break;
                        case "10":
                            $month = 'octubre';
                            break;
                        case "11":
                            $month = 'noviembre';
                            break;
                        case "12":
                            $month = 'diciembre';
                            break;
                    } 	
                    $value = ([
                        [
                            (int) $item_date->format('Y'),
                            (int) $item_date->format('m'),
                            (int) $item_date->format('d'),
                        ],
                        0,
                        $item_date->format('d').' de '.$month. ' de '.$item_date->format('Y').':',
                    ]);
                    array_push ( $items , $value);
                    $item_date->addDay();
                }
                foreach ($financing_phases as $financing_phase) {
                    /*
                        Estados a listar: TODOS
                        1  => 'Solicitado' >>>>>>>>>>>>>>>>>> ::getFirst()
                        2  => 'Financiando' >>>>>>>>>>>>>>>>> ::getFinancing()
                        3  => 'Financiacion Finalizada' >>>>> ::getFinanced()
                        4  => 'Otorgado' >>>>>>>>>>>>>>>>>>>> ::getGrant()
                        5  => 'Al dia' >>>>>>>>>>>>>>>>>>>>>> ::getGood()
                        6  => 'Pago retrasado' >>>>>>>>>>>>>> ::getPaymentLate()
                        7  => 'Mora' >>>>>>>>>>>>>>>>>>>>>>>> ::getDefaulter()
                        8  => 'Citado' >>>>>>>>>>>>>>>>>>>>>> ::getJudicialCitation()
                        9  => 'Litigio' >>>>>>>>>>>>>>>>>>>>> ::getLawsuit()
                        10 => 'Saldado' >>>>>>>>>>>>>>>>>>>>> ::getLiquidate()
                        11 => 'Cancelado' >>>>>>>>>>>>>>>>>>> ::getAbort()
                        12 => 'Rechazado' >>>>>>>>>>>>>>>>>>> ::getReject()
                        13 => 'Incobrable' >>>>>>>>>>>>>>>>>> ::getBad()
                        14 >> 'Financiacion rehabierta'>>>>>> ::getFinancingReopened()
                    */
                    $stage = $financing_phase->financingStage;    
                    foreach ($items as $key => $item){
                        if( new Carbon($item[0][0].'-'.$item[0][1].'-'.$item[0][2].' 00:00:00') == new Carbon($financing_phase->date->toDateString())) {
                            $items[$key][1] = 1;
                            if (substr($items[$key][2], -1) == ':'){
                                $items[$key][2] = $items[$key][2].' '.$stage->name;
                            }else{
                                $items[$key][2] = $items[$key][2].'. '.$stage->name;
                            }
                            break;
                        }
                    }
                }//foreach ($financing_phases as $financing_phase) 
                return $items;
            }
        }
     });
});



Route::get ('/', 'Site\SiteController@index');

Auth::routes();




/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|*/
